﻿Module Module1

    Const APP_NAME = "Instalador de Tablas catalogo_errores_crm y configuracion_crm"
    Const MSG_ODBCKEY = 1
    Const MSG_IVA = 2
    Const MSG_IB = 3

    Dim Connection

    Sub Main()
        Try
            Call StartConnection()

            Call CreartablaCatalogoErrores()
            Console.WriteLine("Se creo la tabla Catalogo de erores")

            Call CreartablaConfiguracionCrm()
            Console.WriteLine("Se creo la tabla Configuracion CRM")

            Call CreartablaChequesOffline()
            Console.WriteLine("Se creo la tabla Configuracion Cheques")

            Call CreartablaAcreditaciones()
            Console.WriteLine("Se creo la tabla Configuracion Acreditaciones")

            Call CreartablaNiveles()
            Console.WriteLine("Se creo la tabla Configuracion Niveles")

            Call CreartablaRespuesta()
            Console.WriteLine("Se creo la tabla Configuracion Respuesta")


            Call CreartablaLoyTrans()
            Console.WriteLine("Se creo la tabla Configuracion LoyTrans")

            Call CreartablaLoyTransOff()
            Console.WriteLine("Se creo la tabla Configuracion LoyTransOff")

            Call CreartablaLoyTransRes()
            Console.WriteLine("Se creo la tabla Configuracion LoyTranRes")

            Call CreartablaPagaTodo()
            Console.WriteLine("Se creo la tabla Configuracion PagaTodo")

            Call CreartablaClientes()
            Console.WriteLine("Se creo la tabla Configuracion Clientes")

            Call CreartablaSoa()
            Console.WriteLine("Se creo la tabla Configuracion Soa")

            Call CreartablaMicrosWow()
            Console.WriteLine("Se creo la tabla Configuracion Soa")

            Console.WriteLine("Se crearon todas las tablas correctamete")

            Call CloseConnection()

        Catch ex As Exception
            MsgBox("Error>> " & vbCrLf & ex.Message)
            Call CloseConnection()
        End Try
    End Sub

    Sub StartConnection()
        Connection = CreateObject("ADODB.Connection")
        Connection.Open("DSN=micros; UID=dba; PWD=Password1")
    End Sub

    Sub CloseConnection()
        Connection.Close()
    End Sub

    Function TablesExist(ByVal tipo As Integer)
        Dim RS
        Dim strResponse
        If tipo = 1 Then
            RS = Connection.Execute("select * from sysobjects where name = 'catalogo_errores_crm'")
        ElseIf tipo = 2 Then
            RS = Connection.Execute("select * from sysobjects where name = 'configuracion_crm'")
        End If

        TablesExist = Not RS.Eof
    End Function

    Sub CreartablaCatalogoErrores()
        Dim iReturn

        If TablesExist(1) Then
            Call DropTablesCatErro()
            Call CreateTablesCatErro()
            Call InsertTableCatErro()
        Else
            Call CreateTablesCatErro()
            Call InsertTableCatErro()
        End If
    End Sub

    Sub DropTablesCatErro()
        Dim RS
        RS = Connection.Execute("drop table custom.catalogo_errores_crm")
    End Sub

    Sub CreateTablesCatErro()
        Dim RS
        Dim sqlCmd
        sqlCmd = "CREATE TABLE custom.catalogo_errores_crm ("
        sqlCmd = sqlcmd & " Id_Error_Crm    integer     NOT NULL DEFAULT autoincrement,"
        sqlCmd = sqlcmd & " Error           varchar(25) NOT NULL,"
        sqlCmd = sqlcmd & " Tipo            varchar(25) NOT NULL,"
        sqlCmd = sqlcmd & " Mensaje         varchar(120) NOT NULL,"
        sqlCmd = sqlcmd & " PRIMARY KEY (Id_Error_Crm) )"

        RS = Connection.Execute(sqlCmd)
    End Sub

    Sub InsertTableCatErro()
        Dim RS
        Dim sqlCmd
        sqlCmd = "INSERT INTO custom.catalogo_errores_crm (Error,Tipo,Mensaje)"
        sqlCmd = sqlcmd & " VALUES ('<WS_TIMEOUT>','<Success>','Tiempo de espera demasiado grande.');"
        sqlCmd = sqlcmd & " INSERT INTO custom.catalogo_errores_crm (Error,Tipo,Mensaje)"
        sqlCmd = sqlcmd & " VALUES ('<WS_TIMEOUT>','<Success>','No se puede procesar la informacion de acreditacion.');"
        sqlCmd = sqlcmd & " INSERT INTO custom.catalogo_errores_crm (Error,Tipo,Mensaje)"
        sqlCmd = sqlcmd & " VALUES ('<WS_TIMEOUT>','<Success>','Continua con el proceso de Venta de manera normal.');"
        sqlCmd = sqlcmd & " INSERT INTO custom.catalogo_errores_crm (Error,Tipo,Mensaje)"
        sqlCmd = sqlcmd & " VALUES ('<WS_OFFLINE>','<Success>','Operacion no permitida debido a sistema Offline');"
        sqlCmd = sqlcmd & " INSERT INTO custom.catalogo_errores_crm (Error,Tipo,Mensaje)"
        sqlCmd = sqlcmd & " VALUES ('<WS_OFFLINE>','<Success>','Por el momento no se pueden mostrar los datos del cliente.');"
        sqlCmd = sqlcmd & " INSERT INTO custom.catalogo_errores_crm (Error,Tipo,Mensaje)"
        sqlCmd = sqlcmd & " VALUES ('<WS_SOA_ERROR>','<SVC-MEMBER-001>','No es posible leer los datos de la tarjeta.');"
        sqlCmd = sqlcmd & " INSERT INTO custom.catalogo_errores_crm (Error,Tipo,Mensaje)"
        sqlCmd = sqlcmd & " VALUES ('<WS_SOA_ERROR>','<SVC-MEMBER-001>','[SVC-MEMBER-001]');"
        sqlCmd = sqlcmd & " INSERT INTO custom.catalogo_errores_crm (Error,Tipo,Mensaje)"
        sqlCmd = sqlcmd & " VALUES ('<WS_SOA_ERROR>','<SVC-MEMBER-002>','Ocurrio un error al almacenar los datos de la tarjeta');"
        sqlCmd = sqlcmd & " INSERT INTO custom.catalogo_errores_crm (Error,Tipo,Mensaje)"
        sqlCmd = sqlcmd & " VALUES ('<WS_SOA_ERROR>','<SVC-MEMBER-002>','[SVC-MEMBER-002]');"
        sqlCmd = sqlcmd & " INSERT INTO custom.catalogo_errores_crm (Error,Tipo,Mensaje)"
        sqlCmd = sqlcmd & " VALUES ('<WS_SOA_ERROR>','<SVC-MEMBER-003>','No se pudo guardar la acreditacion al cliente');"
        sqlCmd = sqlcmd & " INSERT INTO custom.catalogo_errores_crm (Error,Tipo,Mensaje)"
        sqlCmd = sqlcmd & " VALUES ('<WS_SOA_ERROR>','<SVC-MEMBER-003>','Pide a cliente comunicarse a Call Center');"
        sqlCmd = sqlcmd & " INSERT INTO custom.catalogo_errores_crm (Error,Tipo,Mensaje)"
        sqlCmd = sqlcmd & " VALUES ('<WS_SOA_ERROR>','<SVC-MEMBER-004>','Ocurrio un error al almacenar la transacción de acreditacion');"
        sqlCmd = sqlcmd & " INSERT INTO custom.catalogo_errores_crm (Error,Tipo,Mensaje)"
        sqlCmd = sqlcmd & " VALUES ('<WS_SOA_ERROR>','<SVC-MEMBER-004>','[SVC-MEMBER-004]');"
        sqlCmd = sqlcmd & " INSERT INTO custom.catalogo_errores_crm (Error,Tipo,Mensaje)"
        sqlCmd = sqlcmd & " VALUES ('<WS_SOA_ERROR>','<SVC-MEMBER-005>','Ocurrio un error al crear el miembro [SVC-MEMBER-005]');"
        sqlCmd = sqlcmd & " INSERT INTO custom.catalogo_errores_crm (Error,Tipo,Mensaje)"
        sqlCmd = sqlcmd & " VALUES ('<WS_SOA_ERROR>','<SVC-MEMBER-006>','Ocurrio un error al crear el miembro [SVC-MEMBER-006]');"
        sqlCmd = sqlcmd & " INSERT INTO custom.catalogo_errores_crm (Error,Tipo,Mensaje)"
        sqlCmd = sqlcmd & " VALUES ('<WS_SOA_ERROR>','<SVC-MEMBER-007>','No se pudo guardar la redencion al cliente');"
        sqlCmd = sqlcmd & " INSERT INTO custom.catalogo_errores_crm (Error,Tipo,Mensaje)"
        sqlCmd = sqlcmd & " VALUES ('<WS_SOA_ERROR>','<SVC-MEMBER-007>','Levanta Ticket en Mesa de Servicio e indica No. de cheque');"
        sqlCmd = sqlcmd & " INSERT INTO custom.catalogo_errores_crm (Error,Tipo,Mensaje)"
        sqlCmd = sqlcmd & " VALUES ('<WS_SOA_ERROR>','<SVC-MEMBER-008>','Ocurrio un error al almacenar la transacción de redencion');"
        sqlCmd = sqlcmd & " INSERT INTO custom.catalogo_errores_crm (Error,Tipo,Mensaje)"
        sqlCmd = sqlcmd & " VALUES ('<WS_SOA_ERROR>','<SVC-MEMBER-008>','[SVC-MEMBER-008]');"
        sqlCmd = sqlcmd & " INSERT INTO custom.catalogo_errores_crm (Error,Tipo,Mensaje)"
        sqlCmd = sqlcmd & " VALUES ('<WS_SOA_ERROR>','<SVC-MEMBER-009>','Ocurrio un error al obtener los datos de las transacciones');"
        sqlCmd = sqlcmd & " INSERT INTO custom.catalogo_errores_crm (Error,Tipo,Mensaje)"
        sqlCmd = sqlcmd & " VALUES ('<WS_SOA_ERROR>','<SVC-MEMBER-009>','[SVC-MEMBER-009]');"
        sqlCmd = sqlcmd & " INSERT INTO custom.catalogo_errores_crm (Error,Tipo,Mensaje)"
        sqlCmd = sqlcmd & " VALUES ('<WS_SOA_ERROR>','<SVC-MEMBER-010>','Ocurrio un error al obtener los datos de las transacciones');"
        sqlCmd = sqlcmd & " INSERT INTO custom.catalogo_errores_crm (Error,Tipo,Mensaje)"
        sqlCmd = sqlcmd & " VALUES ('<WS_SOA_ERROR>','<SVC-MEMBER-010>','[SVC-MEMBER-010]');"
        sqlCmd = sqlcmd & " INSERT INTO custom.catalogo_errores_crm (Error,Tipo,Mensaje)"
        sqlCmd = sqlcmd & " VALUES ('<WS_SOA_ERROR>','<SVC-MEMBER-011>','Ocurrio un error al cancelar la acreditacion');"
        sqlCmd = sqlcmd & " INSERT INTO custom.catalogo_errores_crm (Error,Tipo,Mensaje)"
        sqlCmd = sqlcmd & " VALUES ('<WS_SOA_ERROR>','<SVC-MEMBER-011>','[SVC-MEMBER-011]');"
        sqlCmd = sqlcmd & " INSERT INTO custom.catalogo_errores_crm (Error,Tipo,Mensaje)"
        sqlCmd = sqlcmd & " VALUES ('<WS_SOA_ERROR>','<SVC-MEMBER-012>','Ocurrio un error al cancelar la acreditacion');"
        sqlCmd = sqlcmd & " INSERT INTO custom.catalogo_errores_crm (Error,Tipo,Mensaje)"
        sqlCmd = sqlcmd & " VALUES ('<WS_SOA_ERROR>','<SVC-MEMBER-012>','[SVC-MEMBER-012]');"
        sqlCmd = sqlcmd & " INSERT INTO custom.catalogo_errores_crm (Error,Tipo,Mensaje)"
        sqlCmd = sqlcmd & " VALUES ('<WS_SOA_ERROR>','<SVC-MEMBER-013>','Ocurrio un error al actualizar la informacion del miembro');"
        sqlCmd = sqlcmd & " INSERT INTO custom.catalogo_errores_crm (Error,Tipo,Mensaje)"
        sqlCmd = sqlcmd & " VALUES ('<WS_SOA_ERROR>','<SVC-MEMBER-013>','[SVC-MEMBER-013]');"
        sqlCmd = sqlcmd & " INSERT INTO custom.catalogo_errores_crm (Error,Tipo,Mensaje)"
        sqlCmd = sqlcmd & " VALUES ('<WS_SOA_ERROR>','<SVC-MEMBER-014>','Promocion no valida. Necesario revisar productos marcados');"
        sqlCmd = sqlcmd & " INSERT INTO custom.catalogo_errores_crm (Error,Tipo,Mensaje)"
        sqlCmd = sqlcmd & " VALUES ('<WS_SOA_ERROR>','<NO_WS>','Tarjeta Cancelada o no inscrita en el programa');"

        RS = Connection.Execute(sqlCmd)
    End Sub

    Sub CreartablaConfiguracionCrm()
        Dim iReturn

        If TablesExist(2) Then
            Call DropTablesConCrm()
            Call CreateTablesConCrm()
            Call InsertTableConCrm()
        Else
            Call CreateTablesConCrm()
            Call InsertTableConCrm()
        End If
    End Sub

    Sub DropTablesConCrm()
        Dim RS
        RS = Connection.Execute("drop table custom.configuracion_crm")
    End Sub

    Sub CreateTablesConCrm()
        Dim RS
        Dim sqlCmd
        sqlCmd = "CREATE TABLE custom.configuracion_crm ("
        sqlCmd = sqlCmd & " Id_Config_Crm	integer		NOT NULL DEFAULT autoincrement,"
        sqlCmd = sqlCmd & " Etiqueta_Crm	varchar(60)	NOT NULL,"
        sqlCmd = sqlCmd & " Dato_Crm		varchar(60)	NOT NULL,"
        sqlCmd = sqlCmd & " PRIMARY KEY (Id_Config_Crm) )"
        RS = Connection.Execute(sqlCmd)
    End Sub

    Sub InsertTableConCrm()
        Dim RS
        Dim sqlCmd
        sqlCmd = "INSERT INTO custom.configuracion_crm (Etiqueta_Crm,Dato_crm)"
        sqlCmd = sqlcmd & " VALUES ('Interface CRM','6');"
        sqlCmd = sqlcmd & " INSERT INTO custom.configuracion_crm (Etiqueta_Crm,Dato_crm)"
        sqlCmd = sqlcmd & " VALUES ('Nombre del Programa','WOW Rewards');"
        sqlCmd = sqlcmd & " INSERT INTO custom.configuracion_crm (Etiqueta_Crm,Dato_crm)"
        sqlCmd = sqlcmd & " VALUES ('PartnerName','Burger King Mexico');"
        sqlCmd = sqlcmd & " INSERT INTO custom.configuracion_crm (Etiqueta_Crm,Dato_crm)"
        sqlCmd = sqlcmd & " VALUES ('TouchScreen Alfanumerica','50');"
        sqlCmd = sqlcmd & " INSERT INTO custom.configuracion_crm (Etiqueta_Crm,Dato_crm)"
        sqlCmd = sqlcmd & " VALUES ('TouchScreen Numerica ','60');"
        sqlCmd = sqlcmd & " INSERT INTO custom.configuracion_crm (Etiqueta_Crm,Dato_crm)"
        sqlCmd = sqlcmd & " VALUES ('TouchScreen Pago_WOW','61');"
        sqlCmd = sqlcmd & " INSERT INTO custom.configuracion_crm (Etiqueta_Crm,Dato_crm)"
        sqlCmd = sqlcmd & " VALUES ('Tender_Crm','60');"
        sqlCmd = sqlcmd & " INSERT INTO custom.configuracion_crm (Etiqueta_Crm,Dato_crm)"
        sqlCmd = sqlcmd & " VALUES ('Tender_Efectivo_No','1');"
        sqlCmd = sqlcmd & " INSERT INTO custom.configuracion_crm (Etiqueta_Crm,Dato_crm)"
        sqlCmd = sqlcmd & " VALUES ('Descuento_RedondeoWOW','110');"
        sqlCmd = sqlcmd & " INSERT INTO custom.configuracion_crm (Etiqueta_Crm,Dato_crm)"
        sqlCmd = sqlcmd & " VALUES ('Descuento_Redenciones$','109');"
        sqlCmd = sqlcmd & " INSERT INTO custom.configuracion_crm (Etiqueta_Crm,Dato_crm)"
        sqlCmd = sqlcmd & " VALUES ('Descuento_Redenciones%','108');"
        sqlCmd = sqlcmd & " INSERT INTO custom.configuracion_crm (Etiqueta_Crm,Dato_crm)"
        sqlCmd = sqlcmd & " VALUES ('Cupon_Desc_P1','112');"
        sqlCmd = sqlcmd & " INSERT INTO custom.configuracion_crm (Etiqueta_Crm,Dato_crm)"
        sqlCmd = sqlcmd & " VALUES ('Cupon_Desc_P2','113');"
        sqlCmd = sqlcmd & " INSERT INTO custom.configuracion_crm (Etiqueta_Crm,Dato_crm)"
        sqlCmd = sqlcmd & " VALUES ('Cupon_Desc_MM','111');"
        sqlCmd = sqlcmd & " INSERT INTO custom.configuracion_crm (Etiqueta_Crm,Dato_crm)"
        sqlCmd = sqlcmd & " VALUES ('Donaciones','39');"
        sqlCmd = sqlcmd & " INSERT INTO custom.configuracion_crm (Etiqueta_Crm,Dato_crm)"
        sqlCmd = sqlcmd & " VALUES ('Donaciones1','40');"

        RS = Connection.Execute(sqlCmd)
    End Sub

    Sub CreartablaChequesOffline()
        Dim iReturn

        If TablesExist(1) Then
            Call DropTablesCheques()
            Call CreateTablesCheques()

        Else
            Call CreateTablesCheques()

        End If
    End Sub


    Sub DropTablesCheques()
        Dim RS
        RS = Connection.Execute("drop table custom.CHEQUES_OFFLINE")
    End Sub

    Sub CreateTablesCheques()
        Dim RS
        Dim sqlCmd
        sqlCmd = "CREATE TABLE custom.CHEQUES_OFFLINE ("
        sqlCmd = sqlcmd & " Consecutivo   		integer NOT NULL DEFAULT autoincrement, "
        sqlCmd = sqlcmd & " TicketNumber  		varchar(255) NULL,"
        sqlCmd = sqlcmd & " Procesado     		varchar(20) NULL,"
        sqlCmd = sqlcmd & " PRIMARY KEY (Consecutivo) )"
        RS = Connection.Execute(sqlCmd)
    End Sub

    Sub CreartablaAcreditaciones()
        Dim iReturn

        If TablesExist(1) Then
            Call DropTablesAcreditaciones()
            Call CreateTablesAcreditaciones()

        Else
            Call CreateTablesAcreditaciones()

        End If
    End Sub


    Sub DropTablesAcreditaciones()
        Dim RS
        RS = Connection.Execute("drop table custom.CRM_Acreditacion_WS")
    End Sub

    Sub CreateTablesAcreditaciones()
        Dim RS
        Dim sqlCmd
        sqlCmd = "CREATE TABLE  custom . CRM_Acreditacion_WS ("
        sqlCmd = sqlCmd & " Consecutivo    		integer NOT NULL DEFAULT autoincrement,"
        sqlCmd = sqlCmd & " MemberNumber   		char(20) NOT NULL,"
        sqlCmd = sqlCmd & " id     			varchar(255) NULL,"
        sqlCmd = sqlCmd & " ALSELevelPrice         	varchar(255) NULL,"
        sqlCmd = sqlCmd & " BranchDivision         	varchar(255) NULL,"
        sqlCmd = sqlCmd & " ALSEPaymnetMode        	varchar(255) NULL,"
        sqlCmd = sqlCmd & " ActivityDate   		varchar(255) NULL,"
        sqlCmd = sqlCmd & " AdjustedListPrice      	varchar(255) NULL,"
        sqlCmd = sqlCmd & " Amount         		varchar(255) NULL,"
        sqlCmd = sqlCmd & " Comments       		varchar(255) NULL,"
        sqlCmd = sqlCmd & " ItemNumber     		varchar(255) NULL,"
        sqlCmd = sqlCmd & " LocationName   		varchar(255) NULL,"
        sqlCmd = sqlCmd & " PartnerName    		varchar(255) NULL,"
        sqlCmd = sqlCmd & " PointName      		varchar(255) NULL,"
        sqlCmd = sqlCmd & " Points         		varchar(255) NULL,"
        sqlCmd = sqlCmd & " ProcessDate    		varchar(255) NULL,"
        sqlCmd = sqlCmd & " ProcessingComment      	varchar(255) NULL,"
        sqlCmd = sqlCmd & " ProcessingLog  		varchar(255) NULL,"
        sqlCmd = sqlCmd & " ProductName    		varchar(255) NULL,"
        sqlCmd = sqlCmd & " Quantity       		varchar(255) NULL,"
        sqlCmd = sqlCmd & " IntegrationStatus      	varchar(255) NULL,"
        sqlCmd = sqlCmd & " Status         		varchar(255) NULL,"
        sqlCmd = sqlCmd & " SubStatus      		varchar(255) NULL,"
        sqlCmd = sqlCmd & " TicketNumber   		varchar(255) NULL,"
        sqlCmd = sqlCmd & " TransactionChannel     	varchar(255) NULL,"
        sqlCmd = sqlCmd & " TransactionDate        	varchar(255) NULL,"
        sqlCmd = sqlCmd & " TransactionSubType     	varchar(255) NULL,"
        sqlCmd = sqlCmd & " TransactionType        	varchar(255) NULL,"
        sqlCmd = sqlCmd & " VoucherNumber  		varchar(255) NULL,"
        sqlCmd = sqlCmd & " VoucherQty     		varchar(255) NULL,"
        sqlCmd = sqlCmd & " VoucherType    		varchar(255) NULL,"
        sqlCmd = sqlCmd & " Organization   		varchar(255) NULL,"
        sqlCmd = sqlCmd & " Estatus        		varchar(255) NULL,"
        sqlCmd = sqlCmd & " Cajero			varchar(255) NULL,"
        sqlCmd = sqlCmd & " PRIMARY KEY ( Consecutivo ))"

        RS = Connection.Execute(sqlCmd)
    End Sub

    Sub CreartablaNiveles()
        Dim iReturn

        If TablesExist(1) Then
            Call DropTablesNiveles()
            Call CreateTablesNiveles()

        Else
            Call CreateTablesNiveles()

        End If
    End Sub


    Sub DropTablesNiveles()
        Dim RS
        RS = Connection.Execute("drop table custom.CRM_Niveles")
    End Sub

    Sub CreateTablesNiveles()
        Dim RS
        Dim sqlCmd
        sqlCmd = "CREATE TABLE  custom.CRM_Niveles ("
        sqlCmd = sqlCmd & " Consecutivo    		integer NOT NULL DEFAULT autoincrement,"
        sqlCmd = sqlCmd & " Id     			char(20) NOT NULL,"
        sqlCmd = sqlCmd & " Name2  			char(50) NOT NULL,"
        sqlCmd = sqlCmd & " Sequence       		char(5) NOT NULL,"
        sqlCmd = sqlCmd & " TierClassName  		char(50) NOT NULL,"
        sqlCmd = sqlCmd & " Description    		char(50) NOT NULL,"
        sqlCmd = sqlCmd & " LoungeProductName      	char(50) NOT NULL,"
        sqlCmd = sqlCmd & " NumberofVouchers       	char(5) NOT NULL,"
        sqlCmd = sqlCmd & " ProductName    		char(50) NOT NULL,"
        sqlCmd = sqlCmd & " RewardName     		char(50) NOT NULL,"
        sqlCmd = sqlCmd & " RewardNumber   		char(50) NOT NULL,"
        sqlCmd = sqlCmd & " RewardType     		char(50) NOT NULL,"
        sqlCmd = sqlCmd & " RewardTypeDiscount_P_M         char(50) NOT NULL)"

        RS = Connection.Execute(sqlCmd)
    End Sub

    Sub CreartablaRespuesta()
        Dim iReturn

        If TablesExist(1) Then
            Call DropTablesRespuesta()
            Call CreateTablesRespuesta()

        Else
            Call CreateTablesRespuesta()

        End If
    End Sub


    Sub DropTablesRespuesta()
        Dim RS
        RS = Connection.Execute("drop table custom.CRM_Respuesta")
    End Sub

    Sub CreateTablesRespuesta()
        Dim RS
        Dim sqlCmd
        sqlCmd = "CREATE TABLE  custom.CRM_Respuesta  ("
        sqlCmd = sqlCmd & " Consecutivo		integer 	NOT NULL DEFAULT autoincrement,"
        sqlCmd = sqlCmd & " Fecha		timestamp 	NULL,"
        sqlCmd = sqlCmd & " Tienda		integer 	NULL,"
        sqlCmd = sqlCmd & " CentroConsumo	integer		NULL,"
        sqlCmd = sqlCmd & " Cheque		integer		NULL,"
        sqlCmd = sqlCmd & " NumeroMiembro	Char(16)	NULL,"
        sqlCmd = sqlCmd & " Respuesta		varchar(10000)	NULL,"
        sqlCmd = sqlCmd & " PRIMARY KEY ( Consecutivo ) )"

        RS = Connection.Execute(sqlCmd)
    End Sub

    Sub CreartablaLoyTrans()
        Dim iReturn

        If TablesExist(1) Then
            Call DropTablesLoyTran()
            Call CreateTablesLoyTransaccion()

        Else
            Call CreateTablesLoyTransaccion()

        End If
    End Sub


    Sub DropTablesLoyTran()
        Dim RS
        RS = Connection.Execute("drop table custom.LOYTRANSACCION")
    End Sub

    Sub CreateTablesLoyTransaccion()
        Dim RS
        Dim sqlCmd
        sqlCmd = "CREATE TABLE  custom.LOYTRANSACCION   ("
        sqlCmd = sqlCmd & " MemberNumber		varchar(255) NULL,"
        sqlCmd = sqlCmd & " id				varchar(255) NULL,"
        sqlCmd = sqlCmd & " ALSELevelPrice		varchar(255) NULL,"
        sqlCmd = sqlCmd & " BranchDivision		varchar(255) NULL,"
        sqlCmd = sqlCmd & " ALSEPaymnetMode		varchar(255) NULL,"
        sqlCmd = sqlCmd & " ActivityDate		varchar(255) NULL,"
        sqlCmd = sqlCmd & " AdjustedListPrice		varchar(255) NULL,"
        sqlCmd = sqlCmd & " Amount			varchar(255) NULL,"
        sqlCmd = sqlCmd & " Comments			varchar(255) NULL,"
        sqlCmd = sqlCmd & " ItemNumber			varchar(255) NULL,"
        sqlCmd = sqlCmd & " LocationName		varchar(255) NULL,"
        sqlCmd = sqlCmd & " PartnerName			varchar(255) NULL,"
        sqlCmd = sqlCmd & " PointName			varchar(255) NULL,"
        sqlCmd = sqlCmd & " Points			varchar(255) NULL,"
        sqlCmd = sqlCmd & " ProcessDate			varchar(255) NULL,"
        sqlCmd = sqlCmd & " ProcessingComment		varchar(255) NULL,"
        sqlCmd = sqlCmd & " ProcessingLog		varchar(255) NULL,"
        sqlCmd = sqlCmd & " ProductName			varchar(255) NULL,"
        sqlCmd = sqlCmd & " Quantity			varchar(255) NULL,"
        sqlCmd = sqlCmd & " IntegrationStatus		varchar(255) NULL,"
        sqlCmd = sqlCmd & " Status			varchar(255) NULL,"
        sqlCmd = sqlCmd & " SubStatus			varchar(255) NULL,"
        sqlCmd = sqlCmd & " TicketNumber		varchar(255) NULL,"
        sqlCmd = sqlCmd & " TransactionChannel		varchar(255) NULL,"
        sqlCmd = sqlCmd & " TransactionDate		varchar(255) NULL,"
        sqlCmd = sqlCmd & " TransactionSubType		varchar(255) NULL,"
        sqlCmd = sqlCmd & " TransactionType		varchar(255) NULL,"
        sqlCmd = sqlCmd & " VoucherNumber		varchar(255) NULL,"
        sqlCmd = sqlCmd & " VoucherQty			varchar(255) NULL,"
        sqlCmd = sqlCmd & " VoucherType			varchar(255) NULL,"
        sqlCmd = sqlCmd & " Organization		varchar(255) NULL,"
        sqlCmd = sqlCmd & " Estatus			varchar(255) NULL,"
        sqlCmd = sqlCmd & " Bussiness_date         	varchar(30)  NULL,"
        sqlCmd = sqlCmd & " Centroconsumo  		integer	     NULL,"
        sqlCmd = sqlCmd & " Cheque         		varchar(20)  NULL,"
        sqlCmd = sqlCmd & " Transmitido    		varchar(1)   NULL,"
        sqlCmd = sqlCmd & " Consecutivo    		integer      NOT NULL DEFAULT autoincrement,"
        sqlCmd = sqlCmd & " NumeroLineaMicros      	integer      NULL,"
        sqlCmd = sqlCmd & " memberNumber2      		varchar(255) NULL,"
        sqlCmd = sqlCmd & " cardNumber        		varchar(255) NULL,"
        sqlCmd = sqlCmd & " cajero        		varchar(255) NULL,"
        sqlCmd = sqlCmd & " PRIMARY KEY ( Consecutivo ))"
        RS = Connection.Execute(sqlCmd)
    End Sub



    Sub CreartablaLoyTransOff()
        Dim iReturn

        If TablesExist(1) Then
            Call DropTablesLoyTranOff()
            Call CreateTablesLoyTransaccionOff()

        Else
            Call CreateTablesLoyTransaccionOff()

        End If
    End Sub


    Sub DropTablesLoyTranOff()
        Dim RS
        RS = Connection.Execute("drop table custom.LOYTRANSACCION_OFFLINE")
    End Sub

    Sub CreateTablesLoyTransaccionOff()
        Dim RS
        Dim sqlCmd
        sqlCmd = "CREATE TABLE  custom.LOYTRANSACCION_OFFLINE  ("
        sqlCmd = sqlCmd & " MemberNumber   		varchar(255) NULL,"
        sqlCmd = sqlCmd & " id     			varchar(255) NULL,"
        sqlCmd = sqlCmd & " ALSELevelPrice         	varchar(255) NULL,"
        sqlCmd = sqlCmd & " BranchDivision         	varchar(255) NULL,"
        sqlCmd = sqlCmd & " ALSEPaymnetMode        	varchar(255) NULL,"
        sqlCmd = sqlCmd & " ActivityDate   		varchar(255) NULL,"
        sqlCmd = sqlCmd & " AdjustedListPrice      	varchar(255) NULL,"
        sqlCmd = sqlCmd & " Amount         		varchar(255) NULL,"
        sqlCmd = sqlCmd & " Comments       		varchar(255) NULL,"
        sqlCmd = sqlCmd & " ItemNumber     		varchar(255) NULL,"
        sqlCmd = sqlCmd & " LocationName   		varchar(255) NULL,"
        sqlCmd = sqlCmd & " PartnerName    		varchar(255) NULL,"
        sqlCmd = sqlCmd & " PointName      		varchar(255) NULL,"
        sqlCmd = sqlCmd & " Points         		varchar(255) NULL,"
        sqlCmd = sqlCmd & " ProcessDate    		varchar(255) NULL,"
        sqlCmd = sqlCmd & " ProcessingComment      	varchar(255) NULL,"
        sqlCmd = sqlCmd & " ProcessingLog  		varchar(255) NULL,"
        sqlCmd = sqlCmd & " ProductName    		varchar(255) NULL,"
        sqlCmd = sqlCmd & " Quantity       		varchar(255) NULL,"
        sqlCmd = sqlCmd & " IntegrationStatus      	varchar(255) NULL,"
        sqlCmd = sqlCmd & " Status         		varchar(255) NULL,"
        sqlCmd = sqlCmd & " SubStatus      		varchar(255) NULL,"
        sqlCmd = sqlCmd & " TicketNumber   		varchar(255) NULL,"
        sqlCmd = sqlCmd & " TransactionChannel     	varchar(255) NULL,"
        sqlCmd = sqlCmd & " TransactionDate        	varchar(255) NULL,"
        sqlCmd = sqlCmd & " TransactionSubType     	varchar(255) NULL,"
        sqlCmd = sqlCmd & " TransactionType        	varchar(255) NULL,"
        sqlCmd = sqlCmd & " VoucherNumber  		varchar(255) NULL,"
        sqlCmd = sqlCmd & " VoucherQty     		varchar(255) NULL,"
        sqlCmd = sqlCmd & " VoucherType    		varchar(255) NULL,"
        sqlCmd = sqlCmd & " Organization   		varchar(255) NULL,"
        sqlCmd = sqlCmd & " Estatus        		varchar(255) NULL,"
        sqlCmd = sqlCmd & " Bussiness_date         	varchar(30)  NULL,"
        sqlCmd = sqlCmd & " Centroconsumo  		integer      NULL,"
        sqlCmd = sqlCmd & " Cheque         		varchar(20)  NULL,"
        sqlCmd = sqlCmd & " Transmitido    		varchar(1)   NULL,"
        sqlCmd = sqlCmd & " Consecutivo    		integer      NOT NULL DEFAULT autoincrement,"
        sqlCmd = sqlCmd & " NumeroLineaMicros      	integer      NULL,"
        sqlCmd = sqlCmd & " MemberNumber2   		varchar(255) NULL,"
        sqlCmd = sqlCmd & " cardNumber        		varchar(255) NULL,"
        sqlCmd = sqlCmd & " Cajero        		varchar(255) NULL,"
        sqlCmd = sqlCmd & " PRIMARY KEY ( Consecutivo ))"
        RS = Connection.Execute(sqlCmd)
    End Sub

    Sub CreartablaLoyTransRes()
        Dim iReturn

        If TablesExist(1) Then
            Call DropTablesLoyTranRes()
            Call CreateTablesLoyTransaccionRes()

        Else
            Call CreateTablesLoyTransaccionRes()

        End If
    End Sub


    Sub DropTablesLoyTranRes()
        Dim RS
        RS = Connection.Execute("drop table custom.LOYTRANSACCION_RESPONSE")
    End Sub

    Sub CreateTablesLoyTransaccionRes()
        Dim RS
        Dim sqlCmd
        sqlCmd = "CREATE TABLE  custom.LOYTRANSACCION_RESPONSE  ("
        sqlCmd = sqlCmd & " MemberNumber   		varchar(255) NULL,"
        sqlCmd = sqlCmd & " id     			varchar(255) NULL,"
        sqlCmd = sqlCmd & " ALSELevelPrice         	varchar(255) NULL,"
        sqlCmd = sqlCmd & " BranchDivision         	varchar(255) NULL,"
        sqlCmd = sqlCmd & " ALSEPaymnetMode        	varchar(255) NULL,"
        sqlCmd = sqlCmd & " ActivityDate   		varchar(255) NULL,"
        sqlCmd = sqlCmd & " AdjustedListPrice      	varchar(255) NULL,"
        sqlCmd = sqlCmd & " Amount         		varchar(255) NULL,"
        sqlCmd = sqlCmd & " Comments       		varchar(255) NULL,"
        sqlCmd = sqlCmd & " ItemNumber     		varchar(255) NULL,"
        sqlCmd = sqlCmd & " LocationName   		varchar(255) NULL,"
        sqlCmd = sqlCmd & " PartnerName    		varchar(255) NULL,"
        sqlCmd = sqlCmd & " PointName      		varchar(255) NULL,"
        sqlCmd = sqlCmd & " Points         		varchar(255) NULL,"
        sqlCmd = sqlCmd & " ProcessDate    		varchar(255) NULL,"
        sqlCmd = sqlCmd & " ProcessingComment      	varchar(255) NULL,"
        sqlCmd = sqlCmd & " ProcessingLog  		varchar(255) NULL,"
        sqlCmd = sqlCmd & " ProductName    		varchar(255) NULL,"
        sqlCmd = sqlCmd & " Quantity       		varchar(255) NULL,"
        sqlCmd = sqlCmd & " IntegrationStatus      	varchar(255) NULL,"
        sqlCmd = sqlCmd & " Status         		varchar(255) NULL,"
        sqlCmd = sqlCmd & " SubStatus      		varchar(255) NULL,"
        sqlCmd = sqlCmd & " TicketNumber   		varchar(255) NULL,"
        sqlCmd = sqlCmd & " TransactionChannel     	varchar(255) NULL,"
        sqlCmd = sqlCmd & " TransactionDate        	varchar(255) NULL,"
        sqlCmd = sqlCmd & " TransactionSubType     	varchar(255) NULL,"
        sqlCmd = sqlCmd & " TransactionType        	varchar(255) NULL,"
        sqlCmd = sqlCmd & " VoucherNumber  		varchar(255) NULL,"
        sqlCmd = sqlCmd & " VoucherQty     		varchar(255) NULL,"
        sqlCmd = sqlCmd & " VoucherType    		varchar(255) NULL,"
        sqlCmd = sqlCmd & " Organization   		varchar(255) NULL,"
        sqlCmd = sqlCmd & " Estatus        		varchar(255) NULL,"
        sqlCmd = sqlCmd & " Bussiness_date         	varchar(30) NULL,"
        sqlCmd = sqlCmd & " Consecutivo    		integer NOT NULL DEFAULT autoincrement,"
        sqlCmd = sqlCmd & " NumeroLineaMicros      	integer NULL,"
        sqlCmd = sqlCmd & " memberNumber2      		varchar(255) NULL,"
        sqlCmd = sqlCmd & " cardNumber        		varchar(255) NULL,"
        sqlCmd = sqlCmd & " PRIMARY KEY ( Consecutivo ))"
        RS = Connection.Execute(sqlCmd)
    End Sub

    Sub CreartablaPagaTodo()
        Dim iReturn

        If TablesExist(1) Then
            Call DropTablesPagaTodo()
            Call CreateTablesPagaTodo()

        Else
            Call CreateTablesPagaTodo()

        End If
    End Sub


    Sub DropTablesPagaTodo()
        Dim RS
        RS = Connection.Execute("drop table custom.Pagatodo_Inicio")
    End Sub

    Sub CreateTablesPagaTodo()
        Dim RS
        Dim sqlCmd
        sqlCmd = "CREATE TABLE  micros.Pagatodo_Inicio  ("
        sqlCmd = sqlCmd & " Consecutivo    		integer NOT NULL DEFAULT autoincrement,"
        sqlCmd = sqlCmd & " Fecha  			      timestamp NOT NULL,"
        sqlCmd = sqlCmd & " Tienda         		integer NOT NULL,"
        sqlCmd = sqlCmd & " CentroConsumo  		integer NOT NULL,"
        sqlCmd = sqlCmd & " Ticket         		varchar(20) NOT NULL,"
        sqlCmd = sqlCmd & " Abre_Normal    		integer NOT NULL DEFAULT 0,"
        sqlCmd = sqlCmd & " Abre_Mobil    		integer NOT NULL DEFAULT 0,"
        sqlCmd = sqlCmd & " PRIMARY KEY ( Consecutivo ) )"

        RS = Connection.Execute(sqlCmd)
    End Sub

    Sub CreartablaClientes()
        Dim iReturn

        If TablesExist(1) Then
            Call DropTablesClientes()
            Call CreateTablesClientes()

        Else
            Call CreateTablesClientes()

        End If
    End Sub


    Sub DropTablesClientes()
        Dim RS
        RS = Connection.Execute("drop table custom.crm_Clientes")
    End Sub

    Sub CreateTablesClientes()
        Dim RS
        Dim sqlCmd
        sqlCmd = "CREATE TABLE custom.crm_Clientes ("
        sqlCmd = sqlCmd & " Consecutivo		integer NOT NULL DEFAULT autoincrement, "
        sqlCmd = sqlCmd & " Id_Cheque		Char(30)    NULL,"
        sqlCmd = sqlCmd & " Cheque           	integer     NULL,"
        sqlCmd = sqlCmd & " Id_Cliente		Char(30)    NULL,"
        sqlCmd = sqlCmd & " Numero_Tarjeta	Char(30)    NULL,"
        sqlCmd = sqlCmd & " Numero_Tarjeta_O	Char(30)    NULL,"
        sqlCmd = sqlCmd & " Saldo		NUMERIC     NULL,"
        sqlCmd = sqlCmd & " Marcado_Crm		Char(1)     NULL,"
        sqlCmd = sqlCmd & " Stars		integer     NULL,"
        sqlCmd = sqlCmd & " Pausa_Recarga	Char(1)     NULL,"
        sqlCmd = sqlCmd & " Cheque_Cerrado	integer     NULL,"
        sqlCmd = sqlCmd & " Datos_1		Char(5000)  NULL,"
        sqlCmd = sqlCmd & " Datos_2		Char(5000)  NULL,"
        sqlCmd = sqlCmd & " PRIMARY KEY (Consecutivo) )"
        RS = Connection.Execute(sqlCmd)
    End Sub

    Sub CreartablaSoa()
        Dim iReturn

        If TablesExist(1) Then
            Call DropTablesSoa()
            Call CreateTablesSoa()

        Else
            Call CreateTablesSoa()

        End If
    End Sub


    Sub DropTablesSoa()
        Dim RS
        RS = Connection.Execute("drop table custom.Crm_Soa_Configuracion")
    End Sub

    Sub CreateTablesSoa()
        Dim RS
        Dim sqlCmd
        sqlCmd = "CREATE TABLE custom.Crm_Soa_Configuracion ("
        sqlCmd = sqlCmd & " seq			integer		NOT NULL DEFAULT autoincrement, "
        sqlCmd = sqlCmd & " marca		char(30)	NULL,"
        sqlCmd = sqlCmd & " url_ws		char(30)	NULL,"
        sqlCmd = sqlCmd & " puerto		integer		NULL,"
        sqlCmd = sqlCmd & " PRIMARY KEY (seq) )"
        RS = Connection.Execute(sqlCmd)
    End Sub

    Sub CreartablaMicrosWow()
        Dim iReturn

        If TablesExist(1) Then
            Call DropTablesMicrosWow()
            Call CreateTablesMicrosWow()

        Else
            Call CreateTablesMicrosWow()

        End If
    End Sub


    Sub DropTablesMicrosWow()
        Dim RS
        RS = Connection.Execute("drop table custom.configuracion_Micros_Wow")
    End Sub

    Sub CreateTablesMicrosWow()
        Dim RS
        Dim sqlCmd
        sqlCmd = "CREATE TABLE micros.configuracion_Micros_Wow ("
        sqlCmd = sqlCmd & " Consecutivo 	INTEGER		NOT NULL DEFAULT autoincrement, "
        sqlCmd = sqlCmd & " numero_Pos  	INTEGER		NOT NULL,"
        sqlCmd = sqlCmd & " accion      	char(20)	NULL,"
        sqlCmd = sqlCmd & " datos       	CHAR(100)	NULL,"
        sqlCmd = sqlCmd & " PRIMARY KEY (Consecutivo) )"
        RS = Connection.Execute(sqlCmd)
    End Sub

End Module
