Dim Connection
CALL Main

Sub Main
CALL StartConnection()
	If TablesExistTADatosCupon() Then
		CALL DropTablesTADatosCupon()
		CALL CreateTablesTADatosCupon()	
	ELSE
		CALL CreateTablesTADatosCupon()
	End If
	
	If ExistSPCuponVerFamInc() Then
		CALL DropSPCuponVerFamInc()
		CALL CreateSPCuponVerFamInc()
	ELSE 
		CALL CreateSPCuponVerFamInc()
	End If
	
	If ExistSPCuponVerFamInc2x1() Then
		CALL DropSPCuponVerFamInc2x1()
		CALL CreateSPCuponVerFamInc2x1()
	ELSE 
		CALL CreateSPCuponVerFamInc2x1()
	End If
	
	If ExistSPCuponVerSkuInc() Then
		CALL DropSPCuponVerSkuInc()
		CALL CreateSPCuponVerSkuInc()
	ELSE 
		CALL CreateSPCuponVerSkuInc()
	End If
	
	If ExistSPCuponVerSkuInc2x1() Then
		CALL DropSPCuponVerSkuInc2x1()
		CALL CreateSPCuponVerSkuInc2x1()
	ELSE 
		CALL CreateSPCuponVerSkuInc2x1()
	End If
	
	If ExistSPCuponVerFamExc() Then
		CALL DropSPCuponVerFamExc()
		CALL CreateSPCuponVerFamExc()
	ELSE 
		CALL CreateSPCuponVerFamExc()
	End If
	
	If ExistSPCuponVerSkuExc() Then
		CALL DropSPCuponVerSkuExc()
		CALL CreateSPCuponVerSkuExc()
	ELSE 
		CALL CreateSPCuponVerSkuExc()
	End If
	
    IF ExistSPCuponVerOrderType() Then
		CALL DropSPCuponVerOrderType()
		CALL CreateSPCuponVerOrderType()
	ELSE 
		CALL CreateSPCuponVerOrderType()
	End If
	
    IF ExistSPConsultaCupon() Then
		CALL DropSPConsultaCupon()
		CALL CreateSPConsultaCupon()
	ELSE 
		CALL CreateSPConsultaCupon()
	End If	
	
	If ExistSPConsultaCuponesExistentes() Then
		CALL DropSPConsultaCuponesExistentes()
		CALL CreateSPConsultaCuponesExistentes()
	ELSE 
		CALL CreateSPConsultaCuponesExistentes()
	End If	
	
	If ExistSPVerCuponExist() Then
		CALL DropSPVerCuponExist()
		CALL CreateSPVerCuponExist()
	ELSE 
		CALL CreateSPVerCuponExist()
	End If
	
    If ExistSPVerCuponEMPExist() Then
		CALL DropSPVerCuponEMPExist()
		CALL CreateSPVerCuponEMPExist()
	ELSE 
		CALL CreateSPVerCuponEMPExist()
	End If
    
	If ExistSPObtFolioCupon() Then
		CALL DropSPObtFolioCupon()
		CALL CreateSPObtFolioCupon()
	ELSE 
		CALL CreateSPObtFolioCupon()
	End If
	
	
	If TablesTATipoCuponConfgExist() Then
       CALL DropTablesTATipoCuponConfg()
       CALL CreateTablesTATipoCuponConfg()
    ELSE
       CALL CreateTablesTATipoCuponConfg()
    End If
    
	If ExistSPObtTipoCuponConfg() Then
		CALL DropSPObtTipoCuponConfg()
		CALL CreateSPObtTipoCuponConfg()
	ELSE 
		CALL CreateSPObtTipoCuponConfg()
	End If
	
	 IF ExistSPVerificaFPagoCupon() Then
		CALL DropSPVerificaFPagoCupon()
		CALL CreateSPVerificaFPagoCupon()
	ELSE 
		CALL CreateSPVerificaFPagoCupon()
	End If
	
    IF ExistSPverTarjetaCupon() Then
		CALL DropSPverTarjetaCupon()
		CALL CreateSPverTarjetaCupon()
	ELSE 
		CALL CreateSPverTarjetaCupon()
	End If	
	
    IF ExistSPupdCuponRedime() Then
		CALL DropSPupdCuponRedime()
		CALL CreateSPupdCuponRedime()
	ELSE 
		CALL CreateSPupdCuponRedime()
	End If
		
	If ExistSPObtFolioCuponLigado() Then
		CALL DropSPObtFolioCuponLigado()
		CALL CreateSPObtFolioCuponLigado()
	ELSE 
		CALL CreateSPObtFolioCuponLigado()
	End If
	
	If ExistSPConsulTipoCuponDesc() Then
		CALL DropSPConsulTipoCuponDesc()
		CALL CreateSPConsulTipoCuponDesc()
	ELSE 
		CALL CreateSPConsulTipoCuponDesc()
	End If
	
	CALL InsertTablesTipoCuponConfg
		
	CALL CloseConnection()
End Sub

Sub StartConnection()
	Set Connection = WScript.CreateObject("ADODB.Connection")
		Connection.Open "DSN=Micros; UID=dba; PWD=Password1"
End Sub

Sub CloseConnection()
	Connection.Close
End Sub

Sub CreateTablesTADatosCupon()
	Dim RS
	Dim sqlCmd
		sqlCmd = " CREATE TABLE custom.TADatosCupon( "		
        sqlCmd = sqlcmd & " FIIDTicket VARCHAR(12) NULL "
        sqlCmd = sqlcmd & ",FCIDFolioCupon VARCHAR(10)"		
        sqlCmd = sqlcmd & ",FDDOB DATETIME NULL"
        sqlCmd = sqlcmd & ",FIIDTienda INT NULL"
        sqlCmd = sqlcmd & ",FIIDMarca INT NULL"		
        sqlCmd = sqlcmd & ",FIIDPais INT NULL"
        sqlCmd = sqlcmd & ",FCUid VARCHAR(30) NULL"
        sqlCmd = sqlcmd & ",FCNombreEmpleado VARCHAR(50) NULL"
		sqlCmd = sqlcmd & ",FIIDColaborador BIGINT NULL  "	
        sqlCmd = sqlcmd & ",FCEstacionTrabajo VARCHAR(5) NULL "
        sqlCmd = sqlcmd & ",FIIDTipoPromocion INT NULL"
        sqlCmd = sqlcmd & ",FIIDCodigoEstatus INT NULL"
        sqlCmd = sqlcmd & ",FCDescripcionEstatus VARCHAR(500) NULL"
        sqlCmd = sqlcmd & ",FIIDCampana INT NULL"
        sqlCmd = sqlcmd & ",FCValorCupon VARCHAR(10) NULL"
        sqlCmd = sqlcmd & ",FCProductosLigados VARCHAR(20000) NULL"
        sqlCmd = sqlcmd & ",FCFamiliasLigadas VARCHAR(20000) NULL"
        sqlCmd = sqlcmd & ",FNMontoLigado DECIMAL(12,2) NULL"
        sqlCmd = sqlcmd & ",FIContieneCuponLigado INT NULL"
        sqlCmd = sqlcmd & ",FCTokenRedencion VARCHAR(50) NULL"
        sqlCmd = sqlcmd & ",FITipoCuponID INT NULL"
        sqlCmd = sqlcmd & ",FIGeneralParcial INT NULL"
        sqlCmd = sqlcmd & ",FITiposOrdenAplican VARCHAR(20000)"
        sqlCmd = sqlcmd & ",FIAplicaWOW INT NULL"
        sqlCmd = sqlcmd & ",FIAplicaMSR INT NULL"
        sqlCmd = sqlcmd & ",FCProductos2x1 VARCHAR(20000) NULL"
        sqlCmd = sqlcmd & ",FCFamilias2x1 VARCHAR(20000) NULL"
        sqlCmd = sqlcmd & ",FIFormaPagLigada VARCHAR(20000) NULL"
        sqlCmd = sqlcmd & ",FCBinesLigados VARCHAR(20000) NULL"
        sqlCmd = sqlcmd & ",FCProdDescartados VARCHAR(20000) NULL"
        sqlCmd = sqlcmd & ",FCFamDescartadas VARCHAR(20000) NULL"
        sqlCmd = sqlcmd & ",FISolicitaTarjeta INT NULL"
        sqlCmd = sqlcmd & ",FITarjetaUlt4Num VARCHAR(16) NULL"
        sqlCmd = sqlcmd & ",FISolicitaEmail INT NULL"
        sqlCmd = sqlcmd & ",FNMontoMax DECIMAL(12,2) NULL"
		sqlCmd = sqlcmd & ",FNFormaPagoMarca BIGINT NULL"
        sqlCmd = sqlcmd & ",FCFolioCuponLigadoID VARCHAR(10) NULL "
        sqlCmd = sqlcmd & ",FCLeyendaCupon VARCHAR(150) NULL  "
        sqlCmd = sqlcmd & ",FCCampoAdicion1 VARCHAR(1500) NULL "
        sqlCmd = sqlcmd & ",FCCampoAdicion2 VARCHAR(1500) NULL "
	    sqlCmd = sqlcmd & ",FCEmailCliente VARCHAR(150) NULL  "	
		sqlCmd = sqlcmd & ",FDFechaHoraCanje DATETIME NULL  "
        sqlCmd = sqlcmd & ",FIMontoTotalVenta DECIMAL(12,2) NULL)"
	Set RS = Connection.Execute( sqlCmd )
End Sub

Sub CreateTablesTATipoCuponConfg()
    Dim RS
    Dim sqlCmd
    sqlCmd = "CREATE TABLE custom.TATipoCuponConfg ("
	sqlCmd = sqlcmd & " FBActivo   integer NOT NULL , "
	sqlCmd = sqlcmd & " FITipoDesc bigint  NOT NULL , "
    sqlCmd = sqlcmd & " FIIDDesc   bigint  NOT NULL )"
    Set RS = Connection.Execute( sqlCmd )
End Sub


Sub InsertTablesTipoCuponConfg()
    Dim RS
    Dim sqlCmd

             sqlCmd = " INSERT INTO custom.TATipoCuponConfg VALUES (1,1,115) "
    sqlCmd = sqlCmd & " INSERT INTO custom.TATipoCuponConfg VALUES (1,2,0) "
    sqlCmd = sqlCmd & " INSERT INTO custom.TATipoCuponConfg VALUES (1,3,114) "
	sqlCmd = sqlCmd & " INSERT INTO custom.TATipoCuponConfg VALUES (1,4,82) "
	sqlCmd = sqlCmd & " INSERT INTO custom.TATipoCuponConfg VALUES (1,5,116) "
	sqlCmd = sqlCmd & " INSERT INTO custom.TATipoCuponConfg VALUES (1,6,117) "
	sqlCmd = sqlCmd & " INSERT INTO custom.TATipoCuponConfg VALUES (1,7,118) "
    
    Set RS = Connection.Execute( sqlCmd )
End Sub



Sub CreateSPupdCuponRedime()
	Dim RS
	Dim sqlCmd
		sqlCmd = " CREATE PROCEDURE custom.SPupdCuponRedime(IN paIDTicket VARCHAR(10) ,IN paNombreEmpleado VARCHAR(50),IN paEstacionTrabajo VARCHAR(5), IN paMontoTotalVenta DECIMAL(12,2), IN paIDColaborador BIGINT) "
		sqlCmd = sqlcmd & " BEGIN "
		sqlCmd = sqlcmd & " DECLARE @paFechaHoraCanje DATETIME; "
		sqlCmd = sqlcmd & " DECLARE @paNombreEmpleado VARCHAR(50); "
		sqlCmd = sqlcmd & " DECLARE @paMontoTotalVenta DECIMAL(12,2); "
		sqlCmd = sqlcmd & " DECLARE @paIDColaborador BIGINT; "
		sqlCmd = sqlcmd & " DECLARE @paEstacionTrabajo VARCHAR(5); "
		sqlCmd = sqlcmd & " DECLARE @paIDTicket VARCHAR(12); "
		sqlCmd = sqlcmd & " SET @paFechaHoraCanje = GetDate(); "
		sqlCmd = sqlcmd & " SET @paNombreEmpleado  = paNombreEmpleado; "
		sqlCmd = sqlcmd & " SET @paMontoTotalVenta  = paMontoTotalVenta; "
		sqlCmd = sqlcmd & " SET @paIDColaborador  = paIDColaborador; "
		sqlCmd = sqlcmd & " SET @paEstacionTrabajo  = paEstacionTrabajo; "
		sqlCmd = sqlcmd & " SET @paIDTicket  = paIDTicket; "
		sqlCmd = sqlcmd & " Update custom.TADatosCupon Set FDFechaHoraCanje = @paFechaHoraCanje, FCNombreEmpleado = @paNombreEmpleado,FIMontoTotalVenta = @paMontoTotalVenta, FIIDColaborador = @paIDColaborador, FCEstacionTrabajo = @paEstacionTrabajo WHERE FIIDTicket = @paIDTicket; "
		sqlCmd = sqlcmd & " END "
	Set RS = Connection.Execute( sqlCmd )
End Sub

Sub CreateSPCuponVerFamInc()
	Dim RS
	Dim sqlCmd
		sqlCmd = " CREATE PROCEDURE custom.SPCuponVerFamInc(IN paIDFolioCupon VARCHAR(10) ,IN paFamInc VARCHAR(20)) "
		sqlCmd = sqlcmd & " BEGIN "
		sqlCmd = sqlcmd & " DECLARE @paIDFolioCupon VARCHAR(10); "
		sqlCmd = sqlcmd & " DECLARE @paFamInc VARCHAR(20); "
		sqlCmd = sqlcmd & " SET @paIDFolioCupon = paIDFolioCupon; "
		sqlCmd = sqlcmd & " SET @paFamInc  = paFamInc; "
		sqlCmd = sqlcmd & " SELECT CASE WHEN COUNT(1) > 0 THEN 1 ELSE 0 END AS Resp "
		sqlCmd = sqlcmd & " FROM ( "
		sqlCmd = sqlcmd & "	SELECT ISNULL(FCFamiliasLigadas,'T') AS Resp , FCIDFolioCupon " 
		sqlCmd = sqlcmd & "	FROM custom.TADatosCupon) AS x " 
		sqlCmd = sqlcmd & "	WHERE FCIDFolioCupon = @paIDFolioCupon "
		sqlCmd = sqlcmd & "	AND (x.Resp LIKE '%,'+TRIM(@paFamInc)+',%' OR x.Resp='T'); "
		sqlCmd = sqlcmd & " END "
	Set RS = Connection.Execute( sqlCmd )
End Sub

Sub CreateSPCuponVerFamInc2x1()
	Dim RS
	Dim sqlCmd
		sqlCmd = " CREATE PROCEDURE custom.SPCuponVerFamInc2x1(IN paIDFolioCupon VARCHAR(10) ,IN paFamInc VARCHAR(20)) "
		sqlCmd = sqlcmd & " BEGIN "
		sqlCmd = sqlcmd & " DECLARE @paIDFolioCupon VARCHAR(10); "
		sqlCmd = sqlcmd & " DECLARE @paFamInc VARCHAR(20); "
		sqlCmd = sqlcmd & " SET @paIDFolioCupon = paIDFolioCupon; "
		sqlCmd = sqlcmd & " SET @paFamInc  = paFamInc; "
		sqlCmd = sqlcmd & " SELECT CASE WHEN COUNT(1) > 0 THEN 1 ELSE 0 END AS Resp "
		sqlCmd = sqlcmd & " FROM ( "
		sqlCmd = sqlcmd & "	SELECT ISNULL(FCFamilias2x1,'T') AS Resp , FCIDFolioCupon " 
		sqlCmd = sqlcmd & "	FROM custom.TADatosCupon) AS x " 
		sqlCmd = sqlcmd & "	WHERE FCIDFolioCupon = @paIDFolioCupon "
		sqlCmd = sqlcmd & "	AND (x.Resp LIKE '%,'+TRIM(@paFamInc)+',%' OR x.Resp='T'); "
		sqlCmd = sqlcmd & " END "
	Set RS = Connection.Execute( sqlCmd )
End Sub

Sub CreateSPCuponVerSkuInc()
	Dim RS
	Dim sqlCmd                                            
		sqlCmd = " CREATE PROCEDURE custom.SPCuponVerSkuInc(IN paIDFolioCupon VARCHAR(10) ,IN paSkuInc VARCHAR(20)) "
		sqlCmd = sqlcmd & " BEGIN "
		sqlCmd = sqlcmd & " DECLARE @paIDFolioCupon VARCHAR(10); "
		sqlCmd = sqlcmd & " DECLARE @paSkuInc VARCHAR(20); "
		sqlCmd = sqlcmd & " SET @paIDFolioCupon = paIDFolioCupon; "
		sqlCmd = sqlcmd & " SET @paSkuInc  = paSkuInc; "
		sqlCmd = sqlcmd & " SELECT CASE WHEN COUNT(1) > 0 THEN 1 ELSE 0 END AS Resp "
		sqlCmd = sqlcmd & " FROM ( "
		sqlCmd = sqlcmd & "	SELECT ISNULL(FCProductosLigados,'T') AS Resp , FCIDFolioCupon " 
		sqlCmd = sqlcmd & "	FROM custom.TADatosCupon) AS x " 
		sqlCmd = sqlcmd & "	WHERE FCIDFolioCupon = @paIDFolioCupon "
		sqlCmd = sqlcmd & "	AND (x.Resp LIKE '%,'+TRIM(@paSkuInc)+',%' OR x.Resp='T'); "
		sqlCmd = sqlcmd & " END "
	Set RS = Connection.Execute( sqlCmd )
End Sub

Sub CreateSPCuponVerSkuInc2x1()
	Dim RS
	Dim sqlCmd                                            
		sqlCmd = " CREATE PROCEDURE custom.SPCuponVerSkuInc2x1(IN paIDFolioCupon VARCHAR(10) ,IN paSkuInc VARCHAR(20)) "
		sqlCmd = sqlcmd & " BEGIN "
		sqlCmd = sqlcmd & " DECLARE @paIDFolioCupon VARCHAR(10); "
		sqlCmd = sqlcmd & " DECLARE @paSkuInc VARCHAR(20); "
		sqlCmd = sqlcmd & " SET @paIDFolioCupon = paIDFolioCupon; "
		sqlCmd = sqlcmd & " SET @paSkuInc  = paSkuInc; "
		sqlCmd = sqlcmd & " SELECT CASE WHEN COUNT(1) > 0 THEN 1 ELSE 0 END AS Resp "
		sqlCmd = sqlcmd & " FROM ( "
		sqlCmd = sqlcmd & "	SELECT ISNULL(FCProductos2x1,'T') AS Resp , FCIDFolioCupon " 
		sqlCmd = sqlcmd & "	FROM custom.TADatosCupon) AS x " 
		sqlCmd = sqlcmd & "	WHERE FCIDFolioCupon = @paIDFolioCupon "
		sqlCmd = sqlcmd & "	AND (x.Resp LIKE '%,'+TRIM(@paSkuInc)+',%' OR x.Resp='T'); "
		sqlCmd = sqlcmd & " END "
	Set RS = Connection.Execute( sqlCmd )
End Sub

Sub CreateSPCuponVerFamExc()
	Dim RS
	Dim sqlCmd                                            
		sqlCmd = " CREATE PROCEDURE custom.SPCuponVerFamExc(IN paIDFolioCupon VARCHAR(10) ,IN paFamExc VARCHAR(20)) "
		sqlCmd = sqlcmd & " BEGIN "
		sqlCmd = sqlcmd & " DECLARE @paIDFolioCupon VARCHAR(10); "
		sqlCmd = sqlcmd & " DECLARE @paFamExc VARCHAR(20); "
		sqlCmd = sqlcmd & " SET @paIDFolioCupon = paIDFolioCupon; "
		sqlCmd = sqlcmd & " SET @paFamExc  = paFamExc; "
		sqlCmd = sqlcmd & " SELECT CASE WHEN COUNT(1) > 0 THEN 1 ELSE 0 END AS Resp "
		sqlCmd = sqlcmd & " FROM ( "
		sqlCmd = sqlcmd & "	SELECT ISNULL(FCFamDescartadas,'T') AS Resp , FCIDFolioCupon " 
		sqlCmd = sqlcmd & "	FROM custom.TADatosCupon) AS x " 
		sqlCmd = sqlcmd & "	WHERE FCIDFolioCupon = @paIDFolioCupon "
		sqlCmd = sqlcmd & "	AND (x.Resp LIKE '%,'+TRIM(@paFamExc)+',%' OR x.Resp='T'); "
		sqlCmd = sqlcmd & " END "
	Set RS = Connection.Execute( sqlCmd )
End Sub

Sub CreateSPCuponVerSkuExc()
	Dim RS
	Dim sqlCmd                                            
		sqlCmd = " CREATE PROCEDURE custom.SPCuponVerSkuExc(IN paIDFolioCupon VARCHAR(10) ,IN paSkuExc VARCHAR(20)) "
		sqlCmd = sqlcmd & " BEGIN "
		sqlCmd = sqlcmd & " DECLARE @paIDFolioCupon VARCHAR(10); "
		sqlCmd = sqlcmd & " DECLARE @paSkuExc VARCHAR(20); "
		sqlCmd = sqlcmd & " SET @paIDFolioCupon = paIDFolioCupon; "
		sqlCmd = sqlcmd & " SET @paSkuExc  = paSkuExc; "
		sqlCmd = sqlcmd & " SELECT CASE WHEN COUNT(1) > 0 THEN 1 ELSE 0 END AS Resp "
		sqlCmd = sqlcmd & " FROM ( "
		sqlCmd = sqlcmd & "	SELECT ISNULL(FCProdDescartados,'T') AS Resp , FCIDFolioCupon " 
		sqlCmd = sqlcmd & "	FROM custom.TADatosCupon) AS x " 
		sqlCmd = sqlcmd & "	WHERE FCIDFolioCupon = @paIDFolioCupon "
		sqlCmd = sqlcmd & "	AND (x.Resp LIKE '%,'+TRIM(@paSkuExc)+',%' OR x.Resp='T'); "
		sqlCmd = sqlcmd & " END "
	Set RS = Connection.Execute( sqlCmd )
End Sub

Sub CreateSPCuponVerOrderType()
	Dim RS
	Dim sqlCmd                                            
		sqlCmd = " CREATE PROCEDURE custom.SPCuponVerOrderType(IN paIDFolioCupon VARCHAR(10) ,IN paOdrTyp VARCHAR(20)) "
		sqlCmd = sqlcmd & " BEGIN "
		sqlCmd = sqlcmd & " DECLARE @paIDFolioCupon VARCHAR(10); "
		sqlCmd = sqlcmd & " DECLARE @paOdrTyp VARCHAR(20); "
		sqlCmd = sqlcmd & " SET @paIDFolioCupon = paIDFolioCupon; "
		sqlCmd = sqlcmd & " SET @paOdrTyp  = paOdrTyp; "
		sqlCmd = sqlcmd & " SELECT CASE WHEN COUNT(1) > 0 THEN 1 ELSE 0 END AS Resp "
		sqlCmd = sqlcmd & " FROM ( "
		sqlCmd = sqlcmd & "	SELECT CASE WHEN ISNULL((CASE WHEN FITiposOrdenAplican = '' THEN 'T' ELSE FITiposOrdenAplican END),'T') = 'T' THEN 'T' ELSE FITiposOrdenAplican END AS Resp , FCIDFolioCupon " 
		sqlCmd = sqlcmd & "	FROM custom.TADatosCupon) AS x " 
		sqlCmd = sqlcmd & "	WHERE FCIDFolioCupon = @paIDFolioCupon "
		sqlCmd = sqlcmd & "	AND (x.Resp LIKE '%,'+TRIM(@paOdrTyp)+',%' OR x.Resp='T' OR x.Resp=',0,'); "
		sqlCmd = sqlcmd & " END "
	Set RS = Connection.Execute( sqlCmd )
End Sub

Sub CreateSPverTarjetaCupon()
	Dim RS
	Dim sqlCmd                                            
		sqlCmd = " CREATE PROCEDURE custom.SPverTarjetaCupon(IN paIDFolioCupon VARCHAR(10),IN paIDTicket VARCHAR(12) ,IN paBin VARCHAR(4)) "
		sqlCmd = sqlcmd & " BEGIN "
		sqlCmd = sqlcmd & " DECLARE @paIDFolioCupon VARCHAR(10); "
		sqlCmd = sqlcmd & " DECLARE @paIDTicket VARCHAR(10); "
		sqlCmd = sqlcmd & " DECLARE @paBin VARCHAR(20); "
		sqlCmd = sqlcmd & " SET @paIDFolioCupon = paIDFolioCupon; "
		sqlCmd = sqlcmd & " SET @paIDTicket = paIDTicket; "
		sqlCmd = sqlcmd & " SET @paBin  = paBin; "
		sqlCmd = sqlcmd & " SELECT CASE WHEN COUNT(1) > 0 THEN 1 ELSE 0 END AS Resp "
		sqlCmd = sqlcmd & " FROM ( "
		sqlCmd = sqlcmd & "	SELECT ISNULL((CASE WHEN FITarjetaUlt4Num = '' THEN '911' ELSE FITarjetaUlt4Num END),'911') AS Resp , FCIDFolioCupon, FIIDTicket,FISolicitaTarjeta" 
		sqlCmd = sqlcmd & "	FROM custom.TADatosCupon) AS x " 
		sqlCmd = sqlcmd & "	WHERE FCIDFolioCupon = @paIDFolioCupon AND FIIDTicket = @paIDTicket AND ISNULL(FISolicitaTarjeta,0) = 1"
		sqlCmd = sqlcmd & "	AND (x.Resp = @paBin OR x.Resp=911); "
		sqlCmd = sqlcmd & " END "
	Set RS = Connection.Execute( sqlCmd )
End Sub

Sub CreateSPConsultaCupon()
	Dim RS
	Dim sqlCmd                                            
		sqlCmd = " CREATE PROCEDURE custom.SPConsultaCupon(IN paIDFolioCupon VARCHAR(10) ,IN paIDTicket VARCHAR(12),IN paEstacionTrabajo VARCHAR(5)) "
		sqlCmd = sqlcmd & " BEGIN "
		sqlCmd = sqlcmd & " DECLARE @paIDFolioCupon VARCHAR(10); "
		sqlCmd = sqlcmd & " DECLARE @paIDTicket VARCHAR(12); "
		sqlCmd = sqlcmd & " DECLARE @paEstacionTrabajo VARCHAR(5); "
		sqlCmd = sqlcmd & " SET @paIDFolioCupon = paIDFolioCupon; "
		sqlCmd = sqlcmd & " SET @paIDTicket  = paIDTicket; "
	    sqlCmd = sqlcmd & " SET @paEstacionTrabajo  = paEstacionTrabajo; "
		sqlCmd = sqlcmd & " SELECT  FIIDTicket, FCEstacionTrabajo ,FCIDFolioCupon ,FIIDTienda "
		sqlCmd = sqlcmd & " ,FIIDMarca, FIIDCodigoEstatus, FCDescripcionEstatus, FIIDCampana "
		sqlCmd = sqlcmd & " ,FIIDTipoPromocion, FCValorCupon, FNMontoLigado, FIContieneCuponLigado " 
		sqlCmd = sqlcmd & " ,FITipoCuponID, FIGeneralParcial, 1 as FIAplicaWOW, 1 as FIAplicaMSR,  Case WHEN ISNULL(Case WHEN FIFormaPagLigada='' THEN 'F' ELSE 'T' END,'F')='F' THEN 'F' ELSE 'T' END AS FIFormaPagLigada   "
		sqlCmd = sqlcmd & " ,FISolicitaTarjeta, FITarjetaUlt4Num, FISolicitaEmail, FNMontoMax "
		sqlCmd = sqlcmd & " ,FCNombreEmpleado, FCFolioCuponLigadoID, FCLeyendaCupon, FIIDColaborador"
		sqlCmd = sqlcmd & " ,FIMontoTotalVenta, FCEmailCliente,FNFormaPagoMarca FROM custom.TADatosCupon "
		sqlCmd = sqlcmd & "  WHERE FCIDFolioCupon = @paIDFolioCupon AND FIIDTicket = @paIDTicket "
		sqlCmd = sqlcmd & "  AND FCEstacionTrabajo = @paEstacionTrabajo AND (FIIDCodigoEstatus = 0 OR FIIDCodigoEstatus = 1 OR FIIDCodigoEstatus = 3); "
		sqlCmd = sqlcmd & " END "
	Set RS = Connection.Execute( sqlCmd )
End Sub

Sub CreateSPConsultaCuponesExistentes()
	Dim RS
	Dim sqlCmd                                            
		sqlCmd = " CREATE PROCEDURE custom.SPConsultaCuponesExistentes(IN paIDTicket VARCHAR(12)) "
		sqlCmd = sqlcmd & " BEGIN "
		sqlCmd = sqlcmd & " DECLARE @paIDTicket VARCHAR(12); "
		sqlCmd = sqlcmd & " SET @paIDTicket  = paIDTicket; "
		sqlCmd = sqlcmd & " SELECT FCIDFolioCupon FROM custom.TADatosCupon "
		sqlCmd = sqlcmd & "  WHERE FIIDTicket = @paIDTicket "
		sqlCmd = sqlcmd & "  AND (FIIDCodigoEstatus = 0 OR FIIDCodigoEstatus = 1); "
		sqlCmd = sqlcmd & " END "
	Set RS = Connection.Execute( sqlCmd )
End Sub

Sub CreateSPVerCuponExist()
	Dim RS
	Dim sqlCmd                                            
		sqlCmd = " CREATE PROCEDURE custom.SPVerCuponExist(IN paIDFolioCupon VARCHAR(10) ,IN paEstacionTrabajo VARCHAR(5)) "
		sqlCmd = sqlcmd & " BEGIN "
		sqlCmd = sqlcmd & " DECLARE @paIDFolioCupon VARCHAR(10); "
		sqlCmd = sqlcmd & " DECLARE @paEstacionTrabajo VARCHAR(5); "
		sqlCmd = sqlcmd & " SET @paIDFolioCupon = paIDFolioCupon; "
	    sqlCmd = sqlcmd & " SET @paEstacionTrabajo  = paEstacionTrabajo; "
		sqlCmd = sqlcmd & " SELECT FITipoCuponID FROM custom.TADatosCupon "
		sqlCmd = sqlcmd & "  WHERE FCIDFolioCupon = @paIDFolioCupon AND FCEstacionTrabajo != @paEstacionTrabajo AND (FIIDCodigoEstatus = 0 OR FIIDCodigoEstatus = 1 OR FIIDCodigoEstatus = 3); "
		sqlCmd = sqlcmd & " END "
	Set RS = Connection.Execute( sqlCmd )
End Sub

Sub CreateSPVerCuponEMPExist()
	Dim RS
	Dim sqlCmd                                            
		sqlCmd = " CREATE PROCEDURE custom.SPVerCuponEMPExist(IN paIDFolioCupon VARCHAR(10) ,IN paEstacionTrabajo VARCHAR(5)) "
		sqlCmd = sqlcmd & " BEGIN "
		sqlCmd = sqlcmd & " DECLARE @paIDFolioCupon VARCHAR(10); "
		sqlCmd = sqlcmd & " DECLARE @paEstacionTrabajo VARCHAR(5); "
		sqlCmd = sqlcmd & " SET @paIDFolioCupon = paIDFolioCupon; "
	    sqlCmd = sqlcmd & " SET @paEstacionTrabajo  = paEstacionTrabajo; "
		sqlCmd = sqlcmd & " SELECT FITipoCuponID FROM custom.TADatosCupon "
		sqlCmd = sqlcmd & "  WHERE FCIDFolioCupon = @paIDFolioCupon AND FIIDTipoPromocion = 6 AND FCEstacionTrabajo != @paEstacionTrabajo AND (FIIDCodigoEstatus = 0 OR FIIDCodigoEstatus = 1); "
		sqlCmd = sqlcmd & " END "
	Set RS = Connection.Execute( sqlCmd )
End Sub

Sub CreateSPConsulTipoCuponDesc()
	Dim RS
	Dim sqlCmd                                            
		sqlCmd = " CREATE PROCEDURE custom.SPConsulTipoCuponDesc(IN paObjDTLCupon VARCHAR(10)) "
		sqlCmd = sqlcmd & " BEGIN "
		sqlCmd = sqlcmd & " DECLARE @paObjDTLCupon VARCHAR(10); "
		sqlCmd = sqlcmd & " SET @paObjDTLCupon = paObjDTLCupon; "
		sqlCmd = sqlcmd & " SELECT FITipoDesc from custom.TATipoCuponConfg where FBActivo = 1 AND FIIDDesc = paObjDTLCupon"
		sqlCmd = sqlcmd & " END "
	Set RS = Connection.Execute( sqlCmd )
End Sub

Sub CreateSPVerificaFPagoCupon()
	Dim RS
	Dim sqlCmd
		sqlCmd = " CREATE PROCEDURE custom.SPVerificaFPagoCupon(IN paIDFolioCupon VARCHAR(10), IN paIDTicket VARCHAR(12), IN paFPago VARCHAR(20)) "
		sqlCmd = sqlcmd & " BEGIN "
		sqlCmd = sqlcmd & " DECLARE @paIDFolioCupon VARCHAR(10); "
		sqlCmd = sqlcmd & " DECLARE @paIDTicket VARCHAR(12); "
		sqlCmd = sqlcmd & " DECLARE @paFPago VARCHAR(20); "
		sqlCmd = sqlcmd & " SET @paIDFolioCupon = paIDFolioCupon; "
		sqlCmd = sqlcmd & " SET @paIDTicket = paIDTicket; "
		sqlCmd = sqlcmd & " SET @paFPago = paFPago; "
		sqlCmd = sqlcmd & " SELECT CASE WHEN (SELECT COUNT(1) AS Resp "
        sqlCmd = sqlcmd & " FROM custom.TADatosCupon " 
		sqlCmd = sqlcmd & " WHERE FCIDFolioCupon = @paIDFolioCupon AND FIIDTicket = paIDTicket) = 0 " 
        sqlCmd = sqlcmd & " THEN 1 "  
        sqlCmd = sqlcmd & " ELSE "
        sqlCmd = sqlcmd & " (SELECT COUNT(1) AS Resp "
		sqlCmd = sqlcmd & " FROM ( "
		sqlCmd = sqlcmd & " SELECT ISNULL(FIFormaPagLigada,'T') AS Resp , FCIDFolioCupon  "
		sqlCmd = sqlcmd & " FROM custom.TADatosCupon) AS x " 
		sqlCmd = sqlcmd & " WHERE FCIDFolioCupon = @paIDFolioCupon AND FIIDTicket = paIDTicket " 
		sqlCmd = sqlcmd & " AND (x.Resp LIKE '%,'+TRIM(@paFPago)+',%' OR x.Resp='T')) "  
       	sqlCmd = sqlcmd & " END AS Resp; "
		sqlCmd = sqlcmd & " END "
	Set RS = Connection.Execute( sqlCmd )
End Sub

Sub CreateSPObtTipoCuponConfg()
	Dim RS
	Dim sqlCmd                                            
		sqlCmd = " CREATE PROCEDURE custom.SPObtTipoCuponConfg(IN paFITipoDesc Int) "
		sqlCmd = sqlcmd & " BEGIN "
		sqlCmd = sqlcmd & " DECLARE @paFITipoDesc Int; "
		sqlCmd = sqlcmd & " SET @paFITipoDesc = paFITipoDesc; "
		sqlCmd = sqlcmd & " SELECT FIIDDesc from custom.TATipoCuponConfg where FBActivo = 1 AND FITipoDesc = @paFITipoDesc;"
		sqlCmd = sqlcmd & " END "
	Set RS = Connection.Execute( sqlCmd )
End Sub

Sub CreateSPObtFolioCupon()
	Dim RS
	Dim sqlCmd                                            
		sqlCmd = " CREATE PROCEDURE custom.SPObtFolioCupon(IN paFIIDTicket VARCHAR(10)) "
		sqlCmd = sqlcmd & " BEGIN "
		sqlCmd = sqlcmd & " DECLARE @paFIIDTicket VARCHAR(10); "
		sqlCmd = sqlcmd & " SET @paFIIDTicket = paFIIDTicket; "
		sqlCmd = sqlcmd & " SELECT FCIDFolioCupon,(CASE WHEN FIIDTipoPromocion= 4 THEN 9999 ELSE FIIDTipoPromocion END)AS B FROM custom.TADatosCupon WHERE FIIDTicket = @paFIIDTicket AND (FIIDCodigoEstatus = 1 OR FIIDCodigoEstatus = 3) order by 2 DESC; "
		sqlCmd = sqlcmd & " END "
	Set RS = Connection.Execute( sqlCmd )
End Sub

Sub CreateSPObtFolioCuponLigado()
	Dim RS
	Dim sqlCmd                                            
		sqlCmd = " CREATE PROCEDURE custom.SPObtFolioCuponLigado(IN paIDFolioCupon VARCHAR(10), IN paIDTicket VARCHAR(12)) "
		sqlCmd = sqlcmd & " BEGIN "
		sqlCmd = sqlcmd & " DECLARE @paIDTicket VARCHAR(10); "		
		sqlCmd = sqlcmd & " DECLARE @paIDFolioCupon VARCHAR(12); "
		sqlCmd = sqlcmd & " SET @paIDFolioCupon = paIDFolioCupon; "
		sqlCmd = sqlcmd & " SET @paIDTicket = paIDTicket; "
		sqlCmd = sqlcmd & " SELECT FCLeyendaCupon, FCFolioCuponLigadoID FROM custom.TADatosCupon WHERE FIContieneCuponLigado = 1 AND FCIDFolioCupon = @paIDFolioCupon AND FIIDTicket = @paIDTicket AND (FIIDCodigoEstatus = 1 OR FIIDCodigoEstatus = 3); "
		sqlCmd = sqlcmd & " END "
	Set RS = Connection.Execute( sqlCmd )
End Sub
REM Drop
Function DropTablesTADatosCupon()
	Dim RS
	Set RS = Connection.Execute("DROP TABLE custom.TADatosCupon")
End Function

Function DropSPObtFolioCuponLigado()
	Dim RS
	Set RS = Connection.Execute("DROP PROCEDURE custom.SPObtFolioCuponLigado")
End Function

Function DropSPupdCuponRedime()
	Dim RS
	Set RS = Connection.Execute("DROP PROCEDURE custom.SPupdCuponRedime")
End Function

Function DropSPverTarjetaCupon()
	Dim RS
	Set RS = Connection.Execute("DROP PROCEDURE custom.SPverTarjetaCupon")
End Function

Function DropSPObtFolioCupon()
	Dim RS
	Set RS = Connection.Execute("DROP PROCEDURE custom.SPObtFolioCupon")
End Function

Function DropSPObtTipoCuponConfg()
	Dim RS
	Set RS = Connection.Execute(" DROP PROCEDURE custom.SPObtTipoCuponConfg")
End Function

Function DropSPCuponVerFamInc()
Dim RS
	Set RS = Connection.Execute(" DROP PROCEDURE custom.SPCuponVerFamInc ")
End Function

Function DropSPCuponVerFamInc2x1()
Dim RS
	Set RS = Connection.Execute(" DROP PROCEDURE custom.SPCuponVerFamInc2x1 ")
End Function

Function DropSPCuponVerSkuInc()
Dim RS
	Set RS = Connection.Execute(" DROP PROCEDURE custom.SPCuponVerSkuInc ")
End Function

Function DropSPCuponVerSkuInc2x1()
Dim RS
	Set RS = Connection.Execute(" DROP PROCEDURE custom.SPCuponVerSkuInc2x1 ")
End Function

Function DropSPCuponVerFamExc()
Dim RS
	Set RS = Connection.Execute(" DROP PROCEDURE custom.SPCuponVerFamExc ")
End Function

Function DropSPCuponVerSkuExc()
Dim RS
	Set RS = Connection.Execute(" DROP PROCEDURE custom.SPCuponVerSkuExc ")
End Function

Function DropSPCuponVerOrderType()
Dim RS
	Set RS = Connection.Execute(" DROP PROCEDURE custom.SPCuponVerOrderType ")
End Function

Function DropSPConsultaCupon()
Dim RS
	Set RS = Connection.Execute(" DROP PROCEDURE custom.SPConsultaCupon ")
End Function

Function DropSPVerificaFPagoCupon()
Dim RS
	Set RS = Connection.Execute(" DROP PROCEDURE custom.SPVerificaFPagoCupon ")
End Function

Function DropSPConsulTipoCuponDesc()
Dim RS
	Set RS = Connection.Execute(" DROP PROCEDURE custom.SPConsulTipoCuponDesc ")
End Function

Function DropSPVerCuponExist()
Dim RS
	Set RS = Connection.Execute(" DROP PROCEDURE custom.SPVerCuponExist ")
End Function

Function DropSPVerCuponEMPExist()
Dim RS
	Set RS = Connection.Execute(" DROP PROCEDURE custom.SPVerCuponEMPExist ")
End Function

Function DropTablesTATipoCuponConfg()
    Dim RS
    Set RS = Connection.Execute("DROP TABLE custom.TATipoCuponConfg")
End Function

Function DropSPConsultaCuponesExistentes()
    Dim RS
    Set RS = Connection.Execute("DROP PROCEDURE custom.SPConsultaCuponesExistentes")
End Function

REM Codigo para buscar en Sybase
Function TablesExistTADatosCupon()
	Dim RS
	Dim StrResponse
	Set RS = Connection.Execute("SELECT * from sysobjects where name = 'TADatosCupon'")
		TablesExistTADatosCupon = Not RS.Eof
End Function

Function ExistSPverTarjetaCupon() 
	Dim RS
	Dim StrResponse
	Set RS = Connection.Execute("SELECT * from sysobjects where name = 'SPverTarjetaCupon'")
		ExistSPverTarjetaCupon = Not RS.Eof
End Function

Function ExistSPObtFolioCuponLigado() 
	Dim RS
	Dim StrResponse
	Set RS = Connection.Execute("SELECT * from sysobjects where name = 'SPObtFolioCuponLigado'")
		ExistSPObtFolioCuponLigado= Not RS.Eof
End Function

Function ExistSPupdCuponRedime() 
	Dim RS
	Dim StrResponse
	Set RS = Connection.Execute("SELECT * from sysobjects where name = 'SPupdCuponRedime'")
		ExistSPupdCuponRedime= Not RS.Eof
End Function

Function ExistSPObtFolioCupon() 
	Dim RS
	Dim StrResponse
	Set RS = Connection.Execute("SELECT * from sysobjects where name = 'SPObtFolioCupon'")
		ExistSPObtFolioCupon = Not RS.Eof
End Function

Function ExistSPObtTipoCuponConfg() 
	Dim RS
	Dim StrResponse
	Set RS = Connection.Execute("SELECT * from sysobjects where name = 'SPObtTipoCuponConfg'")
		ExistSPObtTipoCuponConfg = Not RS.Eof
End Function

Function ExistSPCuponVerFamInc() 
	Dim RS
	Dim StrResponse
	Set RS = Connection.Execute("SELECT * from sysobjects where name = 'SPCuponVerFamInc'")
		ExistSPCuponVerFamInc = Not RS.Eof
End Function

Function ExistSPCuponVerFamInc2x1() 
	Dim RS
	Dim StrResponse
	Set RS = Connection.Execute("SELECT * from sysobjects where name = 'SPCuponVerFamInc2x1'")
		ExistSPCuponVerFamInc2x1 = Not RS.Eof
End Function

Function ExistSPCuponVerSkuInc() 
	Dim RS
	Dim StrResponse
	Set RS = Connection.Execute("SELECT * from sysobjects where name = 'SPCuponVerSkuInc'")
		ExistSPCuponVerSkuInc = Not RS.Eof
End Function

Function ExistSPCuponVerSkuInc2x1() 
	Dim RS
	Dim StrResponse
	Set RS = Connection.Execute("SELECT * from sysobjects where name = 'SPCuponVerSkuInc2x1'")
		ExistSPCuponVerSkuInc2x1 = Not RS.Eof
End Function

Function ExistSPCuponVerFamExc() 
	Dim RS
	Dim StrResponse
	Set RS = Connection.Execute("SELECT * from sysobjects where name = 'SPCuponVerFamExc'")
		ExistSPCuponVerFamExc = Not RS.Eof
End Function

Function ExistSPCuponVerSkuExc() 
	Dim RS
	Dim StrResponse
	Set RS = Connection.Execute("SELECT * from sysobjects where name = 'SPCuponVerSkuExc'")
		ExistSPCuponVerSkuExc = Not RS.Eof
End Function

Function ExistSPCuponVerOrderType() 
	Dim RS
	Dim StrResponse
	Set RS = Connection.Execute("SELECT * from sysobjects where name = 'SPCuponVerOrderType'")
		ExistSPCuponVerOrderType = Not RS.Eof
End Function

Function ExistSPConsultaCupon() 
	Dim RS
	Dim StrResponse
	Set RS = Connection.Execute("SELECT * from sysobjects where name = 'SPConsultaCupon'")
		ExistSPConsultaCupon = Not RS.Eof
End Function

Function ExistSPVerCuponExist() 
	Dim RS
	Dim StrResponse
	Set RS = Connection.Execute("SELECT * from sysobjects where name = 'SPVerCuponExist'")
		ExistSPVerCuponExist = Not RS.Eof
End Function

Function ExistSPVerCuponEMPExist() 
	Dim RS
	Dim StrResponse
	Set RS = Connection.Execute("SELECT * from sysobjects where name = 'SPVerCuponEMPExist'")
		ExistSPVerCuponEMPExist = Not RS.Eof
End Function

Function ExistSPVerificaFPagoCupon()
    Dim RS
    Dim strResponse
    Set RS = Connection.Execute("SELECT * from sysobjects where name = 'SPVerificaFPagoCupon'")
    ExistSPVerificaFPagoCupon = Not RS.Eof	
End Function

Function ExistSPConsulTipoCuponDesc()
    Dim RS
    Dim strResponse
    Set RS = Connection.Execute("SELECT * from sysobjects where name = 'SPConsulTipoCuponDesc'")
    ExistSPConsulTipoCuponDesc = Not RS.Eof	
End Function

Function ExistSPConsultaCuponesExistentes()
    Dim RS
    Dim strResponse
    Set RS = Connection.Execute("SELECT * from sysobjects where name = 'SPConsultaCuponesExistentes'")
    ExistSPConsultaCuponesExistentes = Not RS.Eof	
End Function


Function TablesTATipoCuponConfgExist()
    Dim RS
    Dim strResponse
    Set RS = Connection.Execute("SELECT * from sysobjects where name = 'TATipoCuponConfg'")
    TablesTATipoCuponConfgExist = Not RS.Eof	
End Function

