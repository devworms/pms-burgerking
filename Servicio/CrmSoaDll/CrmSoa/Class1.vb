﻿Imports System.IO
Imports System.Net
Imports System.Xml
Imports System.Text

Imports System.Data.Odbc


Public Class CrmSoaDll
    Dim filewrite As StreamWriter

    Dim name_file As String = "C:\CrmSoa\Archivo"
    Const name_file_xml_log As String = "C:\CrmSoa\Xml_Log_Crm_Soa.txt"
    Const name_file_log As String = "C:\CrmSoa\Log_Crm_Soa.txt"
    Public Const FileConfiguracion_Url As String = "C:\urlCrmSoa.TxT"

    Dim campo, name_file2, Numero_tarjetas, Error_Field, Respuesta, directorio_file, _xml, soapResult As String
    Dim error_ws, respuesta_ws, Registro_Log As String
    Dim contador_file, contador, Contador_I As Integer

    Dim Permite_Conexcion_CRM_Micros_Dos, Salir As Boolean
    Dim ConexcionMCRSPOS_Dos As New OdbcConnection
    Dim ReadSql_odbc_Dos As OdbcDataReader

    Dim T_MemberNumber, T_id, T_ALSELevelPrice, T_BranchDivision, T_ALSEPaymnetMode, T_ActivityDate, T_AdjustedListPrice, T_Amount, T_Comments, T_ItemNumber, T_LocationName, T_PartnerName, T_PointName, T_Points, T_ProcessDate, T_ProcessingComment, T_ProcessingLog, T_ProductName, T_Quantity, T_IntegrationStatus, T_Status, T_SubStatus, T_TicketNumber, T_TransactionChannel, T_TransactionDate, T_TransactionSubType, T_TransactionType, T_VoucherNumber, T_VoucherQty, T_VoucherType, T_Organization, T_Estatus, T_Bussiness_date, T_centroconsumo, T_Cheque, T_Transmitido, T_NumeroLineaMicros, T_Waiter As String
    Dim Numero_ticket, errorDescription, Numero_tienda, Id, earnedPoints, detail, numeroCliente, validDiscountAmount, validDiscountType As String


    'GetMember
    Public Function main_crmsoa_getMember(ByVal numeroMiembro As String, ByVal sUri2 As String, ByVal campo22 As String, ByVal campo33 As String, ByVal campo44 As String, ByVal campo55 As String, ByVal campo66 As String, ByVal campo77 As String, ByVal campo99 As String, ByVal campo100 As String, ByVal timeOut_Ws As String) As String
        Try
            ''
            '26Dic2014 Dim sUri2 As String = "https://soaprod.grupoalsea.com.mx/Services/Loyalty/ALSEAMemberService"

            '26Dic2014 Const campo22 As String = "http://schemas.xmlsoap.org/soap/envelope/"
            '26Dic2014 Const campo33 As String = "http://services.alsea.com/Loyalty/Schema/ALSEAMemberServiceMsg"

            '26Dic2014 Const campo55 As String = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"
            '26Dic2014 Const campo66 As String = "soap"

            '26Dic2014 Const campo77 As String = "UsernameToken-XAqqET7bPF88fgxQD4ytyw22"

            '26Dic2014 Const campo99 As String = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"

            '26Dic2014 Const campo100 As String = "<wsse:Password Type=" & Chr(34) & "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText" & Chr(34) & ">L0y417y.</wsse:Password>"

            '26Dic2014 Const timeOut_Ws As Integer = 30000

            Dim myUri2 As System.Uri = New System.Uri(sUri2)
            Dim HttpWRequest As HttpWebRequest = WebRequest.Create(myUri2)

            ''13Ene2015 Dim request2 As HttpWebRequest = CreateWebRequest_GetMember_2(timeOut_Ws, sUri2, HttpWRequest)
            Dim soapEnvelopeXml2 As New XmlDocument()


            ''
            campo = "<LOG main_crmsoa_getMember START: " & campo22 & ">"
            grabaLogSoa(campo)

            grabaXmlToSend_GetMember_2(campo22, campo33, campo55, campo66, campo77, campo99, campo100, numeroMiembro)

            campo = "<LOG main_crmsoa_getMember END: " & campo22 & ">"
            grabaLogSoa(campo)



            campo = "<LOG respuesta_ws getmember START: " & sUri2 & ">"
            grabaLogSoa(campo)

            ''13Ene2015 Dim respuesta_ws = ExecutateWsdlSoa_GetMember_2(timeOut_Ws, request2, sUri2, soapEnvelopeXml2)
            Dim respuesta_ws = ExecutateWsdlSoa_GetMember_2(timeOut_Ws, HttpWRequest, sUri2, soapEnvelopeXml2)

            campo = "<LOG respuesta_ws getmember END: " & respuesta_ws & ">"
            grabaLogSoa(campo)

            myUri2 = Nothing
            HttpWRequest = Nothing
            ''13Ene2015 request2 = Nothing
            soapEnvelopeXml2 = Nothing

            Return (respuesta_ws)
        Catch exc As Exception
            campo = "<Error: main_crmsoa_getMember: " & exc.ToString() & ">"
            grabaLogSoa(campo)
            campo = "NO HAY COMUNICACION CON WS"
            Return (campo)
        Finally

        End Try
    End Function
    Public Sub grabaXmlToSend_GetMember_2(ByVal campo22_ As String, ByVal campo33_ As String, ByVal campo55_ As String, ByVal campo66_ As String, ByVal campo77_ As String, ByVal campo99_ As String, ByVal campo10_ As String, ByVal numero_Miembro_ As String)
        Try
            contador_file = contador_file + 1
            'name_file2 = name_file & contador_file.ToString & ".xml"
            name_file2 = name_file & ".xml"
            Dim oFile As New FileStream(name_file2, FileMode.Create)
            Dim oXML As New XmlTextWriter(oFile, Encoding.UTF8)

            Numero_tarjetas = numero_Miembro_
            oXML.Formatting = Formatting.Indented
            oXML.WriteProcessingInstruction("xml", "version=""1.0""")
            oXML.WriteStartElement("soapenv:Envelope")
            oXML.WriteAttributeString("xmlns:soapenv", campo22_)
            oXML.WriteAttributeString("xmlns:als", campo33_)

            'Header
            Graba_Header_Ws_2(oXML, campo55_, campo66_, campo77_, campo99_, campo10_)


            oXML.WriteStartElement("soapenv:Body") ' Abro DataSource.
            oXML.WriteStartElement("als:GetMemberRequest")
            oXML.WriteElementString("als:programName", "WOW Rewards")
            oXML.WriteElementString("als:cardNumber", Numero_tarjetas)
            oXML.WriteElementString("als:contactEmailAddress", " ")
            oXML.WriteElementString("als:partnerSource", "Burger King Mexico")
            oXML.WriteElementString("als:responseType", "short")
            oXML.WriteEndElement() ' Cierro ConnectionProperties.
            oXML.WriteEndElement() ' Cierro DataSource.
            oXML.WriteEndElement() ' Cierro el report.
            oXML.Flush()

            contador = oXML.BaseStream.Length()
            oXML.Close()
            oFile.Close()
            oFile.Dispose()

        Catch exc As Exception
            ' Show the exception to the user.
            'Error_Field = "grabaXmlToSend_GetMember." + vbCrLf + "Exception: " + exc.Message
            campo = "<Error: grabaXmlToSend_GetMember_2: " & exc.ToString() & ">"
            grabaLogSoa(campo)
            'MessageBox.Show(Error_Field)
        Finally
        End Try
    End Sub
    Private Sub Graba_Header_Ws_2(ByRef oXML As XmlTextWriter, ByVal campo55_ As String, ByVal campo66_ As String, ByVal campo77_ As String, ByVal campo99_ As String, ByVal campo10_ As String)
        oXML.WriteStartElement("soapenv:Header") ' Abro DataSources.

        oXML.WriteStartElement("wsse:Security")
        oXML.WriteAttributeString("soap:mustUnderstand", 1)
        oXML.WriteAttributeString("xmlns:wsse", campo55_)
        oXML.WriteAttributeString("xmlns:soap", campo66_)

        oXML.WriteStartElement("wsse:UsernameToken")
        oXML.WriteAttributeString("wsu:Id", campo77_)
        oXML.WriteAttributeString("xmlns:wsu", campo99_)
        oXML.WriteElementString("wsse:Username", "soaloyalty")

        oXML.WriteRaw(campo10_)

        oXML.WriteEndElement() ' Cierro DataSources.
        oXML.WriteEndElement() ' Cierro DataSources.
        oXML.WriteEndElement() ' Cierro DataSources.

    End Sub
    Public Function ExecutateWsdlSoa_GetMember_2(ByVal timeOut_WsWs_ As Integer, ByVal HttpWRequest As HttpWebRequest, ByVal sUri2_ As String, ByVal soapEnvelopeXml As XmlDocument) As String
        Try
            'New
            Dim request22 As HttpWebRequest = CreateWebRequest_GetMember_2(timeOut_WsWs_, sUri2_, HttpWRequest)
            request22.Timeout = timeOut_WsWs_
            'New

            readXmlToSend_2()
            _xml = String.Format(Respuesta)
            Dim xtr As New XmlTextReader(New System.IO.StringReader(_xml))

            ''new
            soapEnvelopeXml.Load(xtr)
            xtr.Close()

            campo = "<Envio=Url " & soapEnvelopeXml.InnerXml.ToString() & ">"
            grabaXmlLogSoa(campo)

            ''campo = "<Envio=Url Paso 17>"
            ''grabaLogSoa(campo)

            ''New Dim stream22 As Stream = request2.GetRequestStream()

            campo = "<Envio=Url Paso 18>"
            grabaLogSoa(campo)

            Using stream22 As Stream = request22.GetRequestStream()
                campo = "<Envio=Url Paso 19>"
                grabaLogSoa(campo)
                ''Using stream2 As Stream = request.GetRequestStream()
                soapEnvelopeXml.Save(stream22)
                campo = "<Envio=Url Paso 20>"
                grabaLogSoa(campo)
            End Using

            campo = "<Envio=Url Paso 21>"
            grabaLogSoa(campo)

            Using response As WebResponse = request22.GetResponse()
                campo = "<Envio=Url Paso 22>"
                grabaLogSoa(campo)
                Using rd As New StreamReader(response.GetResponseStream())
                    soapResult = rd.ReadToEnd()
                    campo = "<Respuesta=Url " & soapResult & ">"
                    grabaXmlLogSoa(campo)
                End Using
            End Using

            campo = "<Envio=Url Paso 23>"
            grabaLogSoa(campo)

            Return soapResult

        Catch ex As WebException
            If ex.Status = 14 Or ex.Status = 1 Then
                If ex.Message = "No es posible conectar con el servidor remoto" Or ex.Message.Substring(0, 38) = "No se puede resolver el nombre remoto:" Or ex.Message = "Error en el servidor remoto: (500) Error interno del servidor." Or ex.Message = "Se excedió el tiempo de espera de la operación" Then
                    soapResult = ex.Message
                    error_ws = ex.Message
                    'MessageBox.Show(ex.Message)
                    campo = "<Respuesta=Url ex.Status:" & soapResult & ">"
                    grabaXmlLogSoa(campo)
                    Return soapResult
                End If
            End If

            ex.Response.GetResponseStream()
            Using rd As New StreamReader(ex.Response.GetResponseStream())
                soapResult = rd.ReadToEnd()
                grabaLogSoa(soapResult)
                campo = "<Respuesta=Url rd:" & soapResult & ">"
                grabaXmlLogSoa(campo)
                'MessageBox.Show(soapResult)
            End Using
            Return soapResult
        End Try
    End Function
    Public Sub readXmlToSend_2()
        directorio_file = name_file2
        Dim TextLine As String
        TextLine = ""
        If System.IO.File.Exists(directorio_file) = True Then
            Dim objReader As New System.IO.StreamReader(directorio_file)
            Do While objReader.Peek() <> -1
                TextLine = TextLine & objReader.ReadLine()
            Loop
            Respuesta = TextLine
            objReader.Close()
        Else
            campo = "<File Does Not Exist: " & directorio_file & ">"
            grabaLogSoa(campo)
        End If
        If File.Exists(directorio_file) Then
            File.Delete(directorio_file)
        End If

    End Sub
    Public Function CreateWebRequest_GetMember_2(ByVal timeOut_WsWs_ As Integer, ByVal sUri2_ As String, ByVal HttpWRequest_ As HttpWebRequest) As HttpWebRequest
        Try
            campo = "<WebService=Url " & sUri2_ & " >"
            grabaLogSoa(campo)
            'grabaLogSoa(sUri2)
            'Dim myUri2 As System.Uri = New System.Uri(sUri)
            'Dim HttpWRequest2 As HttpWebRequest = WebRequest.Create(myUri)
            HttpWRequest_.Headers.Add("SOAP:Action")
            HttpWRequest_.ContentType = "text/xml;charset=""UTF-8"""
            HttpWRequest_.Accept = "text/xml"
            HttpWRequest_.Method = "POST"
            HttpWRequest_.KeepAlive = False
            'HttpWRequest.AuthenticationLevel = Security.AuthenticationLevel.None
            HttpWRequest_.Timeout = timeOut_WsWs_
            'HttpWRequest.PreAuthenticate = True
            Return HttpWRequest_
        Catch ex As Exception
            campo = "<Error: CreateWebRequest_GetMember_2: " & ex.ToString() & ">"
            grabaLogSoa(campo)
            grabaLogSoa(ex.ToString())
            Return HttpWRequest_
        End Try
    End Function
    Public Function CreateWebRequest_GetMember_2(ByVal timeOut_WsWs_ As Integer, ByVal HttpWebRequest2 As HttpWebRequest, ByVal sUri2_ As String) As HttpWebRequest
        Try
            campo = "<WebService=Url " & sUri2_ & " >"
            grabaLogSoa(campo)
            'grabaLogSoa(sUri2)
            'Dim myUri2 As System.Uri = New System.Uri(sUri)
            'Dim HttpWRequest2 As HttpWebRequest = WebRequest.Create(myUri)
            HttpWebRequest2.Headers.Add("SOAP:Action")
            HttpWebRequest2.ContentType = "text/xml;charset=""UTF-8"""
            HttpWebRequest2.Accept = "text/xml"
            HttpWebRequest2.Method = "POST"
            HttpWebRequest2.KeepAlive = False
            'HttpWRequest.AuthenticationLevel = Security.AuthenticationLevel.None
            HttpWebRequest2.Timeout = timeOut_WsWs_
            'HttpWRequest.PreAuthenticate = True
            Return HttpWebRequest2
        Catch ex As Exception
            campo = "<Error: CreateWebRequest: " & ex.ToString() & ">"
            grabaLogSoa(campo)
            grabaLogSoa(ex.ToString())
            Return HttpWebRequest2
        End Try
    End Function
    'GetMember


    'Redencion
    Public Function main_crmsoa_Redencion(ByVal Campos_Paso() As String, ByVal sUri2 As String, ByVal campo22 As String, ByVal campo33 As String, ByVal campo44 As String, ByVal campo55 As String, ByVal campo66 As String, ByVal campo77 As String, ByVal campo99 As String, ByVal campo100 As String, ByVal timeOut_Ws As String) As String
        Try
            ''
            '26Dic2014 Dim sUri2 As String = "https://soaprod.grupoalsea.com.mx/Services/Loyalty/ALSEAMemberService"

            '26Dic2014 Const campo22 As String = "http://schemas.xmlsoap.org/soap/envelope/"
            '26Dic2014 Const campo33 As String = "http://services.alsea.com/Loyalty/Schema/ALSEAMemberServiceMsg"
            '26Dic2014 Const campo44 As String = "http://services.alsea.com/Loyalty/Schema/ALSEATransaction"

            '26Dic2014 Const campo55 As String = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"
            '26Dic2014 Const campo66 As String = "soap"

            '26Dic2014 Const campo77 As String = "UsernameToken-XAqqET7bPF88fgxQD4ytyw22"

            '26Dic2014 Const campo99 As String = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"

            '26Dic2014 Const campo100 As String = "<wsse:Password Type=" & Chr(34) & "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText" & Chr(34) & ">L0y417y.</wsse:Password>"

            '26Dic2014 Const timeOut_Ws As Integer = 30000

            Dim myUri2 As System.Uri = New System.Uri(sUri2)
            Dim HttpWRequest As HttpWebRequest = WebRequest.Create(myUri2)

            Dim request2 As HttpWebRequest = CreateWebRequest_GetMember_2(timeOut_Ws, sUri2, HttpWRequest)
            Dim soapEnvelopeXml2 As New XmlDocument()


            ''
            campo = "<LOG grabaXmlToSend_Redem: START " & campo22 & ">"
            grabaLogSoa(campo)

            grabaXmlToSend_Redem(campo22, campo33, campo44, campo55, campo66, campo77, campo99, campo100, Campos_Paso)

            Dim respuesta_ws = ExecutateWsdlSoaRedem(timeOut_Ws, HttpWRequest, sUri2)

            campo = "<LOG respuesta_ws grabaXmlToSend_Redem: END " & respuesta_ws & ">"
            grabaLogSoa(campo)

            myUri2 = Nothing
            HttpWRequest = Nothing
            request2 = Nothing
            soapEnvelopeXml2 = Nothing

            Return (respuesta_ws)
        Catch exc As Exception
            campo = "<Error: main_crmsoa_Redencion: " & exc.ToString() & ">"
            grabaLogSoa(campo)
            Return (campo)
        Finally

        End Try
    End Function
    Public Sub grabaXmlToSend_Redem(ByVal campo2 As String, ByVal campo3 As String, ByVal campo4 As String, ByVal campo5 As String, ByVal campo6 As String, ByVal campo7 As String, ByVal campo9 As String, ByVal campo10 As String, ByVal Campos_Paso() As String)
        contador_file = contador_file + 1
        name_file2 = name_file & contador_file.ToString
        Dim oFile As New FileStream(name_file2, FileMode.Create)
        Dim oXML As New XmlTextWriter(oFile, Encoding.UTF8)
        'MessageBox.Show("Redenciones")

        oXML.Formatting = Formatting.Indented
        oXML.WriteProcessingInstruction("xml", "version=""1.0""")

        oXML.WriteStartElement("soapenv:Envelope")

        oXML.WriteAttributeString("xmlns:soapenv", campo2)
        oXML.WriteAttributeString("xmlns:als", campo3)
        oXML.WriteAttributeString("xmlns:als1", campo4)

        ''AQUI VA HEADER
        Genera_Header_WS(oXML, campo5, campo6, campo7, campo9, campo10)

        oXML.WriteStartElement("soapenv:Body") ' Abro DataSource.
        oXML.WriteStartElement("als:RedeemPointsRequest")

        ''OnLine
        oXML.WriteElementString("als:transactionType", "online")
        oXML.WriteElementString("als:memberNumber", Campos_Paso(1))

        If Len(Campos_Paso(40)) > 0 Then
            oXML.WriteElementString("als:cardNumber", Campos_Paso(40))
        Else
            oXML.WriteElementString("als:cardNumber", Campos_Paso(1))
        End If

        ''OnLine

        ''Test
        ''oXML.WriteElementString("als:transactionType", "online")
        ''oXML.WriteElementString("als:memberNumber", "6012578683364132")
        ''oXML.WriteElementString("als:cardNumber", "6012578683364132")
        ''Test

        oXML.WriteStartElement("als:transactionsList")

        ''Aqui va registro
        Dim Inicio As Integer
        For Inicio = 1 To 1
            ''Graba_Redencion_WS_Test(oXML)
        Next Inicio

        Graba_Redencion_WS(oXML, Campos_Paso)


        oXML.WriteEndElement() ' Cierro DataSource.
        oXML.WriteEndElement() ' Cierro el report.
        oXML.WriteEndElement() ' Cierro el report.
        oXML.WriteEndElement() ' Cierro el report.

        oXML.Flush()
        contador = oXML.BaseStream.Length()
        oFile.Close()

    End Sub
    Public Function ExecutateWsdlSoaRedem(ByVal timeOut_Ws As Integer, ByVal HttpWRequest As HttpWebRequest, ByVal sUri As String) As String
        Try
            Dim request As HttpWebRequest = CreateWebRequestRedem(HttpWRequest, sUri, timeOut_Ws)
            Dim soapEnvelopeXml As New XmlDocument()

            readXmlToSend_2()

            _xml = String.Format(Respuesta)
            Dim xtr As New XmlTextReader(New System.IO.StringReader(_xml))
            'ds.ReadXml(_xml)

            soapEnvelopeXml.Load(xtr)

            campo = "<<Envio=Url>>"
            grabaXmlLogSoa(campo)
            'campo = "InnerText:"
            'grabaLogSoa(soapEnvelopeXml.InnerText.ToString())
            campo = "InnerXml:"
            grabaXmlLogSoa(soapEnvelopeXml.InnerXml.ToString())
            campo = "<<Respuesta=Url>>"
            grabaXmlLogSoa(campo)

            request.Timeout = timeOut_Ws
            Using stream As Stream = request.GetRequestStream()
                soapEnvelopeXml.Save(stream)
            End Using

            Using response As WebResponse = request.GetResponse()
                Using rd As New StreamReader(response.GetResponseStream())
                    soapResult = rd.ReadToEnd()
                    grabaXmlLogSoa(soapResult)
                    Return soapResult
                End Using
            End Using

        Catch ex As WebException
            'MessageBox.Show(ex.Status)

            If ex.Status = 14 Or ex.Status = 1 Then
                If ex.Message = "No es posible conectar con el servidor remoto" Or ex.Message.Substring(0, 38) = "No se puede resolver el nombre remoto:" Or ex.Message = "Error en el servidor remoto: (500) Error interno del servidor." Or ex.Message = "Se excedió el tiempo de espera de la operación" Then
                    soapResult = ex.Message
                    error_ws = ex.Message
                    grabaXmlLogSoa(soapResult)
                    'MessageBox.Show(ex.Message)
                    Return soapResult
                End If
            End If

            ex.Response.GetResponseStream()
            Using rd As New StreamReader(ex.Response.GetResponseStream())
                soapResult = rd.ReadToEnd()
                grabaXmlLogSoa(soapResult)
                'MessageBox.Show(soapResult)
                Return soapResult
            End Using
        End Try
    End Function
    Public Function CreateWebRequestRedem(ByVal HttpWRequest As HttpWebRequest, ByVal sUri As String, ByVal timeOut_Ws As Integer) As HttpWebRequest
        Try
            campo = "<<WebService=Url>>"
            grabaLogSoa(campo)
            grabaLogSoa(sUri)

            'HttpWRequest.Accept = "text/xml;charset=UTF-8"
            'HttpWRequest.UserAgent = "Apache-HttpClient/4.1.1 (java 1.5)"
            'HttpWRequest.UseDefaultCredentials = Security.AuthenticationLevel.None
            'HttpWRequest.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.0.3705;)"
            'HttpWRequest.AuthenticationLevel = Security.AuthenticationLevel.None
            'HttpWRequest.ContentType = "application/x-www-form-urlencoded"


            HttpWRequest.Headers.Add("Accept-Encoding: gzip,deflate")
            HttpWRequest.ContentType = "text/xml;charset=""UTF-8"""
            HttpWRequest.Headers.Add("SOAPAction: cancelAccrual")
            HttpWRequest.UserAgent = "Jakarta Commons-HttpClient/3.1"
            'HttpWRequest.Host = "alsea-soatest.oracleoutsourcing.com"


            ''HttpWRequest.Headers.Add("Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7")
            ''HttpWRequest.Headers.Add("Accept-Language: en-us,en;q=0.5")
            HttpWRequest.Headers.Add("Keep-Alive: 30")

            HttpWRequest.Headers.Add("SOAPAction", "https://alsea-soatest.oracleoutsourcing.com:443/Services/Loyalty/ALSEAMemberService/cancelAccrual")
            ''HttpWRequest.UserAgent = "Apache-HttpClient/4.1.1 (java 1.5)"


            HttpWRequest.Method = "POST"
            HttpWRequest.KeepAlive = True
            ''HttpWRequest.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"
            ''HttpWRequest.Referer = sUri

            HttpWRequest.Timeout = timeOut_Ws

            Return HttpWRequest
        Catch ex As Exception
            campo = "Error: CreateWebRequest:"
            grabaLogSoa(campo)
            grabaLogSoa(ex.ToString())
            Return HttpWRequest

        End Try

    End Function
    Private Sub Graba_Redencion_WS(ByRef oXML As XmlTextWriter, ByVal Campos_Paso() As String)
        ''Registro
        oXML.WriteStartElement("als1:transaction")


        oXML.WriteElementString("als1:adjustedListPrice", Campos_Paso(7))

        oXML.WriteElementString("als1:itemNumber", "0")

        ''oXML.WriteElementString("als1:locationId", "")
        oXML.WriteElementString("als1:locationName", Campos_Paso(11))
        oXML.WriteElementString("als1:partnerName", Campos_Paso(12))
        ''oXML.WriteElementString("als1:status", "Acceptable")

        ''oXML.WriteElementString("als1:partnerId", "")

        ''oXML.WriteElementString("als1:pointId", "")

        ''16Ene2015 oXML.WriteElementString("als1:pointName", "")

        oXML.WriteElementString("als1:points", "0")

        ''oXML.WriteElementString("als1:postDate", "")

        ''oXML.WriteElementString("als1:processDate", "")

        ''oXML.WriteElementString("als1:productId", "")
        oXML.WriteElementString("als1:productName", Campos_Paso(18))

        ''oXML.WriteElementString("als1:programID", "")
        oXML.WriteElementString("als1:programName", "WOW Rewards")

        oXML.WriteElementString("als1:status", "Acceptable")

        ''oXML.WriteElementString("als1:subStatus", "")
        oXML.WriteElementString("als1:ticketNumber", Campos_Paso(23))
        oXML.WriteElementString("als1:transactionChannel", Campos_Paso(24))

        ''oXML.WriteElementString("als1:transactionNumber", "")
        oXML.WriteElementString("als1:transactionDate", Campos_Paso(25)) ''"10/14/2014 13:18:44")
        oXML.WriteElementString("als1:transactionSubtype", Campos_Paso(26))

        oXML.WriteElementString("als1:transactionType", "Redemption")   ''"Acceptable")

        ''oXML.WriteElementString("als1:voucherNumber", "")

        '26ENen2015

        'Trim(Campos(0)) = "OFF" Or Datos.IndexOf("OFF") > -1
        If Mid(Trim(Campos_Paso(18)), 1, 3) = "Aportacion" Or Campos_Paso(18).IndexOf("Aportacion") > -1 Then
            'No genera etiqueta
        Else
            oXML.WriteElementString("als1:voucherQuantity", Campos_Paso(29))
            oXML.WriteElementString("als1:voucherType", Campos_Paso(30))  '09Jul2015
        End If
        '26ENen2015


        oXML.WriteElementString("als1:amount", Campos_Paso(8))
        oXML.WriteElementString("als1:paymentMode", Campos_Paso(5))

        oXML.WriteElementString("als1:levelPrice", Campos_Paso(3))
        oXML.WriteElementString("als1:waiter", Campos_Paso(38))
        oXML.WriteElementString("als1:numberOfPeople", "1")
        oXML.WriteElementString("als1:quantity", Campos_Paso(19))

        ''oXML.WriteElementString("als1:branchDivision", Campos_Paso(4))

        ''oXML.WriteElementString("als1:activityDate", "")

        ''oXML.WriteElementString("als1:comments", "")
        oXML.WriteElementString("als1:organization", Campos_Paso(31))

        ''oXML.WriteElementString("als1:processingComment", "")

        ''oXML.WriteElementString("als1:processingLog", "")

        oXML.WriteEndElement() ' Cierro ConnectionProperties.
        ''Registro End

    End Sub
    Sub Genera_Header_WS(ByRef oxML As XmlTextWriter, ByVal campo5 As String, ByVal campo6 As String, ByVal campo7 As String, ByVal campo9 As String, ByVal campo10 As String)
        oxML.WriteStartElement("soapenv:Header") ' Abro DataSources.

        oxML.WriteStartElement("wsse:Security")
        oxML.WriteAttributeString("soap:mustUnderstand", 1)
        oxML.WriteAttributeString("xmlns:wsse", campo5)
        oxML.WriteAttributeString("xmlns:soap", campo6)

        oxML.WriteStartElement("wsse:UsernameToken")
        oxML.WriteAttributeString("wsu:Id", campo7)
        oxML.WriteAttributeString("xmlns:wsu", campo9)
        oxML.WriteElementString("wsse:Username", "soaloyalty")
        ''oXML.WriteElementString("wsse:Password", " ", campo8, "L0y417y.")

        oxML.WriteRaw(campo10)


        'oXML.WriteStartElement("wsse:UsernameToken wsu:Id=", "UsernameToken-XAqqET7bPF88fgxQD4ytyw22")
        'oXML.WriteAttributeString("xmlns:wsu", campo5)
        'oXML.WriteAttributeString("xmlns:wsu", campo6)


        'oXML.WriteStartElement("wsse:Security soap:mustUnderstand=", 1)
        'oXML.WriteAttributeString("xmlns:soap", campo7)
        'oXML.WriteAttributeString("xmlns:soap", campo6)
        'oXML.WriteElementString("wsse:Username", "soaloyalty")
        'oXML.WriteElementString(campo8, "L0y417y")



        'oXML.WriteEndElement() ' Cierro DataSources.
        oxML.WriteEndElement() ' Cierro DataSources.
        oxML.WriteEndElement() ' Cierro DataSources.
        oxML.WriteEndElement() ' Cierro DataSources.

    End Sub
    'Redencion

    'Redencion Tmed Wow
    Public Function main_crmsoa_Redencion_Tmed_Wow(ByVal Campos_Paso() As String, ByVal sUri2 As String, ByVal campo22 As String, ByVal campo33 As String, ByVal campo44 As String, ByVal campo55 As String, ByVal campo66 As String, ByVal campo77 As String, ByVal campo99 As String, ByVal campo100 As String, ByVal timeOut_Ws As String) As String
        Try
            ''
            '26Dic2014 Dim sUri2 As String = "https://soaprod.grupoalsea.com.mx/Services/Loyalty/ALSEAMemberService"

            '26Dic2014 Const campo22 As String = "http://schemas.xmlsoap.org/soap/envelope/"
            '26Dic2014 Const campo33 As String = "http://services.alsea.com/Loyalty/Schema/ALSEAMemberServiceMsg"
            '26Dic2014 Const campo44 As String = "http://services.alsea.com/Loyalty/Schema/ALSEATransaction"

            '26Dic2014 Const campo55 As String = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"
            '26Dic2014 Const campo66 As String = "soap"

            '26Dic2014 Const campo77 As String = "UsernameToken-XAqqET7bPF88fgxQD4ytyw22"

            '26Dic2014 Const campo99 As String = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"

            '26Dic2014 Const campo100 As String = "<wsse:Password Type=" & Chr(34) & "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText" & Chr(34) & ">L0y417y.</wsse:Password>"

            '26Dic2014 Const timeOut_Ws As Integer = 30000

            Dim myUri2 As System.Uri = New System.Uri(sUri2)
            Dim HttpWRequest As HttpWebRequest = WebRequest.Create(myUri2)

            Dim request2 As HttpWebRequest = CreateWebRequest_GetMember_2(timeOut_Ws, sUri2, HttpWRequest)
            Dim soapEnvelopeXml2 As New XmlDocument()


            ''
            campo = "<LOG grabaXmlToSend_Redem: START " & campo22 & ">"
            grabaLogSoa(campo)

            grabaXmlToSend_Redem_Tmed_Wow(campo22, campo33, campo44, campo55, campo66, campo77, campo99, campo100, Campos_Paso)

            Dim respuesta_ws = ExecutateWsdlSoaRedem(timeOut_Ws, HttpWRequest, sUri2)

            campo = "<LOG respuesta_ws grabaXmlToSend_Redem: END " & respuesta_ws & ">"
            grabaLogSoa(campo)

            myUri2 = Nothing
            HttpWRequest = Nothing
            request2 = Nothing
            soapEnvelopeXml2 = Nothing

            Return (respuesta_ws)
        Catch exc As Exception
            campo = "<Error: main_crmsoa_Redencion: " & exc.ToString() & ">"
            grabaLogSoa(campo)
            Return (campo)
        Finally

        End Try
    End Function
    Public Sub grabaXmlToSend_Redem_Tmed_Wow(ByVal campo2 As String, ByVal campo3 As String, ByVal campo4 As String, ByVal campo5 As String, ByVal campo6 As String, ByVal campo7 As String, ByVal campo9 As String, ByVal campo10 As String, ByVal Campos_Paso() As String)
        contador_file = contador_file + 1
        name_file2 = name_file & contador_file.ToString
        Dim oFile As New FileStream(name_file2, FileMode.Create)
        Dim oXML As New XmlTextWriter(oFile, Encoding.UTF8)
        'MessageBox.Show("Redenciones")

        oXML.Formatting = Formatting.Indented
        oXML.WriteProcessingInstruction("xml", "version=""1.0""")

        oXML.WriteStartElement("soapenv:Envelope")

        oXML.WriteAttributeString("xmlns:soapenv", campo2)
        oXML.WriteAttributeString("xmlns:als", campo3)
        oXML.WriteAttributeString("xmlns:als1", campo4)

        ''AQUI VA HEADER
        Genera_Header_WS(oXML, campo5, campo6, campo7, campo9, campo10)

        oXML.WriteStartElement("soapenv:Body") ' Abro DataSource.
        oXML.WriteStartElement("als:RedeemPointsRequest")

        ''OnLine
        oXML.WriteElementString("als:transactionType", "online")
        oXML.WriteElementString("als:memberNumber", Campos_Paso(1))
        '20Ene2015
        If Len(Campos_Paso(39)) > 0 Then
            oXML.WriteElementString("als:cardNumber", Campos_Paso(39))
        Else
            oXML.WriteElementString("als:cardNumber", Campos_Paso(1))
        End If
        '20Ene2015
        ''OnLine

        ''Test
        ''oXML.WriteElementString("als:transactionType", "online")
        ''oXML.WriteElementString("als:memberNumber", "6012578683364132")
        ''oXML.WriteElementString("als:cardNumber", "6012578683364132")
        ''Test

        oXML.WriteStartElement("als:transactionsList")

        ''Aqui va registro
        Dim Inicio As Integer
        For Inicio = 1 To 1
            ''Graba_Redencion_WS_Test(oXML)
        Next Inicio

        Graba_Redencion_WS_Tmed_Wow(oXML, Campos_Paso)


        oXML.WriteEndElement() ' Cierro DataSource.
        oXML.WriteEndElement() ' Cierro el report.
        oXML.WriteEndElement() ' Cierro el report.
        oXML.WriteEndElement() ' Cierro el report.

        oXML.Flush()
        contador = oXML.BaseStream.Length()
        oFile.Close()

    End Sub
    Private Sub Graba_Redencion_WS_Tmed_Wow(ByRef oXML As XmlTextWriter, ByVal Campos_Paso() As String)
        ''Registro
        oXML.WriteStartElement("als1:transaction")


        oXML.WriteElementString("als1:adjustedListPrice", Campos_Paso(7))

        oXML.WriteElementString("als1:itemNumber", "0")

        ''oXML.WriteElementString("als1:locationId", "")
        oXML.WriteElementString("als1:locationName", Campos_Paso(11))
        oXML.WriteElementString("als1:partnerName", Campos_Paso(12))
        ''oXML.WriteElementString("als1:status", "Acceptable")

        ''oXML.WriteElementString("als1:partnerId", "")

        ''oXML.WriteElementString("als1:pointId", "")

        ''13Ene2015 oXML.WriteElementString("als1:pointName", "")

        oXML.WriteElementString("als1:points", "0")

        ''oXML.WriteElementString("als1:postDate", "")

        ''oXML.WriteElementString("als1:processDate", "")

        ''oXML.WriteElementString("als1:productId", "")
        oXML.WriteElementString("als1:productName", Campos_Paso(18))

        ''oXML.WriteElementString("als1:programID", "")
        oXML.WriteElementString("als1:programName", "WOW Rewards")

        oXML.WriteElementString("als1:status", "Acceptable")

        ''oXML.WriteElementString("als1:subStatus", "")
        oXML.WriteElementString("als1:ticketNumber", Campos_Paso(23))
        oXML.WriteElementString("als1:transactionChannel", Campos_Paso(24))

        ''oXML.WriteElementString("als1:transactionNumber", "")
        oXML.WriteElementString("als1:transactionDate", Campos_Paso(25)) ''"10/14/2014 13:18:44")
        oXML.WriteElementString("als1:transactionSubtype", Campos_Paso(26))

        oXML.WriteElementString("als1:transactionType", "Redemption")   ''"Acceptable")

        ''oXML.WriteElementString("als1:voucherNumber", "")

        ''13Ene2015 oXML.WriteElementString("als1:voucherQuantity", Campos_Paso(34))

        ''13Ene2015 oXML.WriteElementString("als1:voucherType", Campos_Paso(30))
        oXML.WriteElementString("als1:amount", Campos_Paso(8))
        oXML.WriteElementString("als1:paymentMode", Campos_Paso(5))

        oXML.WriteElementString("als1:levelPrice", Campos_Paso(3))
        oXML.WriteElementString("als1:waiter", Campos_Paso(38))
        oXML.WriteElementString("als1:numberOfPeople", "1")
        oXML.WriteElementString("als1:quantity", Campos_Paso(19))

        ''oXML.WriteElementString("als1:branchDivision", Campos_Paso(4))

        ''oXML.WriteElementString("als1:activityDate", "")

        ''oXML.WriteElementString("als1:comments", "")
        oXML.WriteElementString("als1:organization", Campos_Paso(31))

        ''oXML.WriteElementString("als1:processingComment", "")

        ''oXML.WriteElementString("als1:processingLog", "")

        oXML.WriteEndElement() ' Cierro ConnectionProperties.
        ''Registro End

    End Sub
    'Redencion Tmed Wow

    'Acreditacion
    Public Function main_crmsoa_Acreditacion(ByVal Campos_Paso() As String, ByVal Id As String, ByVal sUri2 As String, ByVal campo22 As String, ByVal campo33 As String, ByVal campo44 As String, ByVal campo55 As String, ByVal campo66 As String, ByVal campo77 As String, ByVal campo99 As String, ByVal campo100 As String, ByVal timeOut_Ws As String)
        Try
            ''
            '26Dic2014 Dim sUri2 As String = "https://soaprod.grupoalsea.com.mx/Services/Loyalty/ALSEAMemberService"

            '26Dic2014 Const campo22 As String = "http://schemas.xmlsoap.org/soap/envelope/"
            '26Dic2014 Const campo33 As String = "http://services.alsea.com/Loyalty/Schema/ALSEAMemberServiceMsg"
            '26Dic2014 Const campo44 As String = "http://services.alsea.com/Loyalty/Schema/ALSEATransaction"

            '26Dic2014 Const campo55 As String = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"
            '26Dic2014 Const campo66 As String = "soap"

            '26Dic2014 Const campo77 As String = "UsernameToken-XAqqET7bPF88fgxQD4ytyw22"

            '26Dic2014 Const campo99 As String = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"

            '26Dic2014 Const campo100 As String = "<wsse:Password Type=" & Chr(34) & "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText" & Chr(34) & ">L0y417y.</wsse:Password>"

            '26Dic2014 Const timeOut_Ws As Integer = 30000

            Dim myUri2 As System.Uri = New System.Uri(sUri2)
            Dim HttpWRequest As HttpWebRequest = WebRequest.Create(myUri2)

            Dim request2 As HttpWebRequest = CreateWebRequest_GetMember_2(timeOut_Ws, sUri2, HttpWRequest)
            Dim soapEnvelopeXml2 As New XmlDocument()

            ''
            campo = "<LOG main_crmsoa_Acreditacion: START " & campo22 & ">"
            grabaLogSoa(campo)

            grabaXmlToSend_Accrual(campo22, campo33, campo44, campo55, campo66, campo77, campo99, campo100, Campos_Paso, Id)

            Dim respuesta_ws = ExecutateWsdlSoaRedem(timeOut_Ws, HttpWRequest, sUri2)
            campo = "<LOG respuesta_ws ACR: END " & respuesta_ws & ">"
            grabaLogSoa(campo)

            myUri2 = Nothing
            HttpWRequest = Nothing
            request2 = Nothing
            soapEnvelopeXml2 = Nothing

            Return (respuesta_ws)
        Catch exc As Exception
            campo = "<Error: main_crmsoa_Acreditacion: " & exc.ToString() & ">"
            grabaLogSoa(campo)
            Return (campo)
        Finally

        End Try
    End Function
    Public Sub grabaXmlToSend_Accrual(ByVal campo2 As String, ByVal campo3 As String, ByVal campo4 As String, ByVal campo5 As String, ByVal campo6 As String, ByVal campo7 As String, ByVal campo9 As String, ByVal campo10 As String, ByVal Campos_Paso() As String, ByVal Id As String)
        contador_file = contador_file + 1
        name_file2 = name_file & contador_file.ToString
        Dim oFile As New FileStream(name_file2, FileMode.Create)
        Dim oXML As New XmlTextWriter(oFile, Encoding.UTF8)
        'MessageBox.Show("Acreditaciones")
        oXML.Formatting = Formatting.Indented
        oXML.WriteProcessingInstruction("xml", "version=""1.0""")

        oXML.WriteStartElement("soapenv:Envelope")

        oXML.WriteAttributeString("xmlns:soapenv", campo2)
        oXML.WriteAttributeString("xmlns:als", campo3)
        oXML.WriteAttributeString("xmlns:als1", campo4)

        ''AQUI VA HEADER
        Genera_Header_WS(oXML, campo5, campo6, campo7, campo9, campo10)


        oXML.WriteStartElement("soapenv:Body") ' Abro DataSource.
        oXML.WriteStartElement("als:AccruePointsRequest")

        ''OnLine
        oXML.WriteElementString("als:transactionType", "online")
        oXML.WriteElementString("als:memberNumber", Campos_Paso(2))
        oXML.WriteElementString("als:cardNumber", Campos_Paso(3))
        ''OnLine

        ''Test
        'oXML.WriteElementString("als:transactionType", "online")
        'oXML.WriteElementString("als:memberNumber", "6012578683364132")
        'oXML.WriteElementString("als:cardNumber", "6012578683364132")


        oXML.WriteStartElement("als:transactionsList")


        ''Aqui va registro

        SQLSearch_Acreditaciones(Id, oXML)

        'Dim Inicio As Integer
        'For Inicio = 1 To 2
        'Graba_Acreditacion_WS_Test(oXML)
        'Next Inicio



        'oXML.WriteEndElement() ' Cierro ConnectionProperties.
        oXML.WriteEndElement() ' Cierro DataSource.
        oXML.WriteEndElement() ' Cierro el report.
        oXML.WriteEndElement() ' Cierro el report.
        oXML.WriteEndElement() ' Cierro el report.

        oXML.Flush()
        contador = oXML.BaseStream.Length()
        oFile.Close()

    End Sub
    Private Sub SQLSearch_Acreditaciones(ByVal TicketNumber_ As String, ByRef oXML As XmlTextWriter)
        Try
            Permite_Conexcion_CRM_Micros_Dos = False
            Salir = False
            ODBC_ConnectStringMICROS_Dos()
            If Permite_Conexcion_CRM_Micros_Dos = True Then
                'campo = "SELECT MemberNumber, id,ALSELevelPrice,BranchDivision,ALSEPaymnetMode,ActivityDate,AdjustedListPrice,Amount,Comments,ItemNumber,LocationName,PartnerName,PointName,Points,ProcessDate,ProcessingComment,ProcessingLog,ProductName,Quantity,IntegrationStatus,Status,SubStatus,TicketNumber,TransactionChannel,TransactionDate,TransactionSubType,TransactionType,VoucherNumber,VoucherQty,VoucherType,Organization,Estatus, Bussiness_date, centroconsumo, Cheque, Transmitido, NumeroLineaMicros FROM custom.LOYTRANSACCION Where ProductName <> 'Ticket' And TicketNumber = '" & TicketNumber_ & "'  And TransactionType = 'Accrual' ORDER BY Consecutivo DESC"
                campo = "SELECT MemberNumber, id,ALSELevelPrice,BranchDivision,ALSEPaymnetMode,ActivityDate,AdjustedListPrice,Amount,Comments,ItemNumber,LocationName,PartnerName,PointName,Points,ProcessDate,ProcessingComment,ProcessingLog,ProductName,Quantity,IntegrationStatus,Status,SubStatus,TicketNumber,TransactionChannel,TransactionDate,TransactionSubType,TransactionType,VoucherNumber,VoucherQty,VoucherType,Organization,Estatus, Bussiness_date, centroconsumo, Cheque, Transmitido, NumeroLineaMicros, Isnull(Cajero, ' ') as cajero FROM custom.LOYTRANSACCION Where TicketNumber = '" & TicketNumber_ & "'  And TransactionType = 'Accrual' ORDER BY Consecutivo DESC"
                Dim Command As New OdbcCommand(campo, ConexcionMCRSPOS_Dos)
                Command.CommandType = CommandType.Text
                Registro_Log = ("SQLSearchCanceladosOffLine: " & campo)
                grabaLogSoa(Registro_Log)
                ReadSql_odbc_Dos = Command.ExecuteReader(CommandBehavior.CloseConnection)
                While ReadSql_odbc_Dos.Read = True
                    T_MemberNumber = ReadSql_odbc_Dos.GetValue(0)
                    T_id = ReadSql_odbc_Dos.GetValue(1)
                    T_ALSELevelPrice = ReadSql_odbc_Dos.GetValue(2)
                    T_BranchDivision = ReadSql_odbc_Dos.GetValue(3)
                    T_ALSEPaymnetMode = ReadSql_odbc_Dos.GetValue(4)
                    T_ActivityDate = ReadSql_odbc_Dos.GetValue(5)
                    T_AdjustedListPrice = ReadSql_odbc_Dos.GetValue(6)
                    T_Amount = ReadSql_odbc_Dos.GetValue(7)
                    T_Comments = ReadSql_odbc_Dos.GetValue(8)
                    T_ItemNumber = ReadSql_odbc_Dos.GetValue(9)
                    T_LocationName = ReadSql_odbc_Dos.GetValue(10)
                    T_PartnerName = ReadSql_odbc_Dos.GetValue(11)
                    T_PointName = ReadSql_odbc_Dos.GetValue(12)
                    T_Points = ReadSql_odbc_Dos.GetValue(13)
                    T_ProcessDate = ReadSql_odbc_Dos.GetValue(14)
                    T_ProcessingComment = ReadSql_odbc_Dos.GetValue(15)
                    T_ProcessingLog = ReadSql_odbc_Dos.GetValue(16)
                    T_ProductName = ReadSql_odbc_Dos.GetValue(17)
                    T_Quantity = ReadSql_odbc_Dos.GetValue(18)
                    T_IntegrationStatus = ReadSql_odbc_Dos.GetValue(19)
                    T_Status = ReadSql_odbc_Dos.GetValue(20)
                    T_SubStatus = ReadSql_odbc_Dos.GetValue(21)
                    T_TicketNumber = ReadSql_odbc_Dos.GetValue(22)
                    T_TransactionChannel = ReadSql_odbc_Dos.GetValue(23)
                    T_TransactionDate = ReadSql_odbc_Dos.GetValue(24)
                    T_TransactionSubType = ReadSql_odbc_Dos.GetValue(25)
                    T_TransactionType = ReadSql_odbc_Dos.GetValue(26)
                    T_VoucherNumber = ReadSql_odbc_Dos.GetValue(27)
                    T_VoucherQty = ReadSql_odbc_Dos.GetValue(28)
                    T_VoucherType = ReadSql_odbc_Dos.GetValue(29)
                    T_Organization = ReadSql_odbc_Dos.GetValue(30)
                    T_Estatus = ReadSql_odbc_Dos.GetValue(31)
                    T_Bussiness_date = ReadSql_odbc_Dos.GetValue(32)
                    T_centroconsumo = ReadSql_odbc_Dos.GetValue(33)
                    T_Cheque = ReadSql_odbc_Dos.GetValue(34)
                    T_Transmitido = ReadSql_odbc_Dos.GetValue(35)
                    T_NumeroLineaMicros = ReadSql_odbc_Dos.GetValue(36)
                    T_Waiter = ReadSql_odbc_Dos.GetValue(37)

                    Registro_Log = ("LOG Graba_Acreditacion_WS_T INICIO: " & campo)
                    grabaLogSoa(Registro_Log)

                    Graba_Acreditacion_WS_T(oXML)

                    Registro_Log = ("LOG Graba_Acreditacion_WS_T FIN: " & campo)
                    grabaLogSoa(Registro_Log)


                    'Registro_Log = ("SQLReadResponse: " & Consecutivo.ToString & ":" & Estatus & ":" & IntegrationStatus & ":" & Status & ":" & SubStatus)
                    'Write_event_log_error_file(Registro_Log)
                End While
            Else
                Salir = True
            End If
        Catch Errores_SQLSearch_Acreditaciones As Exception
            Registro_Log = "Errores_SQLSearch_Acreditaciones               :" + Errores_SQLSearch_Acreditaciones.ToString
            'EventLog.WriteEntry(Registro_Log)
        Finally
            If Salir = False Then
                ReadSql_odbc_Dos.Close()
                ODBC_ConnectStringMICROS_Dos()
            End If
        End Try
    End Sub
    Private Sub Graba_Acreditacion_WS_T(ByRef oXML As XmlTextWriter)
        ''Registro
        oXML.WriteStartElement("als1:transaction")


        oXML.WriteElementString("als1:adjustedListPrice", T_AdjustedListPrice)

        oXML.WriteElementString("als1:itemNumber", T_ItemNumber)

        ''oXML.WriteElementString("als1:locationId", "")
        oXML.WriteElementString("als1:locationName", T_LocationName)

        T_PartnerName = "Burger King Mexico"

        oXML.WriteElementString("als1:partnerName", T_PartnerName)

        ''oXML.WriteElementString("als1:partnerId", "")

        ''oXML.WriteElementString("als1:pointId", "")

        ''oXML.WriteElementString("als1:pointName", "")

        oXML.WriteElementString("als1:points", "0")

        ''oXML.WriteElementString("als1:postDate", "")

        ''oXML.WriteElementString("als1:processDate", "")

        ''oXML.WriteElementString("als1:productId", "")
        oXML.WriteElementString("als1:productName", T_ProductName)

        ''oXML.WriteElementString("als1:programID", "")

        oXML.WriteElementString("als1:programName", "WOW Rewards")

        If Len(T_Status) = 0 Then
            T_Status = "Queued"
        End If

        oXML.WriteElementString("als1:status", T_Status)

        ''oXML.WriteElementString("als1:subStatus", "")
        oXML.WriteElementString("als1:ticketNumber", T_TicketNumber)
        oXML.WriteElementString("als1:transactionChannel", T_TransactionChannel)

        ''oXML.WriteElementString("als1:transactionNumber", "")
        oXML.WriteElementString("als1:transactionDate", T_TransactionDate) ''"10/14/2014 13:18:44")
        oXML.WriteElementString("als1:transactionSubtype", T_TransactionSubType)

        oXML.WriteElementString("als1:transactionType", T_TransactionType)   ''"Acceptable")

        ''oXML.WriteElementString("als1:voucherNumber", "")

        ''oXML.WriteElementString("als1:voucherQuantity", T_VoucherQty)

        ''oXML.WriteElementString("als1:voucherType", "")
        oXML.WriteElementString("als1:amount", T_Amount)
        oXML.WriteElementString("als1:paymentMode", T_ALSEPaymnetMode)

        oXML.WriteElementString("als1:levelPrice", T_ALSELevelPrice)
        oXML.WriteElementString("als1:waiter", T_Waiter)
        oXML.WriteElementString("als1:numberOfPeople", "1")
        oXML.WriteElementString("als1:quantity", T_Quantity)

        ''oXML.WriteElementString("als1:branchDivision", T_BranchDivision)

        ''oXML.WriteElementString("als1:activityDate", "")

        ''oXML.WriteElementString("als1:comments", "")
        oXML.WriteElementString("als1:organization", T_Organization)

        ''oXML.WriteElementString("als1:processingComment", "")

        ''oXML.WriteElementString("als1:processingLog", "")

        oXML.WriteEndElement() ' Cierro ConnectionProperties.
        ''Registro End

    End Sub
    'Acreditacion

    'Acreditacion TMed
    Public Function main_crmsoa_Acreditacion_Tmed(ByVal Campos_Paso() As String, ByVal Id As String, ByVal sUri2 As String, ByVal campo22 As String, ByVal campo33 As String, ByVal campo44 As String, ByVal campo55 As String, ByVal campo66 As String, ByVal campo77 As String, ByVal campo99 As String, ByVal campo100 As String, ByVal timeOut_Ws As String, ByVal Campos_Paso_WOW() As String)
        Try
            ''
            Dim myUri2 As System.Uri = New System.Uri(sUri2)
            Dim HttpWRequest As HttpWebRequest = WebRequest.Create(myUri2)

            Dim request2 As HttpWebRequest = CreateWebRequest_GetMember_2(timeOut_Ws, sUri2, HttpWRequest)
            Dim soapEnvelopeXml2 As New XmlDocument()

            ''
            campo = "<LOG grabaXmlToSend_Accrual_Tmed_Wow: START>"
            grabaLogSoa(campo)

            grabaXmlToSend_Accrual_Tmed_Wow(campo22, campo33, campo44, campo55, campo66, campo77, campo99, campo100, Campos_Paso, Id, Campos_Paso_WOW)

            campo = "<LOG grabaXmlToSend_Accrual_Tmed_Wow: END>"
            grabaLogSoa(campo)


            campo = "<LOG ExecutateWsdlSoaRedem: START>"
            grabaLogSoa(campo)

            Dim respuesta_ws = ExecutateWsdlSoaRedem(timeOut_Ws, HttpWRequest, sUri2)
            campo = "<LOG ExecutateWsdlSoaRedem: START " & respuesta_ws & ">"
            grabaLogSoa(campo)

            myUri2 = Nothing
            HttpWRequest = Nothing
            request2 = Nothing
            soapEnvelopeXml2 = Nothing

            Return (respuesta_ws)
        Catch exc As Exception
            campo = "<Error: main_crmsoa_Acreditacion_Tmed: " & exc.ToString() & ">"
            grabaLogSoa(campo)
            Return (campo)
        Finally

        End Try
    End Function
    Public Sub grabaXmlToSend_Accrual_Tmed_Wow(ByVal campo2 As String, ByVal campo3 As String, ByVal campo4 As String, ByVal campo5 As String, ByVal campo6 As String, ByVal campo7 As String, ByVal campo9 As String, ByVal campo10 As String, ByVal Campos_Paso() As String, ByVal Id As String, ByVal Campos_Paso_WOW() As String)
        contador_file = contador_file + 1
        name_file2 = name_file & contador_file.ToString
        Dim oFile As New FileStream(name_file2, FileMode.Create)
        Dim oXML As New XmlTextWriter(oFile, Encoding.UTF8)
        'MessageBox.Show("Acreditaciones")
        oXML.Formatting = Formatting.Indented
        oXML.WriteProcessingInstruction("xml", "version=""1.0""")

        oXML.WriteStartElement("soapenv:Envelope")

        oXML.WriteAttributeString("xmlns:soapenv", campo2)
        oXML.WriteAttributeString("xmlns:als", campo3)
        oXML.WriteAttributeString("xmlns:als1", campo4)

        ''AQUI VA HEADER
        Genera_Header_WS(oXML, campo5, campo6, campo7, campo9, campo10)


        oXML.WriteStartElement("soapenv:Body") ' Abro DataSource.
        oXML.WriteStartElement("als:AccruePointsRequest")

        ''OnLine
        oXML.WriteElementString("als:transactionType", "online")
        oXML.WriteElementString("als:memberNumber", Campos_Paso(2))
        oXML.WriteElementString("als:cardNumber", Campos_Paso(3))
        ''OnLine

        ''Test
        'oXML.WriteElementString("als:transactionType", "online")
        'oXML.WriteElementString("als:memberNumber", "6012578683364132")
        'oXML.WriteElementString("als:cardNumber", "6012578683364132")


        oXML.WriteStartElement("als:transactionsList")


        ''Aqui va registro

        SQLSearch_Acreditaciones_Tmed_Wow(Id, oXML, Campos_Paso_WOW)

        'Dim Inicio As Integer
        'For Inicio = 1 To 2
        'Graba_Acreditacion_WS_Test(oXML)
        'Next Inicio



        'oXML.WriteEndElement() ' Cierro ConnectionProperties.
        oXML.WriteEndElement() ' Cierro DataSource.
        oXML.WriteEndElement() ' Cierro el report.
        oXML.WriteEndElement() ' Cierro el report.
        oXML.WriteEndElement() ' Cierro el report.

        oXML.Flush()
        contador = oXML.BaseStream.Length()
        oFile.Close()

    End Sub
    Private Sub SQLSearch_Acreditaciones_Tmed_Wow(ByVal TicketNumber_ As String, ByRef oXML As XmlTextWriter, ByVal Campos_Paso_WOW() As String)
        Try
            Registro_Log = ("SQLSearch_Acreditaciones_Wow: " & Campos_Paso_WOW(0) & Campos_Paso_WOW(1))
            grabaLogSoa(Registro_Log)
            T_MemberNumber = Campos_Paso_WOW(0)
            T_id = Campos_Paso_WOW(1)
            T_ALSELevelPrice = Campos_Paso_WOW(2)
            T_BranchDivision = Campos_Paso_WOW(3)
            T_ALSEPaymnetMode = Campos_Paso_WOW(4)
            T_ActivityDate = Campos_Paso_WOW(5)
            T_AdjustedListPrice = Campos_Paso_WOW(6)
            T_Amount = Campos_Paso_WOW(7)
            T_Comments = Campos_Paso_WOW(8)
            T_ItemNumber = Campos_Paso_WOW(9)
            T_LocationName = Campos_Paso_WOW(10)
            T_PartnerName = Campos_Paso_WOW(11)
            T_PointName = Campos_Paso_WOW(12)
            T_Points = Campos_Paso_WOW(13)
            T_ProcessDate = Campos_Paso_WOW(14)
            T_ProcessingComment = Campos_Paso_WOW(15)
            T_ProcessingLog = Campos_Paso_WOW(16)
            T_ProductName = Campos_Paso_WOW(17)
            T_Quantity = Campos_Paso_WOW(18)
            T_IntegrationStatus = Campos_Paso_WOW(19)
            T_Status = Campos_Paso_WOW(20)
            T_SubStatus = Campos_Paso_WOW(21)
            T_TicketNumber = Campos_Paso_WOW(22)
            T_TransactionChannel = Campos_Paso_WOW(23)
            T_TransactionDate = Campos_Paso_WOW(24)
            T_TransactionSubType = Campos_Paso_WOW(25)
            T_TransactionType = Campos_Paso_WOW(26)
            T_VoucherNumber = Campos_Paso_WOW(27)
            T_VoucherQty = Campos_Paso_WOW(28)
            T_VoucherType = Campos_Paso_WOW(29)
            T_Organization = Campos_Paso_WOW(30)
            T_Estatus = Campos_Paso_WOW(31)
            T_Bussiness_date = Campos_Paso_WOW(32)
            T_centroconsumo = Campos_Paso_WOW(33)
            T_Cheque = Campos_Paso_WOW(34)
            T_Transmitido = Campos_Paso_WOW(35)
            T_NumeroLineaMicros = Campos_Paso_WOW(36)
            T_Waiter = Campos_Paso_WOW(37)

            Graba_Acreditacion_WS_T_Tmed_Wow(oXML)

        Catch ErroresSQLSearch_Acreditaciones_Wow As Exception
            Registro_Log = "ErroresSQLSearch_Acreditaciones_Wow               :" + ErroresSQLSearch_Acreditaciones_Wow.ToString
            grabaLogSoa(Registro_Log)
        Finally
        End Try
    End Sub
    Private Sub Graba_Acreditacion_WS_T_Tmed_Wow(ByRef oXML As XmlTextWriter)
        ''Registro
        oXML.WriteStartElement("als1:transaction")


        oXML.WriteElementString("als1:adjustedListPrice", T_AdjustedListPrice)

        oXML.WriteElementString("als1:itemNumber", T_ItemNumber)

        ''oXML.WriteElementString("als1:locationId", "")
        oXML.WriteElementString("als1:locationName", T_LocationName)

        T_PartnerName = "Burger King Mexico"

        oXML.WriteElementString("als1:partnerName", T_PartnerName)

        ''oXML.WriteElementString("als1:partnerId", "")

        ''oXML.WriteElementString("als1:pointId", "")

        ''oXML.WriteElementString("als1:pointName", "")

        oXML.WriteElementString("als1:points", "0")

        ''oXML.WriteElementString("als1:postDate", "")

        ''oXML.WriteElementString("als1:processDate", "")

        ''oXML.WriteElementString("als1:productId", "")
        oXML.WriteElementString("als1:productName", T_ProductName)

        ''oXML.WriteElementString("als1:programID", "")

        oXML.WriteElementString("als1:programName", "WOW Rewards")

        If Len(T_Status) = 0 Then
            T_Status = "Acceptable"
        End If

        oXML.WriteElementString("als1:status", T_Status)

        ''oXML.WriteElementString("als1:subStatus", "")
        oXML.WriteElementString("als1:ticketNumber", T_TicketNumber)
        oXML.WriteElementString("als1:transactionChannel", T_TransactionChannel)

        ''oXML.WriteElementString("als1:transactionNumber", "")
        oXML.WriteElementString("als1:transactionDate", T_TransactionDate) ''"10/14/2014 13:18:44")
        oXML.WriteElementString("als1:transactionSubtype", T_TransactionSubType)

        oXML.WriteElementString("als1:transactionType", T_TransactionType)   ''"Acceptable")

        ''oXML.WriteElementString("als1:voucherNumber", "")

        ''oXML.WriteElementString("als1:voucherQuantity", T_VoucherQty)

        ''oXML.WriteElementString("als1:voucherType", "")
        oXML.WriteElementString("als1:amount", T_Amount)
        oXML.WriteElementString("als1:paymentMode", T_ALSEPaymnetMode)

        oXML.WriteElementString("als1:levelPrice", T_ALSELevelPrice)
        oXML.WriteElementString("als1:quantity", T_Quantity)

        ''oXML.WriteElementString("als1:branchDivision", T_BranchDivision)

        ''oXML.WriteElementString("als1:activityDate", "")

        ''oXML.WriteElementString("als1:comments", "")
        oXML.WriteElementString("als1:organization", T_Organization)

        ''oXML.WriteElementString("als1:processingComment", "")

        ''oXML.WriteElementString("als1:processingLog", "")

        oXML.WriteEndElement() ' Cierro ConnectionProperties.
        ''Registro End

    End Sub
    'Acreditacion TMed


    'Odbc
    Private Sub ODBC_ConnectStringMICROS_Dos()
        Try
            If ConexcionMCRSPOS_Dos.State = ConnectionState.Closed Then
                ''ConexcionMCRSPOS.ConnectionString = "DSN=Micros;UID=dba;PWD=smc9nair;Mode=Read"
                'ConexcionMCRSPOS_Dos.ConnectionString = "DSN=Micros;UID=dba;PWD=Db@M@5t3r$;Mode=Read"
                ConexcionMCRSPOS_Dos.ConnectionString = "DSN=Micros;UID=dba;PWD=Password1;Mode=Read"
                ''ConexcionMCRSPOS.ConnectionString = "DSN=Micros;UID=dba;PWD=Micros3998.;Mode=Read"
                ConexcionMCRSPOS_Dos.Open()
            End If
            Permite_Conexcion_CRM_Micros_Dos = True
        Catch Error_ODBC_ConnectStringMICROS_Dos As Exception
            Permite_Conexcion_CRM_Micros_Dos = False
            Registro_Log = "Error_ODBC_ConnectStringMICROS              :" + Permite_Conexcion_CRM_Micros_Dos.ToString + " " + Error_ODBC_ConnectStringMICROS_Dos.ToString
            grabaLogSoa(Registro_Log)
        End Try
    End Sub
    'Odbc


    'GrabaLog
    Public Sub grabaXmlLogSoa(ByVal valor As String)
        Try
            Dim Valor2 As String
            filewrite = File.AppendText(name_file_xml_log)
            Valor2 = "<" & Now.ToLocalTime.ToString() & ">" & valor & ">"
            filewrite.WriteLine(Valor2)
            filewrite.Close()
        Catch ex As Exception
            'ListBox1.Items.Add(ex)
            campo = "<Error: grabaLogSoa: " & ex.ToString() & ">"
        End Try

    End Sub
    Public Sub grabaLogSoa(ByVal valor As String)
        Try
            Dim Valor2 As String
            filewrite = File.AppendText(name_file_log)
            Valor2 = "<" & Now.ToLocalTime.ToString() & ">" & valor & ">"
            filewrite.WriteLine(Valor2)
            filewrite.Close()
        Catch ex As Exception
            'ListBox1.Items.Add(ex)
            campo = "<Error: grabaLogSoa: " & ex.ToString() & ">"
        End Try

    End Sub
    'GrabaLog


End Class


