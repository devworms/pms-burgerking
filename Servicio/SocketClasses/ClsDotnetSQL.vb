Imports System.Data.SqlClient
Public Class ClsDotnetSQL

#Region "Declarations"
	'ADOnet Requisites
	Dim m_DA As New SqlClient.SqlDataAdapter()	  'Bridge of Dataset and DB
	Dim m_CB As SqlClient.SqlCommandBuilder	  'Automation of Update, Insert &  Delete
	Dim m_DS As New DataSet()
	Dim DatRow As DataRow

	'Indicators
	Private m_Addmode As Boolean	 'has user done an AddNew
	Private m_Editmode As Boolean	 'has user tag a record for Delete
	Public m_RecordCount As Integer	'Record Count
	Private m_Index As Integer	  'Index no of row
	Private mblnNoMatch As Boolean	 'result of Find method

	Private m_BOF As Boolean	'Begining of file
	Private m_EOF As Boolean	'End of file

	'Binding Indicators
	Private cBoundControl As New Collection()
	Private cDataField As New SortedList()
	Private cDataFieldType As New SortedList()
	Private cLockOnEdit As New SortedList()

	'Event Declarations:
	Public Event AfterAdd()
	Public Event AfterDelete()
	Public Event AfterEdit()
	Public Event BeforeAdd()
	Public Event BeforeEdit()
	Public Event MoveComplete()
	Public Event AfterCancelUpdate()
	Public Event BeforeCancelUpdate()
#End Region

#Region "Connection Methods"

	Public Function Open(ByVal strSQL As String, ByVal ConnObj As SqlClient.SqlConnection) As Boolean
		Dim objCMD = New SqlClient.SqlCommand(strSQL, ConnObj)
		m_DA = New SqlClient.SqlDataAdapter(objCMD)
		m_DA.SelectCommand = New SqlClient.SqlCommand(strSQL, ConnObj)
		m_CB = New SqlClient.SqlCommandBuilder(m_DA)
		Try
			m_DA.FillSchema(m_DS, SchemaType.Source, "MyTable")
			m_DA.Fill(m_DS, "MyTable")			'set the RecordCount property			m_RecordCount = m_DS.Tables("MyTable").Rows.Count()			If m_RecordCount > 0 Then				m_Index = 0				'When the DataSet is made, the record index should be 0			Else				m_Index = -1			End If			Return True
		Catch
			MsgBox("Could not open the RecordSet !  Error: " & Err.Description)
		Finally
			objCMD = Nothing
		End Try
	End Function

	Public Sub Close()
		'Close it out.
		m_DS.Dispose()
		m_DA.Dispose()
		m_CB.Dispose()
	End Sub

#End Region

#Region "Record Methods"

	Public Sub Load(Optional ByVal firstrec As Boolean = False)
		If firstrec Then
			MoveFirst()
		Else
			MoveLast()
		End If
		Lockcontrols(False)
	End Sub

	Public Sub MoveFirst()
		If m_RecordCount > 0 Then
			Try
				m_Index = 0
				DatRow = m_DS.Tables("MyTable").Rows(m_Index)
				BindDatatoControl()
				RaiseEvent MoveComplete()
			Catch err As Exception

			End Try

		Else
			'do nothing
		End If
	End Sub

	Public Sub MoveNext()
		If Me.m_EOF = True Or m_RecordCount <= 0 Then
			'do nothing
		Else
			Try
				If Not m_Index = m_RecordCount - 1 Then
					m_Index += 1
					DatRow = m_DS.Tables("MyTable").Rows(m_Index)
					BindDatatoControl()
					RaiseEvent MoveComplete()
				End If
			Catch err As Exception

			End Try
		End If
	End Sub

	Public Sub MovePrevious()
		If Me.m_BOF = True Or m_RecordCount <= 0 Then
			'do nothing
		Else
			Try
				If Not m_Index = 0 Then
					m_Index -= 1
					DatRow = m_DS.Tables("MyTable").Rows(m_Index)
					BindDatatoControl()
					RaiseEvent MoveComplete()
				End If
			Catch err As Exception

			End Try
		End If
	End Sub

	Public Sub MoveLast()
		If m_RecordCount > 0 Then
			Try
				m_Index = m_RecordCount - 1
				DatRow = m_DS.Tables("MyTable").Rows(m_Index)
				BindDatatoControl()
				RaiseEvent MoveComplete()
			Catch err As Exception

			End Try
		Else

		End If
	End Sub

	Public Sub AddNew()
		Dim i As Integer
		RaiseEvent BeforeAdd()

		Try
			Lockcontrols(True)
			m_Addmode = True			'Needed for raising events
			DatRow = m_DS.Tables("MyTable").NewRow
			PopulateNew()
			m_DS.Tables("MyTable").Rows.Add(DatRow)
			m_Index = m_RecordCount
			m_DA.Update(m_DS, "MyTable")
			m_RecordCount = m_DS.Tables("MyTable").Rows.Count()

			MoveLast()
			ClearItems()
		Catch err As SqlException
			MsgBox(err.Message)
		Catch err As Exception
			MsgBox(err.Message)
		End Try
	End Sub

	Public Sub Delete()
		Try
			DatRow.Delete()
			m_DA.Update(m_DS, "MyTable")
			m_RecordCount = m_DS.Tables("MyTable").Rows.Count()
			MoveLast()
		Catch err As SqlException
			MsgBox(err.Message)
		Catch err As Exception
			MsgBox(err.Message)
		End Try

	End Sub

	Public Sub cancel()
		If m_Addmode = True Then
			Try
				DatRow.Delete()
				m_DA.Update(m_DS, "MyTable")
				m_RecordCount = m_DS.Tables("MyTable").Rows.Count()
				Lockcontrols(False)
				MoveLast()
			Catch err As SqlException
				MsgBox(err.Message)
			Catch err As Exception
				MsgBox(err.Message)
			End Try

		Else
			MoveLast()
		End If
	End Sub

	Public Sub edit()
		Try
			RaiseEvent BeforeEdit()
			m_Addmode = False
			m_Editmode = True
			Lockcontrols(True)
		Catch err As Exception
			MsgBox(err.Message)
		End Try

	End Sub

	Public Sub save()
		Try
			If m_Addmode = True Then
				Try
					BindControltoData()
					m_DA.Update(m_DS, "MyTable")
					m_RecordCount = m_DS.Tables("MyTable").Rows.Count()
					RaiseEvent AfterAdd()
				Catch err As SqlException
					MsgBox(err.Message)
				Catch err As Exception
					MsgBox(err.Message)
				End Try

			Else
				Try
					BindControltoData()
					m_DA.Update(m_DS, "MyTable")
					m_RecordCount = m_DS.Tables("MyTable").Rows.Count()
					RaiseEvent AfterEdit()
				Catch err As SqlException
					MsgBox(err.Message)
				Catch err As Exception
					MsgBox(err.Message)
				End Try

			End If
		Catch err As SqlException
			MsgBox(err.Message)
		Catch err As Exception
			MsgBox(err.Message)
		Finally
			Lockcontrols(False)
		End Try
	End Sub

#End Region

#Region "Binding Methods"

	Public Sub AddBoundControl(ByVal objControl As System.Windows.Forms.Control, ByVal DataFieldIndex As Object, ByVal DataFieldtype As SqlDbType)
		cBoundControl.Add(objControl)
		cDataField.Add(DataFieldIndex, objControl)
		cDataFieldType.Add(DataFieldIndex, DataFieldtype)
	End Sub

	Sub BindDatatoControl()
		Dim i As Integer
		If m_Addmode = True Then
			'If we are adding a new record we do not have to set mobjRow here again
			Try
				DatRow = m_DS.Tables("MyTable").Rows(m_Index)
			Catch err As SqlException
				MsgBox(err.Message)
			Catch err As Exception
				MsgBox(err.Message)
			End Try
		End If
		Try
			For i = 1 To cBoundControl.Count
				If cBoundControl(i).GetType.ToString = "System.Windows.Forms.TextBox" Then
					cBoundControl(i).text = DatRow.Item(i - 1)
				ElseIf cBoundControl(i).GetType.ToString = "System.Windows.Forms.Combobox" Then
					cBoundControl(i).text = DatRow.Item(i - 1)
				ElseIf cBoundControl(i).GetType.ToString = "System.Windows.Forms.DateTimePicker" Then
					If IsDBNull(DatRow.Item(i - 1)) = True Then
						cBoundControl(i).value = "1/1/1753"
					Else
						cBoundControl(i).value = DatRow.Item(i - 1)
					End If

				ElseIf cBoundControl(i).GetType.ToString = "System.Windows.Forms.CheckBox" Then
					If IsDBNull(DatRow.Item(i - 1)) = True Then
						cBoundControl(i).checkstate = 0
					Else
						If DatRow.Item(i - 1) = False Then						'Bit DataType
							cBoundControl(i).checkstate = 0
						ElseIf DatRow.Item(i - 1) = True Then						'Bit DataType
							cBoundControl(i).checkstate = 1
						Else
							cBoundControl(i).checkstate = DatRow.Item(i - 1)							'Tiny INt
						End If
					End If
				End If
			Next
		Catch err As Exception
			MsgBox(err.Message)
		End Try

	End Sub

	Sub BindControltoData()
		Dim i As Integer
		If m_Addmode = False Then
			'If we are adding a new record we do not have to set mobjRow here again
			DatRow = m_DS.Tables("MyTable").Rows(m_Index)
		End If
		Try
			For i = 1 To cBoundControl.Count
				If cBoundControl(i).GetType.ToString = "System.Windows.Forms.TextBox" Then
					DatRow.Item(i - 1) = cBoundControl(i).Text
				ElseIf cBoundControl(i).GetType.ToString = "System.Windows.Forms.Combobox" Then
					DatRow.Item(i - 1) = cBoundControl(i).Text
				ElseIf cBoundControl(i).GetType.ToString = "System.Windows.Forms.DateTimePicker" Then
					DatRow.Item(i - 1) = cBoundControl(i).Value
				ElseIf cBoundControl(i).GetType.ToString = "System.Windows.Forms.CheckBox" Then
					DatRow.Item(i - 1) = cBoundControl(i).checkstate
				End If
			Next
		Catch err As Exception
			MsgBox(err.Message)
		End Try

	End Sub


#End Region

#Region "ControlsManipulation"

	Sub Lockcontrols(ByVal Addedit As Boolean)
		Dim i As Integer
		Dim FormControl As System.Windows.Forms.Control

		If Addedit = True Then
			For Each FormControl In Me.cBoundControl
				If FormControl.GetType.ToString = "System.Windows.Forms.TextBox" Then
					FormControl.BackColor = System.Drawing.Color.White
					FormControl.Enabled = True
				ElseIf FormControl.GetType.ToString = "System.Windows.Forms.Combobox" Then
					FormControl.BackColor = System.Drawing.Color.White
					FormControl.Enabled = True
				ElseIf FormControl.GetType.ToString = "System.Windows.Forms.DateTimePicker" Then
					FormControl.BackColor = System.Drawing.Color.White
					FormControl.Enabled = True
				ElseIf FormControl.GetType.ToString = "System.Windows.Forms.CheckBox" Then
					FormControl.Enabled = True
				End If
			Next
		Else
			For Each FormControl In Me.cBoundControl
				If FormControl.GetType.ToString = "System.Windows.Forms.TextBox" Then
					FormControl.Enabled = False
					FormControl.BackColor = System.Drawing.Color.LemonChiffon
				ElseIf FormControl.GetType.ToString = "System.Windows.Forms.Combobox" Then
					FormControl.Enabled = False
					FormControl.BackColor = System.Drawing.Color.LemonChiffon
				ElseIf FormControl.GetType.ToString = "System.Windows.Forms.DateTimePicker" Then
					FormControl.Enabled = False
					FormControl.BackColor = System.Drawing.Color.LemonChiffon
				ElseIf FormControl.GetType.ToString = "System.Windows.Forms.CheckBox" Then
					FormControl.Enabled = False
				End If
			Next

		End If
	End Sub

	Sub PopulateNew()
		Dim i As Integer

		For i = 1 To cBoundControl.Count
			Try
				If cDataFieldType.Item(i - 1) = 2 Then				'Bit
					DatRow.Item(i - 1) = 0
				ElseIf cDataFieldType.Item(i - 1) = 4 Then				'Datetime
					DatRow.Item(i - 1) = Format(Now, "MM/dd/yyyy hh:mm")
				ElseIf cDataFieldType.Item(i - 1) = 7 Then				'Image
					DatRow.Item(i - 1) = ""
				ElseIf cDataFieldType.Item(i - 1) = 8 Then				'INTeger
					DatRow.Item(i - 1) = "0"
				ElseIf cDataFieldType.Item(i - 1) = 9 Then				'Money
					DatRow.Item(i - 1) = "0.00"
				ElseIf cDataFieldType.Item(i - 1) = 12 Then				'nvarchar
					DatRow.Item(i - 1) = "Null "
				ElseIf cDataFieldType.Item(i - 1) = 13 Then				'real
					DatRow.Item(i - 1) = "0.00"
				ElseIf cDataFieldType.Item(i - 1) = 14 Then				'small date
					DatRow.Item(i - 1) = Format(Now, "MM/dd/yyyy")
					DatRow.Item(i - 1) = Format(Now, "hh:mm")
				ElseIf cDataFieldType.Item(i - 1) = 18 Then				 'Tiny INt
					DatRow.Item(i - 1) = 0
				End If
			Catch err As Exception
				MsgBox(err.Message)

			End Try
		Next
	End Sub

	Sub ClearItems()
		Dim i As Integer
		For i = 1 To cBoundControl.Count
			If cBoundControl(i).GetType.ToString = "System.Windows.Forms.TextBox" Then
				cBoundControl(i).Text = ""
			ElseIf cBoundControl(i).GetType.ToString = "System.Windows.Forms.Combobox" Then
				cBoundControl(i).Text = ""
			ElseIf cBoundControl(i).GetType.ToString = "System.Windows.Forms.DateTimePicker" Then
				cBoundControl(i).Value = Now
			ElseIf cBoundControl(i).GetType.ToString = "System.Windows.Forms.CheckBox" Then
				cBoundControl(i).checkstate = 0
			End If
		Next

	End Sub

#End Region

End Class
