Imports System.ServiceProcess
Imports System.Timers
Imports System.IO
Imports System.Data.Odbc
'Imports System.Web.Mail
Imports System.Text
Imports System.Xml
Imports System.Net
Imports CrmSoa

Public Class ServicioCrmSoa
    Inherits System.ServiceProcess.ServiceBase
    Private WithEvents server As New Irc.Networking.ServerSocket
    Dim CrmSoaw As New CrmSoa.CrmSoaDll

    ''08Abr2015
    Public Inicio_Voucher, Final_Vocuher As Integer
    Dim validFrecAttibuteType, minimumAmount As String
    ''08Abr2015

    Dim validDiscountAmount, validDiscountType, lastName As String
    '''FILES Const
    Public Const FileEjecutaProgramaJava As String = "execute.Bat"
    Public Const FileConfiguracion_CRM As String = "D:\MICROS\Res\Pos\Etc\Configuracion_CRM.TxT"
    Public File_Log_Redenciones_CRM As String = "CRM_Redenciones_Generadas_OffLine.TxT"
    Public Const Slash As String = "|"
    Public string_xml, entidad, campo_tarjeta, errorCode As String

    '23Nov2013
    Public campos_Devolucion(9999) As String
    Public campos_Reverso(9999) As String
    Public campos_Consulta(9999) As String
    '23Nov2013

    Public iad, applabel, no_serie_1, campo_hex, caracter_str As String
    Public txtFileNameLog, txtFileNameRedencionesLog, TicketNumber, MemberNumber As String
    Public Campo01, Campo02, Cadena, Campo As String
    Public Registro_Log As String
    Public Cadena_Inicio As String
    Dim Cadena_Inicio2 As Char()
    Public Socket_Peticion As String
    Dim Campos(), Beneficios(10) As String
    Public Datos, Nombre As String
    Public Respuesta, Respuesta1, Respuesta2, Respuesta3 As String
    Public Encabezado As String
    Public Datos1 As String
    Public Trailer As String
    Public Const Separador As String = "|"
    Public Numero_WSID_A, Card_Status, LifetimePoint1Value, LifetimePoint2Value, SubStatus, Status, Status2, pointAmount As String
    Public Lectura(10) As String
    Public Marca, numero_Miembro, Jar_Ejecutar As String
    Public Path_Entrada, Path_Salida, DeleteTicket, InsertTicket, ReadSql, Last_Beverage, Last_Food, AttributeValue As String
    Public Business_date, Tienda, CentroConsumo, Cheque, ALSEFavoriteProduct As String
    Public ListOfLOYMember_Contact, BirthDate, Archivo_Trabajar, Numero_Ticket_Void, Estatus, IntegrationStatus, StatusObject As String
    Public Cheques_Lotes(1000) As String

    '''''''Integer
    Public numero_Interface, Inicio_caracter, contador1, caracter_int, contador_hex, contador_hex2 As Integer
    Public size_17, size_18, size_19, size_20, size_31, size_sFlag, numeroCampos As Integer
    Public Inicio_Encabezado As Integer
    Public Fin_Encabezado As Integer
    Public Inicio_Datos As Integer
    Public Fin_Datos As Integer
    Public Inicio_Trailer As Integer
    Public Fin_Trailer As Integer
    Public Inicio_Separador As Integer
    Public Fin_Separador As Integer
    Public Longitud, Inicio_L, Inicio_L2, Inicio_L3, Inicio_L4, Final_L, Longitud_L, Inicio_M, Indice2 As Integer
    Public Contador_Datos As Integer
    Dim PuertoID As Integer
    Public Inicio, Inicio2, Inicio3 As Integer
    Dim Timeout_Ifc As Integer
    Public Contador_Registros_Lote As Integer

    '''Int32
    Public Timer_Lectura_Files_Fix As Int32

    ''' Int64
    Public Numero_WSID As Int64
    Public Timeout_Ifc_Ws As Int64

    ''' Boolean
    Public Permite_Conexcion As Boolean
    Public Permite_Conexcion_MCRSPOS As Boolean
    Public Permite_Conexcion_CRM As Boolean
    Public Salir, Delete_Datos_CRM, Insert_Datos_CRM, Permite_Conexcion_CRM_Micros, Permite_Conexcion_CRM_Micros_Dos, Permite_Conexcion_CRM_Micros_Tres, Permite_Conexcion_Lotes_Micros As Boolean
    Public Servicio_Micros_Start, Servicio_Micros_First As Boolean

    ''' StreamReader and StreamWriter
    Dim myStreamReader As StreamReader
    Dim myStreamWriter As StreamWriter

    ''' ConexcionSybase odbc
    Dim ConexcionSybase As New OdbcConnection
    Dim Conexcion As New OdbcConnection
    Dim ReadSql_odbc As OdbcDataReader
    Dim ReadSql_odbc_Dos As OdbcDataReader
    Dim ConexcionMCRSPOS As New OdbcConnection
    Dim ConexcionMCRSPOS_Dos As New OdbcConnection
    Dim ConexcionMCRSPOS_Tres As New OdbcConnection

    Dim Campos_Paso(), Campos_Paso_WOW() As String

    Public Nivel, Organizacion, NivelId, NivelNombre, Promociones(20), Voucher(20) As String
    Public PromocionId, PromocionName, PromocionUniqueKey, PromocionDisplayName, PromocionStatus As String
    Public Contador_Promociones, Contador_Voucher As Integer
    Public VoucherCalStatus, VoucherProductId, VoucherProductName, VoucherStatus, VoucherTransactionId As String

    Public Path_WS, Nombre_Ws, Respuesta22, Campo4_Wow As String
    Public Const FileRutaCRM As String = "C:\PathCRM.TxT"

    Public Mail_Remitente, Mail_Destinatario, Mail_Remitente_Password, Procesado_Nivel As String


    ''Dim LeeTarjeta As New vx810v1.LeeBandaChip
    '06ago2012 Dim Lee1 As New Nurit293v30.Transacciones

    '06ago2012 Dim LeeTarjeta_Nurit293v30 As New Nurit293v30.Transacciones
    '06ago2012 Dim LeeTarjeta_Nurit293v32 As New Nurit293v32.LeeBandaChip

    ''Nuevas
    Dim Conexcioncfd_Facturacion As New OdbcConnection
    Dim Conexcion_odbc As New OdbcConnection
    Dim InstrSql As String
    Public Invoice_Add, Log_Add As Boolean
    Public Serie As String
    Public poolId, montoPesos, montoPuntos, montoTotal, leyenda1, leyenda2, saldoAnteriorPuntos, saldoAnteriorPesos, saldoRedimidoPuntos, saldoRedimidoPesos, saldoActualPuntos, saldoActualPesos, vigenciaPuntos As String
    Public Permite_Conexion_Lealtad As Boolean
    Public II As Integer
    Public Separador1, No_Tarjeta_A, Bines_Lealtad As String
    Public imprimir, autorizacion, mensaje, razonSocial, comercio, direccion, Bines(200) As String
    Public fhTransaccion, tarjeta, fhVencimiento, emisor, operacion, monto_A, status_code, Monto_Puntos As String
    Public ingreso, criptograma, Hora_A, firma, Tipo_Tarjeta, cd_respuesta, code, s_pago, Log_Field As String
    ''Nuevas

    Public No_Tarjeta, Titular, Fecha_Vencimiento, CVV2, Digest, Digest2, Mensaje_Pinpad, Mensaje_Pinpad2, Mensaje_Pinpad3, Mensaje_Pinpad_All, Mensaje_Pinpad_All1, Consecutivo As String
    Public Secuencia_transmision, Referencia, Tipo_Servicio, Anio, Mes As String
    Public Numero_Tarjeta, Email_cliente, Telefono_Cliente, Identificador_Cliente As String
    Public Tipo_TDC, Tipo_Pago, Track1, Track1_A, Track2, Track2_A, Tipo_Plan, Periodo_Financiamiento, sFlag As String
    Public Email_Administrador, Email_Transaccion, Afiliacion, Plataforma As String
    Public Tag_5F34, Tags_EMV, Bandera_Firma, Tag_9F26, Tag_9F27, Pagare_Pinpad, Flag As String
    Public Business_Unit, Codigo_Moneda, Enverioment As Integer
    Public Monto As Decimal
    Public Respuesta_Autorizacion As Boolean
    Public Respuesta_Autorizacion_VisaMaster, Respuesta_Bines_VisaMaster As String
    Public Respuesta_Autorizacion_Amex As String
    Public Titulares() As String
    'DLL PINPAD

    'New
    Public Valor0, Respuesta11(), Respuesta_ws As String
    'New
    'WS


    'Dim sUri As String = "https://alsea-soatest.oracleoutsourcing.com:443/Services/Loyalty/ALSEAMemberService"
    'Dim sUri As String = "https://alsea-soadev.oracleoutsourcing.com/Services/Loyalty/ALSEAMemberService"
    'Dim sUri As String = "https://alsea-soaprod.oracleoutsourcing.com/Services/Loyalty/ALSEAMemberService"


    'Dim sUri As String = "https://soatest.grupoalsea.com.mx/Services/Loyalty/ALSEAMemberService"

    'Dim sUri As String = "https://alsea-soadev.oracleoutsourcing.com:443/Services/Loyalty/ALSEAMemberService" 'Desarrollo
    'Dim sUri As String = "https://soatest.grupoalsea.com.mx/Services/Loyalty/ALSEAMemberService"  'TEST
    Dim sUri As String = "https://soaprod.grupoalsea.com.mx/Services/Loyalty/ALSEAMemberService"  'PRODUCCION


    Const campo2 As String = "http://schemas.xmlsoap.org/soap/envelope/"
    Const campo3 As String = "http://services.alsea.com/Loyalty/Schema/ALSEAMemberServiceMsg"
    Const campo4 As String = "http://services.alsea.com/Loyalty/Schema/ALSEATransaction"

    Const campo5 As String = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"
    Const campo6 As String = "soap"

    Const campo7 As String = "UsernameToken-XAqqET7bPF88fgxQD4ytyw22"

    Const campo9 As String = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"

    Const campo10 As String = "<wsse:Password Type=" & Chr(34) & "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText" & Chr(34) & ">L0y417y.</wsse:Password>"

    Const timeOut_Ws As Integer = 30000

    ''OK Dim webRequest As HttpWebRequest = CType(Net.WebRequest.Create(sUri), HttpWebRequest)
    '26Dic2014 Dim myUri As System.Uri = New System.Uri(sUri)
    '26Dic2014 Dim HttpWRequest As HttpWebRequest = WebRequest.Create(myUri)

    '26Dic2014 Dim oFile As New FileStream("Archivo.xml", FileMode.Create)


    Dim xml_valor, directorio_file, readXml, _xml As String
    Dim contador As Integer
    Dim ds As New DataSet
    Dim filewrite As StreamWriter

    Dim statusCode, ResponseText, statusdescription, Numero_tarjetas, numeroCliente, faultstring As String
    Dim tienda_capturada, errorDescription, detail, Id, Id2 As String

    Dim SendResult As Boolean
    Dim SendStatus As Integer
    'WS

    Dim error_ws, name_file2 As String
    Const FileConfiguracion_Url As String = "C:\urlCrmSoa.TxT"
    Dim urlLeida As String
    Const name_file_xml_log As String = "C:\CrmSoa\Xml_Log_Crm_Soa.txt"
    Const name_file_log As String = "C:\CrmSoa\Log_Crm_Soa.txt"


#Region " Component Designer generated code "

    Public Sub New()
        MyBase.New()

        ' This call is required by the Component Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call

    End Sub

    'UserService overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    ' The main entry point for the process
    <MTAThread()> _
    Shared Sub Main()
        Dim ServicesToRun() As System.ServiceProcess.ServiceBase

        ' More than one NT Service may run within the same process. To add
        ' another service to this process, change the following line to
        ' create a second service object. For example,
        '
        '   ServicesToRun = New System.ServiceProcess.ServiceBase () {New Service1, New MySecondUserService}
        '
        ServicesToRun = New System.ServiceProcess.ServiceBase() {New ServicioCrmSoa}

        System.ServiceProcess.ServiceBase.Run(ServicesToRun)
    End Sub

    'Required by the Component Designer
    Private Tiempo As Timers.Timer
    Private components As System.ComponentModel.IContainer
    Private timeStart As DateTime          ' Used to note the start time of the service
    Private timeEnd As DateTime            ' Used to note the end time of the service
    Private timeElapsed As New TimeSpan(0) ' Initialize to 0
    Private timeDifference As TimeSpan
    Private isPaused As Boolean = False    ' Notes whether the service is paused
    Private Error_Field, Vacio, Field_Log As String
    Private File_Error, File_Lee_Error, File_Contador, File_Terminales, File_Operations, File_Log, File_Log_Bak, File_Uws As String
    Private Respuesta_N, rowCount, rowCount0, UWS_Reset, Process_To_Kill, Resp, I, Hora_Delete, Minuto_Delete As Integer
    Dim Numero_Uws(100), Numero_Proceso(100) As Integer
    Private Flag_Operations, Flag_Borrar_Archivo As Boolean
    '''NEW
    '''NEW
    '''NOTE: The following procedure is required by the Component Designer
    '''It can be modified using the Component Designer.  
    '''Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        '
        'Servicio Crm Soa
        '
        Me.CanHandlePowerEvent = True
        Me.CanShutdown = True
        Me.ServiceName = "Servicio Crm Soa"

    End Sub

#End Region

    '''PROCESS SERVICES
    Protected Overrides Sub OnStart(ByVal args() As String)
        Try
            Tiempo = New Timers.Timer
            AddHandler Tiempo.Elapsed, AddressOf TimerFired
            Tiempo.Enabled = False
            Tiempo.Stop()
            AutoLog = True
            Servicio_Micros_Start = False
            Servicio_Micros_First = False
            'System.Threading.Thread.CurrentThread.Sleep(180000)
            'SQLReaderTimeout_interface_micros()
            'If Permite_Conexcion_MCRSPOS = False Then
            'SQLReaderTimeout_Facturacion()
            'Dispose(True)
            'Else
            'ReadFile()
            '06ago2012 Solicita_Path_Micros()
            '06ago2012 ReadFilePathMicros()
            Calcula_Tiempo_Servicio()
            'Abre_Puerto_Comunicacion()
            'End If
            'Shell("C:\Program Files\SetupServicioCrmSoa\Socket\cs.exe", AppWinStyle.Hide, False)
        Catch exc As Exception
            Error_Field = "OnStart: " & exc.Message
            Write_event_log_error_file(Error_Field)
        Finally
        End Try
    End Sub
    Protected Overrides Sub OnStop()
        Try
            Error_Field = "OnStop: " & "** OnStop: "
            Write_event_log_error_file(Error_Field)
            'Cierra_Puerto_Comunicacion()
            'kill_proceso()
            Tiempo.Dispose()
            AutoLog = False
            Procesado_Nivel = ""
        Catch exc As Exception
            Error_Field = exc.Message
            Write_event_log_error_file(Error_Field)
        Finally
        End Try
    End Sub
    Protected Overrides Function OnPowerEvent(ByVal powerStatus As System.ServiceProcess.PowerBroadcastStatus) As Boolean
        Try
            Error_Field = "OnPowerEvent: " & "** OnStop: "
            Write_event_log_error_file(Error_Field)
            Tiempo.Stop()
            Tiempo.Dispose()
            AutoLog = False
        Catch exc As Exception
            Error_Field = exc.Message
            Write_event_log_error_file(Error_Field)
        Finally
        End Try
    End Function
    Private Sub TimerFired(ByVal sender As Object, ByVal e As ElapsedEventArgs)
        Try
            'Error_Field = "**TimerFired Inicia: "
            'Write_event_log_error_file(Error_Field)
            Tiempo.Enabled = False
            Tiempo.Stop()
            '20Dic2011 Procesado_Nivel = ""
            Detect_Servicio_Micros()
            If Servicio_Micros_Start = True Then
                If Servicio_Micros_First = False Then
                    'SQLReaderTimeout_interface_micros()
                    'If Permite_Conexcion_MCRSPOS = True Then
                    'ReadFile()
                    'Calcula_Tiempo_Servicio()
                    'Abre_Puerto_Comunicacion()
                    Error_Field = "**TimerFired True: " + "  " + Servicio_Micros_First.ToString + " " + Servicio_Micros_Start.ToString
                    Write_event_log_error_file(Error_Field)
                    Proceso_PrincipalTimerFired()
                    Servicio_Micros_First = True

                    'Else
                    'Servicio_Micros_First = False
                    'Else
                    'Error_Field = "**TimerFired False: " + "  " + Servicio_Micros_First.ToString + " " + Servicio_Micros_Start.ToString
                    'Write_event_log_error_file(Error_Field)

                End If
            Else
                If Servicio_Micros_First = True Then
                    Error_Field = "Servicio_Micros_Start false y Servicio_Micros_First true: Cierra Puerto"
                    Write_event_log_error_file(Error_Field)
                    'Cierra_Puerto_Comunicacion()
                End If
            End If

            'If Servicio_Micros_First = True Then
            'Proceso_PrincipalTimerFired()
            'End If
            'End If
        Catch exc As Exception
            Error_Field = "**TimerFired Inicia. Exception: " + exc.Message
            Write_event_log_error_file(Error_Field)
        Finally
            '21Dic2011 Proceso_Main_Busqueda_Nivel()
            Calcula_Tiempo_Servicio()
        End Try
    End Sub
    Sub Detect_Servicio_Micros()
        Try
            Dim Computername As String = System.Net.Dns.GetHostName
            Dim controller As New ServiceController
            Dim status As String
            controller.MachineName = "."
            controller.ServiceName = "SQLANYs_sql" & Computername
            status = controller.Status.ToString
            controller.Refresh()
            If status = "Running" Then
                Servicio_Micros_Start = True
            Else
                Servicio_Micros_Start = False
            End If
            'Error_Field = "**Detect_Servicio_Micros: " + "  " + controller.ServiceName.ToString + " " + status + " " + Servicio_Micros_Start.ToString
            'Write_event_log_error_file(Error_Field)
        Catch Error_Detect_Servicio_Micros As Exception
            Error_Field = "**Error_Detect_Servicio_Micros: " + "  " + Error_Detect_Servicio_Micros.ToString
            Write_event_log_error_file(Error_Field)
        End Try
    End Sub

    '''MAIN PROCESS
    Private Sub Proceso_PrincipalTimerFired()
        Try
            SQLReaderTimeout_interface_micros_CRM()
            If Permite_Conexcion_MCRSPOS = False Then
                SQLReaderTimeout_interface_micros_CRM()
            End If
            ReadFile()
            'Calcula_Tiempo_Servicio()
            Abre_Puerto_Comunicacion()
            Error_Field = "**Proceso_Principal. PROCESS MAIN: "
            Write_event_log_error_file(Error_Field)
        Catch exc As Exception
            Error_Field = "**Proceso_Principal. Exeception: " & exc.Message
            Write_event_log_error_file(Error_Field)
        Finally
        End Try
    End Sub

    Private Sub Calcula_Tiempo_Servicio()
        Try
            With Tiempo
                .Interval = 1
                .AutoReset = True
                .Enabled = True
                .Start()
            End With
        Catch exc As Exception
            Error_Field = exc.Message
            Write_event_log_error_file(Error_Field)
        Finally
        End Try
    End Sub

    '''SHOW MESSAGE ON SERVER
    Public Sub Muestra_Mensaje_Server()
        Try
            'Dim current As Thread = Thread.CurrentThread
            'MsgBox(current.Name, MsgBoxStyle.MsgBoxSetForeground, "Facturacion Electronica: ")
        Catch Errores_Muestra_Mensaje_Server As Exception
            Registro_Log = "Errores_Muestra_Mensaje_Server        :" + Errores_Muestra_Mensaje_Server.ToString
            'AppendLog(Registro_Log)
        Finally
        End Try
    End Sub

    ''' MAIN COMUNICATIONS
    Private Sub Abre_Puerto_Comunicacion()
        Try
            PuertoID = 5201
            If Not server.Listening() Then
                server.Listen(Int32.Parse(Convert.ToString(PuertoID)))
                Error_Field = "Abre_Puerto_Comunicacion: " & PuertoID.ToString
                Write_event_log_error_file(Error_Field)
            Else
            End If
        Catch Error_Abre_Puerto_Comunicacion As Exception
            Error_Field = "**Error_Abre_Puerto_Comunicacion: " & Error_Abre_Puerto_Comunicacion.Message
            Write_event_log_error_file(Error_Field)
        Finally
        End Try
    End Sub
    Private Sub server_ClientConnectionClosed(ByVal sender As Object, ByVal e As Irc.Networking.ServerSocket.ServerSocketEventArgs) Handles server.ClientConnectionClosed
        Try
            Error_Field = "server_ClientConnectionClosed      :" + e.ClientSocket.ToString
            Write_event_log_error_file(Error_Field)
            'New lbConnectedClients.Items.Remove(e.ClientSocket)
        Catch Errores_ClientConnectionClosed As Exception
            Error_Field = Errores_ClientConnectionClosed.Message
            Write_event_log_error_file(Error_Field)
            'New MessageBox.Show("Error ClientConnectionClosed", "Error cerrando Socket", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally

        End Try

    End Sub
    Private Sub server_ClientConnectSuccess(ByVal sender As Object, ByVal e As Irc.Networking.ServerSocket.ServerSocketEventArgs) Handles server.ClientConnectSuccess
        Try
            Error_Field = "server_ClientConnectSuccess        :" + e.ClientSocket.ToString
            Write_event_log_error_file(Error_Field)
            'New lbConnectedClients.Items.Add(e.ClientSocket)
        Catch Errores_ClientConnectSuccess As Exception
            Error_Field = "Errores_ClientConnectSuccess       :" + e.ClientSocket.ToString + "  " + Errores_ClientConnectSuccess.ToString
            Write_event_log_error_file(Error_Field)
            'New MessageBox.Show("Error ClientConnectSuccess", "Error en Sockets", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally

        End Try

    End Sub
    Private Sub server_ConnectionRequest(ByVal sender As Object, ByVal e As System.EventArgs) Handles server.ConnectionRequest
        Try
            Error_Field = "server_ConnectionRequest           :" + e.GetType.ToString
            Write_event_log_error_file(Error_Field)
            server.Accept()
        Catch Errores_ConnectionRequest As Exception
            Error_Field = "Errores_ConnectionRequest          :" + Errores_ConnectionRequest.ToString
            Write_event_log_error_file(Error_Field)
            'New MessageBox.Show("Error ConnectionRequest", "Error Aceptando Socket", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally

        End Try

    End Sub
    Private Sub Configuracion_crm()
        Dim fileReader As String
        fileReader = My.Computer.FileSystem.ReadAllText("C:\\Program Files (x86)\\Devworms\\SetupCRMWOW\\config.txt")
        SUri = fileReader

    End Sub

    Private Sub server_DataArrivalFromClient(ByVal sender As Object, ByVal e As Irc.Networking.ServerSocket.ServerSocketEventArgs) Handles server.DataArrivalFromClient
        Try
            Configuracion_crm()
            If sUri = "" Or sUri = " " Then
                sUri = "https://soaprod.grupoalsea.com.mx/Services/Loyalty/ALSEAMemberService"
            End If
            Cadena_Inicio = e.ClientSocket.GetArrivedText.ToString
            'Cadena_Inicio2 = e.ClientSocket.GetArrivedText.ToCharArray
            Socket_Peticion = e.ClientSocket.ToString()
            Registro_Log = "server_DataArrivalFromClient <<Cadena_Inicio<<   :" + e.ClientSocket.ToString + "  " + Cadena_Inicio
            AppendLog(Registro_Log)
            'Registro_Log = "server_DataArrivalFromClient <<Cadena_Inicio2<<   :" + e.ClientSocket.ToString + "  " + Cadena_Inicio2(0).ToString + "  " + "  " + Cadena_Inicio2(1).ToString + "  " + Cadena_Inicio2(2).ToString + "  " + Cadena_Inicio2(3).ToString + "  " + Cadena_Inicio2(4).ToString + "  " + Cadena_Inicio2(5).ToString + "  " + Cadena_Inicio2(6).ToString + "  " + Cadena_Inicio2(7).ToString + "  " + Cadena_Inicio2(8).ToString + "  " + Cadena_Inicio2(9).ToString + "  " + Cadena_Inicio2(10).ToString
            'AppendLog(Registro_Log)
            Registro_Log = "server_DataArrivalFromClient <<02<<   :" + e.ClientSocket.GetArrivedText
            AppendLog(Registro_Log)
            Registro_Log = "server_DataArrivalFromClient <<02a<   :" + Socket_Peticion.ToString()
            AppendLog(Registro_Log)
            Registro_Log = "server_DataArrivalFromClient <<02b<   :" + Cadena_Inicio
            AppendLog(Registro_Log)
            Registro_Log = "server_DataArrivalFromClient <<02c<   :" + e.ClientSocket.GetArrivedText.ToString
            AppendLog(Registro_Log)
            If Cadena_Inicio.Length() > 0 Then
                'New tbMessages.Text += "<" + Socket_Peticion + ">" + "Recibo" + vbCrLf

                '24Feb2014
                If Cadena_Inicio.Length() < 33 Then
                    Envia_Respuesta(e, Cadena_Inicio)
                    Exit Sub
                End If
                '24Feb2014

                Separa_Cadena_Datos_Recibida(Cadena_Inicio)
                Campos = Datos.Split("|")

                numeroCampos = (Campos.Length)

                Registro_Log = "server_DataArrivalFromClient <<Campos<<   :" + numeroCampos.ToString + " comando: <" + Campos(0) + ">"
                AppendLog(Registro_Log)

                numeroCampos = (Campos.Length - 1)

                For contador1 = 0 To numeroCampos
                    If contador1 <= numeroCampos Then
                        Registro_Log = "server_DataArrivalFromClient <<Campos<<   :" + contador1.ToString + " valor: <" + Campos(contador1) + ">" + " Len: <" + Campos(contador1).Length.ToString + ">"
                        AppendLog(Registro_Log)
                    End If
                Next

                Registro_Log = "server_DataArrivalFromClient <<Termina campos>>"
                AppendLog(Registro_Log)

                'End If
                'Datos = Encabezado & "RE-INTENTARLO" & Chr(3) & "ABCD" & Chr(4)
                If Mid(Datos, 1, 3) = "CLN" Or Trim(Campos(0)) = "CLN" Or Mid(Datos, 2, 3) = "CLN" Or Datos.IndexOf("CLN") > -1 Then '///Solicita Miembro
                    error_ws = " "
                    numero_Miembro = ""
                    Procesa_Busqueda_Miembro()
                    'Respuesta2 = "PROCESADO: " & Respuesta2
                    Respuesta = Encabezado & Respuesta2 & Trailer
                    Registro_Log = "server_DataASendtoClient <<   :" + Respuesta2
                    'Write_event_log_error_file(Registro_Log)
                    Envia_Respuesta(e, Respuesta)
                    error_ws = " "
                    Respuesta2 = ""
                ElseIf Mid(Datos, 1, 3) = "RED" Or Trim(Campos(0)) = "RED" Or Datos.IndexOf("RED") > -1 Then '///Cancela Ticket
                    '26Ene2012 Procesa_Cancelacion_Ticket()
                    error_ws = " "
                    faultstring = ""
                    'Respuesta2 = "Success|"
                    Procesa_Redencion_Ticket()
                    'Respuesta2 = "PROCESADO: " & Respuesta2
                    ''MessageBox.Show(Respuesta2)
                    If Respuesta2.Length = 0 Then
                        Respuesta2 = "REDENCION OK"
                    End If
                    Respuesta = Encabezado & Respuesta2 & Trailer
                    ''MessageBox.Show(Respuesta)
                    Registro_Log = "server_DataASendtoClient <<   :" + Respuesta
                    'Write_event_log_error_file(Registro_Log)
                    'grabaLogSoa(Respuesta)
                    Envia_Respuesta(e, Respuesta)
                    faultstring = ""
                    error_ws = " "
                    Respuesta2 = ""
                ElseIf Mid(Datos, 1, 3) = "REW" Or Trim(Campos(0)) = "REW" Or Datos.IndexOf("REW") > -1 Then '///Cancela Ticket
                    '26Ene2012 Procesa_Cancelacion_Ticket()
                    error_ws = " "
                    faultstring = ""
                    'Respuesta2 = "Success|"

                    Registro_Log = "REW OK:"
                    grabaLogSoa(Registro_Log)

                    Procesa_Redencion_Ticket_Tmed_Wow()
                    'Respuesta2 = "PROCESADO: " & Respuesta2
                    ''MessageBox.Show(Respuesta2)
                    If Respuesta2.Length = 0 Then
                        Respuesta2 = "REDENCION OK"
                    End If
                    Respuesta = Encabezado & Respuesta2 & Trailer
                    ''MessageBox.Show(Respuesta)
                    Registro_Log = "server_DataASendtoClient <<   :" + Respuesta
                    'Write_event_log_error_file(Registro_Log)
                    'grabaLogSoa(Respuesta)
                    Envia_Respuesta(e, Respuesta)
                    faultstring = ""
                    error_ws = " "
                    Respuesta2 = ""
                ElseIf Mid(Datos, 1, 3) = "OFF" Or Trim(Campos(0)) = "OFF" Or Datos.IndexOf("OFF") > -1 Then  '///////
                    Respuesta = 0
                    Respuesta2 = "PROCESADO:"
                    Respuesta = Encabezado & Separador & Respuesta2 & Trailer
                    'grabaLogSoa(Respuesta)
                    Envia_Respuesta(e, Respuesta)
                    Procesa_Ejecuta_Acreditacion_OffLine()
                    Respuesta2 = ""
                ElseIf Mid(Datos, 1, 3) = "ACR" Or Trim(Campos(0)) = "ACR" Or Datos.IndexOf("ACR") > -1 Then '///Cancela Ticket
                    '26Ene2012 Procesa_Cancelacion_Ticket()
                    error_ws = " "
                    faultstring = ""
                    Procesa_Acreditacion_Ticket()
                    'Respuesta2 = "Success|" & Respuesta2
                    'Respuesta2 = "ACREDITACION OK"
                    Respuesta = Encabezado & Respuesta2 & Trailer
                    Registro_Log = "server_DataASendtoClient <<   :" + Respuesta
                    'Write_event_log_error_file(Registro_Log)
                    'grabaLogSoa(Respuesta)
                    Envia_Respuesta(e, Respuesta)
                    faultstring = ""
                    error_ws = " "
                    Respuesta2 = ""
                ElseIf Mid(Datos, 1, 8) = "NOUSED" Or Trim(Campos(0)) = "NOUSED" Or Datos.IndexOf("NOUSED") > -1 Then '///Tmed Acreditacion
                    error_ws = " "
                    faultstring = ""
                    Procesa_Acreditacion_Ticket_Tmed()
                    Respuesta = Encabezado & Respuesta2 & Trailer
                    Registro_Log = "server_DataASendtoClient <<   :" + Respuesta
                    'Write_event_log_error_file(Registro_Log)
                    'grabaLogSoa(Respuesta)
                    Envia_Respuesta(e, Respuesta)
                    faultstring = ""
                    error_ws = " "
                    Respuesta2 = ""
                ElseIf Mid(Datos, 1, 3) = "VDT" Or Trim(Campos(0)) = "VDT" Or Datos.IndexOf("VDT") > -1 Then '///Cancela Ticket
                    '26Ene2012 Not Used on SBUX
                    error_ws = " "
                    faultstring = ""
                    'Respuesta2 = "Success|" & Respuesta2
                    Respuesta = Encabezado & Respuesta2 & Trailer
                    Envia_Respuesta(e, Respuesta)
                    Procesa_Cancelacion_Ticket()
                    faultstring = ""
                    error_ws = " "
                    Respuesta2 = ""

                    'New
                    'New
                    'New

                End If
                Borra_Variables()
            End If
        Catch Errores_DataArrivalFromClient As Exception
            Registro_Log = "Errores_DataArrivalFromClient      :" + Errores_DataArrivalFromClient.ToString
            Respuesta = Encabezado & Separador & "ERROR DATOS" & Trailer
            AppendLog(Registro_Log)
            'New MessageBox.Show("Error DataArrivalFromClient", "Datos Recibido Incorrectos", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
        End Try
    End Sub

    Private Sub Abre_Cierra_Socket()
        Try
            PuertoID = 5201
            If Not server.Listening() Then
                If Permite_Conexcion = True Then
                    Registro_Log = "Abre_Cierra_Socket                 :" + Convert.ToString(PuertoID)
                    AppendLog(Registro_Log)
                    server.Listen(Int32.Parse(Convert.ToString(PuertoID)))
                Else
                    Registro_Log = "Abre_Cierra_Socket                 :" + "No se Puede Abrir Puerto de Comunicacion  " + Convert.ToString(PuertoID)
                End If
            Else
                Registro_Log = "Abre_Cierra_Socket                 :" + "Puerto Cerrado  " + Convert.ToString(PuertoID)
                AppendLog(Registro_Log)
                server.Close()
            End If
        Catch Error_Abre_Cierra_Socket As Exception
            Error_Field = "Error_Abre_Cierra_Socket           :" + Convert.ToString(PuertoID) + "  " + Error_Abre_Cierra_Socket.ToString
            Write_event_log_error_file(Error_Field)
        Finally
        End Try
    End Sub
    Private Sub Cierra_Puerto_Comunicacion()
        Try
            If server.Listening() Then
                server.Close()
                'New lbConnectedClients.ClearSelected()
                Registro_Log = "Cierra_Puerto_Comunicacion         :" + "server.Close"
                Error_Field = Registro_Log
                Write_event_log_error_file(Error_Field)
                'AppendLog(Registro_Log)
            Else
                'MessageBox.Show("El Servicio ya Esta Detenido", "Servicio", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        Catch Error_Cierra_Puerto_Comunicacion As Exception
            Error_Field = "**Error_Cierra_Puerto_Comunicacion: " & Error_Cierra_Puerto_Comunicacion.Message
            Write_event_log_error_file(Error_Field)
        Finally
        End Try
    End Sub


    ''''COMUNICATION DATA
    Private Sub Envia_Respuesta(ByVal e As Irc.Networking.ServerSocket.ServerSocketEventArgs, ByVal Envia As String)
        Try
            If (TypeOf (e.ClientSocket) Is Irc.Networking.ClientSocket) Then
                Dim c As Irc.Networking.ClientSocket = e.ClientSocket
                'Registro_Log = "Envia_Respuesta >>>                :"
                'AppendLog(Registro_Log)
                Registro_Log = "Envia_Respuesta >>>                :" + e.RemoteHost.ToString() + ":" + e.RemotePort.ToString + ">>> Respondio >>>" + Envia + ControlChars.CrLf
                AppendLog(Registro_Log)
                c.SendTextAppendCrlf(Envia)
                'New tbMessages.Text += "<" + e.RemoteHost.ToString() + ":" + e.RemotePort.ToString + "> Respondio " + ControlChars.CrLf
            End If
        Catch Errores_Envia_Respuesta As Exception
            Error_Field = "Errores_Envia_Respuesta >>>        :" + Envia + "  " + Errores_Envia_Respuesta.ToString
            Write_event_log_error_file(Error_Field)
            'New MessageBox.Show("Error Envia_Respuesta", "Error Enviando Respuesta", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
        End Try
        Respuesta = ""
    End Sub
    ''Private Sub Separa_Cadena_Datos_Recibida(ByVal Cadena As String)
    Private Sub Separa_Cadena_Datos_Recibida(ByVal Cadena As String)
        Try
            Registro_Log = "Separa_Cadena_Datos_Recibida 1      :" + Cadena
            AppendLog(Registro_Log)
            For Inicio = 1 To Len(Cadena)
                If Mid(Cadena, Inicio, 1) = Chr(1) Then
                    Inicio_Encabezado = Inicio
                ElseIf Mid(Cadena, Inicio, 1) = Chr(2) Then
                    Fin_Encabezado = Inicio
                    Inicio_Datos = Inicio + 1
                ElseIf Mid(Cadena, Inicio, 1) = Chr(3) Then
                    Fin_Datos = Inicio
                    Inicio_Trailer = Inicio
                ElseIf Mid(Cadena, Inicio, 1) = Chr(4) Then
                    Fin_Trailer = Inicio
                ElseIf Mid(Cadena, Inicio, 1) = Chr(28) Then
                    Inicio_Separador = Inicio
                End If
            Next
            Registro_Log = "Separa_Cadena_Datos_Recibida 2      :" + Inicio_Encabezado.ToString + " " + Fin_Encabezado.ToString + " " + Inicio_Datos.ToString + " " + Fin_Datos.ToString + " " + Inicio_Trailer.ToString + " " + Fin_Trailer.ToString
            AppendLog(Registro_Log)
            ''
            If Fin_Datos = 0 Then
                Fin_Datos = Len(Cadena)
            End If
            If Fin_Trailer = 0 Then
                Trailer = Chr(3) & "ABCD" & Chr(4)
            End If
            ''
            Registro_Log = "Separa_Cadena_Datos_Recibida 3      :" + Fin_Datos.ToString + " " + Trailer
            AppendLog(Registro_Log)
            Inicio_Encabezado = 1
            Fin_Encabezado = 27
            If Inicio_Encabezado > 0 Then
                Encabezado = (Mid(Cadena, Inicio_Encabezado, Fin_Encabezado))
            End If
            Registro_Log = "Separa_Cadena_Datos_Recibida 4 enca :" + Encabezado
            AppendLog(Registro_Log)
            Inicio_Datos = Fin_Encabezado + 1
            'Fin_Datos = Inicio_Trailer - 1
            If Inicio_Datos > 0 Then
                Datos = (Mid(Cadena, Inicio_Datos, (Fin_Datos - Inicio_Datos)))
                ''Datos = (Mid(Cadena, Inicio_Datos, Fin_Datos))
            End If
            Registro_Log = "Separa_Cadena_Datos_Recibida 5 dato :" + Datos
            AppendLog(Registro_Log)
            If Inicio_Trailer > 0 Then
                Trailer = (Mid(Cadena, Inicio_Trailer, (Fin_Trailer - Inicio_Trailer + 1)))
            End If
            Registro_Log = "Separa_Cadena_Datos_Recibida 6 trai :" + Trailer
            AppendLog(Registro_Log)
            If Inicio_Separador > 0 Then
                Separador1 = Mid(Cadena, Inicio_Separador, 4)
            End If
            Registro_Log = "Separa_Cadena_Datos_Recibida 7      :" + Separador1
            AppendLog(Registro_Log)
            If Encabezado.Length > 10 Then
                Numero_WSID_A = Encabezado.Substring(8, 2)
                Numero_WSID = Convert.ToInt64(Numero_WSID_A)
            End If
            Registro_Log = "Separa_Cadena_Datos_Recibida 8      :" + Numero_WSID_A + " " + Numero_WSID.ToString
            AppendLog(Registro_Log)
        Catch Errores_Separa_Cadena_Datos_Recibid As Exception
            Error_Field = "Errores_Separa_Cadena_Datos_Recibid:" + Cadena + "  " + Errores_Separa_Cadena_Datos_Recibid.ToString
            Write_event_log_error_file(Error_Field)
            Datos = Encabezado & Chr(2) & Chr(28) & "00 |" & "RE-INTENTARLO" & Chr(3) & "ABCD" & Chr(4)
            'New MessageBox.Show("Error Separa_Cadena_Datos_Recibida", "Error Separando Datos Recibidos", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
        End Try
    End Sub

    '''' LOGFILE
    Private Sub CloseLog()
        Try
            'Return
            If Not myStreamWriter Is Nothing Then
                myStreamWriter.Close()
            End If
        Catch exc As Exception
            MsgBox("CloseLog: Exception: " + "<>" + exc.Message)
        Finally

        End Try
    End Sub
    Private Sub Verifica_Dia_Log()
        Try
            'Return
            Campo02 = Format(Date.Now.Day, "00") + "-" + Format(Date.Now.Month, "00") + "-" + Format(Date.Now.Year, "0000") + ".Log"
            If txtFileNameLog <> Campo02 Then
                CloseLog()
                txtFileNameLog = Format(Date.Now.Day, "00") + "-" + Format(Date.Now.Month, "00") + "-" + Format(Date.Now.Year, "0000") + ".Log"
                myStreamWriter = File.AppendText(txtFileNameLog)
            End If
        Catch exc As Exception
            MsgBox("Verifica_Dia_Log: Exception: " + txtFileNameLog + "<>" + exc.Message)
        Finally
        End Try
    End Sub
    Private Sub OpenLog()
        Try
            'Return
            If Not myStreamWriter Is Nothing Then
                myStreamWriter.Close()
            End If
            txtFileNameLog = Format(Date.Now.Day, "00") + "-" + Format(Date.Now.Month, "00") + "-" + Format(Date.Now.Year, "0000") + ".Log"
            myStreamWriter = File.AppendText(txtFileNameLog)
        Catch exc As Exception
            MsgBox("OpenLog: Exception: " + txtFileNameLog + "<>" + exc.Message)
        Finally
        End Try
    End Sub
    Private Sub AppendLog(ByVal Reg_Log As String)
        Try
            'Return  Quitar
            Verifica_Dia_Log()
            OpenLog()
            Campo01 = Format(Date.Now.Day, "00") + "-" + Format(Date.Now.Month, "00.") + "-" + Format(Date.Now.Year, "0000 ") + Format(Date.Now.Hour, "00") + ":" + Format(Date.Now.Minute, "00") + ":" + Format(Date.Now.Second, "00") + ":" + Format(Date.Now.Millisecond, "000") + "..." + Reg_Log + ">>>" + ControlChars.CrLf
            myStreamWriter.Write(Campo01)
            myStreamWriter.Flush()
            CloseLog()
        Catch exc As Exception
            'MsgBox("AppendLog: Exception: " + Campo01 + "<>" + exc.Message)
        Finally
        End Try
    End Sub

    ''' EVENTLOG
    Public Sub Write_event_log_error_file(ByVal Error_Campo As String)
        Try
            EventLog.WriteEntry("Servicio Crm Soa: " + Error_Campo)
        Catch exc As Exception
            Error_Field = "Write_event_log_error_file. Exception: " + exc.Message
            EventLog.WriteEntry("Servicio Crm Soa: (!ERROR INTERNO!)" + Error_Field)
        Finally
        End Try
    End Sub

    '''CONECCTION DB
    Private Sub SQLConnectString(ByVal sServer As String, ByVal sUser As String, ByVal sPassword As String)
        Try
            Registro_Log = "SQLConnectString                   :" + sServer + "  " + sUser + "  " + sPassword
            AppendLog(Registro_Log)
            If Conexcion.State = ConnectionState.Closed Then
                Conexcion.ConnectionString = "SERVER=" & sServer & ";UID=" & sUser & _
                                     ";PWD=" & sPassword & ";DATABASE=micros.db"
                Conexcion.Open()
            End If
            Permite_Conexcion = True  'MessageBox.Show("Base de datos Conetada")
            Registro_Log = "SQLConnectString                   :" + sServer + "  " + sUser + "  " + sPassword + "  " + "Base de datos Conetada  " + Permite_Conexcion.ToString
            AppendLog(Registro_Log)
        Catch Error_SQLConnectString As Exception
            Permite_Conexcion = False
            Error_Field = "SQLConnectString                   :" + sServer + "  " + sUser + "  " + sPassword + "  " + "Base de datos NO Conetada  " + Permite_Conexcion.ToString + "  " + Error_SQLConnectString.ToString
            Write_event_log_error_file(Error_Field)
            'New MessageBox.Show("Error SQLConnectString" & Error_SQLConnectString.ToString, "Error Conectando a la Base de Datos " & Error_SQLConnectString.ToString, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub ODBC_ConnectStringMICROS(ByVal sDataBase As String)
        Try
            Registro_Log = "ODBCDataBase                      :" + sDataBase
            AppendLog(Registro_Log)
            If ConexcionMCRSPOS.State = ConnectionState.Closed Then
                'ConexcionMCRSPOS.ConnectionString = "DSN=Micros;UID=dba;PWD=Db@M@5t3r$;Mode=Read"
                ConexcionMCRSPOS.ConnectionString = "DSN=Micros;UID=dba;PWD=Password1;Mode=Read"
                'ConexcionMCRSPOS.ConnectionString = "DSN=Micros;UID=dba;PWD=Micros3998.;Mode=Read"
                ConexcionMCRSPOS.Open()
            End If
            Permite_Conexcion_MCRSPOS = True
            Registro_Log = "ODBC_ConnectStringMICROS           :" + sDataBase + "  " + Permite_Conexcion_MCRSPOS.ToString
            AppendLog(Registro_Log)
        Catch Error_ODBC_ConnectStringMICROS As Exception
            Permite_Conexcion = False
            Error_Field = "Error_ODBC_ConnectStringMICROS              :" + sDataBase + "  " + Permite_Conexcion_MCRSPOS.ToString + " " + Error_ODBC_ConnectStringMICROS.ToString
            Write_event_log_error_file(Error_Field)
        End Try
    End Sub

    '''PORT CRM FROM MICROS
    Private Sub SQLReaderTimeout_interface_micros_CRM()
        Try
            Registro_Log = "SQLReaderTimeout_interface_micros_CRM                      :"
            AppendLog(Registro_Log)
            Permite_Conexcion_MCRSPOS = False
            Salir = False
            ODBC_ConnectStringMICROS("micros.db")
            If Permite_Conexcion_MCRSPOS = True Then
                Dim Command As New OdbcCommand("SELECT timeout, tcp_port_number FROM micros.interface_def where obj_num = 6 ", ConexcionMCRSPOS)
                Command.CommandType = CommandType.Text
                ReadSql_odbc = Command.ExecuteReader(CommandBehavior.CloseConnection)
                If ReadSql_odbc.Read = True Then
                    Timeout_Ifc = ReadSql_odbc.GetValue(0)
                    PuertoID = ReadSql_odbc.GetValue(1)
                    Timeout_Ifc_Ws = ((Timeout_Ifc - 5) * 1000)
                    Permite_Conexcion_MCRSPOS = True
                    Registro_Log = "SQLReaderTimeout_interface_micros_CRM                      :" + Convert.ToString(PuertoID)
                    AppendLog(Registro_Log)
                End If
            Else
                Salir = True
            End If
            If Timeout_Ifc = 0 Then
                Timeout_Ifc = 30
                Timeout_Ifc_Ws = ((Timeout_Ifc - 5) * 1000)
            End If
        Catch Errores_SQLReaderTimeout_cfd_facturacion As Exception
            Permite_Conexcion_MCRSPOS = False
            Error_Field = "Errores_SQLReaderTimeout_cfd_facturacion                  :" + Convert.ToString(PuertoID) + "  " + Errores_SQLReaderTimeout_cfd_facturacion.ToString
            Write_event_log_error_file(Error_Field)
        Finally
            If Salir = False Then
                ReadSql_odbc.Close()
            End If
        End Try
    End Sub

    '''DELETE VAR
    Private Sub Borra_Variables()
        Registro_Log = "Borra_Variables                    :"
        AppendLog(Registro_Log)
        Permite_Conexcion = False

        Inicio = 0
        Inicio_Encabezado = 0
        Fin_Encabezado = 0
        Inicio_Datos = 0
        Fin_Datos = 0
        Inicio_Trailer = 0
        Fin_Trailer = 0
        Inicio_Separador = 0
        Fin_Separador = 0
        Longitud = 0
        Cadena_Inicio = ""
        Socket_Peticion = ""
        Encabezado = ""
        Datos = ""
        Datos1 = ""
        Trailer = ""
        Separador1 = ""
        Respuesta = ""
        Nombre = ""
        Numero_Tarjeta = ""
        Card_Status = ""
        Beneficios(1) = ""
        Beneficios(2) = ""
        Beneficios(3) = ""
        Respuesta = ""
        Respuesta2 = ""

    End Sub

    '''FILE CONFIG
    Private Sub ReadFile()
        Try
            myStreamReader = File.OpenText(FileConfiguracion_CRM)
            ' The Read() method returns '-1' when the end of the
            '   file has been reached
            While myStreamReader.Peek <> -1
                Respuesta = myStreamReader.ReadLine()
                Lectura = Respuesta.Split(",")
                If Lectura(0) = "<Interface CRM>" Then
                    numero_Interface = Convert.ToInt32(Lectura(1))
                ElseIf Lectura(0) = "<Mail Remitente>" Then
                    Mail_Remitente = Lectura(1)
                    Mail_Remitente_Password = Lectura(2)
                ElseIf Lectura(0) = "<Mail Destinatario>" Then
                    Mail_Destinatario = Lectura(1)
                ElseIf Lectura(0) = "<Identificador_cliente>" Then
                    entidad = Lectura(1)
                End If
            End While
        Catch exc As Exception
            ' Show the exception to the user.
            Error_Field = "No se puede abrir archivo." + vbCrLf + FileConfiguracion_CRM + vbCrLf + _
            vbCrLf + vbCrLf + "Exception: " + exc.Message
        Finally
            If Not myStreamReader Is Nothing Then
                myStreamReader.Close()
            End If
        End Try
    End Sub


    Sub Borra_Totales_Busqueda_Xml()
        PromocionId = ""
        PromocionName = ""
        PromocionUniqueKey = ""
        PromocionDisplayName = ""
        PromocionStatus = ""
    End Sub
    Sub Borra_Totales_Busqueda_Voucher_Xml()
        VoucherCalStatus = ""
        VoucherProductId = ""
        VoucherProductName = ""
        VoucherStatus = ""
        VoucherTransactionId = ""
    End Sub


    Sub Separa_Campos_Individules_Indice(ByVal Valor As String, ByRef Resp_valor As String, ByRef Indice As Integer)
        Resp_valor = ""
        If Respuesta1.IndexOf(Valor, Indice) > 0 Then
            Inicio2 = (Valor.Length + 1)
            Inicio3 = (Valor.Length - 1)
            Inicio_L = Respuesta1.IndexOf(Valor, Indice)
            Inicio_L = Inicio_L + Inicio2
            Cadena = Mid(Valor, 1, 1) & "/" & Mid(Valor, 2, Inicio3)
            If Respuesta1.IndexOf(Cadena, Inicio_L) > 0 Then
                Final_L = Respuesta1.IndexOf(Cadena, Inicio_L)
            End If
            If Inicio_L > 0 And Final_L > 0 Then
                Longitud_L = (Final_L - Inicio_L) + 1
                Resp_valor = Mid(Respuesta1, Inicio_L, Longitud_L)
                Indice = Final_L
            End If
        End If
    End Sub
    Sub Separa_Campos_Individules_Indice_Uno(ByVal Valor As String, ByRef Resp_valor As String, ByRef Indice As Integer)
        Resp_valor = ""
        If Respuesta1.IndexOf(Valor, Indice) > 0 Then
            Inicio2 = (Valor.Length + 1)
            Inicio3 = (Valor.Length - 1)
            Inicio_L = Respuesta1.IndexOf(Valor, Indice)
            Inicio_L = Inicio_L + Inicio2
            Cadena = Mid(Valor, 1, 1) & "/" & Mid(Valor, 2, Inicio3)
            If Respuesta1.IndexOf(Cadena, Inicio_L) > 0 Then
                Final_L = Respuesta1.IndexOf(Cadena, Inicio_L)
            Else
                Final_L = Inicio_L
                Indice = Final_L
                Resp_valor = ""
                Exit Sub
            End If
            If Inicio_L > 0 And Final_L > 0 Then
                Longitud_L = (Final_L - Inicio_L) + 1
                Resp_valor = Mid(Respuesta1, Inicio_L, Longitud_L)
                Indice = Final_L
            End If
        End If
    End Sub

    Sub Separa_Campos_Individules(ByVal Valor As String, ByRef Resp_valor As String)
        Try

            Resp_valor = ""
            ''Write_event_log_error_file(Valor)

            Error_Field = "Inicia Separa_Campos_Individules:<<" + Valor + ">>"
            Write_event_log_error_file(Error_Field)

            If Respuesta1.IndexOf(Valor, 0) > 0 Then
                Inicio2 = (Len(Valor) + 1)
                Inicio3 = (Len(Valor) - 1)
                Inicio_L = Respuesta1.IndexOf(Valor, 0) + 0
                '23Dix2014 Inicio_L = Inicio_L + Inicio2 + 9
                Inicio_L = Inicio_L + Inicio2
                ''Cadena = Mid(Valor, 1, 1) & "/" & Mid(Valor, 2, Inicio3)
                Cadena = Mid(Valor, 1, 1) & "/" & Mid(Valor, 2, Inicio3)

                ''Error_Field = "Separa_Campos_Individules.: Cadena:<<" + Cadena + ">>"
                ''Write_event_log_error_file(Error_Field)

                ''Write_event_log_error_file(Cadena)
                If Respuesta1.IndexOf(Cadena, 0) > 0 Then
                    Final_L = Respuesta1.IndexOf(Cadena, 0)
                End If

                ''Error_Field = "Separa_Campos_Individules.: Inicio3:<<" + Inicio3.ToString + ">>"
                ''Write_event_log_error_file(Error_Field)

                ''Error_Field = "Separa_Campos_Individules.: Inicio_L:<<" + Inicio_L.ToString + ">>"
                ''Write_event_log_error_file(Error_Field)

                ''Error_Field = "Separa_Campos_Individules.: Final_L:<<" + Final_L.ToString + ">>"
                ''Write_event_log_error_file(Error_Field)


                If Inicio_L > 0 And Final_L > 0 Then
                    Longitud_L = (Final_L - Inicio_L) + 1

                    ''Error_Field = "Separa_Campos_Individules.: Longitud_L:<<" + Longitud_L.ToString + ">>"
                    ''Write_event_log_error_file(Error_Field)


                    ''Error_Field = "Separa_Campos_Individules.: Resp_valor:<<" + Resp_valor + ">>"
                    ''Write_event_log_error_file(Error_Field)

                    Resp_valor = Mid(Respuesta1, Inicio_L, Longitud_L)

                    ''Error_Field = "Separa_Campos_Individules.: Resp_valor 01:<<" + Resp_valor + ">>"
                    ''Write_event_log_error_file(Error_Field)

                    ''Resp_valor = Mid(Respuesta1, Inicio_L - 2, 1) + Resp_valor

                    ''Error_Field = "Separa_Campos_Individules.: Resp_valor 02:<<" + Resp_valor + ">>"
                    ''Write_event_log_error_file(Error_Field)
                End If
            End If
            Error_Field = "Final Separa_Campos_Individules:<<" + Resp_valor + ">>"
            Write_event_log_error_file(Error_Field)
        Catch exp As Exception
            Respuesta = exp.ToString
            Error_Field = "!!ERROR!! Separa_Campos_Individules: <<" + exp.ToString + ">>"
            Write_event_log_error_file(Error_Field)
            'MessageBox.Show(Respuesta)
        End Try
        ''Write_event_log_error_file(Resp_valor)
    End Sub
    Sub Separa_Campos_Individules_Favorite(ByVal Valor As String, ByVal Valor2 As String, ByVal Valor3 As String, ByRef Resp_valor As String, ByRef Indice As Integer)
        ''Separa_Campos_Individules_Favorite("<Value>", "Last Beverage", Last_Beverage, Inicio_L)
        Resp_valor = ""
        Inicio_L = 0
        ''Write_event_log_error_file(Valor)
        Try
            If Respuesta1.IndexOf(Valor2, Indice) > 0 Then
                If Respuesta1.IndexOf(Valor, Respuesta1.IndexOf(Valor2, Indice)) > 0 Then
                    Inicio2 = (Valor.Length + 1)
                    Inicio3 = (Valor.Length - 1)
                    Inicio_L = Respuesta1.IndexOf(Valor, Respuesta1.IndexOf(Valor2, Indice))
                    Inicio_L = Inicio_L + Inicio2 + 1
                    Cadena = Mid(Valor, 1, 1) & "/" & Mid(Valor, 2, Inicio3)
                    Cadena = Valor3
                    ''Write_event_log_error_file(Cadena)
                    If Respuesta1.IndexOf(Cadena, Inicio_L) > 0 Then
                        Final_L = Respuesta1.IndexOf(Cadena, Inicio_L)
                    End If
                    If Inicio_L > 0 And Final_L > 0 Then
                        Longitud_L = (Final_L - Inicio_L)
                        Resp_valor = Mid(Respuesta1, Inicio_L, Longitud_L)
                        Indice = Final_L
                        Resp_valor.Replace(Chr(34), ".")
                    End If
                ElseIf Respuesta1.IndexOf(Valor3, Respuesta1.IndexOf(Valor2, Indice)) > 0 Then
                    Inicio2 = (Valor3.Length + 1)
                    Inicio3 = (Valor3.Length - 1)
                    Inicio_L = Respuesta1.IndexOf(Valor3, Respuesta1.IndexOf(Valor2, Indice))
                    'Inicio_L = Inicio_L + Inicio2
                    Cadena = Valor3
                    ''Write_event_log_error_file(Cadena)
                    If Respuesta1.IndexOf(Cadena, Inicio_L) > 0 Then
                        Final_L = Respuesta1.IndexOf(Cadena, Inicio_L)
                    End If
                    If Inicio_L > 0 And Final_L > 0 Then
                        Longitud_L = (Final_L - Inicio_L) + 1
                        Resp_valor = Mid(Respuesta1, Inicio_L, Longitud_L)
                        Indice = Final_L
                    End If
                End If
            End If
        Catch exp As Exception
            Respuesta = exp.ToString
            'MessageBox.Show(Respuesta)
        End Try
        ''Write_event_log_error_file(Resp_valor)
    End Sub
    Sub Separa_Campos_Individules_Favorite_Sin_Borrar(ByVal Valor As String, ByVal Valor2 As String, ByVal Valor3 As String, ByRef Resp_valor As String, ByRef Indice As Integer)
        ''Separa_Campos_Individules_Favorite("<Value>", "Last Beverage", Last_Beverage, Inicio_L)
        Resp_valor = ""
        ''Inicio_L = 0
        ''Write_event_log_error_file(Valor)
        Try
            If Respuesta1.IndexOf(Valor2, Indice) > 0 Then
                If Respuesta1.IndexOf(Valor, Respuesta1.IndexOf(Valor2, Indice)) > 0 Then
                    Inicio2 = (Valor.Length + 1)
                    Inicio3 = (Valor.Length - 1)
                    Inicio_L = Respuesta1.IndexOf(Valor, Respuesta1.IndexOf(Valor2, Indice))
                    Inicio_L = Inicio_L + Inicio2 + 1
                    Cadena = Mid(Valor, 1, 1) & "/" & Mid(Valor, 2, Inicio3)
                    Cadena = Valor3
                    ''Write_event_log_error_file(Cadena)
                    If Respuesta1.IndexOf(Cadena, Inicio_L) > 0 Then
                        Final_L = Respuesta1.IndexOf(Cadena, Inicio_L)
                    End If
                    If Inicio_L > 0 And Final_L > 0 Then
                        Longitud_L = (Final_L - Inicio_L)
                        Resp_valor = Mid(Respuesta1, Inicio_L, Longitud_L)
                        Indice = Final_L
                        Resp_valor.Replace(Chr(34), ".")
                    End If
                ElseIf Respuesta1.IndexOf(Valor3, Respuesta1.IndexOf(Valor2, Indice)) > 0 Then
                    Inicio2 = (Valor3.Length + 1)
                    Inicio3 = (Valor3.Length - 1)
                    Inicio_L = Respuesta1.IndexOf(Valor3, Respuesta1.IndexOf(Valor2, Indice))
                    'Inicio_L = Inicio_L + Inicio2
                    Cadena = Valor3
                    ''Write_event_log_error_file(Cadena)
                    If Respuesta1.IndexOf(Cadena, Inicio_L) > 0 Then
                        Final_L = Respuesta1.IndexOf(Cadena, Inicio_L)
                    End If
                    If Inicio_L > 0 And Final_L > 0 Then
                        Longitud_L = (Final_L - Inicio_L) + 1
                        Resp_valor = Mid(Respuesta1, Inicio_L, Longitud_L)
                        Indice = Final_L
                    End If
                End If
            End If
        Catch exp As Exception
            Respuesta = exp.ToString
            'MessageBox.Show(Respuesta)
        End Try
        ''Write_event_log_error_file(Resp_valor)
    End Sub
    
    Private Sub ODBC_ConnectStringMICROS()
        Try
            If ConexcionMCRSPOS.State = ConnectionState.Closed Then
                ''ConexcionMCRSPOS.ConnectionString = "DSN=Micros;UID=dba;PWD=smc9nair;Mode=Read"
                'ConexcionMCRSPOS.ConnectionString = "DSN=Micros;UID=dba;PWD=Db@M@5t3r$;Mode=Read"
                ConexcionMCRSPOS.ConnectionString = "DSN=Micros;UID=dba;PWD=Password1;Mode=Read"
                ''ConexcionMCRSPOS.ConnectionString = "DSN=Micros;UID=dba;PWD=Micros3998.;Mode=Read"
                ConexcionMCRSPOS.Open()
            End If
            Permite_Conexcion_CRM_Micros = True
        Catch Error_ODBC_ConnectStringMICROS As Exception
            Permite_Conexcion_CRM_Micros = False
            Registro_Log = "Error_ODBC_ConnectStringMICROS              :" + Permite_Conexcion_MCRSPOS.ToString + " " + Error_ODBC_ConnectStringMICROS.ToString
            Write_event_log_error_file(Registro_Log)
        End Try
    End Sub
    Private Sub ODBC_ConnectStringMICROS_Dos()
        Try
            If ConexcionMCRSPOS_Dos.State = ConnectionState.Closed Then
                ''ConexcionMCRSPOS.ConnectionString = "DSN=Micros;UID=dba;PWD=smc9nair;Mode=Read"
                'ConexcionMCRSPOS_Dos.ConnectionString = "DSN=Micros;UID=dba;PWD=Db@M@5t3r$;Mode=Read"
                ConexcionMCRSPOS.ConnectionString = "DSN=Micros;UID=dba;PWD=Password1;Mode=Read"
                ''ConexcionMCRSPOS.ConnectionString = "DSN=Micros;UID=dba;PWD=Micros3998.;Mode=Read"
                ConexcionMCRSPOS_Dos.Open()
            End If
            Permite_Conexcion_CRM_Micros_Dos = True
        Catch Error_ODBC_ConnectStringMICROS_Dos As Exception
            Permite_Conexcion_CRM_Micros_Dos = False
            Registro_Log = "Error_ODBC_ConnectStringMICROS              :" + Permite_Conexcion_CRM_Micros_Dos.ToString + " " + Error_ODBC_ConnectStringMICROS_Dos.ToString
            Write_event_log_error_file(Registro_Log)
        End Try
    End Sub
    Private Sub ODBC_ConnectStringMICROS_Tres()
        Try
            If ConexcionMCRSPOS_Tres.State = ConnectionState.Closed Then
                ''ConexcionMCRSPOS.ConnectionString = "DSN=Micros;UID=dba;PWD=smc9nair;Mode=Read"
                'ConexcionMCRSPOS_Tres.ConnectionString = "DSN=Micros;UID=dba;PWD=Db@M@5t3r$;Mode=Read"
                ConexcionMCRSPOS.ConnectionString = "DSN=Micros;UID=dba;PWD=Password1;Mode=Read"
                ''ConexcionMCRSPOS.ConnectionString = "DSN=Micros;UID=dba;PWD=Micros3998.;Mode=Read"
                ConexcionMCRSPOS_Tres.Open()
            End If
            Permite_Conexcion_CRM_Micros_Tres = True
        Catch Error_ODBC_ConnectStringMICROS_Tres As Exception
            Permite_Conexcion_CRM_Micros_Tres = False
            Registro_Log = "Error_ODBC_ConnectStringMICROS_Tres              :" + Permite_Conexcion_CRM_Micros_Tres.ToString + " " + Error_ODBC_ConnectStringMICROS_Tres.ToString
            Write_event_log_error_file(Registro_Log)
        End Try
    End Sub
    Private Sub ODBC_ConnectStringMicrosClose()
        Try
            If ConexcionMCRSPOS.State = ConnectionState.Open Then
                ConexcionMCRSPOS.Close()
                Permite_Conexcion_CRM_Micros = False
            End If
        Catch Error_ODBC_ConnectStringFacturacionClose As Exception
            Registro_Log = "ODBC_ConnectStringMicrosClose      :" + Error_ODBC_ConnectStringFacturacionClose.ToString
            Write_event_log_error_file(Registro_Log)
        End Try
    End Sub
    Private Sub ODBC_ConnectStringMicrosCloseTres()
        Try
            If ConexcionMCRSPOS_Tres.State = ConnectionState.Open Then
                ConexcionMCRSPOS_Tres.Close()
                Permite_Conexcion_CRM_Micros_Tres = False
            End If
        Catch Error_ODBC_ConnectStringMicrosCloseTres As Exception
            Registro_Log = "Error_ODBC_ConnectStringMicrosCloseTres      :" + Error_ODBC_ConnectStringMicrosCloseTres.ToString
            Write_event_log_error_file(Registro_Log)
        End Try
    End Sub

    Private Sub Graba_Log_Redenciones(ByVal Reg_Redenciones_Log As String)
        Try
            txtFileNameRedencionesLog = Path_WS & File_Log_Redenciones_CRM
            myStreamWriter = File.AppendText(txtFileNameRedencionesLog)
            myStreamWriter.WriteLine(Now.ToLongDateString & ", " & Now.ToLongTimeString & ", " & Reg_Redenciones_Log)
            myStreamWriter.Flush()
        Catch Errores_graba_log As Exception
            Registro_Log = "Graba_Log_Redenciones          :" + Errores_graba_log.ToString
        Finally
            If Not myStreamWriter Is Nothing Then
                myStreamWriter.Close()
            End If
        End Try
    End Sub

    Private Sub SQLInsert_Acreditaciones_CRM()
        Try
            Registro_Log = "SQLInsert_Acreditaciones_CRM"
            AppendLog(Registro_Log)

            Insert_Datos_CRM = False
            Permite_Conexcion_CRM_Micros = False
            Salir = False
            ODBC_ConnectStringMICROS()

            Registro_Log = "Permite_Conexcion_CRM_Micros:" & Permite_Conexcion_CRM_Micros.ToString
            AppendLog(Registro_Log)

            If Permite_Conexcion_CRM_Micros = True Then
                InsertTicket = "INSERT INTO custom.LOYTRANSACCION_OFFLINE SELECT * FROM custom.LOYTRANSACCION "
                InsertTicket = InsertTicket & "WHERE TicketNumber = '" & TicketNumber & "' "
                InsertTicket = InsertTicket & "AND  TransactionType = 'Accrual' "
                'Write_event_log_error_file(InsertTicket)
                Registro_Log = InsertTicket
                AppendLog(Registro_Log)
                Dim Command As New OdbcCommand(InsertTicket, ConexcionMCRSPOS)
                Command.CommandType = CommandType.Text
                ReadSql = Command.ExecuteScalar
                Insert_Datos_CRM = True
            End If
        Catch Errores_SQLInsert_Ticket_CFD As Exception
            Registro_Log = "SQLInsert_Acreditaciones_CRM              :" + Errores_SQLInsert_Ticket_CFD.ToString
            Insert_Datos_CRM = False
            AppendLog(Registro_Log)
            'Write_event_log_error_file(Registro_Log)
        Finally
            ODBC_ConnectStringMicrosClose()
        End Try
    End Sub
    Private Sub SQLUpdate_Acreditaciones_CRM()
        Try
            Registro_Log = "SQLUpdate_Acreditaciones_CRM"
            AppendLog(Registro_Log)

            Insert_Datos_CRM = False
            Permite_Conexcion_CRM_Micros = False
            Salir = False
            ODBC_ConnectStringMICROS()
            If Permite_Conexcion_CRM_Micros = True Then
                InsertTicket = "UPDATE  custom.LOYTRANSACCION_OFFLINE set Estatus = 'PEN', TicketNumber2 = '" & Campos_Paso(2) & "'  "
                InsertTicket = InsertTicket & "WHERE TicketNumber = '" & TicketNumber & "' "
                'Write_event_log_error_file(InsertTicket)
                Dim Command As New OdbcCommand(InsertTicket, ConexcionMCRSPOS)
                Command.CommandType = CommandType.Text
                ReadSql = Command.ExecuteScalar
                Insert_Datos_CRM = True
            End If
        Catch Errores_SQLInsert_Ticket_CFD As Exception
            Registro_Log = "SQLUpdate_Acreditaciones_CRM              :" + Errores_SQLInsert_Ticket_CFD.ToString
            AppendLog(Registro_Log)

            Insert_Datos_CRM = False
            'Write_event_log_error_file(Registro_Log)
        Finally
            ODBC_ConnectStringMicrosClose()
        End Try
    End Sub
    Private Sub SQLUpdateLoytransaccion_Acreditaciones_CRM()
        Try
            Registro_Log = "SQLUpdateLoytransaccion_Acreditaciones_CRM"
            AppendLog(Registro_Log)

            Insert_Datos_CRM = False
            Permite_Conexcion_CRM_Micros = False
            Salir = False
            ODBC_ConnectStringMICROS()
            If Permite_Conexcion_CRM_Micros = True Then
                InsertTicket = "UPDATE  custom.LOYTRANSACCION set Estatus = 'TER' "
                InsertTicket = InsertTicket & "WHERE TicketNumber = '" & TicketNumber & "' "
                'Write_event_log_error_file(InsertTicket)
                Dim Command As New OdbcCommand(InsertTicket, ConexcionMCRSPOS)
                Command.CommandType = CommandType.Text
                ReadSql = Command.ExecuteScalar
                Insert_Datos_CRM = True
            End If
        Catch Errores_SQLInsert_Ticket_CFD As Exception
            Registro_Log = "SQLUpdate_Acreditaciones_CRM              :" + Errores_SQLInsert_Ticket_CFD.ToString
            Insert_Datos_CRM = False
            AppendLog(Registro_Log)
            'Write_event_log_error_file(Registro_Log)
        Finally
            ODBC_ConnectStringMicrosClose()
        End Try
    End Sub


    Sub Procesa_Ejecuta_Acreditacion_OffLine()
        Campos_Paso = Datos.Split("|")
        TicketNumber = Campos_Paso(1)
        SQLInsert_Acreditaciones_CRM()
        SQLUpdate_Acreditaciones_CRM()
        SQLUpdateLoytransaccion_Acreditaciones_CRM()
    End Sub

    Private Sub SQLUpdateLoytransaccion_Acreditaciones_CRM_Not_Used()
        Try
            Insert_Datos_CRM = False
            Permite_Conexcion_CRM_Micros = False
            Salir = False
            ODBC_ConnectStringMICROS()
            If Permite_Conexcion_CRM_Micros = True Then
                InsertTicket = "UPDATE  custom.LOYTRANSACCION set Estatus = 'TER' "
                InsertTicket = InsertTicket & "WHERE TicketNumber = '" & TicketNumber & "' "
                Write_event_log_error_file(InsertTicket)
                Dim Command As New OdbcCommand(InsertTicket, ConexcionMCRSPOS)
                Command.CommandType = CommandType.Text
                ReadSql = Command.ExecuteScalar
                Insert_Datos_CRM = True
            End If
        Catch Errores_SQLInsert_Ticket_CFD As Exception
            Registro_Log = "SQLUpdate_Acreditaciones_CRM              :" + Errores_SQLInsert_Ticket_CFD.ToString
            Insert_Datos_CRM = False
            Write_event_log_error_file(Registro_Log)
        Finally
            ODBC_ConnectStringMicrosClose()
        End Try
    End Sub
    Private Sub SQLSearchCheques_Respuesta_Acreditaciones(ByVal TicketNumber_ As String)
        Try
            Permite_Conexcion_CRM_Micros_Dos = False
            Salir = False
            ODBC_ConnectStringMICROS_Dos()
            If Permite_Conexcion_CRM_Micros_Dos = True Then
                Campo = "SELECT FIRST Consecutivo, Estatus, IntegrationStatus, Status, SubStatus FROM custom.LOYTRANSACCION_RESPONSE Where ProductName <> 'Ticket' And TicketNumber = '" & TicketNumber_ & "'  And TransactionType = 'Accrual' ORDER BY Consecutivo DESC"
                Dim Command As New OdbcCommand(Campo, ConexcionMCRSPOS_Dos)
                Command.CommandType = CommandType.Text
                Registro_Log = ("SQLSearchCanceladosOffLine: " & Campo)
                Write_event_log_error_file(Registro_Log)
                ReadSql_odbc_Dos = Command.ExecuteReader(CommandBehavior.CloseConnection)
                While ReadSql_odbc_Dos.Read = True
                    Consecutivo = ReadSql_odbc_Dos.GetValue(0)
                    Estatus = ReadSql_odbc_Dos.GetValue(1)
                    IntegrationStatus = ReadSql_odbc_Dos.GetValue(2)
                    Status = ReadSql_odbc_Dos.GetValue(3)
                    SubStatus = ReadSql_odbc_Dos.GetValue(4)
                    Registro_Log = ("SQLReadResponse: " & Consecutivo.ToString & ":" & Estatus & ":" & IntegrationStatus & ":" & Status & ":" & SubStatus)
                    Write_event_log_error_file(Registro_Log)
                    If Estatus.Trim = "Fail" Or Mid(IntegrationStatus, 1, 4) = "Fail" Then
                        'SQLCancela_Ticket_Acreditaciones_CRM()
                        If Mid(IntegrationStatus, 1, 64) = "Fail - Duplicate Transaction exists for attributes Ticket Number" Then
                            'Sale No agrega a Off Line
                        Else
                            SQLInsert_Acreditaciones_CRM()
                            SQLUpdate_Acreditaciones_CRM()
                            SQLUpdate_Acreditaciones_Transmitido_CRM()
                        End If
                        'Else   TEST
                        'SQLInsert_Acreditaciones_CRM()
                        'SQLUpdate_Acreditaciones_CRM()
                        'SQLUpdate_Acreditaciones_Transmitido_CRM()
                    End If
                End While
            Else
                Salir = True
            End If
        Catch Errores_SQLSearchCheques_Respuesta As Exception
            Registro_Log = "Errores_SQLSearchCheques_Respuesta               :" + Errores_SQLSearchCheques_Respuesta.ToString
            EventLog.WriteEntry(Registro_Log)
        Finally
            If Salir = False Then
                ReadSql_odbc_Dos.Close()
                ODBC_ConnectStringMICROS_Dos()
            End If
        End Try
    End Sub
    Private Sub SQLCancela_Ticket_Acreditaciones_CRM()
        Try
            Permite_Conexcion_CRM_Micros_Tres = False
            Salir = False
            Delete_Datos_CRM = False
            ODBC_ConnectStringMICROS_Tres()
            If Permite_Conexcion_CRM_Micros_Tres = True Then
                DeleteTicket = "Delete custom.LOYTRANSACCION_RESPONSE WHERE Estatus = 'Success' And IntegrationStatus = 'Success' And TicketNumber = '" & TicketNumber & "' "
                Write_event_log_error_file(DeleteTicket)
                Dim Command As New OdbcCommand(DeleteTicket, ConexcionMCRSPOS_Tres)
                Command.CommandType = CommandType.Text
                ReadSql = Command.ExecuteScalar
                Delete_Datos_CRM = True
            End If
        Catch Errores_SQLCancela_Ticket_Acreditaciones_CRM As Exception
            Registro_Log = "Errores_SQLCancela_Ticket_Acreditaciones_CRM              :" + Errores_SQLCancela_Ticket_Acreditaciones_CRM.ToString
            Delete_Datos_CRM = False
            Write_event_log_error_file(Registro_Log)
        Finally
            ODBC_ConnectStringMicrosCloseTres()
        End Try
    End Sub
    Private Sub SQLSearchCheques_Respuesta_Redenciones(ByVal TicketNumber_ As String)
        Try
            Permite_Conexcion_CRM_Micros_Dos = False
            Salir = False
            ODBC_ConnectStringMICROS_Dos()
            If Permite_Conexcion_CRM_Micros_Dos = True Then
                Campo = "SELECT FIRST Consecutivo, Estatus, IntegrationStatus, Status, SubStatus FROM custom.LOYTRANSACCION_RESPONSE Where ProductName <> 'Ticket' And TicketNumber = '" & TicketNumber_ & "'  And TransactionType = 'Redemption' ORDER BY Consecutivo DESC"
                Dim Command As New OdbcCommand(Campo, ConexcionMCRSPOS_Dos)
                Command.CommandType = CommandType.Text
                Registro_Log = ("SQLSearchCanceladosOffLine: " & Campo)
                Write_event_log_error_file(Registro_Log)
                ReadSql_odbc_Dos = Command.ExecuteReader(CommandBehavior.CloseConnection)
                Consecutivo = 0
                Estatus = ""
                IntegrationStatus = ""
                Status = ""
                SubStatus = ""
                While ReadSql_odbc_Dos.Read = True
                    Consecutivo = ReadSql_odbc_Dos.GetValue(0)
                    Estatus = ReadSql_odbc_Dos.GetValue(1)
                    IntegrationStatus = ReadSql_odbc_Dos.GetValue(2)
                    Status = ReadSql_odbc_Dos.GetValue(3)
                    SubStatus = ReadSql_odbc_Dos.GetValue(4)
                    Registro_Log = ("SQLReadResponse: " & Consecutivo.ToString & ":" & Estatus & ":" & IntegrationStatus & ":" & Status & ":" & SubStatus)
                    Write_event_log_error_file(Registro_Log)
                    If Estatus.Trim = "Fail" Or Mid(IntegrationStatus, 1, 4) = "Fail" Then
                        Respuesta = "Respuesta [Redencion]:," & TicketNumber & ", " & MemberNumber & ", " & IntegrationStatus
                        Graba_Log_Redenciones(Respuesta)
                    ElseIf Estatus.Length <> 0 And IntegrationStatus.Length <> 0 Then
                        'Respuesta = "Valor [Redencion]," & Estatus & ", " & IntegrationStatus
                        'Graba_Log_Redenciones(Respuesta)
                    End If
                End While
            Else
                Salir = True
            End If
        Catch Errores_SQLSearchCheques_Respuesta_Redenciones As Exception
            Registro_Log = "Errores_SQLSearchCheques_Respuesta_Redenciones               :" + Errores_SQLSearchCheques_Respuesta_Redenciones.ToString
            EventLog.WriteEntry(Registro_Log)
        Finally
            If Salir = False Then
                ReadSql_odbc_Dos.Close()
                ODBC_ConnectStringMICROS_Dos()
            End If
        End Try
    End Sub


    ''New



    Private Sub SQLInsertLogPinpad(ByVal Solicito_ As String, ByVal Proceso_ As String, ByVal Datos_ As String)
        Try
            Log_Add = False
            Permite_Conexcion_MCRSPOS = False
            Salir = False
            ODBC_ConnectStringMICROS("micros.db")
            If Permite_Conexcion_MCRSPOS = True Then
                InstrSql = "INSERT INTO custom.Log_Pinpad ( Proceso, Solicito, Datos ) Values ( " & _
                           "'" & Proceso_ & "', " & _
                           "'" & Solicito_ & "', " & _
                           "'" & Datos_ & "'   " & _
                           " ) "
                Dim Command As New OdbcCommand(InstrSql, ConexcionMCRSPOS)
                Command.CommandType = CommandType.Text
                ReadSql = Command.ExecuteScalar
                Log_Add = True
            Else
                Salir = True
            End If
        Catch Errores_SQLInsertLogPinpad As Exception
            Log_Add = False
            Registro_Log = "Errores_SQLInsertLogPinpad        :" + Errores_SQLInsertLogPinpad.ToString
            'AppendLog(Registro_Log)
        Finally
            ODBC_ConnectStringMicrosClose()
        End Try
    End Sub

    Sub Borra_Totales_Tarjetas()
        No_Tarjeta = ""
        Titular = ""
        Anio = ""
        Mes = ""
        Fecha_Vencimiento = ""
        Tags_EMV = ""
        Tag_5F34 = ""
        Tag_9F27 = ""
        Tag_9F26 = ""
        Track2 = ""
        Flag = ""
        CVV2 = ""
        Digest2 = ""
        Digest = ""
        No_Tarjeta = ""
        Email_Administrador = ""
        Fecha_Vencimiento = ""
    End Sub

    Sub Proceso_Separa()
        Try
            ODBC_ConnectStringMICROS()
            For I = 1 To Len(Bines_Lealtad) Step 6
                Bines(II) = Mid(Bines_Lealtad, I, 6)

                InstrSql = "INSERT INTO custom.Bines_Pinpad (Nombre_Tarjeta, Bin ) VALUES " & _
                    "(" & "'Bancomer'," & "'" & Bines(II) & "' )"
                If Permite_Conexion_Lealtad = True Then
                    Dim Command As New OdbcCommand(InstrSql, ConexcionMCRSPOS)
                    Command.CommandType = CommandType.Text
                    ReadSql = Command.ExecuteScalar
                End If

                II = (II + 1)
            Next
            ODBC_ConnectStringMicrosClose()
        Catch exp As Exception
            Respuesta = Encabezado & Separador & exp.ToString & Trailer
        End Try
    End Sub
    Sub Limpia_Totales()
        imprimir = ""                     ''1
        autorizacion = ""
        razonSocial = ""
        comercio = ""
        direccion = ""
        Afiliacion = ""
        Serie = ""
        fhTransaccion = ""
        Hora_A = ""
        tarjeta = ""
        fhVencimiento = ""
        emisor = ""
        operacion = ""
        monto_A = ""
        ingreso = ""
        Referencia = ""
        criptograma = ""
        Titular = ""
        firma = ""
        mensaje = ""
        montoPesos = ""
        montoPuntos = ""
        montoTotal = ""
        leyenda2 = ""
        saldoAnteriorPuntos = ""
        saldoAnteriorPesos = ""
        saldoRedimidoPuntos = ""
        saldoRedimidoPesos = ""
        saldoActualPuntos = ""
        saldoActualPesos = ""
        vigenciaPuntos = ""
    End Sub
    ''New
    ''New 23Jul2013
    Sub Busca_Cheques_Lote()
        ReadFile()
        Email_Administrador = "ramon.padilla@alsea.com.mx"
        ''Respuesta = LeeTarjeta.enviar(string_xml, entidad, Email_Administrador)
    End Sub
    Private Sub SQLReadAutorizaciones()
        Try
            Permite_Conexcion_Lotes_Micros = False
            Salir = False
            Contador_Registros_Lote = 0
            ODBC_ConnectStringMICROS_Lotes()
            If Permite_Conexcion_Lotes_Micros = True Then
                Campo = "SELECT * FROM custom.Cierre_Lotes_Pinpad "
                Campo = Campo & " WHERE Tipo_Registro In ('Autorizacion VM', 'Autorizacion AM' ) "
                Campo = Campo & "ORDER BY Referencia"
                Dim Command As New OdbcCommand(Campo, ConexcionMCRSPOS)
                Command.CommandType = CommandType.Text
                Registro_Log = ("SQLReadAutorizaciones: " & Campo)
                ReadSql_odbc = Command.ExecuteReader(CommandBehavior.CloseConnection)
                While ReadSql_odbc.Read = True
                    Contador_Registros_Lote = (Contador_Registros_Lote + 1)
                    Cheques_Lotes(Contador_Registros_Lote) = ReadSql_odbc.GetValue(0)
                End While
            Else
                Salir = True
            End If
        Catch Errores_SQLSearchCanceladosOffLine As Exception
            Registro_Log = "Errores_SQLSearchCanceladosOffLine               :" + Errores_SQLSearchCanceladosOffLine.ToString
            'EventLog.WriteEntry(Registro_Log)
        Finally
            If Salir = False Then
                ReadSql_odbc.Close()
            End If
        End Try
    End Sub
    Private Sub ODBC_ConnectStringMICROS_Lotes()
        Try
            If ConexcionMCRSPOS.State = ConnectionState.Closed Then
                ''ConexcionMCRSPOS.ConnectionString = "DSN=Micros;UID=dba;PWD=smc9nair;Mode=Read"
                'ConexcionMCRSPOS.ConnectionString = "DSN=Micros;UID=dba;PWD=Db@M@5t3r$;Mode=Read"
                ConexcionMCRSPOS.ConnectionString = "DSN=Micros;UID=dba;PWD=Password1;Mode=Read"
                ''ConexcionMCRSPOS.ConnectionString = "DSN=Micros;UID=dba;PWD=Micros3998.;Mode=Read"
                ConexcionMCRSPOS.Open()
            End If
            Permite_Conexcion_Lotes_Micros = True
        Catch Error_ODBC_ConnectStringMICROS_Lotes As Exception
            Permite_Conexcion_Lotes_Micros = False
            Registro_Log = "Error_ODBC_ConnectStringMICROS_Lotes              :" + Permite_Conexcion_MCRSPOS.ToString + " " + Error_ODBC_ConnectStringMICROS_Lotes.ToString
            Write_event_log_error_file(Registro_Log)
        End Try
    End Sub
    Sub cambiaCaracter31()
        Registro_Log = "cambiaCaracter31 Antes  :<" + Campos(31) + ">  Len: " + Len(Campos(31)).ToString
        AppendLog(Registro_Log)

        Hexadecimal(Campos(31))
        Registro_Log = "cambiaCaracter Hexad  :<" + campo_hex
        AppendLog(Registro_Log)

        If Campos(31).Length > 0 Then
            For Inicio_caracter = 1 To Len(Campos(31))
                If Mid(Campos(31), Inicio_caracter, 1) = Chr(29) Then
                    Mid(Campos(31), Inicio_caracter, 1) = Chr(0)
                End If
            Next
        End If
        Registro_Log = "cambiaCaracter31 Despues:<" + Campos(31) + ">  Len: " + Len(Campos(31)).ToString
        AppendLog(Registro_Log)

        Hexadecimal(Campos(31))
        Registro_Log = "cambiaCaracter Hexad  :<" + campo_hex
        AppendLog(Registro_Log)

    End Sub
    Sub cambiaCaracter19()
        Registro_Log = "cambiaCaracter19 Antes  :<" + Campos(19) + ">  Len: " + Len(Campos(19)).ToString
        AppendLog(Registro_Log)

        Hexadecimal(Campos(19))
        Registro_Log = "cambiaCaracter Hexad  :<" + campo_hex
        AppendLog(Registro_Log)

        If Campos(19).Length > 0 Then
            For Inicio_caracter = 1 To Len(Campos(19))
                If Mid(Campos(19), Inicio_caracter, 1) = Chr(29) Then
                    Mid(Campos(19), Inicio_caracter, 1) = Chr(0)
                End If
            Next
        End If
        Registro_Log = "cambiaCaracter19 Despues :<" + Campos(19) + ">  Len: " + Len(Campos(19)).ToString
        AppendLog(Registro_Log)

        Hexadecimal(Campos(19))
        Registro_Log = "cambiaCaracter Hexad  :<" + campo_hex
        AppendLog(Registro_Log)
    End Sub
    Sub cambiaCaracter18()
        Registro_Log = "cambiaCaracter18 Antes  :<" + Campos(18) + ">  Len: " + Len(Campos(18)).ToString
        AppendLog(Registro_Log)

        Hexadecimal(Campos(18))
        Registro_Log = "cambiaCaracter Hexad  :<" + campo_hex
        AppendLog(Registro_Log)

        If Campos(18).Length > 0 Then
            For Inicio_caracter = 1 To Len(Campos(18))
                If Mid(Campos(18), Inicio_caracter, 1) = Chr(29) Then
                    Mid(Campos(18), Inicio_caracter, 1) = Chr(0)
                End If
            Next
        End If
        Registro_Log = "cambiaCaracter18 Despues :<" + Campos(18) + ">  Len: " + Len(Campos(18)).ToString
        AppendLog(Registro_Log)

        Hexadecimal(Campos(18))
        Registro_Log = "cambiaCaracter Hexad  :<" + campo_hex
        AppendLog(Registro_Log)
    End Sub
    Sub cambiaCaracterSflag()
        Registro_Log = "cambiaCaracterSflag Antes  :<" + sFlag + ">  Len: " + Len(sFlag).ToString
        AppendLog(Registro_Log)

        Hexadecimal(sFlag)
        Registro_Log = "cambiaCaracter Hexad  :<" + campo_hex
        AppendLog(Registro_Log)

        If sFlag.Length > 0 Then
            For Inicio_caracter = 1 To Len(sFlag)
                If Mid(sFlag, Inicio_caracter, 1) = Chr(29) Then
                    Mid(sFlag, Inicio_caracter, 1) = Chr(0)
                End If
            Next
        End If
        Registro_Log = "cambiaCaracterSflag Despues :<" + sFlag + ">  Len: " + Len(sFlag).ToString
        AppendLog(Registro_Log)

        Hexadecimal(sFlag)
        Registro_Log = "cambiaCaracter Hexad  :<" + campo_hex
        AppendLog(Registro_Log)
    End Sub
    Sub cambiaCaracter20()
        Registro_Log = "cambiaCaracter20 Antes  :<" + Campos(20) + ">  Len: " + Len(Campos(20)).ToString
        AppendLog(Registro_Log)

        Hexadecimal(Campos(20))
        Registro_Log = "cambiaCaracter Hexad  :<" + campo_hex
        AppendLog(Registro_Log)

        If Campos(20).Length > 0 Then
            For Inicio_caracter = 1 To Len(Campos(20))
                If Mid(Campos(20), Inicio_caracter, 1) = Chr(29) Then
                    Mid(Campos(20), Inicio_caracter, 1) = Chr(0)
                End If
            Next
        End If
        Registro_Log = "cambiaCaracter20 Despues:<" + Campos(20) + ">  Len: " + Len(Campos(20)).ToString
        AppendLog(Registro_Log)

        Hexadecimal(Campos(20))
        Registro_Log = "cambiaCaracter Hexad  :<" + campo_hex
        AppendLog(Registro_Log)
    End Sub
    Sub cambiaCaracter17()
        Registro_Log = "cambiaCaracter17 Antes  :<" + Campos(17) + ">  Len: " + Len(Campos(17)).ToString
        AppendLog(Registro_Log)

        Hexadecimal(Campos(17))
        Registro_Log = "cambiaCaracter Hexad  :<" + campo_hex
        AppendLog(Registro_Log)

        If Campos(17).Length > 0 Then
            For Inicio_caracter = 1 To Len(Campos(17))
                If Mid(Campos(17), Inicio_caracter, 1) = Chr(29) Then
                    Mid(Campos(17), Inicio_caracter, 1) = Chr(0)
                End If
            Next
        End If
        Registro_Log = "cambiaCaracter17 Despues:<" + Campos(17) + ">  Len: " + Len(Campos(17)).ToString
        AppendLog(Registro_Log)

        Hexadecimal(Campos(17))
        Registro_Log = "cambiaCaracter Hexad  :<" + campo_hex
        AppendLog(Registro_Log)
    End Sub
    Sub Hexadecimal(ByVal valor_ As String)
        caracter_str = ""
        caracter_int = 0
        contador_hex2 = 0
        campo_hex = ""
        For contador_hex = 1 To valor_.Length
            caracter_str = Mid(valor_, contador_hex, 1)
            caracter_int = Asc(caracter_str)
            contador_hex2 = contador_hex2 + 1
            campo_hex = campo_hex + Hex(caracter_int) + " "
        Next
        campo_hex = campo_hex + " Len: " + contador_hex2.ToString
    End Sub
    ''New 23Jul2013

    '26 Nov 2013
    Sub Arma_Reverso_Datos()
        'Numero_WSID
        campos_Reverso(Numero_WSID) = ""

    End Sub
    Sub Arma_Reverso_Vacio()
        'Numero_WSID
        campos_Reverso(Numero_WSID) = ""

    End Sub


    'ws get_member
    Sub Procesa_Busqueda_Miembro()
        Try
            'Write_event_log_error_file(Mid(Datos, 1, 3))
            Campos_Paso = Datos.Split("|")
            numero_Miembro = Campos_Paso(1)
            Business_date = Campos_Paso(2)
            Tienda = Campos_Paso(3)
            CentroConsumo = Campos_Paso(4)
            Cheque = Campos_Paso(5)
            'Write_event_log_error_file(numero_Miembro)
            'ws.busca_numero_miembro_Recompensas(,,,
            'Respuesta1 = Ws.busca_numero_miembro("C:\\WS\\ALSE LOY Servicios Miembro.WSDL", "ConsultaMiembro", "myStarbucks Rewards Mexico", numero_Miembro)
            'Nombre_Ws = Path_WS + "ALSE_LOY_Servicios_Miembro.WSDL"

            'Respuesta1 = Ws.busca_numero_miembro(Nombre_Ws, "ConsultaMiembro", "myStarbucks Rewards Mexico", numero_Miembro)

            Respuesta1 = CrmSoaw.main_crmsoa_getMember(numero_Miembro, sUri, campo2, campo3, campo4, campo5, campo6, campo7, campo9, campo10, timeOut_Ws)

            'Write_event_log_error_file(Respuesta1)
            'Valor0 = Replace(Respuesta1, "'", "!")
            'Respuesta1 = Valor0
            Error_Field = "Procesa_Busqueda_Miembro. Respuesta1:" + Respuesta1
            Write_event_log_error_file(Error_Field)

            If Respuesta1 = "NO HAY COMUNICACION CON WS" Then
                Respuesta2 = "NO HAY COMUNICACION CON WS [Consulta Miembro]"
                Exit Sub
            End If

            elimina_valores()

            Error_Field = "Procesa_Busqueda_Miembro. elimina_valores:" + Respuesta1
            Write_event_log_error_file(Error_Field)

            ''New
            'Respuesta11 = Respuesta1.Split("|")
            'If Respuesta11.Length > 0 Then
            'Respuesta1 = Respuesta11(0)
            'End If
            ''New

            error_ws = " "
            Separa_Campos_Xml_Individuales()


            Error_Field = "Procesa_Busqueda_Miembro. Separa_Campos_Xml_Individuales:Numero_Tarjeta:" + Numero_Tarjeta + " " + Nombre
            Write_event_log_error_file(Error_Field)
            ''New
            'If Respuesta11.Length > 0 And Respuesta11.Length < 2 Then
            'Respuesta1 = Respuesta11(0)
            'End If
            'If Respuesta11.Length > 1 And Respuesta11.Length < 3 Then
            'Respuesta1 = Respuesta11(0) & "|" & Respuesta11(1)
            'End If
            'If Respuesta11.Length > 2 And Respuesta11.Length < 4 Then
            'Respuesta1 = Respuesta11(0) & "|" & Respuesta11(1) & "|" & Respuesta11(2)
            'End If
            ''New

            SQLInsert_Datos_CRM()

            Error_Field = "Procesa_Busqueda_Miembro. SQLInsert_Datos_CRM:" + Numero_Tarjeta + " " + Nombre
            Write_event_log_error_file(Error_Field)

            ''New
            'If Respuesta11.Length > 0 Then
            'Respuesta1 = Respuesta11(0)
            'End If
            ''New
            'Error_Field = "Procesa_Busqueda_Miembro. Respuesta11.Length2:" + Respuesta11.Length.ToString
            'Write_event_log_error_file(Error_Field)

            numero_Miembro = numeroCliente

            Error_Field = "Procesa_Busqueda_Miembro. numero_Miembro:" + numero_Miembro
            Write_event_log_error_file(Error_Field)

            Error_Field = "Procesa_Busqueda_Miembro. Nombre:" + Nombre
            Write_event_log_error_file(Error_Field)

            If Len(Numero_Tarjeta) > 0 And Len(Nombre) > 0 Then
                ''Respuesta2 = Numero_Tarjeta & Separador
                Respuesta2 = numero_Miembro & Separador
                Respuesta2 = Respuesta2 & Nombre & Separador
                Respuesta2 = Respuesta2 & Card_Status & Separador
                Respuesta2 = Respuesta2 & Nivel & Separador
                Respuesta2 = Respuesta2 & Organizacion & Separador
                Respuesta2 = Respuesta2 & NivelId & Separador
                Respuesta2 = Respuesta2 & NivelNombre & Separador
                ''Respuesta2 = Respuesta2 & LifetimePoint1Value & Separador
                Respuesta2 = Respuesta2 & Contador_Promociones.ToString & Separador
                For I = 1 To Contador_Promociones
                    Respuesta2 = Respuesta2 & Convert.ToString(Promociones(I)).ToString & Separador
                Next I
                Respuesta2 = Respuesta2 & Contador_Voucher.ToString & Separador
                For I = 1 To Contador_Voucher
                    Respuesta2 = Respuesta2 & Convert.ToString(Voucher(I)).ToString & Separador
                Next I
                Respuesta2 = Respuesta2 & BirthDate & Separador & ALSEFavoriteProduct & Separador & Last_Beverage & Separador & Last_Food & Separador & LifetimePoint1Value & Separador & LifetimePoint2Value & Separador & Status & Separador & AttributeValue & Separador & errorCode & Separador & pointAmount

            ElseIf Len(error_ws) > 0 Then '' = "No es posible conectar con el servidor remoto" Or error_ws.Substring(0, 38) = "No se puede resolver el nombre remoto:" Or error_ws = "Error en el servidor remoto: (500) Error interno del servidor." Or error_ws = "Se excedi� el tiempo de espera de la operaci�n" Then
                Respuesta2 = "NO HAY COMUNICACION CON WS [Consulta Miembro]" & "|" & errorCode
            Else
                Respuesta2 = "NO EXISTE MIEMBRO"
            End If
        Catch exc As Exception
            ' Show the exception to the user.
            Error_Field = "Procesa_Busqueda_Miembro." + vbCrLf + "Exception: " + exc.Message
            Write_event_log_error_file(Error_Field)
        Finally
        End Try
    End Sub
    Public Sub elimina_valores()
        Dim paso As String
        paso = " xmlns:ns1="
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = " xmlns:ns2="
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = " xmlns:ns3="
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = " xmlns:ns4="
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = " xmlns:ns5="
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = " xmlns:ns6="
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = " xmlns:ns7="
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = " xmlns:ns8="
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = " xmlns:ns9="
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = " xmlns:ns10="
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = " xmlns:ns11="
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = " xmlns:ns12="
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = " xmlns:ns13="
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = " xmlns:ns14="
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = " xmlns:ns15="
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0


        paso = "http://services.alsea.com/Loyalty/Schema/ALSEAPerson"
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = "http://services.alsea.com/Loyalty/Schema/ALSEAMember"
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = "http://services.alsea.com/Loyalty/Schema/ALSEAContact"
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = "http://services.alsea.com/Loyalty/Schema/ALSEAAddress"
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = "http://services.alsea.com/Loyalty/Schema/ALSEAPhone"
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = "http://services.alsea.com/Loyalty/Schema/ALSEAContactAlternative"
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = "http://services.alsea.com/Loyalty/Schema/ALSEACard"
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = "http://servces.alsea.com/Loyalty/Schema/ALSEAProgram"
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = "http://services.alsea.com/Loyalty/Schema/ALSEAPoints"
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = "http://services.alsea.com/Loyalty/Schema/ALSEAVoucher"
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = "ns1:"
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = "ns2:"
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = "ns3:"
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = "ns4:"
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = "ns5:"
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = "ns6:"
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = "ns7:"
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = "ns8:"
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = "ns9:"
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = "ns10:"
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = "ns11:"
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = "ns12:"
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = "ns13:"
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = "ns14:"
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = "ns15:"
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0

        paso = "als8:"
        Valor0 = Replace(Respuesta1, paso, "")
        Respuesta1 = Valor0


        Valor0 = Replace(Respuesta1, Chr(34), "")
        Respuesta1 = Valor0

        Valor0 = Replace(Respuesta1, Chr(13), "")
        Respuesta1 = Valor0

        Valor0 = Replace(Respuesta1, Chr(10), "")
        Respuesta1 = Valor0

    End Sub
    Sub Separa_Campos_Xml_Individuales()
        Try
            Inicio_L = 0
            Contador_Voucher = 0
            Contador_Promociones = 0
            Error_Field = "Separa_Campos_Xml_Individuales.: errorDescription:" + error_ws
            Write_event_log_error_file(Error_Field)

            Separa_Campos_Individules("<als10:errorCode>", errorCode)

            Error_Field = "Separa_Campos_Xml_Individuales.: als10:errorCode: " + errorCode
            Write_event_log_error_file(Error_Field)


            If Len(error_ws) > 1 Then
                Borra_Totales_Busqueda_Xml()
                Borra_Totales_Busqueda_Voucher_Xml()
                Exit Sub
            End If
            Separa_Campos_Individules("<als10:errorDescription>", error_ws)

            Error_Field = "Separa_Campos_Xml_Individuales.: error_ws.Length.ToString: " + Len(error_ws).ToString
            Write_event_log_error_file(Error_Field)

            If Len(error_ws) > 0 Then
                Borra_Totales_Busqueda_Xml()
                Borra_Totales_Busqueda_Voucher_Xml()
                Exit Sub
            End If

            Error_Field = "Separa_Campos_Xml_Individuales.: Len(error_ws): " + Len(error_ws).ToString
            Write_event_log_error_file(Error_Field)

            Separa_Campos_Individules("<name>", Nombre)
            Separa_Campos_Individules("<lastName>", lastName)
            Separa_Campos_Individules("<favoriteProduct>", ALSEFavoriteProduct)
            Separa_Campos_Individules("<description>", LifetimePoint1Value)

            '18Ene2015
            Separa_Campos_Individules_Indice("<earnedPoints>", LifetimePoint2Value, Inicio_L)
            '18Ene2015

            '21Jul2015
            Separa_Campos_Individules_Indice("<pointAmount>", pointAmount, Inicio_L)
            '21Jul2015

            '18Ene2015 Separa_Campos_Individules("<description>", LifetimePoint2Value)

            Separa_Campos_Individules("<cardNumber>", Numero_Tarjeta)
            Separa_Campos_Individules("<cardStatusCode>", Card_Status)
            Separa_Campos_Individules("<tierName>", Nivel)
            Separa_Campos_Individules("<Organization>", Organizacion)
            Separa_Campos_Individules("<tierName>", NivelId)
            Separa_Campos_Individules("<memberStatus>", Status)
            Separa_Campos_Individules("<tierName>", NivelNombre)
            Separa_Campos_Individules("<memberNumber>", numeroCliente)
            Numero_Tarjeta = numeroCliente
            Nombre = Nombre & " " & lastName
            Card_Status = Status
            ''Separa_Campos_Individules("<earnedPoints>", earnedPoints)

            Inicio_L3 = 0
            Separa_Campos_Individules_Indice_Uno("<memberStatus>", Status, Inicio_L3)
            Respuesta3 = "Status :" & Status & "   " & Inicio_L3.ToString
            'Write_event_log_error_file(Respuesta3)

            Inicio_L4 = 0
            Separa_Campos_Individules_Indice_Uno("<Status />", Status2, Inicio_L4)
            Respuesta3 = "Status /:" & Status2 & "   " & Inicio_L4.ToString
            'Write_event_log_error_file(Respuesta3)

            If Inicio_L3 > Inicio_L4 And Inicio_L4 > 0 Then
                Status = "Sin Status"
                Status2 = ""
            End If

            Inicio_L = 0
            Separa_Campos_Individules_Favorite("<Value>", "Last Beverage", "<Value />", Last_Beverage, Inicio_L)
            Inicio_L = 0
            Separa_Campos_Individules_Favorite("<Value>", "Last Food", "<Value />", Last_Food, Inicio_L)

            Inicio_L = 0
            Separa_Campos_Individules_Favorite("<AttributeValue>", "SBX MEX Redemption � Birthdate Beverage", "<AttributeValue />", AttributeValue, Inicio_L)

            Inicio_L = 0
            Separa_Campos_Individules_Indice("<TierName>", NivelNombre, Inicio_L)
            Inicio_L2 = Inicio_L
            ''Promotion
            For I = 1 To 20
                Borra_Totales_Busqueda_Xml()
                Separa_Campos_Individules_Indice("<PromotionId>", PromocionId, Inicio_L)
                Separa_Campos_Individules_Indice("<PromotionName>", PromocionName, Inicio_L)
                Separa_Campos_Individules_Indice("<UniqueKey>", PromocionUniqueKey, Inicio_L)
                Separa_Campos_Individules_Indice("<DisplayName>", PromocionDisplayName, Inicio_L)
                Separa_Campos_Individules_Indice("<Status>", PromocionStatus, Inicio_L)
                If PromocionId.Length <> 0 And PromocionName.Length <> 0 Then
                    Contador_Promociones = (Contador_Promociones + 1)
                    Promociones(Contador_Promociones) = PromocionId & "|" & PromocionName & "|" & PromocionUniqueKey & "|" & PromocionDisplayName & "|" & PromocionStatus
                Else
                    Exit For
                End If
            Next I
            ''Voucher
            Dim voucher_paso As String = ""
            Separa_Campos_Individules("<vouchersList>", voucher_paso)
            ''08Abr2015 Inicio_L2 = Inicio_L
            Inicio_L2 = (Inicio_L - 1)  ''08Abr2015

            ''08Abr2015
            ''Separa_vouchers(Respuesta1_voucher)
            Busca_Final_Vocuher(Inicio_L2, Final_Vocuher)
            ''08Abr2015 I = 20
            ''08Abr2015

            ''06Mar2015 If I = 40 Then
            For I = 1 To 9
                ''08Abr2015 For I = 1 to 20
                Borra_Totales_Busqueda_Voucher_Xml()
                ''08Abr2015 Separa_Campos_Individules_Indice("<productId>", VoucherProductId, Inicio_L2)
                ''08Abr2015 Separa_Campos_Individules_Indice("<productName>", VoucherProductName, Inicio_L2)
                ''08Abr2015 Separa_Campos_Individules_Indice("<status>", VoucherStatus, Inicio_L2)
                ''08Abr2015 Separa_Campos_Individules_Indice("<voucherNumber>", VoucherTransactionId, Inicio_L2)
                ''08Abr2015 Separa_Campos_Individules_Indice("<validDiscountAmount>", validDiscountAmount, Inicio_L2)
                ''08Abr2015 Separa_Campos_Individules_Indice("<validDiscountType>", validDiscountType, Inicio_L2)
                'Separa_Campos_Individules_Indice("<validFrecAttibuteType>", VoucherCalStatus, Inicio_L2)

                Separa_Campos_Individules_Indice_Voucher("<productId>", VoucherProductId, Inicio_L2)
                Separa_Campos_Individules_Indice_Voucher("<productName>", VoucherProductName, Inicio_L2)
                Separa_Campos_Individules_Indice_Voucher("<status>", VoucherStatus, Inicio_L2)
                Separa_Campos_Individules_Indice_Voucher("<voucherNumber>", VoucherTransactionId, Inicio_L2)
                Separa_Campos_Individules_Indice_Voucher("<validDiscountAmount>", validDiscountAmount, Inicio_L2)
                Separa_Campos_Individules_Indice_Voucher("<validDiscountType>", validDiscountType, Inicio_L2)
                Separa_Campos_Individules_Indice_Voucher("<validFrecAttibuteType>", validFrecAttibuteType, Inicio_L2)
                Separa_Campos_Individules_Indice_Voucher("<minimumAmount>", minimumAmount, Inicio_L2)

                ''08Abr2015
                Busca_Final_Vocuher(Inicio_L2, Final_Vocuher)

                If Len(validDiscountType) = 0 Then
                    validDiscountType = "Vacio"
                End If
                If Len(validDiscountAmount) = 0 Then
                    validDiscountAmount = "0"
                End If
                ''08Abr2015

                If VoucherCalStatus.Length = 0 Then
                    VoucherCalStatus = "."
                End If
                If VoucherStatus.Length <> 0 And VoucherProductName.Length <> 0 Then
                    Contador_Voucher = (Contador_Voucher + 1)
                    Voucher(Contador_Voucher) = VoucherCalStatus & "|" & VoucherProductId & "|" & VoucherProductName & "|" & VoucherStatus & "|" & VoucherTransactionId & "|" & validDiscountAmount & "|" & validDiscountType & "|" & minimumAmount
                Else
                    Exit For
                End If
            Next I
            ''BirthDate
            Inicio_L2 = 0
            Separa_Campos_Individules_Indice("<name>", ListOfLOYMember_Contact, Inicio_L2)
            Separa_Campos_Individules_Indice("<birthday>", BirthDate, Inicio_L2)
        Catch exc As Exception
            ' Show the exception to the user.
            Error_Field = "Separa_Campos_Xml_Individuales." + vbCrLf + "ERROR: " + exc.Message
            Write_event_log_error_file(Error_Field)
        Finally
        End Try
    End Sub
    Sub Separa_Campos_Xml_Individuales_Acreditaciones()
        Inicio_L = 0
        Contador_Voucher = 0
        Contador_Promociones = 0
        If Len(error_ws) > 1 Then
            Borra_Totales_Busqueda_Xml()
            Borra_Totales_Busqueda_Voucher_Xml()
            Exit Sub
        End If
        Separa_Campos_Individules("<als10:errorDescription>", error_ws)
        If Len(error_ws) > 0 Then
            Borra_Totales_Busqueda_Xml()
            Borra_Totales_Busqueda_Voucher_Xml()
            Exit Sub
        End If
        Separa_Campos_Individules("<name>", Nombre)
        Separa_Campos_Individules("<favoriteProduct>", ALSEFavoriteProduct)
        Separa_Campos_Individules("<earnedPoints>", LifetimePoint1Value)
        Separa_Campos_Individules("<earnedPoints>", LifetimePoint2Value)
        Separa_Campos_Individules("<cardNumber>", Numero_Tarjeta)
        Separa_Campos_Individules("<cardStatusCode>", Card_Status)
        Separa_Campos_Individules("<tierName>", Nivel)
        Separa_Campos_Individules("<Organization>", Organizacion)
        Separa_Campos_Individules("<tierName>", NivelId)
        Separa_Campos_Individules("<memberStatus>", Status)
        Separa_Campos_Individules("<tierName>", NivelNombre)

        Inicio_L3 = 0
        Separa_Campos_Individules_Indice_Uno("<memberStatus>", Status, Inicio_L3)
        Respuesta3 = "Status :" & Status & "   " & Inicio_L3.ToString
        'Write_event_log_error_file(Respuesta3)

        Inicio_L4 = 0
        Separa_Campos_Individules_Indice_Uno("<Status />", Status2, Inicio_L4)
        Respuesta3 = "Status /:" & Status2 & "   " & Inicio_L4.ToString
        'Write_event_log_error_file(Respuesta3)

        If Inicio_L3 > Inicio_L4 And Inicio_L4 > 0 Then
            Status = "Sin Status"
            Status2 = ""
        End If

        Inicio_L = 0
        Separa_Campos_Individules_Favorite("<Value>", "Last Beverage", "<Value />", Last_Beverage, Inicio_L)
        Inicio_L = 0
        Separa_Campos_Individules_Favorite("<Value>", "Last Food", "<Value />", Last_Food, Inicio_L)

        Inicio_L = 0
        Separa_Campos_Individules_Favorite("<AttributeValue>", "SBX MEX Redemption � Birthdate Beverage", "<AttributeValue />", AttributeValue, Inicio_L)

        Inicio_L = 0
        Separa_Campos_Individules_Indice("<TierName>", NivelNombre, Inicio_L)
        Inicio_L2 = Inicio_L
        ''Promotion
        For I = 1 To 20
            Borra_Totales_Busqueda_Xml()
            Separa_Campos_Individules_Indice("<PromotionId>", PromocionId, Inicio_L)
            Separa_Campos_Individules_Indice("<PromotionName>", PromocionName, Inicio_L)
            Separa_Campos_Individules_Indice("<UniqueKey>", PromocionUniqueKey, Inicio_L)
            Separa_Campos_Individules_Indice("<DisplayName>", PromocionDisplayName, Inicio_L)
            Separa_Campos_Individules_Indice("<Status>", PromocionStatus, Inicio_L)
            If PromocionId.Length <> 0 And PromocionName.Length <> 0 Then
                Contador_Promociones = (Contador_Promociones + 1)
                Promociones(Contador_Promociones) = PromocionId & "|" & PromocionName & "|" & PromocionUniqueKey & "|" & PromocionDisplayName & "|" & PromocionStatus
            Else
                Exit For
            End If
        Next I
        ''Voucher
        Dim voucher_paso As String = ""
        Separa_Campos_Individules("<vouchersList>", voucher_paso)
        Inicio_L2 = Inicio_L
        For I = 1 To 20
            Borra_Totales_Busqueda_Voucher_Xml()
            Separa_Campos_Individules_Indice("<productId>", VoucherProductId, Inicio_L2)
            Separa_Campos_Individules_Indice("<productName>", VoucherProductName, Inicio_L2)
            Separa_Campos_Individules_Indice("<status>", VoucherStatus, Inicio_L2)
            Separa_Campos_Individules_Indice("<voucherNumber>", VoucherTransactionId, Inicio_L2)
            Separa_Campos_Individules_Indice("<validFrecAttibuteType>", VoucherCalStatus, Inicio_L2)
            If VoucherCalStatus.Length <> 0 And VoucherProductName.Length <> 0 Then
                Contador_Voucher = (Contador_Voucher + 1)
                Voucher(Contador_Voucher) = VoucherCalStatus & "|" & VoucherProductId & "|" & VoucherProductName & "|" & VoucherStatus & "|" & VoucherTransactionId
            Else
                Exit For
            End If
        Next I
        ''BirthDate
        Inicio_L2 = 0
        Separa_Campos_Individules_Indice("<name>", ListOfLOYMember_Contact, Inicio_L2)
        Separa_Campos_Individules_Indice("<birthday>", BirthDate, Inicio_L2)
    End Sub


    Private Sub SQLInsert_Datos_CRM()
        Try
            Insert_Datos_CRM = False
            Permite_Conexcion_CRM_Micros = False
            Salir = False
            ODBC_ConnectStringMICROS()
            If Permite_Conexcion_CRM_Micros = True Then
                InsertTicket = "INSERT INTO custom.CRM_Respuesta(Fecha, Tienda, CentroConsumo, Cheque, NumeroMiembro, Respuesta ) VALUES("
                InsertTicket = InsertTicket & "'" & Business_date & "',"
                InsertTicket = InsertTicket & " " & Convert.ToInt32(Tienda) & ","
                InsertTicket = InsertTicket & " " & Convert.ToInt32(CentroConsumo) & ","
                InsertTicket = InsertTicket & " " & Convert.ToInt32(Cheque) & ","
                InsertTicket = InsertTicket & " '" & numero_Miembro & "',"
                InsertTicket = InsertTicket & " '" & Respuesta1 & "' "
                'InsertTicket = InsertTicket & " '" & Card_Status & "', "
                'InsertTicket = InsertTicket & " '" & Nivel & "', "
                'InsertTicket = InsertTicket & " '" & Organizacion & "', "
                'InsertTicket = InsertTicket & " '" & NivelId & "', "
                'InsertTicket = InsertTicket & " '" & NivelNombre & "' "
                InsertTicket = InsertTicket & " ) "
                Write_event_log_error_file(InsertTicket)
                Dim Command As New OdbcCommand(InsertTicket, ConexcionMCRSPOS)
                Command.CommandType = CommandType.Text
                ReadSql = Command.ExecuteScalar
                Insert_Datos_CRM = True
            End If
        Catch Errores_SQLInsert_Ticket_CFD As Exception
            Registro_Log = "Errores_SQLInsert_Ticket_CFD              :" + Errores_SQLInsert_Ticket_CFD.ToString
            Insert_Datos_CRM = False
            Write_event_log_error_file(Registro_Log)
        Finally
            ODBC_ConnectStringMicrosClose()
        End Try
    End Sub
    'ws get_member
    'ws Accrual
    Sub Procesa_Acreditacion_Ticket()
        Try

            Campos_Paso = Datos.Split("|")
            Id = Campos_Paso(1)

            SQLUpdate_Acreditaciones_Ticket2_CRM()

            'Business_date = Campos_Paso(2)
            'Tienda = Campos_Paso(3)
            'CentroConsumo = Campos_Paso(4)
            'Cheque = Campos_Paso(5)

            'grabaXmlToSend_Accrual()
            'ExecutateWsdlSoaAccrual()


            Respuesta1 = CrmSoaw.main_crmsoa_Acreditacion(Campos_Paso, Id, sUri, campo2, campo3, campo4, campo5, campo6, campo7, campo9, campo10, timeOut_Ws)

            If Respuesta1.IndexOf("Error:") > -1 Then
                Respuesta2 = "NO HAY COMUNICACION CON WS [ACREDITACION]"
            Else
                elimina_valores()

                ''New
                'Respuesta11 = Respuesta1.Split("|")
                'If Respuesta11.Length > 0 Then
                'Respuesta1 = Respuesta11(0)
                'End If
                ''New


                Separa_Campos_Xml_Individuales_Redenciones()

                Respuesta2 = "ACREDITACION OK"

                If errorDescription.Length > 0 Or faultstring.Length > 0 Then
                    'Respuesta2 = errorDescription & Separador & faultstring & Separador
                    Respuesta2 = "NO SE REALIZO LA ACREDITACION" & "|" & errorCode
                ElseIf detail.Substring(0, 16) = "Se ha almacenado" Then
                    Respuesta2 = "ACREDITACION OK"
                ElseIf Len(error_ws) > 0 And Len(detail) = 0 Then '' = "No es posible conectar con el servidor remoto" Or error_ws.Substring(0, 38) = "No se puede resolver el nombre remoto:" Or error_ws = "Error en el servidor remoto: (500) Error interno del servidor." Or error_ws = "Se excedi� el tiempo de espera de la operaci�n" Then
                    Respuesta2 = "NO HAY COMUNICACION CON WS [ACREDITACION]" & "|" & errorCode
                ElseIf Respuesta1.Length = 0 Then
                    errorCode = "99999"
                    Respuesta2 = "NO HAY COMUNICACION CON WS [ACREDITACION:]" & "|" & errorCode
                Else
                    Respuesta2 = "ERROR EN ACREDITACION" & "|" & errorCode
                End If
            End If
        Catch exc As Exception
            ' Show the exception to the user.
            Error_Field = "Procesa_Redencion_Ticket." + vbCrLf + "Exception: " + exc.Message
            'Write_event_log_error_file(Error_Field)
        Finally
        End Try

    End Sub
    Private Sub SQLUpdate_Acreditaciones_Ticket2_CRM()
        Try
            Insert_Datos_CRM = False
            Permite_Conexcion_CRM_Micros = False
            Salir = False
            ODBC_ConnectStringMICROS()
            If Permite_Conexcion_CRM_Micros = True Then
                InsertTicket = "Update custom.LOYTRANSACCION set MemberNumber2 = '" & Campos_Paso(2) & "', cardNumber = '" & Campos_Paso(3) & "' WHERE TicketNumber = '" & Id & "' "
                'Write_event_log_error_file(InsertTicket)
                Dim Command As New OdbcCommand(InsertTicket, ConexcionMCRSPOS)
                Command.CommandType = CommandType.Text
                ReadSql = Command.ExecuteScalar
                Insert_Datos_CRM = True
            End If
        Catch Errores_SQLUpdate_Acreditaciones_Ticket2_CRM As Exception
            Registro_Log = "Errores_SQLUpdate_Acreditaciones_Ticket2_CRM              :" + Errores_SQLUpdate_Acreditaciones_Ticket2_CRM.ToString
            Insert_Datos_CRM = False
            'Write_event_log_error_file(Registro_Log)
        Finally
            ODBC_ConnectStringMicrosClose()
        End Try
    End Sub

    '27Ene2015
    Private Sub SQLUpdate_Redime_Ticket2_CRM()
        Try
            If Len(Campos_Paso(40)) = 0 Then
                Campos_Paso(40) = Campos_Paso(1)
            End If
            Insert_Datos_CRM = False
            Permite_Conexcion_CRM_Micros = False
            Salir = False
            ODBC_ConnectStringMICROS()
            If Permite_Conexcion_CRM_Micros = True Then
                InsertTicket = "Update custom.LOYTRANSACCION set MemberNumber2 = '" & Campos_Paso(40) & "', cardNumber = '" & Campos_Paso(40) & "' WHERE TicketNumber = '" & Id2 & "' "
                'Write_event_log_error_file(InsertTicket)
                Dim Command As New OdbcCommand(InsertTicket, ConexcionMCRSPOS)
                Command.CommandType = CommandType.Text
                ReadSql = Command.ExecuteScalar
                Insert_Datos_CRM = True
            End If
        Catch Errores_SQLUpdate_Redime_Ticket2_CRM As Exception
            Registro_Log = "Errores_SQLUpdate_Redime_Ticket2_CRM              :" + Errores_SQLUpdate_Redime_Ticket2_CRM.ToString
            Insert_Datos_CRM = False
            'Write_event_log_error_file(Registro_Log)
        Finally
            ODBC_ConnectStringMicrosClose()
        End Try
    End Sub
    '27Ene2015

    'ws Accrual

    'ws Accrual Tmed
    Sub Procesa_Acreditacion_Ticket_Tmed()
        Try

            Campos_Paso = Datos.Split("/")
            Id = Campos_Paso(1)
            Campo4_Wow = Campos_Paso(4)
            Campos_Paso_WOW = Campo4_Wow.Split("|")

            Error_Field = "LOG Procesa_Acreditacion_Ticket_Tmed 01" + Campos_Paso(1)
            grabaLogSoa(Error_Field)

            Error_Field = "LOG Procesa_Acreditacion_Ticket_Tmed 02" + Campos_Paso(2)
            grabaLogSoa(Error_Field)

            Error_Field = "LOG Procesa_Acreditacion_Ticket_Tmed 03" + Campos_Paso(3)
            grabaLogSoa(Error_Field)

            Error_Field = "LOG Procesa_Acreditacion_Ticket_Tmed 04" + Campos_Paso(4)
            grabaLogSoa(Error_Field)

            Error_Field = "LOG Procesa_Acreditacion_Ticket_Tmed 001" + Campos_Paso_WOW(1)
            grabaLogSoa(Error_Field)

            Error_Field = "LOG Procesa_Acreditacion_Ticket_Tmed 002" + Campos_Paso_WOW(2)
            grabaLogSoa(Error_Field)

            SQLUpdate_Acreditaciones_Ticket2_CRM()

            'Business_date = Campos_Paso(2)
            'Tienda = Campos_Paso(3)
            'CentroConsumo = Campos_Paso(4)
            'Cheque = Campos_Paso(5)

            'grabaXmlToSend_Accrual()
            'ExecutateWsdlSoaAccrual()


            Respuesta1 = CrmSoaw.main_crmsoa_Acreditacion_Tmed(Campos_Paso, Id, sUri, campo2, campo3, campo4, campo5, campo6, campo7, campo9, campo10, timeOut_Ws, Campos_Paso_WOW)

            'If Respuesta1.IndexOf("Error:") > -1 Then
            'Respuesta2 = "NO HAY COMUNICACION CON WS [ACREDITACION]"
            'Else
            elimina_valores()
            ''New
            'Respuesta11 = Respuesta1.Split("|")
            'If Respuesta11.Length > 0 Then
            'Respuesta1 = Respuesta11(0)
            'End If
            ''New
            Separa_Campos_Xml_Individuales_Redenciones()
            Respuesta2 = "ACREDITACION OK"
            If errorDescription.Length > 0 Or faultstring.Length > 0 Then
                'Respuesta2 = errorDescription & Separador & faultstring & Separador
                Respuesta2 = "NO SE REALIZO LA ACREDITACION" & "|" & errorCode
            ElseIf detail.Substring(0, 16) = "Se ha almacenado" Then
                Respuesta2 = "ACREDITACION OK"
            ElseIf Len(error_ws) > 0 And Len(detail) = 0 Then '' = "No es posible conectar con el servidor remoto" Or error_ws.Substring(0, 38) = "No se puede resolver el nombre remoto:" Or error_ws = "Error en el servidor remoto: (500) Error interno del servidor." Or error_ws = "Se excedi� el tiempo de espera de la operaci�n" Then
                Respuesta2 = "NO HAY COMUNICACION CON WS [ACREDITACION]" & "|" & errorCode
            Else
                Respuesta2 = "ERROR EN ACREDITACION" & "|" & errorCode
            End If
            'End If
        Catch exc As Exception
            ' Show the exception to the user.
            Error_Field = "Procesa_Acreditacion_Ticket_Tmed." + vbCrLf + "Exception: " + exc.Message
            Write_event_log_error_file(Error_Field)
        Finally
        End Try

    End Sub
    'ws Accrual Tmed

    Sub kill_proceso()
        Dim p As Process

        For Each p In Process.GetProcesses()
            If Not p Is Nothing Then
                If p.ProcessName.ToString = "cs.exe" Or p.ProcessName.ToString = "cs" Or p.ProcessName.ToString = "CS" Or p.ProcessName.ToString = "CS.EXE" Then
                    Try
                        p.Kill() ' lo cierra  
                        Exit For ' sale  
                    Catch msg As Exception
                        'MsgBox(msg.Message.ToString, MsgBoxStyle.Critical)
                        Exit Sub
                    End Try
                End If
            End If
        Next
    End Sub

    
    'ws CancelAccrual
    Sub Procesa_Cancelacion_Ticket()
        Try
            'Write_event_log_error_file(Mid(Datos, 1, 3))
            Campos_Paso = Datos.Split("|")
            Numero_Ticket_Void = Campos_Paso(1)
            tienda_capturada = Campos_Paso(2)
            'Write_event_log_error_file(Numero_Ticket_Void)
            'Respuesta1 = Ws.solicita_ticket_cancelar("C:\\WS\\ALSE_LOY_Cancel_Transacciones.WSDL", "CancelTranAcreditacion", Numero_Ticket_Void)
            'Nombre_Ws = Path_WS + "ALSE_LOY_Cancel_Transacciones.WSDL"
            'Respuesta1 = Ws.solicita_ticket_cancelar(Nombre_Ws, "CancelTranAcreditacion", Numero_Ticket_Void)

            'Write_event_log_error_file(Respuesta1)

            'grabaXmlToSend_CancelAccrual()
            'ExecutateWsdlSoaCancelAccrua()

            'Respuesta1 = soapResult

            elimina_valores()


            Valor0 = Replace(Respuesta1, "'", "!")
            Respuesta1 = Valor0

            ''New
            Respuesta11 = Respuesta1.Split("|")
            If Respuesta11.Length > 0 Then
                Respuesta1 = Respuesta11(0)
            End If
            ''New

            Separa_Campos_Xml_Individuales_Cancelacion()


            'Write_event_log_error_file(IntegrationStatus)
            If IntegrationStatus.Length > 0 Then
                'Respuesta2 = IntegrationStatus & Separador
                Respuesta2 = "NO SE REALIZO LA CANCELACION."
                'Respuesta2 = Respuesta2 & StatusObject & Separador
            ElseIf Len(error_ws) > 0 Then '' = "No es posible conectar con el servidor remoto" Or error_ws.Substring(0, 38) = "No se puede resolver el nombre remoto:" Or error_ws = "Error en el servidor remoto: (500) Error interno del servidor." Or error_ws = "Se excedi� el tiempo de espera de la operaci�n" Then
                Respuesta2 = "NO HAY COMUNICACION CON WS [CANCELACION]"
            ElseIf faultstring.Length > 0 Then
                'Respuesta2 = faultstring
                Respuesta2 = "NO SE REALIZO LA CANCELACION:"
            Else
                Respuesta2 = "NO EXISTE TICKET EN CRM"
            End If
        Catch Errores_Procesa_Cancelacion_Ticket As Exception
            Registro_Log = "Errores_Procesa_Cancelacion_Ticket              :" + Errores_Procesa_Cancelacion_Ticket.Message
            Write_event_log_error_file(Registro_Log)
        Finally
        End Try
    End Sub
    Sub Separa_Campos_Xml_Individuales_Cancelacion()
        Inicio_L = 0
        Contador_Voucher = 0
        Contador_Promociones = 0
        If Len(error_ws) > 1 Then
            Borra_Totales_Busqueda_Xml()
            Borra_Totales_Busqueda_Voucher_Xml()
            Exit Sub
        End If
        IntegrationStatus = ""
        Separa_Campos_Individules("<message>", IntegrationStatus)
        Separa_Campos_Individules("<faultstring>", faultstring)
    End Sub
    'ws CancelAccrual


    'ws Redem
    Sub Procesa_Redencion_Ticket()
        Try

            Campos_Paso = Datos.Split("|")

            '27Ene2015
            Id2 = Campos_Paso(1)
            SQLUpdate_Redime_Ticket2_CRM()
            '27Ene2015


            'numero_Miembro = Campos_Paso(1)
            'Business_date = Campos_Paso(2)
            'Tienda = Campos_Paso(3)
            'CentroConsumo = Campos_Paso(4)
            'Cheque = Campos_Paso(5)

            ''MessageBox.Show("1.1")
            'aqui grabaXmlToSend_Redem()
            'aqui ExecutateWsdlSoaRedem()
            ''MessageBox.Show("1.2:" & Respuesta1)

            Respuesta1 = CrmSoaw.main_crmsoa_Redencion(Campos_Paso, sUri, campo2, campo3, campo4, campo5, campo6, campo7, campo9, campo10, timeOut_Ws)
            'Respuesta1 = soapResult

            elimina_valores()

            ''MessageBox.Show("1.3:" & Respuesta1)

            ''New
            Respuesta11 = Respuesta1.Split("|")
            If Respuesta11.Length > 0 Then
                Respuesta1 = Respuesta11(0)
            End If
            ''New

            ''MessageBox.Show("1.4: " & Respuesta1)
            Separa_Campos_Xml_Individuales_Redenciones()

            Respuesta2 = "REDENCION OK"
            ''MessageBox.Show("1.5")
            If errorDescription.Length > 0 Or faultstring.Length > 0 Then
                'Respuesta2 = errorDescription & Separador & faultstring & Separador
                Respuesta2 = "NO SE REALIZO LA REDENCION" & "|" & errorCode
            ElseIf detail.Length > 15 Then
                If detail.Substring(0, 16) = "Se ha almacenado" Then
                    Respuesta2 = "REDENCION OK"
                End If
            ElseIf Len(error_ws) > 0 And detail.Length = 0 Then '' = "No es posible conectar con el servidor remoto" Or error_ws.Substring(0, 38) = "No se puede resolver el nombre remoto:" Or error_ws = "Error en el servidor remoto: (500) Error interno del servidor." Or error_ws = "Se excedi� el tiempo de espera de la operaci�n" Then
                Respuesta2 = "NO HAY COMUNICACION CON WS [REDENCION]" & "|" & errorCode
            Else
                Respuesta2 = "ERROR EN REDENCION" & "|" & errorCode
            End If
            ''MessageBox.Show("1.6")
        Catch exc As Exception
            ' Show the exception to the user.
            Error_Field = "Procesa_Redencion_Ticket." + vbCrLf + "Exception: " + exc.Message
            ''MessageBox.Show(Error_Field)
            Write_event_log_error_file(Error_Field)
        Finally
        End Try

    End Sub
    Sub Separa_Campos_Xml_Individuales_Redenciones()
        Try
            Inicio_L = 0
            Contador_Voucher = 0
            Contador_Promociones = 0
            'MessageBox.Show("1.1.1")
            If Len(error_ws) > 1 Then
                Borra_Totales_Busqueda_Xml()
                Borra_Totales_Busqueda_Voucher_Xml()
                Exit Sub
            End If
            ''MessageBox.Show("A:    " & Respuesta1)
            If Respuesta1.Length > 0 Then
                IntegrationStatus = ""
                Separa_Campos_Individules("<als6:errorDescription>", errorDescription)
                ''MessageBox.Show("B")
                Separa_Campos_Individules("<faultstring>", faultstring)
                ''MessageBox.Show("C")
                Separa_Campos_Individules("<als6:errorCode>", errorCode)
                ''MessageBox.Show("C")
                Separa_Campos_Individules("<als6:detail>", detail)
            End If
        Catch exc As Exception
            ' Show the exception to the user.
            Error_Field = "!!ERROR!! Separa_Campos_Xml_Individuales_Redenciones." + vbCrLf + "Exception: " + exc.Message
            Write_event_log_error_file(Error_Field)
            'MessageBox.Show(Error_Field)
        Finally
        End Try
    End Sub
    'ws Redem

    'ws Reden Tmed
    Sub Procesa_Redencion_Ticket_Tmed_Wow()
        Try

            Campos_Paso = Datos.Split("|")
            'numero_Miembro = Campos_Paso(1)
            'Business_date = Campos_Paso(2)
            'Tienda = Campos_Paso(3)
            'CentroConsumo = Campos_Paso(4)
            'Cheque = Campos_Paso(5)

            ''MessageBox.Show("1.1")
            'aqui grabaXmlToSend_Redem()
            'aqui ExecutateWsdlSoaRedem()
            ''MessageBox.Show("1.2:" & Respuesta1)

            Respuesta1 = CrmSoaw.main_crmsoa_Redencion_Tmed_Wow(Campos_Paso, sUri, campo2, campo3, campo4, campo5, campo6, campo7, campo9, campo10, timeOut_Ws)
            'Respuesta1 = soapResult

            elimina_valores()

            ''MessageBox.Show("1.3:" & Respuesta1)

            ''New
            Respuesta11 = Respuesta1.Split("|")
            If Respuesta11.Length > 0 Then
                Respuesta1 = Respuesta11(0)
            End If
            ''New

            ''MessageBox.Show("1.4: " & Respuesta1)
            Separa_Campos_Xml_Individuales_Redenciones()

            Respuesta2 = "REDENCION OK"
            ''MessageBox.Show("1.5")
            If errorDescription.Length > 0 Or faultstring.Length > 0 Then
                'Respuesta2 = errorDescription & Separador & faultstring & Separador
                Respuesta2 = "NO SE REALIZO LA REDENCION" & "|" & errorCode
            ElseIf detail.Length > 15 Then
                If detail.Substring(0, 16) = "Se ha almacenado" Then
                    Respuesta2 = "REDENCION OK"
                End If
            ElseIf Len(error_ws) > 0 And detail.Length = 0 Then '' = "No es posible conectar con el servidor remoto" Or error_ws.Substring(0, 38) = "No se puede resolver el nombre remoto:" Or error_ws = "Error en el servidor remoto: (500) Error interno del servidor." Or error_ws = "Se excedi� el tiempo de espera de la operaci�n" Then
                Respuesta2 = "NO HAY COMUNICACION CON WS [REDENCION]" & "|" & errorCode
            Else
                Respuesta2 = "ERROR EN REDENCION" & "|" & errorCode
            End If
            ''MessageBox.Show("1.6")
        Catch exc As Exception
            ' Show the exception to the user.
            Error_Field = "Procesa_Redencion_Ticket." + vbCrLf + "Exception: " + exc.Message
            ''MessageBox.Show(Error_Field)
            Write_event_log_error_file(Error_Field)
        Finally
        End Try

    End Sub
    'ws Redem Tmed

    Private Sub SQLUpdate_Acreditaciones_Transmitido_CRM()
        Try
            Insert_Datos_CRM = False
            Permite_Conexcion_CRM_Micros = False
            Salir = False
            ODBC_ConnectStringMICROS()
            If Permite_Conexcion_CRM_Micros = True Then
                InsertTicket = "Update custom.LOYTRANSACCION set Transmitido = 'F' WHERE TicketNumber = '" & TicketNumber & "' "
                'Write_event_log_error_file(InsertTicket)
                Dim Command As New OdbcCommand(InsertTicket, ConexcionMCRSPOS)
                Command.CommandType = CommandType.Text
                ReadSql = Command.ExecuteScalar
                Insert_Datos_CRM = True
            End If
        Catch Errores_SQLInsert_Ticket_CFD As Exception
            Registro_Log = "SQLUpdate_Acreditaciones_CRM              :" + Errores_SQLInsert_Ticket_CFD.ToString
            Insert_Datos_CRM = False
            'Write_event_log_error_file(Registro_Log)
        Finally
            ODBC_ConnectStringMicrosClose()
        End Try
    End Sub

    'Log
    Public Sub grabaXmlLogSoa(ByVal valor As String)
        Try
            Dim Valor2 As String
            filewrite = File.AppendText(name_file_xml_log)
            Valor2 = "<" & Now.ToLocalTime.ToString() & ">" & valor & ">"
            filewrite.WriteLine(Valor2)
            filewrite.Close()
        Catch ex As Exception
            'ListBox1.Items.Add(ex)
            campo = "<Error: grabaLogSoa: " & ex.ToString() & ">"
        End Try

    End Sub
    Public Sub grabaLogSoa(ByVal valor As String)
        Try
            Dim Valor2 As String
            filewrite = File.AppendText(name_file_log)
            Valor2 = "<" & Now.ToLocalTime.ToString() & ">" & valor & ">"
            filewrite.WriteLine(Valor2)
            filewrite.Close()
        Catch ex As Exception
            'ListBox1.Items.Add(ex)
            campo = "<Error: grabaLogSoa: " & ex.ToString() & ">"
        End Try

    End Sub
    'Log

    ''08Abr2015
    Sub Separa_Campos_Individules_Indice_Voucher(ByVal Valor As String, ByRef Resp_valor As String, ByRef Indice As Integer)
        Resp_valor = ""
        If Respuesta1.IndexOf(Valor, Indice) > 0 Then
            Inicio2 = (Valor.Length + 1)
            Inicio3 = (Valor.Length - 1)
            Inicio_L = Respuesta1.IndexOf(Valor, Indice)
            Inicio_L = Inicio_L + Inicio2
            Cadena = Mid(Valor, 1, 1) & "/" & Mid(Valor, 2, Inicio3)
            If Respuesta1.IndexOf(Cadena, Inicio_L) > 0 Then
                Final_L = Respuesta1.IndexOf(Cadena, Inicio_L)
            End If
            If Inicio_L > 0 And Final_L > 0 Then
                Longitud_L = (Final_L - Inicio_L) + 1
                Resp_valor = Mid(Respuesta1, Inicio_L, Longitud_L)
                ''06Mar2015 Indice = Final_L
            End If

            ''06Mar2015
            If Final_L > Final_Vocuher Then
                Resp_valor = ""
            Else
                Indice = Final_L
            End If

            Registro_Log = Valor & Resp_valor & "<>" & Indice.ToString
            AppendLog(Registro_Log)
            ''06Mar2015

        End If
    End Sub
    Sub Busca_Final_Vocuher(ByRef inicio_ As Integer, ByRef final_ As Integer)
        Inicio_Voucher = inicio_
        Separa_Campos_Individules_Indice("<voucher>", validFrecAttibuteType, inicio_)
        If Len(validFrecAttibuteType) = 0 Then
            ''Separa_Campos_Individules_Indice_Final("<validFrecAttibuteType/>", inicio_)
        End If
        final_ = inicio_
        inicio_ = Inicio_Voucher
    End Sub

    Sub Separa_Campos_Individules_Indice_Final(ByVal Valor As String, ByRef Indice As Integer)
        If Respuesta1.IndexOf(Valor, Indice) > 0 Then
            Inicio2 = (Valor.Length + 1)
            Inicio3 = (Valor.Length - 1)
            Inicio_L = Respuesta1.IndexOf(Valor, Indice)
            Inicio_L = Inicio_L + Inicio2
            Cadena = Valor
            If Respuesta1.IndexOf(Cadena, Inicio_L) > 0 Then
                Final_L = Respuesta1.IndexOf(Cadena, Inicio_L)
            End If
            If Inicio_L > 0 And Final_L > 0 Then
                Longitud_L = (Final_L - Inicio_L) + 1
                Indice = Final_L
            End If
        End If
    End Sub
    ''08Abr2015
End Class



