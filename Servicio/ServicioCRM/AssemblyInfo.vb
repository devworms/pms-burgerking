Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("CRM 2014")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("TESSA")> 
<Assembly: AssemblyProduct("")> 
<Assembly: AssemblyCopyright("TESSA")> 
<Assembly: AssemblyTrademark("MAMC")> 
<Assembly: CLSCompliant(True)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("8A0925F2-CAAA-49A9-8EA7-97711C6FC973")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("1.0.1.*")> 
