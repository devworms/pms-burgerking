Imports System.ServiceProcess
Imports System.Timers
Imports System.IO
Imports System.Data.Odbc
Imports System.Web.Mail
Imports System.Text
Imports wsReader
Imports vx810v1

''campos(29) Fallback
''campos(30) Numero de Serie
''campos(31) IAD
''campos(32) APPLABEL

Public Class ServicioPinpad
    Inherits System.ServiceProcess.ServiceBase
    Private WithEvents server As New Irc.Networking.ServerSocket
    Private WithEvents Ws As New wsReader.Clasews


    '''FILES Const
    Public Const FileEjecutaProgramaJava As String = "execute.Bat"
    Public Const FileConfiguracion_CRM As String = "D:\MICROS\Res\Pos\Etc\Configuracion_CRM.TxT"
    Public File_Log_Redenciones_CRM As String = "CRM_Redenciones_Generadas_OffLine.TxT"
    Public Const Slash As String = "|"
    Public string_xml, entidad, campo_tarjeta As String

    '''String

    '23Nov2013
    Public campos_Devolucion(9999) As String
    Public campos_Reverso(9999) As String
    Public campos_Consulta(9999) As String
    '23Nov2013

    Public iad, applabel, no_serie_1, campo_hex, caracter_str As String
    Public txtFileNameLog, txtFileNameRedencionesLog, TicketNumber, MemberNumber As String
    Public Campo01, Campo02, Cadena, Campo As String
    Public Registro_Log As String
    Public Cadena_Inicio As String
    Dim Cadena_Inicio2 As Char()
    Public Socket_Peticion As String
    Dim Campos(), Beneficios(10) As String
    Public Datos, Nombre As String
    Public Respuesta, Respuesta1, Respuesta2, Respuesta3 As String
    Public Encabezado As String
    Public Datos1 As String
    Public Trailer As String
    Public Const Separador As String = "|"
    Public Numero_WSID_A, Card_Status, LifetimePoint1Value, LifetimePoint2Value, SubStatus, Status, Status2 As String
    Public Lectura(10) As String
    Public Marca, numero_Miembro, Jar_Ejecutar As String
    Public Path_Entrada, Path_Salida, DeleteTicket, InsertTicket, ReadSql, Last_Beverage, Last_Food, AttributeValue As String
    Public Business_date, Tienda, CentroConsumo, Cheque, ALSEFavoriteProduct As String
    Public ListOfLOYMember_Contact, BirthDate, Archivo_Trabajar, Numero_Ticket_Void, Estatus, IntegrationStatus, StatusObject As String
    Public Cheques_Lotes(1000) As String

    '''''''Integer
    Public numero_Interface, Inicio_caracter, contador1, caracter_int, contador_hex, contador_hex2 As Integer
    Public size_17, size_18, size_19, size_20, size_31, size_sFlag, numeroCampos As Integer
    Public Inicio_Encabezado As Integer
    Public Fin_Encabezado As Integer
    Public Inicio_Datos As Integer
    Public Fin_Datos As Integer
    Public Inicio_Trailer As Integer
    Public Fin_Trailer As Integer
    Public Inicio_Separador As Integer
    Public Fin_Separador As Integer
    Public Longitud, Inicio_L, Inicio_L2, Inicio_L3, Inicio_L4, Final_L, Longitud_L, Inicio_M, Indice2 As Integer
    Public Contador_Datos As Integer
    Dim PuertoID As Integer
    Public Inicio, Inicio2, Inicio3 As Integer
    Dim Timeout_Ifc As Integer
    Public Contador_Registros_Lote As Integer

    '''Int32
    Public Timer_Lectura_Files_Fix As Int32

    ''' Int64
    Public Numero_WSID As Int64
    Public Timeout_Ifc_Ws As Int64

    ''' Boolean
    Public Permite_Conexcion As Boolean
    Public Permite_Conexcion_MCRSPOS As Boolean
    Public Permite_Conexcion_CRM As Boolean
    Public Salir, Delete_Datos_CRM, Insert_Datos_CRM, Permite_Conexcion_CRM_Micros, Permite_Conexcion_CRM_Micros_Dos, Permite_Conexcion_CRM_Micros_Tres, Permite_Conexcion_Lotes_Micros As Boolean
    Public Servicio_Micros_Start, Servicio_Micros_First As Boolean

    ''' StreamReader and StreamWriter
    Dim myStreamReader As StreamReader
    Dim myStreamWriter As StreamWriter

    ''' ConexcionSybase odbc
    Dim ConexcionSybase As New OdbcConnection
    Dim Conexcion As New OdbcConnection
    Dim ReadSql_odbc As OdbcDataReader
    Dim ReadSql_odbc_Dos As OdbcDataReader
    Dim ConexcionMCRSPOS As New OdbcConnection
    Dim ConexcionMCRSPOS_Dos As New OdbcConnection
    Dim ConexcionMCRSPOS_Tres As New OdbcConnection

    Dim Campos_Paso() As String

    Public Nivel, Organizacion, NivelId, NivelNombre, Promociones(20), Voucher(20) As String
    Public PromocionId, PromocionName, PromocionUniqueKey, PromocionDisplayName, PromocionStatus As String
    Public Contador_Promociones, Contador_Voucher As Integer
    Public VoucherCalStatus, VoucherProductId, VoucherProductName, VoucherStatus, VoucherTransactionId As String

    Public Path_WS, Nombre_Ws, Respuesta22 As String
    Public Const FileRutaCRM As String = "C:\PathCRM.TxT"

    Public Mail_Remitente, Mail_Destinatario, Mail_Remitente_Password, Procesado_Nivel As String


    Dim LeeTarjeta As New vx810v1.LeeBandaChip
    '06ago2012 Dim Lee1 As New Nurit293v30.Transacciones

    '06ago2012 Dim LeeTarjeta_Nurit293v30 As New Nurit293v30.Transacciones
    '06ago2012 Dim LeeTarjeta_Nurit293v32 As New Nurit293v32.LeeBandaChip

    ''Nuevas
    Dim Conexcioncfd_Facturacion As New OdbcConnection
    Dim Conexcion_odbc As New OdbcConnection
    Dim InstrSql As String
    Public Invoice_Add, Log_Add As Boolean
    Public Serie As String
    Public poolId, montoPesos, montoPuntos, montoTotal, leyenda1, leyenda2, saldoAnteriorPuntos, saldoAnteriorPesos, saldoRedimidoPuntos, saldoRedimidoPesos, saldoActualPuntos, saldoActualPesos, vigenciaPuntos As String
    Public Permite_Conexion_Lealtad As Boolean
    Public II As Integer
    Public Separador1, No_Tarjeta_A, Bines_Lealtad As String
    Public imprimir, autorizacion, mensaje, razonSocial, comercio, direccion, Bines(200) As String
    Public fhTransaccion, tarjeta, fhVencimiento, emisor, operacion, monto_A, status_code, Monto_Puntos As String
    Public ingreso, criptograma, Hora_A, firma, Tipo_Tarjeta, cd_respuesta, code, s_pago, Log_Field As String
    ''Nuevas

    Public No_Tarjeta, Titular, Fecha_Vencimiento, CVV2, Digest, Digest2, Mensaje_Pinpad, Mensaje_Pinpad2, Mensaje_Pinpad3, Mensaje_Pinpad_All, Mensaje_Pinpad_All1, Consecutivo As String
    Public Secuencia_transmision, Referencia, Tipo_Servicio, Anio, Mes As String
    Public Numero_Tarjeta, Email_cliente, Telefono_Cliente, Identificador_Cliente As String
    Public Tipo_TDC, Tipo_Pago, Track1, Track1_A, Track2, Track2_A, Tipo_Plan, Periodo_Financiamiento, sFlag As String
    Public Email_Administrador, Email_Transaccion, Afiliacion, Plataforma As String
    Public Tag_5F34, Tags_EMV, Bandera_Firma, Tag_9F26, Tag_9F27, Pagare_Pinpad, Flag As String
    Public Business_Unit, Codigo_Moneda, Enverioment As Integer
    Public Monto As Decimal
    Public Respuesta_Autorizacion As Boolean
    Public Respuesta_Autorizacion_VisaMaster, Respuesta_Bines_VisaMaster As String
    Public Respuesta_Autorizacion_Amex As String
    Public Titulares() As String
    'DLL PINPAD



#Region " Component Designer generated code "

    Public Sub New()
        MyBase.New()

        ' This call is required by the Component Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call

    End Sub

    'UserService overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    ' The main entry point for the process
    <MTAThread()> _
    Shared Sub Main()
        Dim ServicesToRun() As System.ServiceProcess.ServiceBase

        ' More than one NT Service may run within the same process. To add
        ' another service to this process, change the following line to
        ' create a second service object. For example,
        '
        '   ServicesToRun = New System.ServiceProcess.ServiceBase () {New Service1, New MySecondUserService}
        '
        ServicesToRun = New System.ServiceProcess.ServiceBase() {New ServicioPinpad}

        System.ServiceProcess.ServiceBase.Run(ServicesToRun)
    End Sub

    'Required by the Component Designer
    Private Tiempo As Timers.Timer
    Private components As System.ComponentModel.IContainer
    Private timeStart As DateTime          ' Used to note the start time of the service
    Private timeEnd As DateTime            ' Used to note the end time of the service
    Private timeElapsed As New TimeSpan(0) ' Initialize to 0
    Private timeDifference As TimeSpan
    Private isPaused As Boolean = False    ' Notes whether the service is paused
    Private Error_Field, Vacio, Field_Log As String
    Private File_Error, File_Lee_Error, File_Contador, File_Terminales, File_Operations, File_Log, File_Log_Bak, File_Uws As String
    Private Respuesta_N, rowCount, rowCount0, UWS_Reset, Process_To_Kill, Resp, Contador, I, Hora_Delete, Minuto_Delete As Integer
    Dim Numero_Uws(100), Numero_Proceso(100) As Integer
    Private Flag_Operations, Flag_Borrar_Archivo As Boolean
    '''NEW
    '''NEW
    '''NOTE: The following procedure is required by the Component Designer
    '''It can be modified using the Component Designer.  
    '''Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        '
        'ServicioCFDFacturacion
        '
        Me.CanHandlePowerEvent = True
        Me.CanShutdown = True
        Me.ServiceName = "Servicio Pinpad"

    End Sub

#End Region

    '''PROCESS SERVICES
    Protected Overrides Sub OnStart(ByVal args() As String)
        Try
            Tiempo = New Timers.Timer
            AddHandler Tiempo.Elapsed, AddressOf TimerFired
            Tiempo.Enabled = False
            Tiempo.Stop()
            AutoLog = True
            Servicio_Micros_Start = False
            Servicio_Micros_First = False
            'System.Threading.Thread.CurrentThread.Sleep(180000)
            'SQLReaderTimeout_interface_micros()
            'If Permite_Conexcion_MCRSPOS = False Then
            'SQLReaderTimeout_Facturacion()
            'Dispose(True)
            'Else
            'ReadFile()
            '06ago2012 Solicita_Path_Micros()
            '06ago2012 ReadFilePathMicros()
            Calcula_Tiempo_Servicio()
            'Abre_Puerto_Comunicacion()
            'End If
        Catch exc As Exception
            Error_Field = "OnStart: " & exc.Message
            Write_event_log_error_file(Error_Field)
        Finally
        End Try
    End Sub
    Protected Overrides Sub OnStop()
        Try
            Error_Field = "OnStop: " & "** OnStop: "
            Write_event_log_error_file(Error_Field)
            Cierra_Puerto_Comunicacion()
            Tiempo.Dispose()
            AutoLog = False
            Procesado_Nivel = ""
        Catch exc As Exception
            Error_Field = exc.Message
            Write_event_log_error_file(Error_Field)
        Finally
        End Try
    End Sub
    Protected Overrides Function OnPowerEvent(ByVal powerStatus As System.ServiceProcess.PowerBroadcastStatus) As Boolean
        Try
            Error_Field = "OnPowerEvent: " & "** OnStop: "
            Write_event_log_error_file(Error_Field)
            Tiempo.Stop()
            Tiempo.Dispose()
            AutoLog = False
        Catch exc As Exception
            Error_Field = exc.Message
            Write_event_log_error_file(Error_Field)
        Finally
        End Try
    End Function
    Private Sub TimerFired(ByVal sender As Object, ByVal e As ElapsedEventArgs)
        Try
            'Error_Field = "**TimerFired Inicia: "
            'Write_event_log_error_file(Error_Field)
            Tiempo.Enabled = False
            Tiempo.Stop()
            '20Dic2011 Procesado_Nivel = ""
            Detect_Servicio_Micros()
            If Servicio_Micros_Start = True Then
                If Servicio_Micros_First = False Then
                    'SQLReaderTimeout_interface_micros()
                    'If Permite_Conexcion_MCRSPOS = True Then
                    'ReadFile()
                    'Calcula_Tiempo_Servicio()
                    'Abre_Puerto_Comunicacion()
                    Error_Field = "**TimerFired True: " + "  " + Servicio_Micros_First.ToString + " " + Servicio_Micros_Start.ToString
                    Write_event_log_error_file(Error_Field)
                    Proceso_PrincipalTimerFired()
                    Servicio_Micros_First = True

                    'Else
                    'Servicio_Micros_First = False
                    'Else
                    'Error_Field = "**TimerFired False: " + "  " + Servicio_Micros_First.ToString + " " + Servicio_Micros_Start.ToString
                    'Write_event_log_error_file(Error_Field)

                End If
            Else
                If Servicio_Micros_First = True Then
                    Error_Field = "Servicio_Micros_Start false y Servicio_Micros_First true: Cierra Puerto"
                    Write_event_log_error_file(Error_Field)
                    Cierra_Puerto_Comunicacion()
                End If
            End If

            'If Servicio_Micros_First = True Then
            'Proceso_PrincipalTimerFired()
            'End If
            'End If
        Catch exc As Exception
            Error_Field = "**TimerFired Inicia. Exception: " + exc.Message
            Write_event_log_error_file(Error_Field)
        Finally
            '21Dic2011 Proceso_Main_Busqueda_Nivel()
            Calcula_Tiempo_Servicio()
        End Try
    End Sub
    Sub Detect_Servicio_Micros()
        Try
            Dim Computername As String = System.Net.Dns.GetHostName
            Dim controller As New ServiceController
            Dim status As String
            controller.MachineName = "."
            controller.ServiceName = "SQLANYs_sql" & Computername
            status = controller.Status.ToString
            controller.Refresh()
            If status = "Running" Then
                Servicio_Micros_Start = True
            Else
                Servicio_Micros_Start = False
            End If
            'Error_Field = "**Detect_Servicio_Micros: " + "  " + controller.ServiceName.ToString + " " + status + " " + Servicio_Micros_Start.ToString
            'Write_event_log_error_file(Error_Field)
        Catch Error_Detect_Servicio_Micros As Exception
            Error_Field = "**Error_Detect_Servicio_Micros: " + "  " + Error_Detect_Servicio_Micros.ToString
            Write_event_log_error_file(Error_Field)
        End Try
    End Sub

    '''MAIN PROCESS
    Private Sub Proceso_PrincipalTimerFired()
        Try
            SQLReaderTimeout_interface_micros_CRM()
            If Permite_Conexcion_MCRSPOS = False Then
                SQLReaderTimeout_interface_micros_CRM()
            End If
            ReadFile()
            'Calcula_Tiempo_Servicio()
            Abre_Puerto_Comunicacion()
            Error_Field = "**Proceso_Principal. PROCESS MAIN: "
            Write_event_log_error_file(Error_Field)
        Catch exc As Exception
            Error_Field = "**Proceso_Principal. Exeception: " & exc.Message
            Write_event_log_error_file(Error_Field)
        Finally
        End Try
    End Sub
    Private Sub Calcula_Tiempo_Servicio()
        Try
            With Tiempo
                .Interval = 1
                .AutoReset = True
                .Enabled = True
                .Start()
            End With
        Catch exc As Exception
            Error_Field = exc.Message
            Write_event_log_error_file(Error_Field)
        Finally
        End Try
    End Sub

    '''SHOW MESSAGE ON SERVER
    Public Sub Muestra_Mensaje_Server()
        Try
            'Dim current As Thread = Thread.CurrentThread
            'MsgBox(current.Name, MsgBoxStyle.MsgBoxSetForeground, "Facturacion Electronica: ")
        Catch Errores_Muestra_Mensaje_Server As Exception
            Registro_Log = "Errores_Muestra_Mensaje_Server        :" + Errores_Muestra_Mensaje_Server.ToString
            'AppendLog(Registro_Log)
        Finally
        End Try
    End Sub

    ''' MAIN COMUNICATIONS
    Private Sub Abre_Puerto_Comunicacion()
        Try
            If Not server.Listening() Then
                server.Listen(Int32.Parse(Convert.ToString(PuertoID)))
                Error_Field = "Abre_Puerto_Comunicacion: " & PuertoID.ToString
                Write_event_log_error_file(Error_Field)
            Else
            End If
        Catch Error_Abre_Puerto_Comunicacion As Exception
            Error_Field = "**Error_Abre_Puerto_Comunicacion: " & Error_Abre_Puerto_Comunicacion.Message
            Write_event_log_error_file(Error_Field)
        Finally
        End Try
    End Sub
    Private Sub server_ClientConnectionClosed(ByVal sender As Object, ByVal e As Irc.Networking.ServerSocket.ServerSocketEventArgs) Handles server.ClientConnectionClosed
        Try
            Error_Field = "server_ClientConnectionClosed      :" + e.ClientSocket.ToString
            Write_event_log_error_file(Error_Field)
            'New lbConnectedClients.Items.Remove(e.ClientSocket)
        Catch Errores_ClientConnectionClosed As Exception
            Error_Field = Errores_ClientConnectionClosed.Message
            Write_event_log_error_file(Error_Field)
            'New MessageBox.Show("Error ClientConnectionClosed", "Error cerrando Socket", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally

        End Try

    End Sub
    Private Sub server_ClientConnectSuccess(ByVal sender As Object, ByVal e As Irc.Networking.ServerSocket.ServerSocketEventArgs) Handles server.ClientConnectSuccess
        Try
            Error_Field = "server_ClientConnectSuccess        :" + e.ClientSocket.ToString
            Write_event_log_error_file(Error_Field)
            'New lbConnectedClients.Items.Add(e.ClientSocket)
        Catch Errores_ClientConnectSuccess As Exception
            Error_Field = "Errores_ClientConnectSuccess       :" + e.ClientSocket.ToString + "  " + Errores_ClientConnectSuccess.ToString
            Write_event_log_error_file(Error_Field)
            'New MessageBox.Show("Error ClientConnectSuccess", "Error en Sockets", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally

        End Try

    End Sub
    Private Sub server_ConnectionRequest(ByVal sender As Object, ByVal e As System.EventArgs) Handles server.ConnectionRequest
        Try
            Error_Field = "server_ConnectionRequest           :" + e.GetType.ToString
            Write_event_log_error_file(Error_Field)
            server.Accept()
        Catch Errores_ConnectionRequest As Exception
            Error_Field = "Errores_ConnectionRequest          :" + Errores_ConnectionRequest.ToString
            Write_event_log_error_file(Error_Field)
            'New MessageBox.Show("Error ConnectionRequest", "Error Aceptando Socket", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally

        End Try

    End Sub
    Private Sub server_DataArrivalFromClient(ByVal sender As Object, ByVal e As Irc.Networking.ServerSocket.ServerSocketEventArgs) Handles server.DataArrivalFromClient
        Try
            Cadena_Inicio = e.ClientSocket.GetArrivedText.ToString
            'Cadena_Inicio2 = e.ClientSocket.GetArrivedText.ToCharArray
            Socket_Peticion = e.ClientSocket.ToString()
            Registro_Log = "server_DataArrivalFromClient <<Cadena_Inicio<<   :" + e.ClientSocket.ToString + "  " + Cadena_Inicio
            AppendLog(Registro_Log)
            'Registro_Log = "server_DataArrivalFromClient <<Cadena_Inicio2<<   :" + e.ClientSocket.ToString + "  " + Cadena_Inicio2(0).ToString + "  " + "  " + Cadena_Inicio2(1).ToString + "  " + Cadena_Inicio2(2).ToString + "  " + Cadena_Inicio2(3).ToString + "  " + Cadena_Inicio2(4).ToString + "  " + Cadena_Inicio2(5).ToString + "  " + Cadena_Inicio2(6).ToString + "  " + Cadena_Inicio2(7).ToString + "  " + Cadena_Inicio2(8).ToString + "  " + Cadena_Inicio2(9).ToString + "  " + Cadena_Inicio2(10).ToString
            'AppendLog(Registro_Log)
            Registro_Log = "server_DataArrivalFromClient <<02<<   :" + e.ClientSocket.GetArrivedText
            AppendLog(Registro_Log)
            Registro_Log = "server_DataArrivalFromClient <<02a<   :" + Socket_Peticion.ToString()
            AppendLog(Registro_Log)
            Registro_Log = "server_DataArrivalFromClient <<02b<   :" + Cadena_Inicio
            AppendLog(Registro_Log)
            Registro_Log = "server_DataArrivalFromClient <<02c<   :" + e.ClientSocket.GetArrivedText.ToString
            AppendLog(Registro_Log)
            If Cadena_Inicio.Length() > 0 Then
                'New tbMessages.Text += "<" + Socket_Peticion + ">" + "Recibo" + vbCrLf

                '24Feb2014
                If Cadena_Inicio.Length() < 33 Then
                    Envia_Respuesta(e, Cadena_Inicio)
                    Exit Sub
                End If
                '24Feb2014

                Separa_Cadena_Datos_Recibida(Cadena_Inicio)
                Campos = Datos.Split("|")

                numeroCampos = (Campos.Length)

                Registro_Log = "server_DataArrivalFromClient <<Campos<<   :" + numeroCampos.ToString + " comando: <" + Campos(0) + ">"
                AppendLog(Registro_Log)

                numeroCampos = (Campos.Length - 1)

                For contador1 = 0 To numeroCampos
                    If contador1 <= numeroCampos Then
                        Registro_Log = "server_DataArrivalFromClient <<Campos<<   :" + contador1.ToString + " valor: <" + Campos(contador1) + ">" + " Len: <" + Campos(contador1).Length.ToString + ">"
                        AppendLog(Registro_Log)
                    End If
                Next

                Registro_Log = "server_DataArrivalFromClient <<Termina campos>>"
                AppendLog(Registro_Log)

                'End If
                'Datos = Encabezado & "RE-INTENTARLO" & Chr(3) & "ABCD" & Chr(4)
                If Mid(Datos, 1, 13) = "AUTORIZACION1" Or Trim(Campos(0)) = "AUTORIZACION1" Then '///Envio de Mail
                    Procesa_Autorizacion_Pinpad()
                    'Procesa_Autorizacion_Amex_Pinpad()
                    'Respuesta = Encabezado & Respuesta2 & Trailer
                    Envia_Respuesta(e, Respuesta)
                ElseIf Mid(Datos, 1, 13) = "AUTORIZACION2" Or Trim(Campos(0)) = "AUTORIZACION2" Then '///Envio de Mail
                    Procesa_Autorizacion_Pinpad_II()
                    'Procesa_Autorizacion_Amex_Pinpad()
                    'Respuesta = Encabezado & Respuesta2 & Trailer
                    Envia_Respuesta(e, Respuesta)
                    Limpia_Totales()
                ElseIf Mid(Datos, 1, 12) = "CANCELACION1" Or Trim(Campos(0)) = "CANCELACION1" Then '///Envio de Mail
                    Procesa_Cancelacion_Pinpad_Final()
                    'Respuesta = Encabezado & Respuesta2 & Trailer
                    Envia_Respuesta(e, Respuesta)
                ElseIf Mid(Datos, 1, 12) = "CANCELACION2" Or Trim(Campos(0)) = "CANCELACION2" Then '///Envio de Mail
                    Procesa_Cancelacion_Pinpad_Final_II()
                    'Respuesta = Encabezado & Respuesta2 & Trailer
                    Envia_Respuesta(e, Respuesta)
                ElseIf Mid(Datos, 1, 17) = "AUTORIZACION_AMEX" Or Trim(Campos(0)) = "AUTORIZACION_AMEX" Then '///Envio de Mail
                    Procesa_Autorizacion_Pinpad_Amex_II()
                    'Respuesta = Encabezado & Respuesta2 & Trailer
                    Envia_Respuesta(e, Respuesta)
                ElseIf Mid(Datos, 1, 16) = "CANCELACION_AMEX" Or Trim(Campos(0)) = "CANCELACION_AMEX" Then '///Envio de Mail
                    Procesa_Cancelacion_Amex_Pinpad()
                    'Respuesta = Encabezado & Respuesta2 & Trailer
                    Envia_Respuesta(e, Respuesta)
                ElseIf Mid(Datos, 1, 24) = "CANCELACION_AMEX_DIRECTA" Or Trim(Campos(0)) = "CANCELACION_AMEX_DIRECTA" Then '///Envio de Mail
                    Procesa_Cancelacion_Amex_Directa_Pinpad()
                    'Respuesta = Encabezado & Respuesta2 & Trailer
                    Envia_Respuesta(e, Respuesta)
                ElseIf Mid(Datos, 1, 11) = "DEVOLUCION2" Or Trim(Campos(0)) = "DEVOLUCION2" Then '///Envio de Mail
                    Procesa_Devolucion_Pinpad()
                    'Respuesta = Encabezado & Respuesta2 & Trailer
                    Envia_Respuesta(e, Respuesta)
                ElseIf Mid(Datos, 1, 9) = "CONSULTA1" Or Trim(Campos(0)) = "CONSULTA1" Then '///Envio de Mail
                    Procesa_Puntos()
                    Envia_Respuesta(e, Respuesta)
                ElseIf Mid(Datos, 1, 9) = "CONSULTA2" Or Trim(Campos(0)) = "CONSULTA2" Then '///Envio de Mail
                    Procesa_Puntos_II()
                    Envia_Respuesta(e, Respuesta)
                ElseIf Mid(Datos, 1, 14) = "TERMINA_PINPAD" Or Trim(Campos(0)) = "TERMINA_PINPAD" Then '///Envio de Mail
                    Termina_Transaccion_Pinpad()
                    Respuesta = Encabezado & Separador & "OK" & Trailer
                    Envia_Respuesta(e, Respuesta)
                ElseIf Mid(Datos, 1, 13) = "BINES_LEALTAD" Or Trim(Campos(0)) = "BINES_LEALTAD" Then '///Envio de Mail
                    Procesa_Bines_Lealtad_II()
                    Respuesta = Encabezado & Separador & "OK" & Trailer
                    Envia_Respuesta(e, Respuesta)
                ElseIf Mid(Datos, 1, 20) = "AUTORIZACION_PINPAD1" Or Trim(Campos(0)) = "AUTORIZACION_PINPAD1" Then '///Envio de Mail
                    Procesa_Autorizacion_Pinpad()
                    'Procesa_Autorizacion_Amex_Pinpad()
                    'Respuesta = Encabezado & Respuesta2 & Trailer
                    Envia_Respuesta(e, Respuesta)
                    Respuesta = Encabezado & Separador & Datos & Trailer
                    Envia_Respuesta(e, Respuesta)
                ElseIf Mid(Datos, 1, 16) = "CONSULTA_VOUCHER" Or Trim(Campos(0)) = "CONSULTA_VOUCHER" Then '///Envio de Mail
                    Procesa_Consulta_Pinpad_Voucher_II()
                    'Procesa_Autorizacion_Amex_Pinpad()
                    'Respuesta = Encabezado & Respuesta2 & Trailer
                    Envia_Respuesta(e, Respuesta)
                End If
                Borra_Variables()
            End If
        Catch Errores_DataArrivalFromClient As Exception
            Registro_Log = "Errores_DataArrivalFromClient      :" + Errores_DataArrivalFromClient.ToString
            Respuesta = Encabezado & Separador & "ERROR DATOS" & Trailer
            AppendLog(Registro_Log)
            'New MessageBox.Show("Error DataArrivalFromClient", "Datos Recibido Incorrectos", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
        End Try
    End Sub

    Private Sub Abre_Cierra_Socket()
        Try
            If Not server.Listening() Then
                If Permite_Conexcion = True Then
                    Registro_Log = "Abre_Cierra_Socket                 :" + Convert.ToString(PuertoID)
                    AppendLog(Registro_Log)
                    server.Listen(Int32.Parse(Convert.ToString(PuertoID)))
                Else
                    Registro_Log = "Abre_Cierra_Socket                 :" + "No se Puede Abrir Puerto de Comunicacion  " + Convert.ToString(PuertoID)
                End If
            Else
                Registro_Log = "Abre_Cierra_Socket                 :" + "Puerto Cerrado  " + Convert.ToString(PuertoID)
                AppendLog(Registro_Log)
                server.Close()
            End If
        Catch Error_Abre_Cierra_Socket As Exception
            Error_Field = "Error_Abre_Cierra_Socket           :" + Convert.ToString(PuertoID) + "  " + Error_Abre_Cierra_Socket.ToString
            Write_event_log_error_file(Error_Field)
        Finally
        End Try
    End Sub
    Private Sub Cierra_Puerto_Comunicacion()
        Try
            If server.Listening() Then
                server.Close()
                'New lbConnectedClients.ClearSelected()
                Registro_Log = "Cierra_Puerto_Comunicacion         :" + "server.Close"
                Error_Field = Registro_Log
                Write_event_log_error_file(Error_Field)
                'AppendLog(Registro_Log)
            Else
                'MessageBox.Show("El Servicio ya Esta Detenido", "Servicio", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        Catch Error_Cierra_Puerto_Comunicacion As Exception
            Error_Field = "**Error_Cierra_Puerto_Comunicacion: " & Error_Cierra_Puerto_Comunicacion.Message
            Write_event_log_error_file(Error_Field)
        Finally
        End Try
    End Sub


    ''''COMUNICATION DATA
    Private Sub Envia_Respuesta(ByVal e As Irc.Networking.ServerSocket.ServerSocketEventArgs, ByVal Envia As String)
        Try
            If (TypeOf (e.ClientSocket) Is Irc.Networking.ClientSocket) Then
                Dim c As Irc.Networking.ClientSocket = e.ClientSocket
                'Registro_Log = "Envia_Respuesta >>>                :"
                'AppendLog(Registro_Log)
                Registro_Log = "Envia_Respuesta >>>                :" + e.RemoteHost.ToString() + ":" + e.RemotePort.ToString + ">>> Respondio >>>" + Envia + ControlChars.CrLf
                AppendLog(Registro_Log)
                c.SendTextAppendCrlf(Envia)
                'New tbMessages.Text += "<" + e.RemoteHost.ToString() + ":" + e.RemotePort.ToString + "> Respondio " + ControlChars.CrLf
            End If
        Catch Errores_Envia_Respuesta As Exception
            Error_Field = "Errores_Envia_Respuesta >>>        :" + Envia + "  " + Errores_Envia_Respuesta.ToString
            Write_event_log_error_file(Error_Field)
            'New MessageBox.Show("Error Envia_Respuesta", "Error Enviando Respuesta", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
        End Try
        Respuesta = ""
    End Sub
    ''Private Sub Separa_Cadena_Datos_Recibida(ByVal Cadena As String)
    Private Sub Separa_Cadena_Datos_Recibida(ByVal Cadena As String)
        Try
            Registro_Log = "Separa_Cadena_Datos_Recibida 1      :" + Cadena
            AppendLog(Registro_Log)
            For Inicio = 1 To Len(Cadena)
                If Mid(Cadena, Inicio, 1) = Chr(1) Then
                    Inicio_Encabezado = Inicio
                ElseIf Mid(Cadena, Inicio, 1) = Chr(2) Then
                    Fin_Encabezado = Inicio
                    Inicio_Datos = Inicio + 1
                ElseIf Mid(Cadena, Inicio, 1) = Chr(3) Then
                    Fin_Datos = Inicio
                    Inicio_Trailer = Inicio
                ElseIf Mid(Cadena, Inicio, 1) = Chr(4) Then
                    Fin_Trailer = Inicio
                ElseIf Mid(Cadena, Inicio, 1) = Chr(28) Then
                    Inicio_Separador = Inicio
                End If
            Next
            Registro_Log = "Separa_Cadena_Datos_Recibida 2      :" + Inicio_Encabezado.ToString + " " + Fin_Encabezado.ToString + " " + Inicio_Datos.ToString + " " + Fin_Datos.ToString + " " + Inicio_Trailer.ToString + " " + Fin_Trailer.ToString
            AppendLog(Registro_Log)
            ''
            If Fin_Datos = 0 Then
                Fin_Datos = Len(Cadena)
            End If
            If Fin_Trailer = 0 Then
                Trailer = Chr(3) & "ABCD" & Chr(4)
            End If
            ''
            Registro_Log = "Separa_Cadena_Datos_Recibida 3      :" + Fin_Datos.ToString + " " + Trailer
            AppendLog(Registro_Log)
            Inicio_Encabezado = 1
            Fin_Encabezado = 27
            If Inicio_Encabezado > 0 Then
                Encabezado = (Mid(Cadena, Inicio_Encabezado, Fin_Encabezado))
            End If
            Registro_Log = "Separa_Cadena_Datos_Recibida 4 enca :" + Encabezado
            AppendLog(Registro_Log)
            Inicio_Datos = Fin_Encabezado + 1
            'Fin_Datos = Inicio_Trailer - 1
            If Inicio_Datos > 0 Then
                Datos = (Mid(Cadena, Inicio_Datos, (Fin_Datos - Inicio_Datos)))
                ''Datos = (Mid(Cadena, Inicio_Datos, Fin_Datos))
            End If
            Registro_Log = "Separa_Cadena_Datos_Recibida 5 dato :" + Datos
            AppendLog(Registro_Log)
            If Inicio_Trailer > 0 Then
                Trailer = (Mid(Cadena, Inicio_Trailer, (Fin_Trailer - Inicio_Trailer + 1)))
            End If
            Registro_Log = "Separa_Cadena_Datos_Recibida 6 trai :" + Trailer
            AppendLog(Registro_Log)
            If Inicio_Separador > 0 Then
                Separador1 = Mid(Cadena, Inicio_Separador, 4)
            End If
            Registro_Log = "Separa_Cadena_Datos_Recibida 7      :" + Separador1
            AppendLog(Registro_Log)
            If Encabezado.Length > 10 Then
                Numero_WSID_A = Encabezado.Substring(8, 2)
                Numero_WSID = Convert.ToInt64(Numero_WSID_A)
            End If
            Registro_Log = "Separa_Cadena_Datos_Recibida 8      :" + Numero_WSID_A + " " + Numero_WSID.ToString
            AppendLog(Registro_Log)
        Catch Errores_Separa_Cadena_Datos_Recibid As Exception
            Error_Field = "Errores_Separa_Cadena_Datos_Recibid:" + Cadena + "  " + Errores_Separa_Cadena_Datos_Recibid.ToString
            Write_event_log_error_file(Error_Field)
            Datos = Encabezado & Chr(2) & Chr(28) & "00 |" & "RE-INTENTARLO" & Chr(3) & "ABCD" & Chr(4)
            'New MessageBox.Show("Error Separa_Cadena_Datos_Recibida", "Error Separando Datos Recibidos", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
        End Try
    End Sub

    '''' LOGFILE
    Private Sub CloseLog()
        Try
            'Return
            If Not myStreamWriter Is Nothing Then
                myStreamWriter.Close()
            End If
        Catch exc As Exception
            MsgBox("CloseLog: Exception: " + "<>" + exc.Message)
        Finally

        End Try
    End Sub
    Private Sub Verifica_Dia_Log()
        Try
            'Return
            Campo02 = Format(Date.Now.Day, "00") + "-" + Format(Date.Now.Month, "00") + "-" + Format(Date.Now.Year, "0000") + ".Log"
            If txtFileNameLog <> Campo02 Then
                CloseLog()
                txtFileNameLog = Format(Date.Now.Day, "00") + "-" + Format(Date.Now.Month, "00") + "-" + Format(Date.Now.Year, "0000") + ".Log"
                myStreamWriter = File.AppendText(txtFileNameLog)
            End If
        Catch exc As Exception
            MsgBox("Verifica_Dia_Log: Exception: " + txtFileNameLog + "<>" + exc.Message)
        Finally
        End Try
    End Sub
    Private Sub OpenLog()
        Try
            'Return
            If Not myStreamWriter Is Nothing Then
                myStreamWriter.Close()
            End If
            txtFileNameLog = Format(Date.Now.Day, "00") + "-" + Format(Date.Now.Month, "00") + "-" + Format(Date.Now.Year, "0000") + ".Log"
            myStreamWriter = File.AppendText(txtFileNameLog)
        Catch exc As Exception
            MsgBox("OpenLog: Exception: " + txtFileNameLog + "<>" + exc.Message)
        Finally
        End Try
    End Sub
    Private Sub AppendLog(ByVal Reg_Log As String)
        Try
            'Return  Quitar
            Verifica_Dia_Log()
            OpenLog()
            Campo01 = Format(Date.Now.Day, "00") + "-" + Format(Date.Now.Month, "00.") + "-" + Format(Date.Now.Year, "0000 ") + Format(Date.Now.Hour, "00") + ":" + Format(Date.Now.Minute, "00") + ":" + Format(Date.Now.Second, "00") + ":" + Format(Date.Now.Millisecond, "000") + "..." + Reg_Log + ">>>" + ControlChars.CrLf
            myStreamWriter.Write(Campo01)
            myStreamWriter.Flush()
            CloseLog()
        Catch exc As Exception
            'MsgBox("AppendLog: Exception: " + Campo01 + "<>" + exc.Message)
        Finally
        End Try
    End Sub

    ''' EVENTLOG
    Public Sub Write_event_log_error_file(ByVal Error_Campo As String)
        Try
            EventLog.WriteEntry("Service Repositorio: " + Error_Campo)
        Catch exc As Exception
            Error_Field = "Write_event_log_error_file. Exception: " + exc.Message
            EventLog.WriteEntry("Service Repositorio: (!ERROR INTERNO!)" + Error_Field)
        Finally
        End Try
    End Sub

    '''CONECCTION DB
    Private Sub SQLConnectString(ByVal sServer As String, ByVal sUser As String, ByVal sPassword As String)
        Try
            Registro_Log = "SQLConnectString                   :" + sServer + "  " + sUser + "  " + sPassword
            AppendLog(Registro_Log)
            If Conexcion.State = ConnectionState.Closed Then
                Conexcion.ConnectionString = "SERVER=" & sServer & ";UID=" & sUser & _
                                     ";PWD=" & sPassword & ";DATABASE=micros.db"
                Conexcion.Open()
            End If
            Permite_Conexcion = True  'MessageBox.Show("Base de datos Conetada")
            Registro_Log = "SQLConnectString                   :" + sServer + "  " + sUser + "  " + sPassword + "  " + "Base de datos Conetada  " + Permite_Conexcion.ToString
            AppendLog(Registro_Log)
        Catch Error_SQLConnectString As Exception
            Permite_Conexcion = False
            Error_Field = "SQLConnectString                   :" + sServer + "  " + sUser + "  " + sPassword + "  " + "Base de datos NO Conetada  " + Permite_Conexcion.ToString + "  " + Error_SQLConnectString.ToString
            Write_event_log_error_file(Error_Field)
            'New MessageBox.Show("Error SQLConnectString" & Error_SQLConnectString.ToString, "Error Conectando a la Base de Datos " & Error_SQLConnectString.ToString, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub ODBC_ConnectStringMICROS(ByVal sDataBase As String)
        Try
            Registro_Log = "ODBCDataBase                      :" + sDataBase
            AppendLog(Registro_Log)
            If ConexcionMCRSPOS.State = ConnectionState.Closed Then
                ConexcionMCRSPOS.ConnectionString = "DSN=Micros;UID=dba;PWD=Db@M@5t3r$;Mode=Read"
                'ConexcionMCRSPOS.ConnectionString = "DSN=Micros;UID=dba;PWD=Micros3998.;Mode=Read"
                ConexcionMCRSPOS.Open()
            End If
            Permite_Conexcion_MCRSPOS = True
            Registro_Log = "ODBC_ConnectStringMICROS           :" + sDataBase + "  " + Permite_Conexcion_MCRSPOS.ToString
            AppendLog(Registro_Log)
        Catch Error_ODBC_ConnectStringMICROS As Exception
            Permite_Conexcion = False
            Error_Field = "Error_ODBC_ConnectStringMICROS              :" + sDataBase + "  " + Permite_Conexcion_MCRSPOS.ToString + " " + Error_ODBC_ConnectStringMICROS.ToString
            Write_event_log_error_file(Error_Field)
        End Try
    End Sub

    '''PORT CRM FROM MICROS
    Private Sub SQLReaderTimeout_interface_micros_CRM()
        Try
            Registro_Log = "SQLReaderTimeout_interface_micros_CRM                      :"
            AppendLog(Registro_Log)
            Permite_Conexcion_MCRSPOS = False
            Salir = False
            ODBC_ConnectStringMICROS("micros.db")
            If Permite_Conexcion_MCRSPOS = True Then
                Dim Command As New OdbcCommand("SELECT timeout, tcp_port_number FROM micros.interface_def where obj_num = 9 ", ConexcionMCRSPOS)
                Command.CommandType = CommandType.Text
                ReadSql_odbc = Command.ExecuteReader(CommandBehavior.CloseConnection)
                If ReadSql_odbc.Read = True Then
                    Timeout_Ifc = ReadSql_odbc.GetValue(0)
                    PuertoID = ReadSql_odbc.GetValue(1)
                    Timeout_Ifc_Ws = ((Timeout_Ifc - 5) * 1000)
                    Permite_Conexcion_MCRSPOS = True
                    Registro_Log = "SQLReaderTimeout_interface_micros_CRM                      :" + Convert.ToString(PuertoID)
                    AppendLog(Registro_Log)
                End If
            Else
                Salir = True
            End If
            If Timeout_Ifc = 0 Then
                Timeout_Ifc = 30
                Timeout_Ifc_Ws = ((Timeout_Ifc - 5) * 1000)
            End If
        Catch Errores_SQLReaderTimeout_cfd_facturacion As Exception
            Permite_Conexcion_MCRSPOS = False
            Error_Field = "Errores_SQLReaderTimeout_cfd_facturacion                  :" + Convert.ToString(PuertoID) + "  " + Errores_SQLReaderTimeout_cfd_facturacion.ToString
            Write_event_log_error_file(Error_Field)
        Finally
            If Salir = False Then
                ReadSql_odbc.Close()
            End If
        End Try
    End Sub

    '''DELETE VAR
    Private Sub Borra_Variables()
        Registro_Log = "Borra_Variables                    :"
        AppendLog(Registro_Log)
        Permite_Conexcion = False

        Inicio = 0
        Inicio_Encabezado = 0
        Fin_Encabezado = 0
        Inicio_Datos = 0
        Fin_Datos = 0
        Inicio_Trailer = 0
        Fin_Trailer = 0
        Inicio_Separador = 0
        Fin_Separador = 0
        Longitud = 0
        Cadena_Inicio = ""
        Socket_Peticion = ""
        Encabezado = ""
        Datos = ""
        Datos1 = ""
        Trailer = ""
        Separador1 = ""
        Respuesta = ""
        Nombre = ""
        Numero_Tarjeta = ""
        Card_Status = ""
        Beneficios(1) = ""
        Beneficios(2) = ""
        Beneficios(3) = ""
        Respuesta = ""
        Respuesta2 = ""

    End Sub

    '''FILE CONFIG
    Private Sub ReadFile()
        Try
            myStreamReader = File.OpenText(FileConfiguracion_CRM)
            ' The Read() method returns '-1' when the end of the
            '   file has been reached
            While myStreamReader.Peek <> -1
                Respuesta = myStreamReader.ReadLine()
                Lectura = Respuesta.Split(",")
                If Lectura(0) = "<Interface CRM>" Then
                    numero_Interface = Convert.ToInt32(Lectura(1))
                ElseIf Lectura(0) = "<Mail Remitente>" Then
                    Mail_Remitente = Lectura(1)
                    Mail_Remitente_Password = Lectura(2)
                ElseIf Lectura(0) = "<Mail Destinatario>" Then
                    Mail_Destinatario = Lectura(1)
                ElseIf Lectura(0) = "<Identificador_cliente>" Then
                    entidad = Lectura(1)
                End If
            End While
        Catch exc As Exception
            ' Show the exception to the user.
            Error_Field = "No se puede abrir archivo." + vbCrLf + FileConfiguracion_CRM + vbCrLf + _
            vbCrLf + vbCrLf + "Exception: " + exc.Message
        Finally
            If Not myStreamReader Is Nothing Then
                myStreamReader.Close()
            End If
        End Try
    End Sub


    Sub Borra_Totales_Busqueda_Xml()
        PromocionId = ""
        PromocionName = ""
        PromocionUniqueKey = ""
        PromocionDisplayName = ""
        PromocionStatus = ""
    End Sub
    Sub Borra_Totales_Busqueda_Voucher_Xml()
        VoucherCalStatus = ""
        VoucherProductId = ""
        VoucherProductName = ""
        VoucherStatus = ""
        VoucherTransactionId = ""
    End Sub


    Sub Separa_Campos_Individules_Indice(ByVal Valor As String, ByRef Resp_valor As String, ByRef Indice As Integer)
        Resp_valor = ""
        If Respuesta1.IndexOf(Valor, Indice) > 0 Then
            Inicio2 = (Valor.Length + 1)
            Inicio3 = (Valor.Length - 1)
            Inicio_L = Respuesta1.IndexOf(Valor, Indice)
            Inicio_L = Inicio_L + Inicio2
            Cadena = Mid(Valor, 1, 1) & "/" & Mid(Valor, 2, Inicio3)
            If Respuesta1.IndexOf(Cadena, Inicio_L) > 0 Then
                Final_L = Respuesta1.IndexOf(Cadena, Inicio_L)
            End If
            If Inicio_L > 0 And Final_L > 0 Then
                Longitud_L = (Final_L - Inicio_L) + 1
                Resp_valor = Mid(Respuesta1, Inicio_L, Longitud_L)
                Indice = Final_L
            End If
        End If
    End Sub
    Sub Separa_Campos_Individules_Indice_Uno(ByVal Valor As String, ByRef Resp_valor As String, ByRef Indice As Integer)
        Resp_valor = ""
        If Respuesta1.IndexOf(Valor, Indice) > 0 Then
            Inicio2 = (Valor.Length + 1)
            Inicio3 = (Valor.Length - 1)
            Inicio_L = Respuesta1.IndexOf(Valor, Indice)
            Inicio_L = Inicio_L + Inicio2
            Cadena = Mid(Valor, 1, 1) & "/" & Mid(Valor, 2, Inicio3)
            If Respuesta1.IndexOf(Cadena, Inicio_L) > 0 Then
                Final_L = Respuesta1.IndexOf(Cadena, Inicio_L)
            Else
                Final_L = Inicio_L
                Indice = Final_L
                Resp_valor = ""
                Exit Sub
            End If
            If Inicio_L > 0 And Final_L > 0 Then
                Longitud_L = (Final_L - Inicio_L) + 1
                Resp_valor = Mid(Respuesta1, Inicio_L, Longitud_L)
                Indice = Final_L
            End If
        End If
    End Sub

    Sub Separa_Campos_Individules(ByVal Valor As String, ByRef Resp_valor As String)
        Resp_valor = ""
        ''Write_event_log_error_file(Valor)
        If Respuesta1.IndexOf(Valor, 0) > 0 Then
            Inicio2 = (Valor.Length + 1)
            Inicio3 = (Valor.Length - 1)
            Inicio_L = Respuesta1.IndexOf(Valor, 0)
            Inicio_L = Inicio_L + Inicio2 + 9
            ''Cadena = Mid(Valor, 1, 1) & "/" & Mid(Valor, 2, Inicio3)
            Cadena = "/" & Mid(Valor, 1, Inicio3)
            ''Write_event_log_error_file(Cadena)
            If Respuesta1.IndexOf(Cadena, Inicio_L) > 0 Then
                Final_L = Respuesta1.IndexOf(Cadena, 0)
            End If
            If Inicio_L > 0 And Final_L > 0 Then
                Longitud_L = (Final_L - Inicio_L) + 1
                Resp_valor = Mid(Respuesta1, Inicio_L, Longitud_L)
                Resp_valor = Mid(Respuesta1, Inicio_L - 2, 1) + Resp_valor
            End If
        End If
        ''Write_event_log_error_file(Resp_valor)
    End Sub
    Sub Separa_Campos_Individules_Favorite(ByVal Valor As String, ByVal Valor2 As String, ByVal Valor3 As String, ByRef Resp_valor As String, ByRef Indice As Integer)
        ''Separa_Campos_Individules_Favorite("<Value>", "Last Beverage", Last_Beverage, Inicio_L)
        Resp_valor = ""
        Inicio_L = 0
        ''Write_event_log_error_file(Valor)
        Try
            If Respuesta1.IndexOf(Valor2, Indice) > 0 Then
                If Respuesta1.IndexOf(Valor, Respuesta1.IndexOf(Valor2, Indice)) > 0 Then
                    Inicio2 = (Valor.Length + 1)
                    Inicio3 = (Valor.Length - 1)
                    Inicio_L = Respuesta1.IndexOf(Valor, Respuesta1.IndexOf(Valor2, Indice))
                    Inicio_L = Inicio_L + Inicio2 + 1
                    Cadena = Mid(Valor, 1, 1) & "/" & Mid(Valor, 2, Inicio3)
                    Cadena = Valor3
                    ''Write_event_log_error_file(Cadena)
                    If Respuesta1.IndexOf(Cadena, Inicio_L) > 0 Then
                        Final_L = Respuesta1.IndexOf(Cadena, Inicio_L)
                    End If
                    If Inicio_L > 0 And Final_L > 0 Then
                        Longitud_L = (Final_L - Inicio_L)
                        Resp_valor = Mid(Respuesta1, Inicio_L, Longitud_L)
                        Indice = Final_L
                        Resp_valor.Replace(Chr(34), ".")
                    End If
                ElseIf Respuesta1.IndexOf(Valor3, Respuesta1.IndexOf(Valor2, Indice)) > 0 Then
                    Inicio2 = (Valor3.Length + 1)
                    Inicio3 = (Valor3.Length - 1)
                    Inicio_L = Respuesta1.IndexOf(Valor3, Respuesta1.IndexOf(Valor2, Indice))
                    'Inicio_L = Inicio_L + Inicio2
                    Cadena = Valor3
                    ''Write_event_log_error_file(Cadena)
                    If Respuesta1.IndexOf(Cadena, Inicio_L) > 0 Then
                        Final_L = Respuesta1.IndexOf(Cadena, Inicio_L)
                    End If
                    If Inicio_L > 0 And Final_L > 0 Then
                        Longitud_L = (Final_L - Inicio_L) + 1
                        Resp_valor = Mid(Respuesta1, Inicio_L, Longitud_L)
                        Indice = Final_L
                    End If
                End If
            End If
        Catch exp As Exception
            Respuesta = exp.ToString
            'MessageBox.Show(Respuesta)
        End Try
        ''Write_event_log_error_file(Resp_valor)
    End Sub
    Sub Separa_Campos_Individules_Favorite_Sin_Borrar(ByVal Valor As String, ByVal Valor2 As String, ByVal Valor3 As String, ByRef Resp_valor As String, ByRef Indice As Integer)
        ''Separa_Campos_Individules_Favorite("<Value>", "Last Beverage", Last_Beverage, Inicio_L)
        Resp_valor = ""
        ''Inicio_L = 0
        ''Write_event_log_error_file(Valor)
        Try
            If Respuesta1.IndexOf(Valor2, Indice) > 0 Then
                If Respuesta1.IndexOf(Valor, Respuesta1.IndexOf(Valor2, Indice)) > 0 Then
                    Inicio2 = (Valor.Length + 1)
                    Inicio3 = (Valor.Length - 1)
                    Inicio_L = Respuesta1.IndexOf(Valor, Respuesta1.IndexOf(Valor2, Indice))
                    Inicio_L = Inicio_L + Inicio2 + 1
                    Cadena = Mid(Valor, 1, 1) & "/" & Mid(Valor, 2, Inicio3)
                    Cadena = Valor3
                    ''Write_event_log_error_file(Cadena)
                    If Respuesta1.IndexOf(Cadena, Inicio_L) > 0 Then
                        Final_L = Respuesta1.IndexOf(Cadena, Inicio_L)
                    End If
                    If Inicio_L > 0 And Final_L > 0 Then
                        Longitud_L = (Final_L - Inicio_L)
                        Resp_valor = Mid(Respuesta1, Inicio_L, Longitud_L)
                        Indice = Final_L
                        Resp_valor.Replace(Chr(34), ".")
                    End If
                ElseIf Respuesta1.IndexOf(Valor3, Respuesta1.IndexOf(Valor2, Indice)) > 0 Then
                    Inicio2 = (Valor3.Length + 1)
                    Inicio3 = (Valor3.Length - 1)
                    Inicio_L = Respuesta1.IndexOf(Valor3, Respuesta1.IndexOf(Valor2, Indice))
                    'Inicio_L = Inicio_L + Inicio2
                    Cadena = Valor3
                    ''Write_event_log_error_file(Cadena)
                    If Respuesta1.IndexOf(Cadena, Inicio_L) > 0 Then
                        Final_L = Respuesta1.IndexOf(Cadena, Inicio_L)
                    End If
                    If Inicio_L > 0 And Final_L > 0 Then
                        Longitud_L = (Final_L - Inicio_L) + 1
                        Resp_valor = Mid(Respuesta1, Inicio_L, Longitud_L)
                        Indice = Final_L
                    End If
                End If
            End If
        Catch exp As Exception
            Respuesta = exp.ToString
            'MessageBox.Show(Respuesta)
        End Try
        ''Write_event_log_error_file(Resp_valor)
    End Sub
    Private Sub SQLInsert_Datos_CRM()
        Try
            Insert_Datos_CRM = False
            Permite_Conexcion_CRM_Micros = False
            Salir = False
            ODBC_ConnectStringMICROS()
            If Permite_Conexcion_CRM_Micros = True Then
                InsertTicket = "INSERT INTO custom.CRM_Respuesta(Fecha, Tienda, CentroConsumo, Cheque, NumeroMiembro, Respuesta ) VALUES("
                InsertTicket = InsertTicket & "'" & Business_date & "',"
                InsertTicket = InsertTicket & " " & Convert.ToInt32(Tienda) & ","
                InsertTicket = InsertTicket & " " & Convert.ToInt32(CentroConsumo) & ","
                InsertTicket = InsertTicket & " " & Convert.ToInt32(Cheque) & ","
                InsertTicket = InsertTicket & " '" & numero_Miembro & "',"
                InsertTicket = InsertTicket & " '" & Respuesta1 & "' "
                'InsertTicket = InsertTicket & " '" & Card_Status & "', "
                'InsertTicket = InsertTicket & " '" & Nivel & "', "
                'InsertTicket = InsertTicket & " '" & Organizacion & "', "
                'InsertTicket = InsertTicket & " '" & NivelId & "', "
                'InsertTicket = InsertTicket & " '" & NivelNombre & "' "
                InsertTicket = InsertTicket & " ) "
                Write_event_log_error_file(InsertTicket)
                Dim Command As New OdbcCommand(InsertTicket, ConexcionMCRSPOS)
                Command.CommandType = CommandType.Text
                ReadSql = Command.ExecuteScalar
                Insert_Datos_CRM = True
            End If
        Catch Errores_SQLInsert_Ticket_CFD As Exception
            Registro_Log = "Errores_SQLInsert_Ticket_CFD              :" + Errores_SQLInsert_Ticket_CFD.ToString
            Insert_Datos_CRM = False
            Write_event_log_error_file(Registro_Log)
        Finally
            ODBC_ConnectStringMicrosClose()
        End Try
    End Sub
    Private Sub ODBC_ConnectStringMICROS()
        Try
            If ConexcionMCRSPOS.State = ConnectionState.Closed Then
                ''ConexcionMCRSPOS.ConnectionString = "DSN=Micros;UID=dba;PWD=smc9nair;Mode=Read"
                ConexcionMCRSPOS.ConnectionString = "DSN=Micros;UID=dba;PWD=Db@M@5t3r$;Mode=Read"
                ''ConexcionMCRSPOS.ConnectionString = "DSN=Micros;UID=dba;PWD=Micros3998.;Mode=Read"
                ConexcionMCRSPOS.Open()
            End If
            Permite_Conexcion_CRM_Micros = True
        Catch Error_ODBC_ConnectStringMICROS As Exception
            Permite_Conexcion_CRM_Micros = False
            Registro_Log = "Error_ODBC_ConnectStringMICROS              :" + Permite_Conexcion_MCRSPOS.ToString + " " + Error_ODBC_ConnectStringMICROS.ToString
            Write_event_log_error_file(Registro_Log)
        End Try
    End Sub
    Private Sub ODBC_ConnectStringMICROS_Dos()
        Try
            If ConexcionMCRSPOS_Dos.State = ConnectionState.Closed Then
                ''ConexcionMCRSPOS.ConnectionString = "DSN=Micros;UID=dba;PWD=smc9nair;Mode=Read"
                ConexcionMCRSPOS_Dos.ConnectionString = "DSN=Micros;UID=dba;PWD=Db@M@5t3r$;Mode=Read"
                ''ConexcionMCRSPOS.ConnectionString = "DSN=Micros;UID=dba;PWD=Micros3998.;Mode=Read"
                ConexcionMCRSPOS_Dos.Open()
            End If
            Permite_Conexcion_CRM_Micros_Dos = True
        Catch Error_ODBC_ConnectStringMICROS_Dos As Exception
            Permite_Conexcion_CRM_Micros_Dos = False
            Registro_Log = "Error_ODBC_ConnectStringMICROS              :" + Permite_Conexcion_CRM_Micros_Dos.ToString + " " + Error_ODBC_ConnectStringMICROS_Dos.ToString
            Write_event_log_error_file(Registro_Log)
        End Try
    End Sub
    Private Sub ODBC_ConnectStringMICROS_Tres()
        Try
            If ConexcionMCRSPOS_Tres.State = ConnectionState.Closed Then
                ''ConexcionMCRSPOS.ConnectionString = "DSN=Micros;UID=dba;PWD=smc9nair;Mode=Read"
                ConexcionMCRSPOS_Tres.ConnectionString = "DSN=Micros;UID=dba;PWD=Db@M@5t3r$;Mode=Read"
                ''ConexcionMCRSPOS.ConnectionString = "DSN=Micros;UID=dba;PWD=Micros3998.;Mode=Read"
                ConexcionMCRSPOS_Tres.Open()
            End If
            Permite_Conexcion_CRM_Micros_Tres = True
        Catch Error_ODBC_ConnectStringMICROS_Tres As Exception
            Permite_Conexcion_CRM_Micros_Tres = False
            Registro_Log = "Error_ODBC_ConnectStringMICROS_Tres              :" + Permite_Conexcion_CRM_Micros_Tres.ToString + " " + Error_ODBC_ConnectStringMICROS_Tres.ToString
            Write_event_log_error_file(Registro_Log)
        End Try
    End Sub
    Private Sub ODBC_ConnectStringMicrosClose()
        Try
            If ConexcionMCRSPOS.State = ConnectionState.Open Then
                ConexcionMCRSPOS.Close()
                Permite_Conexcion_CRM_Micros = False
            End If
        Catch Error_ODBC_ConnectStringFacturacionClose As Exception
            Registro_Log = "ODBC_ConnectStringMicrosClose      :" + Error_ODBC_ConnectStringFacturacionClose.ToString
            Write_event_log_error_file(Registro_Log)
        End Try
    End Sub
    Private Sub ODBC_ConnectStringMicrosCloseTres()
        Try
            If ConexcionMCRSPOS_Tres.State = ConnectionState.Open Then
                ConexcionMCRSPOS_Tres.Close()
                Permite_Conexcion_CRM_Micros_Tres = False
            End If
        Catch Error_ODBC_ConnectStringMicrosCloseTres As Exception
            Registro_Log = "Error_ODBC_ConnectStringMicrosCloseTres      :" + Error_ODBC_ConnectStringMicrosCloseTres.ToString
            Write_event_log_error_file(Registro_Log)
        End Try
    End Sub

    Sub Procesa_Busqueda_Nivel()
        Write_event_log_error_file(Mid(Datos, 1, 3))
        'Respuesta1 = Ws.busca_numero_miembro_Recompensas("C:\\WS\\ALSE_LOY_Recompensas.WSDL", "ConsultaRecompensas", "", "myStarbucks Rewards Mexico")
        Nombre_Ws = Path_WS + "ALSE_LOY_Recompensas.WSDL"
        Respuesta1 = Ws.busca_numero_miembro_Recompensas(Nombre_Ws, "ConsultaRecompensas", "", "myStarbucks Rewards Mexico")
        Write_event_log_error_file(Respuesta1)
    End Sub
    Sub Proceso_Main_Busqueda_Nivel()
        If Now.Hour = 6 Then
            If Procesado_Nivel <> "T" Then
                Procesa_Busqueda_Nivel()
                Procesado_Nivel = "T"
            End If
        Else
            Procesado_Nivel = ""
        End If
    End Sub
    Sub Procesa_Ejecuta_JAR()
        Campos_Paso = Datos.Split("|")
        Jar_Ejecutar = Campos_Paso(1)
        TicketNumber = Campos_Paso(2)
        MemberNumber = Campos_Paso(3)
        If Jar_Ejecutar = "REDENCIONES" Then
            Solicita_JAR_WS_Redencion()
            If Respuesta <> 0 Then
                Respuesta = "NO CONTESTA WS [Redencion]," & TicketNumber & ", " & MemberNumber
                Graba_Log_Redenciones(Respuesta)
                Respuesta = 0
            Else
                SQLSearchCheques_Respuesta_Redenciones(TicketNumber)
            End If
        ElseIf Jar_Ejecutar = "ACREDITACIONES" Then
            Solicita_JAR_WS_Acreditacion()
            If Respuesta <> 0 Then
                Respuesta = "NO CONTESTA WS [Acreditacion]"
                SQLInsert_Acreditaciones_CRM()
                SQLUpdate_Acreditaciones_CRM()
                SQLUpdate_Acreditaciones_Transmitido_CRM()
                Respuesta = 0
            Else
                SQLSearchCheques_Respuesta_Acreditaciones(TicketNumber)
            End If
        End If
    End Sub


    Private Sub Solicita_JAR_WS_Acreditacion()
        Try
            Archivo_Trabajar = "C:\JAVA_WS_CRM\clienteAcreditacionJAR\" & FileEjecutaProgramaJava
            'Timeout_Ifc_Ws = (15 * 1000)
            Timeout_Ifc_Ws = (80 * 1000)
            Respuesta = Shell(Archivo_Trabajar, AppWinStyle.Hide, True, Timeout_Ifc_Ws)
            Registro_Log = "Solicita_JAR_WS_Acreditacion              :" + Archivo_Trabajar
            Write_event_log_error_file(Registro_Log)
        Catch Errores_Solicita_JAR_WS_Acreditacion As Exception
            Registro_Log = "Solicita_JAR_WS_Acreditacion              :" + Archivo_Trabajar
            Write_event_log_error_file(Registro_Log)
        Finally
        End Try
    End Sub


    Private Sub Envio_mail_CRM()
        Dim services() As ServiceController = ServiceController.GetServices
        Dim service As ServiceController
        Dim blnHasSmtpService As Boolean = False
        Dim Server As String
        Dim Mail_To As String
        Dim Mail_From As String
        Dim Mensaje, Subject, Body As String
        Dim arlAttachments As ArrayList
        For Each service In services
            If service.ServiceName.ToLower = "smtpsvc" Then
                blnHasSmtpService = True
                Exit For
            End If
        Next
        If Not blnHasSmtpService Then
            Registro_Log = "You do not have SMTP Service installed on this machine. Please check the Readme file for information on how to install SMTP Service."
            Write_event_log_error_file(Registro_Log)
        End If
        ' Ensure the SMTP Service is running. If not, start it.
        If Not service.Status = ServiceControllerStatus.Running Then
            Registro_Log = "SMTP Service not currently running. Starting service..."
            Try
                service.Start()
            Catch
                Registro_Log = "There was an error when attempting to start SMTP Service. Please consult the Readme file for more information."
                Write_event_log_error_file(Registro_Log)
            End Try
        End If
        Try
            Campos_Paso = Datos.Split("|")
            Mail_From = Mail_Remitente
            Mail_To = Mail_Destinatario
            Subject = "Error CRM"
            Dim sb As New StringBuilder

            ' Build the email message body.
            sb.Append("Mensaje de Servico CRM ")
            sb.Append(vbCrLf)
            sb.Append(vbCrLf)
            sb.Append("MENSAJE: ")
            sb.Append(Campos_Paso(0))
            sb.Append(Campos_Paso(1))
            sb.Append(Campos_Paso(2))
            sb.Append(Campos_Paso(3))
            sb.Append(Campos_Paso(4))
            sb.Append(vbCrLf)
            sb.Append(vbCrLf)
            sb.Append("TIENDA:" & Tienda)
            ' Creating a mail message is as simple as instantiating a class and 
            ' setting a few properties.
            Dim mailMsg As New MailMessage
            With mailMsg
                .From = Mail_From
                .To = Mail_To
                '.Bcc = txtBCC.Text.Trim
                .Subject = Subject
                .Body = sb.ToString
                .Priority = MailPriority.Normal

                If Not IsNothing(arlAttachments) Then
                    Dim mailAttachment As Object
                    For Each mailAttachment In arlAttachments
                        .Attachments.Add(mailAttachment)
                    Next
                End If
                '.Attachments.Add(New MailAttachment(Archivo_Enviar))
                mailMsg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserverport", "26") 'Set port number here
                mailMsg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", 1)  'Set the authenticate
                mailMsg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "facturacionalsea") 'Set the username
                mailMsg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "Fac4l$ea10") 'Set the password
                Server = "email.alsea.com.mx"
                SmtpMail.SmtpServer = Server  'Set server name
                'Try
                SmtpMail.Send(mailMsg)
            End With
            Respuesta2 = "ENVIADO"
        Catch exp As Exception
            Registro_Log = "The following problem occurred when attempting to send your email (invoice): " & exp.Message
            Respuesta2 = "NO ENVIADO"
        End Try
    End Sub

    Private Sub Graba_Log_Redenciones(ByVal Reg_Redenciones_Log As String)
        Try
            txtFileNameRedencionesLog = Path_WS & File_Log_Redenciones_CRM
            myStreamWriter = File.AppendText(txtFileNameRedencionesLog)
            myStreamWriter.WriteLine(Now.ToLongDateString & ", " & Now.ToLongTimeString & ", " & Reg_Redenciones_Log)
            myStreamWriter.Flush()
        Catch Errores_graba_log As Exception
            Registro_Log = "Graba_Log_Redenciones          :" + Errores_graba_log.ToString
        Finally
            If Not myStreamWriter Is Nothing Then
                myStreamWriter.Close()
            End If
        End Try
    End Sub

    Private Sub SQLInsert_Acreditaciones_CRM()
        Try
            Insert_Datos_CRM = False
            Permite_Conexcion_CRM_Micros = False
            Salir = False
            ODBC_ConnectStringMICROS()
            If Permite_Conexcion_CRM_Micros = True Then
                InsertTicket = "INSERT INTO custom.LOYTRANSACCION_OFFLINE SELECT * FROM custom.LOYTRANSACCION "
                InsertTicket = InsertTicket & "WHERE TicketNumber = '" & TicketNumber & "' "
                InsertTicket = InsertTicket & "AND  TransactionType = 'Accrual' "
                Write_event_log_error_file(InsertTicket)
                Dim Command As New OdbcCommand(InsertTicket, ConexcionMCRSPOS)
                Command.CommandType = CommandType.Text
                ReadSql = Command.ExecuteScalar
                Insert_Datos_CRM = True
            End If
        Catch Errores_SQLInsert_Ticket_CFD As Exception
            Registro_Log = "SQLInsert_Acreditaciones_CRM              :" + Errores_SQLInsert_Ticket_CFD.ToString
            Insert_Datos_CRM = False
            Write_event_log_error_file(Registro_Log)
        Finally
            ODBC_ConnectStringMicrosClose()
        End Try
    End Sub
    Private Sub SQLUpdate_Acreditaciones_CRM()
        Try
            Insert_Datos_CRM = False
            Permite_Conexcion_CRM_Micros = False
            Salir = False
            ODBC_ConnectStringMICROS()
            If Permite_Conexcion_CRM_Micros = True Then
                InsertTicket = "UPDATE  custom.LOYTRANSACCION_OFFLINE set Estatus = 'PEN' "
                InsertTicket = InsertTicket & "WHERE TicketNumber = '" & TicketNumber & "' "
                Write_event_log_error_file(InsertTicket)
                Dim Command As New OdbcCommand(InsertTicket, ConexcionMCRSPOS)
                Command.CommandType = CommandType.Text
                ReadSql = Command.ExecuteScalar
                Insert_Datos_CRM = True
            End If
        Catch Errores_SQLInsert_Ticket_CFD As Exception
            Registro_Log = "SQLUpdate_Acreditaciones_CRM              :" + Errores_SQLInsert_Ticket_CFD.ToString
            Insert_Datos_CRM = False
            Write_event_log_error_file(Registro_Log)
        Finally
            ODBC_ConnectStringMicrosClose()
        End Try
    End Sub
    Private Sub SQLUpdate_Acreditaciones_Transmitido_CRM()
        Try
            Insert_Datos_CRM = False
            Permite_Conexcion_CRM_Micros = False
            Salir = False
            ODBC_ConnectStringMICROS()
            If Permite_Conexcion_CRM_Micros = True Then
                InsertTicket = "Update custom.LOYTRANSACCION set Transmitido = 'F' WHERE TicketNumber = '" & TicketNumber & "' "
                Write_event_log_error_file(InsertTicket)
                Dim Command As New OdbcCommand(InsertTicket, ConexcionMCRSPOS)
                Command.CommandType = CommandType.Text
                ReadSql = Command.ExecuteScalar
                Insert_Datos_CRM = True
            End If
        Catch Errores_SQLInsert_Ticket_CFD As Exception
            Registro_Log = "SQLUpdate_Acreditaciones_CRM              :" + Errores_SQLInsert_Ticket_CFD.ToString
            Insert_Datos_CRM = False
            Write_event_log_error_file(Registro_Log)
        Finally
            ODBC_ConnectStringMicrosClose()
        End Try
    End Sub


    Sub Procesa_Ejecuta_Acreditacion_OffLine()
        Campos_Paso = Datos.Split("|")
        TicketNumber = Campos_Paso(1)
        SQLInsert_Acreditaciones_CRM()
        SQLUpdate_Acreditaciones_CRM()
        SQLUpdateLoytransaccion_Acreditaciones_CRM()
    End Sub
    Private Sub SQLUpdateLoytransaccion_Acreditaciones_CRM()
        Try
            Insert_Datos_CRM = False
            Permite_Conexcion_CRM_Micros = False
            Salir = False
            ODBC_ConnectStringMICROS()
            If Permite_Conexcion_CRM_Micros = True Then
                InsertTicket = "UPDATE  custom.LOYTRANSACCION set Estatus = 'TER' "
                InsertTicket = InsertTicket & "WHERE TicketNumber = '" & TicketNumber & "' "
                Write_event_log_error_file(InsertTicket)
                Dim Command As New OdbcCommand(InsertTicket, ConexcionMCRSPOS)
                Command.CommandType = CommandType.Text
                ReadSql = Command.ExecuteScalar
                Insert_Datos_CRM = True
            End If
        Catch Errores_SQLInsert_Ticket_CFD As Exception
            Registro_Log = "SQLUpdate_Acreditaciones_CRM              :" + Errores_SQLInsert_Ticket_CFD.ToString
            Insert_Datos_CRM = False
            Write_event_log_error_file(Registro_Log)
        Finally
            ODBC_ConnectStringMicrosClose()
        End Try
    End Sub
    Private Sub SQLSearchCheques_Respuesta_Acreditaciones(ByVal TicketNumber_ As String)
        Try
            Permite_Conexcion_CRM_Micros_Dos = False
            Salir = False
            ODBC_ConnectStringMICROS_Dos()
            If Permite_Conexcion_CRM_Micros_Dos = True Then
                Campo = "SELECT FIRST Consecutivo, Estatus, IntegrationStatus, Status, SubStatus FROM custom.LOYTRANSACCION_RESPONSE Where ProductName <> 'Ticket' And TicketNumber = '" & TicketNumber_ & "'  And TransactionType = 'Accrual' ORDER BY Consecutivo DESC"
                Dim Command As New OdbcCommand(Campo, ConexcionMCRSPOS_Dos)
                Command.CommandType = CommandType.Text
                Registro_Log = ("SQLSearchCanceladosOffLine: " & Campo)
                Write_event_log_error_file(Registro_Log)
                ReadSql_odbc_Dos = Command.ExecuteReader(CommandBehavior.CloseConnection)
                While ReadSql_odbc_Dos.Read = True
                    Consecutivo = ReadSql_odbc_Dos.GetValue(0)
                    Estatus = ReadSql_odbc_Dos.GetValue(1)
                    IntegrationStatus = ReadSql_odbc_Dos.GetValue(2)
                    Status = ReadSql_odbc_Dos.GetValue(3)
                    SubStatus = ReadSql_odbc_Dos.GetValue(4)
                    Registro_Log = ("SQLReadResponse: " & Consecutivo.ToString & ":" & Estatus & ":" & IntegrationStatus & ":" & Status & ":" & SubStatus)
                    Write_event_log_error_file(Registro_Log)
                    If Estatus.Trim = "Fail" Or Mid(IntegrationStatus, 1, 4) = "Fail" Then
                        'SQLCancela_Ticket_Acreditaciones_CRM()
                        If Mid(IntegrationStatus, 1, 64) = "Fail - Duplicate Transaction exists for attributes Ticket Number" Then
                            'Sale No agrega a Off Line
                        Else
                            SQLInsert_Acreditaciones_CRM()
                            SQLUpdate_Acreditaciones_CRM()
                            SQLUpdate_Acreditaciones_Transmitido_CRM()
                        End If
                        'Else   TEST
                        'SQLInsert_Acreditaciones_CRM()
                        'SQLUpdate_Acreditaciones_CRM()
                        'SQLUpdate_Acreditaciones_Transmitido_CRM()
                    End If
                End While
            Else
                Salir = True
            End If
        Catch Errores_SQLSearchCheques_Respuesta As Exception
            Registro_Log = "Errores_SQLSearchCheques_Respuesta               :" + Errores_SQLSearchCheques_Respuesta.ToString
            EventLog.WriteEntry(Registro_Log)
        Finally
            If Salir = False Then
                ReadSql_odbc_Dos.Close()
                ODBC_ConnectStringMICROS_Dos()
            End If
        End Try
    End Sub
    Private Sub SQLCancela_Ticket_Acreditaciones_CRM()
        Try
            Permite_Conexcion_CRM_Micros_Tres = False
            Salir = False
            Delete_Datos_CRM = False
            ODBC_ConnectStringMICROS_Tres()
            If Permite_Conexcion_CRM_Micros_Tres = True Then
                DeleteTicket = "Delete custom.LOYTRANSACCION_RESPONSE WHERE Estatus = 'Success' And IntegrationStatus = 'Success' And TicketNumber = '" & TicketNumber & "' "
                Write_event_log_error_file(DeleteTicket)
                Dim Command As New OdbcCommand(DeleteTicket, ConexcionMCRSPOS_Tres)
                Command.CommandType = CommandType.Text
                ReadSql = Command.ExecuteScalar
                Delete_Datos_CRM = True
            End If
        Catch Errores_SQLCancela_Ticket_Acreditaciones_CRM As Exception
            Registro_Log = "Errores_SQLCancela_Ticket_Acreditaciones_CRM              :" + Errores_SQLCancela_Ticket_Acreditaciones_CRM.ToString
            Delete_Datos_CRM = False
            Write_event_log_error_file(Registro_Log)
        Finally
            ODBC_ConnectStringMicrosCloseTres()
        End Try
    End Sub
    Private Sub SQLSearchCheques_Respuesta_Redenciones(ByVal TicketNumber_ As String)
        Try
            Permite_Conexcion_CRM_Micros_Dos = False
            Salir = False
            ODBC_ConnectStringMICROS_Dos()
            If Permite_Conexcion_CRM_Micros_Dos = True Then
                Campo = "SELECT FIRST Consecutivo, Estatus, IntegrationStatus, Status, SubStatus FROM custom.LOYTRANSACCION_RESPONSE Where ProductName <> 'Ticket' And TicketNumber = '" & TicketNumber_ & "'  And TransactionType = 'Redemption' ORDER BY Consecutivo DESC"
                Dim Command As New OdbcCommand(Campo, ConexcionMCRSPOS_Dos)
                Command.CommandType = CommandType.Text
                Registro_Log = ("SQLSearchCanceladosOffLine: " & Campo)
                Write_event_log_error_file(Registro_Log)
                ReadSql_odbc_Dos = Command.ExecuteReader(CommandBehavior.CloseConnection)
                Consecutivo = 0
                Estatus = ""
                IntegrationStatus = ""
                Status = ""
                SubStatus = ""
                While ReadSql_odbc_Dos.Read = True
                    Consecutivo = ReadSql_odbc_Dos.GetValue(0)
                    Estatus = ReadSql_odbc_Dos.GetValue(1)
                    IntegrationStatus = ReadSql_odbc_Dos.GetValue(2)
                    Status = ReadSql_odbc_Dos.GetValue(3)
                    SubStatus = ReadSql_odbc_Dos.GetValue(4)
                    Registro_Log = ("SQLReadResponse: " & Consecutivo.ToString & ":" & Estatus & ":" & IntegrationStatus & ":" & Status & ":" & SubStatus)
                    Write_event_log_error_file(Registro_Log)
                    If Estatus.Trim = "Fail" Or Mid(IntegrationStatus, 1, 4) = "Fail" Then
                        Respuesta = "Respuesta [Redencion]:," & TicketNumber & ", " & MemberNumber & ", " & IntegrationStatus
                        Graba_Log_Redenciones(Respuesta)
                    ElseIf Estatus.Length <> 0 And IntegrationStatus.Length <> 0 Then
                        'Respuesta = "Valor [Redencion]," & Estatus & ", " & IntegrationStatus
                        'Graba_Log_Redenciones(Respuesta)
                    End If
                End While
            Else
                Salir = True
            End If
        Catch Errores_SQLSearchCheques_Respuesta_Redenciones As Exception
            Registro_Log = "Errores_SQLSearchCheques_Respuesta_Redenciones               :" + Errores_SQLSearchCheques_Respuesta_Redenciones.ToString
            EventLog.WriteEntry(Registro_Log)
        Finally
            If Salir = False Then
                ReadSql_odbc_Dos.Close()
                ODBC_ConnectStringMICROS_Dos()
            End If
        End Try
    End Sub
    Sub Procesa_Ejecuta_JAR_REDENCIONES()
        Campos_Paso = Datos.Split("|")
        Jar_Ejecutar = Campos_Paso(1)
        TicketNumber = Campos_Paso(2)
        MemberNumber = Campos_Paso(3)
        Solicita_JAR_WS_Redencion()
        If Respuesta <> 0 Then
            Respuesta = "NO CONTESTA WS [Redencion]," & TicketNumber & ", " & MemberNumber
            Graba_Log_Redenciones(Respuesta)
        Else
            SQLSearchCheques_Respuesta_Redenciones(TicketNumber)
        End If
    End Sub
    Private Sub Solicita_JAR_WS_Redencion()
        Try
            Archivo_Trabajar = "C:\JAVA_WS_CRM\clienteRedencionJAR\" & FileEjecutaProgramaJava
            'Timeout_Ifc_Ws = (15 * 1000)
            'Timeout_Ifc_Ws = (16 * 1000)
            'Timeout_Ifc_Ws = (3 * 1000)
            Respuesta = Shell(Archivo_Trabajar, AppWinStyle.Hide, True, Timeout_Ifc_Ws)
            Registro_Log = "Solicita_JAR_WS_Redencion              :" + Archivo_Trabajar
            Write_event_log_error_file(Registro_Log)
        Catch Errores_Solicita_JAR_WS_Redencion As Exception
            Registro_Log = "Solicita_JAR_WS_Redencion              :" + Archivo_Trabajar
            Write_event_log_error_file(Registro_Log)
        Finally
        End Try
    End Sub

    ''New
    Sub Procesa_Autorizacion_Pinpad_Amex_II()
        Try
            CVV2 = LeeTarjeta.encriptaRijndael(Campos(11))
            Digest2 = Campos(1) & Campos(2) & Campos(7) & Campos(11)
            Digest = LeeTarjeta.encriptaHmac(Digest2)
            '28Oct2013 Fecha_Vencimiento = Campos(10) & Campos(9)
            no_serie_1 = Campos(10) & Campos(9)  ''28Oct2013
            No_Tarjeta_A = Campos(8)
            No_Tarjeta = LeeTarjeta.encriptaRijndael(No_Tarjeta_A)
            Titulares = Campos(6).Split("^")
            If Titulares.Length > 1 Then
                Titular = Titulares(1)
            Else
                Titular = Campos(6)
            End If
            Track1_A = Campos(12)
            Track1 = LeeTarjeta.encriptaRijndael(Track1_A)
            Track2_A = Campos(13)
            If Len(Track2_A) > 0 Then
                Track2 = LeeTarjeta.encriptaRijndael(Track2_A)
            Else
                Track2 = ""
            End If

            Email_Administrador = "ramon.padilla@alsea.com.mx"
            ''28Oct2013 Fecha_Vencimiento = LeeTarjeta.encriptaRijndael(Fecha_Vencimiento)
            no_serie_1 = LeeTarjeta.encriptaRijndael(no_serie_1)  ''28Oct2013
            Telefono_Cliente = ""
            Email_cliente = ""
            Enverioment = 3
            ''Log_Field = Campos(1) & Separador & Campos(2) & Separador & Campos(3) & Separador & Campos(4) & Separador & Campos(5) & Separador & Titular & Separador & Campos(7) & Separador & No_Tarjeta & Separador & Fecha_Vencimiento & Separador & CVV2 & Separador & Digest & Separador & Email_cliente & Separador & Telefono_Cliente & Separador & Enverioment & Separador & "JUAN" & Separador & "LOPEZ" & Separador & "06600" & Separador & "REFORMA 350 PISO 16" & Separador & Track1 & Separador & Track2 & Separador & 0 & Separador & 0 & Separador & Campos(14)
            Log_Field = Campos(1) & Separador & Campos(2) & Separador & Campos(3) & Separador & Campos(4) & Separador & Campos(5) & Separador & Titular & Separador & Campos(7) & Separador & No_Tarjeta & Separador & Fecha_Vencimiento & Separador & CVV2 & Separador & Digest & Separador & Email_cliente & Separador & Telefono_Cliente & Separador & "" & Separador & "" & Separador & "" & Separador & "" & Separador & Track1 & Separador & Track2 & Separador & 0 & Separador & 0 & Separador & Campos(14) & Separador & Campos(15) & Separador & Campos(16) & Separador & Campos(17) & Separador & Campos(18) & Separador & Email_Administrador  ''02Abr2013
            SQLInsertLogPinpad(Encabezado, Campos(0), Log_Field)
            ''Respuesta_Autorizacion_Amex = LeeTarjeta.compraAmex(Campos(1), Campos(2), Campos(3), Campos(4), Campos(5), Titular, Campos(7), No_Tarjeta, Fecha_Vencimiento, CVV2, Digest, Email_cliente, Telefono_Cliente, Enverioment, "JUAN", "LOPEZ", "06600", "REFORMA 350 PISO 16", Track1, Track2, 0, 0, Campos(14))
            ''Mar2013 Respuesta_Autorizacion_Amex = LeeTarjeta.compraAmex(Campos(1), Campos(2), Campos(3), Campos(4), Campos(5), Titular, Campos(7), No_Tarjeta, Fecha_Vencimiento, CVV2, Digest, Email_cliente, Telefono_Cliente, Enverioment, "", "", "", "", Track1, Track2, 0, 0, Campos(14))  ''Agregar los campos Mar2013
            Respuesta_Autorizacion_Amex = LeeTarjeta.compraAmex(Campos(1), Campos(2), Campos(3), Campos(4), Campos(5), Titular, Campos(7), No_Tarjeta, no_serie_1, CVV2, Digest, Email_cliente, Telefono_Cliente, Titular, "", "", "", Track1, Track2, 0, 0, Campos(14), Campos(15), Campos(16), Campos(17), Campos(18), Email_Administrador)  ''Agregados los campos Mar2013
            SQLInsertLogPinpad(Encabezado, "RESPUESTA:" & Campos(0), Respuesta_Autorizacion_Amex)
            Respuesta1 = Respuesta_Autorizacion_Amex
            Mensaje_Pinpad = LeeTarjeta.getMensaje().ToString
            Mensaje_Pinpad2 = LeeTarjeta.getMensajeError().ToString
            Mensaje_Pinpad3 = LeeTarjeta.getMensajeWebSer().ToString
            Mensaje_Pinpad_All1 = "3|" & Mensaje_Pinpad2 & "|" & Mensaje_Pinpad3 & "|" & Respuesta_Autorizacion_Amex
            ''MessageBox.Show(Mensaje_Pinpad_All1)
            'Else
            'Respuesta = Encabezado & Separador & Mensaje_Pinpad & Trailer
            'End If
            If Respuesta_Autorizacion_Amex.Length > 10 Then
                Separa_Campos_Individules_Favorite("value=", "imprimir", "/>", imprimir, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "autorizacion", "/>", autorizacion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "mensaje", "/>", mensaje, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "razonSocial", "/>", razonSocial, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "comercio", "/>", comercio, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "direccion", "/>", direccion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "afiliacion", "/>", Afiliacion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "serie", "/>", Serie, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "fhTransaccion", "/>", fhTransaccion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "hora", "/>", Hora_A, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "tarjeta", "/>", tarjeta, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "fhVencimiento", "/>", fhVencimiento, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "emisor", "/>", emisor, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "operacion", "/>", operacion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "monto", "/>", monto_A, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "ingreso", "/>", ingreso, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "referencia", "/>", Referencia, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "criptograma", "/>", criptograma, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "firma", "/>", firma, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "titular", "/>", Titular, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "consecutivo", "/>", Consecutivo, Inicio_L)

                Separa_Campos_Individules_Favorite("value=", "montoPesos", "/>", montoPesos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "montoPuntos", "/>", montoPuntos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "montoTotal", "/>", montoTotal, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "leyenda2", "/>", leyenda2, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoAnteriorPuntos", "/>", saldoAnteriorPuntos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoAnteriorPesos", "/>", saldoAnteriorPesos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoRedimidoPuntos", "/>", saldoRedimidoPuntos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoRedimidoPesos", "/>", saldoRedimidoPesos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoActualPuntos", "/>", saldoActualPuntos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoActualPesos", "/>", saldoActualPesos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "vigenciaPuntos", "/>", vigenciaPuntos, Inicio_L)

                'autorizacion.Replace(""",".Replace")
                If autorizacion.Length = 0 Then
                    autorizacion = "000000"
                    Pagare_Pinpad = "ERROR_A" & Separador & mensaje & Separador
                    ''MessageBox.Show("NO APROBADA 03")
                Else
                    Pagare_Pinpad = imprimir & Separador                      ''1
                    Pagare_Pinpad = Pagare_Pinpad & autorizacion & Separador  ''2
                    Pagare_Pinpad = Pagare_Pinpad & razonSocial & Separador   ''3
                    Pagare_Pinpad = Pagare_Pinpad & comercio & Separador      ''4
                    Pagare_Pinpad = Pagare_Pinpad & direccion & Separador     ''5
                    Pagare_Pinpad = Pagare_Pinpad & Afiliacion & Separador    ''6
                    Pagare_Pinpad = Pagare_Pinpad & Serie & Separador         ''7
                    Pagare_Pinpad = Pagare_Pinpad & fhTransaccion & Separador ''8
                    Pagare_Pinpad = Pagare_Pinpad & Hora_A & Separador        ''9
                    Pagare_Pinpad = Pagare_Pinpad & tarjeta & Separador       ''10
                    Pagare_Pinpad = Pagare_Pinpad & fhVencimiento & Separador ''11
                    Pagare_Pinpad = Pagare_Pinpad & emisor & Separador        ''12
                    Pagare_Pinpad = Pagare_Pinpad & operacion & Separador     ''13
                    Pagare_Pinpad = Pagare_Pinpad & monto_A & Separador       ''14
                    Pagare_Pinpad = Pagare_Pinpad & ingreso & Separador       ''15
                    Pagare_Pinpad = Pagare_Pinpad & Referencia & Separador    ''16
                    Pagare_Pinpad = Pagare_Pinpad & criptograma & Separador   ''17
                    Pagare_Pinpad = Pagare_Pinpad & Titular & Separador       ''18
                    Pagare_Pinpad = Pagare_Pinpad & firma & Separador         ''19
                    Pagare_Pinpad = Pagare_Pinpad & mensaje & Separador       ''20
                    Pagare_Pinpad = Pagare_Pinpad & montoPesos & Separador           ''21
                    Pagare_Pinpad = Pagare_Pinpad & montoPuntos & Separador          ''22
                    Pagare_Pinpad = Pagare_Pinpad & montoTotal & Separador           ''23
                    Pagare_Pinpad = Pagare_Pinpad & leyenda2 & Separador             ''24
                    Pagare_Pinpad = Pagare_Pinpad & saldoAnteriorPuntos & Separador  ''25
                    Pagare_Pinpad = Pagare_Pinpad & saldoAnteriorPesos & Separador   ''26
                    Pagare_Pinpad = Pagare_Pinpad & saldoRedimidoPuntos & Separador  ''27
                    Pagare_Pinpad = Pagare_Pinpad & saldoRedimidoPesos & Separador   ''28
                    Pagare_Pinpad = Pagare_Pinpad & saldoActualPuntos & Separador    ''29
                    Pagare_Pinpad = Pagare_Pinpad & saldoActualPesos & Separador     ''30
                    Pagare_Pinpad = Pagare_Pinpad & vigenciaPuntos & Separador       ''31
                    Pagare_Pinpad = Pagare_Pinpad & Consecutivo & Separador          ''32
                    ''MessageBox.Show("APROBADA")
                End If
                'LeeTarjeta.terminaTransaccion(autorizacion)

                'Pagare_Pinpad = LeeTarjeta.g
                'LeeTarjeta.terminaTransaccion(LeeTarjeta.getAutorizacion)
                Respuesta = Encabezado & Separador & Pagare_Pinpad & Trailer
            Else
                Mensaje_Pinpad = LeeTarjeta.getMensaje().ToString
                Respuesta = Encabezado & Separador & Mensaje_Pinpad_All1 & Trailer
                'LeeTarjeta.terminaTransaccion("000000")
                'LeeTarjeta.setMensajeError("NO APROBADA 02")
                'LeeTarjeta.terminaTransaccion("000000")
                Pagare_Pinpad = "NO APROBADA, 02"
                Respuesta = Encabezado & Separador & Pagare_Pinpad & Trailer
                ''MessageBox.Show("NO APROBADA 02")
            End If
            Borra_Totales_Tarjetas()
        Catch exp As Exception
            Respuesta = Encabezado & Separador & exp.ToString & Trailer
            SQLInsertLogPinpad(Encabezado, "RESPUESTA:" & Campos(0), Respuesta)
            ''MessageBox.Show(Respuesta)
        End Try
    End Sub

    Sub Procesa_Consulta_Pinpad_Voucher_II()
        Try
            Email_Administrador = "ramon.padilla@alsea.com.mx"
            Log_Field = Campos(1) & Separador & Campos(2) & Separador & Campos(3) & Separador & Campos(4) & Separador & Campos(5) & Separador & Campos(6) & Separador & Email_Administrador & Separador & Campos(7) & Separador & Campos(8)
            SQLInsertLogPinpad(Encabezado, Campos(0), Log_Field)
            Respuesta_Autorizacion_VisaMaster = LeeTarjeta.getStatus(Campos(1), Campos(2), Campos(3), Campos(4), Campos(5), Campos(6), Email_Administrador, Campos(7), Campos(8))
            SQLInsertLogPinpad(Encabezado, "RESPUESTA:" & Campos(0), Respuesta_Autorizacion_VisaMaster)
            Respuesta1 = Respuesta_Autorizacion_VisaMaster
            Mensaje_Pinpad = LeeTarjeta.getMensaje().ToString
            Mensaje_Pinpad2 = LeeTarjeta.getMensajeError().ToString
            Mensaje_Pinpad3 = LeeTarjeta.getMensajeWebSer().ToString
            Mensaje_Pinpad_All1 = "3|" & Mensaje_Pinpad2 & "|" & Mensaje_Pinpad3 & "|" & Respuesta_Autorizacion_VisaMaster
            ''MessageBox.Show(Mensaje_Pinpad_All1)
            'Else
            'Respuesta = Encabezado & Separador & Mensaje_Pinpad & Trailer
            'End If
            If Respuesta_Autorizacion_VisaMaster.Length > 10 Then
                Separa_Campos_Individules_Favorite("value=", "imprimir", "/>", imprimir, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "s_pago", "/>", s_pago, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "autorizacion", "/>", autorizacion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "mensaje", "/>", mensaje, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "razonSocial", "/>", razonSocial, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "comercio", "/>", comercio, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "direccion", "/>", direccion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "afiliacion", "/>", Afiliacion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "serie", "/>", Serie, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "fhTransaccion", "/>", fhTransaccion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "hrTransaccion", "/>", Hora_A, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "tarjeta", "/>", tarjeta, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "fhVencimiento", "/>", fhVencimiento, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "emisor", "/>", emisor, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "operacion", "/>", operacion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "monto", "/>", monto_A, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "tpPago", "/>", ingreso, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "referencia", "/>", Referencia, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "criptograma", "/>", criptograma, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "firma", "/>", firma, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "titular", "/>", Titular, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "cd_respuesta", "/>", cd_respuesta, Inicio_L)

                Separa_Campos_Individules_Favorite("value=", "montoPesos", "/>", montoPesos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "montoPuntos", "/>", montoPuntos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "montoTotal", "/>", montoTotal, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "leyenda2", "/>", leyenda2, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoAnteriorPuntos", "/>", saldoAnteriorPuntos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoAnteriorPesos", "/>", saldoAnteriorPesos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoRedimidoPuntos", "/>", saldoRedimidoPuntos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoRedimidoPesos", "/>", saldoRedimidoPesos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoActualPuntos", "/>", saldoActualPuntos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoActualPesos", "/>", saldoActualPesos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "vigenciaPuntos", "/>", vigenciaPuntos, Inicio_L)
                Separa_Campos_Individules("status", status_code)

                'autorizacion.Replace(""",".Replace")
                If autorizacion.Length = 0 Then
                    autorizacion = "000000"
                    mensaje = "ERROR_C|" + status_code
                    Pagare_Pinpad = mensaje & Separador
                    ''MessageBox.Show("NO APROBADA 03")
                Else
                    If autorizacion.Length > 0 Then
                        imprimir = "SI"
                    End If
                    Pagare_Pinpad = imprimir & Separador                      ''1
                    Pagare_Pinpad = Pagare_Pinpad & autorizacion & Separador  ''2
                    Pagare_Pinpad = Pagare_Pinpad & razonSocial & Separador   ''3
                    Pagare_Pinpad = Pagare_Pinpad & comercio & Separador      ''4
                    Pagare_Pinpad = Pagare_Pinpad & direccion & Separador     ''5
                    Pagare_Pinpad = Pagare_Pinpad & Afiliacion & Separador    ''6
                    Pagare_Pinpad = Pagare_Pinpad & Serie & Separador         ''7
                    Pagare_Pinpad = Pagare_Pinpad & fhTransaccion & Separador ''8
                    Pagare_Pinpad = Pagare_Pinpad & Hora_A & Separador        ''9
                    Pagare_Pinpad = Pagare_Pinpad & tarjeta & Separador       ''10
                    Pagare_Pinpad = Pagare_Pinpad & fhVencimiento & Separador ''11
                    Pagare_Pinpad = Pagare_Pinpad & emisor & Separador        ''12
                    Pagare_Pinpad = Pagare_Pinpad & operacion & Separador     ''13
                    Pagare_Pinpad = Pagare_Pinpad & monto_A & Separador       ''14
                    Pagare_Pinpad = Pagare_Pinpad & ingreso & Separador       ''15
                    Pagare_Pinpad = Pagare_Pinpad & Referencia & Separador    ''16
                    Pagare_Pinpad = Pagare_Pinpad & criptograma & Separador   ''17
                    Pagare_Pinpad = Pagare_Pinpad & Titular & Separador       ''18
                    Pagare_Pinpad = Pagare_Pinpad & firma & Separador         ''19
                    Pagare_Pinpad = Pagare_Pinpad & mensaje & Separador       ''20
                    Pagare_Pinpad = Pagare_Pinpad & montoPesos & Separador           ''21
                    Pagare_Pinpad = Pagare_Pinpad & montoPuntos & Separador          ''22
                    Pagare_Pinpad = Pagare_Pinpad & montoTotal & Separador           ''23
                    Pagare_Pinpad = Pagare_Pinpad & leyenda2 & Separador             ''24
                    Pagare_Pinpad = Pagare_Pinpad & saldoAnteriorPuntos & Separador  ''25
                    Pagare_Pinpad = Pagare_Pinpad & saldoAnteriorPesos & Separador   ''26
                    Pagare_Pinpad = Pagare_Pinpad & saldoRedimidoPuntos & Separador  ''27
                    Pagare_Pinpad = Pagare_Pinpad & saldoRedimidoPesos & Separador   ''28
                    Pagare_Pinpad = Pagare_Pinpad & saldoActualPuntos & Separador    ''29
                    Pagare_Pinpad = Pagare_Pinpad & saldoActualPesos & Separador     ''30
                    Pagare_Pinpad = Pagare_Pinpad & vigenciaPuntos & Separador       ''31
                    Pagare_Pinpad = Pagare_Pinpad & cd_respuesta & Separador         ''32
                    Pagare_Pinpad = Pagare_Pinpad & s_pago & Separador               ''33
                    ''MessageBox.Show("APROBADA")
                End If
                'LeeTarjeta.terminaTransaccion(autorizacion)

                'Pagare_Pinpad = LeeTarjeta.g
                'LeeTarjeta.terminaTransaccion(LeeTarjeta.getAutorizacion)
                Respuesta = Encabezado & Separador & Pagare_Pinpad & Trailer
            Else
                Mensaje_Pinpad = LeeTarjeta.getMensaje().ToString
                Respuesta = Encabezado & Separador & Mensaje_Pinpad_All1 & Trailer
                'LeeTarjeta.terminaTransaccion("000000")
                'LeeTarjeta.setMensajeError("NO APROBADA 02")
                'LeeTarjeta.terminaTransaccion("000000")
                Pagare_Pinpad = "NO APROBADA. 02"
                Respuesta = Encabezado & Separador & Pagare_Pinpad & Trailer
                ''MessageBox.Show("NO APROBADA 02")
            End If
            Borra_Totales_Tarjetas()
        Catch exp As Exception
            Respuesta = Encabezado & Separador & exp.ToString & Trailer
            SQLInsertLogPinpad(Encabezado, "RESPUESTA:" & Campos(0), Respuesta)
            ''MessageBox.Show(Respuesta)
        End Try
    End Sub
    Private Sub SQLInsertLogPinpad(ByVal Solicito_ As String, ByVal Proceso_ As String, ByVal Datos_ As String)
        Try
            Log_Add = False
            Permite_Conexcion_MCRSPOS = False
            Salir = False
            ODBC_ConnectStringMICROS("micros.db")
            If Permite_Conexcion_MCRSPOS = True Then
                InstrSql = "INSERT INTO custom.Log_Pinpad ( Proceso, Solicito, Datos ) Values ( " & _
                           "'" & Proceso_ & "', " & _
                           "'" & Solicito_ & "', " & _
                           "'" & Datos_ & "'   " & _
                           " ) "
                Dim Command As New OdbcCommand(InstrSql, ConexcionMCRSPOS)
                Command.CommandType = CommandType.Text
                ReadSql = Command.ExecuteScalar
                Log_Add = True
            Else
                Salir = True
            End If
        Catch Errores_SQLInsertLogPinpad As Exception
            Log_Add = False
            Registro_Log = "Errores_SQLInsertLogPinpad        :" + Errores_SQLInsertLogPinpad.ToString
            'AppendLog(Registro_Log)
        Finally
            ODBC_ConnectStringMicrosClose()
        End Try
    End Sub
    Sub Procesa_Devolucion_Pinpad()
        Try
            'If LeeTarjeta.getMensaje().Equals("LECTURA EXITOSA") Or Len(Track2) > 0 Then
            Log_Field = Campos(1) & Separador & Campos(2) & Separador & Campos(3) & Separador & Campos(4) & Separador & Campos(5) & Separador & Campos(6) & Separador & Campos(7) & Separador & Campos(8) & Separador & Campos(9) & Separador & Campos(10)
            SQLInsertLogPinpad(Encabezado, Campos(0), Log_Field)
            Respuesta_Autorizacion_VisaMaster = LeeTarjeta.devolucion(Campos(1), Campos(2), Campos(3), Campos(4), Campos(5), Campos(6), Campos(7), Campos(8), Campos(9), Campos(10))
            SQLInsertLogPinpad(Encabezado, "RESPUESTA:" & Campos(0), Respuesta_Autorizacion_VisaMaster)
            Respuesta1 = Respuesta_Autorizacion_VisaMaster
            Mensaje_Pinpad = LeeTarjeta.getMensaje().ToString
            Mensaje_Pinpad2 = LeeTarjeta.getMensajeError().ToString
            Mensaje_Pinpad3 = LeeTarjeta.getMensajeWebSer().ToString
            Mensaje_Pinpad_All1 = "3|" & Mensaje_Pinpad2 & "|" & Mensaje_Pinpad3 & "|" & Respuesta_Autorizacion_VisaMaster
            ''MessageBox.Show(Mensaje_Pinpad_All1)
            'Else
            'Respuesta = Encabezado & Separador & Mensaje_Pinpad & Trailer
            'End If
            If Respuesta_Autorizacion_VisaMaster.Length > 10 Then
                Separa_Campos_Individules_Favorite("value=", "imprimir", "/>", imprimir, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "autorizacion", "/>", autorizacion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "mensaje", "/>", mensaje, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "razonSocial", "/>", razonSocial, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "comercio", "/>", comercio, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "direccion", "/>", direccion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "afiliacion", "/>", Afiliacion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "serie", "/>", Serie, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "fhTransaccion", "/>", fhTransaccion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "hora", "/>", Hora_A, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "tarjeta", "/>", tarjeta, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "fhVencimiento", "/>", fhVencimiento, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "emisor", "/>", emisor, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "operacion", "/>", operacion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "monto", "/>", monto_A, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "ingreso", "/>", ingreso, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "referencia", "/>", Referencia, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "criptograma", "/>", criptograma, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "firma", "/>", firma, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "titular", "/>", Titular, Inicio_L)

                Separa_Campos_Individules_Favorite("value=", "montoPesos", "/>", montoPesos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "montoPuntos", "/>", montoPuntos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "montoTotal", "/>", montoTotal, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "leyenda2", "/>", leyenda2, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoAnteriorPuntos", "/>", saldoAnteriorPuntos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoAnteriorPesos", "/>", saldoAnteriorPesos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoRedimidoPuntos", "/>", saldoRedimidoPuntos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoRedimidoPesos", "/>", saldoRedimidoPesos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoActualPuntos", "/>", saldoActualPuntos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoActualPesos", "/>", saldoActualPesos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "vigenciaPuntos", "/>", vigenciaPuntos, Inicio_L)

                'autorizacion.Replace(""",".Replace")
                If autorizacion.Length = 0 Then
                    autorizacion = "000000"
                    Pagare_Pinpad = "ERROR_II" & Separador & mensaje & Separador
                    ''MessageBox.Show("NO APROBADA 03")
                Else
                    Pagare_Pinpad = imprimir & Separador                      ''1
                    Pagare_Pinpad = Pagare_Pinpad & autorizacion & Separador  ''2
                    Pagare_Pinpad = Pagare_Pinpad & razonSocial & Separador   ''3
                    Pagare_Pinpad = Pagare_Pinpad & comercio & Separador      ''4
                    Pagare_Pinpad = Pagare_Pinpad & direccion & Separador     ''5
                    Pagare_Pinpad = Pagare_Pinpad & Afiliacion & Separador    ''6
                    Pagare_Pinpad = Pagare_Pinpad & Serie & Separador         ''7
                    Pagare_Pinpad = Pagare_Pinpad & fhTransaccion & Separador ''8
                    Pagare_Pinpad = Pagare_Pinpad & Hora_A & Separador        ''9
                    Pagare_Pinpad = Pagare_Pinpad & tarjeta & Separador       ''10
                    Pagare_Pinpad = Pagare_Pinpad & fhVencimiento & Separador ''11
                    Pagare_Pinpad = Pagare_Pinpad & emisor & Separador        ''12
                    Pagare_Pinpad = Pagare_Pinpad & operacion & Separador     ''13
                    Pagare_Pinpad = Pagare_Pinpad & monto_A & Separador       ''14
                    Pagare_Pinpad = Pagare_Pinpad & ingreso & Separador       ''15
                    Pagare_Pinpad = Pagare_Pinpad & Referencia & Separador    ''16
                    Pagare_Pinpad = Pagare_Pinpad & criptograma & Separador   ''17
                    Pagare_Pinpad = Pagare_Pinpad & Titular & Separador       ''18
                    Pagare_Pinpad = Pagare_Pinpad & firma & Separador         ''19
                    Pagare_Pinpad = Pagare_Pinpad & mensaje & Separador       ''20
                    Pagare_Pinpad = Pagare_Pinpad & montoPesos & Separador           ''21
                    Pagare_Pinpad = Pagare_Pinpad & montoPuntos & Separador          ''22
                    Pagare_Pinpad = Pagare_Pinpad & montoTotal & Separador           ''23
                    Pagare_Pinpad = Pagare_Pinpad & leyenda2 & Separador             ''24
                    Pagare_Pinpad = Pagare_Pinpad & saldoAnteriorPuntos & Separador  ''25
                    Pagare_Pinpad = Pagare_Pinpad & saldoAnteriorPesos & Separador   ''26
                    Pagare_Pinpad = Pagare_Pinpad & saldoRedimidoPuntos & Separador  ''27
                    Pagare_Pinpad = Pagare_Pinpad & saldoRedimidoPesos & Separador   ''28
                    Pagare_Pinpad = Pagare_Pinpad & saldoActualPuntos & Separador    ''29
                    Pagare_Pinpad = Pagare_Pinpad & saldoActualPesos & Separador     ''30
                    Pagare_Pinpad = Pagare_Pinpad & vigenciaPuntos & Separador       ''31
                    ''MessageBox.Show("APROBADA")
                End If
                'LeeTarjeta.terminaTransaccion(autorizacion)

                'Pagare_Pinpad = LeeTarjeta.g
                'LeeTarjeta.terminaTransaccion(LeeTarjeta.getAutorizacion)
                Respuesta = Encabezado & Separador & Pagare_Pinpad & Trailer
            Else
                Mensaje_Pinpad = LeeTarjeta.getMensaje().ToString
                Respuesta = Encabezado & Separador & Mensaje_Pinpad_All1 & Trailer
                'LeeTarjeta.terminaTransaccion("000000")
                'LeeTarjeta.setMensajeError("NO APROBADA 02")
                'LeeTarjeta.terminaTransaccion("000000")
                Pagare_Pinpad = "NO APROBADA; 02"
                Respuesta = Encabezado & Separador & Pagare_Pinpad & Trailer
                ''MessageBox.Show("NO APROBADA 02")
            End If
            Borra_Totales_Tarjetas()
        Catch exp As Exception
            Respuesta = Encabezado & Separador & exp.ToString & Trailer
            SQLInsertLogPinpad(Encabezado, "RESPUESTA:" & Campos(0), Respuesta)
            ''MessageBox.Show(Respuesta)
        End Try
    End Sub

    Sub Procesa_Cancelacion_Amex_Pinpad()
        Try
            Log_Field = Campos(1) & Separador & Campos(2) & Separador & Campos(3) & Separador & Campos(4) & Separador & Campos(5) & Separador & Campos(6)
            SQLInsertLogPinpad(Encabezado, Campos(0), Log_Field)
            ''Mar2013 Respuesta_Autorizacion_VisaMaster = LeeTarjeta.cancelaAmex(Campos(1), Campos(2), Campos(3), Campos(4), Campos(5), Campos(6) )  ''Agregar campos Mar2013
            Respuesta_Autorizacion_VisaMaster = LeeTarjeta.cancelaAmex(Campos(1), Campos(2), Campos(3), Campos(4), Campos(5), Campos(6), Campos(7))  ''Agregados campos Mar2013
            SQLInsertLogPinpad(Encabezado, "RESPUESTA:" & Campos(0), Respuesta_Autorizacion_VisaMaster)
            Respuesta1 = Respuesta_Autorizacion_VisaMaster
            Mensaje_Pinpad = LeeTarjeta.getMensaje().ToString
            Mensaje_Pinpad2 = LeeTarjeta.getMensajeError().ToString
            Mensaje_Pinpad3 = LeeTarjeta.getMensajeWebSer().ToString
            Mensaje_Pinpad_All1 = "3|" & Mensaje_Pinpad2 & "|" & Mensaje_Pinpad3 & "|" & Respuesta_Autorizacion_VisaMaster
            ''MessageBox.Show(Mensaje_Pinpad_All1)
            'Else
            'Respuesta = Encabezado & Separador & Mensaje_Pinpad & Trailer
            'End If
            If Respuesta_Autorizacion_VisaMaster.Length > 10 Then
                Separa_Campos_Individules_Favorite("value=", "imprimir", "/>", imprimir, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "autorizacion", "/>", autorizacion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "mensaje", "/>", mensaje, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "razonSocial", "/>", razonSocial, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "comercio", "/>", comercio, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "direccion", "/>", direccion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "afiliacion", "/>", Afiliacion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "serie", "/>", Serie, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "fhTransaccion", "/>", fhTransaccion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "hora", "/>", Hora_A, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "tarjeta", "/>", tarjeta, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "fhVencimiento", "/>", fhVencimiento, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "emisor", "/>", emisor, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "operacion", "/>", operacion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "monto", "/>", monto_A, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "ingreso", "/>", ingreso, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "referencia", "/>", Referencia, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "criptograma", "/>", criptograma, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "firma", "/>", firma, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "titular", "/>", Titular, Inicio_L)

                Separa_Campos_Individules_Favorite("value=", "montoPesos", "/>", montoPesos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "montoPuntos", "/>", montoPuntos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "montoTotal", "/>", montoTotal, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "leyenda2", "/>", leyenda2, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoAnteriorPuntos", "/>", saldoAnteriorPuntos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoAnteriorPesos", "/>", saldoAnteriorPesos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoRedimidoPuntos", "/>", saldoRedimidoPuntos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoRedimidoPesos", "/>", saldoRedimidoPesos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoActualPuntos", "/>", saldoActualPuntos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoActualPesos", "/>", saldoActualPesos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "vigenciaPuntos", "/>", vigenciaPuntos, Inicio_L)

                'autorizacion.Replace(""",".Replace")
                If autorizacion.Length = 0 Then
                    autorizacion = "000000"
                    Pagare_Pinpad = "ERROR_II" & Separador & mensaje & Separador
                    ''MessageBox.Show("NO APROBADA 03")
                Else
                    Pagare_Pinpad = imprimir & Separador                      ''1
                    Pagare_Pinpad = Pagare_Pinpad & autorizacion & Separador  ''2
                    Pagare_Pinpad = Pagare_Pinpad & razonSocial & Separador   ''3
                    Pagare_Pinpad = Pagare_Pinpad & comercio & Separador      ''4
                    Pagare_Pinpad = Pagare_Pinpad & direccion & Separador     ''5
                    Pagare_Pinpad = Pagare_Pinpad & Afiliacion & Separador    ''6
                    Pagare_Pinpad = Pagare_Pinpad & Serie & Separador         ''7
                    Pagare_Pinpad = Pagare_Pinpad & fhTransaccion & Separador ''8
                    Pagare_Pinpad = Pagare_Pinpad & Hora_A & Separador        ''9
                    Pagare_Pinpad = Pagare_Pinpad & tarjeta & Separador       ''10
                    Pagare_Pinpad = Pagare_Pinpad & fhVencimiento & Separador ''11
                    Pagare_Pinpad = Pagare_Pinpad & emisor & Separador        ''12
                    Pagare_Pinpad = Pagare_Pinpad & operacion & Separador     ''13
                    Pagare_Pinpad = Pagare_Pinpad & monto_A & Separador       ''14
                    Pagare_Pinpad = Pagare_Pinpad & ingreso & Separador       ''15
                    Pagare_Pinpad = Pagare_Pinpad & Referencia & Separador    ''16
                    Pagare_Pinpad = Pagare_Pinpad & criptograma & Separador   ''17
                    Pagare_Pinpad = Pagare_Pinpad & Titular & Separador       ''18
                    Pagare_Pinpad = Pagare_Pinpad & firma & Separador         ''19
                    Pagare_Pinpad = Pagare_Pinpad & mensaje & Separador       ''20
                    Pagare_Pinpad = Pagare_Pinpad & montoPesos & Separador           ''21
                    Pagare_Pinpad = Pagare_Pinpad & montoPuntos & Separador          ''22
                    Pagare_Pinpad = Pagare_Pinpad & montoTotal & Separador           ''23
                    Pagare_Pinpad = Pagare_Pinpad & leyenda2 & Separador             ''24
                    Pagare_Pinpad = Pagare_Pinpad & saldoAnteriorPuntos & Separador  ''25
                    Pagare_Pinpad = Pagare_Pinpad & saldoAnteriorPesos & Separador   ''26
                    Pagare_Pinpad = Pagare_Pinpad & saldoRedimidoPuntos & Separador  ''27
                    Pagare_Pinpad = Pagare_Pinpad & saldoRedimidoPesos & Separador   ''28
                    Pagare_Pinpad = Pagare_Pinpad & saldoActualPuntos & Separador    ''29
                    Pagare_Pinpad = Pagare_Pinpad & saldoActualPesos & Separador     ''30
                    Pagare_Pinpad = Pagare_Pinpad & vigenciaPuntos & Separador       ''31
                    ''MessageBox.Show("APROBADA")
                End If
                'LeeTarjeta.terminaTransaccion(autorizacion)

                'Pagare_Pinpad = LeeTarjeta.g
                'LeeTarjeta.terminaTransaccion(LeeTarjeta.getAutorizacion)
                Respuesta = Encabezado & Separador & Pagare_Pinpad & Trailer
            Else
                Mensaje_Pinpad = LeeTarjeta.getMensaje().ToString
                Respuesta = Encabezado & Separador & Mensaje_Pinpad_All1 & Trailer
                'LeeTarjeta.terminaTransaccion("000000")
                'LeeTarjeta.setMensajeError("NO APROBADA 02")
                'LeeTarjeta.terminaTransaccion("000000")
                Pagare_Pinpad = "NO APROBADA: 02"
                Respuesta = Encabezado & Separador & Pagare_Pinpad & Trailer
                ''MessageBox.Show("NO APROBADA 02")
            End If
            Borra_Totales_Tarjetas()
        Catch exp As Exception
            Respuesta = Encabezado & Separador & exp.ToString & Trailer
            SQLInsertLogPinpad(Encabezado, "RESPUESTA:" & Campos(0), Respuesta)
            ''MessageBox.Show(Respuesta)
        End Try
    End Sub

    Sub Procesa_Cancelacion_Amex_Directa_Pinpad()
        Try
            Log_Field = Campos(1) & Separador & Campos(2) & Separador & Campos(3) & Separador & Campos(4) & Separador & Campos(5) & Separador & Campos(6)
            SQLInsertLogPinpad(Encabezado, Campos(0), Log_Field)
            ''Mar2013 Respuesta_Autorizacion_VisaMaster = LeeTarjeta.cancelaAmex(Campos(1), Campos(2), Campos(3), Campos(4), Campos(5), Campos(6)) ''Mar2013
            Respuesta_Autorizacion_VisaMaster = LeeTarjeta.cancelaAmex(Campos(1), Campos(2), Campos(3), Campos(4), Campos(5), Campos(6), Campos(7))
            SQLInsertLogPinpad(Encabezado, "RESPUESTA:" & Campos(0), Respuesta_Autorizacion_VisaMaster)
            Respuesta1 = Respuesta_Autorizacion_VisaMaster
            Mensaje_Pinpad = LeeTarjeta.getMensaje().ToString
            Mensaje_Pinpad2 = LeeTarjeta.getMensajeError().ToString
            Mensaje_Pinpad3 = LeeTarjeta.getMensajeWebSer().ToString
            Mensaje_Pinpad_All1 = "3|" & Mensaje_Pinpad2 & "|" & Mensaje_Pinpad3 & "|" & Respuesta_Autorizacion_VisaMaster
            ''MessageBox.Show(Mensaje_Pinpad_All1)
            'Else
            'Respuesta = Encabezado & Separador & Mensaje_Pinpad & Trailer
            'End If
            If Respuesta_Autorizacion_VisaMaster.Length > 10 Then
                Separa_Campos_Individules_Favorite("value=", "imprimir", "/>", imprimir, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "autorizacion", "/>", autorizacion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "mensaje", "/>", mensaje, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "razonSocial", "/>", razonSocial, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "comercio", "/>", comercio, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "direccion", "/>", direccion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "afiliacion", "/>", Afiliacion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "serie", "/>", Serie, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "fhTransaccion", "/>", fhTransaccion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "hora", "/>", Hora_A, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "tarjeta", "/>", tarjeta, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "fhVencimiento", "/>", fhVencimiento, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "emisor", "/>", emisor, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "operacion", "/>", operacion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "monto", "/>", monto_A, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "ingreso", "/>", ingreso, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "referencia", "/>", Referencia, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "criptograma", "/>", criptograma, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "firma", "/>", firma, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "titular", "/>", Titular, Inicio_L)

                Separa_Campos_Individules_Favorite("value=", "montoPesos", "/>", montoPesos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "montoPuntos", "/>", montoPuntos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "montoTotal", "/>", montoTotal, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "leyenda2", "/>", leyenda2, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoAnteriorPuntos", "/>", saldoAnteriorPuntos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoAnteriorPesos", "/>", saldoAnteriorPesos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoRedimidoPuntos", "/>", saldoRedimidoPuntos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoRedimidoPesos", "/>", saldoRedimidoPesos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoActualPuntos", "/>", saldoActualPuntos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoActualPesos", "/>", saldoActualPesos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "vigenciaPuntos", "/>", vigenciaPuntos, Inicio_L)

                'autorizacion.Replace(""",".Replace")
                If autorizacion.Length = 0 Then
                    autorizacion = "000000"
                    Pagare_Pinpad = "ERROR_II" & Separador & mensaje & Separador
                    ''MessageBox.Show("NO APROBADA 03")
                Else
                    Pagare_Pinpad = imprimir & Separador                      ''1
                    Pagare_Pinpad = Pagare_Pinpad & autorizacion & Separador  ''2
                    Pagare_Pinpad = Pagare_Pinpad & razonSocial & Separador   ''3
                    Pagare_Pinpad = Pagare_Pinpad & comercio & Separador      ''4
                    Pagare_Pinpad = Pagare_Pinpad & direccion & Separador     ''5
                    Pagare_Pinpad = Pagare_Pinpad & Afiliacion & Separador    ''6
                    Pagare_Pinpad = Pagare_Pinpad & Serie & Separador         ''7
                    Pagare_Pinpad = Pagare_Pinpad & fhTransaccion & Separador ''8
                    Pagare_Pinpad = Pagare_Pinpad & Hora_A & Separador        ''9
                    Pagare_Pinpad = Pagare_Pinpad & tarjeta & Separador       ''10
                    Pagare_Pinpad = Pagare_Pinpad & fhVencimiento & Separador ''11
                    Pagare_Pinpad = Pagare_Pinpad & emisor & Separador        ''12
                    Pagare_Pinpad = Pagare_Pinpad & operacion & Separador     ''13
                    Pagare_Pinpad = Pagare_Pinpad & monto_A & Separador       ''14
                    Pagare_Pinpad = Pagare_Pinpad & ingreso & Separador       ''15
                    Pagare_Pinpad = Pagare_Pinpad & Referencia & Separador    ''16
                    Pagare_Pinpad = Pagare_Pinpad & criptograma & Separador   ''17
                    Pagare_Pinpad = Pagare_Pinpad & Titular & Separador       ''18
                    Pagare_Pinpad = Pagare_Pinpad & firma & Separador         ''19
                    Pagare_Pinpad = Pagare_Pinpad & mensaje & Separador       ''20
                    Pagare_Pinpad = Pagare_Pinpad & montoPesos & Separador           ''21
                    Pagare_Pinpad = Pagare_Pinpad & montoPuntos & Separador          ''22
                    Pagare_Pinpad = Pagare_Pinpad & montoTotal & Separador           ''23
                    Pagare_Pinpad = Pagare_Pinpad & leyenda2 & Separador             ''24
                    Pagare_Pinpad = Pagare_Pinpad & saldoAnteriorPuntos & Separador  ''25
                    Pagare_Pinpad = Pagare_Pinpad & saldoAnteriorPesos & Separador   ''26
                    Pagare_Pinpad = Pagare_Pinpad & saldoRedimidoPuntos & Separador  ''27
                    Pagare_Pinpad = Pagare_Pinpad & saldoRedimidoPesos & Separador   ''28
                    Pagare_Pinpad = Pagare_Pinpad & saldoActualPuntos & Separador    ''29
                    Pagare_Pinpad = Pagare_Pinpad & saldoActualPesos & Separador     ''30
                    Pagare_Pinpad = Pagare_Pinpad & vigenciaPuntos & Separador       ''31
                    ''MessageBox.Show("APROBADA")
                End If
                'LeeTarjeta.terminaTransaccion(autorizacion)

                'Pagare_Pinpad = LeeTarjeta.g
                'LeeTarjeta.terminaTransaccion(LeeTarjeta.getAutorizacion)
                Respuesta = Encabezado & Separador & Pagare_Pinpad & Trailer
            Else
                Mensaje_Pinpad = LeeTarjeta.getMensaje().ToString
                Respuesta = Encabezado & Separador & Mensaje_Pinpad_All1 & Trailer
                'LeeTarjeta.terminaTransaccion("000000")
                'LeeTarjeta.setMensajeError("NO APROBADA 02")
                'LeeTarjeta.terminaTransaccion("000000")
                Pagare_Pinpad = "NO APROBADA- 02"
                Respuesta = Encabezado & Separador & Pagare_Pinpad & Trailer
                ''MessageBox.Show("NO APROBADA 02")
            End If
            Borra_Totales_Tarjetas()
        Catch exp As Exception
            Respuesta = Encabezado & Separador & exp.ToString & Trailer
            SQLInsertLogPinpad(Encabezado, "RESPUESTA:" & Campos(0), Respuesta)
            ''MessageBox.Show(Respuesta)
        End Try
    End Sub
    Sub Procesa_Autorizacion_Pinpad()
        Try
            LeeTarjeta.LeerDatosTarjeta(Campos(6))
            Mensaje_Pinpad = LeeTarjeta.getMensaje().ToString
            Mensaje_Pinpad2 = LeeTarjeta.getMensajeError().ToString
            Mensaje_Pinpad3 = LeeTarjeta.getMensajeWebSer().ToString
            Mensaje_Pinpad_All = "3|" & Mensaje_Pinpad & "|" & Mensaje_Pinpad2 & "|" & Mensaje_Pinpad3
            If Mensaje_Pinpad.Length > 0 Or Mensaje_Pinpad2.Length > 0 Then
                If LeeTarjeta.getMensaje().Equals("LECTURA EXITOSA") Then
                    LeeTarjeta.setMensajeError("LECTURA EXITOSA")
                    'LeeTarjeta.terminaTransaccion("      ")
                Else
                    LeeTarjeta.setMensajeError("NO APROBADA 01")
                    Respuesta = Encabezado & Separador & Mensaje_Pinpad_All & Trailer
                    LeeTarjeta.terminaTransaccion("000000", "", "")
                    Mensaje_Pinpad = "NO APROBADA 01"
                    ''MessageBox.Show("NO APROBADA 01")
                    Exit Sub
                End If
            ElseIf Mensaje_Pinpad = "TIMEOUT" Then
                Mensaje_Pinpad = "_timeout_pinpad"
                Respuesta = Encabezado & Separador & Mensaje_Pinpad & Trailer
                ''MessageBox.Show("TIMEOUT")
                Exit Sub
            End If
            No_Tarjeta = LeeTarjeta.getNumTarjeta
            'Tipo_Tarjeta =  LeeTarjeta.getN
            Titular = LeeTarjeta.getTrack1
            Anio = LeeTarjeta.getAnyoVencimiento
            Mes = LeeTarjeta.getMesVencimiento
            Fecha_Vencimiento = Mes & Anio
            Tags_EMV = LeeTarjeta.getTagEMV
            Tag_5F34 = LeeTarjeta.getTag5F34
            Tag_9F27 = LeeTarjeta.getTag9F27
            Tag_9F26 = LeeTarjeta.getTag9F26
            Track2 = LeeTarjeta.getTrack2
            Flag = LeeTarjeta.getSignatureFlag
            CVV2 = LeeTarjeta.encriptaRijndael(Campos(7))
            Digest2 = Campos(1) & Campos(2) & Campos(6) & Campos(7)
            Digest = LeeTarjeta.encriptaHmac(Digest2)
            No_Tarjeta_A = No_Tarjeta
            No_Tarjeta = LeeTarjeta.encriptaRijndael(No_Tarjeta)
            'Email_Administrador = "vhdelangel@adquira.com.mx"
            Email_Administrador = "ramon.padilla@alsea.com.mx"

            Fecha_Vencimiento = LeeTarjeta.encriptaRijndael(Fecha_Vencimiento)
            'CVV2 = LeeTarjeta.encriptaRijndael(CVV2)
            'Digest2 = "1234567" & "1234567" & "100.00" & "121"
            'Digest = LeeTarjeta.encriptaHmac(Digest2)
            'Variable = "WEUE` WH��^^h^^ZEUE` W"

            Mensaje_Pinpad = No_Tarjeta_A & Separador
            Respuesta = Encabezado & Separador & Mensaje_Pinpad & Trailer
        Catch exp As Exception
            Respuesta = Encabezado & Separador & exp.ToString & Trailer
            ''MessageBox.Show(Respuesta)
        End Try
    End Sub
    Sub Procesa_Autorizacion_Pinpad_II()
        Try
            'If LeeTarjeta.getMensaje().Equals("LECTURA EXITOSA") Or Len(Track2) > 0 Then

            CVV2 = LeeTarjeta.encriptaRijndael(Campos(7))
            Digest2 = Campos(1) & Campos(2) & Campos(6) & Campos(7)
            Digest = LeeTarjeta.encriptaHmac(Digest2)
            '10Jul2012 Digest = LeeTarjeta_Nurit293v30.encriptaHmac(Digest2)
            'Digest = LeeTarjeta_Nurit293v32.encriptaHmac(Digest2)
            Fecha_Vencimiento = Campos(14) & Campos(15)
            No_Tarjeta_A = Campos(11)
            No_Tarjeta = LeeTarjeta.encriptaRijndael(No_Tarjeta_A)
            'No_Tarjeta = LeeTarjeta_Nurit293v30.encriptaRijndael(No_Tarjeta_A)
            'No_Tarjeta = LeeTarjeta_Nurit293v32.encriptaRijndael(No_Tarjeta_A)

            Titular = Campos(21)
            Track2_A = Campos(22)
            ''18Oct2013 Track2 = LeeTarjeta.encriptaRijndael(Track2_A)
            Track2 = Track2_A
            'Email_Administrador = "vhdelangel@adquira.com.mx"
            Email_Administrador = "ramon.padilla@alsea.com.mx"
            'Email_Administrador = "ivan.merced@alsea.com.mx"

            Fecha_Vencimiento = LeeTarjeta.encriptaRijndael(Fecha_Vencimiento)

            Telefono_Cliente = ""
            Email_cliente = ""
            Tipo_Pago = "1"
            sFlag = Campos(16)

            ''Test Quitar
            ''Track2 = "75B71496ADF56B5DDA06F5255C29BAD8767127A6F69221EA"
            ''Test Quitar

            'Respuesta_Autorizacion_VisaMaster = LeeTarjeta.compra(Campos(1), Campos(2), Campos(3), Campos(4), Campos(5), Campos(6), Titular, No_Tarjeta, Fecha_Vencimiento, CVV2, Digest, Email_cliente, Telefono_Cliente, "10710", Campos(9), "1", Track2, Campos(8), "", Email_Administrador, "PAGO", Tag_5F34, Tags_EMV, "", Tag_9F26, Tag_9F27)
            campo_tarjeta = ""
            Log_Field = Campos(1) & Separador & Campos(2) & Separador & Campos(3) & Separador & Campos(4) & Separador & Campos(5) & Separador & Campos(6) & Separador & Campos(10) & Separador & CVV2 & Separador & Digest & Separador & Email_cliente & Separador & Telefono_Cliente & Separador & Campos(26) & Separador & Campos(9) & Separador & Tipo_Pago & Separador & Track2 & Separador & Campos(8) & Separador & "" & Separador & Email_Administrador & Separador & Campos(27) & Separador & Campos(17) & Separador & Campos(20) & Separador & sFlag & Separador & Campos(18) & Separador & Campos(19) & Separador & Campos(30) & Separador & Campos(29) & Separador & Campos(31) & Separador & Campos(32)
            SQLInsertLogPinpad(Encabezado, Campos(0), Log_Field)
            ''18Oct2013 Respuesta_Autorizacion_VisaMaster = LeeTarjeta.compra(Campos(1), Campos(2), Campos(3), Campos(4), Campos(5), Campos(6), Campos(10), No_Tarjeta, Fecha_Vencimiento, CVV2, Digest, Email_cliente, Telefono_Cliente, Campos(26), Campos(9), Tipo_Pago, Track2, Campos(8), "", Email_Administrador, Campos(27), Campos(17), Campos(20), sFlag, Campos(18), Campos(19))
            Registro_Log = "Sizes :"
            AppendLog(Registro_Log)
            size_17 = Len(Campos(17))
            size_18 = Len(Campos(18))
            size_19 = Len(Campos(19))
            size_20 = Len(Campos(20))
            size_31 = Len(Campos(31))
            size_sFlag = Len(sFlag)
            'Registro_Log = "cambiaCaracter :"
            'AppendLog(Registro_Log)
            'cambiaCaracter17()
            'cambiaCaracter18()
            'cambiaCaracter19()
            'cambiaCaracter20()
            'cambiaCaracter31()
            'cambiaCaracterSflag()
            Registro_Log = "compra :"
            AppendLog(Registro_Log)
            '27Nov2013 campos_Consulta(Numero_WSID) = "CONSULTA_VOUCHER" & Slash & Campos(26) & Slash & Campos(3) & Slash & Campos(5) & Slash & Campos(4) & Slash & Campos(1) & Slash & Campos(2) & Slash & "STATUS" & Slash Campos(27) & Slash 
            Respuesta_Autorizacion_VisaMaster = LeeTarjeta.compra(Campos(1), Campos(2), Campos(3), Campos(4), Campos(5), Campos(6), Campos(10), CVV2, Digest, Email_cliente, Telefono_Cliente, Campos(26), Campos(9), Tipo_Pago, Track2, Campos(8), "", Email_Administrador, Campos(27), Campos(17), Campos(20), sFlag, Campos(18), Campos(19), Campos(30), Campos(29), Campos(31), Campos(32))
            ''Respuesta_Autorizacion_VisaMaster = LeeTarjeta.compra(Campos(1), Campos(2), Campos(3), Campos(4), Campos(5), Campos(6), Campos(10), No_Tarjeta, Fecha_Vencimiento, CVV2, Digest, Email_cliente, Telefono_Cliente, Campos(26), Campos(9), Tipo_Pago, Track2, Campos(8), "", Email_Administrador, Campos(27), Campos(17), Campos(20), "", Campos(18), Campos(19))
            SQLInsertLogPinpad(Encabezado, "RESPUESTA:" & Campos(0), Respuesta_Autorizacion_VisaMaster)
            Respuesta1 = Respuesta_Autorizacion_VisaMaster
            Mensaje_Pinpad = LeeTarjeta.getMensaje().ToString
            Mensaje_Pinpad2 = LeeTarjeta.getMensajeError().ToString
            Mensaje_Pinpad3 = LeeTarjeta.getMensajeWebSer().ToString
            Mensaje_Pinpad_All1 = "3|" & Mensaje_Pinpad2 & "|" & Mensaje_Pinpad3 & "|" & Respuesta_Autorizacion_VisaMaster
            ''MessageBox.Show(Mensaje_Pinpad_All1)
            'Else
            'Respuesta = Encabezado & Separador & Mensaje_Pinpad & Trailer
            'End If
            If Respuesta_Autorizacion_VisaMaster.Length > 10 Then
                Separa_Campos_Individules_Favorite("value=", "imprimir", "/>", imprimir, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "autorizacion", "/>", autorizacion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "mensaje", "/>", mensaje, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "razonSocial", "/>", razonSocial, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "comercio", "/>", comercio, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "direccion", "/>", direccion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "afiliacion", "/>", Afiliacion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "serie", "/>", Serie, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "fhTransaccion", "/>", fhTransaccion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "hora", "/>", Hora_A, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "tarjeta", "/>", tarjeta, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "fhVencimiento", "/>", fhVencimiento, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "emisor", "/>", emisor, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "operacion", "/>", operacion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "monto", "/>", monto_A, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "ingreso", "/>", ingreso, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "referencia", "/>", Referencia, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "criptograma", "/>", criptograma, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "firma", "/>", firma, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "titular", "/>", Titular, Inicio_L)

                Separa_Campos_Individules_Favorite("value=", "montoPesos", "/>", montoPesos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "montoPuntos", "/>", montoPuntos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "montoTotal", "/>", montoTotal, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "leyenda2", "/>", leyenda2, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoAnteriorPuntos", "/>", saldoAnteriorPuntos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoAnteriorPesos", "/>", saldoAnteriorPesos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoRedimidoPuntos", "/>", saldoRedimidoPuntos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoRedimidoPesos", "/>", saldoRedimidoPesos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoActualPuntos", "/>", saldoActualPuntos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoActualPesos", "/>", saldoActualPesos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "vigenciaPuntos", "/>", vigenciaPuntos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "iad", "/>", iad, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "applabel", "/>", applabel, Inicio_L)

                'autorizacion.Replace(""",".Replace")
                If autorizacion.Length = 0 Then
                    autorizacion = "000000"
                    Pagare_Pinpad = "ERROR_II" & Separador & mensaje & Separador
                    ''MessageBox.Show("NO APROBADA 03")
                    Pagare_Pinpad = imprimir & Separador                      ''1
                    Pagare_Pinpad = Pagare_Pinpad & autorizacion & Separador  ''2
                    Pagare_Pinpad = Pagare_Pinpad & razonSocial & Separador   ''3
                    Pagare_Pinpad = Pagare_Pinpad & comercio & Separador      ''4
                    Pagare_Pinpad = Pagare_Pinpad & direccion & Separador     ''5
                    Pagare_Pinpad = Pagare_Pinpad & Afiliacion & Separador    ''6
                    Pagare_Pinpad = Pagare_Pinpad & Serie & Separador         ''7
                    Pagare_Pinpad = Pagare_Pinpad & fhTransaccion & Separador ''8
                    Pagare_Pinpad = Pagare_Pinpad & Hora_A & Separador        ''9
                    Pagare_Pinpad = Pagare_Pinpad & tarjeta & Separador       ''10
                    Pagare_Pinpad = Pagare_Pinpad & fhVencimiento & Separador ''11
                    Pagare_Pinpad = Pagare_Pinpad & emisor & Separador        ''12
                    Pagare_Pinpad = Pagare_Pinpad & operacion & Separador     ''13
                    Pagare_Pinpad = Pagare_Pinpad & monto_A & Separador       ''14
                    Pagare_Pinpad = Pagare_Pinpad & ingreso & Separador       ''15
                    Pagare_Pinpad = Pagare_Pinpad & Referencia & Separador    ''16
                    Pagare_Pinpad = Pagare_Pinpad & criptograma & Separador   ''17
                    Pagare_Pinpad = Pagare_Pinpad & Titular & Separador       ''18
                    Pagare_Pinpad = Pagare_Pinpad & firma & Separador         ''19
                    Pagare_Pinpad = Pagare_Pinpad & mensaje & Separador       ''20
                    Pagare_Pinpad = Pagare_Pinpad & montoPesos & Separador           ''21
                    Pagare_Pinpad = Pagare_Pinpad & montoPuntos & Separador          ''22
                    Pagare_Pinpad = Pagare_Pinpad & montoTotal & Separador           ''23
                    Pagare_Pinpad = Pagare_Pinpad & leyenda2 & Separador             ''24
                    Pagare_Pinpad = Pagare_Pinpad & saldoAnteriorPuntos & Separador  ''25
                    Pagare_Pinpad = Pagare_Pinpad & saldoAnteriorPesos & Separador   ''26
                    Pagare_Pinpad = Pagare_Pinpad & saldoRedimidoPuntos & Separador  ''27
                    Pagare_Pinpad = Pagare_Pinpad & saldoRedimidoPesos & Separador   ''28
                    Pagare_Pinpad = Pagare_Pinpad & saldoActualPuntos & Separador    ''29
                    Pagare_Pinpad = Pagare_Pinpad & saldoActualPesos & Separador     ''30
                    Pagare_Pinpad = Pagare_Pinpad & vigenciaPuntos & Separador       ''31
                    autorizacion = "000000"
                    Pagare_Pinpad = Pagare_Pinpad & Separador & "ERROR_II" & Separador & mensaje & Separador & iad & Separador & applabel & Separador
                Else
                    Pagare_Pinpad = imprimir & Separador                      ''1
                    Pagare_Pinpad = Pagare_Pinpad & autorizacion & Separador  ''2
                    Pagare_Pinpad = Pagare_Pinpad & razonSocial & Separador   ''3
                    Pagare_Pinpad = Pagare_Pinpad & comercio & Separador      ''4
                    Pagare_Pinpad = Pagare_Pinpad & direccion & Separador     ''5
                    Pagare_Pinpad = Pagare_Pinpad & Afiliacion & Separador    ''6
                    Pagare_Pinpad = Pagare_Pinpad & Serie & Separador         ''7
                    Pagare_Pinpad = Pagare_Pinpad & fhTransaccion & Separador ''8
                    Pagare_Pinpad = Pagare_Pinpad & Hora_A & Separador        ''9
                    Pagare_Pinpad = Pagare_Pinpad & tarjeta & Separador       ''10
                    Pagare_Pinpad = Pagare_Pinpad & fhVencimiento & Separador ''11
                    Pagare_Pinpad = Pagare_Pinpad & emisor & Separador        ''12
                    Pagare_Pinpad = Pagare_Pinpad & operacion & Separador     ''13
                    Pagare_Pinpad = Pagare_Pinpad & monto_A & Separador       ''14
                    Pagare_Pinpad = Pagare_Pinpad & ingreso & Separador       ''15
                    Pagare_Pinpad = Pagare_Pinpad & Referencia & Separador    ''16
                    Pagare_Pinpad = Pagare_Pinpad & criptograma & Separador   ''17
                    Pagare_Pinpad = Pagare_Pinpad & Titular & Separador       ''18
                    Pagare_Pinpad = Pagare_Pinpad & firma & Separador         ''19
                    Pagare_Pinpad = Pagare_Pinpad & mensaje & Separador       ''20
                    Pagare_Pinpad = Pagare_Pinpad & montoPesos & Separador           ''21
                    Pagare_Pinpad = Pagare_Pinpad & montoPuntos & Separador          ''22
                    Pagare_Pinpad = Pagare_Pinpad & montoTotal & Separador           ''23
                    Pagare_Pinpad = Pagare_Pinpad & leyenda2 & Separador             ''24
                    Pagare_Pinpad = Pagare_Pinpad & saldoAnteriorPuntos & Separador  ''25
                    Pagare_Pinpad = Pagare_Pinpad & saldoAnteriorPesos & Separador   ''26
                    Pagare_Pinpad = Pagare_Pinpad & saldoRedimidoPuntos & Separador  ''27
                    Pagare_Pinpad = Pagare_Pinpad & saldoRedimidoPesos & Separador   ''28
                    Pagare_Pinpad = Pagare_Pinpad & saldoActualPuntos & Separador    ''29
                    Pagare_Pinpad = Pagare_Pinpad & saldoActualPesos & Separador     ''30
                    Pagare_Pinpad = Pagare_Pinpad & vigenciaPuntos & Separador       ''31
                    Pagare_Pinpad = Pagare_Pinpad & iad & Separador                  ''32
                    Pagare_Pinpad = Pagare_Pinpad & applabel & Separador             ''33
                    ''MessageBox.Show("APROBADA")
                End If
                'LeeTarjeta.terminaTransaccion(autorizacion)

                'Pagare_Pinpad = LeeTarjeta.g
                'LeeTarjeta.terminaTransaccion(LeeTarjeta.getAutorizacion)
                Respuesta = Encabezado & Separador & Pagare_Pinpad & Trailer
            Else
                Mensaje_Pinpad = LeeTarjeta.getMensaje().ToString
                Respuesta = Encabezado & Separador & Mensaje_Pinpad_All1 & Trailer
                'LeeTarjeta.terminaTransaccion("000000")
                'LeeTarjeta.setMensajeError("NO APROBADA 02")
                'LeeTarjeta.terminaTransaccion("000000")
                Pagare_Pinpad = "NO APROBADA� 02"
                Respuesta = Encabezado & Separador & Pagare_Pinpad & Trailer
                ''MessageBox.Show("NO APROBADA 02")
            End If
            Borra_Totales_Tarjetas()
        Catch exp As Exception
            Respuesta = Encabezado & Separador & exp.ToString & Trailer
            ''MessageBox.Show(Respuesta)
        End Try
    End Sub
    Sub Procesa_Cancelacion_Pinpad_Final_II()
        Try
            CVV2 = LeeTarjeta.encriptaRijndael(Campos(7))
            Digest2 = Campos(1) & Campos(2) & Campos(6) & Campos(7)
            Digest = LeeTarjeta.encriptaHmac(Digest2)
            '06ago2012 Digest = LeeTarjeta_Nurit293v30.encriptaHmac(Digest2)
            'Digest = LeeTarjeta_Nurit293v32.encriptaHmac(Digest2)
            Fecha_Vencimiento = Campos(14) & Campos(15)
            No_Tarjeta_A = Campos(11)
            No_Tarjeta = LeeTarjeta.encriptaRijndael(No_Tarjeta_A)
            'No_Tarjeta = LeeTarjeta_Nurit293v30.encriptaRijndael(No_Tarjeta_A)
            'No_Tarjeta = LeeTarjeta_Nurit293v32.encriptaRijndael(No_Tarjeta_A)

            Titular = Campos(21)
            'Email_Administrador = "vhdelangel@adquira.com.mx"
            Email_Administrador = "ramon.padilla@alsea.com.mx"
            'Email_Administrador = "ivan.merced@alsea.com.mx"

            Fecha_Vencimiento = LeeTarjeta.encriptaRijndael(Fecha_Vencimiento)

            Telefono_Cliente = ""
            Email_cliente = ""

            ''Test Quitar
            ''Campos(22) = "75B71496ADF56B5DDA06F5255C29BAD8767127A6F69221EA"
            ''Test Quitar

            'Respuesta_Autorizacion_VisaMaster = LeeTarjeta.compra(Campos(1), Campos(2), Campos(3), Campos(4), Campos(5), Campos(6), Titular, No_Tarjeta, Fecha_Vencimiento, CVV2, Digest, Email_cliente, Telefono_Cliente, "10710", Campos(9), "1", Track2, Campos(8), "", Email_Administrador, "PAGO", Tag_5F34, Tags_EMV, "", Tag_9F26, Tag_9F27)
            Log_Field = Campos(1) & Separador & Campos(2) & Separador & Campos(3) & Separador & Campos(4) & Separador & Campos(5) & Separador & Campos(6) & Separador & Titular & Separador & CVV2 & Separador & Digest & Separador & Campos(26) & Separador & Campos(9) & Separador & "1" & Separador & Campos(22) & Separador & Campos(8) & Separador & "" & Separador & Email_Administrador & Separador & Campos(27) & Separador & Campos(17) & Separador & Campos(20) & Separador & "" & Separador & Campos(18) & Separador & Campos(19) & Separador & Campos(28) & Separador & Campos(29)
            SQLInsertLogPinpad(Encabezado, Campos(0), Log_Field)
            ''18Oct2013 Respuesta_Autorizacion_VisaMaster = LeeTarjeta.cancelacion(Campos(1), Campos(2), Campos(3), Campos(4), Campos(5), Campos(6), Titular, No_Tarjeta, Fecha_Vencimiento, CVV2, Digest, Campos(26), Campos(9), "1", Campos(22), Campos(8), "", Email_Administrador, Campos(27), Campos(17), Campos(20), "", Campos(18), Campos(19), Campos(28))
            Respuesta_Autorizacion_VisaMaster = LeeTarjeta.cancelacion(Campos(1), Campos(2), Campos(3), Campos(4), Campos(5), Campos(6), Titular, CVV2, Digest, Campos(26), Campos(9), "1", Campos(22), Campos(8), "", Email_Administrador, Campos(27), Campos(17), Campos(20), "", Campos(18), Campos(19), Campos(28), Campos(29))
            SQLInsertLogPinpad(Encabezado, "RESPUESTA:" & Campos(0), Respuesta_Autorizacion_VisaMaster)
            'Respuesta_Autorizacion_VisaMaster = LeeTarjeta.cancelacion(Campos(1), Campos(2), Campos(3), Campos(4), Campos(5), Campos(6), Titular, No_Tarjeta, Fecha_Vencimiento, CVV2, Digest, "10710", Campos(10), "1", Track2, Campos(8), "", Email_Administrador, "CANCELACION", Tag_5F34, Tags_EMV, "", Tag_9F26, Tag_9F27, Campos(9))
            Respuesta1 = Respuesta_Autorizacion_VisaMaster
            Mensaje_Pinpad = LeeTarjeta.getMensaje().ToString
            Mensaje_Pinpad2 = LeeTarjeta.getMensajeError().ToString
            Mensaje_Pinpad3 = LeeTarjeta.getMensajeWebSer().ToString
            Mensaje_Pinpad_All1 = "3|" & Mensaje_Pinpad2 & "|" & Mensaje_Pinpad3 & "|" & Respuesta_Autorizacion_VisaMaster
            ''MessageBox.Show(Mensaje_Pinpad_All1)
            'Else
            'Respuesta = Encabezado & Separador & Mensaje_Pinpad & Trailer
            'End If
            If Respuesta_Autorizacion_VisaMaster.Length > 10 Then
                Separa_Campos_Individules_Favorite("value=", "imprimir", "/>", imprimir, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "autorizacion", "/>", autorizacion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "mensaje", "/>", mensaje, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "razonSocial", "/>", razonSocial, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "comercio", "/>", comercio, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "direccion", "/>", direccion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "afiliacion", "/>", Afiliacion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "serie", "/>", Serie, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "fhTransaccion", "/>", fhTransaccion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "hora", "/>", Hora_A, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "tarjeta", "/>", tarjeta, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "fhVencimiento", "/>", fhVencimiento, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "emisor", "/>", emisor, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "operacion", "/>", operacion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "monto", "/>", monto_A, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "ingreso", "/>", ingreso, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "referencia", "/>", Referencia, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "criptograma", "/>", criptograma, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "firma", "/>", firma, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "titular", "/>", Titular, Inicio_L)

                Separa_Campos_Individules_Favorite("value=", "montoPesos", "/>", montoPesos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "montoPuntos", "/>", montoPuntos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "montoTotal", "/>", montoTotal, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "leyenda2", "/>", leyenda2, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoAnteriorPuntos", "/>", saldoAnteriorPuntos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoAnteriorPesos", "/>", saldoAnteriorPesos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoRedimidoPuntos", "/>", saldoRedimidoPuntos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoRedimidoPesos", "/>", saldoRedimidoPesos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoActualPuntos", "/>", saldoActualPuntos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoActualPesos", "/>", saldoActualPesos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "vigenciaPuntos", "/>", vigenciaPuntos, Inicio_L)

                'autorizacion.Replace(""",".Replace")
                If autorizacion.Length = 0 Then
                    autorizacion = "000000"
                    Pagare_Pinpad = "ERROR_VII" & Separador & mensaje & Separador
                    ''MessageBox.Show("NO APROBADA 03")
                Else
                    Pagare_Pinpad = imprimir & Separador                      ''1
                    Pagare_Pinpad = Pagare_Pinpad & autorizacion & Separador  ''2
                    Pagare_Pinpad = Pagare_Pinpad & razonSocial & Separador   ''3
                    Pagare_Pinpad = Pagare_Pinpad & comercio & Separador      ''4
                    Pagare_Pinpad = Pagare_Pinpad & direccion & Separador     ''5
                    Pagare_Pinpad = Pagare_Pinpad & Afiliacion & Separador    ''6
                    Pagare_Pinpad = Pagare_Pinpad & Serie & Separador         ''7
                    Pagare_Pinpad = Pagare_Pinpad & fhTransaccion & Separador ''8
                    Pagare_Pinpad = Pagare_Pinpad & Hora_A & Separador        ''9
                    Pagare_Pinpad = Pagare_Pinpad & tarjeta & Separador       ''10
                    Pagare_Pinpad = Pagare_Pinpad & fhVencimiento & Separador ''11
                    Pagare_Pinpad = Pagare_Pinpad & emisor & Separador        ''12
                    Pagare_Pinpad = Pagare_Pinpad & operacion & Separador     ''13
                    Pagare_Pinpad = Pagare_Pinpad & monto_A & Separador       ''14
                    Pagare_Pinpad = Pagare_Pinpad & ingreso & Separador       ''15
                    Pagare_Pinpad = Pagare_Pinpad & Referencia & Separador    ''16
                    Pagare_Pinpad = Pagare_Pinpad & criptograma & Separador   ''17
                    Pagare_Pinpad = Pagare_Pinpad & Titular & Separador       ''18
                    Pagare_Pinpad = Pagare_Pinpad & firma & Separador         ''19
                    Pagare_Pinpad = Pagare_Pinpad & mensaje & Separador       ''20

                    Pagare_Pinpad = Pagare_Pinpad & montoPesos & Separador           ''21
                    Pagare_Pinpad = Pagare_Pinpad & montoPuntos & Separador          ''22
                    Pagare_Pinpad = Pagare_Pinpad & montoTotal & Separador           ''23
                    Pagare_Pinpad = Pagare_Pinpad & leyenda2 & Separador             ''24
                    Pagare_Pinpad = Pagare_Pinpad & saldoAnteriorPuntos & Separador  ''25
                    Pagare_Pinpad = Pagare_Pinpad & saldoAnteriorPesos & Separador   ''26
                    Pagare_Pinpad = Pagare_Pinpad & saldoRedimidoPuntos & Separador  ''27
                    Pagare_Pinpad = Pagare_Pinpad & saldoRedimidoPesos & Separador   ''28
                    Pagare_Pinpad = Pagare_Pinpad & saldoActualPuntos & Separador    ''29
                    Pagare_Pinpad = Pagare_Pinpad & saldoActualPesos & Separador     ''30
                    Pagare_Pinpad = Pagare_Pinpad & vigenciaPuntos & Separador       ''31
                    ''MessageBox.Show("APROBADA")
                End If
                'LeeTarjeta.terminaTransaccion(autorizacion)

                'Pagare_Pinpad = LeeTarjeta.g
                'LeeTarjeta.terminaTransaccion(LeeTarjeta.getAutorizacion)
                Respuesta = Encabezado & Separador & Pagare_Pinpad & Trailer
            Else
                Mensaje_Pinpad = LeeTarjeta.getMensaje().ToString
                Respuesta = Encabezado & Separador & Mensaje_Pinpad_All1 & Trailer
                'LeeTarjeta.terminaTransaccion("000000")
                'LeeTarjeta.setMensajeError("NO APROBADA 02")
                'LeeTarjeta.terminaTransaccion("000000")
                Pagare_Pinpad = "NO APROBADA! 02"
                Respuesta = Encabezado & Separador & Pagare_Pinpad & Trailer
                ''MessageBox.Show("NO APROBADA 02")
            End If
            Borra_Totales_Tarjetas()
        Catch exp As Exception
            Respuesta = Encabezado & Separador & exp.ToString & Trailer
            ''MessageBox.Show(Respuesta)
        End Try
    End Sub

    Sub Procesa_Puntos_II()
        Try
            'If LeeTarjeta.getMensaje().Equals("LECTURA EXITOSA") Or Len(Track2) > 0 Then

            CVV2 = LeeTarjeta.encriptaRijndael(Campos(7))
            Digest2 = Campos(1) & Campos(2) & Campos(6) & Campos(7)
            Digest = LeeTarjeta.encriptaHmac(Digest2)
            '06ago2012 Digest = LeeTarjeta_Nurit293v30.encriptaHmac(Digest2)
            'Digest = LeeTarjeta_Nurit293v32.encriptaHmac(Digest2)
            Fecha_Vencimiento = Campos(14) & Campos(15)
            No_Tarjeta_A = Campos(11)
            No_Tarjeta = LeeTarjeta.encriptaRijndael(No_Tarjeta_A)
            Track2_A = Campos(22)
            '04Nov2013 Track2 = LeeTarjeta.encriptaRijndael(Track2_A)
            Track2 = Campos(22)
            'No_Tarjeta = LeeTarjeta_Nurit293v30.encriptaRijndael(No_Tarjeta_A)
            'No_Tarjeta = LeeTarjeta_Nurit293v32.encriptaRijndael(No_Tarjeta_A)

            Titular = Campos(21)
            'Email_Administrador = "vhdelangel@adquira.com.mx"
            Email_Administrador = "ramon.padilla@alsea.com.mx"
            'Email_Administrador = "ivan.merced@alsea.com.mx"

            Fecha_Vencimiento = LeeTarjeta.encriptaRijndael(Fecha_Vencimiento)

            Telefono_Cliente = ""
            Email_cliente = ""

            ''Test Quitar
            ''Track2 = "75B71496ADF56B5DDA06F5255C29BAD8767127A6F69221EA"
            ''Test Quitar

            'Respuesta_Autorizacion_VisaMaster = LeeTarjeta.compra(Campos(1), Campos(2), Campos(3), Campos(4), Campos(5), Campos(6), Titular, No_Tarjeta, Fecha_Vencimiento, CVV2, Digest, Email_cliente, Telefono_Cliente, "10710", Campos(9), "1", Track2, Campos(8), "", Email_Administrador, "PAGO", Tag_5F34, Tags_EMV, "", Tag_9F26, Tag_9F27)
            'Respuesta_Autorizacion_VisaMaster = LeeTarjeta.compra(Campos(1), Campos(2), Campos(3), Campos(4), Campos(5), Campos(6), Campos(10), No_Tarjeta, Fecha_Vencimiento, CVV2, Digest, Email_cliente, Telefono_Cliente, Campos(26), Campos(9), "1", Campos(22), Campos(8), "", Email_Administrador, Campos(27), Campos(17), Campos(20), "", Campos(18), Campos(19))
            Log_Field = Campos(26) & Separador & Campos(3) & Separador & Campos(5) & Separador & Campos(4) & Separador & Campos(1) & Separador & Campos(2) & Separador & Campos(6) & Separador & No_Tarjeta & Separador & Fecha_Vencimiento & Separador & CVV2 & Separador & Digest & Separador & Campos(9) & Separador & "1" & Separador & Campos(27) & Separador & Email_Administrador & Separador & Campos(21) & Separador & Track2 & Separador & Campos(17) & Separador & Campos(20) & Separador & Campos(16) & Separador & Campos(18) & Separador & Campos(19) & Separador & Campos(29) & Separador & Campos(30)
            SQLInsertLogPinpad(Encabezado, Campos(0), Log_Field)
            ''18Oct2013 Respuesta_Autorizacion_VisaMaster = LeeTarjeta.consulta(Campos(26), Campos(3), Campos(5), Campos(4), Campos(1), Campos(2), Campos(6), No_Tarjeta, Fecha_Vencimiento, CVV2, Digest, Campos(9), "1", Campos(27), Email_Administrador, Campos(21), Track2, Campos(17), Campos(20), Campos(16), Campos(18), Campos(19))
            Respuesta_Autorizacion_VisaMaster = LeeTarjeta.consulta(Campos(26), Campos(3), Campos(5), Campos(4), Campos(1), Campos(2), Campos(6), CVV2, Digest, Campos(9), "1", Campos(27), Email_Administrador, Campos(21), Track2, Campos(17), Campos(20), Campos(16), Campos(18), Campos(19), Campos(29), Campos(30))
            SQLInsertLogPinpad(Encabezado, "RESPUESTA:" & Campos(0), Respuesta_Autorizacion_VisaMaster)
            Respuesta1 = Respuesta_Autorizacion_VisaMaster
            Mensaje_Pinpad = LeeTarjeta.getMensaje().ToString
            Mensaje_Pinpad2 = LeeTarjeta.getMensajeError().ToString
            Mensaje_Pinpad3 = LeeTarjeta.getMensajeWebSer().ToString
            Mensaje_Pinpad_All1 = "3|" & Mensaje_Pinpad2 & "|" & Mensaje_Pinpad3 & "|" & Respuesta_Autorizacion_VisaMaster
            ''MessageBox.Show(Mensaje_Pinpad_All1)
            'Else
            'Respuesta = Encabezado & Separador & Mensaje_Pinpad & Trailer
            'End If
            If Respuesta_Autorizacion_VisaMaster.Length > 10 Then
                Separa_Campos_Individules_Favorite("value=", "imprimir", "/>", imprimir, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "autorizacion", "/>", autorizacion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "mensaje", "/>", mensaje, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "razonSocial", "/>", razonSocial, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "comercio", "/>", comercio, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "direccion", "/>", direccion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "afiliacion", "/>", Afiliacion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "serie", "/>", Serie, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "fhTransaccion", "/>", fhTransaccion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "hora", "/>", Hora_A, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "tarjeta", "/>", tarjeta, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "fhVencimiento", "/>", fhVencimiento, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "emisor", "/>", emisor, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "operacion", "/>", operacion, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "monto", "/>", monto_A, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "ingreso", "/>", ingreso, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "referencia", "/>", Referencia, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "criptograma", "/>", criptograma, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "firma", "/>", firma, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "titular", "/>", Titular, Inicio_L)

                Separa_Campos_Individules_Favorite("value=", "leyenda1", "/>", leyenda1, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "leyenda2", "/>", leyenda2, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "poolId", "/>", poolId, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoActualPuntos", "/>", saldoActualPuntos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "saldoActualPesos", "/>", saldoActualPesos, Inicio_L)
                Separa_Campos_Individules_Favorite_Sin_Borrar("value=", "monto", "/>", Monto_Puntos, Inicio_L)
                Separa_Campos_Individules_Favorite("value=", "vigenciaPuntos", "/>", vigenciaPuntos, Inicio_L)

                'autorizacion.Replace(""",".Replace")
                If autorizacion.Length = 0 Then
                    autorizacion = "000000"
                    Pagare_Pinpad = "ERROR_PT" & Separador & mensaje & Separador
                    ''MessageBox.Show("NO APROBADA 03")
                Else
                    Pagare_Pinpad = imprimir & Separador                      ''1
                    Pagare_Pinpad = Pagare_Pinpad & autorizacion & Separador  ''2
                    Pagare_Pinpad = Pagare_Pinpad & razonSocial & Separador   ''3
                    Pagare_Pinpad = Pagare_Pinpad & comercio & Separador      ''4
                    Pagare_Pinpad = Pagare_Pinpad & direccion & Separador     ''5
                    Pagare_Pinpad = Pagare_Pinpad & Afiliacion & Separador    ''6
                    Pagare_Pinpad = Pagare_Pinpad & Serie & Separador         ''7
                    Pagare_Pinpad = Pagare_Pinpad & fhTransaccion & Separador ''8
                    Pagare_Pinpad = Pagare_Pinpad & Hora_A & Separador        ''9
                    Pagare_Pinpad = Pagare_Pinpad & tarjeta & Separador       ''10
                    Pagare_Pinpad = Pagare_Pinpad & fhVencimiento & Separador ''11
                    Pagare_Pinpad = Pagare_Pinpad & emisor & Separador        ''12
                    Pagare_Pinpad = Pagare_Pinpad & operacion & Separador     ''13
                    Pagare_Pinpad = Pagare_Pinpad & monto_A & Separador       ''14
                    Pagare_Pinpad = Pagare_Pinpad & ingreso & Separador       ''15
                    Pagare_Pinpad = Pagare_Pinpad & Referencia & Separador    ''16
                    Pagare_Pinpad = Pagare_Pinpad & criptograma & Separador   ''17
                    Pagare_Pinpad = Pagare_Pinpad & Titular & Separador       ''18
                    Pagare_Pinpad = Pagare_Pinpad & firma & Separador         ''19
                    Pagare_Pinpad = Pagare_Pinpad & mensaje & Separador       ''20

                    Pagare_Pinpad = Pagare_Pinpad & leyenda1 & Separador           ''21
                    Pagare_Pinpad = Pagare_Pinpad & leyenda2 & Separador           ''22
                    Pagare_Pinpad = Pagare_Pinpad & poolId & Separador             ''23
                    Pagare_Pinpad = Pagare_Pinpad & saldoActualPuntos & Separador  ''24
                    Pagare_Pinpad = Pagare_Pinpad & saldoActualPesos & Separador   ''25
                    Pagare_Pinpad = Pagare_Pinpad & Monto_Puntos & Separador       ''26
                    Pagare_Pinpad = Pagare_Pinpad & vigenciaPuntos & Separador     ''27
                    ''MessageBox.Show("APROBADA")
                End If
                'LeeTarjeta.terminaTransaccion(autorizacion)

                'Pagare_Pinpad = LeeTarjeta.g
                'LeeTarjeta.terminaTransaccion(LeeTarjeta.getAutorizacion)
                Respuesta = Encabezado & Separador & Pagare_Pinpad & Trailer
            Else
                Mensaje_Pinpad = LeeTarjeta.getMensaje().ToString
                Respuesta = Encabezado & Separador & Mensaje_Pinpad_All1 & Trailer
                'LeeTarjeta.terminaTransaccion("000000")
                'LeeTarjeta.setMensajeError("NO APROBADA 02")
                'LeeTarjeta.terminaTransaccion("000000")
                Pagare_Pinpad = "NO APROBADA+ 02"
                Respuesta = Encabezado & Separador & Pagare_Pinpad & Trailer
                ''MessageBox.Show("NO APROBADA 02")
            End If
            Borra_Totales_Tarjetas()
        Catch exp As Exception
            Respuesta = Encabezado & Separador & exp.ToString & Trailer
            ''MessageBox.Show(Respuesta)
        End Try
    End Sub


    Sub Procesa_Bines_Lealtad_II()
        Try
            Log_Field = "snochebuena@adquira.com.mx" & Separador & "CONSULTA"
            SQLInsertLogPinpad(Encabezado, Campos(0), Log_Field)
            Respuesta_Autorizacion_VisaMaster = LeeTarjeta.getBinesDeLealtad("snochebuena@adquira.com.mx", "CONSULTA")
            SQLInsertLogPinpad(Encabezado, "RESPUESTA:" & Campos(0), Respuesta_Autorizacion_VisaMaster)
            Respuesta1 = Respuesta_Autorizacion_VisaMaster

            Inicio_L = 0
            Separa_Campos_Individules_Favorite("value=", "bancomer", "/>", Bines_Lealtad, Inicio_L)

            If Bines_Lealtad.Length > 0 Then
                Proceso_Separa()
            End If
            Mensaje_Pinpad = LeeTarjeta.getMensaje().ToString
            Mensaje_Pinpad2 = LeeTarjeta.getMensajeError().ToString
            Mensaje_Pinpad3 = LeeTarjeta.getMensajeWebSer().ToString
            Mensaje_Pinpad_All1 = "3|" & Mensaje_Pinpad2 & "|" & Mensaje_Pinpad3 & "|" & Respuesta_Autorizacion_VisaMaster
            Respuesta = Encabezado & Separador & Mensaje_Pinpad_All1 & Trailer
        Catch exp As Exception
            Respuesta = Encabezado & Separador & exp.ToString & Trailer
            ''MessageBox.Show(Respuesta)
        End Try
    End Sub
    Sub Procesa_Puntos()
        Try
            LeeTarjeta.LeerDatosTarjeta("1.00")
            Mensaje_Pinpad = LeeTarjeta.getMensaje().ToString
            Mensaje_Pinpad2 = LeeTarjeta.getMensajeError().ToString
            Mensaje_Pinpad3 = LeeTarjeta.getMensajeWebSer().ToString
            Mensaje_Pinpad_All = "3|" & Mensaje_Pinpad & "|" & Mensaje_Pinpad2 & "|" & Mensaje_Pinpad3
            If Mensaje_Pinpad.Length > 0 Or Mensaje_Pinpad2.Length > 0 Then
                If LeeTarjeta.getMensaje().Equals("LECTURA EXITOSA") Then
                    LeeTarjeta.setMensajeError("LECTURA EXITOSA")
                    'LeeTarjeta.terminaTransaccion("      ")
                Else
                    LeeTarjeta.setMensajeError("NO APROBADA 01")
                    Respuesta = Encabezado & Separador & Mensaje_Pinpad_All & Trailer
                    LeeTarjeta.terminaTransaccion("000000", "", "")
                    Mensaje_Pinpad = "NO APROBADA 01"
                    ''MessageBox.Show("NO APROBADA 01")
                    Exit Sub
                End If
            ElseIf Mensaje_Pinpad = "TIMEOUT" Then
                Mensaje_Pinpad = "_timeout_pinpad"
                Respuesta = Encabezado & Separador & Mensaje_Pinpad & Trailer
                ''MessageBox.Show("TIMEOUT")
                Exit Sub
            End If
            No_Tarjeta = LeeTarjeta.getNumTarjeta
            Titular = LeeTarjeta.getTrack1
            Anio = LeeTarjeta.getAnyoVencimiento
            Mes = LeeTarjeta.getMesVencimiento
            Fecha_Vencimiento = Mes & Anio
            Tags_EMV = LeeTarjeta.getTagEMV
            Tag_5F34 = LeeTarjeta.getTag5F34
            Tag_9F27 = LeeTarjeta.getTag9F27
            Tag_9F26 = LeeTarjeta.getTag9F26
            Track2 = LeeTarjeta.getTrack2
            Flag = LeeTarjeta.getSignatureFlag
            CVV2 = LeeTarjeta.encriptaRijndael(Campos(7))
            Digest2 = Campos(1) & Campos(2) & Campos(6) & Campos(7)
            Digest = LeeTarjeta.encriptaHmac(Digest2)
            No_Tarjeta_A = No_Tarjeta
            No_Tarjeta = LeeTarjeta.encriptaRijndael(No_Tarjeta)
            'Email_Administrador = "vhdelangel@adquira.com.mx"
            Email_Administrador = "ramon.padilla@alsea.com.mx"

            Fecha_Vencimiento = LeeTarjeta.encriptaRijndael(Fecha_Vencimiento)
            'CVV2 = LeeTarjeta.encriptaRijndael(CVV2)
            'Digest2 = "1234567" & "1234567" & "100.00" & "121"
            'Digest = LeeTarjeta.encriptaHmac(Digest2)
            'Variable = "WEUE` WH��^^h^^ZEUE` W"

            Mensaje_Pinpad = No_Tarjeta_A & Separador
            Respuesta = Encabezado & Separador & Mensaje_Pinpad & Trailer
        Catch exp As Exception
            Respuesta = Encabezado & Separador & exp.ToString & Trailer
            SQLInsertLogPinpad(Encabezado, "RESPUESTA:" & Campos(0), Respuesta)
            ''MessageBox.Show(Respuesta)
        End Try
    End Sub
    Sub Termina_Transaccion_Pinpad()
        LeeTarjeta.terminaTransaccion("000000", "", "")
        'Mensaje_Pinpad = LeeTarjeta.getMensaje().ToString
        'Mensaje_Pinpad2 = LeeTarjeta.getMensajeError().ToString
        'Mensaje_Pinpad3 = LeeTarjeta.getMensajeWebSer().ToString
        'Mensaje_Pinpad_All1 = "3|" & Mensaje_Pinpad2 & "|" & Mensaje_Pinpad3 & "|" & Respuesta_Autorizacion_VisaMaster
    End Sub
    Sub Procesa_Cancelacion_Pinpad_Final()
        Try
            LeeTarjeta.LeerDatosTarjeta(Campos(6))
            Mensaje_Pinpad = LeeTarjeta.getMensaje().ToString
            Mensaje_Pinpad2 = LeeTarjeta.getMensajeError().ToString
            Mensaje_Pinpad3 = LeeTarjeta.getMensajeWebSer().ToString
            Mensaje_Pinpad_All = "3|" & Mensaje_Pinpad & "|" & Mensaje_Pinpad2 & "|" & Mensaje_Pinpad3
            If Mensaje_Pinpad.Length > 0 Or Mensaje_Pinpad2.Length > 0 Then
                If LeeTarjeta.getMensaje().Equals("LECTURA EXITOSA") Then
                    LeeTarjeta.setMensajeError("LECTURA EXITOSA")
                    'LeeTarjeta.terminaTransaccion("      ")
                Else
                    LeeTarjeta.setMensajeError("NO APROBADA 01")
                    Respuesta = Encabezado & Separador & Mensaje_Pinpad_All & Trailer
                    LeeTarjeta.terminaTransaccion("000000", "", "")
                    Mensaje_Pinpad = "NO APROBADA 01"
                    ''MessageBox.Show("NO APROBADA 01")
                    Exit Sub
                End If
            ElseIf Mensaje_Pinpad = "TIMEOUT" Then
                Mensaje_Pinpad = "_timeout_pinpad"
                Respuesta = Encabezado & Separador & Mensaje_Pinpad & Trailer
                ''MessageBox.Show("TIMEOUT")
                Exit Sub
            End If
            No_Tarjeta = LeeTarjeta.getNumTarjeta
            Titular = LeeTarjeta.getTrack1
            Anio = LeeTarjeta.getAnyoVencimiento
            Mes = LeeTarjeta.getMesVencimiento
            Fecha_Vencimiento = Mes & Anio
            Tags_EMV = LeeTarjeta.getTagEMV
            Tag_5F34 = LeeTarjeta.getTag5F34
            Tag_9F27 = LeeTarjeta.getTag9F27
            Tag_9F26 = LeeTarjeta.getTag9F26
            Track2 = LeeTarjeta.getTrack2
            Flag = LeeTarjeta.getSignatureFlag
            CVV2 = LeeTarjeta.encriptaRijndael(Campos(7))
            Digest2 = Campos(1) & Campos(2) & Campos(6) & Campos(7)
            Digest = LeeTarjeta.encriptaHmac(Digest2)
            No_Tarjeta_A = No_Tarjeta
            No_Tarjeta = LeeTarjeta.encriptaRijndael(No_Tarjeta)
            'Email_Administrador = "vhdelangel@adquira.com.mx"
            Email_Administrador = "ramon.padilla@alsea.com.mx"

            Fecha_Vencimiento = LeeTarjeta.encriptaRijndael(Fecha_Vencimiento)
            Mensaje_Pinpad = No_Tarjeta_A & Separador
            Respuesta = Encabezado & Separador & Mensaje_Pinpad & Trailer

        Catch exp As Exception
            Respuesta = Encabezado & Separador & exp.ToString & Trailer
            SQLInsertLogPinpad(Encabezado, "RESPUESTA:" & Campos(0), Respuesta)
            ''MessageBox.Show(Respuesta)
        End Try

    End Sub
    Sub Borra_Totales_Tarjetas()
        No_Tarjeta = ""
        Titular = ""
        Anio = ""
        Mes = ""
        Fecha_Vencimiento = ""
        Tags_EMV = ""
        Tag_5F34 = ""
        Tag_9F27 = ""
        Tag_9F26 = ""
        Track2 = ""
        Flag = ""
        CVV2 = ""
        Digest2 = ""
        Digest = ""
        No_Tarjeta = ""
        Email_Administrador = ""
        Fecha_Vencimiento = ""
    End Sub

    Sub Proceso_Separa()
        Try
            ODBC_ConnectStringMICROS()
            For I = 1 To Len(Bines_Lealtad) Step 6
                Bines(II) = Mid(Bines_Lealtad, I, 6)

                InstrSql = "INSERT INTO custom.Bines_Pinpad (Nombre_Tarjeta, Bin ) VALUES " & _
                    "(" & "'Bancomer'," & "'" & Bines(II) & "' )"
                If Permite_Conexion_Lealtad = True Then
                    Dim Command As New OdbcCommand(InstrSql, ConexcionMCRSPOS)
                    Command.CommandType = CommandType.Text
                    ReadSql = Command.ExecuteScalar
                End If

                II = (II + 1)
            Next
            ODBC_ConnectStringMicrosClose()
        Catch exp As Exception
            Respuesta = Encabezado & Separador & exp.ToString & Trailer
        End Try
    End Sub
    Sub Limpia_Totales()
        imprimir = ""                     ''1
        autorizacion = ""
        razonSocial = ""
        comercio = ""
        direccion = ""
        Afiliacion = ""
        Serie = ""
        fhTransaccion = ""
        Hora_A = ""
        tarjeta = ""
        fhVencimiento = ""
        emisor = ""
        operacion = ""
        monto_A = ""
        ingreso = ""
        Referencia = ""
        criptograma = ""
        Titular = ""
        firma = ""
        mensaje = ""
        montoPesos = ""
        montoPuntos = ""
        montoTotal = ""
        leyenda2 = ""
        saldoAnteriorPuntos = ""
        saldoAnteriorPesos = ""
        saldoRedimidoPuntos = ""
        saldoRedimidoPesos = ""
        saldoActualPuntos = ""
        saldoActualPesos = ""
        vigenciaPuntos = ""
    End Sub
    ''New
    ''New 23Jul2013
    Sub Busca_Cheques_Lote()
        ReadFile()
        Email_Administrador = "ramon.padilla@alsea.com.mx"
        Respuesta = LeeTarjeta.enviar(string_xml, entidad, Email_Administrador)
    End Sub
    Private Sub SQLReadAutorizaciones()
        Try
            Permite_Conexcion_Lotes_Micros = False
            Salir = False
            Contador_Registros_Lote = 0
            ODBC_ConnectStringMICROS_Lotes()
            If Permite_Conexcion_Lotes_Micros = True Then
                Campo = "SELECT * FROM custom.Cierre_Lotes_Pinpad "
                Campo = Campo & " WHERE Tipo_Registro In ('Autorizacion VM', 'Autorizacion AM' ) "
                Campo = Campo & "ORDER BY Referencia"
                Dim Command As New OdbcCommand(Campo, ConexcionMCRSPOS)
                Command.CommandType = CommandType.Text
                Registro_Log = ("SQLReadAutorizaciones: " & Campo)
                ReadSql_odbc = Command.ExecuteReader(CommandBehavior.CloseConnection)
                While ReadSql_odbc.Read = True
                    Contador_Registros_Lote = (Contador_Registros_Lote + 1)
                    Cheques_Lotes(Contador_Registros_Lote) = ReadSql_odbc.GetValue(0)
                End While
            Else
                Salir = True
            End If
        Catch Errores_SQLSearchCanceladosOffLine As Exception
            Registro_Log = "Errores_SQLSearchCanceladosOffLine               :" + Errores_SQLSearchCanceladosOffLine.ToString
            'EventLog.WriteEntry(Registro_Log)
        Finally
            If Salir = False Then
                ReadSql_odbc.Close()
            End If
        End Try
    End Sub
    Private Sub ODBC_ConnectStringMICROS_Lotes()
        Try
            If ConexcionMCRSPOS.State = ConnectionState.Closed Then
                ''ConexcionMCRSPOS.ConnectionString = "DSN=Micros;UID=dba;PWD=smc9nair;Mode=Read"
                ConexcionMCRSPOS.ConnectionString = "DSN=Micros;UID=dba;PWD=Db@M@5t3r$;Mode=Read"
                ''ConexcionMCRSPOS.ConnectionString = "DSN=Micros;UID=dba;PWD=Micros3998.;Mode=Read"
                ConexcionMCRSPOS.Open()
            End If
            Permite_Conexcion_Lotes_Micros = True
        Catch Error_ODBC_ConnectStringMICROS_Lotes As Exception
            Permite_Conexcion_Lotes_Micros = False
            Registro_Log = "Error_ODBC_ConnectStringMICROS_Lotes              :" + Permite_Conexcion_MCRSPOS.ToString + " " + Error_ODBC_ConnectStringMICROS_Lotes.ToString
            Write_event_log_error_file(Registro_Log)
        End Try
    End Sub
    Sub cambiaCaracter31()
        Registro_Log = "cambiaCaracter31 Antes  :<" + Campos(31) + ">  Len: " + Len(Campos(31)).ToString
        AppendLog(Registro_Log)

        Hexadecimal(Campos(31))
        Registro_Log = "cambiaCaracter Hexad  :<" + campo_hex
        AppendLog(Registro_Log)

        If Campos(31).Length > 0 Then
            For Inicio_caracter = 1 To Len(Campos(31))
                If Mid(Campos(31), Inicio_caracter, 1) = Chr(29) Then
                    Mid(Campos(31), Inicio_caracter, 1) = Chr(0)
                End If
            Next
        End If
        Registro_Log = "cambiaCaracter31 Despues:<" + Campos(31) + ">  Len: " + Len(Campos(31)).ToString
        AppendLog(Registro_Log)

        Hexadecimal(Campos(31))
        Registro_Log = "cambiaCaracter Hexad  :<" + campo_hex
        AppendLog(Registro_Log)

    End Sub
    Sub cambiaCaracter19()
        Registro_Log = "cambiaCaracter19 Antes  :<" + Campos(19) + ">  Len: " + Len(Campos(19)).ToString
        AppendLog(Registro_Log)

        Hexadecimal(Campos(19))
        Registro_Log = "cambiaCaracter Hexad  :<" + campo_hex
        AppendLog(Registro_Log)

        If Campos(19).Length > 0 Then
            For Inicio_caracter = 1 To Len(Campos(19))
                If Mid(Campos(19), Inicio_caracter, 1) = Chr(29) Then
                    Mid(Campos(19), Inicio_caracter, 1) = Chr(0)
                End If
            Next
        End If
        Registro_Log = "cambiaCaracter19 Despues :<" + Campos(19) + ">  Len: " + Len(Campos(19)).ToString
        AppendLog(Registro_Log)

        Hexadecimal(Campos(19))
        Registro_Log = "cambiaCaracter Hexad  :<" + campo_hex
        AppendLog(Registro_Log)
    End Sub
    Sub cambiaCaracter18()
        Registro_Log = "cambiaCaracter18 Antes  :<" + Campos(18) + ">  Len: " + Len(Campos(18)).ToString
        AppendLog(Registro_Log)

        Hexadecimal(Campos(18))
        Registro_Log = "cambiaCaracter Hexad  :<" + campo_hex
        AppendLog(Registro_Log)

        If Campos(18).Length > 0 Then
            For Inicio_caracter = 1 To Len(Campos(18))
                If Mid(Campos(18), Inicio_caracter, 1) = Chr(29) Then
                    Mid(Campos(18), Inicio_caracter, 1) = Chr(0)
                End If
            Next
        End If
        Registro_Log = "cambiaCaracter18 Despues :<" + Campos(18) + ">  Len: " + Len(Campos(18)).ToString
        AppendLog(Registro_Log)

        Hexadecimal(Campos(18))
        Registro_Log = "cambiaCaracter Hexad  :<" + campo_hex
        AppendLog(Registro_Log)
    End Sub
    Sub cambiaCaracterSflag()
        Registro_Log = "cambiaCaracterSflag Antes  :<" + sFlag + ">  Len: " + Len(sFlag).ToString
        AppendLog(Registro_Log)

        Hexadecimal(sFlag)
        Registro_Log = "cambiaCaracter Hexad  :<" + campo_hex
        AppendLog(Registro_Log)

        If sFlag.Length > 0 Then
            For Inicio_caracter = 1 To Len(sFlag)
                If Mid(sFlag, Inicio_caracter, 1) = Chr(29) Then
                    Mid(sFlag, Inicio_caracter, 1) = Chr(0)
                End If
            Next
        End If
        Registro_Log = "cambiaCaracterSflag Despues :<" + sFlag + ">  Len: " + Len(sFlag).ToString
        AppendLog(Registro_Log)

        Hexadecimal(sFlag)
        Registro_Log = "cambiaCaracter Hexad  :<" + campo_hex
        AppendLog(Registro_Log)
    End Sub
    Sub cambiaCaracter20()
        Registro_Log = "cambiaCaracter20 Antes  :<" + Campos(20) + ">  Len: " + Len(Campos(20)).ToString
        AppendLog(Registro_Log)

        Hexadecimal(Campos(20))
        Registro_Log = "cambiaCaracter Hexad  :<" + campo_hex
        AppendLog(Registro_Log)

        If Campos(20).Length > 0 Then
            For Inicio_caracter = 1 To Len(Campos(20))
                If Mid(Campos(20), Inicio_caracter, 1) = Chr(29) Then
                    Mid(Campos(20), Inicio_caracter, 1) = Chr(0)
                End If
            Next
        End If
        Registro_Log = "cambiaCaracter20 Despues:<" + Campos(20) + ">  Len: " + Len(Campos(20)).ToString
        AppendLog(Registro_Log)

        Hexadecimal(Campos(20))
        Registro_Log = "cambiaCaracter Hexad  :<" + campo_hex
        AppendLog(Registro_Log)
    End Sub
    Sub cambiaCaracter17()
        Registro_Log = "cambiaCaracter17 Antes  :<" + Campos(17) + ">  Len: " + Len(Campos(17)).ToString
        AppendLog(Registro_Log)

        Hexadecimal(Campos(17))
        Registro_Log = "cambiaCaracter Hexad  :<" + campo_hex
        AppendLog(Registro_Log)

        If Campos(17).Length > 0 Then
            For Inicio_caracter = 1 To Len(Campos(17))
                If Mid(Campos(17), Inicio_caracter, 1) = Chr(29) Then
                    Mid(Campos(17), Inicio_caracter, 1) = Chr(0)
                End If
            Next
        End If
        Registro_Log = "cambiaCaracter17 Despues:<" + Campos(17) + ">  Len: " + Len(Campos(17)).ToString
        AppendLog(Registro_Log)

        Hexadecimal(Campos(17))
        Registro_Log = "cambiaCaracter Hexad  :<" + campo_hex
        AppendLog(Registro_Log)
    End Sub
    Sub Hexadecimal(ByVal valor_ As String)
        caracter_str = ""
        caracter_int = 0
        contador_hex2 = 0
        campo_hex = ""
        For contador_hex = 1 To valor_.Length
            caracter_str = Mid(valor_, contador_hex, 1)
            caracter_int = Asc(caracter_str)
            contador_hex2 = contador_hex2 + 1
            campo_hex = campo_hex + Hex(caracter_int) + " "
        Next
        campo_hex = campo_hex + " Len: " + contador_hex2.ToString
    End Sub
    ''New 23Jul2013

    '26 Nov 2013
    Sub Arma_Reverso_Datos()
        'Numero_WSID
        campos_Reverso(Numero_WSID) = ""

    End Sub
    Sub Arma_Reverso_Vacio()
        'Numero_WSID
        campos_Reverso(Numero_WSID) = ""

    End Sub
    '26 Nov 2013
End Class



