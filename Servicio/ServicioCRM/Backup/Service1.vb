Imports System.ServiceProcess
Imports System.Timers
Imports System.IO
'Imports System.Data.SqlClient
'Imports System.Data.OleDb
Imports System.Data.Odbc
Imports System.Threading
Imports System.Web.Mail
Imports System.Text


Public Class ServicioCRM
    Inherits System.ServiceProcess.ServiceBase
    Private WithEvents server As New Irc.Networking.ServerSocket

    '''FILES Const
    Public Const FileConfiguracion_CRM = "D:\MICROS\Res\Pos\Etc\Configuracion_CRM.TxT"
    Public Const Slash = "|"

    '''String
    Public txtFileNameLog As String
    Public Campo01, Campo02 As String
    Public Registro_Log As String
    Public Cadena_Inicio As String
    Public Socket_Peticion As String
    Dim Campos() As String
    Public Datos As String
    Public Respuesta As String
    Public Encabezado As String
    Public Datos1 As String
    Public Trailer As String
    Public Separador As String
    Public Numero_WSID_A As String
    Public Lectura(10) As String
    Public Marca As String
    Public Path_Entrada, Path_Salida As String

    '''''''Integer
    Public numero_Interface As Integer
    Public Inicio_Encabezado As Integer
    Public Fin_Encabezado As Integer
    Public Inicio_Datos As Integer
    Public Fin_Datos As Integer
    Public Inicio_Trailer As Integer
    Public Fin_Trailer As Integer
    Public Inicio_Separador As Integer
    Public Fin_Separador As Integer
    Public Longitud As Integer
    Public Contador_Datos As Integer
    Dim PuertoID As Integer
    Public Inicio As Integer
    Dim Timeout_Ifc As Integer

    '''Int32
    Public Timer_Lectura_Files_Fix As Int32

    ''' Int64
    Public Numero_WSID As Int64
    Public Timeout_ifc_ws As Int64

    ''' Boolean
    Public Permite_Conexcion As Boolean
    Public Permite_Conexcion_MCRSPOS As Boolean
    Public Permite_Conexcion_CRM As Boolean
    Public Salir As Boolean
    Public Servicio_Micros_Start, Servicio_Micros_First As Boolean

    ''' StreamReader and StreamWriter
    Dim myStreamReader As StreamReader
    Dim myStreamWriter As StreamWriter

    ''' ConexcionSybase odbc
    Dim ConexcionSybase As New OdbcConnection
    Dim Conexcion As New OdbcConnection
    Dim ReadSql_odbc As OdbcDataReader
    Dim ConexcionMCRSPOS As New OdbcConnection



#Region " Component Designer generated code "

    Public Sub New()
        MyBase.New()

        ' This call is required by the Component Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call

    End Sub

    'UserService overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    ' The main entry point for the process
    <MTAThread()> _
    Shared Sub Main()
        Dim ServicesToRun() As System.ServiceProcess.ServiceBase

        ' More than one NT Service may run within the same process. To add
        ' another service to this process, change the following line to
        ' create a second service object. For example,
        '
        '   ServicesToRun = New System.ServiceProcess.ServiceBase () {New Service1, New MySecondUserService}
        '
        ServicesToRun = New System.ServiceProcess.ServiceBase() {New ServicioCRM}

        System.ServiceProcess.ServiceBase.Run(ServicesToRun)
    End Sub

    'Required by the Component Designer
    Private Tiempo As Timers.Timer
    Private components As System.ComponentModel.IContainer
    Private timeStart As DateTime          ' Used to note the start time of the service
    Private timeEnd As DateTime            ' Used to note the end time of the service
    Private timeElapsed As New TimeSpan(0) ' Initialize to 0
    Private timeDifference As TimeSpan
    Private isPaused As Boolean = False    ' Notes whether the service is paused
    Private Error_Field, Vacio, Field_Log As String
    Private File_Error, File_Lee_Error, File_Contador, File_Terminales, File_Operations, File_Log, File_Log_Bak, File_Uws As String
    Private Respuesta_N, rowCount, rowCount0, UWS_Reset, Process_To_Kill, Resp, Contador, I, Hora_Delete, Minuto_Delete As Integer
    Dim Numero_Uws(100), Numero_Proceso(100) As Integer
    Private Flag_Operations, Flag_Borrar_Archivo As Boolean
    '''NEW
    '''NEW

    ' NOTE: The following procedure is required by the Component Designer
    ' It can be modified using the Component Designer.  
    ' Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        '
        'ServicioCFDFacturacion
        '
        Me.CanHandlePowerEvent = True
        Me.CanShutdown = True
        Me.ServiceName = "ServicioCFDFacturacion"

    End Sub

#End Region

    '''PROCESS SERVICES
    Protected Overrides Sub OnStart(ByVal args() As String)
        Try
            Tiempo = New Timers.Timer
            AddHandler Tiempo.Elapsed, AddressOf TimerFired
            Tiempo.Enabled = False
            Tiempo.Stop()
            AutoLog = True
            Servicio_Micros_Start = False
            Servicio_Micros_First = False
            'System.Threading.Thread.CurrentThread.Sleep(180000)
            'SQLReaderTimeout_interface_micros()
            'If Permite_Conexcion_MCRSPOS = False Then
            'SQLReaderTimeout_Facturacion()
            'Dispose(True)
            'Else
            'ReadFile()
            Calcula_Tiempo_Servicio()
            'Abre_Puerto_Comunicacion()
            'End If
        Catch exc As Exception
            Error_Field = "OnStart: " & exc.Message
            Write_event_log_error_file(Error_Field)
        Finally
            '''Error_Field = "OnStart: Finally"
            '''Write_event_log_error_file(Error_Field)
        End Try
    End Sub
    Protected Overrides Sub OnStop()
        Try
            Error_Field = "OnStop: " & "** OnStop: "
            Write_event_log_error_file(Error_Field)
            Cierra_Puerto_Comunicacion()
            Tiempo.Dispose()
            AutoLog = False
        Catch exc As Exception
            Error_Field = exc.Message
            Write_event_log_error_file(Error_Field)
        Finally
        End Try
    End Sub
    Protected Overrides Function OnPowerEvent(ByVal powerStatus As System.ServiceProcess.PowerBroadcastStatus) As Boolean
        Try
            Error_Field = "OnPowerEvent: " & "** OnStop: "
            Write_event_log_error_file(Error_Field)
            Tiempo.Stop()
            Tiempo.Dispose()
            AutoLog = False
        Catch exc As Exception
            Error_Field = exc.Message
            Write_event_log_error_file(Error_Field)
        Finally
        End Try
    End Function
    Private Sub TimerFired(ByVal sender As Object, ByVal e As ElapsedEventArgs)
        Try
            'Error_Field = "**TimerFired Inicia: "
            'Write_event_log_error_file(Error_Field)
            Tiempo.Enabled = False
            Tiempo.Stop()
            Detect_Servicio_Micros()
            If Servicio_Micros_Start = True Then
                If Servicio_Micros_First = False Then
                    'SQLReaderTimeout_interface_micros()
                    'If Permite_Conexcion_MCRSPOS = True Then
                    'ReadFile()
                    'Calcula_Tiempo_Servicio()
                    'Abre_Puerto_Comunicacion()
                    Error_Field = "**TimerFired True: " + "  " + Servicio_Micros_First.ToString + " " + Servicio_Micros_Start.ToString
                    Write_event_log_error_file(Error_Field)
                    Proceso_PrincipalTimerFired()
                    Servicio_Micros_First = True
                    'Else
                    'Servicio_Micros_First = False
                Else
                    '''Error_Field = "**TimerFired False: " + "  " + Servicio_Micros_First.ToString + " " + Servicio_Micros_Start.ToString
                    '''Write_event_log_error_file(Error_Field)
                End If
            Else
                If Servicio_Micros_First = True Then
                    Error_Field = "Servicio_Micros_Start false y Servicio_Micros_First true: Cierra Puerto"
                    Write_event_log_error_file(Error_Field)
                    Cierra_Puerto_Comunicacion()
                End If
            End If

            'If Servicio_Micros_First = True Then
            'Proceso_PrincipalTimerFired()
            'End If
            'End If
        Catch exc As Exception
            Error_Field = "**TimerFired Inicia. Exception: " + exc.Message
            Write_event_log_error_file(Error_Field)
        Finally
            Calcula_Tiempo_Servicio()
        End Try
    End Sub
    Sub Detect_Servicio_Micros()
        Try
            Dim Computername As String = System.Net.Dns.GetHostName
            Dim controller As New ServiceController
            Dim status As String
            controller.MachineName = "."
            controller.ServiceName = "SQLANYs_sql" & Computername
            status = controller.Status.ToString
            controller.Refresh()
            If status = "Running" Then
                Servicio_Micros_Start = True
            Else
                Servicio_Micros_Start = False
            End If
            '''Error_Field = "**Detect_Servicio_Micros: " + "  " + controller.ServiceName.ToString + " " + status + " " + Servicio_Micros_Start.ToString
            '''Write_event_log_error_file(Error_Field)
        Catch Error_Detect_Servicio_Micros As Exception
            Error_Field = "**Error_Detect_Servicio_Micros: " + "  " + Error_Detect_Servicio_Micros.ToString
            Write_event_log_error_file(Error_Field)
        End Try
    End Sub

    '''MAIN PROCESS
    Private Sub Proceso_PrincipalTimerFired()
        Try
            SQLReaderTimeout_interface_micros_CRM()
            If Permite_Conexcion_MCRSPOS = False Then
                SQLReaderTimeout_interface_micros_CRM()
            End If
            ReadFile()
            'Calcula_Tiempo_Servicio()
            Abre_Puerto_Comunicacion()
            Error_Field = "**Proceso_Principal. PROCESS MAIN: "
            Write_event_log_error_file(Error_Field)
        Catch exc As Exception
            Error_Field = "**Proceso_Principal. Exeception: " & exc.Message
            Write_event_log_error_file(Error_Field)
        Finally
        End Try
    End Sub
    Private Sub Calcula_Tiempo_Servicio()
        Try
            With Tiempo
                .Interval = 1
                .AutoReset = True
                .Enabled = True
                .Start()
            End With
        Catch exc As Exception
            Error_Field = exc.Message
            Write_event_log_error_file(Error_Field)
        Finally
        End Try
    End Sub

    '''SHOW MESSAGE ON SERVER
    Public Sub Muestra_Mensaje_Server()
        Try
            Dim current As Thread = Thread.CurrentThread
            MsgBox(current.Name, MsgBoxStyle.MsgBoxSetForeground, "Facturacion Electronica: ")
        Catch Errores_Muestra_Mensaje_Server As Exception
            Registro_Log = "Errores_Muestra_Mensaje_Server        :" + Errores_Muestra_Mensaje_Server.ToString
            'AppendLog(Registro_Log)
        Finally
        End Try
    End Sub

    ''' MAIN COMUNICATIONS
    Private Sub Abre_Puerto_Comunicacion()
        Try
            If Not server.Listening() Then
                server.Listen(Int32.Parse(Convert.ToString(PuertoID)))
                '''Error_Field = "Abre_Puerto_Comunicacion: " & Registro_Log
                '''Write_event_log_error_file(Error_Field)
            Else
            End If
        Catch Error_Abre_Puerto_Comunicacion As Exception
            Error_Field = "**Error_Abre_Puerto_Comunicacion: " & Error_Abre_Puerto_Comunicacion.Message
            Write_event_log_error_file(Error_Field)
        Finally
        End Try
    End Sub
    Private Sub server_ClientConnectionClosed(ByVal sender As Object, ByVal e As Irc.Networking.ServerSocket.ServerSocketEventArgs) Handles server.ClientConnectionClosed
        Try
            Error_Field = "server_ClientConnectionClosed      :" + e.ClientSocket.ToString
            Write_event_log_error_file(Error_Field)
            'New lbConnectedClients.Items.Remove(e.ClientSocket)
        Catch Errores_ClientConnectionClosed As Exception
            Error_Field = Errores_ClientConnectionClosed.Message
            Write_event_log_error_file(Error_Field)
            'New MessageBox.Show("Error ClientConnectionClosed", "Error cerrando Socket", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally

        End Try

    End Sub
    Private Sub server_ClientConnectSuccess(ByVal sender As Object, ByVal e As Irc.Networking.ServerSocket.ServerSocketEventArgs) Handles server.ClientConnectSuccess
        Try
            Error_Field = "server_ClientConnectSuccess        :" + e.ClientSocket.ToString
            Write_event_log_error_file(Error_Field)
            'New lbConnectedClients.Items.Add(e.ClientSocket)
        Catch Errores_ClientConnectSuccess As Exception
            Error_Field = "Errores_ClientConnectSuccess       :" + e.ClientSocket.ToString + "  " + Errores_ClientConnectSuccess.ToString
            Write_event_log_error_file(Error_Field)
            'New MessageBox.Show("Error ClientConnectSuccess", "Error en Sockets", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally

        End Try

    End Sub
    Private Sub server_ConnectionRequest(ByVal sender As Object, ByVal e As System.EventArgs) Handles server.ConnectionRequest
        Try
            Error_Field = "server_ConnectionRequest           :" + e.GetType.ToString
            Write_event_log_error_file(Error_Field)
            server.Accept()
        Catch Errores_ConnectionRequest As Exception
            Error_Field = "Errores_ConnectionRequest          :" + Errores_ConnectionRequest.ToString
            Write_event_log_error_file(Error_Field)
            'New MessageBox.Show("Error ConnectionRequest", "Error Aceptando Socket", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally

        End Try

    End Sub
    Private Sub server_DataArrivalFromClient(ByVal sender As Object, ByVal e As Irc.Networking.ServerSocket.ServerSocketEventArgs) Handles server.DataArrivalFromClient
        Try
            Cadena_Inicio = e.ClientSocket.GetArrivedText.ToString
            Socket_Peticion = e.ClientSocket.ToString()
            Registro_Log = "server_DataArrivalFromClient <<01<<   :" + e.ClientSocket.ToString + "  " + e.ClientSocket.GetArrivedText.ToString
            AppendLog(Registro_Log)
            Registro_Log = "server_DataArrivalFromClient <<02<<   :" + e.ClientSocket.GetArrivedText
            AppendLog(Registro_Log)
            Registro_Log = "server_DataArrivalFromClient <<02a<   :" + Socket_Peticion.ToString()
            AppendLog(Registro_Log)
            Registro_Log = "server_DataArrivalFromClient <<02b<   :" + Cadena_Inicio
            AppendLog(Registro_Log)
            Registro_Log = "server_DataArrivalFromClient <<02c<   :" + e.ClientSocket.GetArrivedText.ToString
            AppendLog(Registro_Log)
            If Cadena_Inicio.Length() > 0 Then
                'New tbMessages.Text += "<" + Socket_Peticion + ">" + "Recibo" + vbCrLf
                Separa_Cadena_Datos_Recibida(Cadena_Inicio)
                Campos = Datos.Split("|")
                Registro_Log = "server_DataArrivalFromClient <<03<<   :" + Mid(Datos, 6, 5)
                AppendLog(Registro_Log)

                'End If
                'Datos = Encabezado & "RE-INTENTARLO" & Chr(3) & "ABCD" & Chr(4)
                If Mid(Datos, 28, 13) = "RE-INTENTARLO" Then
                    Respuesta = Encabezado & "RE-INTENTARLO" & Chr(3) & "ABCD" & Chr(4)
                    Envia_Respuesta(e, Respuesta)
                ElseIf Mid(Datos, 6, 5) = "FCH_J" Then     '///Solicita Fecha Juliana////
                    'Procesa_FCH_J(Datos)
                    Envia_Respuesta(e, Respuesta)
                    'ElseIf Mid(Datos, 6, 5) = "FAM_V" Then  '///Valida Familias////
                    'Valida_Familias(Datos)
                    'Envia_Respuesta(e, Respuesta)
                ElseIf Mid(Datos, 6, 5) = "RFC_C" Then  '///Consulta de Clientes////
                    'Procesa_RFC_C(Datos)
                    Envia_Respuesta(e, Respuesta)
                ElseIf Mid(Datos, 6, 5) = "RFC_U" Then  '///Modifica Datos de Clientes////
                    'Update_RFC_U()
                    Envia_Respuesta(e, Respuesta)
                ElseIf Mid(Datos, 6, 5) = "RFC_A" Then  '///Alta Datos de Clientes////
                    'Insert_RFC_A()
                    Envia_Respuesta(e, Respuesta)
                ElseIf Mid(Datos, 6, 5) = "FAC_S" Then  '///Busca Factura en Interfactura////
                    'Procesa_FAC_Busca(Datos)
                    Envia_Respuesta(e, Respuesta)
                ElseIf Mid(Datos, 6, 5) = "FAC_A" Then   '///Solicita Factura///
                    'Borra_Facturas_Pdf_Txt()
                    'Insert_FAC_N()
                    Envia_Respuesta(e, Respuesta)
                ElseIf Mid(Datos, 6, 5) = "FAC_V" Then    '////Cancela factura/// *********
                    'Procesa_FAC_V(Datos)
                    Envia_Respuesta(e, Respuesta)
                ElseIf Mid(Datos, 6, 5) = "FAC_L" Then   '///Imprime Factura///
                    'Imprime_FAC_L()
                    Envia_Respuesta(e, Respuesta)
                ElseIf Mid(Datos, 6, 5) = "FAC_E" Then   '///eMail Factura///
                    'Envia_eMail_FAC_E()
                    Envia_Respuesta(e, Respuesta)
                ElseIf Mid(Datos, 6, 5) = "TIK_V" Then  '///Cancela Ticket en Repositorio BK////
                    'Procesa_Cancela_Ticket_Repositorio(Datos)
                    Envia_Respuesta(e, Respuesta)
                ElseIf Mid(Datos, 6, 5) = "TIK_R" Then  '///Send Ticket a Repositorio Delivery////
                    Datos1 = "OK"
                    Contador_Datos = 1
                    Respuesta = Encabezado & Separador & Contador_Datos & Slash & Datos1 & Trailer
                    Envia_Respuesta(e, Respuesta)
                    'Procesa_Envia_Ticket_Repositorio(Datos)
                ElseIf Mid(Datos, 6, 5) = "SOR_T" Then  '///Ordena Totales Mayor a Menor////
                    'Procesa_Sort_Cantidades_Mayor_a_Menor(Datos)
                    Envia_Respuesta(e, Respuesta)
                ElseIf Mid(Datos, 6, 5) = "INS_T" Then  '///Inserta Cheque de Micros en CFD////
                    Datos1 = "OK"
                    Contador_Datos = 1
                    Respuesta = Encabezado & Separador & Contador_Datos & Slash & Datos1 & Trailer
                    Envia_Respuesta(e, Respuesta)
                    'Procesa_Insert_Ticket_CFD(Datos)
                Else
                    Respuesta = Encabezado & Separador & Datos & Trailer
                    Envia_Respuesta(e, Respuesta)
                End If
                Borra_Variables()
            End If
        Catch Errores_DataArrivalFromClient As Exception
            Error_Field = "Errores_DataArrivalFromClient      :" + Errores_DataArrivalFromClient.ToString
            Respuesta = Encabezado & Separador & "ERROR DATOS" & Trailer
            Write_event_log_error_file(Error_Field)
            'New MessageBox.Show("Error DataArrivalFromClient", "Datos Recibido Incorrectos", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
        End Try
    End Sub
    Private Sub Abre_Cierra_Socket()
        Try
            If Not server.Listening() Then
                If Permite_Conexcion = True Then
                    Registro_Log = "Abre_Cierra_Socket                 :" + Convert.ToString(PuertoID)
                    AppendLog(Registro_Log)
                    server.Listen(Int32.Parse(Convert.ToString(PuertoID)))
                Else
                    Registro_Log = "Abre_Cierra_Socket                 :" + "No se Puede Abrir Puerto de Comunicacion  " + Convert.ToString(PuertoID)
                End If
            Else
                Registro_Log = "Abre_Cierra_Socket                 :" + "Puerto Cerrado  " + Convert.ToString(PuertoID)
                AppendLog(Registro_Log)
                server.Close()
            End If
        Catch Error_Abre_Cierra_Socket As Exception
            Error_Field = "Error_Abre_Cierra_Socket           :" + Convert.ToString(PuertoID) + "  " + Error_Abre_Cierra_Socket.ToString
            Write_event_log_error_file(Error_Field)
        Finally
        End Try
    End Sub
    Private Sub Cierra_Puerto_Comunicacion()
        Try
            If server.Listening() Then
                server.Close()
                'New lbConnectedClients.ClearSelected()
                Registro_Log = "Cierra_Puerto_Comunicacion         :" + "server.Close"
                Error_Field = Registro_Log
                Write_event_log_error_file(Error_Field)
                'AppendLog(Registro_Log)
            Else
                'MessageBox.Show("El Servicio ya Esta Detenido", "Servicio", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        Catch Error_Cierra_Puerto_Comunicacion As Exception
            Error_Field = "**Error_Cierra_Puerto_Comunicacion: " & Error_Cierra_Puerto_Comunicacion.Message
            Write_event_log_error_file(Error_Field)
        Finally
        End Try
    End Sub


    ''''COMUNICATION DATA
    Private Sub Envia_Respuesta(ByVal e As Irc.Networking.ServerSocket.ServerSocketEventArgs, ByVal Envia As String)
        Try
            If (TypeOf (e.ClientSocket) Is Irc.Networking.ClientSocket) Then
                Dim c As Irc.Networking.ClientSocket = e.ClientSocket
                'Registro_Log = "Envia_Respuesta >>>                :"
                'AppendLog(Registro_Log)
                Registro_Log = "Envia_Respuesta >>>                :" + e.RemoteHost.ToString() + ":" + e.RemotePort.ToString + ">>> Respondio >>>" + Envia + ControlChars.CrLf
                AppendLog(Registro_Log)
                c.SendTextAppendCrlf(Envia)
                'New tbMessages.Text += "<" + e.RemoteHost.ToString() + ":" + e.RemotePort.ToString + "> Respondio " + ControlChars.CrLf
            End If
        Catch Errores_Envia_Respuesta As Exception
            Error_Field = "Errores_Envia_Respuesta >>>        :" + Envia + "  " + Errores_Envia_Respuesta.ToString
            Write_event_log_error_file(Error_Field)
            'New MessageBox.Show("Error Envia_Respuesta", "Error Enviando Respuesta", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
        End Try
        Respuesta = ""
    End Sub
    Private Sub Separa_Cadena_Datos_Recibida(ByVal Cadena)
        Try
            Registro_Log = "Separa_Cadena_Datos_Recibida       :" + Cadena
            AppendLog(Registro_Log)
            For Inicio = 1 To Len(Cadena)
                If Mid(Cadena, Inicio, 1) = Chr(1) Then
                    Inicio_Encabezado = Inicio
                ElseIf Mid(Cadena, Inicio, 1) = Chr(2) Then
                    Fin_Encabezado = Inicio
                    Inicio_Datos = Inicio + 1
                ElseIf Mid(Cadena, Inicio, 1) = Chr(3) Then
                    Fin_Datos = Inicio
                    Inicio_Trailer = Inicio
                ElseIf Mid(Cadena, Inicio, 1) = Chr(4) Then
                    Fin_Trailer = Inicio
                ElseIf Mid(Cadena, Inicio, 1) = Chr(28) Then
                    Inicio_Separador = Inicio
                End If
            Next
            ''
            If Fin_Datos = 0 Then
                Fin_Datos = Len(Cadena)
            End If
            If Fin_Trailer = 0 Then
                Trailer = Chr(3) & "ABCD" & Chr(4)
            End If
            ''
            If Inicio_Encabezado > 0 Then
                Encabezado = (Mid(Cadena, Inicio_Encabezado, Fin_Encabezado))
            End If
            If Inicio_Datos > 0 Then
                Datos = (Mid(Cadena, Inicio_Datos, (Fin_Datos - Inicio_Datos)))
            End If
            If Inicio_Trailer > 0 Then
                Trailer = (Mid(Cadena, Inicio_Trailer, (Fin_Trailer - Inicio_Trailer + 1)))
            End If
            If Inicio_Separador > 0 Then
                Separador = Mid(Cadena, Inicio_Separador, 4)
            End If
            If Encabezado.Length > 10 Then
                Numero_WSID_A = Encabezado.Substring(8, 2)
                Numero_WSID = Convert.ToInt64(Numero_WSID_A)
            End If
        Catch Errores_Separa_Cadena_Datos_Recibid As Exception
            Error_Field = "Errores_Separa_Cadena_Datos_Recibid:" + Cadena + "  " + Errores_Separa_Cadena_Datos_Recibid.ToString
            Write_event_log_error_file(Error_Field)
            Datos = Encabezado & Chr(2) & Chr(28) & "00 |" & "RE-INTENTARLO" & Chr(3) & "ABCD" & Chr(4)
            'New MessageBox.Show("Error Separa_Cadena_Datos_Recibida", "Error Separando Datos Recibidos", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
        End Try
    End Sub

    '''' LOGFILE
    Private Sub CloseLog()
        Try
            Return
            If Not myStreamWriter Is Nothing Then
                myStreamWriter.Close()
            End If
        Catch exc As Exception
            MsgBox("CloseLog: Exception: " + "<>" + exc.Message)
        Finally

        End Try
    End Sub
    Private Sub Verifica_Dia_Log()
        Try
            Return
            Campo02 = Format(Date.Now.Day, "00") + "-" + Format(Date.Now.Month, "00") + "-" + Format(Date.Now.Year, "0000") + ".Log"
            If txtFileNameLog <> Campo02 Then
                CloseLog()
                txtFileNameLog = Format(Date.Now.Day, "00") + "-" + Format(Date.Now.Month, "00") + "-" + Format(Date.Now.Year, "0000") + ".Log"
                myStreamWriter = File.AppendText(txtFileNameLog)
            End If
        Catch exc As Exception
            MsgBox("Verifica_Dia_Log: Exception: " + txtFileNameLog + "<>" + exc.Message)
        Finally
        End Try
    End Sub
    Private Sub OpenLog()
        Try
            Return
            If Not myStreamWriter Is Nothing Then
                myStreamWriter.Close()
            End If
            txtFileNameLog = Format(Date.Now.Day, "00") + "-" + Format(Date.Now.Month, "00") + "-" + Format(Date.Now.Year, "0000") + ".Log"
            myStreamWriter = File.AppendText(txtFileNameLog)
        Catch exc As Exception
            MsgBox("OpenLog: Exception: " + txtFileNameLog + "<>" + exc.Message)
        Finally
        End Try
    End Sub
    Private Sub AppendLog(ByVal Reg_Log As String)
        Try
            Return
            Verifica_Dia_Log()
            OpenLog()
            Campo01 = Format(Date.Now.Day, "00") + "-" + Format(Date.Now.Month, "00.") + "-" + Format(Date.Now.Year, "0000 ") + Format(Date.Now.Hour, "00") + ":" + Format(Date.Now.Minute, "00") + ":" + Format(Date.Now.Second, "00") + ":" + Format(Date.Now.Millisecond, "000") + "..." + Reg_Log + "..." + ControlChars.CrLf
            myStreamWriter.Write(Campo01)
            myStreamWriter.Flush()
            CloseLog()
        Catch exc As Exception
            '''MsgBox("AppendLog: Exception: " + Campo01 + "<>" + exc.Message)
        Finally
        End Try
    End Sub

    ''' EVENTLOG
    Public Sub Write_event_log_error_file(ByVal Error_Campo As String)
        Try
            EventLog.WriteEntry("Service Repositorio: " + Error_Campo)
        Catch exc As Exception
            Error_Field = "Write_event_log_error_file. Exception: " + exc.Message
            EventLog.WriteEntry("Service Repositorio: (!ERROR INTERNO!)" + Error_Field)
        Finally
        End Try
    End Sub

    '''CONECCTION DB
    Private Sub SQLConnectString(ByVal sServer, ByVal sUser, ByVal sPassword)
        Try
            Registro_Log = "SQLConnectString                   :" + sServer + "  " + sUser + "  " + sPassword
            AppendLog(Registro_Log)
            If Conexcion.State = ConnectionState.Closed Then
                Conexcion.ConnectionString = "SERVER=" & sServer & ";UID=" & sUser & _
                                     ";PWD=" & sPassword & ";DATABASE=micros.db"
                Conexcion.Open()
            End If
            Permite_Conexcion = True  'MessageBox.Show("Base de datos Conetada")
            Registro_Log = "SQLConnectString                   :" + sServer + "  " + sUser + "  " + sPassword + "  " + "Base de datos Conetada  " + Permite_Conexcion.ToString
            AppendLog(Registro_Log)
        Catch Error_SQLConnectString As Exception
            Permite_Conexcion = False
            Error_Field = "SQLConnectString                   :" + sServer + "  " + sUser + "  " + sPassword + "  " + "Base de datos NO Conetada  " + Permite_Conexcion.ToString + "  " + Error_SQLConnectString.ToString
            Write_event_log_error_file(Error_Field)
            'New MessageBox.Show("Error SQLConnectString" & Error_SQLConnectString.ToString, "Error Conectando a la Base de Datos " & Error_SQLConnectString.ToString, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub ODBC_ConnectStringMICROS(ByVal sDataBase)
        Try
            Registro_Log = "ODBCDataBase                      :" + sDataBase
            AppendLog(Registro_Log)
            If ConexcionMCRSPOS.State = ConnectionState.Closed Then
                ConexcionMCRSPOS.ConnectionString = "DSN=Micros;UID=dba;PWD=smc9nair;Mode=Read"
                ConexcionMCRSPOS.Open()
            End If
            Permite_Conexcion_MCRSPOS = True
            Registro_Log = "ODBC_ConnectStringMICROS           :" + sDataBase + "  " + Permite_Conexcion_MCRSPOS.ToString
            AppendLog(Registro_Log)
        Catch Error_ODBC_ConnectStringMICROS As Exception
            Permite_Conexcion = False
            Error_Field = "Error_ODBC_ConnectStringMICROS              :" + sDataBase + "  " + Permite_Conexcion_MCRSPOS.ToString + " " + Error_ODBC_ConnectStringMICROS.ToString
            Write_event_log_error_file(Error_Field)
        End Try
    End Sub

    '''PORT CRM FROM MICROS
    Private Sub SQLReaderTimeout_interface_micros_CRM()
        Try
            Registro_Log = "SQLReaderTimeout_interface_micros_CRM                      :"
            AppendLog(Registro_Log)
            Permite_Conexcion_MCRSPOS = False
            Salir = False
            ODBC_ConnectStringMICROS("micros.db")
            If Permite_Conexcion_MCRSPOS = True Then
                Dim Command As New OdbcCommand("SELECT timeout, tcp_port_number FROM micros.interface_def where obj_num = 6 ", ConexcionMCRSPOS)
                Command.CommandType = CommandType.Text
                ReadSql_odbc = Command.ExecuteReader(CommandBehavior.CloseConnection)
                If ReadSql_odbc.Read = True Then
                    Timeout_Ifc = ReadSql_odbc.GetValue(0)
                    PuertoID = ReadSql_odbc.GetValue(1)
                    Timeout_ifc_ws = ((Timeout_Ifc - 5) * 1000)
                    Permite_Conexcion_MCRSPOS = True
                    Registro_Log = "SQLReaderTimeout_interface_micros_CRM                      :" + Convert.ToString(PuertoID)
                    AppendLog(Registro_Log)
                End If
            Else
                Salir = True
            End If
        Catch Errores_SQLReaderTimeout_cfd_facturacion As Exception
            Permite_Conexcion_MCRSPOS = False
            Error_Field = "Errores_SQLReaderTimeout_cfd_facturacion                  :" + Convert.ToString(PuertoID) + "  " + Errores_SQLReaderTimeout_cfd_facturacion.ToString
            Write_event_log_error_file(Error_Field)
        Finally
            If Salir = False Then
                ReadSql_odbc.Close()
            End If
        End Try
    End Sub

    '''DELETE VAR
    Private Sub Borra_Variables()
        Registro_Log = "Borra_Variables                    :"
        AppendLog(Registro_Log)
        Permite_Conexcion = False

        Inicio = 0
        Inicio_Encabezado = 0
        Fin_Encabezado = 0
        Inicio_Datos = 0
        Fin_Datos = 0
        Inicio_Trailer = 0
        Fin_Trailer = 0
        Inicio_Separador = 0
        Fin_Separador = 0
        Longitud = 0
        Cadena_Inicio = ""
        Socket_Peticion = ""
        Encabezado = ""
        Datos = ""
        Datos1 = ""
        Trailer = ""
        Separador = ""
        Respuesta = ""


    End Sub

    '''FILE CONFIG
    Private Sub ReadFile()
        Try
            myStreamReader = File.OpenText(FileConfiguracion_CRM)
            Respuesta = myStreamReader.ReadLine()
            Lectura = Respuesta.Split(",")
            numero_Interface = Convert.ToInt32(Lectura(1))
            ' The Read() method returns '-1' when the end of the
            '   file has been reached
            'While myNextInt <> -1
            ' Convert the integer to a unicode Char and add it
            '   to the text box.
            'RServer += ChrW(myNextInt)
            ' Read the next value from the Stream
            'myNextInt = myStreamReader.Read()
            'System.Threading.Thread.CurrentThread.Sleep(100)
            'End While
        Catch exc As Exception
            ' Show the exception to the user.
            Error_Field = "No se puede abrir archivo." + vbCrLf + FileConfiguracion_CRM + vbCrLf + _
            vbCrLf + vbCrLf + "Exception: " + exc.Message
            EventLog.WriteEntry("Service Repositorio: (!ERROR INTERNO!)" + Error_Field)
        Finally
            If Not myStreamReader Is Nothing Then
                myStreamReader.Close()
            End If
        End Try
    End Sub
    '''OLD Y SE USAN
    '''New
End Class
