/* ServerSocket class by sahand(sstiger3000@yahoo.com)
 * I spent a lot of time finding and 
 * solving the bugs to as far as i know
 * this code is bug free.
 * These classes are very easy to use
 * but make sure you take a look at those
 * sample files before you start using or 
 * testing them. */ 

using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text; 
using System.Threading;
using System.Collections;

namespace Irc.Networking
{
	/// <summary>
	/// Provides an easy way of event driven socket programming.
	/// </summary>
	public class ServerSocket
	{
		#region "Nested classes"		
		public class SocketEventArgs	:	EventArgs
		{
			private string _localhost,_remotehost;
			private int _localport,_remoteport;
			public string LocalHost
			{
				get
				{
					return _localhost;
				}
				set
				{
					_localhost=value;
				}
			}
			public int LocalPort
			{
				get
				{
					return _localport;
				}
				set
				{
					_localport=value;
				}
			}
			public string RemoteHost
			{
				get
				{
					return _remotehost;
				}
				set
				{
					_remotehost=value;
				}
			}
			public int RemotePort
			{
				get
				{
					return _remoteport;
				}
				set
				{
					_remoteport=value;
				}
			}
		}
		public class ServerSocketEventArgs	:	SocketEventArgs
		{
			Networking.ClientSocket _socket;
			public Networking.ClientSocket ClientSocket
			{
				get
				{
					return _socket;
				}
				set
				{
					_socket=value;
				}
			}
		}
		
		#endregion

		#region "Events and delegates declaration"				
		public delegate void SocketEventHandler(object sender,SocketEventArgs e);
		public delegate void ServerSocketEventHanlder(object sender,ServerSocketEventArgs e);        
        public event ServerSocketEventHanlder ClientConnectionClosed;        
		public event ServerSocketEventHanlder ClientConnectSuccess;        
		public event EventHandler ConnectionRequest;		
		public event ServerSocketEventHanlder DataArrivalFromClient;		
		#endregion

		#region "Private memeber variables"		
		private bool		_autoaccept;
        private bool		_listening;
		private Thread		_thread=null;
		private Socket		_socket=null;
		private Hashtable	_clients=null;
		#endregion

		#region "Constructor and destructor"		
		public ServerSocket()
		{            
			_thread=new Thread(new ThreadStart(CheckSocket));
			_thread.IsBackground=true;
			_socket=new Socket(AddressFamily.InterNetwork,SocketType.Stream,ProtocolType.Tcp);
			_clients=new Hashtable();
		}
		~ServerSocket()
		{
			Close();
		}
		#endregion

		#region "Properties"		
		public bool AutoAcceptConnectionRequests
		{
			get
			{
				return _autoaccept;
			}
			set
			{
                _autoaccept=value;
			}
		}
		public Socket Client
		{
			get
			{
				return _socket;
			}
		}

			
		public bool Listening
		{
			get
			{
				return _listening;
			}
		}

		
		public string LocalHost
		{
			get
			{		
				try
				{				
					if(_socket==null)
						return Dns.GetHostName();				
					if(_socket.LocalEndPoint==null)
						return Dns.GetHostName();			
					IPEndPoint ep;
					ep=(IPEndPoint)_socket.LocalEndPoint;
					return ep.Address.ToString();
				}
				catch(ObjectDisposedException)
				{
					return "";
				}
			}
		}

		
		public int LocalPort
		{
			get
			{
				try
				{				
					if(_socket==null)
						return 0;
					if(_socket.LocalEndPoint==null)
						return 0;			
					IPEndPoint ep;
					ep=(IPEndPoint)_socket.LocalEndPoint;
					return ep.Port;
				}
				catch(ObjectDisposedException)
				{
					return 0;
				}
			}
		}
		#endregion		

		private void ClientDataArrival(object sender,EventArgs e)
		{
			try
			{
				if(DataArrivalFromClient!=null)
				{
					ServerSocketEventArgs er=new ServerSocketEventArgs() ;
					ClientSocket s=(ClientSocket)sender;
					er.ClientSocket=s;
					er.LocalHost=LocalHost;
					er.LocalPort=LocalPort;
					er.RemoteHost=s.RemoteHost;
					er.RemotePort=s.RemotePort;
					DataArrivalFromClient(s,er);
				}
			}
			catch(Exception exp)
			{
				string Resultado;
				Resultado = exp.Message.ToString();
			}
		}
		private void ClientClosedNoRemove(object sender,EventArgs e)
		{
			try
			{
				if(ClientConnectionClosed!=null)
				{
					ServerSocketEventArgs er=new ServerSocketEventArgs() ;
					ClientSocket s=(ClientSocket)sender;
					er.ClientSocket=s;
					er.LocalHost=LocalHost;
					er.LocalPort=LocalPort;
					er.RemoteHost=s.RemoteHost;
					er.RemotePort=s.RemotePort;
					ClientConnectionClosed(s,er);
				}
			}
			catch(Exception exp)
			{
				string Resultado;
				Resultado = exp.Message.ToString();
			}

		}

		private void ClientClosed(object sender,EventArgs e)
		{
			try
			{
				if(ClientConnectionClosed!=null)
				{
					ServerSocketEventArgs er=new ServerSocketEventArgs() ;
					ClientSocket s=(ClientSocket)sender;
					er.ClientSocket=s;
					er.LocalHost=LocalHost;
					er.LocalPort=LocalPort;
					er.RemoteHost=s.RemoteHost;
					er.RemotePort=s.RemotePort;
					ClientConnectionClosed(s,er);
				}
				object key=null;
				foreach(DictionaryEntry oSocket in _clients)
				{			
					if(object.Equals(oSocket.Value,sender))
					{
						key=oSocket.Key;
					}
				}			
				if(key!=null)
					_clients.Remove(key);
			}
			catch(Exception exp)
			{
				string Resultado;
				Resultado = exp.Message.ToString();
			}

		}

		private void ClientConnected(object sender,EventArgs e)
		{
			try
			{
				if(ClientConnectSuccess!=null)
				{
					ServerSocketEventArgs er=new ServerSocketEventArgs() ;
					ClientSocket s=(ClientSocket)sender;
					er.ClientSocket=s;
					er.LocalHost=LocalHost;
					er.LocalPort=LocalPort;
					er.RemoteHost=s.RemoteHost;
					er.RemotePort=s.RemotePort;
					ClientConnectSuccess(s,er);
				}
			}
			catch(Exception exp)
			{
				string Resultado;
				Resultado = exp.Message.ToString();
			}

		}

		
		private void CheckSocket()
		{
			try
			{
				while(_listening)
				{
					if(_socket.Poll(-1, System.Net.Sockets.SelectMode.SelectRead))
					{
						if(ConnectionRequest!=null)					
							ConnectionRequest(this, new EventArgs());					
						if(_autoaccept)
							Accept();
					}
					else
					{
						Close();
					}
				}
			}
			catch(Exception exp)
			{
				string Resultado;
				Resultado = exp.Message.ToString();
			}

		}

		public ClientSocket Accept(bool HandlerEvents)
		{
			try
			{
				if(HandlerEvents)
				{
					Socket s=_socket.Accept();
					return new ClientSocket(s);
				}		
				else
				{
					Socket s=_socket.Accept();			
					IPEndPoint rep=(IPEndPoint)s.RemoteEndPoint;			
					return AddClientSocket(s,rep.Address.ToString() + " " + rep.Port.ToString());
				}
			}
			catch(Exception exp)
			{
				string Resultado;
				Socket s=_socket.Accept();			
				Resultado = exp.Message.ToString();
				return new ClientSocket(s);
			}

		}

		public ClientSocket Accept()
		{
			try
			{
				if(_listening)
				{
					Socket s=_socket.Accept();
					IPEndPoint rep=(IPEndPoint)s.RemoteEndPoint;			
					return AddClientSocket(s,rep.Address.ToString() + " " + rep.Port.ToString());
				}
				else
					return null;
			}
			catch(Exception exp)
			{
				string Resultado;
				Resultado = exp.Message.ToString();
				return null;
			}

		}

		public ClientSocket Accept(string Key)
		{
			try
			{
				Socket s=_socket.Accept();
				return AddClientSocket(s,Key);			
			}
			catch(Exception exp)
			{
				string Resultado;
				Resultado = exp.Message.ToString();
				return null;
			}
		}

		public ClientSocket AddClientSocket(ClientSocket socket,string key)
		{			
			try
			{
				socket.DataArrival+=new EventHandler(ClientDataArrival);
				socket.Closed+=new EventHandler(ClientClosed);
				socket.ConnectSuccess +=new EventHandler(ClientConnected);
				_clients.Add(key,socket);						
				ClientConnected(socket,new EventArgs());
				return socket;
			}
			catch(Exception exp)
			{
				string Resultado;
				Resultado = exp.Message.ToString();
				return null;
			}

		}

		public ClientSocket AddClientSocket(Socket socket,string Key)
		{
			try
			{
				ClientSocket client=new ClientSocket(socket);
				AddClientSocket(client,Key);
				return client;
			}
			catch(Exception exp)
			{
				string Resultado;
				Resultado = exp.Message.ToString();
				return null;
			}

		}

		public ClientSocket GetClient(string Key)
		{
			try
			{
				if(_clients.ContainsKey(Key))
				{
					return (ClientSocket)((DictionaryEntry)_clients[Key]).Value;
				}
				else
				{
					return null;
				}
			}
			catch(Exception exp)
			{
				string Resultado;
				Resultado = exp.Message.ToString();
				return null;
			}

		}

		public void Listen(int LocalPort)
		{
			try
			{
				IPEndPoint lep=new IPEndPoint(IPAddress.Any,LocalPort);
				Listen(lep);
			}
			catch(Exception exp)
			{
				string Resultado;
				Resultado = exp.Message.ToString();
			}

		}

		public void Listen(string LocalAddress,int Localport)
		{
			try
			{
				IPEndPoint lep=new IPEndPoint(Dns.Resolve(LocalAddress).AddressList[0],Localport);
				Listen(lep);
			}
			catch(Exception exp)
			{
				string Resultado;
				Resultado = exp.Message.ToString();
			}

		}

		public void Listen(IPEndPoint LocalEndPoint)
		{            
			Listen(LocalEndPoint,10);
		}

		public void Listen(IPEndPoint LocalEndPoint,int BackLog)
		{
			try
			{
				_socket.Bind(LocalEndPoint);
				_socket.Listen(BackLog);
				_listening=true;
				_thread.Start();
			}
			catch(Exception exp)
			{
				string Resultado;
				Resultado = exp.Message.ToString();
			}
		}		

		public void Close()
		{
			try
			{

				_listening=false;			
				foreach(DictionaryEntry oSocket in _clients)
				{
					if(oSocket.Value!=null)
					{
						ClientSocket s=(ClientSocket)oSocket.Value;
						s.Close(false);
					}
				}			
				_socket.Close();			
				foreach(DictionaryEntry d in _clients)
					ClientClosedNoRemove(d.Value,new EventArgs());
				_clients=new Hashtable();
				_thread=new Thread(new ThreadStart(CheckSocket));
				_thread.IsBackground=true;
				_socket=new Socket(AddressFamily.InterNetwork,SocketType.Stream,ProtocolType.Tcp);			
			}
			catch(Exception exp)
			{
				string Resultado;
				Resultado = exp.Message.ToString();
			}
		}

		public void SendToAllClients(byte[] data)
		{
			try
			{
				foreach(DictionaryEntry d in _clients)
				{
					if(d.Value is ClientSocket)
					{
						ClientSocket s=(ClientSocket)d.Value;
						if(s.Connected)
						{
							s.SendData(data);
						}
					}
				}
			}
			catch(Exception exp)
			{
				string Resultado;
				Resultado = exp.Message.ToString();
			}
		}

		public void SendToAllClients(string text)
		{
            SendToAllClients(System.Text.Encoding.ASCII.GetBytes(text));
		}

		public void SendToAllClients(string text,Irc.Networking.ClientSocket.TextEncodingTypes encodingtype)
		{
			try 
			{
				switch(encodingtype)
				{
					case Irc.Networking.ClientSocket.TextEncodingTypes.Ascii:
						SendToAllClients(System.Text.Encoding.ASCII.GetBytes(text));
						break;
					case Irc.Networking.ClientSocket.TextEncodingTypes.Unicode:
						SendToAllClients(System.Text.Encoding.Unicode.GetBytes(text));
						break;
					case Irc.Networking.ClientSocket.TextEncodingTypes.Utf7:
						SendToAllClients(System.Text.Encoding.UTF7.GetBytes(text));
						break;
					case Irc.Networking.ClientSocket.TextEncodingTypes.Utf8:
						SendToAllClients(System.Text.Encoding.UTF8.GetBytes(text));
						break;
				}
			}
			catch(Exception exp)
			{
				string Resultado;
				Resultado = exp.Message.ToString();
			}
		}
		public Hashtable GetAllClients()
		{
			return _clients;
		}		
		public void Genera_Pdf(string Archivo_Entrada, string Archivo_Salida, ref bool status_pdf)
		{
			try
			{
				System.IO.StreamReader inFile;
				byte[] bytes;
				string sBase64 = "";
				if (File.Exists(Archivo_Salida) )
				{
					File.Delete(Archivo_Salida);
				}
				inFile = new System.IO.StreamReader(Archivo_Entrada,System.Text.Encoding.ASCII);
				FileStream fs = new FileStream(Archivo_Salida, FileMode.CreateNew, FileAccess.Write);
				sBase64 = inFile.ReadToEnd() ;
				BinaryWriter bw = new BinaryWriter(fs);
				bytes = Convert.FromBase64String(sBase64);
				bw.Write(bytes);
				fs.Close();
				bytes = null;
				bw = null;
				sBase64 = null;
				inFile.Close();
				status_pdf = true;
			}
			catch(Exception exp)
			{
				string Resultado;
				Resultado = exp.Message.ToString();
			}
		}
	}
}