using System.Text;
using System;
using System.Net;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Web.Services;
using System.Web.Services.Description;
using System.Xml.Serialization;
using System.Reflection;
using WsdlReader; 

namespace wsReader
{
    public class Clasews
    {
        private MethodInfo[] methodInfo;
        private ParameterInfo[] param;
        private Type service;
        private Type[] paramTypes;
        private properties myProperty;
        private string MethodName = "";
        string stringUrl;
        string pathNameURL;

        public class Messagerespuesta : Object
        {

            private string fromField;

            private string messageField;
        }

        //Busca Miembro en CRM
        public string busca_numero_miembro(string urlName, string metodoNombre, string programName, string numberMember)
        {
            try
            {
                ////textBox1.Text = "C:\\Utilities\\Presentacion SBUX (CRM)\\Desarrollo\\Programas\\Ejemplos\\Wsdl Reader\\WsdlReader\\bin\\Debug\\ALSE LOY Servicios Miembro.WSDL";
                ////textBox1.Text = "D:\\Utilities\\PROG2005\\ALSE LOY Servicios Miembro.WSDL";
                ////stringUrl = "C:\\Utilities\\Presentacion SBUX (CRM)\\Desarrollo\\Programas\\Ejemplos\\Wsdl Reader\\WsdlReader\\bin\\Debug\\ALSE LOY Servicios Miembro.WSDL";
                ////stringUrl = "D:\\Utilities\\PROG2005\\ALSE LOY Servicios Miembro.WSDL";
                stringUrl = urlName;
                DynamicInvocation_Automatic();
                treeWsdl_AfterSelect_Automatico(metodoNombre);
                treeParameters_AfterSelect_Automatico();
                string result = invokeButton_Automatico(programName, numberMember);
                return result;
            }
            catch (Exception ex)
            {
                ////MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return "ERROR: NO HAY COMUNICACION"; //// +ex.ToString();
            }
        }
        public string invokeButton_Automatico(string nameProgram, string IdMiembro)
        {
            try
            {
                ////treeOutput.Nodes.Clear();
                object[] param1 = new object[param.Length];
                param1[0] = Convert.ChangeType(nameProgram, myProperty.TypeParameter[0]);
                param1[1] = Convert.ChangeType(IdMiembro, myProperty.TypeParameter[1]);
                param1[2] = Convert.ChangeType(null, myProperty.TypeParameter[2]);
                param1[3] = Convert.ChangeType(null, myProperty.TypeParameter[3]);

                foreach (MethodInfo t in methodInfo)
                    if (t.Name == MethodName)
                    {
                        StringWriter valorWs = new StringWriter();
                        //Invoke Method
                        Object obj = Activator.CreateInstance(service);
                        Object response = t.Invoke(obj, param1);
                        //Object response1 = Convert.ChangeType(response, campo);
                        ///
                        ////treeOutput.Nodes.Add(t.Name);
                        ////treeOutput.Nodes[0].Nodes.Add("Result = " + response.ToString());
                        ///
                        Object obj1 = Convert.GetTypeCode(response);
                        Object obj2 = response;
                        Messagerespuesta msg = new Messagerespuesta();
                        //msg = response;
                        ////treeOutput.ExpandAll();
                        //Serializer
                        XmlSerializer xmlSerial = new XmlSerializer(response.GetType());
                        xmlSerial.Serialize(valorWs, response);
                        //String
                        string result = valorWs.ToString();
                        //Close StringWriter
                        valorWs.Close();
                        //MessageBox.Show(result);
                        ////textBox3.Text = result;
                        ////break;
                        return result;
                    }
            }
            catch (Exception ex)
            {
                ////MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return "invokeButton_Automatico (Error): NO HAY COMUNICACION";
            }
            return "invokeButton_Automatico (Final): NO HAY COMUNICACION";
        }
        
        private void DynamicInvocation_Automatic()
        {
            try
            {
                ////messageTextBox.Text += "Generating WSDL \r\n";
                ////progressBar1.PerformStep();
                Uri uri = new Uri(stringUrl);
                ////messageTextBox.Text += "Generating WSDL \r\n";
                ////progressBar1.PerformStep();
                WebRequest webRequest = WebRequest.Create(uri);
                System.IO.Stream requestStream = webRequest.GetResponse().GetResponseStream();
                // Get a WSDL file describing a service
                ServiceDescription sd = ServiceDescription.Read(requestStream);
                string sdName = sd.Services[0].Name;
                // Add in tree view
                ////treeWsdl.Nodes.Add(sdName);
                ////messageTextBox.Text += "Generating Proxy \r\n";
                ////progressBar1.PerformStep();
                // Initialize a service description servImport
                ServiceDescriptionImporter servImport = new ServiceDescriptionImporter();
                servImport.AddServiceDescription(sd, String.Empty, String.Empty);
                servImport.ProtocolName = "Soap";
                servImport.CodeGenerationOptions = CodeGenerationOptions.GenerateProperties;
                ////messageTextBox.Text += "Generating assembly  \r\n";
                ////progressBar1.PerformStep();
                CodeNamespace nameSpace = new CodeNamespace();
                CodeCompileUnit codeCompileUnit = new CodeCompileUnit();
                codeCompileUnit.Namespaces.Add(nameSpace);
                // Set Warnings
                ServiceDescriptionImportWarnings warnings = servImport.Import(nameSpace, codeCompileUnit);
                if (warnings == 0)
                {
                    StringWriter stringWriter = new StringWriter(System.Globalization.CultureInfo.CurrentCulture);
                    Microsoft.CSharp.CSharpCodeProvider prov = new Microsoft.CSharp.CSharpCodeProvider();
                    prov.GenerateCodeFromNamespace(nameSpace, stringWriter, new CodeGeneratorOptions());
                    ////messageTextBox.Text += "Compiling assembly \r\n";
                    ////progressBar1.PerformStep();
                    // Compile the assembly with the appropriate references
                    string[] assemblyReferences = new string[2] { "System.Web.Services.dll", "System.Xml.dll" };
                    CompilerParameters param = new CompilerParameters(assemblyReferences);
                    param.GenerateExecutable = false;
                    param.GenerateInMemory = true;
                    param.TreatWarningsAsErrors = false;
                    param.WarningLevel = 4;
                    CompilerResults results = new CompilerResults(new TempFileCollection());
                    results = prov.CompileAssemblyFromDom(param, codeCompileUnit);
                    Assembly assembly = results.CompiledAssembly;
                    service = assembly.GetType(sdName);
                    ////messageTextBox.Text += "Get Methods of Wsdl \r\n";
                    ////progressBar1.PerformStep();
                    methodInfo = service.GetMethods();
                    foreach (MethodInfo t in methodInfo)
                    {
                        if (t.Name == "Discover")
                            break;
                        ////treeWsdl.Nodes[0].Nodes.Add(t.Name);
                    }
                    ////treeWsdl.Nodes[0].Expand();
                    ////messageTextBox.Text += "Now ready to invoke \r\n ";
                    ////progressBar1.PerformStep();
                    ////this.tabControl1.SelectedTab = this.tabPage1;
                }
                else
                {

                    ////messageTextBox.Text += warnings;
                }
            }
            catch (Exception ex)
            {
                ////messageTextBox.Text += "\r\n" + ex.Message + "\r\n\r\n" + ex.ToString(); ;
                ////progressBar1.Value = 70;
            }
        }
        private void treeWsdl_AfterSelect_Automatico(string metodoNombre)
        {
            try
            {
                ////MethodName = "ConsultaMiembro";
                MethodName = metodoNombre;
                param = methodInfo[0].GetParameters();
                myProperty = new properties(param.Length);
                // Get the Parameters Type
                paramTypes = new Type[param.Length];
                for (int i = 0; i < paramTypes.Length; i++)
                {
                    paramTypes[i] = param[i].ParameterType;
                }
            }
            catch (Exception ex)
            {
                ////MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        private void treeParameters_AfterSelect_Automatico()
        {
            try
            {
                for (int i = 0; i < paramTypes.Length; i++)
                {
                    myProperty.Index = i;
                    myProperty.Type = param[i].ParameterType;
                    ////propertyGrid1.SelectedObject = myProperty;
                }
            }
            catch (Exception ex)
            {
                ////MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void ReadFile(string nombreArchivo)
        {
            StreamReader myStreamReader = null;
            try
            {
                myStreamReader = File.OpenText(nombreArchivo);
                pathNameURL = myStreamReader.ReadLine();
            }
            catch (Exception exc)
            {
                ////MessageBox.Show("File could not be opened or read." + Environment.NewLine + "Please verify that the filename is correct, and that you have read permissions for the desired directory." + Environment.NewLine + Environment.NewLine + "Exception: " + exc.Message);
            }
            finally
            {
                if (myStreamReader != null)
                {
                    myStreamReader.Close();
                }
            }
        }
        //Busca Niveles en CRM
        public string busca_numero_miembro_Recompensas(string urlName, string metodoNombre, string programName, string numberMember)
        {
            try
            {
                stringUrl = urlName;
                DynamicInvocation_Automatic();
                treeWsdl_AfterSelect_Automatico_Recompensas(metodoNombre);
                treeParameters_AfterSelect_Automatico();
                string result = invokeButton_Automatico_Recompensas(programName, numberMember);
                return result;
            }
            catch (Exception ex)
            {
                ////MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return "ERROR: NO HAY COMUNICACION";
            }
        }
        private void treeWsdl_AfterSelect_Automatico_Recompensas(string metodoNombre)
        {
            try
            {
                ////MethodName = "ConsultaMiembro";
                MethodName = metodoNombre;
                param = methodInfo[0].GetParameters();
                myProperty = new properties(param.Length);
                // Get the Parameters Type
                paramTypes = new Type[param.Length];
                for (int i = 0; i < paramTypes.Length; i++)
                {
                    paramTypes[i] = param[i].ParameterType;
                }
            }
            catch (Exception ex)
            {
                ////MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        public string invokeButton_Automatico_Recompensas(string nameProgram, string IdMiembro)
        {
            try
            {
                ////treeOutput.Nodes.Clear();
                object[] param1 = new object[param.Length];
                param1[0] = Convert.ChangeType(nameProgram, myProperty.TypeParameter[0]);
                param1[1] = Convert.ChangeType(IdMiembro, myProperty.TypeParameter[1]);
                param1[2] = Convert.ChangeType(null, myProperty.TypeParameter[2]);
                param1[3] = Convert.ChangeType(null, myProperty.TypeParameter[3]);

                foreach (MethodInfo t in methodInfo)
                    if (t.Name == MethodName)
                    {
                        //Invoke Method
                        Object obj = Activator.CreateInstance(service);
                        Object response = t.Invoke(obj, param1);
                        //Object response1 = Convert.ChangeType(response, campo);
                        ///
                        ////treeOutput.Nodes.Add(t.Name);
                        ////treeOutput.Nodes[0].Nodes.Add("Result = " + response.ToString());
                        ///
                        Object obj1 = Convert.GetTypeCode(response);
                        Object obj2 = response;
                        Object obj11 = Convert.GetTypeCode(param1[2]);
                        Object obj22 = param1[2];
                        object[] param3 = new object[1];
                        param3[0] = param1[2];

                        Messagerespuesta msg = new Messagerespuesta();
                        //msg = response;
                        ////treeOutput.ExpandAll();
                        StringWriter sw = new StringWriter();
                        StringWriter sw2 = new StringWriter();
                        XmlSerializer xmlSerial = new XmlSerializer(response.GetType());
                        xmlSerial.Serialize(sw, response);
                        string result = sw.ToString();

                        string param2 = "";
                        XmlSerializer xmlSerial2 = new XmlSerializer(obj22.GetType());
                        xmlSerial2.Serialize(sw2, obj22);
                        string result2 = sw2.ToString();


                        sw.Close();
                        sw2.Close();
                        //MessageBox.Show(result);
                        ////textBox3.Text = result;
                        ////break;
                        return result2;
                    }
            }
            catch (Exception ex)
            {
                ////MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return "invokeButton_Automatico (Error): NO HAY COMUNICACION|" + ex.ToString();
            }
            return "invokeButton_Automatico (Final): NO HAY COMUNICACION";
        }
        //Cancela en CRM
        public string solicita_ticket_cancelar(string urlName, string metodoNombre, string ticket_spcNumber_void)
        {
            try
            {
                stringUrl = urlName;
                DynamicInvocation_Automatic();
                treeWsdl_AfterSelect_Automatico_Cancelaciones(metodoNombre);
                treeParameters_AfterSelect_Automatico();
                string result = invokeButton_Automatico_Cancelaciones(ticket_spcNumber_void);
                return result;
            }
            catch (Exception ex)
            {
                ////MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return "ERROR: NO HAY COMUNICACION";
            }
        }
        private void treeWsdl_AfterSelect_Automatico_Cancelaciones(string metodoNombre)
        {
            try
            {
                ////MethodName = "Cancelaciones";
                MethodName = metodoNombre;
                param = methodInfo[0].GetParameters();
                myProperty = new properties(param.Length);
                // Get the Parameters Type
                paramTypes = new Type[param.Length];
                for (int i = 0; i < paramTypes.Length; i++)
                {
                    paramTypes[i] = param[i].ParameterType;
                }
            }
            catch (Exception ex)
            {
                ////MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        public string invokeButton_Automatico_Cancelaciones(string Cancelacion)
        {
            try
            {
                ////treeOutput.Nodes.Clear();
                object[] param1 = new object[param.Length];
                param1[0] = Convert.ChangeType(Cancelacion, myProperty.TypeParameter[0]);
                param1[1] = Convert.ChangeType(null, myProperty.TypeParameter[1]);

                foreach (MethodInfo t in methodInfo)
                    if (t.Name == MethodName)
                    {
                        StringWriter valorWs = new StringWriter();
                        //Invoke Method
                        Object obj = Activator.CreateInstance(service);
                        Object response = t.Invoke(obj, param1);
                        //Object response1 = Convert.ChangeType(response, campo);
                        ///
                        ////treeOutput.Nodes.Add(t.Name);
                        ////treeOutput.Nodes[0].Nodes.Add("Result = " + response.ToString());
                        ///
                        Object obj1 = Convert.GetTypeCode(response);
                        Object obj2 = response;
                        Messagerespuesta msg = new Messagerespuesta();
                        //msg = response;
                        ////treeOutput.ExpandAll();
                        //Serializer
                        XmlSerializer xmlSerial = new XmlSerializer(response.GetType());
                        xmlSerial.Serialize(valorWs, response);
                        //String
                        string result = valorWs.ToString();
                        //Close StringWriter
                        valorWs.Close();
                        //MessageBox.Show(result);
                        ////textBox3.Text = result;
                        ////break;

                        StringWriter sw1 = new StringWriter();
                        XmlSerializer xmlSerial1 = new XmlSerializer(param1[1].GetType());
                        xmlSerial1.Serialize(sw1, param1[1]);
                        string result1 = sw1.ToString();
                        sw1.Close();


                        return result1;
                    }
            }
            catch (Exception ex)
            {
                ////MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return "invokeButton_Automatico (Error): NO HAY COMUNICACION";
            }
            return "invokeButton_Automatico (Final): NO HAY COMUNICACION";
        }
    }
}
