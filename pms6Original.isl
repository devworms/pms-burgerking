//Event Inq: 31 ////Consulta de Miembro CRM Wow (Siebel)
//Event Inq: 32 ///Continuacion de consulta de miembro cuando no se ha abierto un cheque
//Event Inq: 33 ///Borra los archivos temporales al iniciar una venta sin  tarjeta WOW 
//Event Inq: 63 ////Consulta de Miembro CRM Wow (Siebel) Pantalla  Old 33
//Event Inq: 35 //Redenciones    Redemption
//Event Inq: 37 //Cancelaciones CRM Devuelve Inventario  Void
//Event Inq: 38 //Cancelaciones CRM NO Devuelve Inventario  Return
//Event Inq: 39 ///Socio Wow Rewards  (SI / NO)  LOCAL
//Event Inq: 40 ///Socio Wow Rewards  (SI / NO)  LLEVAR
//Event Inq: 41      ///Socio Wow Rewards  (SI / NO)  DRIVE THRU  Inicia Cuenta
//Event Inq: 42      ///Cancelacion de Miembro en Cheque
//Event Inq: 43      ///Socio Wow Rewards  (SI / NO)  LOBBY
//Event Inq: 61      ///Consulta Socio CRM
//Event Final_Tender //Acreditaciones Accrual
//Event Pickup_Check   ///Socio Wow Rewards  (SI / NO)  DRIVE THRU  Seguir Cuenta
//Event Srvc_Total: 401
//Event Inq: 30   ///Pause and Re-_
//Event Print_Trailer : Impresion_CRM
//Event TMED     //Redenciones    Redemption   
//Event Inq: 71   //Inicio de Tender Media Pago Puntos
//Event Inq: 72   //Tecla Void Micros 

////// 3857 8983 7605 4
//6060 0860 6058 5511
//Status_Respuesta 
//O = timeout
//P = PROCESADO: NO CONTESTA WS
//T = Contesta que autorizada
//F = Contesta que no autorizada
////////////////////////////////////////////////////
// Programa CRM  Clave 741101
///////////////////////////////////////////////////
//Archivo Off line de Redenciones
//Fecha, Tienda, Ticket, No_Miembro, Monto, Error
///////////////////////////////////////////////////
//Isl error on line 2143
// Windows rows out of range
//SELECT transactiontype,Estatus,Integrationstatus,Status,SubStatus,ticketnumber,amount,* FROM "custom"."LOYTRANSACCION_RESPONSE" order by activitydate,ticketnumber
//SELECT transactiontype,Estatus,Integrationstatus,Status,SubStatus,ticketnumber,amount,* FROM "custom"."LOYTRANSACCION_RESPONSE" order by activitydate,ticketnumber,transactiontype
//************************** [ INSTALACION ] *****************************************************
//Teamviewer 693 688 635 Micros 4.7
//Modificar la busqueda de la tarjeta en el servicio el campo a empezar es SSAPrimaryField='Y'
//Generar un Archivo para validar las lineas que ya se redimieron y no mandarlas a Acreditacion OK
//Sumar el total de condimentos a eliminar en redencines
//ENVIO DE MAIL
//Configrar las teclas de local, llevar drive thru como }
//Se necesita habilitar la opcion de void closed check en class of employees
////////SELECT datediff( day,   '2011-01-01', '2011-05-12' )  as Dias to birthday
//SELECT * FROM custom.LOYTRANSACCION_OFFLINE A
//WHERE 1 =
//(SELECT count() as contador FROM custom.LOYTRANSACCION_OFFLINE B
//WHERE A.membernumber = B.membernumber AND A.ticketnumber = B.ticketnumber 
//group by B.membernumber, B.ticketnumber)
//=======================================OK=======================================================
//ContinueOnCancel
DiscardGlobalVar
UseCompatFormat
UseISLTimeOuts
SetSignOnLeft
//########################################################
//                     ARCHIVOS INCLUIDOS EN EL ISL  
//########################################################
Include "Pms6_SubRutinas.Isl"
Include "Odbc_SubRutinas.Isl"
//########################################################
//=====================================VARIABLES==================================================
Var MontoTtlCheque               : $12 = 0
//21Jul2015
Var pointAmount                  : A10 = ""
//21Jul2015

//15Jul2015
Var Forma_Pago_en_Cheque         : A1  = ""
Var Existe_Aportacion            : A1  = ""
Var Existe_Menu_Item             : A1  = ""
//15Jul2015

//07Ju2015
Var Flag_Socio_Starbucks_Rewards : A1  = ""
Var Prefijo_Aportaciones         : A3  = "AP-"
Var Total_Aportaciones_Wow       : $10 = 0
//07Ju2015

//07Jun2015
Var Linea_Encontrado_D  : N3  = 0
Var Contador_Descto     : N3  = 0
Var Si_Existe_Descuento : A1  = ""
Var Snivel_Descto       : A4  = "NA"
Var Cheque_Descto       : N4  = @CKNUM
Var Busca_Descto        : N8  = 0
Var Objeto_Descto[10]   : N8
Var Nivel_Descto[10]    : N8
Var SubNivel_Descto[10] : N8
Var Precio_Descto[10]   : $10
Var Nombre_Descto[10]   : A20
Var Unidades_Descto[10] : N6
Var Linea_Descto[10]    : N3
//07Jun2015


Var usuario_Dba                 : A20   = "dba"
Var password_Dba                : A20   = ""
Var Wsid_wow                    : N6    = @WSID
Var Tarjeta_wow                 : A20   = ""
Var Cupon_Desc_P1               : N6    = 0
Var Cupon_Desc_P2               : N6    = 0
Var Descuento_Valido            : $10   = 0
Var Cupon_Desc_MM               : N6    = 0
Var numero_Pos                  : N6    = 0
Var accion                      : A20   = ""
Var datos_wsid                  : A100  = ""
//21May2015
Var Ttl_Calculo                   : $10   = 0
Var Descuento_aplicar             : $10   = 0
//21May2015

//08Abr2015
Var Tipo_Sub_Descuento            : A1    = ""
//08Abr2015

//07Abr2015
Var Item_Redondeo                 : N8    = 0
//07Abr2015

//COMBOS
Var v_business_date               : A30   = ""
Var v_mi_seq                      : A10   = ""
Var v_obj_num                     : A10   = ""
Var v_status                      : A10   = "I"
Var v_mi_seq_cmb                  : A10   = "1"
Var SqlStrA                       : A5000 = ""
var v_menu_lvl                    : N1
var v_qty                         : N5
//COMBOS

Var Objeto_Buscar                 : N8    = 0
Var No_Producto_Promocional       : N8    = 0
Var CC_Promocional                : N3    = 0
Var Producto_Promocional[100]     : N8
Var Points_Cheque_A               : A10   = ""  //04Feb2015
Var Points_Cheque                 : $10   = 0   //04Feb2015
Var Flag_Final_Tender             : A2    = ""
Var AdjustedListPrice             : A50   = ""  //26Ene2015
Var Opcion_Redencion              : N2    = 0   //26Ene2015
Var Total_MenuItem                : $10   = 0
Var Flag_Busca_Donacion           : A1    = ""
Var Total_Aportaciones            : $10   = 0
Var Name_FP                       : A90   = ""
Var Tender_Efectivo_Ttl           : $10   = 0
Var Tender_Efectivo_No            : N6    = 0
Var Flag_Descuento_Wow_Void       : N1    = 0
Var Flag_FPago_Wow_Void           : N1    = 0
Var Nombre_Forma_Pago_Ws          : A20   = ""
Var No_Formas_Pago                : N2    = 0
Var Numero_F                      : N6    = 0
Var Cuenta_F                      : A10   = ""
Var Facturacion_Parcial           : N1    = 0
Var Facturacion_Parcial_Real      : N1    = 0
Var Facturacion_Parcial_Original  : N1    = 0
Var Monto_Original                : $10   = 0
Var Max_FPago                     : N3    = 100
Var FPago_Status_Facturable[Max_FPago] : N1
Var Suma_Total_Facturar_I : $12   = 0
Var Suma_Total_Facturar   : $12   = 0
Var Suma_Total_No_Factura : $10   = 0
Var hODBCDLL_CFD          : N12   = 0
Var Descuento_RedondeoWOW : N6  = 0
Var Donaciones[10]      : N6
Var Flag_Donacion_WOW   : N1    = 0
Var Flag_Descuento_WOW  : N1    = 0
Var PtsAniv_monto       : A10   = 0
Var PtsAniv_ProductName : A99   = ""
Var Name_Cajero         : A100  = ""
Var Tender_Crm          : N6    = 0
Var Resp_Red[5]         : A50
Var ErrorCode           : A50   = ""
Var emp_bo_class_seq    : N6    = 0
Var Id_Cheque           : A30   = ""
Var Numero_Tarjeta      : A30   = ""
Var Numero_Tarjeta_O    : A30   = ""
Var Datos_1             : A5000 = ""

Var Descuento_Calculado : $12   = 0
Var Descuento_P_Cal     : $10  = 0
Var Miembro_2[2]        : A100
Var Arma_Cadena_82      : A1000 = ""
Var Arma_Cadena_2       : A1000 = ""
Var Tienda_Capturada    : N6    = 0
Var ID                  : A50   = ""
Var ID_WS               : A50   = ""
Var Salir_ACR           : A1    = ""
Var Drive_Pos_ready     : A100  = "\micros\res\pos\etc\"
Var Drive_Pos_Win_Ce    : A100  = "\CF\micros\etc\"
Var Condimento_Aplicado : A1    = ""
Var Valor_Item_Redimido : $12   = 0
Var Marcado_Crm         : A1    = ""
Var Ticket_Unico        : A20   = ""
Var Fecha_a_Convertir   : A20   = ""
Var Fecha_Juliana_M     : A20   = ""
Var Fecha_Numerica      : N6    = 0
Var Saldo_Ws            : A30   = ""
Var Numero_Tarjeta__O   : A50   = ""
Var FileMiembro_O       : A40   = "Miembro_O.TxT"
Var Nombre_Miembro_O    : A50   = ""
Var path_DeleteFile     : A50   = ""
Var status_DeleteFile   : A50   = ""
Var FileDelete          : A50   = ""
Var Path_Pos_ready      : A100  = ""  ///20Nov2014 "c:\micros\res\pos\etc\"
Var Path_Pos_Win_Ce     : A100  = "\\CF\\micros\\etc\\"
Var Cheque_PR           : N6    = 0


//Nuevas
var FileMiembroSolo             : A40 = "Miembro_Solo.TxT"
Var ALL                         : A4  = ""
Var Borrar                      : A1  = ""
Var Flag_Copiar                 : A1  = ""
Var Consulta_Pantalla           : A1  = ""
Var Error_Time_Out_SbuxCard     : A2  = ""
Var TicketNumber                : A50 = ""
Var TicketNumber_Off            : A50 = ""
Var Flag_Off_Line               : A1  = ""
Var G                           : A20 = ""
Var Test_Descto                 : N5  = 142
Var Test_Monto                  : $10 = 0
Var Salida_Consulta_Clientes    : A1   = ""
Var Lineas_Acreditas_Contador   : N2   = 0
Var IntegrationStatus_M         : A500 = ""
Var Status_M                    : A50  = ""	
Var SubStatus_M                 : A50  = ""
Var Estatus_M                   : A50  = ""
Var IntegrationStatus           : A500 = ""
Var Status                      : A50  = ""	
Var SubStatus                   : A50  = ""
Var Estatus                     : A50  = ""
//Nuevas
Var CC01                        : N3    = 0
Var Errores_Mostrar[12]         : A99
Var Leidos_Consulta             : N3    = 0
Var Sim4                        : A5    = ""
Var Fecha_Cumplanios_A          : A15   = ""
Var Fecha_Cumplanios_D          : A15   = ""
Var Diasparacumpleanios         : N4    = 0
Var Clave_ID                    : A40   = ""
Var Clave_ID_TrackI             : A200  = ""
Var Clave_ID_Paso1              : A40   = ""
Var Clave_ID_Paso2              : A40   = ""
Var Clave_ID_TrackII_Origin		: A20 //Test 10/08/2015
Var Clave_ID_TrackII			: A20 //Test 10/08/2015
Var Clave_Id_Track_II[5]        : A40
Var Clave_Id_Track_II_1[5]      : A40
Var Campo_Busqueda              : A99   = ""
Var Saldo                       : A30   = ""
Var Manda                       : A2000 = ""
Var Respuesta                   : A5000 = ""
Var Separador                   : A1    = "|"
Var Separador_Campos            : A1    = "^"
Var Contador_InfoLines_A        : N3    = 0 
Var Nombre                      : A100  = ""
Var NumeroMiembro               : A100  = ""
Var NumeroMiembro_PR            : A100  = ""
Var NumeroMiembroSolo           : A100  = ""
Var Activo                      : A30   = ""
Var Nivel_Price[10]             : A10
    Nivel_Price[1]                      = "Kids"
    Nivel_Price[2]                      = "Chico"
    Nivel_Price[3]                      = "Mediano"
    Nivel_Price[4]                      = "Grande"
    Nivel_Price[5]                      = "King"
    Nivel_Price[6]                      = "NA"
    Nivel_Price[7]                      = "NA"
    Nivel_Price[8]                      = "NA"
    Nivel_Price[9]                      = "NA"
    Nivel_Price[10]                     = "NA"
Var Nivel                       : A20   = ""
Var Nivel_Id                    : A20   = ""
Var Nivel_nombre                : A50   = ""
Var Organizacion                : A10   = ""
Var FavoriteProduct             : A100  = ""
Var LastBeverage                : A100  = ""
Var LastFood                    : A100  = ""
Var AttributeValue              : A10   = ""

Var PromocionId[30]             : A10
Var PromocionNombre[30]         : A100
Var PromocionStatus[30]         : A24
Var PromocionUniqueKey[30]      : A24
Var PromocionActive[30]         : A24
Var PromocionDisplayName[30]    : A24
 
Var VoucherCalStatus[30]        : A34
Var VoucherProductId[30]        : A34
Var VoucherProductName[30]      : A70
Var VoucherStatus[30]           : A34
Var VoucherTransactionId[30]    : A34
Var validDiscountAmount[30]     : A34
Var validDiscountType[30]       : A34
Var minimumAmount[30]           : A20
 
Var Privilegios[30]             : A100
Var InfoLines_Save[30]          : A40

Var Version_Micros_A            : A4    = ""
Var Version_Micros              : N2    = 0
Var hODBCDLL                    : N12   = 0
Var hODBCDLL_File               : N12   = 0
Var Campo                       : A5000 = ""
Var Campo_                      : A100  = ""
Var SqlStr                      : A3000 = ""
Var SqlStr1                     : A3000 = ""
Var Business_Date               : A30   = ""
Var Business_Date_Real          : A40   = ""
Var Tienda                      : N6    = 0
Var DiaB                        : A2    = ""
Var MesB                        : A2    = ""
Var AnioB                       : A4    = ""
Var Fecha                       : A12   = ""
Var Fecha1                      : A30   = ""
Var Contador_Campos             : N3    = 0
Var Contador_Voucher            : N3    = 0
Var I                           : N3    = 0
Var II                          : N4    = 0
Var Cheque                      : N4    = 0
Var BirthDate                   : A30   = ""
Var DiaBirthDate_A              : A2    = ""
Var DiaBirthDate                : N2    = 0
Var MesBirthDate_A              : A2    = ""
Var MesBirthDate                : N2    = 0
Var AnioBirthDate               : N4    = 0
Var AnioBirthDate_A             : A4    = ""
Var Numero_Touchscreen          : N6    = 0
Var Numero_TouchscreenN         : N6    = 0
Var Numero_TouchscreenNueva     : N6    = 0
Var FileTemporal                : A40   = "Temporal.TxT"
Var File                        : A40   = "Configuracion_CRM.TxT"
Var FileCatalogoErrores         : A40   = "Catalogo_Errores_CRM.TxT"
Var FileLineaRedChq             : A40   = "LineaRedChq.TxT"
Var FileMiembro                 : A40   = "Miembro.TxT"
Var FileMiembro_Soa             : A40   = "Miembro_Soa.TxT"
Var FileMiembro_Pausa_R_Cheque  : A40   = "Miembro_PR_Cheque.TxT"
Var FileMiembro_Pausa_R         : A40   = "Miembro_PR.TxT"
Var FileMiembroServicio         : A40   = "MiembroServicio.TxT"
Var FileNivel                   : A40   = "MiembroNivel.TxT"
Var FileNivelServicio           : A40   = "MiembroNivelServicio.TxT"
Var MiembroCheckInfoServicio    : A40   = "MiembroCheckInfoServicio.TxT"
Var FileNivel_Miembro           : A40   = "MiembroNivelMiembro.TxT"
Var FileNivel_MiembroServicio   : A40   = "MiembroNivelMiembroServicio.TxT"
Var FileConsultaSaldo           : A40   = "Consulta_Saldo.TxT"
Var FileConsultaSaldoServicio   : A40   = "Consulta_SaldoServicio.TxT"
Var TicketCancelar              : A40   = "Ticket_Cancelar.TxT"
Var FileLeyendaCRM_Cheque       : A40   = "LeyendaCRMCheque.TxT"
Var FileLeyendaCRM              : A40   = "LeyendaCRM.TxT"
Var FileMiembroScreen           : A40   = "MiembroScreenCRM.TxT"
Var FileErrorMostrar            : A40   = "Catalogo_Errores_CRM.TxT"
Var FileOff_Line_CRM            : A40   = "Off_Line_CRM.TxT"
Var ChequeContinuar             : A40   = "ChequeContinuar.TxT"
Var Nombre_Del_Programa         : A100  = ""
Var PartnerName                 : A100  = ""
Var Procesado                   : N1    = 0
Var Estatus_A_R                 : A50   = ""
Var Reden_Acred_Aprobada        : A10   = "Success"
Var Reden_Acred_Aprobada_Success: A10   = "Success"
Var Reden_Acred_Aprobada_Status : A10   = "Processed"
Var Reden_Acred_Aprobada_Queued : A10   = "Queued"
Var Reden_Acred_Aprobada_SubStat: A10   = "Success"
Var Name_Jar                    : A20   = ""
Var Descuento_Redenciones_P     : N6    = 0
Var Descuento_Redenciones_M     : N6    = 0
Var Descuento_Aplicado          : A1    = ""
Var Descuento_Aplicado_Cheque   : A1    = ""
Var Forma_Pago_Sbux             : A1    = ""
Var Miembro_Existe              : A1    = ""
Var FP_Sbux_Aplicado            : A1    = ""
Var FP_Sbux_Aplicado_Off_Line   : A1    = ""
Var FP_Aplicada_Otra            : A1    = ""
Var FormaPagoSbux               : N6    = 0
Var FormaPagoSbuxOffLine        : N6    = 0
Var Unid_Seleccionado           : N10   = 0
Var Item_No_Seleccionado        : N10   = 0
Var Item_Nombre_Seleccionado    : A20   = ""
Var Item_Precio_Seleccionado    : $10   = 0
Var Item_Precio_Seleccionado_Con: $10   = 0
Var Item_Qty_Seleccionado       : N4    = 0
Var Item_Linea_Seleccionado     : N3    = 0
Var Contador_Redenciones        : N3    = 0
Var Total_Fpago_Cheque          : $10   = 0
Var Condimentos                 : N3    = 0
Var MenuItems                   : N3    = 0
Var Lineas_Leidas_Redimidas     : N3    = 0
Var Cheque_Leido                : N4    = 0
Var Linea_Redimida_Encontrada   : A1    = ""
Var Mensaje_WS_Windows          : A100  = "NO HAY COMUNICACION CON EL [SERVICIO WINDOWS CRM]"
Var Mensaje_WS                  : A100  = "NO HAY COMUNICACION CON EL [WS CRM]"
Var Ticket_Cancelar_Cheque      : N4    = 0
Var Ticket_Cancelar_Numero      : A20   = ""
Var Contador_respuesta          : N3    = 0
Var Contador_respuesta_R        : N3    = 0
Var Ticket_Unico_Armado         : A20   = ""
Var Flag_Salir                  : A1    = ""
Var Flag_Socio_Wow_Rewards      : A1    = ""
Var Lineas_Leyenda_Crm          : N3    = 0
Var Lineas_Leyenda_Crm_Cheque   : N3    = 0
Var Leyenda_Leida_Crm           : A45   = ""
Var Leyenda_Leida_Crm_L[20]     : A45
Var Contador_respuesta_Nivel    : N3    = 0
Var Linea_Cheque                : N3    = 0
Var Tipo_Descuento              : A1    = ""
Var Tipo_Descuento_Valor_P      : $10   = 0
Var Tipo_Descuento_Valor_M      : $10   = 0
Var Tipo_Descuento_Valor_T      : N6    = 0
Var Tipo_Descuento_All          : N6    = 0
Var LifetimePoint1Value         : N6    = 0
Var LifetimePoint2Value         : N6    = 0
//Var Status                      : A20   = ""
Var Arma_Cadena_22              : A2000 = ""
Var CC_Leyenda                  : N2    = 0
Var CC_Donaciones               : N2    = 0
Var Contador_Lineas_Pantalla    : N2    = 0
Var KeyPress                    : Key
Var Data                        : A20
Var Linea_Redimida[100]         : N3
Var Redencion_Aplicada[12]      : A20
Var Datos_Enviar_WS[100]        : A100
Var Respuesta_WS[100]           : A100
Var Unidades_Void[100]          : N3
Var Articulo_ObjNum_Void[100]   : A10
Var Leyenda_CRM[15]             : A45
Var Leyenda_CRM_Cheque[15]      : A45
Var Nivel_Name_A[200]            : A50
Var RewardName_A[200]            : A50
Var Description_A[200]           : N3
Var RewardTypeDiscount_P_M_A[200]: A50
Var RewardPoints[200]            : N3

//===DevWorms - JMG
Var No_Tarjeta_WOW               : A30 //prueba de variable No_Tarjeta_WOW 
Var No_Tarjeta_WOW_anterior      : A30
Var LeyendaCRM_WOW[10]           : A31

Var Miembro_O_Archivo            : A99
Var rspst_Miembro                : A5000 
Var Cheque_Miembro               : N4    
Var rspst_Miembro_Soa            : A5000 
Var Cheque_Miembro_Soa           : N4
Var rspst_Miembro_PR             : A5000 
Var Cheque_Miembro_PR            : N4
Var rspst_Miembro_Servicio       : A5000 
Var Cheque_Miembro_Servicio      : N4

Var Miembro_Pausa_R_Cheque       : N6

Var Redencion_Aplicada_MNM[12]   : A20
Var Contador_Redenciones_MNM     : N3

Var FiloeMiembroScreenAppend[10] : A30

Var FileOff_Line_CRM_string      : A99

Var TicketCancelarNum            : A20

Var Leyenda_CRM_Cheque_Arreglo[15] : A45

Var Chq[100]                     : N4
Var Linea_Red[100]               : N3

Var MiembroChkInfoServ[100]      : A45

Var Cheque_ChqContinuar          : N4

var infoSocio :N1  //test 10/08/2015
var offline :N1  //test 10/08/2015
var puntosWOW_Global : $20 //test 10/08/2015
var metodo_entrada : a20 //test 10/08/2015
var entra_puntos_primera_vez : n1 //test 10/08/2015
Var puntosActuales	 : $10  //tEST 10/08/2015
 VAR minicostosuma: N20 //tEST 10/08/2015
 var banderamini: N2 //tEST 10/08/2015
 var banderaentro: N2 //tEST 10/08/2015
 var banderainfowow : N2 //tEST 10/08/2015
 var puntoswow: a20 //tEST 10/08/2015
 

 
//=======================================OK=======================================================
//=======================================OK=======================================================
Sub Rutina_Principal_Busca_Miembro
    
    ClearArray InfoLines_Save
    Call Muestra_datos_consulta_pantalla
    Call Lee_Temporal_File
    
    If Len(Campo_Busqueda) = 0 Or Leidos_Consulta < 2
       Call Lee_Configuracion_CRM
       //17Jun2013
       Call Lee_Miembro_PR
       //WaitForEnter Len(Trim(NumeroMiembro_PR))
       //WaitForEnter NumeroMiembro_PR
	   
	   
	   
       If Len(Trim(NumeroMiembro_PR)) > 0
       
          If Mid(NumeroMiembro_PR,1,9) = "PROCESADO"  //DEBUG
             //WaitForEnter NumeroMiembro_PR
          endif
                 
          Campo_Busqueda = NumeroMiembro_PR

       Else
       //17Jun2013
     
	      Call Lee_Tarjeta_Magnetica_Wow_Card
	
       EndIf  //17Jun2013
    EndIf
    Call Consulta_Business_Date_SyBase
    Call Manda_Cliente_Nombre
	
	Call Valida_Respuesta
	
    if offline=1
		
		ExitContinue
	endif
		If   Salida_Consulta_Clientes = "T" //tEST 10/08/2015
			ExitContinue // comentado el 16/12/2015 JMG 
			  
		EndIf
    Call Separa_Campos
	
	//08Ene2015 WaitForEnter Status
    
    If Trim(Status) <> "Active" //13Nov2014 or Trim(Activo) <> "Active"
       Format Campo_VR as "MIEMBRO BK[", Trim(Clave_ID), "]  NO ACTIVO   [", Trim(Activo), "][", Trim(Status), "]"
	   ExitWithError Campo_VR
    EndIf
	
	Format No_Tarjeta_WOW_anterior as No_Tarjeta_WOW // <======= 10/12/2015 JMG
	
	//infomessage "Aviso", "Fin de la rutina principal"
	
EndSub

Event Inq: 31 ////Consulta de Miembro CRM Wow (Siebel)
      
	
	  Var Campo_VR  : A200 = ""
      call Lee_Configuracion_CRM
	  Call Verifica_Descuento_Tmed_Wow
      //08Ene2015 WaitForEnter "Inq: 31"
	
			
	
     
      Call Rutina_Principal_Busca_Miembro
	  
	  
      If @CKNUM = 0
      
         //08Ene2015 WaitForEnter "Inq: 31 01"
         
         Call Graba_Miembro
         
         //08Ene2015 WaitForEnter "Inq: 31 01 01"
         
         //17Dci2014
         //17Dci2014 Format Datos_1 as @CKNUM, Respuesta, Contador_Redenciones, Redencion_Aplicada[1], Redencion_Aplicada[2], Redencion_Aplicada[3], Redencion_Aplicada[4], Redencion_Aplicada[5], Redencion_Aplicada[6], Redencion_Aplicada[7], Redencion_Aplicada[8], Redencion_Aplicada[9], Redencion_Aplicada[10]
         //17Dci2014 Format Marcado_Crm As "1"
         //17Dci2014 Call Inserta_Crm_users( Numero_Tarjeta, Saldo, Marcado_Crm, Datos_1)
         //17Dci2014
         
         //WaitForEnter "31"
         
         LoadKyBdMacro Key(1, 327681), MakeKeys(@TREMP), Key(1,65549), MakeKeys("0"), Key(1,65549)
         Call Continuacion(16, 32)
      Else
      	
      	//08Ene2015 WaitForEnter "Inq: 31 02"
      	infoSocio = 0
      	 Call Graba_Miembro
      	
      	//08Ene2015 WaitForEnter "Inq: 31 02 01" 
      	
      	 //17Dci2014
      	 //17Dci2014 Format Datos_1 as @CKNUM, Respuesta, Contador_Redenciones, Redencion_Aplicada[1], Redencion_Aplicada[2], Redencion_Aplicada[3], Redencion_Aplicada[4], Redencion_Aplicada[5], Redencion_Aplicada[6], Redencion_Aplicada[7], Redencion_Aplicada[8], Redencion_Aplicada[9], Redencion_Aplicada[10]
         //17Dci2014 Format Marcado_Crm As "1"
         //17Dci2014 Call Inserta_Crm_users( Ref Numero_Tarjeta_, Ref Saldo_, Marcado_Crm, Datos_1)
         //17Dci2014
         
         Call Carga_Miembro_Cheque
         
         //08Ene2015 WaitForEnter "Inq: 31 03"
         
         //08Ene2015 If @RVC = 2
            //08Ene2015 LoadKyBdMacro Key (19,102)
         //08Ene2015 EndIf
         //08Ene2015 Call Verifica_PR_Consulta
      ENDIF
      
EndEvent

Sub Verifica_PR_Consulta
    //04Jun2013
    If Len(Trim(NumeroMiembro_PR)) > 0
       //waitForenter "Borra PR"
       Call Borra_Miembro_PR
       LoadKyBdMacro Key (17,201) //SLU
    EndIf
    //04Jun2013
EndSub

Event Inq: 32  ///Continuacion de consulta de miembro cuando no se ha abierto un cheque
      //waitforenter "32"
      //WaitForEnter "Inq: 32"
      Call Lee_Miembro_ws
      Call Carga_Miembro_Cheque
      //Call Verifica_PR_Consulta
EndEvent

Event Inq:33 // Funcion de borrar datos al comenzar una venta sin tarjeta ligada
      //Se agrega funcion para mostrar mensaje POP-UP 08-11-16
	  Call msgbox_BK
      Call Rutina_Borra_Archivos_Temporales      // 4 FEB 2016 <======= JMG  
      Call Rutina_Borra_Archivos_Temporales_Dll
	  Call Rutina_Borra_Archivos_Temporales_Wow 
      Call Borra_FileMiembro_Soa
	  //infomessage "Todos los archivos temporales han sido borrados"
EndEvent


Event Inq: 35 //Redenciones    Redemption

	 Var i_count_disc : N4 = 0
	For i_count_disc = 1 to @numdtlt

		if mid(@dtl_name[i_count_disc],1,8) = "15% Desc" or \\
		   mid(@dtl_name[i_count_disc],1,8) = "10% Desc"
			ExitWithError "DESCUENTO YA APLICADO EN CHEQUE" 
		endif
	endfor

      Var MemberNumber       : A100 = ""
      
      Var ALSELevelPrice     : A50  = ""
      Var BranchDivision     : A50  = ""
      Var ALSEPaymnetMode    : A50  = ""
      Var ActivityDate       : A50  = ""
      
      Var Amount             : A50  = ""	
      Var Comments           : A50  = ""
      Var ItemNumber         : A50  = ""
      Var LocationName       : A50  = ""
      Var PartnerName        : A50  = ""
      Var PointName          : A50  = ""
      Var Points             : A50  = ""	
      Var ProcessDate        : A50  = ""
      Var ProcessingComment  : A50  = ""
      Var ProcessingLog      : A50  = ""
      Var ProductName        : A50  = ""
      Var Quantity           : A50  = ""
      //New17Nov Var IntegrationStatus  : A500 = ""
      //New17Nov Var Status             : A50  = ""	
      //New17Nov Var SubStatus          : A50  = ""
      Var TransactionChannel : A50  = ""
      Var TransactionDate    : A50  = ""
      Var TransactionSubType : A50  = ""
      Var TransactionType    : A50  = ""
      Var VoucherNumber      : A50  = ""
      Var VoucherQty         : A50  = "1"
      Var VoucherType        : A50  = ""
      Var Organization       : A50  = ""
      //New17Nov Var Estatus            : A50  = ""
      Var Bussiness_date     : A50  = ""
      Var Centroconsumo      : A50  = ""
      Var Cheque             : A50  = ""
      Var Transmitido        : A50  = ""
      Var Arma_Cadena        : A2000 = ""
      Var Status_Respuesta   : A1   = ""
      //Var Fecha_Juliana_M    : A20  = ""
      //Var Fecha_a_Convertir  : A20  = ""
      //Var Fecha_Numerica     : N6   = 0
      Var CC                 : N2   = 0
      Var Campo                   : A78  = ""
      Var Sub_Nivel_Seleccionado  : N3   = ""
      Var Main_Nivel_Seleccionado : N1   = 0
      Var Grupo                   : A1   = ""
      Var Grupo_Numerico          : N2   = 0
      Var Tecla_Descuento         : Key
      Var Numerico                : N6   = 0
      //26Ene2015 Var Opcion_Redencion        : N2   = 0
      Var Cheque_Red              : N4   = @CKNUM
      Var SNivel                  : A5   = ""
     
      //15Jul2015|
      Call Busca_Forma_en_Cheque
      If Forma_Pago_en_Cheque = "T"
         InfoMessage "Redencion", "NO SE PERMITE REDENCION, CON FORMAS DE PAGO"
         ExitCancel
      EndIf
      //15Jul2015
      
      Prompt "Espere ...1"
      //27Ene2015 Call Verifica_Item_Seleccionado
      Call Lee_Configuracion_CRM
      Call Lee_Miembro
      //Call Lee_Privilegios_Nivel
      

//     NumeroMiembro = "6085470565857488"

      If Len(Trim(NumeroMiembro)) = 0
         ExitWithError "NO EXISTE MIEMBRO DE CRM PARA ESTE CHEQUE"
      Else
      EndIf
      
      //Call Verifica_Descuento_Aplicado_Cheque
      //If Descuento_Aplicado_Cheque = "T"
		//ExitWithError "DESCUENTO YA APLICADO EN CHEQUE"
      //EndIf

Prompt "Espere ...2"
      
      Call Consulta_Business_Date_SyBase
      Call Lee_Privilegios_Nivel_Miembro
      Call Escoge_Voucher_Redencion 
      
      If Contador_Voucher = 0
         ExitCancel  //27Dic2011 
      EndIf
      
Prompt "Espere ...3"

      //Call Lee_Busca_Privilegios_Nivel

      If Tipo_Descuento = "T"
         If LifetimePoint2Value < Tipo_Descuento_Valor_T
            ExitWithError "NO TIENE PUNTOS SUFICIENTES"
         EndIf
      EndIf
      
      Format Ticket_Unico      as @WSID{01}, @CKNUM{04}, Mid(@Chk_Open_Time,10,2), Mid(@Chk_Open_Time,13,2)
      Format Fecha_a_Convertir as "20", Mid(@Chk_Open_Time,7,2), "-", Mid(@Chk_Open_Time,1,2), "-",Mid(@Chk_Open_Time,4,2)

      Call Consulta_Fecha_Juliana_Micros(Fecha_a_Convertir)      

      //27Ene2015 Format SNivel            as Nivel_Price[Sub_Nivel_Seleccionado]
      Format TicketNumber as Fecha_Numerica{04}, Tienda{05}, Ticket_Unico

Prompt "Espere ...4"

      //27Ene2015 If Item_Qty_Seleccionado <> 1
         //27Ene2015 InfoMessage "WOW CRM", "SOLO SE PERMITE UN ARTICULO PARA REDIMIR A LA VEZ"
         //27Ene2015 ExitCancel
      //27Ene2015 EndIf

      //Tienda = 38101  //QUITAR //QUITAR //QUITAR //QUITAR
      
      //WaitForEnter VoucherProductName[Contador_Voucher]  //50 Pts Aniv
      //16Ene2015 WaitForEnter "Redencion"  //OK
      //16Ene2015 WaitForEnter VoucherProductName[Opcion_Redencion]  //50 Pts Aniv

      If Item_Qty_Seleccionado = 0
         Item_Qty_Seleccionado = 1
      EndIf
      //WaitForEnter Trim(VoucherProductName[Opcion_Redencion])
      //26Ago2015
      Call Verifica_Monto_Total_Cheque
      
      //WaitForEnter Opcion_Redencion
      //WaitForEnter minimumAmount[Opcion_Redencion]
      //WaitForEnter MontoTtlCheque
	  if banderamini=1 and banderaentro=0
		minicostosuma= minicostosuma+minimumAmount[Opcion_Redencion]
	  	banderaentro=1
	  elseif banderamini=0 and banderaentro=0
		minicostosuma=minimumAmount[Opcion_Redencion]
	  
	  endif
      If MontoTtlCheque >= minicostosuma
         //Continua...
      ElseIf Len(Trim(minimumAmount[Opcion_Redencion])) = 0
         //Continua...
      Else
         InfoMessage "NO APLICA ESTA REDENCION A ESTE CHEQUE","Monto Minimo Requerido $" ,minicostosuma
	 
         ExitCancel
      EndIf
      //ExitCancel
      //26Ago2015
      If Trim(VoucherProductName[Opcion_Redencion]) = "100pts Aniv"
         //WaitForEnter "Cupon 50 Aniv"  //OK
         //If @TTLDUE < 100 //Test 10/08/2015
           // ExitWithError "MONTO MINIMO DE 100.00 PARA REDIMIR ESTE CUPON"
         //EndIf
		 
		 PtsAniv_monto         = validDiscountAmount[Opcion_Redencion]
         PtsAniv_monto         = "50"
         PtsAniv_ProductName   = "WOW Ticket"
         Item_Qty_Seleccionado = 0
         SNivel                = "NA"
         AdjustedListPrice     = "50"  //08Abr2015
         ItemNumber            = 0
		
      ElseIf Trim(VoucherProductName[Opcion_Redencion]) = "Cono Cumple"
         
		 Call Verifica_Item_Seleccionado //Test 10/08/2015
		 //and  MID(Item_Nombre_Seleccionado,1,4)= "CONO"  //Test 10/08/2015
		If @TTLDUE - Item_Precio_Seleccionado < minimumAmount[Opcion_Redencion] and Item_No_Seleccionado = 8001 //Test 10/08/2015
           		ExitWithError "EL MONTOMINIMO DEBE DE SER SIN EL ARTICULO PARA REDIMIR ESTE CUPON"
         	EndIf

         //27Ene2015
         
         If Item_Qty_Seleccionado <> 1
            InfoMessage "WOW CRM", "SOLO SE PERMITE UN ARTICULO PARA REDIMIR A LA VEZ"
            ExitCancel
         EndIf
         Format SNivel            as Nivel_Price[Sub_Nivel_Seleccionado]
         //27Ene2015
         
         PtsAniv_monto         = Item_Precio_Seleccionado
      	 PtsAniv_ProductName   = Item_No_Seleccionado
      	 AdjustedListPrice     = Item_Precio_Seleccionado
      	 Item_Qty_Seleccionado = 0
      	 
      //21May2015
      ElseIf Trim(VoucherProductName[Opcion_Redencion]) = "BK 10% off"
         Call Verifica_Item_Seleccionado
		 if Item_Qty_Seleccionado <> 1
            InfoMessage "WOW CRM", "SOLO SE PERMITE UN ARTICULO PARA REDIMIR A LA VEZ"
            ExitCancel
         EndIf

         //WaitForEnter Trim(VoucherProductName[Opcion_Redencion])
         //WaitForEnter Item_Precio_Seleccionado
               
         //21May2015
         Descuento_aplicar = .10
         //WaitForEnter Descuento_aplicar
         Ttl_Calculo       = ( Item_Precio_Seleccionado * Descuento_aplicar )
         //WaitForEnter Ttl_Calculo
         //21May2015
         
         Format SNivel         as Nivel_Price[Sub_Nivel_Seleccionado]
      	 
      	 //21May2015 PtsAniv_monto         = Item_Precio_Seleccionado
      	 PtsAniv_monto         = Ttl_Calculo    //21May2015
      	 //WaitForEnter PtsAniv_monto
      	 
      	 PtsAniv_ProductName   = Item_No_Seleccionado
      	 ItemNumber            = 0
      	 //05Jun2015 AdjustedListPrice     = Ttl_Calculo
      	 AdjustedListPrice     = Item_Precio_Seleccionado  //05Jun2015
      //ElseIf Trim(VoucherProductName[Opcion_Redencion]) = "BK 15% Tkt"

//////////////////////////////////////// Descuentos Progresivos		06Mar2017
	ElseIf Trim(VoucherProductName[Opcion_Redencion]) = "WOW 10% BK"
		Var Total_Tck  : $10 = @TTLDUE  //05Jun2015
		Descuento_aplicar = .10
		Ttl_Calculo       = ( Total_Tck * Descuento_aplicar )
		Format SNivel     as "NA"
		PtsAniv_monto         = Ttl_Calculo
		PtsAniv_ProductName   = "WOW Ticket"
		ItemNumber            = 0
		AdjustedListPrice     = Ttl_Calculo
	
	ElseIf Trim(VoucherProductName[Opcion_Redencion]) = "WOW 15% BK"
		Var Total_Tck  : $10 = @TTLDUE  //05Jun2015
		Descuento_aplicar = .15
		Ttl_Calculo       = ( Total_Tck * Descuento_aplicar )
		Format SNivel     as "NA"
		PtsAniv_monto         = Ttl_Calculo
		PtsAniv_ProductName   = "WOW Ticket"
		ItemNumber            = 0
		AdjustedListPrice     = Ttl_Calculo
	
	ElseIf Trim(VoucherProductName[Opcion_Redencion]) = "WOW 20% BK"
		Var Total_Tck  : $10 = @TTLDUE  //05Jun2015
		Descuento_aplicar = .20
		Ttl_Calculo       = ( Total_Tck * Descuento_aplicar )
		Format SNivel     as "NA"
		PtsAniv_monto         = Ttl_Calculo
		PtsAniv_ProductName   = "WOW Ticket"
		ItemNumber            = 0
		AdjustedListPrice     = Ttl_Calculo	
		
	ElseIf Trim(VoucherProductName[Opcion_Redencion]) = "WOW 25% BK"
		Var Total_Tck  : $10 = @TTLDUE  //05Jun2015
		Descuento_aplicar = .25
		Ttl_Calculo       = ( Total_Tck * Descuento_aplicar )
		Format SNivel     as "NA"
		PtsAniv_monto         = Ttl_Calculo
		PtsAniv_ProductName   = "WOW Ticket"
		ItemNumber            = 0
		AdjustedListPrice     = Ttl_Calculo	
	
	ElseIf Trim(VoucherProductName[Opcion_Redencion]) = "Free Postre BK"
		Call Verifica_Item_Seleccionado
		 //and  MID(Item_Nombre_Seleccionado,1,4)= "CONO"  //Test 10/08/2015
		//If @TTLDUE - Item_Precio_Seleccionado < minimumAmount[Opcion_Redencion] and Item_No_Seleccionado = 8001
			//ExitWithError "EL MONTOMINIMO DEBE DE SER SIN EL ARTICULO PARA REDIMIR ESTE CUPON"
		//EndIf

		If Item_Qty_Seleccionado <> 1
			InfoMessage "WOW CRM", "SOLO SE PERMITE UN ARTICULO PARA REDIMIR A LA VEZ"
			ExitCancel
		EndIf
		Format SNivel            as Nivel_Price[Sub_Nivel_Seleccionado]
             
		PtsAniv_monto         = Item_Precio_Seleccionado
		PtsAniv_ProductName   = Item_No_Seleccionado
		AdjustedListPrice     = Item_Precio_Seleccionado
		Item_Qty_Seleccionado = 0

//////////////////////////////////////// Descuentos Progresivos

      ElseIf Trim(VoucherProductName[Opcion_Redencion]) = "30%Desc BK"
		 //infomessage (VoucherProductName[Opcion_Redencion])
		 //05Jun2015 Call Verifica_Item_Seleccionado
         //05Jun2015 If Item_Qty_Seleccionado <> 1
            //05Jun2015 InfoMessage "WOW CRM", "SOLO SE PERMITE UN ARTICULO PARA REDIMIR A LA VEZ"
            //05Jun2015 ExitCancel
         //05Jun2015 EndIf
         
         //WaitForEnter Trim(VoucherProductName[Opcion_Redencion])
         //WaitForEnter Item_Precio_Seleccionado
         
         //21May2015
         Var Total_Tck  : $10 = @TTLDUE  //05Jun2015
         Descuento_aplicar = .30
         //05Jun2015 Ttl_Calculo       = ( Item_Precio_Seleccionado * Descuento_aplicar )
         Ttl_Calculo       = ( Total_Tck * Descuento_aplicar )  //05Jun2015
         //21May2015
         
         //05Jun2015 Format SNivel         as Nivel_Price[Sub_Nivel_Seleccionado]
         Format SNivel     as "NA"  //05Jun2015
      	 
      	 //21May2015 PtsAniv_monto         = Item_Precio_Seleccionado
      	 PtsAniv_monto         = Ttl_Calculo    //21May2015
      	 
      	 //05Jun2015 PtsAniv_ProductName   = Item_No_Seleccionado
      	 PtsAniv_ProductName   = "WOW Ticket"
      	 ItemNumber            = 0
      	 AdjustedListPrice     = Ttl_Calculo
      //21May2015
	  
      Else
      	 //27Ene2015
         Call Verifica_Item_Seleccionado
         If Item_Qty_Seleccionado <> 1
            InfoMessage "WOW CRM", "SOLO SE PERMITE UN ARTICULO PARA REDIMIR A LA VEZ"
            ExitCancel
         EndIf
         Format SNivel            as Nivel_Price[Sub_Nivel_Seleccionado]
         //27Ene2015
         
      	 PtsAniv_monto         = Item_Precio_Seleccionado
      	 PtsAniv_ProductName   = Item_No_Seleccionado
      	 ItemNumber            = 0
      	 AdjustedListPrice     = Item_Precio_Seleccionado
      EndIf
      
      
      
      //WaitForEnter "Aportacion Redencion"
      
      Call Guarda_Campos_Acreditaciones_Redenciones(SNivel, PtsAniv_monto, PtsAniv_ProductName, Item_Qty_Seleccionado, Cheque_Red, ItemNumber)
      Call Arma_Registro_Acreditaciones_Redenciones
      Call Formatea_Datos_Redenciones
      Call Inserta_Solicitud_Acreditacion_Redencion
      Call Arma_Registro_Acreditaciones_Redenciones_CRM_Log
      Call Inserta_Solicitud_Acreditacion_Redencion
		
//New
//27Ene2015 Call Manda_Redemption
Call Lee_Miembro_O
Call Manda_Redemption_Miembro_O
Call Valida_Redemption
Status_Respuesta = "P"
Tipo_Descuento   = "M"
//New

Prompt "Espere ...5" 

      Name_Jar = "REDENCIONES"
      Status_Respuesta = ""   //29Dic2011 
      //28Dic2011 Call Manda_Ejecuta_Acreditaciones_Redenciones_WS_JAR(Name_Jar)
      //OK MOD Call Manda_Ejecuta_Redenciones_Solamente_WS_JAR(Name_Jar)   //////28Dic2011 
      //OK MOD Call Valida_Respuesta_Jar_Redencion  //28Dic2011
      //21Dic2011 Call Valida_Respuesta_Jar
      //OK MOD If Len(Status_Respuesta) = 0   //29Dic2011 
         //OK MOD Call Busca_Respuesta_Acreditaciones_Redenciones(TicketNumber, Business_Date_Real)   //28Dic2011 
      //OK MOD EndIf                          //29Dic2011 

Prompt "Espere ...6"

      //OK MOD Name_Jar = "REDENCION "                   //29Dic2011 
      //OK MOD If Len(Status_Respuesta) = 0              //28Dic2011 
         //OK MOD Call Pantalla_Respuesta_WS(Name_Jar)   //28Dic2011 
      //OK MOD EndIf                                     //29Dic2011 
      
      //29Dic2011 waitforenter Status
      //29Dic2011 waitforenter Reden_Acred_Aprobada_Status
      //29Dic2011 waitforenter SubStatus
      //29Dic2011 waitforenter Reden_Acred_Aprobada_SubStat
      //29Dic2011 waitforenter Status_Respuesta
      
      Call Graba_Archivo_datos(Status_Respuesta)   //29Dic2011 
      
      //29Dic2011If ( Status = Reden_Acred_Aprobada_Status And SubStatus = Reden_Acred_Aprobada_SubStat ) \\
      
      Status_Respuesta   = "P"
      Tipo_Descuento     = "M"
      Tipo_Sub_Descuento = ""  //08Abr2015
      
      //17NOV2014
      
      //WaitForEnter Tipo_Descuento_Valor_M               //
      //WaitForEnter validDiscountType[Opcion_Redencion]  //
      //WaitForEnter VoucherProductName[Opcion_Redencion]
      
      //08Abr2015
      If     Mid(validDiscountType[Opcion_Redencion],1,26) = "Discount Percentage Ticket"
         Tipo_Descuento         = "P"
         Tipo_Sub_Descuento     = "1"
         Descuento_Valido       = validDiscountAmount[Opcion_Redencion]
         Tipo_Descuento_Valor_P = ( ( validDiscountAmount[Opcion_Redencion] / 100 ) * ( @TTLDUE ) )
		 
      ElseIf     Mid(validDiscountType[Opcion_Redencion],1,24) = "Discount Percentage Item"
         Tipo_Descuento         = "P"
         Tipo_Sub_Descuento     = "2"
         Descuento_Valido       = validDiscountAmount[Opcion_Redencion]
         Tipo_Descuento_Valor_P = ( ( Descuento_Valido / 100 ) * ( Item_Precio_Seleccionado ) )
		 
      ElseIf     Mid(validDiscountType[Opcion_Redencion],1,19) = "Discount Percentage"
         Tipo_Descuento         = "P"
         Tipo_Sub_Descuento     = "3"
         Descuento_Valido       = validDiscountAmount[Opcion_Redencion]
         Tipo_Descuento_Valor_P = ( ( validDiscountAmount[Opcion_Redencion] / 100 ) * ( @TTLDUE ) )
	
      ElseIf Mid(validDiscountType[Opcion_Redencion],1,15) = "Discount Amount" Or Mid(validDiscountType[Opcion_Redencion],1,14) = "Discount Amout"
         Tipo_Descuento         = "M"
         Tipo_Sub_Descuento     = "1"
         Tipo_Descuento_Valor_M = validDiscountAmount[Opcion_Redencion]
		  
      ElseIf     Mid(validDiscountType[Opcion_Redencion],1,5) = "Vacio"
         Tipo_Descuento         = "A"
         Tipo_Sub_Descuento     = "1"
         Tipo_Descuento_Valor_M = Item_Precio_Seleccionado
		
      Else
      	 Tipo_Descuento         = "A"
      	 Tipo_Sub_Descuento     = "2"
      	 If Tipo_Descuento_Valor_M = 0
            Tipo_Descuento_Valor_M = Item_Precio_Seleccionado
			
         EndIf
		  
      EndIf
      //08Abr2015
      
      //08Abr2015If     Mid(validDiscountType[Opcion_Redencion],1,19) = "Discount Percentage"
         //08Abr2015 Tipo_Descuento         = "P"
         //08Abr2015 Tipo_Descuento_Valor_P = validDiscountAmount[Opcion_Redencion]
      //08Abr2015 ElseIf Mid(validDiscountType[Opcion_Redencion],1,15) = "Discount Amount" Or Mid(validDiscountType[Opcion_Redencion],1,14) = "Discout Amout"
         //08Abr2015 Tipo_Descuento         = "M"
         //08Abr2015 Tipo_Descuento_Valor_M = validDiscountAmount[Opcion_Redencion]
      //08Abr2015 Else
      	 //08Abr2015 //28Ene2015 If Tipo_Descuento_Valor_M = 0
         //08Abr2015 Tipo_Descuento_Valor_M = Item_Precio_Seleccionado
         //08Abr2015 //28Ene2015 EndIf
      	 //08Abr2015 Tipo_Descuento         = "A"
      //08Abr2015 EndIf

      //WaitForEnter Tipo_Descuento  // A
      //WaitForEnter Tipo_Descuento_Valor_M  //10.00
      
      //17NOV2014

      //16Ene2015 WaitForEnter "Status_Respuesta"  //OK
      //16Ene2015 WaitForEnter Status_Respuesta    //P
      //16Ene2015 WaitForEnter validDiscountType[Opcion_Redencion]  //Discount Amount
      //16Ene2015 WaitForEnter Tipo_Descuento          //M
      //16Ene2015 WaitForEnter Tipo_Descuento_Valor_P  //0.00
      //16Ene2015 WaitForEnter Tipo_Descuento_Valor_M  //50.00
      
      If    ( Status_Respuesta = "T" )  \\
         Or ( Status_Respuesta = "O" )  \\
         Or ( Status_Respuesta = "P" )  \\
         Or ( Status_Respuesta = "S" )     //28Dic2011 
         
         If Trim(VoucherProductName[Opcion_Redencion]) = "50 Pts Aniv"  //22Ene2015
         Else                                                           //22Ene2015
            Call Graba_Linea_Item_Redimida( Cheque_Red, Item_Linea_Seleccionado )
         EndIf                                                          //22Ene2015
         
         Format Campo as Mid(VoucherProductName[Opcion_Redencion],1,16)
         //08Abr2015 
         //WaitForEnter Tipo_Descuento           //P M a
         //WaitForEnter Tipo_Descuento_Valor_M   //0.00 20.00 2900
         //WaitForEnter Tipo_Descuento_Valor_P   //11.40 0.00 
         //WaitForEnter Tipo_Sub_Descuento       //2 1 3
         //WaitForEnter Item_Precio_Seleccionado //57.00 29.00 29
         //WaitForEnter validDiscountAmount[Opcion_Redencion] //20.00  20.00 0
         //WaitForEnter Descuento_Valido  //20.00  0.00 0
         //WaitForEnter Descuento_Redenciones_P  //143
         //WaitForEnter Descuento_Redenciones_M  //142
         //WaitForEnter Tipo_Descuento_Valor_P   //0.00
         
         If Tipo_Descuento = "P"
            If Tipo_Descuento_Valor_P <> 0
               Var Tempo  : $10 = 0
               Descuento_P_Cal = ( Tipo_Descuento_Valor_P / 100 )
               Tempo = Item_Precio_Seleccionado  // validDiscountAmount[Opcion_Redencion]
               Descuento_Calculado = (  Tempo * Descuento_P_Cal )
            EndIf
            If Tipo_Descuento_Valor_P = 0
               Tipo_Descuento_Valor_P = validDiscountAmount[Opcion_Redencion]
            EndIf
            
            If     Tipo_Sub_Descuento = 0
               LoadKyBdMacro MakeKeys(Descuento_Calculado), Key(5, Descuento_Redenciones_M)
               LoadKyBdMacro MakeKeys(Campo), @KEY_ENTER
            ElseIf Tipo_Sub_Descuento = 1
               LoadKyBdMacro MakeKeys(Tipo_Descuento_Valor_P), Key(5, Cupon_Desc_P1)
               LoadKyBdMacro MakeKeys(Campo), @KEY_ENTER
            ElseIf Tipo_Sub_Descuento = 2
               LoadKyBdMacro MakeKeys(Tipo_Descuento_Valor_P), Key(5, Cupon_Desc_P2)
               LoadKyBdMacro MakeKeys(Campo), @KEY_ENTER
            ElseIf Tipo_Sub_Descuento = 3
               LoadKyBdMacro MakeKeys(Descuento_Calculado), Key(5, Descuento_Redenciones_M)
               LoadKyBdMacro MakeKeys(Campo), @KEY_ENTER
            EndIf
         ElseIf Tipo_Descuento = "M"
            If  Tipo_Sub_Descuento = 0
               LoadKyBdMacro MakeKeys(Item_Precio_Seleccionado), Key(5, Descuento_Redenciones_M)
               LoadKyBdMacro MakeKeys(Campo), @KEY_ENTER
            ElseIf  Tipo_Sub_Descuento = 1
               If Tipo_Descuento_Valor_M <> 0
                  LoadKyBdMacro MakeKeys(Tipo_Descuento_Valor_M), Key(5, Descuento_Redenciones_M)
               Else
               	  LoadKyBdMacro MakeKeys(Item_Precio_Seleccionado), Key(5, Descuento_Redenciones_M)
               EndIf
               LoadKyBdMacro MakeKeys(Campo), @KEY_ENTER
            EndIf
         ElseIf Tipo_Descuento = "A"
            LoadKyBdMacro MakeKeys(Tipo_Descuento_Valor_M), Key(5, Descuento_Redenciones_M)
            LoadKyBdMacro MakeKeys(Campo), @KEY_ENTER
         EndIf
         //08Abr2015 
         
         //08Abr2015 If Tipo_Descuento = "P"
            //08Abr2015 //WaitForEnter Descuento_Redenciones_P  //143
            //08Abr2015 //17NOV2014 Tipo_Descuento_Valor_P = validDiscountAmount[Opcion_Redencion]
            //08Abr2015 //17NOV2014 Tipo_Descuento_Valor_P = 100
            
            //08Abr2015 If Tipo_Descuento_Valor_P <> 0
               //08Abr2015 Var Tempo  : $10 = 0
               //08Abr2015 Descuento_P_Cal = ( Tipo_Descuento_Valor_P / 100 )
               //08Abr2015 //WaitForEnter Descuento_P_Cal  //0.20
               //08Abr2015 Tempo = Item_Precio_Seleccionado  // validDiscountAmount[Opcion_Redencion]
               //08Abr2015 //WaitForEnter Tempo            //
               //08Abr2015 Descuento_Calculado = (  Tempo * Descuento_P_Cal )
               //08Abr2015 //WaitForEnter Descuento_Calculado   //
            //08Abr2015 EndIf
            
            //08Abr2015 //08Dic2014 LoadKyBdMacro MakeKeys(Tipo_Descuento_Valor_P), Key(5, Descuento_Redenciones_P)
            
            //08Abr2015 LoadKyBdMacro MakeKeys(Descuento_Calculado), Key(5, Descuento_Redenciones_M)
            //08Abr2015 LoadKyBdMacro MakeKeys(Campo), @KEY_ENTER
         //08Abr2015 ElseIf Tipo_Descuento = "M"
            //08Abr2015 //WaitForEnter Descuento_Redenciones_M
            //08Abr2015 //17NOV2014 Tipo_Descuento_Valor_M = validDiscountAmount[Opcion_Redencion]
            //08Abr2015 //LoadKyBdMacro MakeKeys(Tipo_Descuento_Valor_M), Key(5, Descuento_Redenciones_M)
            
            //08Abr2015 LoadKyBdMacro MakeKeys(Tipo_Descuento_Valor_M), Key(5, Descuento_Redenciones_M)
            //08Abr2015 LoadKyBdMacro MakeKeys(Campo), @KEY_ENTER
         //08Abr2015 ElseIf Tipo_Descuento = "A"
            //08Abr2015 //WaitForEnter Descuento_Redenciones_M
         	  //08Abr2015 //17NOV2014 If Tipo_Descuento_Valor_M = 0
               //08Abr2015 //17NOV2014 Tipo_Descuento_Valor_M = Item_Precio_Seleccionado
            //08Abr2015 //17NOV2014 EndIf
            //08Abr2015 LoadKyBdMacro MakeKeys(Tipo_Descuento_Valor_M), Key(5, Descuento_Redenciones_M)
            //08Abr2015 LoadKyBdMacro MakeKeys(Campo), @KEY_ENTER
         //08Abr2015 EndIf
		 banderamini=1
      Else
      	 ExitWithError "EL CUPON NO APLICA PARA EL PRODUCTO SELECCIONADO"
      EndIf   //28Dic2011 
	
EndEvent

Event Inq: 37        //Cancelaciones CRM Devuelve Inventario  Void
      Var Ticket_Cancelar_1   : A20   = ""
      Var Ticket_Cancelar     : A20   = ""
      Var Ticket_Cancelar_N   : N6    = 0
      Var Cheque_Existe       : N4    = 0
      //Var Ticket_Unico        : A40   = ""
      //Var Fecha_a_Convertir   : A20   = ""
      //Var Fecha_Juliana_M     : A20   = ""
      //Var Fecha_Numerica      : N6    = 0
      Var Arma_Cadena_22      : A2000 = ""
      Var Cheque_Leido1       : A4    = ""
      Var Cheque_Leido1_N     : N4    = ""
      Var Key_Void            : Key
      Var Camp01              : A99 = ""
      Var Camp02              : A99 = ""
      Var Camp03              : A99 = ""
      Var Camp04              : A99 = ""
      Var Mensaje_Error_      : A20 = ""
      Var TiT                 : A20 = "CANCELACIONES"
      
      ////waitforenter "1"
      ContinueOnCancel
      Key_Void = Key(1,458753)
      Call Lee_Configuracion_CRM
      Call Borra_Leyenda_CRM

      If @CKNUM > 0
         Call Read_Ticket_Cancelar
         
         If Len(Trim(Ticket_Cancelar_Numero)) > 0
            LoadKyBdMacro Key_Void  //Void Key
            ExitContinue
         EndIf
         
      EndIf

      Call Main_Solicita_Ticket_Unico
      
      If Len(Trim(Ticket_Unico)) > 0
         Format Ticket_Cancelar as Ticket_Unico
         Format Ticket_Cancelar_Numero as Ticket_Unico
         Call Manda_Solicita_Cancelacion_Ticket
         
         If Trim(@RxMsg) = "_timeout"
            Call Main_Graba_Cheques_OffLine
         ElseIf Mid(@RxMsg,1,7) = "Invalid" or Mid(@RxMsg,1,4) = "Fail" or Mid(@RxMsg,1,10) = "Incomplete" or Mid(@RxMsg,1,6) = "Queued"
            If Mid(@RxMsg,1,7)       = "Invalid"
               Format Mensaje_Error_ as "Invalid"
            ElseIf  Mid(@RxMsg,1,4)  = "Fail"
               Format Mensaje_Error_ as "Fail"
            ElseIf Mid(@RxMsg,1,10)  = "Incomplete"
               Format Mensaje_Error_ as "Incomplete"
            ElseIf Mid(@RxMsg,1,6)   = "Queued"
               Format Mensaje_Error_ as "Queued"
            EndIf
            //26Dic2011 Call Rutina_Muestra_Mensajes_Cancelaciones_Principal(Mensaje_Error_)
            Call Main_Graba_Cheques_OffLine
            //18Ene2012 Call Read_Ticket_Cancelar
         ElseIf Mid(@RxMsg,1,16) = "NO EXISTE TICKET"
            Call Main_Graba_Cheques_OffLine
         ElseIf Mid(@RxMsg,1,26) = "NO HAY COMUNICACION CON WS"
            Call Main_Graba_Cheques_OffLine
         ElseIf Mid(@RxMsg,1,28) = "NO SE REALIZO LA CANCELACION"
            Call Main_Graba_Cheques_OffLine
         Else
        	  Format Mensaje_Error_ as "Success"
            //26Dic2011 Call Rutina_Muestra_Mensajes_Cancelaciones_Principal(Mensaje_Error_)
            
          	Format Leyenda_Leida_Crm as "Folio de Cancelacion:"
            Call Write_Leyenda_CRM(Leyenda_Leida_Crm)
            Format Leyenda_Leida_Crm as Ticket_Unico
            Call Write_Leyenda_CRM(Leyenda_Leida_Crm)
            
            Format Leyenda_Leida_Crm as "."
            Call Write_Leyenda_CRM(Leyenda_Leida_Crm)
            Format Leyenda_Leida_Crm as "."
            Call Write_Leyenda_CRM(Leyenda_Leida_Crm)
            Format Leyenda_Leida_Crm as "________________________________"{32}
            Call Write_Leyenda_CRM(Leyenda_Leida_Crm)
            
         	  //18Ene2012 Call Read_Ticket_Cancelar
         EndIf
      EndIf
      Call Rutina_Carga_Macro_ISL
      //18Ene2012 Call Borra_Ticket_Cancelar
EndEvent
Sub Rutina_Carga_Macro_ISL
    If Len(Trim(Ticket_Cancelar_Numero)) > 0
       Call Write_Ticket_Cancelar(Ticket_Cancelar_Numero)
       //18Ene2012 LoadKyBdMacro Key_Void  //Void Key
    //18Ene2012 Else
       //18Ene2012 Call Write_Ticket_Cancelar(Ticket_Cancelar_Numero)
    EndIf
       If @RVC = 2
          If @CKNUM = 0
             LoadKyBdMacro Key(1, 327681), MakeKeys(@TREMP), Key(1,65549), MakeKeys("0"), Key(1,65549)
             LoadKyBdMacro Key_Void  //Void Key
          Else
          	 LoadKyBdMacro Key_Void  //Void Key
          EndIf
       Else
       	  If @CKNUM = 0
       	     LoadKyBdMacro Key(1, 327681), MakeKeys(@TREMP), Key(1,65549), MakeKeys("0"), Key(1,65549)
             LoadKyBdMacro Key_Void  //Void Key
          Else
          	 LoadKyBdMacro Key_Void  //Void Key
          EndIf
       EndIf
    //18Ene2012 EndIf
EndSub

Event Inq: 38        //Cancelaciones CRM NO Devuelve Inventario  Return
      Var Ticket_Cancelar_1: A20   = ""
      Var Ticket_Cancelar  : A20   = ""
      Var Ticket_Cancelar_N: N6    = 0
      Var Cheque_Existe    : N4    = 0
      //Var Ticket_Unico     : A20   = ""
      //Var Fecha_a_Convertir: A20   = ""
      //Var Fecha_Juliana_M  : A20   = ""
      //Var Fecha_Numerica   : N6    = 0
      Var Arma_Cadena_22   : A2000 = ""
      Var Mensaje_Error_   : A20 = ""
      Var Key_Return       : Key
      Var TiT              : A20 = "CANCELACIONES"
      
      ////waitforenter "2"
      
      ContinueOnCancel
      Key_Return = Key(1,458754)
      Call Lee_Configuracion_CRM
      Call Borra_Leyenda_CRM
      
      If @CKNUM > 0
         Call Read_Ticket_Cancelar

         If Len(Trim(Ticket_Cancelar_Numero)) > 0
            LoadKyBdMacro Key_Return //Return Key
            ExitContinue
         EndIf
         
      EndIf

      Call Main_Solicita_Ticket_Unico
      
      
      If Len(Trim(Ticket_Unico)) > 0
         
         Format Ticket_Cancelar as Ticket_Unico
         Format Ticket_Cancelar_Numero as Ticket_Unico
         Call Manda_Solicita_Cancelacion_Ticket
      
         If Trim(@RxMsg) = "_timeout"
            Call Main_Graba_Cheques_OffLine
         ElseIf Mid(@RxMsg,1,7) = "Invalid" or Mid(@RxMsg,1,4) = "Fail" or Mid(@RxMsg,1,10) = "Incomplete" or Mid(@RxMsg,1,6) = "Queued"
            //Call MAIL
            
            If Mid(@RxMsg,1,7)       = "Invalid"
               Format Mensaje_Error_ as "Invalid"
            ElseIf  Mid(@RxMsg,1,4)  = "Fail"
               Format Mensaje_Error_ as "Fail"
            ElseIf Mid(@RxMsg,1,10)  = "Incomplete"
               Format Mensaje_Error_ as "Incomplete"
            ElseIf Mid(@RxMsg,1,6)   = "Queued"
               Format Mensaje_Error_ as "Queued"
            EndIf
            //26Dic2011 Call Rutina_Muestra_Mensajes_Cancelaciones_Principal(Mensaje_Error_)
            
            Call Main_Graba_Cheques_OffLine
            //18Ene2012 Call Read_Ticket_Cancelar
         ElseIf Mid(@RxMsg,1,16) = "NO EXISTE TICKET"
            Call Main_Graba_Cheques_OffLine
         ElseIf Mid(@RxMsg,1,26) = "NO HAY COMUNICACION CON WS"
            Call Main_Graba_Cheques_OffLine
         ElseIf Mid(@RxMsg,1,28) = "NO SE REALIZO LA CANCELACION"
            Call Main_Graba_Cheques_OffLine
         Else
         	  Format Mensaje_Error_ as "Success"
            //26Dic2011 Call Rutina_Muestra_Mensajes_Cancelaciones_Principal(Mensaje_Error_)
            
            Format Leyenda_Leida_Crm as "Folio de Cancelacion: "
            Call Write_Leyenda_CRM(Leyenda_Leida_Crm)
            Format Leyenda_Leida_Crm as Ticket_Unico
            Call Write_Leyenda_CRM(Leyenda_Leida_Crm)

            Format Leyenda_Leida_Crm as "."
            Call Write_Leyenda_CRM(Leyenda_Leida_Crm)
            Format Leyenda_Leida_Crm as "."
            Call Write_Leyenda_CRM(Leyenda_Leida_Crm)
            Format Leyenda_Leida_Crm as "________________________________"{32}
            
         	  //18Ene2012 Call Read_Ticket_Cancelar
         EndIf

      EndIf
      Call Rutina_Carga_Macro_ISL_Return
      //18Ene2012 Call Borra_Ticket_Cancelar
EndEvent
Sub Rutina_Carga_Macro_ISL_Return
    If Len(Trim(Ticket_Cancelar_Numero)) > 0
       Call Write_Ticket_Cancelar(Ticket_Cancelar_Numero)
       //18Ene2012 LoadKyBdMacro Key_Return  //Return Key
    //18Ene2012 Else
       //18Ene2012 Call Write_Ticket_Cancelar(Ticket_Cancelar_Numero)
    EndIf
       If @RVC = 2
          If @CKNUM = 0
             LoadKyBdMacro Key(1, 327681), MakeKeys(@TREMP), Key(1,65549), MakeKeys("0"), Key(1,65549)
             LoadKyBdMacro Key_Return  //Return Key
          Else
          	 LoadKyBdMacro Key_Return  //Return Key
          EndIf
       Else
       	  If @CKNUM = 0
       	     LoadKyBdMacro Key(1, 327681), MakeKeys(@TREMP), Key(1,65549), MakeKeys("0"), Key(1,65549)
             LoadKyBdMacro Key_Return  //Return Key
          Else
          	 LoadKyBdMacro Key_Return  //Return Key
          EndIf
       EndIf
    //18Ene2012 EndIf
EndSub
Event Inq: 39   ///Socio Wow Rewards  (SI / NO)  LOCAL
      //WaitForEnter "Inicio"
      Call Borra_Leyenda_CRM
      Call Socio_Wow_Rewards
      If Flag_Socio_Wow_Rewards = "N"
         LoadKyBdMacro Key (1, 393233)//Order Type Local
         LoadKyBdMacro Key (17,201)//SLU
      Else
	       LoadKyBdMacro Key (1, 393233)//Order Type Local
	       LoadKyBdMacro Key (17,201)//SLU
      	 //06Ene2015 Call LoadSIMInquire(7, 55)//Miembro
      	 Call Continuacion(10, 31)  //06Ene2015 
      	 //Call loadSIMInquire(15, 31) //Pasa a Pms6 Inq: 31  QUITAR ESTA LINEA
      EndIf
EndEvent
Event Inq: 40   ///Socio Wow Rewards  (SI / NO)  LLEVAR
      //WaitForEnter "Inicio"
      //waitforenter Flag_Socio_Wow_Rewards
      Call Borra_Leyenda_CRM
      Call Socio_Wow_Rewards
      If Flag_Socio_Wow_Rewards = "N"
         LoadKyBdMacro Key (1, 393234)//Order Type Llevar
         LoadKyBdMacro Key (17,201)//SLU
      Else
	       LoadKyBdMacro Key (1, 393234)//Order Type Llevar
	       LoadKyBdMacro Key (17,201)//Slu
      	 Call LoadSIMInquire(7, 55)//Miembro
      EndIf
EndEvent



Event Inq: 41   ///Socio Wow Rewards  (SI / NO)  DRIVE THRU  Inicia Cuenta
      //WaitForEnter "Inicio"
      Call Borra_Leyenda_CRM
      Call Socio_Wow_Rewards
      If Flag_Socio_Wow_Rewards = "N"
         LoadKyBdMacro Key (1, 393236) //Order Type Drive Thru
	     LoadKyBdMacro Key (1, 327681) //Begin Check by number
	     LoadKyBdMacro Key (17,201)    //Slu
      Else
	     LoadKyBdMacro Key (1, 393236) //Order Type Drive Thru
	     LoadKyBdMacro Key (1, 327681) //Begin Check by number
	     LoadKyBdMacro Key (17,201)    //Slu
      	 Call LoadSIMInquire(10, 31)   //Miembro
      EndIf
EndEvent

Event Inq: 42   ///Cancelacion de Miembro en Cheque
      Call Verifica_Miembro_Cheque
      If Miembro_Existe = "T"
         ClearChkInfo
         Call Rutina_Borra_Archivos_Temporales_Dll
      Else
      	 ClearChkInfo
         Call Rutina_Borra_Archivos_Temporales_Dll
      EndIf
EndEvent

Event Inq: 43   ///Socio Wow Rewards  (SI / NO)  LOBBY
      //WaitForEnter "Inicio"
      Call Borra_Leyenda_CRM
      Call Socio_Wow_Rewards
      If Flag_Socio_Wow_Rewards = "N"
         LoadKyBdMacro Key (1, 393237)//Order Type LOBBY
         LoadKyBdMacro Key (17,201)//SLU
      Else
	       LoadKyBdMacro Key (1, 393237)//Order Type LOBBY
	       LoadKyBdMacro Key (17,201)//SLU
      	 Call LoadSIMInquire(7, 55)//Miembro
      EndIf
EndEvent
Event Inq: 61   ///Consulta Socio CRM
      ////waitforenter "inicia 61"
      Call LoadSIMInquire(7, 55)  //Miembro  Pms4Subs Inq 55   y luego a Continua Inq: 31
EndEvent



event tndr
//agrega un metodo de pago
endevent

//Event Inq: 31  
Event Final_Tender //Acreditaciones Accrual

      Var MemberNumber       : A100 = ""
      //Var ID                 : A50  = ""
      Var ALSELevelPrice     : A50  = ""
      Var BranchDivision     : A50  = ""
      Var ALSEPaymnetMode    : A50  = ""
      Var ActivityDate       : A50  = ""
      //26Ene2015 Var AdjustedListPrice  : A50  = ""
      Var Amount             : A50  = ""	
      Var Comments           : A50  = ""
      Var ItemNumber         : A50  = ""
      Var LocationName       : A50  = ""
      Var PartnerName        : A50  = ""
      Var PointName          : A50  = ""
      Var Points             : A50  = ""	
      Var ProcessDate        : A50  = ""
      Var ProcessingComment  : A50  = ""
      Var ProcessingLog      : A50  = ""
      Var ProductName        : A50  = ""
      Var Quantity           : A50  = ""
      Var TicketNumber       : A50  = ""
      Var TransactionChannel : A50  = ""
      Var TransactionDate    : A50  = ""
      Var TransactionSubType : A50  = ""
      Var TransactionType    : A50  = ""
      Var VoucherNumber      : A50  = ""
      Var VoucherQty         : A50  = "1"
      Var VoucherType        : A50  = ""
      Var Organization       : A50  = ""

      Var Bussiness_date     : A50  = ""
      Var Centroconsumo      : A50  = ""
      Var Cheque             : A50  = ""
      Var Transmitido        : A50  = ""
      Var Arma_Cadena        : A2000 = ""
      Var Arma_Cadena_22     : A2000 = ""
      Var Status_Respuesta   : A1   = ""
      //Var Fecha_Juliana_M    : A20  = ""
      //Var Fecha_a_Convertir  : A20  = ""
      //Var Fecha_Numerica     : N6   = 0
      //Var Ticket_Unico       : A20  = ""

		//call barcode //10/08/2015
      //24Ene2015
      //26Ene2015 Call Verifica_Tmed_Soa_Only   //QUITAR O COMENTAR
      //24Ene2015
     
      Call Version_ISL
      
      Call Lee_Configuracion_CRM
      Call Verifica_Descuento_Aplicado
	  
	 
      //New12Ene2012 If FP_Sbux_Aplicado = "T" Or FP_Sbux_Aplicado_Off_Line = "T"
      //WaitForEnter Total_Fpago_Cheque
      	 If Total_Fpago_Cheque > 0
		 	Call Ejecuta_Acreditaciones_Main
         EndIf
		  //entra aqui con internet
		 if puntosWOW_Global > 0  //test 10/08/2015 
         Call imprime_ticket_redencion  //test 10/08/2015
		 endif  //test 10/08/2015
		 
		 puntosWOW_Global = 0 //test 10/08/2015
		 entra_puntos_primera_vez = 0 //test 10/08/2015
		 
      //New12Ene2012 EndIf
      Borrar = "S"
      Call Verifica_Llama_Cheque_Continuar
      Call Rutina_Borra_Archivos_Temporales  //24Dic2014 
      Call Rutina_Borra_Archivos_Temporales_Dll
	  //Call Rutina_Borra_Archivos_Temporales_Wow // 25 ENE 2016 <======= JMG 
      Call Borra_FileMiembro_Soa
	  
EndEvent
//===================================SUBRUTINAS===================================================
sub  barcode

	var barcode : A40

	format barcode as \
		chr(27), chr(64), \
		chr(29), chr(72), \
		chr(3), \
		chr(29), "h", \
		chr(80), \
		chr(29), "w", chr(3), \
		chr(27), "a", 1, \
		chr(29), "k", \
		chr(67), chr(12), \
		1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, \
		chr(27), chr(64)
		startprint @vald
			printline barcode
		endprint
endsub
//===================================SUBRUTINAS===================================================
Sub imprime_ticket_tarjeta_no_inscrita //test 10/08/2015
	Var tarjeta_invalida : a20 = Clave_ID_TrackII
	startprint @vald
        printline "Tarjeta con terminacion ",Mid(tarjeta_invalida,13,16)
        printline "         NO esta activa"
        printline "  Activala en www.wowrewards.com"
        printline "Fecha: ", @Day,"/",@Month,"/",@Year
        printline "Hora: ",@HOUR,":",@Minute,":",@Second
        
    endprint
EndSub //test 10/08/2015
Sub imprime_ticket_redencion //test 10/08/2015
    //fopen file_number, "\CF\micros\etc\Comprobante_PuntosWOW.txt", READ
    //listprint 10, msg //Print the list        
       
    startprint @vald
        printline "Empleado:  ",@TREMP_CHKNAME
        printline "Fecha: ", @Day,"/",@Month,"/",@Year
        printline "Hora: ",@HOUR,":",@Minute,":",@Second
        printline "Folio: ", Ticket_Unico
        printline ""
        printline " Membresia WOW"
        printline "   ",Clave_ID_TrackII
        printline "   Metodo de entrada: ", metodo_entrada
        printline "   Autorizado"
        printline ""
        printline "Puntos utilizados en pesos: $",puntosWOW_Global//tmed: *, inq:71
        printline ""
        printline ""
        printline "     Firma X____________________"
        printline ""
        printline "      GRACIAS POR SU VISITA "
        printline ""
        printline "         COPIA DE NEGOCIO"
        printline ""
                 
    endprint
    
endsub //test 10/08/2015

Sub Rutina_Borra_Archivos_Temporales
    //WaitForenter "Rutina_Borra_Archivos_Temporales"

    If Borrar = "S"
       //WaitForenter "Rutina_Borra_Archivos_Temporales"
       Call Borra_Miembro
       //24Dic2014 Call Borra_Miembro_O    //10Nov2014
       // Call Borra_Consulta_Saldo // Comentado el 14 ABR 2016
       //Call Borra_Privilegios_Nivel
       Call Borra_Privilegios_Nivel_Miembro
    EndIf
    
    Call Borra_Miembro_O    //24Dic2014
    //Call Borra_Leyenda_CRM
    Call Borra_Linea_Item_Redimida
  //Call Borra_Leyenda_CRM
    Call Borra_Ticket_Cancelar
    //Call Borra_Temporal // Comentado el 14 ABR 2016
    Call Borra_Consulta_Miembro_Screen
    CalL Borra_Off_Line_CRM
    Call Borra_Lee_Miembro_Solo
    
    //08Mar2012
    //Call Borra_Miembro_Servicio
    ////Call Borra_Privilegios_Nivel_Servicio
    ////Call Borra_Privilegios_Nivel_Miembro_Servicio
    ////Call Borra_Consulta_Saldo_Servicio
    //08Mar2012
    
    //03Jun2015
    Call Delete_Miembro_Ocupa_Pos(Wsid_wow)
    //03Jun2015
    
EndSub
Sub Ejecuta_Acreditaciones_Main
    Var CC  : N2 = 0
    
    Call Lee_Miembro
    //Call Lee_Miembro_Solo // Comentado el 13 ABR 2016
    Call Lee_Miembro_Soa

    //WaitForEnter NumeroMiembro
    //WaitForEnter NumeroMiembroSolo

    If Len(Trim(NumeroMiembro)) = 0
		If Len(Trim(NumeroMiembroSolo)) = 0 And offline = 0 //Test 10/08/2015 offline
          //WaitForenter "Numero miembro cero"
          Call Verifica_Llama_Cheque_Continuar
		  //InfoMessage "if ex" //test 10/08/2015 offline cuando no tienes socio
          If Borrar = "S"
             Call Borra_Miembro
          EndIf
          Call Borra_Linea_Item_Redimida
          ExitContinue
        Else
        	NumeroMiembro = NumeroMiembroSolo
			  
       EndIf
    EndIf

    Call Consulta_Business_Date_SyBase
    Call Borra_Miembro

    Format Ticket_Unico      as @WSID{01}, @CKNUM{04}, Mid(@Chk_Open_Time,10,2), Mid(@Chk_Open_Time,13,2)
    Format Fecha_a_Convertir as "20", Mid(@Chk_Open_Time,7,2), "-", Mid(@Chk_Open_Time,1,2), "-", Mid(@Chk_Open_Time,4,2)

    Call Consulta_Fecha_Juliana_Micros(Fecha_a_Convertir)
    Call Main_Busca_Articulos_Enviar_Acreditaciones
    Call Borra_Linea_Item_Redimida

EndSub

Sub Lee_Tarjeta_Magnetica_Wow_Card 
    Var IO         : N2  = 0
    Var No_tarjeta : A30 = "0"
    Var Campo_01_  : A80 = ""
    Var Campo_02_  : A80 = ""
    Var Campo_000  : A80 = ""
    //WaitForEnter  "Lee tarjeta"
    
    If Error_Time_Out_SbuxCard = "E" or Error_Time_Out_SbuxCard = "F"
       Format Campo_01_ as "<WS_OFFLINE>"
       Format Campo_02_ as "<Success>"
	   
       Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
	             
				 
       If @RVC = 1  Or @RVC = 2 Or @RVC = 4  Or @RVC = 5
       
          //WaitForEnter  "Lee tarjeta Paso1"
          
          TouchScreen Numero_TouchscreenN
          Window  (CC01 + 1 ), 78, "CRM WOW"
          For IO = 1 to CC01
               If IO = 3 Or IO = 4
                  DisplayInverse  IO, 1, Mid(Errores_Mostrar[IO],1,77)
               Else
               	  Display  IO, 1, Mid(Errores_Mostrar[IO],1,77)
               EndIf
          EndFor
          DisplayMsInput 0,0,Clave_ID{-m2,*},"Deslize Tarjeta CRM WOW", \
                         0,0,Clave_ID_TrackI{-m1,*},"Deslize Tarjeta CRM WOW1:"
						 
          WindowInput
          WindowClose
		  
		  

          If @MAGSTATUS = "Y"
           call Delete_Miembro_Ocupa_Pos(Wsid_wow)
             //WaitForEnter Clave_ID          //
             //WaitForEnter Clave_ID_TrackI   //
             //WaitForEnter Mid(Clave_ID,01,1) //
             //WaitForEnter Mid(Clave_ID,16,1) //
             //WaitForEnter Mid(Clave_ID,18,1) //
             //WaitForEnter Mid(Clave_ID,2,14) //
             //WaitForEnter Mid(Clave_ID,2,16) //
             
             //26Mar2015
             Split Clave_ID, "=", Clave_ID_Paso1, Clave_ID_Paso2
             
             ClearArray Clave_Id_Track_II
             ClearArray Clave_Id_Track_II_1
             
             Split Clave_ID_TrackI, "^", Clave_Id_Track_II[1], Clave_Id_Track_II[2], Clave_Id_Track_II[3]
             If Len(Clave_Id_Track_II[2]) > 0
                Split Clave_Id_Track_II[2], "/", Clave_Id_Track_II_1[1], Clave_Id_Track_II_1[2], Clave_Id_Track_II_1[3]
             EndIf
             //WaitForEnter Clave_Id_Track_II[2]
             //WaitForEnter Mid(Clave_Id_Track_II_1[3],1,30)
             //26Mar2015
             
             If     Mid(Clave_ID,1,1) = "A;" And Mid(Clave_ID,16,1) = "B?"  //No se usa quitar
                //Format No_tarjeta     As Mid(Clave_ID,2,14)
                //Format Campo_Busqueda As No_tarjeta
                //26Mar2015
             ElseIf Mid(Clave_ID_Paso1,1,1) = ";" And Len(Clave_ID_Paso2) > 0
                Var Len1  : N4 = 0
                Len1 = ( Len(Clave_ID_Paso1) - 1 )
                Format No_tarjeta     As Mid(Clave_ID_Paso1,2,Len1)
                Format Campo_Busqueda As No_tarjeta
                //WaitForEnter Campo_Busqueda
                //26Mar2015
             ElseIf Clave_Id_Track_II_1[3] = "ALSEAWOW" Or Clave_Id_Track_II_1[3] = "ALSE"
                Var Len1  : N4 = 0
                Len1 = ( Len(Clave_ID_Paso1) - 1 )
                Format No_tarjeta     As Clave_ID_Paso1
                Format Campo_Busqueda As No_tarjeta
                //WaitForEnter "ALSEAWOW"
                //WaitForEnter Campo_Busqueda
                //26Mar2015
             Else
                Format No_tarjeta     As Mid(Clave_ID,30,1)
                Format No_tarjeta     As No_tarjeta, Mid(Clave_ID,32,2)
                Format No_tarjeta     As No_tarjeta, Mid(Clave_ID, 7,1)
                Format No_tarjeta     As No_tarjeta, Mid(Clave_ID, 8,8)
                Format No_tarjeta     As No_tarjeta, Mid(Clave_ID,34,4)
                Format Campo_Busqueda As No_tarjeta
             EndIf
          Else
			  
		  
             Campo_Busqueda = Clave_ID
          EndIf
       ElseIf @RVC = 555  //14Jul2015 Tenia 5    //5  este solo hace la solicitud manual de miembro
          Format Campo_01_ as "<WS_OFFLINE>"
          Format Campo_02_ as "<Success>"
		  Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
       
          Window  (CC01 + 1 ), 78, "CRM WOW"
              For IO = 1 to CC01
                  If IO = 3 Or IO = 4
                  DisplayInverse  IO, 1, Mid(Errores_Mostrar[IO],1,77)
               Else
               	  Display  IO, 1, Mid(Errores_Mostrar[IO],1,77)
               EndIf
             EndFor
          ForEver
             TouchScreen Numero_TouchscreenN
             Input Clave_ID, "Digite # Tarjeta CRM WOW"
             If @MAGSTATUS = "Y"
                InfoMessage "ERROR DE CAPTURA", "DIGITE MANUALMENTE EL NUMERO DE TARJETA"
             Else
             	 Campo_Busqueda = Clave_ID
          	    Break
             EndIf
       	 EndFor
       	 WindowInput
         WindowClose
       EndIf
    Else
    	If @RVC = 1  Or @RVC = 2 Or @RVC = 4  Or @RVC = 5
    	    //WaitForEnter  "Lee tarjeta Paso2"
          TouchScreen Numero_TouchscreenN
          Window  1, 4, "WOW"
                DisplayMsInput 0,0,Clave_ID{-m2,*},"Deslize Tarjeta CRM WOW2:", \
                               0,0,Clave_ID_TrackI{-m1,*},"Deslize Tarjeta CRM WOW3:", \ //Test 10/08/2015
							   0,0,Clave_ID_TrackII{m2, 1, 1, *},"Deslize Tarjeta CRM WOW4:" //Test 10/08/2015
			
          WindowInput
          WindowClose

          If @MAGSTATUS = "Y"
             call Delete_Miembro_Ocupa_Pos(Wsid_wow) 
            metodo_entrada = "Tarjeta"

			Format Clave_ID_TrackII_Origin as Clave_ID_TrackII //test 10/08/2015
			mid(Clave_ID_TrackII,1,12) = "XXXXXXXXXXXX" //test 10/08/2015
			
			
             //WaitForEnter Clave_ID          //
             //WaitForEnter Clave_ID_TrackI   //
             //WaitForEnter Mid(Clave_ID,01,1) //
             //WaitForEnter Mid(Clave_ID,16,1) //
             //WaitForEnter Mid(Clave_ID,18,1) //
             //WaitForEnter Mid(Clave_ID,2,14) //
             //WaitForEnter Mid(Clave_ID,2,16) //
             
             //26Mar2015
             Split Clave_ID, "=", Clave_ID_Paso1, Clave_ID_Paso2
             
             ClearArray Clave_Id_Track_II
             ClearArray Clave_Id_Track_II_1
             
             Split Clave_ID_TrackI, "^", Clave_Id_Track_II[1], Clave_Id_Track_II[2], Clave_Id_Track_II[3]
             If Len(Clave_Id_Track_II[2]) > 0
                Split Clave_Id_Track_II[2], "/", Clave_Id_Track_II_1[1], Clave_Id_Track_II_1[2], Clave_Id_Track_II_1[3]
             EndIf
			 
             //WaitForEnter Clave_Id_Track_II[2]
             //WaitForEnter Mid(Clave_Id_Track_II_1[3],1,30)
             //26Mar2015

             if Clave_Id_Track_II_1[3] = "ALSEAWOW" Or Clave_Id_Track_II_1[3] = "ALSE" // test 10/08/2015
                else
                    InfoMessage "Tarjeta no es WOW" //Test 10/08/2015          
                    ExitCancel
                endif // test 10/08/2015
                 
             If Mid(Clave_ID,1,1) = ";" And Mid(Clave_ID,16,1) = "?"
                Format No_tarjeta     As Mid(Clave_ID,2,14)
                Format Campo_Busqueda As No_tarjeta
             ElseIf Mid(Clave_ID,1,1) = ";" And Len(Clave_ID_Paso2) > 0
                Format No_tarjeta     As Mid(Clave_ID,2,16)
                Format Campo_Busqueda As No_tarjeta
                //WaitForEnter Campo_Busqueda
                //26Mar2015
             ElseIf Clave_Id_Track_II_1[3] = "ALSEAWOW" Or Clave_Id_Track_II_1[3] = "ALSE"
                Var Len1  : N4 = 0
                Len1 = ( Len(Clave_ID_Paso1) - 1 )
                Format No_tarjeta     As Clave_ID_Paso1
                Format Campo_Busqueda As No_tarjeta
                //WaitForEnter "ALSEAWOW2"
                //WaitForEnter Campo_Busqueda
                //26Mar2015
             Else
                Format No_tarjeta     As Mid(Clave_ID,30,1)
                Format No_tarjeta     As No_tarjeta, Mid(Clave_ID,32,2)
                Format No_tarjeta     As No_tarjeta, Mid(Clave_ID, 7,1)
                Format No_tarjeta     As No_tarjeta, Mid(Clave_ID, 8,8)
                Format No_tarjeta     As No_tarjeta, Mid(Clave_ID,34,4)
                Format Campo_Busqueda As No_tarjeta
             EndIf
          Else
          metodo_entrada = "Smartphone" // test 10/08/2015
             Campo_Busqueda = Clave_ID
          EndIf
		  
		  
       ElseIf @RVC = 5  //5  este solo hace la solicitud manual de miembro
          ForEver
             TouchScreen Numero_TouchscreenN
             Input Clave_ID, "Digite # Tarjeta CRM WOW"
             If @MAGSTATUS = "Y"
                InfoMessage "ERROR DE CAPTURA", "DIGITE MANUALMENTE EL NUMERO DE TARJETA"
             Else
             	 Campo_Busqueda = Clave_ID
          	    Break
             EndIf
       	 EndFor
       EndIf
    EndIf
    
    //WaitForEnter  No_tarjeta
    //ExitCancel
    
    If Len(Trim(Campo_Busqueda)) = 0
       Call Salir_Continua
    EndIf
    
    //03Jun2015  numero_Pos, accion, datos_wsid
    Tarjeta_wow = Campo_Busqueda
    Call Consulta_Miembro_Ocupa_Pos(Tarjeta_wow) 
    //WaitForenter "Consulta_Miembro_Ocupa_Pos"
    //WaitForenter numero_Pos
    //WaitForenter datos_wsid
    If numero_Pos = 0 and banderainfowow = 0
       Call Inserta_Miembro_Ocupa_Pos(Tarjeta_wow, Wsid_wow)
    ElseIf Wsid_wow = numero_Pos or banderainfowow = 1
       //Continua si es la misma Terminal
    Else
       InfoMessage "CRM WOW", "YA EXISTE TARJETA LIGADA A UN CHEQUE EN ESTE MOMENTO"
       ExitCancel
    EndIf
    //03Jun2015
    
		
	//12Nov2015 ====================	
	
	if No_tarjeta = "0"
		format No_Tarjeta_WOW as Clave_ID
		
		//infomessage "Tarjeta WOW por QR", No_Tarjeta_WOW
	
	else
		format No_Tarjeta_WOW as No_tarjeta
	
		//infomessage "Tarjeta WOW", No_Tarjeta_WOW
	endif
	
	//infomessage No_tarjeta_WOW
	
	//infomessage "Clave QR", Clave_ID
	
	//12Nov2015 ====================
	
EndSub
Sub Salir_Continua
    ExitContinue
EndSub
Sub Guarda_InfoLines
    Var I   : N4 = 0
    Contador_InfoLines_A = 0
    FOR I = 1 to @NUMDTLT
        If @DTL_NUMDTL[I] = "I"
           Contador_InfoLines_A = ( Contador_InfoLines_A + 1 )
           InfoLines_Save[Contador_InfoLines_A] = 
        ENDIF
    ENDFOR
EndSub
Sub Manda_Cliente_Nombre
    Manda = ""
    
    //10Nov2014
    Call Graba_Miembro_O(Campo_Busqueda)
    //10Nov2014
    
	//infomessage "Campo_Busqueda", Campo_Busqueda
	
    //17DIc2014
    Numero_Tarjeta = Campo_Busqueda
    //17DIc2014
   
	
    Format Manda, Separador as "CLN", Campo_Busqueda, Business_Date_Real, Tienda, @RVC, @CKNUM
    TxMsg Manda
    GetRxmsg "Esperando Info. Cliente WOW"
EndSub
Sub Valida_Respuesta
	Var Campo_01_ : A50 = ""
    Var Campo_02_ : A50 = ""
    Salida_Consulta_Clientes = ""
    Format Campo_01_ as "<WS_TIMEOUT>"
    Format Campo_02_ as "<Success>"
    
	//infomessage "Error:", mid(trim(@RxMsg),1,26)// <=======10/12/2015 JMG
	
    If Error_Time_Out_SbuxCard = "E" or Error_Time_Out_SbuxCard = "F"
       //26Dic2011 Format Respuesta as @RxMsg
       //26Dic2011 Call   Graba_Archivo_datos(Respuesta)
       //26Dic2011 Call   Graba_Off_Line_CRM
       //26Dic2011 ExitCancel
    EndIf
    If Mid(@RxMsg, 1, 32 ) = "invokeButton_Automatico (Error):"    
	   If @RVC = 2
          LoadKyBdMacro Key (19,102)
          //20Dic2011 InfoMessage Mid(@RxMsg, 33, 20 )
          Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
          Salida_Consulta_Clientes = "T"
       Else
          //20Dic2011 ExitWithError Mid(@RxMsg, 33, 20 )
          Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
       EndIf
       Call Graba_Off_Line_CRM
       ExitCancel
    EndIf
	
    If Mid(Trim(@RxMsg),1,17) = "NO EXISTE MIEMBRO"
       //26Dic2014
       Split @RxMsg, "|", Resp_Red[1], Resp_Red[2]
       //26Dic2014 WaitForEnter Resp_Red[1]
       //26Dic2014 WaitForEnter Resp_Red[2]
       Format Campo_01_ as "<WS_SOA_ERROR>"
       Format Campo_02_ as "<", Resp_Red[2], ">"
       //26Dic2014
       
       If @RVC = 2
          LoadKyBdMacro Key (19,102)
          InfoMessage Mid(@RxMsg, 1, 32 )
          Salida_Consulta_Clientes = "T"
       Else
       	  //26Dic2014
       	  Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
          ExitCancel
          //26Dic2014
          
       	  //26Dic2014 ExitWithError Mid(@RxMsg, 1, 32 )
       EndIf
	   
    ElseIf Mid(Trim(@RxMsg),1, 26)  = "NO HAY COMUNICACION CON WS"
       
	   Format No_Tarjeta_WOW as No_Tarjeta_WOW_anterior // <=======10/12/2015 JMG
	   
	   //26Dic2014
       Split @RxMsg, "|", Resp_Red[1], Resp_Red[2]
       
       if Resp_Red[2] = ""

       //26Dic2014 WaitForEnter Resp_Red[1]
       //26Dic2014 WaitForEnter Resp_Red[2]
            Format Campo_01_ as "<WS_OFFLINE>"// test 10/08/2015
            Format Campo_02_ as "<Success>"// test 10/08/2015
       //26Dic2014
		
		 Call Graba_Off_Line_CRM
		 
		if infoSocio = 0
		offline=1//test 10/08/2015
			ClearChkInfo //test 10/08/2015
            LoadKyBdMacro Key(1, 327681) //manda pantalla compra//test 10/08/2015
			Format Campo as " Offline "//test 10/08/2015
            SaveChkInfo Campo//test 10/08/2015 
            Format Campo as "off ",Clave_ID_TrackII//test 10/08/2015
            SaveChkInfo Campo//test 10/08/2015
			infoMessage "Modo Offline"
		endif

	    ExitContinue //test 10/08/2015
	   
       //26Dic2014 Format Campo_01_ as "<WS_TIMEOUT>"
       //26Dic2014 Format Campo_02_ as "<Success>"
      
	    
       If @RVC = 2
          LoadKyBdMacro Key (19,102)
          //20Dic2011 InfoMessage @RxMsg
		 
          Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
          Salida_Consulta_Clientes = "T"
       Else
         	//20Dic2011 ExitWithError @RxMsg
         	Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
			
              //Input G, "[Enter] para continuar"
			  //waitforenter
		  
       EndIf
	    
			
			//LoadDbKybdMacro 136
			//ExitCancel //test 10/08/2015
			
			else//test 10/08/2015
            Format Campo_01_ as "<WS_SOA_ERROR>"//test 10/08/2015
            Format Campo_02_ as "<", Resp_Red[2], ">"//test 10/08/2015
			
           
            call imprime_ticket_tarjeta_no_inscrita //test 10/08/2015
           
            If @RVC = 2//test 10/08/2015
                LoadKyBdMacro Key (19,102)//test 10/08/2015
	            InfoMessage Mid(@RxMsg, 1, 32 )//test 10/08/2015
    			Salida_Consulta_Clientes = "T"//test 10/08/2015
            Else//test 10/08/2015
                Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
                Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
                ExitCancel//test 10/08/2015
            EndIf//test 10/08/2015
        endif //test 10/08/2015
			
       
    ElseIf Trim(@RxMsg) = "_timeout"
       Format Campo_01_ as "<WS_TIMEOUT>"
       Format Campo_02_ as "<Success>"
	   Call Graba_Off_Line_CRM
       If @RVC = 2
          LoadKyBdMacro Key (19,102)
          //20Dic2011 InfoMessage Mensaje_WS_Windows
          Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
          Salida_Consulta_Clientes = "T"
       Else
       	  //20Dic2011 ExitWithError Mensaje_WS_Windows
       	  Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
       EndIf
        if infoSocio = 0
		offline=1//test 10/08/2015
			ClearChkInfo //test 10/08/2015
            LoadKyBdMacro Key(1, 327681) //manda pantalla compra//test 10/08/2015
            Format Campo as " Offline "//test 10/08/2015
            SaveChkInfo Campo//test 10/08/2015
            Format Campo as "off ",Clave_ID_TrackII//test 10/08/2015
            SaveChkInfo Campo//test 10/08/2015
		endif
	   //ExitContinue //test 10/08/2015
       ExitCancel //test 10/08/2015
    EndIf
    Format Respuesta as @RxMsg
    Call   Graba_Archivo_datos(Respuesta)
EndSub

Sub Separa_Campos
    Var JJ                        : N3 = 0
    Var KK                        : N3 = 0
    Var AnioDate                  : N4 = 0
    Var AnioDate_A                : A4 = ""
    Var BirthDate2                : A30 = ""
    Var FN002                     : N6 = 0
    Call Borra_Array
    BirthDate = ""
    
    //WaitForEnter "Inicio 11"
    
   Split Respuesta, "|", NumeroMiembro, Nombre, Activo, Nivel, Organizacion, Nivel_Id, Nivel_nombre, \
                       Contador_Campos,  PromocionId[]: PromocionNombre[]: PromocionUniqueKey[]: \
                                         PromocionDisplayName[]: PromocionStatus[], \
                       Contador_Voucher, VoucherCalStatus[]: VoucherProductId[]: VoucherProductName[]: \
                                         VoucherStatus[]: VoucherTransactionId[]: validDiscountAmount[]:  validDiscountType[]: minimumAmount[], \
                       BirthDate, FavoriteProduct, LastBeverage, LastFood, LifetimePoint1Value, LifetimePoint2Value, \
                       Status, AttributeValue, ErrorCode, pointAmount
    If Len(Activo) = 0
       Activo = Status
    EndIf

   
	puntoswow= LifetimePoint2Value
	

    //WaitForEnter "Contador_Voucher"
    //WaitForEnter Contador_Voucher
    //For I = 1 to 20
        //If Len(Trim(VoucherCalStatus[I])) > 0 And Len(Trim(VoucherProductName[I])) > 0
           //WaitForEnter VoucherCalStatus[I]
           //WaitForEnter VoucherProductId[I]
           //WaitForEnter VoucherProductName[I]
           //WaitForEnter VoucherStatus[I]
           //WaitForEnter VoucherTransactionId[I]
           //WaitForEnter validDiscountAmount[I]
           //WaitForEnter validDiscountType[I]
        //ENDIF
    //EndFor
    
    AttributeValue = "Always" 
    
    //10Nov2014
    //10Nov2014 Var Marcado_Crm  : A1 = "1"  //1=Marcado como CRM
    //10Nov2014 Call Inserta_Crm_users( NumeroMiembro, Saldo, Marcado_Crm, Respuesta )
    //10Nov2014
    
    //FOpen  FN002, "BirthDate.TxT", Write
    //FWrite FN002, BirthDate2
    //FClose FN002

    //BirthDate = ""
    //FOpen  FN002, "BirthDate.TxT", Read
    //FRead  FN002, BirthDate
    //FClose FN002

    //WaitForEnter BirthDate     //08/06/2011

    //WaitForEnter FavoriteProduct
    //WaitForEnter VoucherTransactionId[Contador_Voucher]

    If Len(Trim(BirthDate)) > 0
       MesBirthDate_A = Mid(BirthDate, 1, 2)
       MesBirthDate   = MesBirthDate_A
       DiaBirthDate_A = Mid(BirthDate, 4, 2)
       DiaBirthDate   = DiaBirthDate_A
    ENDIF
    Format AnioBirthDate_A as "20", @YEAR{02}
    AnioBirthDate           = AnioBirthDate_A

//WaitForEnter "Aqui"
    //WaitForEnter AnioBirthDate  //
    //WaitForEnter AttributeValue //
    //WaitForEnter BirthDate      //
    //WaitForEnter MesBirthDate   //
    
    Format AnioDate_A as "20", @YEAR{02}
    AnioDate = AnioDate_A
    if @MONTH = MesBirthDate And DiaBirthDate > @DAY
       AnioDate = ( AnioDate )
    Else
    	 AnioDate = ( AnioDate + 1 )
    EndIf
    Format Fecha_Cumplanios_A as "20", @YEAR{02}, "-", @MONTH{02}, "-", @DAY{02}
    Format Fecha_Cumplanios_D as AnioDate, "-", MesBirthDate, "-", DiaBirthDate
    
    //WaitForenter "AttributeValue"
    //WaitForenter AttributeValue
    //WaitForEnter AnioBirthDate_A
    //WaitForenter @MONTH
    //WaitForenter MesBirthDate
    //WaitForEnter Contador_Voucher  //3
    //WaitForEnter Contador_Campos
    
        
    If Len(Trim(AttributeValue)) > 0
       //20Nov2014 If @MONTH = MesBirthDate And AnioBirthDate_A = Trim(AttributeValue)
       If AnioBirthDate_A = Trim(AttributeValue)
          For JJ = 1 to Contador_Campos
              If Trim(PromocionNombre[JJ]) = "Beb Cumple"
                 //20Nov2014 PromocionNombre[JJ] = " "
              EndIf
          EndFor
          For JJ = 1 to Contador_Voucher
              If Trim(VoucherProductName[JJ]) = "Beb Cumple"
                 //20Nov2014 VoucherProductName[JJ] = " "
              EndIf
          EndFor
       Else
       EndIf
    Else
       If @MONTH <> MesBirthDate
          For JJ = 1 to Contador_Campos
              If Trim(PromocionNombre[JJ]) = "Beb Cumple"
                 //20Nov2014 PromocionNombre[JJ] = " "
              EndIf
          EndFor
          For JJ = 1 to Contador_Voucher 
              If Trim(VoucherProductName[JJ]) = "Beb Cumple"
                 //20Nov2014 VoucherProductName[JJ] = " "
              EndIf
          EndFor
       Else
       EndIf
    EndIf
EndSub

Sub Carga_Miembro_Cheque
    Var Conta  : N3  = 0
    Var Puntos_: N3  = 0
    
    //08Ene2015 WaitForEnter "1"
    //08Ene2015 WaitForEnter Consulta_Pantalla
    
    //08Ene2015 Call Muestra_datos_consulta_pantalla
    ////08Ene2015 Call Lee_Privilegios_Nivel
    
   
    If Consulta_Pantalla <> "S"
       ClearChkInfo
    Else
    	Window 12,48
    EndIf



//  Format Campo as Mid(NumeroMiembro, 1, 24)
//  SaveChkInfo Campo
    
    Format Campo as Mid(Nombre, 1, 24)
    Call Carga_Screen_Cheque
    
    //WaitForEnter "Activo"
    //WaitForEnter Activo
    
    
    Format Campo as Trim(Activo), "/", Trim(Nivel), "", Mid(BirthDate,1,5)  //New , " ", Nivel_nombre
    Call Carga_Screen_Cheque

    //New Format Campo as "Dias x cumple:", Diasparacumpleanios{03}
    //New SaveChkInfo Campo
    If Len(trim(Saldo)) > 0
       Format Campo as "Saldo ", Saldo
       Call Carga_Screen_Cheque
    EndIf
    
    
    //16Ene2015 Format Campo as "SOCIO WOW REWARDS MEXICO"
    //16Ene2015 Call Write_Leyenda_CRM(Campo)
    
    
    //16Ene2015 If LifetimePoint1Value > 0
       //17Dic2014
       //17Dic2014 Call Actualiza_Stars(LifetimePoint1Value)
       //17Dic2014
       //16Ene2015 Format Campo as "Points antes de esta compra; ", LifetimePoint1Value
       //WaitForEnter "LifetimePoint1Value"
       //WaitForEnter LifetimePoint1Value
       //16Ene2015 Call Carga_Screen_Cheque
       //16Ene2015 Call Write_Leyenda_CRM(Campo)
       //16Ene2015 Call Write_Leyenda_CRM_Cheque(Campo)
    //16Ene2015 EndIf
    
    //16Ene2015
    
    //27Ene2015 If LifetimePoint2Value >= 0
       Format Campo as "Points antes de esta compra; ", LifetimePoint2Value
       Call Carga_Screen_Cheque
       Call Write_Leyenda_CRM_Cheque(Campo)
    //27Ene2015 EndIf
    
    //21Jul2015
    Format Campo as "Pesos: ", pointAmount
    Call Carga_Screen_Cheque
    LifetimePoint2Value = pointAmount
    //21Jul2015
    
    //16Ene2015
    
    If Len(Trim(FavoriteProduct)) > 0
       //06Nov2014 Format Campo as "P:", Mid(FavoriteProduct, 1, 22)
       //06Nov2014 Call Carga_Screen_Cheque
    EndIf
    
    If Len(Trim(LastBeverage)) > 0
       Format Campo as "B:", Mid(LastBeverage,    1, 22)
       Call Carga_Screen_Cheque
    EndIf
    
    If Len(Trim(LastFood)) > 0
       Format Campo as "F:", Mid(LastFood,        1, 22)
       Call Carga_Screen_Cheque
    EndIf
    
    Contador_Redenciones = 0
    ClearArray Redencion_Aplicada
    
    //WaitForEnter VoucherCalStatus[1]
    //WaitForEnter VoucherProductName[1] 
    //WaitForEnter VoucherStatus[1]
    
    For I = 1 to 11
    
        //WaitForEnter I
        //WaitForEnter VoucherStatus[I]
        //WaitForEnter VoucherProductName[I]
        
        If Trim(VoucherStatus[I]) = "Available" And Len(Trim(VoucherProductName[I])) > 0 
           Format Campo as " ", Mid(VoucherProductName[I], 1, 17)
           Format Campo_ as Mid(VoucherProductName[I], 1, 16)
           //WaitForEnter MesBirthDate   //1
           //WaitForEnter AnioBirthDate_A  //2012
           //WaitForEnter Trim(AttributeValue)  //
           //20Nov2014 If Trim(VoucherProductName[I]) = "Beb Cumple"
              //20Nov2014 If @MONTH = MesBirthDate //03Ene2012 And AnioBirthDate_A <> Trim(AttributeValue)
                 //20Nov2014 If Len(Trim(AnioBirthDate_A)) = 0 
                    Format AnioBirthDate_A as "20", @YEAR{02}
                 //20Nov2014 EndIf
                 //20Nov2014 If AnioBirthDate_A <> Trim(AttributeValue) 
                    Call Guarda_en_Cheque
                 //20Nov2014 EndIf
              ENDIF
           Else
           	  Puntos_ = 0
           	  //WaitForEnter "Inicio"
              //03Ene2012 Call Carga_Miembro_Cheque_Nivel(Campo_, Puntos_)  
              Call Carga_Miembro_Cheque_Nivel_Screen(Campo_, Puntos_)  
              //WaitForEnter Campo_
              //WaitForEnter Puntos_
              If Puntos_ = 0
           	     Call Guarda_en_Cheque
           	  EndIf
           ENDIF
        ENDIF
    EndFor
    //03Ene2012 Call Carga_Miembro_Cheque_Nivel_Dos
    If Conta > 0 
       Contador_Redenciones = Conta
       Call Graba_Privilegios_Nivel_Miembro
    EndIf
EndSub
Sub Carga_Screen_Cheque
    Var Campo_  : A100 = ""
    Var Campo2  : A100 = ""
    Var Camp_01 : A20  = ""
    Var Camp_02 : A20  = ""
    If Mid(Trim(Campo),1,5) = "Stars" Or Mid(Trim(Campo),1,5) = "Point"
       Format Campo_ As Mid(Campo, 1, 6), ":", Trim(Mid(Campo, 29, 10))  //, " Mto:", Trim(pointAmount)
       //Format Campo_ As "Pesos:", Trim(pointAmount), " ", Mid(Campo, 1, 6), ":", Trim(Mid(Campo, 29, 10))
       //WaitForEnter Campo_
       Campo = Campo_
       //Return
       
       //16ENE2015
       Call Borra_Leyenda_CRM
       //16Ene2015 WaitForenter Campo  //Points 
       Format Campo2 as "==============================="
	   Call Write_Leyenda_CRM(Campo2)
	   Format Campo2 as "        Felicidades por"
       Call Write_Leyenda_CRM(Campo2)
       Format Campo2 as "   pertenecer a Wow Rewards"
       Call Write_Leyenda_CRM(Campo2)
       
       Split Campo, ":", Camp_01, Camp_02
       Format Campo2 as "   SALDO ANTERIOR PUNTOS: ", Trim(Camp_02)
       Call Write_Leyenda_CRM(Campo2)
       
    ElseIf Mid(Trim(Campo),1,5) = "Pesos"
       Split Campo, ":", Camp_01, Camp_02
       Format Campo2 as "   SALDO ANTERIOR PESOS: ", Trim(Camp_02)
       Call Write_Leyenda_CRM(Campo2)
       Format Campo2 as " ============================="
	   Call Write_Leyenda_CRM(Campo2)
	   
    EndIf
    If Consulta_Pantalla <> "S"
       SaveChkInfo Mid(Trim(Campo),1,20)
    Else
    	 Call Muestra_Datos_Pantalla(Campo)
    EndIf
EndSub
Sub Carga_Miembro_Cheque_Pantalla
    Var Paso_01 : A30 = ""
    Var Conta   : N3  = 0
    Var Puntos_ : N3  = 0
    
    //WaitForEnter "2"
    
    Call Muestra_datos_consulta_pantalla
    //Call Lee_Privilegios_Nivel
    Window 12, 48
    Format Campo as Mid(Nombre, 1, 24)
    Call Muestra_Datos_Pantalla(Campo)
    Format Campo as Activo, " ", Nivel  //New , " ", Nivel_nombre
    Call Muestra_Datos_Pantalla(Campo)
    If Len(trim(Saldo)) > 0
       Format Campo as "Saldo ", Saldo
           Call Muestra_Datos_Pantalla(Campo)
    EndIf
    
    
    Format Campo as "SOCIO WOW REWARDS MEXICO"
    Call Write_Leyenda_CRM(Campo)
    
    
    If Len(Trim(LifetimePoint1Value)) > 0
       Format Campo as "Points: ", Trim(Mid(LifetimePoint1Value, 1, 22))
       //WaitForEnter "23"
       //WaitForEnter LifetimePoint1Value
       Call Carga_Screen_Cheque
       Call Write_Leyenda_CRM(Campo)
    EndIf
    
    If Len(Trim(FavoriteProduct)) > 0
       //06Nov2014 Format Campo as "P:", Mid(FavoriteProduct, 1, 22)
       //06Nov2014 Call Muestra_Datos_Pantalla(Campo)
    EndIf
    
    If Len(Trim(LastBeverage)) > 0
       Format Campo as "B:", Mid(LastBeverage,    1, 22)
       Call Muestra_Datos_Pantalla(Campo)
    EndIf
     
    If Len(Trim(LastFood)) > 0
       Format Campo as "F:", Mid(LastFood,        1, 22)
       Call Muestra_Datos_Pantalla(Campo)
    EndIf
    
    Contador_Redenciones = 0
    ClearArray Redencion_Aplicada
    
    //WaitForEnter VoucherCalStatus[1]
    //WaitForEnter VoucherProductName[1]
    //WaitForEnter VoucherCalStatus[2]
    //WaitForEnter VoucherProductName[2]
    
    For I = 1 to 20
        If Trim(VoucherStatus[I]) = "Available" And Len(Trim(VoucherProductName[I])) > 0 
           Format Campo as "", Mid(VoucherProductName[I], 1, 17)
           Format Campo_ as Mid(VoucherProductName[I], 1, 16)
           If Trim(VoucherProductName[I]) = "Beb Cumple"
              If @MONTH = MesBirthDate And AnioBirthDate_A <> Trim(AttributeValue)
                 //WaitForEnter "Beb Cumple P"
                 Call Muestra_Datos_Pantalla(Campo)
              ENDIF
           Else
           	  Puntos_ = 0
              //03Ene2012 Call Carga_Miembro_Cheque_Nivel(Campo, Puntos_)
              Call Carga_Miembro_Cheque_Nivel_Screen(Campo, Puntos_)
              If Puntos_ = 0
           	     Call Muestra_Datos_Pantalla(Campo)
           	  EndIf
           ENDIF
        ENDIF
    EndFor
    //03Ene2012 Call Carga_Miembro_Cheque_Nivel_Dos_Pantalla
    TouchScreen Numero_TouchscreenNueva
    Input Paso_01, "Enter/Clear Para Continuar"
    WindowClose
EndSub
Sub Muestra_Datos_Pantalla(Ref datos_)
    Contador_Lineas_Pantalla = ( Contador_Lineas_Pantalla + 1 )
    If Contador_Lineas_Pantalla < 13
       Display Contador_Lineas_Pantalla, 1, datos_
    EndIf
EndSub
Sub Carga_Miembro_Cheque_Nivel(ref Campo__, ref Puntos__)
    Var IO   : N3 = 0
    Var Ptos : N3 = 0
    //WaitForEnter Campo__
    For IO = 1 to 10
        If Len(Trim(RewardName_A[IO])) > 0
           //WaitForEnter RewardName_A[IO]
           If Trim(RewardName_A[IO]) = Campo__
              If Trim(RewardTypeDiscount_P_M_A[IO]) = "Redeem"
                 Ptos = RewardPoints[IO]
                 //WaitForEnter LifetimePoint2Value  //4
                 //WaitForEnter Ptos   //15
                 If LifetimePoint2Value >= Ptos
                    Format Campo  as "", Mid(RewardName_A[IO], 1, 16)
                    Format Campo_ as Mid(RewardName_A[IO], 1, 16)
                    Puntos__ = Ptos
                    //WaitForEnter Puntos__
                    Call Guarda_en_Cheque
                    Return
                 Else
              	    Puntos__ = LifetimePoint2Value
                 EndIf
              Else
             	  Puntos__ = 0
              EndIf
           Else
           	  Puntos__ = 0
           EndIf
        EndIf
    EndFor
EndSub
Sub Carga_Miembro_Cheque_Nivel_Dos
    Var IO  : N3 = 0
    For IO = 1 to 10
        If Len(Trim(RewardName_A[IO])) > 0
           If Trim(RewardTypeDiscount_P_M_A[IO]) <> "Points"
              Format Campo  as "Vou.N ", Mid(RewardName_A[IO], 1, 16)
              Format Campo_ as Mid(RewardName_A[IO], 1, 16)
              //20Oct Call Guarda_en_Cheque
           EndIf
        EndIf
    EndFor
EndSub
Sub Carga_Miembro_Cheque_Nivel_Dos_Pantalla
    Var IO  : N3 = 0
    For IO = 1 to 10
        If Len(Trim(RewardName_A[IO])) > 0
           If Trim(RewardTypeDiscount_P_M_A[IO]) <> "Points"
              Format Campo  as "Vou.N ", Mid(RewardName_A[IO], 1, 16)
              Format Campo_ as Mid(RewardName_A[IO], 1, 16)
              //20Oct Call Muestra_Datos_Pantalla(Campo)
           EndIf
        EndIf
    EndFor
EndSub

Sub Guarda_en_Cheque
    Conta = ( Conta + 1 )
    //WaitForEnter Campo_
    If Consulta_Pantalla = "S"
       //03Ene2012 Call Guarda_en_Cheque_Screen
       Call Muestra_Datos_Pantalla(Campo)
    Else
       If Conta < 12
          Redencion_Aplicada[Conta] = Campo_
          SaveChkInfo Campo
       EndIf
    EndIf
EndSub
Sub Graba_Miembro	
	
	Var    FN001   : N6 = 0
    
    //24Dic2014 
    Var File1       : A100 = ""
    //Format File1   as FileMiembro
    //Call Asigna_Path_Archivos(File1)}
    //FOpen  FN001, File1, Write
    ////24Dic2014 
    
    ////24Dic2014 FOpen  FN001, FileMiembro, Write
    //FWrite FN001, @CKNUM, Respuesta
  ////FWrite FN001, @CKNUM, Respuesta, Contador_Redenciones, Redencion_Aplicada[1], Redencion_Aplicada[2], Redencion_Aplicada[3], Redencion_Aplicada[4], Redencion_Aplicada[5], Redencion_Aplicada[6], Redencion_Aplicada[7], Redencion_Aplicada[8], Redencion_Aplicada[9], Redencion_Aplicada[10]
    //FClose FN001
    
	//-------11 ABR 2016
	Cheque_Miembro = @CKNUM
	Format rspst_Miembro as Respuesta
	//-------
	
	
    ////24Dic2014 
    //Format File1   as FileMiembro_Soa
    //Call Asigna_Path_Archivos(File1)}
    //FOpen  FN001, File1, Write
    ////24Dic2014 
    
    ////24Dic2014 FOpen  FN001, FileMiembro_Soa, Write
    //FWrite FN001, @CKNUM, Respuesta
    //FClose FN001
    
	//-------11 ABR 2016
	Cheque_Miembro_Soa = @CKNUM
	Format rspst_Miembro_Soa as Respuesta
	//-------

Endsub
Sub Lee_Miembro
    Var FN001 : N6 = 0
    Var JJ    : N6 = 0
    
    //WaitForEnter "3"
    
    Call Borra_Array
    
    //24Dic2014 
    Var File1       : A100 = ""
    //Format File1   as FileMiembro
    //Call Asigna_Path_Archivos(File1)}
    //FOpen  FN001, File1, Read
    ////24Dic2014 
    
    ////24Dic2014 FOpen  FN001, FileMiembro, Read
    //IF FN001 = 0
    //   Respuesta = ""
    //   Cheque    = 0
    //   //WaitForEnter "3 Sali"
    //   Return
    //ENDIF
    Contador_Redenciones = 10
    //FRead  FN001, Cheque, Respuesta
    //FClose FN001
	
	//-------11 ABR 2016
	Cheque = Cheque_Miembro
	Format Respuesta as rspst_Miembro
	//-------
	
    ALL = ""
    If Mid(Respuesta,1,3) = "ALL" 
       Split Respuesta, "|", NumeroMiembro, Nombre
       ALL = "ALL"
    Else
       Split Respuesta, "|", NumeroMiembro, Nombre, Activo, Nivel, Organizacion, Nivel_Id, Nivel_nombre, \
                          Contador_Campos,  PromocionId[]: PromocionNombre[]: PromocionUniqueKey[]: \
                                            PromocionDisplayName[]: PromocionStatus[], \
                          Contador_Voucher, VoucherCalStatus[]: VoucherProductId[]: VoucherProductName[]: \
                                            VoucherStatus[]: VoucherTransactionId[]: validDiscountAmount[]:  validDiscountType[] : minimumAmount[], \
                          BirthDate, FavoriteProduct, LastBeverage, LastFood, LifetimePoint1Value, LifetimePoint2Value, \
                          Status, AttributeValue, ErrorCode, pointAmount
    EndIf
    
    //WaitForEnter "Aqui"
    //WaitForEnter NumeroMiembro
    //WaitForEnter Nombre
    //WaitForEnter Activo
    //WaitForEnter Nivel
    
    If Len(Trim(BirthDate)) > 0
       MesBirthDate_A = Mid(BirthDate, 1, 2)
       MesBirthDate   = MesBirthDate_A
    ENDIF
    If @MONTH <> MesBirthDate
       For JJ = 1 to Contador_Campos
       //waitforenter PromocionNombre[JJ]
           If Trim(PromocionNombre[JJ]) = "Beb Cumple"
              PromocionNombre[JJ] = ""
           Else
           EndIf
       EndFor
       For JJ = 1 to Contador_Voucher
       //waitforenter VoucherProductName[JJ]
           If Trim(VoucherProductName[JJ]) = "Beb Cumple"
              VoucherProductName[JJ] = ""
           Else
           EndIf
       EndFor
     Else
     	//InfoMessage "Cumpleanios1"
    EndIf
    //Split Respuesta, "|", NumeroMiembro, Nombre, Activo, Nivel, Organizacion, Nivel_Id, Nivel_nombre, \
    //                   Contador_Campos,  PromocionId[]: PromocionNombre[]: PromocionUniqueKey[]: \
    //                                     PromocionDisplayName[]: PromocionStatus[], \
    //                   Contador_Voucher, VoucherCalStatus[]: VoucherProductId[]: VoucherProductName[]: \
    //                                     VoucherStatus[]: VoucherTransactionId[], \
    //                   Contador_Redenciones, Redencion_Aplicada[]
Endsub
Sub Borra_Miembro
    //Var    FN001   : N6 = 0
    
    ////24Dic2014 
    //Var File1       : A100 = ""
    //Format File1   as FileMiembro
    //Call Asigna_Path_Archivos(File1)}
    //FOpen  FN001, File1, Write
    ////24Dic2014 
    
    ////24Dic2014 FOpen  FN001, FileMiembro, Write
    //FWrite FN001, " "
    //FClose FN001
    //System "Del Miembro.TxT"
	
    //-------11 ABR 2016
	Cheque_Miembro = 0
	rspst_Miembro = ""
	//-------
	
Endsub
Sub Borra_Miembro_Servicio // Modificado el 15 ABR 2016
//    Var    FN001   : N6 = 0
    
//    //24Dic2014 
//    Var File1       : A100 = ""
//    Format File1   as FileMiembroServicio
//    Call Asigna_Path_Archivos(File1)}
//    FOpen  FN001, File1, Write
//    //24Dic2014 
    
//    //24Dic2014 FOpen  FN001, FileMiembroServicio, Write
//    FWrite FN001, " "
//    FClose FN001
//    System "Del MiembroServicio.TxT"
	
	//------- 15 ABR 2016
	rspst_Miembro_Servicio = ""
    Cheque_Miembro_Servicio = 0
	//-------
	
Endsub
Sub Consulta_Business_Date_SyBase
    Var File_No   : N6 = 0
    Call Version_Micros_Sybase
    Call Conecta_Base_Datos_Sybase
    Format SqlStr as "SELECT  business_date, (SELECT obj_num  FROM micros.rest_def) as rest_def  FROM micros.rest_status"
    IF Version_Micros = 32 or Version_Micros = 41
       DLLCALL_CDECL hODBCDLL, sqlGetRecordSet(SqlStr)
       SqlStr1 = ""
       DLLCALL_CDECL hODBCDLL, sqlGetLastErrorString(ref SqlStr1)
       IF (SqlStr1 <> "")
           Call Muestra_Error(SqlStr1)
           Call UnloadDLL_SyBase
           ExitCancel
       ENDIF
       SqlStr1 = ""
       DLLCALL_CDECL hODBCDLL, sqlGetFirst(ref SqlStr1)
    ElseIF Version_Micros = 31
       DLLCall hODBCDLL, sqlGetRecordSet(SqlStr)
       SqlStr1 = ""
       DLLCall hODBCDLL, sqlGetLastErrorString(ref SqlStr1)
       IF SqlStr1 <> ""
          Call Muestra_Error(SqlStr1)
          Call UnloadDLL_SyBase
          ExitCancel
       ENDIF
       SqlStr1 = ""
       DLLCall hODBCDLL, sqlGetFirst(ref SqlStr1)
    ENDIF
    IF SqlStr1 = ""
       Business_Date = ""
    Else
       IF Len(Trim(SqlStr1)) > 0 Then
          Split SqlStr1, ";", Business_Date, Tienda
          IF Version_Micros = 31
             Format DiaB  as Mid(Business_Date, 4, 2)
             Format MesB  as Mid(Business_Date, 1, 2)
             Format AnioB as Mid(Business_Date, 7, 4)
             Format Business_Date_Real as Mid(Business_Date,1,10)
             Format Business_Date      as AnioB, "-" , MesB, "-" , DiaB
             Format Business_Date      as MesB, "/" , DiaB, "/" , AnioB, " ", @HOUR{02}, ":", @MINUTE{02}, ":", @SECOND{02}
          Else
             Format DiaB  as Mid(Business_Date, 9, 2)
             Format MesB  as Mid(Business_Date, 6, 2)
             Format AnioB as Mid(Business_Date, 1, 4)
             Format Business_Date_Real as Mid(Business_Date,1,10)
             Format Business_Date      as AnioB, "-" , MesB, "-" , DiaB
             Format Business_Date      as MesB, "/" , DiaB, "/" , AnioB, " ", @HOUR{02}, ":", @MINUTE{02}, ":", @SECOND{02}
          ENDIF
       ENDIF
    ENDIF
    Call UnloadDLL_SyBase
EndSub
// ***************************************************************************
Sub Continuacion(var A: N5, var B: N11)
    LoadKyBdMacro Key(24, (16384 * B) + A)
EndSub
// ***************************************************************************
Sub Version_Micros_SyBase
    Version_Micros = 0
    Format Version_Micros_A as Mid(Trim(@version), 1,1), Mid(Trim(@version), 3,1)
    Version_Micros =       Version_Micros_A
    If Version_Micros = 52
       Version_Micros = 41
    Else
       Version_Micros = 41
    EndIf
EndSub
Sub Conecta_Base_Datos_Sybase
    Call Load_ODBC_DLL_SyBase
    Call ConnectDB_SyBase
EndSub
Sub Load_ODBC_DLL_SyBase
    IF hODBCDLL <> 0
       hODBCDLL  = 0
    ENDIF
    IF hODBCDLL = 0
        IF Version_Micros = 32 OR Version_Micros = 41
           IF @WSTYPE = 3
              DLLFree hODBCDLL
              DLLLoad hODBCDLL, "\cf\micros\bin\MDSSysUtilsProxy.dll"
           Else
              DLLFree hODBCDLL
              DLLLoad hODBCDLL, "MDSSysUtilsProxy.dll"
           ENDIF
        ElseIF Version_Micros = 31
           DLLLoad hODBCDLL, "SimODBC.DLL"
        ENDIF
    ENDIF
    IF     hODBCDLL = 0 and Version_Micros = 32
       ExitWithError "No se Puede Cargar DLL (MDSSysUtilsProxy.DLL)"
    ElseIF hODBCDLL = 0 and Version_Micros = 41
       ExitWithError "No se Puede Cargar DLL (MDSSysUtilsProxy.DLL)"
    ElseIF hODBCDLL = 0 and Version_Micros = 31
       ExitWithError "No se Puede Cargar DLL (SimODBC.DLL)"
    ENDIF
EndSub
Sub ConnectDB_SyBase
    Var Status : N1
    IF Version_Micros = 32
       DLLCALL_CDECL hODBCDLL, sqlIsConnectionOpen(ref Status)
       IF Status =  0
          //DLLCALL_CDECL hODBCDLL, sqlInitConnection("micros","ODBC;UID=custom;PWD=SimRHFE.11", "")
          DLLCALL_CDECL hODBCDLL, sqlInitConnection("micros","ODBC;UID=custom;PWD=custom", "")
       ENDIF
    ElseIF Version_Micros = 41
       DLLCALL_CDECL hODBCDLL, sqlIsConnectionOpen(ref Status)
       IF Status =  0
          //DLLCALL_CDECL hODBCDLL, sqlInitConnection("micros","ODBC;UID=custom;PWD=SimRHFE.11", "")
          DLLCALL_CDECL hODBCDLL, sqlInitConnection("micros","ODBC;UID=custom;PWD=custom", "")
       ENDIF
    ElseIF  Version_Micros = 31
       DLLCall hODBCDLL, sqlIsConnectionOpen(ref Status)
       IF Status =  0
          //08Ene2015 DLLCall hODBCDLL, sqlInitConnection("micros","UID=custom;PWD=SimRHFE.11")
          DLLCall hODBCDLL, sqlInitConnection("micros","UID=custom;PWD=custom")
       ENDIF
    ENDIF
EndSub
Sub UnloadDLL_SyBase
    IF     Version_Micros = 32
       DLLCALL_CDECL hODBCDLL, sqlCloseConnection()
    ElseIF Version_Micros = 31
       DLLCall hODBCDLL, sqlCloseConnection()
    ElseIF Version_Micros = 41
       DLLCALL_CDECL hODBCDLL, sqlCloseConnection()
    ENDIF
    DLLFree  hODBCDLL
    hODBCDLL = 0
EndSub
Sub Muestra_Error(Ref Datos_w)
    Window 5,78
           Display 1, 1, Mid(Datos_w,   1, 78)
           Display 2, 1, Mid(Datos_w,  79, 78)
           Display 3, 1, Mid(Datos_w, 156, 78)
           Display 4, 1, Mid(Datos_w, 234, 78)
           TouchScreen Numero_TouchscreenN 
           Input G, "[Enter] para Continuar"
    WindowClose
EndSub
Sub Borra_Array
    ClearArray PromocionId
    ClearArray PromocionNombre
    ClearArray PromocionUniqueKey
    ClearArray PromocionDisplayName
    ClearArray PromocionStatus

    ClearArray VoucherCalStatus
    ClearArray VoucherProductId
    ClearArray VoucherProductName
    ClearArray VoucherStatus
    ClearArray VoucherTransactionId
    ClearArray validDiscountAmount
    ClearArray validDiscountType
    ClearArray minimumAmount
    ClearArray Redencion_Aplicada
EndSub
Sub Muestra_Miembro_Consulta
    Window 12,78
           Display  1, 1, NumeroMiembro, "  Nombre: ", Nombre
           Display  2, 1, Activo, "   Nivel : ", Nivel, "   Organizacion: ", Organizacion
           
           Display  3, 1, Trim(PromocionStatus[1]), "  ", Trim(PromocionNombre[1]), "  ", Trim(PromocionDisplayName[1])
           Display  4, 1, Trim(PromocionStatus[2]), "  ", Trim(PromocionNombre[2]), "  ", Trim(PromocionDisplayName[2])
           Display  5, 1, Trim(PromocionStatus[3]), "  ", Trim(PromocionNombre[3]), "  ", Trim(PromocionDisplayName[3])
           Display  6, 1, Trim(PromocionStatus[4]), "  ", Trim(PromocionNombre[4]), "  ", Trim(PromocionDisplayName[4])
           Display  7, 1, Trim(PromocionStatus[5]), "  ", Trim(PromocionNombre[5]), "  ", Trim(PromocionDisplayName[5])
           
           Display  8, 1, VoucherCalStatus[1], " ", VoucherProductId[1], " ", VoucherProductName[1], " ", VoucherTransactionId[1]
           Display  9, 1, VoucherCalStatus[2], " ", VoucherProductId[2], " ", VoucherProductName[2], " ", VoucherTransactionId[2]
           Display 10, 1, VoucherCalStatus[3], " ", VoucherProductId[3], " ", VoucherProductName[3], " ", VoucherTransactionId[3]
           Display 11, 1, VoucherCalStatus[4], " ", VoucherProductId[4], " ", VoucherProductName[4], " ", VoucherTransactionId[4]
           Display 12, 1, VoucherCalStatus[5], " ", VoucherProductId[5], " ", VoucherProductName[5], " ", VoucherTransactionId[5]
           TouchScreen Numero_TouchscreenN 
           Input G, "[Enter] para Continuar"
    WindowClose
EndSub

//===================================SUBRUTINAS===================================================


//**************************************************************************************************
//**************************************************************************************************
//**************************************************************************************************
//**************************************************************************************************
//**************************************************************************************************
//**************************************************************************************************
/////////////////////Cierra_DLL//////////////////////////////////////////
Sub Cierra_DLL
    IF hODBCDLL <> 0
       DLLFree hODBCDLL
    ENDIF
EndSub
Sub Carga_DLL
    Var File_Paso : A80 = ""
    IF Mid(Version_Micros_A, 1, 1) = "4" or Mid(Version_Micros_A, 1, 1) = "3"
       IF     @WSTYPE = 3  ///WINDOWS CE
          Format File_Paso as Path_CE, Dll_Name
       ELSEIF @WSTYPE = 1  ///WINDOWS 32
          Format File_Paso as Path_Win32, "bin\", Dll_Name
       ELSE
         ExitWithError "FALTA ESPECIFICAR TIPO DE TERMINAL: (PDriver.DLL)"
       ENDIF
    ENDIF
    DLLLoad hODBCDLL, File_Paso
    Format Campo_Log as  "==Carga  DLL (PINPAD)"{40}, "[",hODBCDLL, "]", "[", File_Paso, "]"
    Call Graba_Log
    IF hODBCDLL = 0
       ExitWithError "No se puede cargar DLL o No Existe: (PDriver.DLL)"
    ENDIF
EndSub
Sub Graba_Log
    Var Campo_11   : A1999 = ""
    Var No_Lee     : N4   = 0
    Var No_Graba   : N4   = 0
    Var File_Paso1 : A80  = ""
    Format Campo_11 as @DAY{02}, "/", @MONTH{02}, "/20", @YEAR{02}, "   ", @HOUR{02}, ":", @MINUTE{02}, ":", @SECOND{02}, "  "
    Format Campo_11 as Campo_11, Campo_Log
    No_Lee           = Len(Campo_11)
    IF     @WSTYPE = 3  ///WINDOWS CE
       Format File_Paso1 as Path_CE, File_Log
    ELSEIF @WSTYPE = 1  ///WINDOWS 32
       Format File_Paso1 as Path_Win32, "etc\", File_Log
    ELSE
      ExitWithError "FALTA ESPECIFICAR TIPO DE TERMINAL: (Graba_Log)"
    ENDIF
    
    //24Dic2014 
    Var File1       : A100 = ""
    Format File1   as File_Log
    Call Asigna_Path_Archivos(File1)}
    FOpen  FN00, File1, Append
    //24Dic2014 
    
    //24Dic2014 FOpen    FN00, File_Paso1, Append  
    FwriteLn FN00, Campo_11
    FClose   FN00
EndSub
Sub Envia_Datos_RS232
    sPReturn   = ""
    iDllReturn = 0
    Format Campo_Log as ">>Envia  Datos (prueba_apertura_cheque)"{40}, "[", sCommand, "*", sPReturn, "*", iDllReturn, "]"
    Call Graba_Log
    DLLCall hODBCDLL, prueba_apertura_cheque_null_dos(ref sCommand, ref sPReturn, ref iDllReturn)
    sCommand = ""
    IF Len(sPReturn) > 0
       Format Campo_Log as "<<Recibe Datos (prueba_apertura_cheque)"{40}, "[", sCommand, "*", sPReturn, "*", iDllReturn, "]"
       Call Graba_Log
       Recibe = sPReturn
    ENDIF
EndSub
Sub Despliega_Leyenda_Test_Pinpad
    Window 5,70
           Display 1,1, "sPReturn ", sPReturn
           Display 2,1, "iDllReturn ", iDllReturn
           TouchScreen Numero_TouchscreenN 
           Input G, "[Enter] para Continuar"
    WindowClose
EndSub
Sub Verifica_Item_Seleccionado
    Condimentos          = 0
    MenuItems            = 0
    For I=1 to @NumDTLT
        Grupo          = mid(@DTL_STATUS[I],5,1)
        If Grupo = "A"
           Grupo_Numerico = 10
        ElseIF Grupo = "B"
           Grupo_Numerico = 11
        ElseIF Grupo = "C"
           Grupo_Numerico = 12
        ElseIF Grupo = "D"
           Grupo_Numerico = 13
        ElseIF Grupo = "E"
           Grupo_Numerico = 14
        ElseIF Grupo = "F"
           Grupo_Numerico = 15
        Else
        	 Grupo_Numerico = Grupo
        EndIf
        If @dtl_type[I] = "M" and @dtl_Is_Void[I] = 0
           If @dtl_Selected[I] = 1
           
              //06Feb2015
              //Call Busca_Producto_Promocional_Cheques
              //06Feb2015
              
                If @Dtl_Is_Cond[I]   = 0     //MENU ITEM
                    If @Dtl_Ttl[I]   > 0
                    
                       //Waitforenter @dtl_name[I]  //
                       //Waitforenter @Dtl_Ttl[I]   //
                       
                       Unid_Seleccionado = ( Unid_Seleccionado + 1 )
                       MenuItems = ( MenuItems + 1 )
                       Linea_Cheque = I                       
                       Call Acumula_Item_Seleccionado
                    Else
                  	   //16Ene2015 ExitWithError "NO SE PERMITEN SELECCIONAR ARTICULOS EN 0.00"
                    EndIf
                Else                         //CONDIMENTO
                	  If @Dtl_Is_Cond[I]   = 1 and @Dtl_Ttl[I]   > 0
                	     Unid_Seleccionado = ( Unid_Seleccionado + 1 )
                	     Condimentos       = ( Condimentos + 1 )
                	     Linea_Cheque = I
                	     Call Acumula_Item_Seleccionado
                	  Else
                	  	 If MenuItems = 0
                	  	    If Condimentos = 1
                	  	       //16Ene2015 ExitWithError "SOLO SE PERMITE SELECCIONAR UN SOLO MODIFICADOR."
                	  	    Else
                	  	       //16Ene2015 ExitWithError "NO SE PERMITE SELECCIONAR MODIFICADOR EN 0.00"
                	  	    EndIf
                	  	 EndIf
                	  EndIf
                EndIf
           EndIf
        EndIf
    EndFor
    If MenuItems   > 1
       ExitWithError "SOLO SE PERMITE SELECCIONAR UN SOLO ARTICULO"
    EndIf
    If MenuItems   = 0 And Condimentos > 1
       ExitWithError "SOLO SE PERMITE SELECCIONAR UN SOLO MODIFICADOR"
    EndIf
    If Unid_Seleccionado = 0
       ExitWithError "NO HAY ARTICULO SELECCIONADO"
    EndIf
EndSub
Sub Acumula_Item_Seleccionado
    If      MenuItems = 0 and Condimentos = 1
       Call Acumula_Item_Seleccionado_Item
    ElseIf  MenuItems = 1 and Condimentos = 0
       Call Acumula_Item_Seleccionado_Item
    ElseIf  MenuItems = 1 and Condimentos > 0
       Call Acumula_Item_Seleccionado_Cond_Menu
    EndIf
EndSub
Sub Acumula_Item_Seleccionado_Item
    Item_No_Seleccionado     = ( @dtl_object[I] )
    Item_Nombre_Seleccionado = ( @dtl_name[I]   )
    Item_Qty_Seleccionado    = ( @dtl_Qty[I]    )
    Item_Precio_Seleccionado = ( @dtl_TTL[I]    )
    Sub_Nivel_Seleccionado   = ( @dtl_Slvl[I]   )
    Main_Nivel_Seleccionado  = ( @Dtl_Mlvl[I]   )
    Item_Linea_Seleccionado  = ( I              )
EndSub
Sub Acumula_Item_Seleccionado_Cond_Menu
    Item_Precio_Seleccionado = ( Item_Precio_Seleccionado + @dtl_TTL[I] )
EndSub

Sub Lee_Configuracion_CRM
    Var File_I    : A50 = ""
    Var FN01      : N6  = 0
    Var Paso1     : A80   = ""
    Var Paso2     : A60   = ""
    Var Paso3     : A60   = ""
    Var Paso4     : A60   = ""
    
    CC_Donaciones  = 0
    CC_Leyenda     = 0
    CC_Promocional = 0
    
    IF @WSTYPE = 3
       Format File_I as "\cf\micros\etc\", File
    ELSE
       Format File_I as File
    ENDIF
    
    //24Dic2014 
    Var File1       : A100 = ""
    Format File1   as File
    Call Asigna_Path_Archivos(File1)
    FOpen  FN01, File1, Read
    //24Dic2014 
    
    
    //24Dic2014 Fopen FN01, File_I, Read
    If FN01 = 0 
       ExitWithError "NO EXISTE ARCHIVO DE CONFIGURACION CRM"
    EndIf
    While  Not FEOF(FN01)
         FRead FN01, Paso1, Paso2, Paso3, Paso4
         If     Paso1 = "<TouchScreen Alfanumerica & Numerica & Pago_WOW>"
            Numero_Touchscreen      = Paso2
            Numero_TouchscreenN     = Paso3
            Numero_TouchscreenNueva = Paso4
         ElseIf Paso1 = "<Nombre del Programa>"
            Nombre_Del_Programa = Paso2
         ElseIf Paso1 = "<PartnerName>"
            PartnerName = Paso2
         ElseIf Paso1 = "<Descuento_RedondeoWOW>"
            Descuento_RedondeoWOW   = Paso2
         //07Abr2015
         ElseIf Paso1 = "<Item_Redondeo>"
            Item_Redondeo = Paso2
         //07Abr2015
         ElseIf Paso1 = "<Descuento_Redenciones$>"
            Descuento_Redenciones_M = Paso2
         ElseIf Paso1 = "<Descuento_Redenciones%>"
            Descuento_Redenciones_P = Paso2
         ElseIf Paso1 = "<Tender_Crm>"
            Tender_Crm = Paso2
         ElseIf Paso1 = "<Tender_Efectivo_No>"
            Tender_Efectivo_No = Paso2
         ElseIf Paso1 = "<FormaPagoSbux>"
            FormaPagoSbux = Paso2
         ElseIf Paso1 = "<FormaPagoSbuxOffLine>"
            FormaPagoSbuxOffLine = Paso2
         ElseIf Paso1 = "<Cupon_Desc_P1>"
            Cupon_Desc_P1 = Paso2
         ElseIf Paso1 = "<Cupon_Desc_P2>"
            Cupon_Desc_P2 = Paso2
         ElseIf Paso1 = "<Cupon_Desc_MM>"
            Cupon_Desc_MM = Paso2
         ElseIf Paso1 = "<Leyenda CRM>"
            CC_Leyenda    = ( CC_Leyenda + 1 )
            If CC_Leyenda = 1
               Leyenda_Leida_Crm               = Paso2
            Else
               Leyenda_Leida_Crm_L[CC_Leyenda] = Paso2
            EndIf
         ElseIf Paso1 = "<Donaciones>"
            CC_Donaciones             = ( CC_Donaciones + 1 )
            Donaciones[CC_Donaciones] = Paso2
         ElseIf Paso1 = "<Producto_Promocional>"
            CC_Promocional                       = ( CC_Promocional + 1 )
            Producto_Promocional[CC_Promocional] = Paso2
         EndIf
    EndWhile
    Fclose FN01
    //WaitForConfirm Nombre_Del_Programa
    //WaitForConfirm PartnerName
EndSub

Sub Graba_File
    Var FN002 : N6 = 0
    
    //24Dic2014 
    Var File1       : A100 = ""
    Format File1   as "Salida.TxT"
    Call Asigna_Path_Archivos(File1)}
    FOpen  FN002, File1, Append
    //24Dic2014 
    
    //24Dic2014 FOpen FN002, "Salida.TxT", Append
    FWrite FN002, Respuesta
    FClose FN002
EndSub
Sub Insert_Registro_CRM_Acreditaciones_2
    For II = 1 to @NUMDTLT
        If @dtl_type[II] = "M" And @dtl_Is_Void[II] = 0 And @dtl_Ttl[II] <> 0
           Call Guarda_Datos_Enviar_WS
        EndIf
    EndFor
EndSub
Sub Guarda_Datos_Enviar_WS
    Var Conta : N4 = 0
    Conta = ( Conta + 1 )
    Format Datos_Enviar_WS[Conta], Separador as Business_Date, NumeroMiembro, "NUEVO", Nivel_Price[@dtl_Slvl[II]], Organizacion, "Points", @dtl_Ttl[II], @dtl_object[II], Tienda, NumeroMiembro, PartnerName, "Star", "1", "Ticket", Nombre_Del_Programa, @CKNUM, "Store", Business_Date, "Product", "Accrual", Organizacion
EndSub
//**************************************************************************************************
//**************************************************************************************************
//Acreditaciones Nuevas////////////////
//**************************************************************************************************
//**************************************************************************************************
Sub Main_Busca_Articulos_Enviar_Acreditaciones
    Var SNivel_R        : A10  = ""
    Var Price_R         : $10 = 0
    Var Number_R        : A10 = 0
    Var Nombre_R        : A20 = ""
    Var Unidades_R      : N4  = 0
    Var Cheque_R        : N4  = 0
    Var SNivel_R2       : A10  = ""
    Var Price_R2        : $10 = 0
    Var Number_R2       : A10 = 0
    Var Nombre_R2       : A20 = ""
    Var Unidades_R2     : N4  = 0
    Var Cheque_R2       : N4  = 0
    Var I               : N4  = 0
    Var Cons_Item       : N4  = 0
    Var Primera_Cero    : A1  = "F"
    Var Total_Ticket    : $10 = @TTLDUE
    Var Contador_OP     : N2  = 0
    Var Total_Paso      : $10 = 0
    Lineas_Acreditas_Contador = 0
    
    Condimento_Aplicado = "F"
    Name_FP              = ""
    
    //18Ene2015
    //WaitForenter "Pago"
    Call Busca_Formas_Pago_Acreditaciones_Soa
    //WaitForenter "Aportacion"
    //26Ene2015 Call Busca_Aportacion_Acreditaciones_Soa
    Call Busca_Aportacion_Redencion_Soa   //26Ene2015
    
    //07Jul2015
    ClearArray Objeto_Descto
    ClearArray Nivel_Descto
    ClearArray Precio_Descto
    ClearArray Nombre_Descto
    ClearArray Unidades_Descto
    ClearArray Linea_Descto
    ClearArray SubNivel_Descto
    Call Consolidado_Descuentos_Acreditaciones_Soa_Wow_2
    //07Jul2015
    
    //WaitForenter "Descuentos"
    //07Jul2015 Call Busca_Descuentos_Acreditaciones_Soa
    
     //WaitForenter "Menu"
    Call Busca_MenuItem_Acreditaciones_Soa
    //18Ene2015
    
    
    //New Crm 2014
    //WaitForEnter "Send"
    //WaitForEnter Lineas_Acreditas_Contador
    
    //28Ene2015
    If Lineas_Acreditas_Contador = 0  //NO HAY NADA QUE ENVIAR --> CONTINUA
       ExitContinue
    EndIf
    //28Ene2015

    Call Manda_Acreditacion

    //Msleep (95000)
    //WaitForenter Salir_ACR

    If Salir_ACR = "T"
       Return
    EndIf

    Call Valida_Acreditacion
    //New Crm 2014
    //FP_Sbux_Aplicado_Off_Line = "T"
    //waitforenter FP_Sbux_Aplicado_Off_Line

    If Trim(FP_Sbux_Aplicado_Off_Line) = "T"

       Status_Respuesta = "F"
       Call Read_Off_Line_CRM
       
       If Len(Trim(TicketNumber_Off)) = 0
          If Len(Trim(TicketNumber))  = 0
             Return
          EndIf
       EndIf
       
       If Len(Trim(TicketNumber)) = 0
          Return
       EndIf
      
       Call Manda_Ejecuta_Acreditaciones_OffLine_WS_JAR
    ElseIf Lineas_Acreditas_Contador = 0
       // Salir 
    Else
       Name_Jar = "ACREDITACIONES"
       //New 30Octo2014 Call Manda_Ejecuta_Acreditaciones_Redenciones_WS_JAR(Name_Jar)
       //03NOV2014 Call Manda_Ejecuta_Acreditaciones_Redenciones_WS_Soa
    EndIf
EndSub

Sub Guarda_Campos_Acreditaciones_Cero(Ref SNivel_ , Ref Price_, Ref Number_, Ref Unidades_, Ref Cheque_, Ref Number_R_, Ref Total_Ticket_ )
    MemberNumber         = NumeroMiembro
    ID                   = "NUEVO"
    ALSELevelPrice       = SNivel_
    //08Ene2015 BranchDivision       = "SBX"
    //08Ene2015 ALSEPaymnetMode      = "SBUX Card"
    BranchDivision       = "WOW"
    ALSEPaymnetMode      = "Puntos WOW"
    ActivityDate         = Business_Date
    AdjustedListPrice    = "0"
    Amount               = Total_Ticket_
    Comments             = ""
    //14Ene2015 ItemNumber           = "0"
    ItemNumber           = Number_
    LocationName         = Tienda
    PartnerName          = PartnerName
    PointName            = ""   //"Star"
    Points               = "0"   //"1"
    ProcessDate          = Business_Date
    ProcessingComment    = ""
    ProcessingLog        = ""
    //08Ene2015 ProductName          = "Ticket"
    ProductName          = "WOW Ticket"
    Quantity             = "0"
    IntegrationStatus    = ""
    Status               = ""	
    SubStatus            = ""
    TicketNumber         = TicketNumber
    TransactionChannel   = "Store"
    TransactionDate      = Business_Date
    TransactionSubType   = "Product"
    TransactionType      = "Accrual"
    VoucherNumber        = ""
    VoucherQty           = "1"
    VoucherType          = ""
    //08Ene2015 Organization         = "SBX"
    Organization         = "BKCASUAL"
    Estatus              = "PEN"
    Bussiness_date       = Business_Date
    Centroconsumo        = @RVC
    Cheque               = Cheque_
    Transmitido          = "F"
    Linea_Cheque         = 1
EndSub
Sub Guarda_Campos_Acreditaciones(Ref SNivel_ , Ref Price_, Ref Number_, Ref Unidades_, Ref Cheque_, Ref Number_R_, Ref Total_Ticket_ )
    //SE MODIFICO UNIDADES A UNO
    //18ENe2015 Cons_Item = ( Cons_Item + 1 )
    MemberNumber         = NumeroMiembro
    ID                   = "NUEVO"
    ALSELevelPrice       = SNivel_
    //08Ene2015 BranchDivision       = "SBX"
    //08Ene2015 ALSEPaymnetMode      = "SBUX Card"
    BranchDivision       = "WOW"
    ALSEPaymnetMode      = "Puntos WOW"
    ActivityDate         = Business_Date
    AdjustedListPrice    = Total_Ticket_   //18Ene2015 Price_ 
    Amount               = Total_Ticket_ 
    Comments             = ""
    //ItemNumber           = Number_
    ItemNumber           = Cons_Item
    LocationName         = Tienda
    PartnerName          = PartnerName
    PointName            = ""
    Points               = "0"
    ProcessDate          = Business_Date
    ProcessingComment    = ""
    ProcessingLog        = ""
    ProductName          = Number_R_
    //10nOV2014 Quantity             = Unidades_
    Quantity             = 1
    IntegrationStatus    = ""
    Status               = ""	
    SubStatus            = ""
    TicketNumber         = TicketNumber
    TransactionChannel   = "Store"
    TransactionDate      = Business_Date
    TransactionSubType   = "Product"
    TransactionType      = "Accrual"
    VoucherNumber        = ""
    VoucherQty           = "1"
    VoucherType          = ""
    Organization         = "BKCASUAL"
    Estatus              = "PEN"
    Bussiness_date       = Business_Date
    Centroconsumo        = @RVC
    Cheque               = Cheque_
    Transmitido          = "F"
    Valor_Item_Redimido  = 0
EndSub
Sub Arma_Registro_Acreditaciones_Redenciones
    Var Arma_Cadena_1  : A1000 = ""
	var tabla: A50=""
   
	Format Id_Ws as ID
	
    Format Name_Cajero As @Tremp, " ", Trim(@TREMP_FNAME), " ", Trim(@TREMP_LNAME)
						If offline=1//test 10/08/2015 
						
							format tabla as "custom.LOYTRANSACCION_OFFLINE"	
						Else//test 10/08/2015
							
							format tabla as "custom.LOYTRANSACCION"				
						Endif//test 10/08/2015
    Format Arma_Cadena_1 as "INSERT INTO ",tabla," ( MemberNumber, id,ALSELevelPrice,BranchDivision,ALSEPaymnetMode,ActivityDate,AdjustedListPrice,Amount,Comments,ItemNumber,LocationName,PartnerName,PointName,Points,ProcessDate,ProcessingComment,ProcessingLog,ProductName,Quantity,IntegrationStatus,Status,SubStatus,TicketNumber,TransactionChannel,TransactionDate,TransactionSubType,TransactionType,VoucherNumber,VoucherQty,VoucherType,Organization,Estatus, Bussiness_date, centroconsumo, Cheque, Transmitido, NumeroLineaMicros, cardNumber, Cajero ) VALUES ( "
    Format Arma_Cadena_2 as "'", MemberNumber, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", id, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", ALSELevelPrice, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", BranchDivision, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", ALSEPaymnetMode, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", ActivityDate, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", AdjustedListPrice, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Amount, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Comments, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", ItemNumber, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", LocationName, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", PartnerName, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", PointName, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Points, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", ProcessDate, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", ProcessingComment, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", ProcessingLog, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", ProductName, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Quantity, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", IntegrationStatus, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Status, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", SubStatus, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", TicketNumber, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", TransactionChannel, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", TransactionDate, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", TransactionSubType, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", TransactionType, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", VoucherNumber, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", VoucherQty, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", VoucherType, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Organization, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Estatus, "', "
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Bussiness_date, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Centroconsumo, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Cheque, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Transmitido, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Linea_Cheque, "',"
	Format Arma_Cadena_2 as Arma_Cadena_2, "'", Clave_ID_TrackII_Origin, "'," //test 10/08/2015
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Name_Cajero, "' "
    Format Arma_Cadena_2 as Arma_Cadena_2, " )"
    Format Arma_Cadena   as Arma_Cadena_1, Arma_Cadena_2
    Format Arma_Cadena_22 as Arma_Cadena_2
EndSub
Sub Arma_Registro_Acreditaciones_Redenciones_CRM_Log
    Var Arma_Cadena_1  : A1000 = ""

    Format Name_Cajero As @Tremp, " ", Trim(@TREMP_FNAME), " ", Trim(@TREMP_LNAME)

    Format Arma_Cadena_1 as "INSERT INTO custom.CRM_Acreditacion_WS ( MemberNumber, id,ALSELevelPrice,BranchDivision,ALSEPaymnetMode,ActivityDate,AdjustedListPrice,Amount,Comments,ItemNumber,LocationName,PartnerName,PointName,Points,ProcessDate,ProcessingComment,ProcessingLog,ProductName,Quantity,IntegrationStatus,Status,SubStatus,TicketNumber,TransactionChannel,TransactionDate,TransactionSubType,TransactionType,VoucherNumber,VoucherQty,VoucherType,Organization,Estatus, Cajero ) VALUES ( "
    Format Arma_Cadena_2 as "'", MemberNumber, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", id, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", ALSELevelPrice, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", BranchDivision, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", ALSEPaymnetMode, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", ActivityDate, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", AdjustedListPrice, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Amount, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Comments, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", ItemNumber, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", LocationName, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", PartnerName, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", PointName, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Points, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", ProcessDate, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", ProcessingComment, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", ProcessingLog, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", ProductName, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Quantity, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", IntegrationStatus, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Status, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", SubStatus, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", TicketNumber, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", TransactionChannel, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", TransactionDate, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", TransactionSubType, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", TransactionType, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", VoucherNumber, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", VoucherQty, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", VoucherType, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Organization, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Estatus, "', "
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Name_Cajero, "' "
    Format Arma_Cadena_2 as Arma_Cadena_2, " )"
    Format Arma_Cadena as Arma_Cadena_1, Arma_Cadena_2
EndSub
Sub Inserta_Solicitud_Acreditacion_Redencion
    Var  Temp1   : $10 = 0
    Call Graba_Archivo_datos(Arma_Cadena)
    Call Conecta_Base_Datos_Sybase
    IF   Version_Micros = 41
       DLLCALL_CDECL hODBCDLL, sqlExecuteQuery(Arma_Cadena)
       SqlStr1 = ""
       DLLCALL_CDECL hODBCDLL, sqlGetLastErrorString(ref SqlStr1)
       IF (SqlStr1 <> "")
           Call Graba_Archivo_datos(SqlStr1)
           InfoMessage "Error 19 A1: ", Mid(SqlStr1,1,30)
       ENDIF
       SqlStr1 = ""
    ENDIF
    IF SqlStr1   = ""
    ELSE
       Split SqlStr1, ";", Temp1
    ENDIF
    Call UnloadDLL_SyBase
EndSub
Sub Graba_Archivo_datos(Ref Campos_)
    Var File_No : N6  = 0
    
    //24Dic2014 
    Var File1       : A100 = ""
    Format File1   as "Ejemplo.TxT"
    Call Asigna_Path_Archivos(File1)}
    FOpen  File_No, File1, Append
    //24Dic2014 
    
    //24Dic2014 FOpen  File_No, "Ejemplo.TxT", Append
    FWrite File_No, "Datos-->", Campos_
    FClose File_No
EndSub
Sub Modifica_Status_Acreditaciones_CRM
  //Format Arma_Cadena as "Update custom.CRM_Acreditacion_WS set Estatus = 'PEN' WHERE TicketNumber = '", TicketNumber, "' "
  //Format Arma_Cadena as "Update custom.LOYTRANSACCION set Estatus = 'PEN', Transmitido = 'F' WHERE TicketNumber = '", TicketNumber, "' "
  Format Arma_Cadena as "Update custom.LOYTRANSACCION set Transmitido = 'F' WHERE TicketNumber = '", TicketNumber, "' "
EndSub
Sub Add_OffLine_Status_Acreditaciones_CRM
    Format Arma_Cadena_22 as "INSERT INTO custom.LOYTRANSACCION_OFFLINE SELECT * FROM custom.LOYTRANSACCION "
    Format Arma_Cadena_22 as Arma_Cadena_22, "WHERE TicketNumber = '", TicketNumber, "' "
    Format Arma_Cadena_22 as Arma_Cadena_22, "AND  TransactionType = 'Accrual' "
EndSub
Sub Modifica_Status_OffLine_Status_Acreditaciones_CR
    Format Arma_Cadena_22 as "UPDATE  custom.LOYTRANSACCION_OFFLINE set Estatus = 'PEN' "
    Format Arma_Cadena_22 as Arma_Cadena_22, "WHERE TicketNumber = '", TicketNumber, "' "
EndSub
Sub Modifica_Status_Acreditaciones_CRM_OK
    Format Arma_Cadena as "Update custom.LOYTRANSACCION set Transmitido = 'T' WHERE TicketNumber = '", TicketNumber, "' "
EndSub
Sub Actualiza_Respuesta_Acreditacion
    Var  Temp1   : $10 = 0
    Call Graba_Archivo_datos(Arma_Cadena)
    Call Conecta_Base_Datos_Sybase
    IF   Version_Micros = 41
       DLLCALL_CDECL hODBCDLL, sqlExecuteQuery(Arma_Cadena)
       SqlStr1 = ""
       DLLCALL_CDECL hODBCDLL, sqlGetLastErrorString(ref SqlStr1)
       IF (SqlStr1 <> "")
           Call Graba_Archivo_datos(SqlStr1)
           InfoMessage "Error 19 B1: ", Mid(SqlStr1,1,30)
       ENDIF
       SqlStr1 = ""
    ENDIF
    IF SqlStr1   = ""
    ELSE
       Split SqlStr1, ";", Temp1
    ENDIF
    Call UnloadDLL_SyBase
EndSub
Sub Actualiza_Respuesta_OffLine_Acreditacion
    Var  Temp1   : $10 = 0
    Call Graba_Archivo_datos(Arma_Cadena_22)
    Call Conecta_Base_Datos_Sybase
    IF   Version_Micros = 41
       DLLCALL_CDECL hODBCDLL, sqlExecuteQuery(Arma_Cadena_22)
       SqlStr1 = ""
       DLLCALL_CDECL hODBCDLL, sqlGetLastErrorString(ref SqlStr1)
       IF (SqlStr1 <> "")
           Call Graba_Archivo_datos(SqlStr1)
           InfoMessage "Error 19.A2: ", Mid(SqlStr1,1,30)
       ENDIF
       SqlStr1 = ""
    ENDIF
    IF SqlStr1   = ""
    ELSE
       Split SqlStr1, ";", Temp1
    ENDIF
    Call UnloadDLL_SyBase
EndSub
Sub Actualiza_Status_Respuesta_OffLine_Acreditacion
    Var  Temp1   : $10 = 0
    Call Graba_Archivo_datos(Arma_Cadena_22)
    Call Conecta_Base_Datos_Sybase
    IF   Version_Micros = 41
       DLLCALL_CDECL hODBCDLL, sqlExecuteQuery(Arma_Cadena_22)
       SqlStr1 = ""
       DLLCALL_CDECL hODBCDLL, sqlGetLastErrorString(ref SqlStr1)
       IF (SqlStr1 <> "")
           Call Graba_Archivo_datos(SqlStr1)
           InfoMessage "Error 19.D1: ", Mid(SqlStr1,1,30)
       ENDIF
       SqlStr1 = ""
    ENDIF
    IF SqlStr1   = ""
    ELSE
       Split SqlStr1, ";", Temp1
    ENDIF
    Call UnloadDLL_SyBase
EndSub

Sub Consulta_Fecha_Juliana_Micros(ref Fecha_C)
    Var File_NO   : N6  = 0
    Call Version_Micros_Sybase
    Call Conecta_Base_Datos_Sybase
    Format SqlStr as "SELECT datediff( day, '2010-01-01', ' ", Trim(Fecha_C), " ') + 1 as FechaJuliana"
    Call Graba_Archivo_datos(SqlStr)
    IF Version_Micros = 41
       DLLCALL_CDECL hODBCDLL, sqlGetRecordSet(SqlStr)
       SqlStr1 = ""
       DLLCALL_CDECL hODBCDLL, sqlGetLastErrorString(ref SqlStr1)
       IF (SqlStr1 <> "")
           Call UnloadDLL_SyBase
           Call Graba_Archivo_datos(SqlStr1)
       ENDIF
       SqlStr1 = ""
       DLLCALL_CDECL hODBCDLL, sqlGetFirst(ref SqlStr1)
    ENDIF
    IF SqlStr1 = ""
       Fecha_Juliana_M = ""
       Fecha_Numerica  = 0
    ELSE
       IF Len(Trim(SqlStr1)) > 0 Then
          Split SqlStr1, ";", Fecha_Juliana_M
          Fecha_Numerica =    Fecha_Juliana_M
       ENDIF
    ENDIF
    Call Graba_Archivo_datos(SqlStr1)
    Call UnloadDLL_SyBase
EndSub
Sub Consulta_Dias_Para_Cumpleanios(ref Fecha_C, ref Fecha_S)
    Var File_NO   : N6  = 0
    Call Conecta_Base_Datos_Sybase
    //WaitForEnter Fecha_C    //2011-11-18
    //WaitForEnter Fecha_S    //2011-1-1
    If Mid(Fecha_S,6,1) = 0 Or Mid(Fecha_S,8,1) = 0
       Diasparacumpleanios = 0
       Return
    EndIf
    Format SqlStr as "SELECT datediff( day, '",Trim(Fecha_C), "', '", Trim(Fecha_S), "') as diasxcumpleanios"
    Call Graba_Archivo_datos(SqlStr)
    IF Version_Micros = 41
       DLLCALL_CDECL hODBCDLL, sqlGetRecordSet(SqlStr)
       SqlStr1 = ""
       DLLCALL_CDECL hODBCDLL, sqlGetLastErrorString(ref SqlStr1)
       IF (SqlStr1 <> "")
           Call UnloadDLL_SyBase
           Call Graba_Archivo_datos(SqlStr1)
       ENDIF
       SqlStr1 = ""
       DLLCALL_CDECL hODBCDLL, sqlGetFirst(ref SqlStr1)
    ENDIF
    IF SqlStr1 = ""
       Fecha_Juliana_M = ""
       Fecha_Numerica  = 0
    ELSE
       IF Len(Trim(SqlStr1)) > 0 Then
          Split SqlStr1, ";", Diasparacumpleanios
       ENDIF
    ENDIF
    //WaitForEnter Diasparacumpleanios
    Call UnloadDLL_SyBase
EndSub
Sub Escoge_Voucher_Redencion
    Var Camp01  : A30 = ""
    Var Camp02  : A30 = ""
    Var Camp03  : A30 = ""
    Var Camp04  : A30 = ""
    Var Campo__ : A30 = ""
    If Contador_Voucher = 0
       Format Camp01  as "Success"
       Format Camp02  as "Processed"
       Format Camp03  as "No Promotions Qualified"
    	 //26Dic2011 Call Rutina_Muestra_Mensajes_Redenciones_Principal(Camp01, Camp02, Camp03)
    	 //27Dic2011 Opcion_Redencion = 1
    	 //27Dic2011 VoucherProductName[Opcion_Redencion] = "OFFLINE"
       Return
    EndIf
    TouchScreen 60
    Window Contador_Voucher, 30
           ListInput 1, 1, Contador_Voucher, VoucherProductName, Opcion_Redencion, "Escoja Redencion"
    WindowClose

    //WaitForEnter Opcion_Redencion
    //WaitForEnter VoucherProductName[Opcion_Redencion]

EndSub
Sub Guarda_Campos_Acreditaciones_Redenciones(Ref SNivel_ , Ref Price_, Ref Number_, Ref Unidades_, Ref Cheque_, Ref ItemNumber_ )
    Var V_Qty  : A1 = ""
    //WaitForEnter Item_Precio_Seleccionado
    
    //26Ene2015
    If Trim(Number_) = "WOW Ticket"
       Unidades_ = 0
    EndIf
    
    //27Ene2015 
    Quantity = Unidades_
    //27Ene2015 
      
    If Flag_Final_Tender = "AF"
       V_Qty     = ""
    Else
    	 V_Qty     = "1"
    EndIf
    //waitforenter Flag_Final_Tender  //AF
    //waitforenter V_Qty              //bco
    
    //26Ene2015
    
    MemberNumber         = NumeroMiembro
    ID                   = "NUEVO"
    ALSELevelPrice       = SNivel_
    //08Ene2015 BranchDivision       = "SBX"
    //08Ene2015 ALSEPaymnetMode      = "SBUX Card"
    BranchDivision       = "BKCASUAL"
    ALSEPaymnetMode      = "Puntos WOW"
    ActivityDate         = Business_Date
    AdjustedListPrice    = AdjustedListPrice
    Amount               = Price_
    Comments             = ""
    ItemNumber           = ItemNumber_
    LocationName         = Tienda
    PartnerName          = PartnerName
    PointName            = ""  //"Star"
    Points               = "0"  //"0"
    ProcessDate          = Business_Date
    ProcessingComment    = ""
    ProcessingLog        = ""
    ProductName          = Number_   //08Jul2015 "Aportacion" Number_  // "22001" // 
    Quantity             = Unidades_
    IntegrationStatus    = ""
    Status               = ""	
    SubStatus            = ""
    TicketNumber         = TicketNumber
    TransactionChannel   = "Store"
    TransactionDate      = Business_Date
    TransactionSubType   = "Product"
    TransactionType      = "Redemption"
    VoucherNumber        = ""  //VoucherTransactionId[Opcion_Redencion]
    VoucherQty           = V_Qty
    VoucherType          = VoucherProductName[Opcion_Redencion]  //Aportacion = debe ir vacio
    Organization         = "BKCASUAL"
    Estatus              = "PEN"
    Bussiness_date       = Business_Date_Real
    Centroconsumo        = @RVC
    Cheque               = Cheque_
    Transmitido          = "F"
EndSub
Sub Guarda_Campos_Acreditaciones_Redenciones_Cero(Ref SNivel_ , Ref Price_, Ref Number_, Ref Unidades_, Ref Cheque_ )
    MemberNumber         = NumeroMiembro
    ID                   = "NUEVO"
    ALSELevelPrice       = SNivel_
    //08Ene2015 BranchDivision       = "SBX"
    //08Ene2015 ALSEPaymnetMode      = "SBUX Card"
    BranchDivision       = "BKCASUAL"
    ALSEPaymnetMode      = "Puntos WOW"
    ActivityDate         = Business_Date
    AdjustedListPrice    = ""
    Amount               = "0"
    Comments             = ""
    ItemNumber           = "0"
    LocationName         = Tienda
    PartnerName          = PartnerName
    PointName            = ""  //"Star"
    Points               = "0"  //"0"
    ProcessDate          = Business_Date
    ProcessingComment    = ""
    ProcessingLog        = ""
    ProductName          = "Ticket"
    Quantity             = "1"
    IntegrationStatus    = ""
    Status               = ""	
    SubStatus            = ""
    TicketNumber         = TicketNumber
    TransactionChannel   = "Store"
    TransactionDate      = Business_Date
    TransactionSubType   = "Product"
    TransactionType      = "Redemption"
    VoucherNumber        = ""  //VoucherTransactionId[Opcion_Redencion]
    VoucherQty           = "1"
    VoucherType          = VoucherProductName[Opcion_Redencion]
    Organization         = "BKCASUAL"
    Estatus              = "PEN"
    Bussiness_date       = Business_Date_Real
    Centroconsumo        = @RVC
    Cheque               = Cheque_
    Transmitido          = "F"
EndSub
Sub Manda_Ejecuta_Acreditaciones_Redenciones_WS_JAR(Ref Ejecuta_)
    Manda = ""
    Format Manda, Separador as "JAR ", Ejecuta_, TicketNumber, MemberNumber
    TxMsg Manda
    //New se quita todo mensaje de acreditacion 21Nov2011 GetRxmsg "Espere...(", Ejecuta_, ")"
    GetRxmsg "Espere...x"
EndSub
Sub Valida_Respuesta_Jar
    If Trim(@RxMsg) = "_timeout"
         ExitWithError Mensaje_WS_Windows
    ElseIf Mid(Trim(@RxMsg),1,25) = "PROCESADO: NO CONTESTA WS" or Mid(Trim(@RxMsg),2,25) = "PROCESADO: NO CONTESTA WS"
         Call Add_OffLine_Status_Acreditaciones_CRM
         Call Actualiza_Respuesta_OffLine_Acreditacion
         Call Modifica_Status_OffLine_Status_Acreditaciones_CR
         Call Actualiza_Status_Respuesta_OffLine_Acreditacion
         Call Modifica_Status_Acreditaciones_CRM
         ExitWithError Mid(Trim(@RxMsg),1,41)
    ENDIF
EndSub
Sub Busca_Respuesta_Acreditaciones_Redenciones(Ref TicketNumber_, Ref Business_Date_)
    Var Contador_respuesta  : N3   = 0
    Var Campo_000           : $10  = 0
    Var Campo_001           : A255 = ""
    Var Campo_002           : A255 = ""
    Var Campo_003           : A255 = ""
    Var Campo_004           : A255 = ""
    Call Conecta_Base_Datos_Sybase
    Format SqlStr as "SELECT FIRST Consecutivo, Estatus, IntegrationStatus, Status, SubStatus FROM custom.LOYTRANSACCION_RESPONSE Where ProductName <> 'Ticket' And TicketNumber = '", TicketNumber_, "'  ORDER BY Consecutivo DESC"
    Call Graba_Archivo_datos(SqlStr)
    IF Version_Micros = 41
       DLLCALL_CDECL hODBCDLL, sqlGetRecordSet(SqlStr)
       SqlStr1 = ""
       DLLCALL_CDECL hODBCDLL, sqlGetLastErrorString(ref SqlStr1)
       IF (SqlStr1 <> "")
           Call UnloadDLL_SyBase
           Call Graba_Archivo_datos(SqlStr1)
       ENDIF
       SqlStr1 = ""
       DLLCALL_CDECL hODBCDLL, sqlGetFirst(ref SqlStr1)
    ENDIF
    ClearArray Respuesta_WS
    Estatus_A_R          = ""
    IntegrationStatus    = ""
    Status               = ""
    SubStatus            = ""
    Campo_000            = 0
    Contador_respuesta   = 0
    Contador_respuesta_R = 0
    Split SqlStr1, ";", Campo_000, Estatus_A_R, IntegrationStatus, Status, SubStatus
    If Len(Trim(IntegrationStatus)) > 0 and Trim(IntegrationStatus) <> "Success"
       Contador_respuesta = ( Contador_respuesta + 1 )
       Respuesta_WS[Contador_respuesta] = Mid(IntegrationStatus,1,100)
    EndIf
    If Len(SqlStr1) <> 0
       Contador_respuesta_R = ( Contador_respuesta_R + 1 )
    EndIf
    Call Graba_Archivo_datos(SqlStr1)
    DLLCALL_CDECL hODBCDLL, sqlGetNext(ref SqlStr1)
    WHILE Len(Trim(SqlStr1)) > 0
          Campo_000 = 0
          Campo_001 = ""
          Campo_002 = ""
          Campo_003 = ""
          Campo_004 = ""
          Contador_respuesta_R = ( Contador_respuesta_R + 1 )
          Split SqlStr1, ";", Campo_000, Campo_001, Campo_002, Campo_003, Campo_004
          If Len(Trim(Campo_002)) > 0 and Trim(Campo_002) <> "Success"
             Contador_respuesta = ( Contador_respuesta + 1 )
             Respuesta_WS[Contador_respuesta] = Mid(Campo_002,1,100)
          EndIf
          Call Graba_Archivo_datos(SqlStr1)
          DLLCALL_CDECL hODBCDLL, sqlGetNext(ref SqlStr1)
    ENDWHILE
    Call UnloadDLL_SyBase
    If Contador_respuesta_R = 0
       Status_Respuesta     = "S"
    EndIf
EndSub
//====================================================
Sub Manda_Solicita_Cancelacion_Ticket
    Manda = ""
    Format Manda, Separador as "VDT ", Ticket_Cancelar, Tienda_Capturada
    TxMsg Manda
    GetRxmsg "Espere....(Cancelacion)"
EndSub
//====================================================
Sub Valida_Exista_Ticket_Cancelar
    Var Mensaje_Error  : A50 = ""
    Call Conecta_Base_Datos_Sybase
    Format SqlStr as "SELECT Estatus, IntegrationStatus FROM custom.LOYTRANSACCION_RESPONSE Where TicketNumber = '", TicketNumber_, "'"
    Format SqlStr as SqlStr, " WHERE Business_Date = '", Business_Date_Real, "' and c_chk_number = ", Ticket_Cancelar
    Call Graba_Archivo_datos(SqlStr)
    IF Version_Micros = 41
       DLLCALL_CDECL hODBCDLL, sqlGetRecordSet(SqlStr)
       SqlStr1 = ""
       DLLCALL_CDECL hODBCDLL, sqlGetLastErrorString(ref SqlStr1)
       IF (SqlStr1 <> "")
           Call UnloadDLL_SyBase
           Call Graba_Archivo_datos(SqlStr1)
       ENDIF
       DLLCALL_CDECL hODBCDLL, sqlGetFirst(ref SqlStr1)
    ENDIF
    Cheque_Existe = 0
    Split SqlStr1, ";", Cheque_Existe
    Call Graba_Archivo_datos(Cheque_Existe)
    Call UnloadDLL_SyBase
    If Ticket_Cancelar <> Cheque_Existe
       Format Mensaje_Error as "NO EXISTE TICKET EN MICROS [", Ticket_Cancelar, "]"
       ExitWithError Mensaje_Error
    EndIf
EndSub
Sub Pantalla_Respuesta_WS(Ref Titulo)  //REDENCIONES
    Var Camp01  : A99  = ""
    Var Camp02  : A99  = ""
    Var Camp03  : A99  = ""
    Var Camp04  : A99  = ""
    Var Campo__ : A50  = ""
    Var CC_PP   : A500 = ""
    Var CC_AR   : A500 = ""
    If Status = Reden_Acred_Aprobada_Status And SubStatus = Reden_Acred_Aprobada_SubStat
       Format Campo__   as Titulo,  " AUTORIZADA"
     //28Dic2011 Call Rutina_Muestra_Mensajes_Redenciones_Principal(Estatus_A_R, Status, SubStatus)
     //Call Pantalla_Error_Redencion_Acreditacion(Campo__)
       Status_Respuesta = "T"
    Else
    	 //Format CC_PP as Trim(IntegrationStatus)
    	 //Format CC_AR as Trim(Estatus_A_R)
    	 //Estatus_A_R, IntegrationStatus, Status, SubStatus
    	 Format Campo__   as Titulo,  " NO AUTORIZADA"
    //28Dic2011 Call Rutina_Muestra_Mensajes_Redenciones_Principal(Estatus_A_R, Status, SubStatus)
     //Call Pantalla_Error_Redencion_Acreditacion(Campo__)
       Status_Respuesta = "F"
    EndIf
EndSub
Sub Verifica_Fail_Fatal_Invalid_Mail(Ref Valor_, Ref Respuesta_)
    If    Mid(Estatus_A_R, 1, 4) = 'Fail'    or Mid(Estatus_A_R, 1,  5) = 'Fatal' \
       or Mid(Estatus_A_R, 1, 7) = 'Invalid' or Mid(Estatus_A_R, 1, 11) = 'Incompleted'
       Call Manda_Solicitud_Mail
       Call Valida_Respuesta_Mail
    EndIf
EndSub
Sub Manda_Solicitud_Mail
    Manda = ""
    Format Manda, Separador as "MAIL ", Estatus_A_R, TicketNumber, Tienda, IntegrationStatus, ProcessingLog
    TxMsg Manda
    GetRxmsg "Enviando Mail CRM..."
EndSub
Sub Valida_Respuesta_Mail
    If Trim(@RxMsg) = "OK ENVIADO"
         Return
    ElseIf Trim(@RxMsg) = "_timeout"
         ExitWithError Mensaje_WS_Windows
    EndIf
EndSub
Sub Pantalla_Respuesta_WS_Acreditacion(Ref Titulo)
    Var Campo__   : A50 = ""
    Var Camp01    : A99 = ""
    Var Camp02    : A99 = ""
    Var Camp03    : A99 = ""
    Var Camp04    : A99 = ""
    
    If Trim(Status) = Reden_Acred_Aprobada_Queued And Trim(IntegrationStatus) = Reden_Acred_Aprobada_Success And Trim(Estatus_A_R) = Reden_Acred_Aprobada_Success
       Format Campo__   as Titulo,  " AUTORIZADA"
     //New se quita para aprobaciones 21Nov2011 Call Rutina_Muestra_Mensajes_Acreditaciones_Principal(Estatus_A_R)
     //Call Pantalla_Error_Redencion_Acreditacion(Campo__)
       Status_Respuesta = "T"
    Else
    	 Format Campo__   as Titulo,  " NO AUTORIZADA"
    	 Call Rutina_Muestra_Mensajes_Acreditaciones_Principal(Estatus_A_R)
     //Call Pantalla_Error_Redencion_Acreditacion(Campo__)
       Status_Respuesta = "F"
    EndIf
EndSub
Sub Pantalla_Error_Redencion_Acreditacion(Ref Titulo_)
    Var I     : N3 = 0
    Var E_P_C : A40 = ""
    Window 11,78, Titulo_
           Display  1, 1, "Status:    ", Status
           Display  2, 1, "SubStatus: ", Mid(SubStatus,1,48)
           Display  3, 1, "IntStatus: ", Mid(IntegrationStatus,1,67)
           If Len(Trim(IntegrationStatus)) > 67
              Display  4, 1, "IntStatus: ", Mid(IntegrationStatus,68,67)
           EndIf
           Display  5, 1, "Estatus: ", Estatus_A_R
           For I = 1 to 1  //New 5
               Display  I+5, 1, Mid(Respuesta_WS[I],1,77)
           EndFor
           TouchScreen Numero_TouchscreenN
           Input E_P_C, "[Enter] para Continuar"
         //WaitForEnter "[Enter] para Continuar"
    WindowClose
EndSub
Sub Verifica_Descuento_Aplicado
    Descuento_Aplicado        = ""
    FP_Aplicada_Otra          = ""
    FP_Sbux_Aplicado          = ""
    FP_Sbux_Aplicado_Off_Line = ""
    For I = 1 to @NumDTLT
        If @dtl_type[I] = "D" and @dtl_Is_Void[I] = 0
           If @DTL_OBJECT[I] = Descuento_Redenciones_P or @DTL_OBJECT[I] = Descuento_Redenciones_M
              Descuento_Aplicado = "T"
           EndIf
        ElseIf @dtl_type[I] = "T" and @dtl_Is_Void[I] = 0
           If ( @DTL_OBJECT[I] = FormaPagoSbux ) 
                FP_Sbux_Aplicado = "T"
           ElseIf ( @DTL_OBJECT[I] = FormaPagoSbuxOffLine )
              FP_Sbux_Aplicado_Off_Line = "T"
              FP_Sbux_Aplicado = "T"
           Else
           	  FP_Aplicada_Otra = "T"
           EndIf
           Total_Fpago_Cheque = ( Total_Fpago_Cheque + @DTL_TTL[I] )
        EndIf
    EndFor
EndSub
Sub Graba_Linea_Item_Redimida(Ref Cheque_, Ref Linea_Red_)// Modificado el 14 ABR 2016
//    Var FN001 : N6 = 0
    Var IJ    : N3 = 0
//    //24Dic2014 
//    Var File1       : A100 = ""
//    Format File1   as FileLineaRedChq
//    Call Asigna_Path_Archivos(File1)}
//    FOpen  FN001, File1, Append
//    //24Dic2014 
    
//    //24Dic2014 FOpen  FN001, FileLineaRedChq, Append
//    FWrite FN001, Cheque_, Linea_Red_
//    FClose FN001
	
	//------- 14 ABR 2016
	For IJ = 1 to 100
	    if Linea_Red[IJ] = 0
		    Chq[IJ] = Cheque_
			Linea_Red[IJ] = Linea_Red_
		    break
		endif
	EndFor
	//-------
	
Endsub
Sub Lee_Linea_Item_Redimida// Modificado el 14 ABR 2016
//    Var FN001      : N6 = 0
//    Var Linea_R    : N3 = 0
    Var Contador_R : N3 = 0
    ClearArray Linea_Redimida
    
//    //24Dic2014 
//    Var File1       : A100 = ""
//    Format File1   as FileLineaRedChq
//    Call Asigna_Path_Archivos(File1)}
//    FOpen  FN001, File1, Read
//    //24Dic2014 
    
//    //24Dic2014 FOpen FN001, FileLineaRedChq, Read
//    If FN001 = 0
//       Contador_R = 0
//      Cheque     = 0
//      Linea_R    = 0
//       Return
//    EndIf
//    While Not Feof(FN001)
//          FRead  FN001, Cheque, Linea_R
//          If Linea_R > 0
//             Contador_R = ( Contador_R + 1 )
//             Cheque_Leido               = Cheque
//             Linea_Redimida[Contador_R] = Linea_R
//          EndIf
//    EndWhile
//    FClose FN001
	
	//------- 14 ABR 2016
	Lineas_Leidas_Redimidas = 0
	For Contador_R = 1 to 100
	    if Linea_Red[Contador_R] > 0
            Cheque_Leido               = Chq[Contador_R]
			Linea_Redimida[Contador_R] = Linea_Red[Contador_R]
			Lineas_Leidas_Redimidas = ( Lineas_Leidas_Redimidas + 1 )
		endif
	EndFor
	//-------
    
	//Lineas_Leidas_Redimidas = Contador_R
	//infomessage "las lineas leidas son:", Lineas_Leidas_Redimidas
Endsub
Sub Borra_Linea_Item_Redimida// Modificado el 14 ABR 2016
//    Var FN001   : N6 = 0
//    Var Campo_1 : A99 = ""
    
//    //24Dic2014 
//    Var File1       : A100 = ""
//    Format File1   as FileLineaRedChq
//    Call Asigna_Path_Archivos(File1)}
//    FOpen  FN001, File1, Write
//    //24Dic2014 
    
//    //24Dic2014 FOpen  FN001, FileLineaRedChq, Write
//    FWrite FN001, " "
//    FClose FN001
//    Format Campo_1 as "DEL ", FileLineaRedChq
//    System Campo_1
	//------- 14 ABR 2016
	ClearArray Chq
	ClearArray Linea_Red
	//-------
Endsub
Sub Verifica_Linea_Redencion( Ref Linea_Redimida_ )
    Var I_  : N3 = 0
    Linea_Redimida_Encontrada = ""
    Call Lee_Linea_Item_Redimida
    //WaitForEnter "Verifica_Linea_Redencion"
    //WaitForEnter Cheque_Leido
    IF @CKNUM <> Cheque_Leido
       Return
    EndIf
    //WaitForEnter Linea_Redimida_   // Linea 17
    For I_ = 1 to Lineas_Leidas_Redimidas
        If Linea_Redimida[I_] <> 0
           If Linea_Redimida[I_] = Linea_Redimida_
              Linea_Redimida_Encontrada = "T"
              Return
           EndIf
        EndIf
    EndFor
EndSub
Sub Add_OffLine_Void_Checks_CRM
    Format Arma_Cadena_22 as "INSERT INTO custom.CHEQUES_OFFLINE (TicketNumber, Procesado) VALUES ( "
    Format Arma_Cadena_22 as Arma_Cadena_22, " '", Ticket_Cancelar, "', 'wait' ) "
EndSub
Sub Actualiza_Respuesta_OffLine_Void_Checks
    Var  Temp1   : $10 = 0
    Call Graba_Archivo_datos(Arma_Cadena_22)
    Call Conecta_Base_Datos_Sybase
    IF   Version_Micros = 41
       DLLCALL_CDECL hODBCDLL, sqlExecuteQuery(Arma_Cadena_22)
       SqlStr1 = ""
       DLLCALL_CDECL hODBCDLL, sqlGetLastErrorString(ref SqlStr1)
       IF (SqlStr1 <> "")
           Call Graba_Archivo_datos(SqlStr1)
           InfoMessage "Error 19.B2: ", Mid(SqlStr1,1,30)
       ENDIF
       SqlStr1 = ""
    ENDIF
    IF SqlStr1   = ""
    ELSE
       Split SqlStr1, ";", Temp1
    ENDIF
    Call UnloadDLL_SyBase
EndSub
Sub Main_Graba_Cheques_OffLine
    Call Add_OffLine_Void_Checks_CRM
    Call Actualiza_Respuesta_OffLine_Void_Checks
EndSub
Sub Muestra_datos_consulta_pantalla
    Var FN004  : N6  = 0
    Var Campo_ : A50 = ""
    Var II     : N3  = 0
    Campo_Busqueda   = ""

    Var File1       : A100 = ""
    Format File1   as "Consulta_Saldo.TxT"
    Call Asigna_Path_Archivos(File1)}
    FOpen  FN004, File1, Read
    
    If FN004 = 0
       Return
    EndIf
    While Not Feof(FN004)
          FRead FN004, Campo_
          II = ( II + 1 )
          //Display II, 1, Mid(Campo_,1,75)
          If Mid(Campo_,1,11) = "Card number"
             Campo_Busqueda = Trim(Mid(Campo_,17,16))
          ElseIf Mid(Campo_,1,7) = "Balance"
             Saldo          = Trim(Mid(Campo_,17,16))
          EndIf
    EndWhile
    
    Format Saldo_Ws As Saldo
    
    FClose FN004
    Leidos_Consulta = II
    //WaitForEnter Saldo
    //WaitForEnter Saldo_Ws
    //WindowClose
EndSub
//Sub Borra_Consulta_Saldo
//    Var    FN001   : N6 = 0
//    
//    //24Dic2014 
//    Var File1       : A100 = ""
//    Format File1   as FileConsultaSaldo
//    Call Asigna_Path_Archivos(File1)}
//    FOpen  FN001, File1, Write
//    //24Dic2014 
    
//    //24Dic2014 FOpen  FN001, FileConsultaSaldo, Write
//    FWrite FN001, " "
//    FClose FN001
//    System "Del Consulta_Saldo.TxT"
//Endsub

//Sub Borra_Consulta_Saldo_Servicio
//    Var    FN001   : N6 = 0
    
//    //24Dic2014 
//    Var File1       : A100 = ""
//    Format File1   as FileConsultaSaldoServicio
//    Call Asigna_Path_Archivos(File1)}
//    FOpen  FN001, File1, Write
//    //24Dic2014 
    
//    //24Dic2014 FOpen  FN001, FileConsultaSaldoServicio, Write
//    FWrite FN001, " "
//    FClose FN001
//    System "Del ", FileConsultaSaldoServicio
	
//	//infomessage "consulta de saldo servicio borrada"
//Endsub

Sub Write_Ticket_Cancelar(Ref Ticket_) //Modificado el 13 ABR 2016
//    Var FN004  : N6 = 0
//    Var III    : N3 = 0
    
//    //24Dic2014 
//    Var File1       : A100 = ""
//    Format File1   as TicketCancelar
//    Call Asigna_Path_Archivos(File1)}
//    FOpen  FN004, File1, Write
//    //24Dic2014 
    
//    //24Dic2014 FOpen  FN004, TicketCancelar, Write
//    FWrite FN004, Ticket_
//    FClose FN004
	
	//------- 13 ABR 2016
    Format TicketCancelarNum as Ticket_
    //-------	
	
EndSub
Sub Read_Ticket_Cancelar // Modificado el 13 ABR 2016
//    Var FN004  : N6  = 0
//    Var Campo_ : A50 = ""
//    Ticket_Cancelar_Cheque = 0
//    Ticket_Cancelar_Numero = ""
    
//    //24Dic2014 
//    Var File1       : A100 = ""
//    Format File1   as TicketCancelar
//    Call Asigna_Path_Archivos(File1)}
//    FOpen  FN004, File1, Read
//    //24Dic2014 
    
//    //24Dic2014 FOpen FN004, TicketCancelar, Read
//    If FN004 <> 0
//       FRead FN004, Ticket_Cancelar_Numero
//       FClose FN004
//    EndIf
	
	//-------13 ABR 2016
	If TicketCancelarNum <> ""
	    Format Ticket_Cancelar_Numero as TicketCancelarNum
	EndIf
	//-------
	
EndSub
Sub Borra_Ticket_Cancelar // Modificado el 13 ABR 2016
//    Var    FN001   : N6 = 0
    
//    //24Dic2014 
//    Var File1       : A100 = ""
//    Format File1   as TicketCancelar
//    Call Asigna_Path_Archivos(File1)}
//    FOpen  FN001, File1, Write
//    //24Dic2014 
    
//    //24Dic2014 FOpen  FN001, TicketCancelar, Write
//    FWrite FN001, " "
//    FClose FN001
//    System "Del Ticket_Cancelar.TxT"
	
    //------- 13 ABR 2016
    TicketCancelarNum = ""
    //-------
	
Endsub

Sub Busca_Detalle_Cheque_Cancelar(Ref Ticketcancelar_)
    Var Campo_001           : A25 = ""
    Var Campo_002           : A25 = ""
    Var Campo_003           : A25 = ""
    Var Campo_004           : A25 = ""
    Call Version_Micros_Sybase
    Call Conecta_Base_Datos_Sybase
    Format SqlStr as "SELECT d_dtl_type, c_chk_num, c_id, MIDEF_obj_num, MIDEF_name, d_rpt_cnt "
    Format SqlStr as SqlStr, "FROM micros.vta_base_baseDtl "
    Format SqlStr as SqlStr, "where d_dtl_type ='M' "
    Format SqlStr as SqlStr, "AND T_training_status = 0 "
    Format SqlStr as SqlStr, "AND D_ob_dtl05_void_flag = 'F' "
    Format SqlStr as SqlStr, "AND D_ob_error_correct = 'F' "
    Format SqlStr as SqlStr, "AND M_ob_dtl04_rtn = 'F' "
    Format SqlStr as SqlStr, "AND c_id = '", Ticketcancelar_, "' "
    Format SqlStr as SqlStr, "Order by d_dtl_seq "
    
    Call Graba_Archivo_datos(SqlStr)
    IF Version_Micros = 41
       DLLCALL_CDECL hODBCDLL, sqlGetRecordSet(SqlStr)
       SqlStr1 = ""
       DLLCALL_CDECL hODBCDLL, sqlGetLastErrorString(ref SqlStr1)
       IF (SqlStr1 <> "")
           Call UnloadDLL_SyBase
           Call Graba_Archivo_datos(SqlStr1)
       ENDIF
       SqlStr1 = ""
       DLLCALL_CDECL hODBCDLL, sqlGetFirst(ref SqlStr1)
    ENDIF
    ClearArray Articulo_ObjNum_Void
    ClearArray Unidades_Void
    WHILE Len(Trim(SqlStr1)) > 0
          Call Graba_Archivo_datos(SqlStr1)
          Contador_respuesta = ( Contador_respuesta + 1 )
          Split SqlStr1, ";", Campo_001, Campo_002, Campo_003, Articulo_ObjNum_Void[Contador_respuesta], Campo_004, Unidades_Void[Contador_respuesta]
          DLLCALL_CDECL hODBCDLL, sqlGetNext(ref SqlStr1)
    ENDWHILE
    Call UnloadDLL_SyBase
EndSub

EVENT Print_Trailer : Impresion_WOW
      Var I_II : N3  = 0
      Var C_II : N3  = 0
      Var Leng : N3  = 0
      Var CCC  : A99 = ""
      Var St_A : A4  = ""
      Var St_N : N6  = 0
      Var Ly[2]: A99
	  var nu : N6 =0
      
      Call Lee_Configuracion_CRM
     
      //Call Lee_Leyenda_CRM //Comentada el 22 DIC 2015 JMG
    //infoMessage "Inicio de la impresion"  
      //If Lineas_Leyenda_Crm > 0 //16Ene2015 Or Lineas_Leyenda_Crm_Cheque > 0
      //   For I_II = 1 to Lineas_Leyenda_Crm 
      //       Format Campo as Leyenda_CRM[I_II]
      //       @TRAILER[I_II] = Campo
	  //	 nu= nu +1
      //   EndFor
	  //	  @TRAILER[nu+1] = " "
		 
      //EndIf
	 
	//  
    
	//<======= 22 DIC 2015 JMG
	
	Var i : N10
	
	For i = 1 to 10
	    if LeyendaCRM_WOW[i] = ""
		    // No hacer nada
		else
			Format Campo as LeyendaCRM_WOW[i]
		    //infomessage i, Campo
		    @TRAILER[i] = Campo
			nu = nu + 1
		endif
	
	EndFor
	
	@TRAILER[nu+1] = " "
	
	//<======= 22 DIC 2015 JMG
	
	//infoMessage "Fin de la impresion"
	
	Call Borra_Leyenda_CRM
    Call Borra_FileMiembro_Pausa_R_Cheque
    Call Borra_FileLeyendaCRM_Cheque
ENDEVENT
Sub Main_Solicita_Ticket_Unico
    Var Fecha_Capturada     : A30 = ""
    Var Fecha_Capturada_M   : A30 = ""
    
    Var Ticket_Capturado_A  : A29 = ""
    Var Ticket_Capturado    : N9  = 0
    Var Ticket_Unico_Armado : A18 = ""
    Var Fecha_Capturada_    : A12 = ""

    Call Version_Micros_Sybase
    Call Consulta_Business_Date_Sybase
    Fecha_Capturada  = Business_Date_Real
    Tienda_Capturada = Tienda
    Fecha_Capturada_ = Trim(Fecha_Capturada)

    ForEver
       TouchScreen Numero_TouchscreenN
       Window 4,40
              Display      1,  1, "Fecha : "
              Display      2,  1, "Tienda: "
              Display      3,  1, "Ticket: "
              Display      1, 21, "(AAAA-MM-DD)"
              DisplayInput 1, 09, Fecha_Capturada_, "Digite Fecha"
              DisplayInput 2, 09, Tienda_Capturada, "Digite No Tinda"
              DisplayInput 3, 09, Ticket_Capturado_A, "Digite Numero Ticket"
              Display      4, 08, "(9 Digitos)"
              WindowEdit
              //okInputKey KeyPress, Data, "Salir o Sigue"
              //WindowEdit
              Fecha_Capturada = Trim(Fecha_Capturada_)
              
              ClearIslTs
                   SetIslTskeyx 1,  1, 10, 10, 3, @Key_Enter, 10059, "B", 10, "SI"
                   SetIslTskeyx 1, 11, 10, 10, 3, @Key_Clear, 10058, "B", 10, "NO"
                   SetIslTskeyx 1,  21,10, 10, 3, Key(1,131073), 10059, "B", 10, "Salir"
              DisplayIslTs
              Inputkey keypress, data, "Datos Correctos"

              If KeyPress = @Key_Clear
                 //Break
              ElseIf KeyPress = @Key_Enter
                 If Len(Fecha_Capturada) = 0 or Len(Tienda_Capturada) = 0 or Len(Trim(Ticket_Capturado_A)) = 0
                    InfoMessage "TICKET", "SIN INFORMACION CAPTURADA"
                 ElseIf Len(Trim(Ticket_Capturado_A)) = 0
                    InfoMessage "TICKET", "TICKET NO CAPTURADO"
                 ElseIf Len(Trim(Ticket_Capturado_A)) <> 9
                    InfoMessage "TICKET", "TICKET INCORRECTO"
                 Else
                 	  //Ticket_Capturado = Ticket_Capturado_A
                    Format Fecha_a_Convertir as Fecha_Capturada ///"20", Mid(Fecha_Capturada,7,2), "-", Mid(Fecha_Capturada,1,2), "-",Mid(Fecha_Capturada,4,2)
                    Call Consulta_Fecha_Juliana_Micros(Fecha_a_Convertir)
                    Format Ticket_Unico_Armado as Fecha_Numerica{04}, Tienda_Capturada{05}, Trim(Ticket_Capturado_A){9}
                    Ticket_Unico = Ticket_Unico_Armado
                    Break
                 EndIf
              ElseIf KeyPress = Key(1,131073)       //Salir
                 Break
              EndIf
    EndFor
       WindowClose
EndSub
Sub Ticket_Correcto
    Var Campo1 : A80 = ""
    Format Campo1 as "TICKET CORRECTO: ", Ticket_Capturado, " ?"
    Flag_Salir = "N"
    ClearIslTs
         IF @PLATFORM = "3700"
            SetIslTsKeyx 1,  1, 12, 15, 3, @Key_Enter, 10059, "Top", 10, "SI"
            SetIslTsKeyx 1, 16, 12, 15, 3, @Key_Clear, 10058, "Top", 10, "NO"
         ENDIF
    DisplayIslTs
    InputKey KeyPress, Data, Campo1
    IF KeyPress = @Key_Clear
       Flag_Salir = "N"
    ELSE
       Flag_Salir = "S"
    ENDIF
    ClearIslTs
    ClearIslTs
EndSub
Sub Socio_Starbucks_Rewards
    Var Campo1 : A80 = ""
    
    ForEver 
       Format Campo1 as "Socio WOW Rewards: "
       Flag_Socio_Starbucks_Rewards = "N"
       ClearIslTs
            IF @PLATFORM = "3700"
               SetIslTsKeyx 1,  1, 12, 15, 3, @Key_End, 10059, "Top", 10, "SI"
               SetIslTsKeyx 1, 16, 12, 15, 3, @Key_Clear, 10058, "Top", 10, "NO"
            ENDIF
       DisplayIslTs
       InputKey KeyPress, Data, Campo1
       If KeyPress = @Key_Clear
	       infoMessage "entra a pantalla"
          //WaitForEnter "Clear"
          Flag_Socio_Starbucks_Rewards = "N"
          Break
       ElseIf KeyPress = @Key_End
          //WaitForEnter "End"
          Flag_Socio_Starbucks_Rewards = "S"
          Break
       EndIf
    EndFor
    
    
    ClearIslTs
    ClearIslTs
EndSub

Sub LoadSIMInquire(var interfaceSeq: N5, var inquireNumber: N11)
    LoadKybdMacro key(24, (16384 * inquireNumber) + interfaceSeq)
EndSub

Sub Borra_Leyenda_CRM

    // Comentado el 22 DIC 2015 JMG
	//
    //Var FN001   : N6 = 0
    //Var Campo_1 : A99 = ""
    ////WaitForEnter "Borra Leyanda CRM"

    ////24Dic2014 
    //Var File1       : A100 = ""
    //Format File1   as FileLeyendaCRM
    //Call Asigna_Path_Archivos(File1)}
    //FOpen  FN001, File1, Write
    ////24Dic2014 
    
    ////24Dic2014 FOpen  FN001, FileLeyendaCRM, Write
    //FWrite FN001, " "
    //FClose FN001
    //Format Campo_1 as "DEL ", FileLeyendaCRM
    //System Campo_1
	
	//<======= 22 DIC 2015 JMG
	
	var i : N10
		
	For i = 1 to 10
	    LeyendaCRM_WOW[i] = ""
		//infoMessage i, LeyendaCRM[i]
    EndFor
	
	//infoMessage "Pila borrada"
	
	//<======= 22 DIC 2015 JMG
	
Endsub
Sub Write_Leyenda_CRM(Ref Ticket_)

    // Comentado el 22 DIC 2015 JMG

    //Var FN004  : N6 = 0
    //Var III    : N3 = 0
    ////ok WaitForEnter Ticket_
    ////ok WaitForEnter FileLeyendaCRM
    
    ////24Dic2014 
    //Var File1       : A100 = ""
    //Format File1   as FileLeyendaCRM
    //Call Asigna_Path_Archivos(File1)}
    //FOpen  FN004, File1, Append
    ////24Dic2014 
    
    ////24Dic2014 FOpen  FN004, FileLeyendaCRM, Append
    //FWrite FN004, Ticket_
    //FClose FN004
    //WaitForEnter "Termino Grabar Leyanda CRM"
	
	//<======= 22 DIC 2015 JMG
	
	var i : N10
		
	For i = 1 to 10
	    
		if LeyendaCRM_WOW[i] = ""
		    LeyendaCRM_WOW[i] = Ticket_
			//infomessage i, LeyendaCRM_WOW[i]
	        break
		endif
    EndFor
	
	//<======= 22 DIC 2015 JMG
	
	
EndSub
//Sub Lee_Leyenda_CRM

    // Comentado el 22 DIC 2015 JMG
    //
    //Var FN001      : N6  = 0
    //Var Linea_R    : A45 = ""
    //Var Contador_R : N3  = 0
    //ClearArray Leyenda_CRM
    
    ////24Dic2014 
    //Var File1       : A100 = ""
    //Format File1   as FileLeyendaCRM
    //Call Asigna_Path_Archivos(File1)}
    //FOpen  FN001, File1, Read
    ////24Dic2014 
    
    ////24Dic2014 FOpen FN001, FileLeyendaCRM, Read
    //If FN001 = 0
    //   Return
    //EndIf
    //While Not Feof(FN001)
    //      FRead  FN001, Linea_R
    //      //WaitForEnter Linea_R
    //      If Len(Trim(Linea_R)) > 0
    //         Contador_R = ( Contador_R + 1 )
    //         Leyenda_CRM[Contador_R] = Linea_R
    //      EndIf
    //EndWhile
    //FClose FN001
    //Lineas_Leyenda_Crm = Contador_R
    ////WaitForEnter Lineas_Leyenda_Crm
//Endsub
Sub Busca_Recompensa_Nivel(Ref Nivel_)
    Var Campo_001           : A255 = ""
    Var Campo_002           : A255 = ""
    Var Campo_003           : A255 = ""
    Var Campo_004           : A255 = ""
    Contador_respuesta_Nivel = 0
    Call Conecta_Base_Datos_Sybase
    Format SqlStr as "SELECT Name2, RewardName, Description, RewardTypeDiscount_P_M  FROM custom.CRM_Niveles WHERE name2 = '", Nivel_, "' "
    Call Graba_Archivo_datos(SqlStr)
    IF Version_Micros = 41
       DLLCALL_CDECL hODBCDLL, sqlGetRecordSet(SqlStr)
       SqlStr1 = ""
       DLLCALL_CDECL hODBCDLL, sqlGetLastErrorString(ref SqlStr1)
       IF (SqlStr1 <> "")
           Call UnloadDLL_SyBase
           Call Graba_Archivo_datos(SqlStr1)
       ENDIF
       SqlStr1 = ""
       DLLCALL_CDECL hODBCDLL, sqlGetFirst(ref SqlStr1)
    ENDIF
    ClearArray Nivel_Name_A
    ClearArray RewardName_A
    ClearArray Description_A
    ClearArray RewardTypeDiscount_P_M_A
    ClearArray RewardPoints
    WHILE Len(Trim(SqlStr1)) > 0
          Campo_001 = ""
          Campo_002 = ""
          Campo_003 = ""
          Campo_004 = ""
          Split SqlStr1, ";", Campo_001, Campo_002, Campo_003, Campo_004
          If Len(Trim(Campo_002)) > 0
             Contador_respuesta_Nivel                           = ( Contador_respuesta_Nivel + 1 )
             Nivel_Name_A[Contador_respuesta_Nivel]             = Campo_001
             RewardName_A[Contador_respuesta_Nivel]             = Campo_002
             Description_A[Contador_respuesta_Nivel]            = Campo_003
             RewardTypeDiscount_P_M_A[Contador_respuesta_Nivel] = Campo_004
             RewardPoints[Contador_respuesta_Nivel]             = Campo_002
          EndIf
          Call Graba_Archivo_datos(SqlStr1)
          DLLCALL_CDECL hODBCDLL, sqlGetNext(ref SqlStr1)
    ENDWHILE
    Call UnloadDLL_SyBase
EndSub
//Sub Graba_Privilegios_Nivel //Comentado el 12 ABR 2016, solo starbucks solo usa niveles de privilegios para sus usuarios
//    Var FN001 : N6 = 0
//    Var IJ    : N2 = 0
//    
//    //24Dic2014 
//    Var File1       : A100 = ""
//    Format File1   as FileNivel
//    Call Asigna_Path_Archivos(File1)}
//    FOpen  FN001, File1, Write
//   //24Dic2014 
    
//   //24Dic2014 FOpen  FN001, FileNivel, Write
//    For IJ = 1 to Contador_respuesta_Nivel
//        FWrite FN001, Nivel_Name_A[IJ], RewardName_A[IJ], Description_A[IJ], RewardTypeDiscount_P_M_A[IJ]
//    EndFor
//    FClose FN001

//Endsub

//Sub Lee_Privilegios_Nivel //Comentado el 12 ABR 2016
//    Var FN001      : N6  = 0
//    Var Linea_R    : A45 = ""
//    Var Contador_R : N3  = 0
//    Var Campo_001  : A255 = ""
//    Var Campo_002  : A255 = ""
//    Var Campo_003  : A255 = ""
//    Var Campo_004  : A255 = ""
    
//    ClearArray Nivel_Name_A
//    ClearArray RewardName_A
//    ClearArray Description_A
//    ClearArray RewardTypeDiscount_P_M_A
//    ClearArray RewardPoints
    
//    Contador_respuesta_Nivel = 0
    
//    //24Dic2014 
//    Var File1       : A100 = ""
//    Format File1   as FileNivel
//    Call Asigna_Path_Archivos(File1)}
//    FOpen  FN001, File1, Read
//    //24Dic2014 
    
//    //24Dic2014 FOpen FN001, FileNivel, Read
//    If FN001 = 0
//       Return
//    EndIf
//    While Not Feof(FN001)
//          FRead  FN001, Campo_001,Campo_002, Campo_003, Campo_004
//          Contador_respuesta_Nivel                           = ( Contador_respuesta_Nivel + 1 )
//          Nivel_Name_A[Contador_respuesta_Nivel]             = Campo_001
//          RewardName_A[Contador_respuesta_Nivel]             = Campo_002
//          Description_A[Contador_respuesta_Nivel]            = Campo_003
//          RewardTypeDiscount_P_M_A[Contador_respuesta_Nivel] = Campo_004
//          RewardPoints[Contador_respuesta_Nivel]             = Campo_003
//          //WaitForEnter Campo_001
//          //WaitForEnter Campo_002
//          //WaitForEnter Campo_003
//          //WaitForEnter Campo_004
//          //new Contador_Voucher = ( Contador_Voucher + 1 )
//          //new VoucherProductName[Contador_Voucher] = Campo_002
//    EndWhile
//    FClose FN001

//Endsub

//Sub Lee_Busca_Privilegios_Nivel //Comentado el 12 ABR 2016
//    Var FN001      : N6  = 0
//    Var Linea_R    : A45 = ""
//    Var Contador_R : N3  = 0
//    Var Campo_001  : A255 = ""
//    Var Campo_002  : A255 = ""
//    Var Campo_003  : A255 = ""
//    Var Campo_004  : A255 = ""
//    Contador_respuesta_Nivel = 0
    
//    //24Dic2014 
//    Var File1       : A100 = ""
//    Format File1   as FileNivel
//    Call Asigna_Path_Archivos(File1)}
//    FOpen  FN001, File1, Read
//    //24Dic2014 
    
//    //24Dic2014 FOpen FN001, FileNivel, Read
//    If FN001 = 0
//       Return
//    EndIf
//    While Not Feof(FN001)
//          FRead  FN001, Campo_001,Campo_002, Campo_003, Campo_004
//          If Trim(Mid(Campo_001,1,16)) = Trim(VoucherProductName[Opcion_Redencion])
//             If Trim(Campo_003) = "Percentage"
//                Tipo_Descuento  = "P"
//                Tipo_Descuento_Valor_P = Campo_002
//                FClose FN001
//                Return
//             ElseIf Trim(Campo_003) = "Points"
//                Tipo_Descuento  = "T"
//                Tipo_Descuento_Valor_T = Campo_002
//                FClose FN001
//                Return
//             Else
//              	Tipo_Descuento  = "M"
//              	Tipo_Descuento_Valor_M = Campo_002
//              	FClose FN001
//                Return
//             EndIf
//          EndIf
//    EndWhile
//    Tipo_Descuento  = "M"
//    Tipo_Descuento_Valor_M = 0
//    FClose FN001
//Endsub

//Sub Borra_Privilegios_Nivel //Comentado el 12 ABR 2016
//    Var FN001   : N6 = 0
//    Var Campo_1 : A99 = ""
    
//    //24Dic2014 
//    Var File1       : A100 = ""
//    Format File1   as FileNivel
//    Call Asigna_Path_Archivos(File1)}
//    FOpen  FN001, File1, Write
//    //24Dic2014 
    
//    //24Dic2014 FOpen  FN001, FileNivel, Write
//    FWrite FN001, " "
//    FClose FN001
//    Format Campo_1 as "DEL ", FileNivel
//    System Campo_1
//Endsub
//Sub Borra_Privilegios_Nivel_Servicio //Comentado el 12 ABR 2016
//    Var FN001   : N6 = 0
//    Var Campo_1 : A99 = ""
    
//    //24Dic2014 
//    Var File1       : A100 = ""
//    Format File1   as FileNivelServicio
//    Call Asigna_Path_Archivos(File1)}
//    FOpen  FN001, File1, Write
//    //24Dic2014 
    
//    //24Dic2014 FOpen  FN001, FileNivelServicio, Write
//    FWrite FN001, " "
//    FClose FN001
//    Format Campo_1 as "DEL ", FileNivelServicio
//    System Campo_1
	
//Endsub
Sub Graba_Privilegios_Nivel_Miembro // Modificado el 12 ABR 2016
//    Var FN001 : N6 = 0
    Var IJ    : N2 = 0
    
//    //24Dic2014 
//    Var File1       : A100 = ""
//    Format File1   as FileNivel_Miembro
//    Call Asigna_Path_Archivos(File1)}
//    FOpen  FN001, File1, Write
//    //24Dic2014 
    
//    //24Dic2014 FOpen  FN001, FileNivel_Miembro, Write
//    //For IJ = 1 to Contador_respuesta_Nivel
//        FWrite FN001, Contador_Redenciones, #Contador_Redenciones,Redencion_Aplicada[]
//    //EndFor
//    FClose FN001
	
	
	Contador_Redenciones_MNM = Contador_Redenciones
	For IJ = 1 to Contador_Redenciones
	    Redencion_Aplicada_MNM[IJ] = Redencion_Aplicada[IJ]
	EndFor
	
Endsub
Sub Borra_Privilegios_Nivel_Miembro // Modificado el 12 ABR 2016
//    Var FN001   : N6 = 0
//    Var Campo_1 : A99 = ""
    
//    //24Dic2014 
//    Var File1       : A100 = ""
//    Format File1   as FileNivel_Miembro
//    Call Asigna_Path_Archivos(File1)}
//    FOpen  FN001, File1, Write
//    //24Dic2014 
    
//    //24Dic2014 FOpen  FN001, FileNivel_Miembro, Write
//    FWrite FN001, " "
//    FClose FN001
//    Format Campo_1 as "DEL ", FileNivel_Miembro
//    System Campo_1
	
	
	ClearArray Redencion_Aplicada_MNM
	Contador_Redenciones_MNM = 0
	
Endsub
//Sub Borra_Privilegios_Nivel_Miembro_Servicio //Comentado el 12 ABR 2016
//    Var FN001   : N6 = 0
//    Var Campo_1 : A99 = ""
    
//    //24Dic2014 
//    Var File1       : A100 = ""
//    Format File1   as FileNivel_MiembroServicio
//    Call Asigna_Path_Archivos(File1)}
//    FOpen  FN001, File1, Write
//    //24Dic2014 
    
//    //24Dic2014 FOpen  FN001, FileNivel_MiembroServicio, Write
//    FWrite FN001, " "
//    FClose FN001
//    Format Campo_1 as "DEL ", FileNivel_MiembroServicio
//    System Campo_1
//Endsub
Sub Lee_Privilegios_Nivel_Miembro
//    Var FN001 : N6 = 0
    Var IJ    : N2 = 0
    
    //24Dic2014 
//    Var File1       : A100 = ""
//    Format File1   as FileNivel_Miembro
//    Call Asigna_Path_Archivos(File1)}
//    FOpen  FN001, File1, Read
//    //24Dic2014 
    
//    //24Dic2014 FOpen  FN001, FileNivel_Miembro, Read
//    If FN001 > 0
//       FRead  FN001, Contador_Voucher, #Contador_Voucher, VoucherProductName[]
//       FClose FN001
//    EndIf
	
	Contador_Voucher = Contador_Redenciones_MNM
	For IJ = 1 to Contador_Redenciones_MNM
	    VoucherProductName[IJ] = Redencion_Aplicada_MNM[IJ]
	EndFor
	
Endsub
Sub Verifica_Descuento_Aplicado_Cheque
    Descuento_Aplicado_Cheque = ""
    For I = 1 to @NumDTLT
        If @dtl_type[I] = "D" and @dtl_Is_Void[I] = 0
           Descuento_Aplicado_Cheque = "T"
           Break
        EndIf
    EndFor
EndSub
Sub Verifica_Miembro_Cheque
    Miembro_Existe = ""
    For I = 1 to @NumDTLT
        If @dtl_type[I] = "I" And Len(Trim(@DTL_NAME[I])) > 0
           Miembro_Existe = "T"
        EndIf 
    EndFor
EndSub
Sub Version_ISL
    Var Demo : A40 = ""
    Format Demo as "VERSION MICROS ISL 1.00.01"
    Prompt Demo
    MSleep(1000)
EndSub
Sub Busca_Forma_Pago_Sbux
    Forma_Pago_Sbux = ""
    For I = 1 to @NumDTLT
        If @dtl_type[I] = "T" and @dtl_Is_Void[I] = 0
           If ( @DTL_OBJECT[I] = FormaPagoSbux ) or ( @DTL_OBJECT[I] = FormaPagoSbuxOffLine )
              Forma_Pago_Sbux = "T"
              Break
           EndIf
        EndIf
    EndFor
EndSub

//Sub Read_Temporal // Comentado el 14 ABR 2016, no hay sentencia que invoque esta funcion
//    Var   FN004  : N6  = 0
//    Sim4 = ""
    
//    //24Dic2014 
//    Var File1       : A100 = ""
//    Format File1   as FileTemporal
//    Call Asigna_Path_Archivos(File1)}
//    FOpen  FN004, File1, Read
//    //24Dic2014 
    
//    //24Dic2014 FOpen FN004, FileTemporal, Read
//    If FN004 <> 0
//       FRead FN004, Sim4
//       FClose FN004
//    EndIf
//EndSub


//Sub Borra_Temporal // Comentado el 14 ABR 2016
//    Var    FN001   : N6 = 0
//    
//    //24Dic2014 
//    Var File1       : A100 = ""
//    Format File1   as FileTemporal
//    Call Asigna_Path_Archivos(File1)}
//    FOpen  FN001, File1, Write
//    //24Dic2014 
    
//    //24Dic2014 FOpen  FN001, FileTemporal, Write
//    FWrite FN001, " "
//    FClose FN001
//    System "Del Temporal.TxT"
//Endsub


Sub Lee_Miembro_Archivo
    Var FN001 : N6 = 0
    Var JJ    : N6 = 0
    Call Borra_Array
    
    //24Dic2014 
    Var File1       : A100 = ""
    //Format File1   as FileMiembro
    //Call Asigna_Path_Archivos(File1)}
    //FOpen  FN001, File1, Read
    ////24Dic2014 
    
    ////24Dic2014 FOpen  FN001, FileMiembro, Read
    //IF FN001 = 0
    //   Respuesta = ""
    //   Cheque    = 0
    //   Return
    //ENDIF
    Contador_Redenciones = 10
    //FRead  FN001, Cheque, Respuesta
    //FClose FN001
	
    //-------11 ABR 2016
	Cheque = Cheque_Miembro
	Format Respuesta as rspst_Miembro
	//-------
	
    If Mid(Respuesta,1,3) = "ALL"
       Split Respuesta, "|", NumeroMiembro, Nombre
    Else
       Split Respuesta, "|", NumeroMiembro, Nombre, Activo, Nivel, Organizacion, Nivel_Id, Nivel_nombre, \
                          Contador_Campos,  PromocionId[]: PromocionNombre[]: PromocionUniqueKey[]: \
                                            PromocionDisplayName[]: PromocionStatus[], \
                          Contador_Voucher, VoucherCalStatus[]: VoucherProductId[]: VoucherProductName[]: \
                                            VoucherStatus[]: VoucherTransactionId[]: validDiscountAmount[]:  validDiscountType[]: minimumAmount[], \
                          BirthDate, FavoriteProduct, LastBeverage, LastFood, LifetimePoint1Value, LifetimePoint2Value, \
                          Status, AttributeValue, ErrorCode, pointAmount
    EndIf
EndSub
Sub Graba_Consulta_Miembro_Screen(Ref Graba_Datos_) // Modificado el 12 ABR 2016
//    Var FN001 : N6 = 0
    Var IJ    : N2 = 0
    
//    //24Dic2014 
//    Var File1       : A100 = ""
//    Format File1   as FileMiembroScreen
//    Call Asigna_Path_Archivos(File1)}
//    FOpen  FN001, File1, Append
//    //24Dic2014 
    
//    //24Dic2014 FOpen  FN001, FileMiembroScreen, Append
//    FWrite FN001, Graba_Datos_
//    FClose FN001
	
	For IJ = 1 to 10
		If FileMiembroScreenAppend[IJ] = ""
		    FiloeMiembroScreenAppend[IJ] = Graba_Datos_
			break
		EndIf
	EndFor
	
	//infomessage "Datos apilados exitosamente"
	
Endsub
Sub Borra_Consulta_Miembro_Screen // Modificado el 12 ABR 2016
//    Var    FN001   : N6 = 0
    Var IJ         : N2 = 0
//    //24Dic2014 
//    Var File1       : A100 = ""
//    Format File1   as FileMiembroScreen
//    Call Asigna_Path_Archivos(File1)}
//    FOpen  FN001, File1, Write
//    //24Dic2014 
    
//    //24Dic2014 FOpen  FN001, FileMiembroScreen, Write
//    FWrite FN001, " "
//    FClose FN001
//    System "Del MiembroScreenCRM.TxT"
	
	For IJ = 1 to 10
	    FiloeMiembroScreenAppend[IJ] = ""
	EndFor
	
	
Endsub
Sub Carga_Miembro_Cheque_Screen
    Var Conta  : N3  = 0
    Var Puntos_: N3  = 0
    
    //WaitForEnter "4"
    
    Call Muestra_datos_consulta_pantalla
    //Call Lee_Privilegios_Nivel
    Format Campo as Mid(Nombre, 1, 24)
    Call Graba_Consulta_Miembro_Screen(Campo)
    Format Campo as Trim(Activo), " ", Trim(Nivel), " ", Mid(BirthDate,1,5)
    Call Graba_Consulta_Miembro_Screen(Campo)
    If Len(trim(Saldo)) > 0
       Format Campo as "Saldo ", Saldo
       Call Graba_Consulta_Miembro_Screen(Campo)
    EndIf
    
    If Len(Trim(FavoriteProduct)) > 0
       //06Nov2014 Format Campo as "P:", Mid(FavoriteProduct, 1, 22)
       //06Nov2014 Call Graba_Consulta_Miembro_Screen(Campo)
    EndIf
    
    If Len(Trim(LastBeverage)) > 0
       Format Campo as "B:", Mid(LastBeverage,    1, 22)
       Call Graba_Consulta_Miembro_Screen(Campo)
    EndIf
    
    If Len(Trim(LastFood))
       Format Campo as "F:", Mid(LastFood,        1, 22)
       Call Graba_Consulta_Miembro_Screen(Campo)
    EndIf
    
    Contador_Redenciones = 0
    ClearArray Redencion_Aplicada
    
    //WaitForEnter VoucherCalStatus[1]
    //WaitForEnter VoucherProductName[1]
    
    
    For I = 1 to 20
        If Trim(VoucherStatus[I]) = "Available" And Len(Trim(VoucherProductName[I])) > 0 
           Format Campo as "", Mid(VoucherProductName[I], 1, 17)
           Format Campo_ as Mid(VoucherProductName[I], 1, 16)
           If Trim(VoucherProductName[I]) = "Beb Cumple"
              If @MONTH = MesBirthDate And AnioBirthDate_A <> Trim(AttributeValue)
                 Call Guarda_en_Cheque_Screen
              ENDIF
           Else
           	  Puntos_ = 0
           	  //New 29Nov2011 
              Call Carga_Miembro_Cheque_Nivel_Screen(Campo_, Puntos_)
              //New 29Nov2011 
              If Puntos_ = 0
           	     Call Guarda_en_Cheque_Screen
           	  EndIf
           ENDIF
        ENDIF
    EndFor
    //03Ene2012 Call Carga_Miembro_Cheque_Nivel_Dos
    If Conta > 0 
       Contador_Redenciones = Conta
       Call Graba_Privilegios_Nivel_Miembro
    EndIf
EndSub
Sub Guarda_en_Cheque_Screen
    Call Graba_Consulta_Miembro_Screen(Campo)
EndSub
Sub Lee_Miembre_Screen // Modificado el 12 ABR 2016
    Var FN001      : N6  = 0
    Var Contador_R : N3  = 0
    Var Campo_L    : A30 = ""
    Window 12,78
    
//    //24Dic2014 
//    Var File1       : A100 = ""
//    Format File1   as FileMiembroScreen
//    Call Asigna_Path_Archivos(File1)}
//    FOpen  FN001, File1, Read
//    //24Dic2014 
    
//    //24Dic2014 FOpen FN001, FileMiembroScreen, Read
//    If FN001 = 0
//       Return
//    EndIf
//    While Not Feof(FN001)
//          FRead  FN001, Campo_L
//          If Len(Trim(Campo_L)) > 0
//             Contador_R = ( Contador_R + 1 )
//             Display Contador_R, 1, Campo_L
//          EndIf
//    EndWhile
//    FClose FN001

	//------- 12 ABR 2016
    For Contador_R = 1 to 10
	    Campo_L = FiloeMiembroScreenAppend[Contador_R]
		If Len(Trim(Campo_L)) > 0
		    Display Contador_R, 1, Campo_L
	    EndIf
	EndFor
	//-------

	TouchScreen Numero_TouchscreenN
    Input FN001, "Enter Para Terminar"
    WindowClose
Endsub

Sub Pantalla_Error_Redencion_Acreditacion_Nueva(Ref Titulo_)
    Var I  : N3 = 0
    If CC01 > 0 and CC01 < 13
       Window CC01,78, Titulo_
              For I = 1 to CC01
                  Display  I, 1, Mid(Errores_Mostrar[I],1,77)
              EndFor
              TouchScreen Numero_TouchscreenN 
              Input G, "[Enter] para Continuar"
              //WaitForEnter "[Enter] para Continuar"
    WindowClose
    EndIf
EndSub
Sub Rutina_Muestra_Mensajes_Acreditaciones_Principal(Ref Estatus_)
    Var Camp01           : A99 = ""
    Var Camp02           : A99 = ""
    Format Camp01 as "<ACREDITACIONES>"
    If      Trim(Estatus_) = "Success"
       Format Camp02 as "<Success>"
    ElseIf  Trim(Estatus_) = "Fail"
       Format Camp02 as "<Fail>"
    ElseIf Mid(Estatus_,1,4) = "Fail" And Mid(Estatus_,8,10) = "Incomplete"
       Format Camp02 as "<Fail = Incomplete>"
    Else
    	 InfoMessage Estatus_, "Mensaje Desconocido (ACREDITACIONES)"
    	 Return
    EndIf
    Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Camp01,Camp02)
    Call Pantalla_Error_Redencion_Acreditacion_Nueva(Camp01)
EndSub
Sub Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Ref Error1_, Ref Error2_)
    Var File_I    : A50 = ""
    Var FN01      : N6  = 0
    Var Paso1     : A60   = ""
    Var Paso2     : A60   = ""
    Var Paso3     : A60   = ""
    Var Paso4     : A60   = ""
    Var Paso5     : A99   = ""
    ClearArray Errores_Mostrar
    CC01 = 0
    
    //24Dic2014 IF @WSTYPE = 3
       //24Dic2014 Format File_I as "\cf\micros\etc\", FileCatalogoErrores
    //24Dic2014 ELSE
       //24Dic2014 Format File_I as FileCatalogoErrores
    //24Dic2014 ENDIF
    
    //24Dic2014 
    Var File1       : A100 = ""
    Format File1   as FileCatalogoErrores
    Call Asigna_Path_Archivos(File1)}
    FOpen  FN01, File1, Read
    //24Dic2014 
    
    //24Dic2014 Fopen FN01, File_I, Read
    If FN01 = 0 
       ExitWithError "NO EXISTE ARCHIVO DE [CATALOGO ERRORES CRM]"
    EndIf
    While  Not FEOF(FN01)
         FRead FN01, Paso1, Paso2, Paso3, Paso4, Paso5
         If Paso1 = Error1_
            If  Paso2 = Error2_
                CC01 = ( CC01 + 1 )
                Errores_Mostrar[CC01] = Paso5
            EndIf
         EndIf
    EndWhile
    Fclose FN01
EndSub
Sub Rutina_Muestra_Mensajes_Cancelaciones_Principal(Ref Estatus_)
    Var Camp01           : A99 = ""
    Var Camp02           : A99 = ""
    Format Camp01 as "<CANCELACIONES>"
    If      Trim(Estatus_) = "Success"
       Format Camp02 as "<Success>"
    ElseIf  Trim(Estatus_) = "Queued"
       Format Camp02 as "<Queued>"
    ElseIf Trim(Estatus_) = "Invalid"
       Format Camp02 as "<Invalid>"
    ElseIf Trim(Estatus_) = "Fail"
       Format Camp02 as "<Fail>"
    ElseIf Trim(Estatus_) = "Incomplete"
       Format Camp02 as "<Incomplete>"
    Else
    	 InfoMessage Estatus_, "Mensaje Desconocido (CANCELACIONES)"
    	 Return
    EndIf
    Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Camp01,Camp02)
    Call Pantalla_Error_Redencion_Acreditacion_Nueva(Camp01)
EndSub

Sub Rutina_Muestra_Mensajes_Redenciones_Principal(Ref Estatus_, Ref Status_, Ref Sub_Estatus_)
    Var Camp01           : A99 = ""
    Var Camp02           : A99 = ""
    Var Camp03           : A99 = ""
    Var Camp04           : A99 = ""
    Format Camp01 as "<REDENCIONES>"
    Format Camp03 as "<>"
    Format Camp04 as "<>"
    If      Trim(Estatus_) = "Success"
       Format Camp02 as "<Success>"
       If     Trim(Status_) = "Processed"
          Format Camp03 as "<Processed>"
          If Trim(Sub_Estatus_) = "Success"
             Format Camp04 as "<Success>"
          ElseIf Sub_Estatus_ = "No Promotions Qualified"
             Format Camp04 as "<No Promotions Qualified>"
          Else
          	InfoMessage Camp04, "Mensaje Desconocido (REDENCIONES.1)"
          	Return
          EndIf
       ElseIf Trim(Status_) = "Rejected Engine"
          Format Camp03 as "<Rejected Engine>"
          If Trim(Sub_Estatus_) = "Insufficient Member Balance"
             Format Camp04 as "<Insufficient Member Balance>"
          ElseIf Trim(Sub_Estatus_) = "Miembro Inactivo"
             Format Camp04 as "<Miembro Inactivo>"
          ElseIf Trim(Sub_Estatus_) = "Attribute Definition Inactive"
             Format Camp04 as "<Attribute Definition Inactive>"
          ElseIf Trim(Sub_Estatus_) = "Error en el programa"
             Format Camp04 as "<Error en el programa>"
          Else
          	InfoMessage Camp04, "Mensaje Desconocido (REDENCIONES.2)"
          	Return
          EndIf
       Else
       	  InfoMessage Status_, "Mensaje Desconocido (REDENCIONES.3)"
       	  Return
       EndIf
    ElseIf  Estatus_ = "Fail"
       Format Camp02 as "<Fail>"
    ElseIf Mid(Estatus_,1,4) = "Fail" And Mid(Estatus_,8,10) = "Incomplete"
       Format Camp02 as "<Fail = Incomplete>"
    ElseIf Estatus_ = "Fatal"
       Format Camp02 as "<Fatal>"
    ElseIf Mid(Estatus_,1,5) = "Fatal" And Mid(Estatus_,9,10) = "Incomplete"
       Format Camp02 as "<Fatal = Incomplete>"
    Else
    	 InfoMessage Estatus_, "Mensaje Desconocido (REDENCIONES.4)"
    	 Return
    EndIf
    Call Lee_Catalogo_Errores_Redenciones_CRM(Camp01,Camp02, Camp03, Camp04)
    Call Pantalla_Error_Redencion_Acreditacion_Nueva(Camp01)
EndSub
Sub Lee_Catalogo_Errores_Redenciones_CRM(Ref Error1_, Ref Error2_, Ref Error3_, Ref Error4_)
    Var File_I    : A50 = ""
    Var FN01      : N6  = 0
    Var Paso1     : A60   = ""
    Var Paso2     : A60   = ""
    Var Paso3     : A60   = ""
    Var Paso4     : A60   = ""
    Var Paso5     : A99   = ""
    ClearArray Errores_Mostrar
    CC01 = 0
    
    //24Dic2014 IF @WSTYPE = 3
       //24Dic2014 Format File_I as "\cf\micros\etc\", FileCatalogoErrores
    //24Dic2014 ELSE
       //24Dic2014 Format File_I as FileCatalogoErrores
    //24Dic2014 ENDIF
    
    //24Dic2014 
    Var File1       : A100 = ""
    Format File1   as FileCatalogoErrores
    Call Asigna_Path_Archivos(File1)}
    FOpen  FN01, File1, Read
    //24Dic2014 
    
    //24Dic2014 Fopen FN01, File_I, Read
    If FN01 = 0 
       ExitWithError "NO EXISTE ARCHIVO DE [CATALOGO ERRORES CRM]"
    EndIf
    While  Not FEOF(FN01)
         FRead FN01, Paso1, Paso2, Paso3, Paso4, Paso5
         If Paso1 = Error1_
            If  Paso2 = Error2_
                If  Paso3 = Error3_
                    If  Paso4 = Error4_
                        CC01 = ( CC01 + 1 )
                        Errores_Mostrar[CC01] = Paso5
                    EndIf
                EndIf
            EndIf
         EndIf
    EndWhile
    Fclose FN01
EndSub

Sub Carga_Miembro_Cheque_Nivel_Screen(ref Campo__, ref Puntos__)
    Var IO   : N3 = 0
    Var Ptos : N3 = 0
    Puntos__      = 0
    For IO = 1 to 30
        If Len(Trim(RewardName_A[IO])) > 0
           //WaitForEnter "1"
           If Trim(RewardName_A[IO]) = Campo__
              If Trim(RewardTypeDiscount_P_M_A[IO]) = "Redeem"
                 //WaitForEnter "2"
                 Ptos = RewardPoints[IO]
                 //WaitForEnter LifetimePoint2Value  //14
                 //WaitForEnter Ptos   //15
                 If LifetimePoint2Value >= Ptos
                    //WaitForEnter "Puntos"
                    Format Campo  as "", Mid(RewardName_A[IO], 1, 16)
                    Format Campo_ as Mid(RewardName_A[IO], 1, 16)
                    Puntos__ = Ptos
                    Call Guarda_en_Cheque
                    Return
                 Else
                 	  //WaitForEnter "3"
                 	  If LifetimePoint2Value = 0
                 	     Puntos__ = 1
                 	  Else
                  	   Puntos__ = LifetimePoint2Value
                  	EndIf
                 EndIf
              ElseIf Trim(RewardTypeDiscount_P_M_A[IO]) = "Frecuency" And Campo__ = "Beb Gold"
                 //WaitForEnter "4"
                 Puntos__ = 1
              Else
              	//WaitForEnter "5"
             	  //Puntos__ = 0
              EndIf
           Else
           	  //WaitForEnter "6"
           	  //Puntos__ = 0
           EndIf
        EndIf
    EndFor
EndSub
Sub Borra_Off_Line_CRM //Modificado el 12 ABR 2016
//    Var FN001   : N6 = 0
//    Var Campo_1 : A99 = ""
    
//    //24Dic2014 
//    Var File1       : A100 = ""
//    Format File1   as FileOff_Line_CRM
//    Call Asigna_Path_Archivos(File1)}
//    FOpen  FN001, File1, Write
//    //24Dic2014 
    
//    //24Dic2014 FOpen  FN001, FileOff_Line_CRM, Write
//    FWrite FN001, " "
//    FClose FN001
//    Format Campo_1 as "DEL ", FileOff_Line_CRM
//    System Campo_1
	
	//------- 12 ABR 2016
	FileOff_Line_CRM_string = ""
	//-------
	
	
	
Endsub
Sub Graba_Off_Line_CRM //Modificado el 12 ABR 2016
//    Var FN001 : N6 = 0
    
//    //24Dic2014 
//    Var File1       : A100 = ""
//    Format File1   as FileOff_Line_CRM
//    Call Asigna_Path_Archivos(File1)}
//    FOpen  FN001, File1, Write
//    //24Dic2014 
    
//    //24Dic2014 FOpen  FN001, FileOff_Line_CRM, Write
//    FWrite FN001, Campo_Busqueda
//    FClose FN001
//    Flag_Off_Line = "T"
//	//infoMessage Campo_Busqueda // 25 ENE 2016 <===== JMG
	//------- 12 ABR 2016
	Format FileOff_Line_CRM_string as Campo_Busqueda
	//-------
	
	
Endsub
Sub Read_Off_Line_CRM //Modificado el 12 ABR 2016
//    Var FN004  : N6  = 0
//    Var Campo_ : A50 = ""
    
//    //24Dic2014 
//    Var File1       : A100 = ""
//    Format File1   as FileOff_Line_CRM
//    Call Asigna_Path_Archivos(File1)}
//    FOpen  FN004, File1, Read
//    //24Dic2014 
    
//    //24Dic2014 FOpen FN004, FileOff_Line_CRM, Read
//    If FN004 <> 0
//       FRead FN004, TicketNumber_Off
//       FClose FN004
//    EndIf
	
	//------- 12 ABR 2016
	Format TicketNumber_Off as FileOff_Line_CRM_string
	//-------
	
EndSub
Sub Manda_Ejecuta_Acreditaciones_OffLine_WS_JAR
    Manda = ""
  //Format Manda, Separador as "OFF", TicketNumber                                         //03Dic2014
    Format Manda, Separador as "OFF", TicketNumber, Trim(Miembro_2[2]), Nombre_Miembro_O   //03Dic2014
    
    TxMsg Manda
    GetRxmsg "Espere...OFF"
EndSub
Sub Graba_Dos_Off_Line_CRM // Modificado el 12 ABR 2016
//    Var FN004  : N6  = 0
//    Var Campo_ : A50 = ""
    
//    //24Dic2014 
//    Var File1       : A100 = ""
//    Format File1   as FileOff_Line_CRM
//    Call Asigna_Path_Archivos(File1)}
//    FOpen  FN004, File1, Append
//    //24Dic2014 
    
//    //24Dic2014 FOpen FN004, FileOff_Line_CRM, Append
//    If FN004 <> 0
//       FRead FN004, TicketNumber
//       FClose FN004
//    EndIf
	//------- 12 ABR 2016
	If FileOff_Line_CRM_string <> ""
	    Format TicketNumber as FileOff_Line_CRM_string
	EndIf
	//-------
	
EndSub
Sub Lee_Temporal_File
    Var FN004   : N6  = 0
    Var Campo_1 : A50 = ""
    Var Campo_2 : A50 = ""
    
    //24Dic2014 
    Var File1       : A100 = ""
    Format File1   as "Temporal.TxT"
    Call Asigna_Path_Archivos(File1)}
    FOpen  FN004, File1, Read
    //24Dic2014 
    
    //24Dic2014 FOpen FN004, "Temporal.TxT", Read
    If FN004 = 0
       Return
    EndIf
    FRead FN004, Campo_1, Campo_2
    FClose FN004
    Error_Time_Out_SbuxCard = Campo_2
EndSub
Sub Valida_Respuesta_Jar_Redencion
    If Trim(@RxMsg) = "_timeout"
       Status_Respuesta = "O"
    ElseIf Mid(Trim(@RxMsg),1,25) = "PROCESADO: NO CONTESTA WS" or Mid(Trim(@RxMsg),2,25) = "PROCESADO: NO CONTESTA WS"
       Status_Respuesta = "P"
    ENDIF
EndSub
Sub Manda_Ejecuta_Redenciones_Solamente_WS_JAR(Ref Ejecuta_)
    Manda = ""
    Format Manda, Separador as "JAR_R ", Ejecuta_, TicketNumber, MemberNumber
    TxMsg Manda
    GetRxmsg "Espere...A"
EndSub

Event Pickup_Check_13Jul2015   ///Socio Starbucks Rewards  (SI / NO)  DRIVE THRU  Seguir Cuenta
      //WaitForEnter "Inicio 6.Pickup"  //OK
      //08Mar2012 If @RVC = 1
         Call Lee_Miembro_Servicio
         //WaitForEnter Flag_Copiar  //
       
       //04Jun2013
       //WaitForEnter "Verifica_Llama_Cheque_Continuar"  //OK
       
       Call Lee_Miembro_PR
       //WaitForEnter ALL
       
       //WaitForEnter Mid(Respuesta,1,10)
       //WaitForEnter ALL
       
       If ALL = "ALL" //or Len(ALL) > 1
          //WaitForEnter "ALL"    //4812
          //Call Borra_Miembro_PR   //09Dic2014
          Call Continuacion(7, 56)
          //Call Borra_Miembro_PR
       EndIf
       //04Jun2013
       
       Call Lee_FileMiembro_Pausa_R_Cheque
       //WaitForenter Cheque_PR
       //WaitForenter @CKNUM
       
       If Cheque_PR = @CKNUM
          //WaitForEnter "Entre" /4812
          //23Dic2014 Call Borra_Miembro_PR
          Call Continuacion(7, 56)
          //Call Borra_Miembro_PR
       EndIf
       
         If Flag_Copiar = "T"
            //Call Copiar_Archivo(FileMiembroServicio,       FileMiembro) // Comentado el 14 ABR 2016
            //Call Copiar_Archivo(FileNivelServicio,         FileNivel) // Comentado el 14 ABR 2016
            //Call Copiar_Archivo(FileNivel_MiembroServicio, FileNivel_Miembro) // Comentado el 14 ABR 2016
            //Call Copiar_Archivo(FileConsultaSaldoServicio, FileConsultaSaldo) // Comentado el 14 ABR 2016
            Call Borra_Miembro_Servicio
            //Call Borra_Privilegios_Nivel_Servicio
            //Call Borra_Privilegios_Nivel_Miembro_Servicio
            //Call Borra_Consulta_Saldo_Servicio // Comentado el 14 ABR 2016
            Call Borra_MiembroCheckInfoServicio
         Else
         	  If @RVC <> 1
               Call Borra_Leyenda_CRM
               Call Socio_Starbucks_Rewards
               If   Flag_Socio_Starbucks_Rewards = "N"
	                  //Continua
               Else
      	            Call LoadSIMInquire(10, 31)   //Miembro
               EndIf
         	  EndIf
         EndIf
         LoadKyBdMacro Key (17,201)//SLU
      //08Mar2012 EndIf
EndEvent

Event Srvc_Total: 401
    //WaitForEnter "Srvc_Total 401"
    Prompt "Service..."
    Msleep(500)
    If @RVC = 1
       Call Lee_Miembro_Existe
       //WaitForEnter Flag_Copiar
       If Flag_Copiar = "T"
          //Call Copiar_Archivo(FileMiembro,       FileMiembroServicio) // Comentado el 14 ABR 2016
          //Call Copiar_Archivo(FileNivel,         FileNivelServicio) // Comentado el 14 ABR 2016
          //Call Copiar_Archivo(FileNivel_Miembro, FileNivel_MiembroServicio) // Comentado el 14 ABR 2016
          //Call Copiar_Archivo(FileConsultaSaldo, FileConsultaSaldoServicio) // Comentado el 14 ABR 2016
          Call Graba_Cheque_Continuar
          Call Graba_MiembroCheckInfoServicio
       
          Call Borra_Miembro
          //Call Borra_Privilegios_Nivel
          Call Borra_Privilegios_Nivel_Miembro
          //Call Borra_Consulta_Saldo // Comentado el 14 ABR 2016
       EndIf
    EndIf
EndEvent

Sub Copiar_Archivo(Ref Archivo_Fuente, Ref Archivo_Destino)
    Var Lectura   : N6 = 0
    Var Escritura : N6 = 0
    Var In        : N6 = 0
    Var Out       : N6 = 0
    Var Wout      : N6 = 2000


    //24Dic2014 
    Var File1       : A100 = ""
    Var File2       : A100 = ""
    Format File1   as Archivo_Fuente
    Format File2   as Archivo_Destino
    Call Asigna_Path_Archivos(File1)}
    Call Asigna_Path_Archivos(File2)}
    FOpen  Lectura,   File1, Read
    FOpen  Escritura, File2, Write
    //24Dic2014 
    
    //24Dic2014 FOpen  Lectura,   Archivo_Fuente, Read
    //24Dic2014 FOpen  Escritura, Archivo_Destino, Write
    IF Lectura = 0
       Respuesta = ""
       Return
    ENDIF
    While Not FEOF(Lectura)
          FReadBfr  Lectura, Respuesta, Wout, In
          FWriteBfr Escritura, Respuesta, In, Out
    EndWhile
    FClose Lectura
    FClose Escritura
EndSub

Sub Lee_Miembro_Servicio // Modificado el 15 ABR 2016
//    Var FN001 : N6 = 0
    
//    //24Dic2014 
//    Var File1       : A100 = ""
//    Format File1   as FileMiembroServicio
//    Call Asigna_Path_Archivos(File1)}
//    FOpen  FN001, File1, Read
//    //24Dic2014 
    
//    //24Dic2014 FOpen  FN001, FileMiembroServicio, Read
//    IF FN001 = 0
//       Respuesta = ""
//       Cheque    = 0
//       Return
//    ENDIF
//    FRead  FN001, Cheque, Respuesta
//    FClose FN001
	
	//-------15 ABR 2016
	Format Respuesta as rspst_Miembro_Servicio
    Cheque = Cheque_Miembro_Servicio
	//-------
    If Len(Respuesta) > 10 And Cheque = @CKNUM
       Flag_Copiar = "T"
    Else
    	 Flag_Copiar = "F"
    EndIf
EndSub

Sub Lee_Miembro_Existe
    Var FN001 : N6 = 0
    
    //24Dic2014 
    Var File1       : A100 = ""
    //Format File1   as FileMiembro
    //Call Asigna_Path_Archivos(File1)}
    //FOpen  FN001, File1, Read
    ////24Dic2014 
    
    ////24Dic2014 FOpen  FN001, FileMiembro, Read
    //IF FN001 = 0
    //   Respuesta = ""
    //   Cheque    = 0
    //   Return
    //ENDIF
    //FRead  FN001, Cheque, Respuesta
    //FClose FN001
	
    //-------11 ABR 2016
	Cheque = Cheque_Miembro
	Format Respuesta as rspst_Miembro
	//-------
	
    If Len(Respuesta) > 10
       Flag_Copiar = "T"
    Else
    	 Flag_Copiar = "F"
    EndIf
EndSub
Sub Graba_Cheque_Continuar// Modificado el 15 ABR 2016
    Var FN001   : N6 = 0
    
    //24Dic2014 
    Var File1       : A100 = ""
    Format File1   as ChequeContinuar
    Call Asigna_Path_Archivos(File1)}
    FOpen  FN001, File1, Write
    //24Dic2014 
    
    //24Dic2014 FOpen  FN001, ChequeContinuar, Write
    FWrite FN001, @CKNUM
    FClose FN001
	
	//------- 15 ABR 2016
	Cheque_ChqContinuar = @CKNUM
	//-------
Endsub
Sub Borra_Cheque_Continuar// Modificado el 15 ABR 2016
    Var FN001   : N6 = 0
    Var Campo_1 : A99 = ""
    
    //24Dic2014 
    Var File1       : A100 = ""
    Format File1   as ChequeContinuar
    Call Asigna_Path_Archivos(File1)}
    FOpen  FN001, File1, Write
    //24Dic2014 
    
    //24Dic2014 FOpen  FN001, ChequeContinuar, Write
    FWrite FN001, " "
    FClose FN001
    Format Campo_1 as "DEL ", ChequeContinuar
    System Campo_1
	//------- 15 ABR 2016
	Cheque_ChqContinuar = "0"
	//-------
Endsub
Sub Verifica_Llama_Cheque_Continuar// Modificado el 15 ABR 2016
    Var FN001 : N6 = 0
    
    //WaitForEnter "Verifica_Llama_Cheque_Continuar"
    
    //24Dic2014 
    Var File1       : A100 = ""
    Format File1   as ChequeContinuar
    Call Asigna_Path_Archivos(File1)}
    FOpen  FN001, File1, Read
    //24Dic2014 
    
    //24Dic2014 FOpen  FN001, ChequeContinuar, Read
    IF FN001 = 0
       Return
    ENDIF
    FRead  FN001, Cheque
    FClose FN001
    
    //WaitForEnter Cheque
    //------- 15 ABR 2016
	Cheque = Cheque_ChqContinuar
	//-------
	
    If Cheque > 0
       //WaitForEnter "Carga Cheque"
                   //Pickup Cheq No
       LoadKyBdMacro Key (1, 327684), MakeKeys(Cheque), @KEY_ENTER
       LoadKyBdMacro Key (17,201)//SLU
       //Call Copiar_Archivo(FileMiembroServicio,       FileMiembro) // Comentado el 14 ABR 2016
       //Call Copiar_Archivo(FileNivelServicio,         FileNivel) // Comentado el 14 ABR 2016
       //Call Copiar_Archivo(FileNivel_MiembroServicio, FileNivel_Miembro) // Comentado el 14 ABR 2016
       //Call Copiar_Archivo(FileConsultaSaldoServicio, FileConsultaSaldo) // Comentado el 14 ABR 2016
       //WaitForEnter "Antes de Borrar"
       Call Borra_Miembro_Servicio
       //Call Borra_Privilegios_Nivel_Servicio
       //Call Borra_Privilegios_Nivel_Miembro_Servicio
       //Call Borra_Consulta_Saldo_Servicio // Comentado 14 ABR 2016
       Call Borra_MiembroCheckInfoServicio
       Call Borra_Cheque_Continuar
       //Call Borra_Miembro_PR
       //WaitForEnter "Termino de Borrar"
       Borrar = "N"
    EndIf
EndSub
Sub Graba_MiembroCheckInfoServicio
//    Var FN001   : N6 = 0
    Var Cont_01 : N5 = 0
    
    //24Dic2014 
//    Var File1       : A100 = ""
//    Format File1   as MiembroCheckInfoServicio
//    Call Asigna_Path_Archivos(File1)}
//    FOpen  FN001, File1, Write
//    //24Dic2014 
	
//    //24Dic2014 FOpen  FN001, MiembroCheckInfoServicio, Write
//    For Cont_01 = 1 to @NUMDTLT
//        //WaitForEnter @DTL_TYPE[Cont_01]
//        If @DTL_TYPE[Cont_01] = "I" and Len(Trim(@DTL_NAME[Cont_01])) > 0
//          FWrite FN001, @DTL_NAME[Cont_01]
//        EndIf
//    EndFor
//    FClose FN001
	
	//------- 14 ABR 2016
	For Cont_01 = 1 to @NUMDTLT
	    If @DTL_TYPE[Cont_01] = "I" and Len(Trim(@DTL_NAME[Cont_01])) > 0
		   Format MiembroChkInfoServ[Cont_01] as @DTL_NAME[Cont_01]
        EndIf
	EndFor
	//-------
Endsub

Sub Borra_MiembroCheckInfoServicio
//    Var FN001   : N6  = 0
//    Var Campo_1 : A99 = ""
    
//    //24Dic2014 
//    Var File1       : A100 = ""
//    Format File1   as MiembroCheckInfoServicio
//    Call Asigna_Path_Archivos(File1)}
//    FOpen  FN001, File1, Write
//    //24Dic2014 
    
//    //24Dic2014 FOpen  FN001, MiembroCheckInfoServicio, Write
//    FWrite FN001, " "
//    FClose FN001
//    Format Campo_1 as "DEL ", File1
//    System Campo_1
	
	//------- 14 ABR 2016
	ClearArray MiembroChkInfoServ
	//-------
Endsub

Sub Lee_MiembroCheckInfoServicio
//    Var FN001 : N6 = 0
    Var IJ    : N3 = 0
//    //24Dic2014 
//   Var File1       : A100 = ""
//    Format File1   as MiembroCheckInfoServicio
//    Call Asigna_Path_Archivos(File1)}
//    FOpen  FN001, File1, Read
//    //24Dic2014 
    
//    //24Dic2014 FOpen  FN001, MiembroCheckInfoServicio, Read
//    IF FN001 = 0
//       Return
//    ENDIF
    ClearChkInfo
//    While Not Feof(FN001)
//          FRead  FN001, Respuesta
//          SaveChkInfo Respuesta
//    EndWhile
//    FClose FN001
	
	//------- 14 ABR 2016
	For IJ = 1 to 100
	    if MiembroChkInfoServ[IJ] <> ""
		    Format Respuesta as MiembroChkInfoServ[IJ]
			SaveChkInfo Respuesta
		endif
	
	EndFor
	//-------
	
EndSub

//Sub Lee_Miembro_SbuxCard //Comentado el 11 ABR 2016
//    Var    FN001   : N6 = 0
//    
//    //24Dic2014 
//    Var File1       : A100 = ""
//    Format File1   as FileMiembro
//    Call Asigna_Path_Archivos(File1)}
//    FOpen  FN001, File1, Write
//    //24Dic2014 
//    
//    //24Dic2014 FOpen  FN001, FileMiembro, Write
//    FWrite FN001, "ALL|", gstrSVCardNumber 
//    FClose FN001
//Endsub

Sub Lee_Miembro_Solo
    Var FN001   : N6 = 0
    Var Nombre1 : A20 = ""
    
    //24Dic2014 
    Var File1       : A100 = ""
    Format File1   as FileMiembroSolo
    Call Asigna_Path_Archivos(File1)}
    FOpen  FN001, File1, Read
    //24Dic2014 
    
    //24Dic2014 FOpen  FN001, FileMiembroSolo, Read
    IF FN001 = 0
       NumeroMiembroSolo = ""
       Nombre1 = ""
       Return
    ENDIF
    FRead  FN001, Nombre, NumeroMiembroSolo
    FClose FN001
    //WaitForEnter Nombre
    //WaitForEnter NumeroMiembroSolo
Endsub
Sub Borra_Lee_Miembro_Solo
    Var FN001   : N6 = 0
    Var Campo_1 : A99 = ""
    
    //24Dic2014 
    Var File1       : A100 = ""
    Format File1   as FileMiembroSolo
    Call Asigna_Path_Archivos(File1)}
    FOpen  FN001, File1, Write
    //24Dic2014 
    
    //24Dic2014 FOpen  FN001, FileMiembroSolo, Write
    FWrite FN001, " "
    FClose FN001
    Format Campo_1 as "DEL ", FileMiembroSolo
    System Campo_1
Endsub
Sub Borra_Miembro_PR // Modificado el 11 ABR 2016

    //WaitForenter "Borra_Miembro_PR"

    //Var FN001   : N6 = 0
    //Var Campo_1 : A99 = ""
    
    ////24Dic2014 
    //Var File1       : A100 = ""
    //Format File1   as FileMiembro_Pausa_R
    //Call Asigna_Path_Archivos(File1)}
    //FOpen  FN001, File1, Write
    ////24Dic2014 
    
    ////24Dic2014 FOpen  FN001, FileMiembro_Pausa_R, Write
    //FWrite FN001, " "
    //FClose FN001
    //Format Campo_1 as "DEL ", File1
    //System Campo_1
    
    ////24Dic2014 
    //Format File1   as FileMiembro_Pausa_R_Cheque
    //Call Asigna_Path_Archivos(File1)}
    //FOpen  FN001, File1, Write
    ////24Dic2014 
    
    ////24Dic2014 FOpen  FN001, FileMiembro_Pausa_R_Cheque, Write
    //FWrite FN001, " "
    //FClose FN001
    //Format Campo_1 as "DEL ", File1
    //System Campo_1
    
    //FileDelete = FileMiembro_Pausa_R
    //Call Delete_File(path_DeleteFile, FileDelete, status_DeleteFile)
    
    //System "Del ", FileMiembro_Pausa_R_Cheque
    
    //FileDelete   = FileMiembro_Pausa_R_Cheque
    //Call  Delete_File(path_DeleteFile, FileDelete, status_DeleteFile)
	
	//-------11 ABR 2016
	Miembro_Pausa_R_Cheque = 00
	
	rspst_Miembro_PR = ""

    Cheque_Miembro_PR = 0
	
	//-------
	
Endsub
Sub Lee_Miembro_PR // Modificado el 15 ABR 2016
//    Var FN001 : N6 = 0
    Var JJ    : N6 = 0
    Call Borra_Array
    
//    //24Dic2014 
//    Var File1       : A100 = ""
//    Format File1   as FileMiembro_Pausa_R
//    Call Asigna_Path_Archivos(File1)}
//    FOpen  FN001, File1, Read
//    //24Dic2014 
    
//    //24Dic2014 FOpen  FN001, FileMiembro_Pausa_R, Read
//    IF FN001 = 0
//       Respuesta = ""
//       Cheque    = 0
//       NumeroMiembro_PR = ""
//       Return
//    ENDIF
    
//    //WaitForenter FileMiembro_Pausa_R
    
//    FRead  FN001, Cheque, Respuesta
//    FClose FN001
    
	//-------15 ABR 2016
	Cheque = Cheque_Miembro_PR
	Format Respuesta as rspst_Miembro_PR
	
	//infomessage "Cheque y Respuesta copiados"
	//-------
	
	ALL = ""
    NumeroMiembro_PR = ""
    If Mid(Respuesta,1,3) = "ALL"
       Split Respuesta, "|", NumeroMiembro_PR, Nombre
       ALL = "ALL"
    Else
       Split Respuesta, "|", NumeroMiembro_PR, Nombre, Activo, Nivel, Organizacion, Nivel_Id, Nivel_nombre, \
                          Contador_Campos,  PromocionId[]: PromocionNombre[]: PromocionUniqueKey[]: \
                                            PromocionDisplayName[]: PromocionStatus[], \
                          Contador_Voucher, VoucherCalStatus[]: VoucherProductId[]: VoucherProductName[]: \
                                            VoucherStatus[]: VoucherTransactionId[]: validDiscountAmount[]:  validDiscountType[]: minimumAmount[], \
                          BirthDate, FavoriteProduct, LastBeverage, LastFood, LifetimePoint1Value, LifetimePoint2Value, \
                          Status, AttributeValue
       ALL = Cheque
    EndIf
Endsub


//New Crm 2014

Sub Manda_Redemption

    //27Ene2015
    Call Lee_Miembro_O
    //27Ene2015
    
    Manda = ""
    Format Manda, Separador as "RED", Arma_Cadena_82, Nombre_Miembro_O    ////27Ene2015
    TxMsg Manda
    GetRxmsg "Espere... Cliente WOW"
EndSub


Sub Valida_Redemption
    Var Campo_01_   : A50 = ""
    Var Campo_02_   : A50 = ""
    
    Salida_Consulta_Clientes = ""
    Format Campo_01_ as "<WS_TIMEOUT>"
    Format Campo_02_ as "<Success>"
    
    //WaitForenter @RxMsg
    
    If Mid(@RxMsg, 1, 32 ) = "invokeButton_Automatico (Error):"
       If @RVC = 2
          LoadKyBdMacro Key (19,102)
          Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
          Salida_Consulta_Clientes = "T"
       Else
          Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
       EndIf
       Call Graba_Off_Line_CRM
       ExitCancel
    EndIf
    If Trim(@RxMsg) = "NO EXISTE MIEMBRO"
       //26Dic2014
       Split @RxMsg, "|", Resp_Red[1], Resp_Red[2]
       //26Dic2014 WaitForEnter Resp_Red[1]
       //26Dic2014 WaitForEnter Resp_Red[2]
       Format Campo_01_ as "<WS_SOA_ERROR>"
       Format Campo_02_ as "<", Resp_Red[2], ">"
       //26Dic2014
       If @RVC = 2
          LoadKyBdMacro Key (19,102)
          InfoMessage Mid(@RxMsg, 1, 32 )
          Salida_Consulta_Clientes = "T"
       Else
       	  //26Dic2014
       	  Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
          ExitCancel
          //26Dic2014
          
       	  //26Dic2014 ExitWithError Mid(@RxMsg, 1, 32 )
       EndIf
    ElseIf Mid(Trim(@RxMsg),1, 26)  = "NO HAY COMUNICACION CON WS"
       //26Dic2014
       Split @RxMsg, "|", Resp_Red[1], Resp_Red[2]
       //26Dic2014 WaitForEnter Resp_Red[1]
       //26Dic2014 WaitForEnter Resp_Red[2]
       Format Campo_01_ as "<WS_SOA_ERROR>"
       Format Campo_02_ as "<", Resp_Red[2], ">"
       //26Dic2014
       
       //26Dic2014 Format Campo_01_ as "<WS_TIMEOUT>"
       //26Dic2014 Format Campo_02_ as "<Success>"
       Call Graba_Off_Line_CRM
	   
       If @RVC = 2
          LoadKyBdMacro Key (19,102)
          Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
          Salida_Consulta_Clientes = "T"
       Else
         	Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
       EndIf
       ExitCancel
    ElseIf Mid(Trim(@RxMsg),1, 26)  = "NO SE REALIZO LA REDENCION"
       //26Dic2014
       Split @RxMsg, "|", Resp_Red[1], Resp_Red[2]
       //26Dic2014 WaitForEnter Resp_Red[1]
       //26Dic2014 WaitForEnter Resp_Red[2]
       Format Campo_01_ as "<WS_SOA_ERROR>"
       Format Campo_02_ as "<", Resp_Red[2], ">"
       //26Dic2014
       //26Dic2014 Format Campo_01_ as "<REDENCIONES>"
       //26Dic2014 Format Campo_02_ as "<Fail>"
       Call Graba_Off_Line_CRM
       If @RVC = 2
          LoadKyBdMacro Key (19,102)
          Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
          Salida_Consulta_Clientes = "T"
       Else
         	Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
       EndIf
       ExitCancel
    ElseIf Mid(Trim(@RxMsg),1, 18)  = "ERROR EN REDENCION"
       //26Dic2014
       Split @RxMsg, "|", Resp_Red[1], Resp_Red[2]
       //26Dic2014 WaitForEnter Resp_Red[1]
       //26Dic2014 WaitForEnter Resp_Red[2]
       Format Campo_01_ as "<WS_SOA_ERROR>"
       Format Campo_02_ as "<", Resp_Red[2], ">"
       //26Dic2014
       //26Dic2014 Format Campo_01_ as "<WS_TIMEOUT>"
       //26Dic2014 Format Campo_02_ as "<Success>"
       Call Graba_Off_Line_CRM
       If @RVC = 2
          LoadKyBdMacro Key (19,102)
          Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
          Salida_Consulta_Clientes = "T"
       Else
         	Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
       EndIf
       ExitCancel
    ElseIf Trim(@RxMsg) = "_timeout"
       Format Campo_01_ as "<WS_TIMEOUT>"
       Format Campo_02_ as "<Success>"
       Call Graba_Off_Line_CRM
       If @RVC = 2
          LoadKyBdMacro Key (19,102)
          Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
          Salida_Consulta_Clientes = "T"
       Else
       	  Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
       EndIf
       ExitCancel
    EndIf
    Format Respuesta as @RxMsg	
    Call   Graba_Archivo_datos(Respuesta)
EndSub

Sub Separa_Campos_Redemption
    Var JJ                        : N3 = 0
    Var KK                        : N3 = 0
    Var AnioDate                  : N4 = 0
    Var AnioDate_A                : A4 = ""
    Var BirthDate2                : A30 = ""
    Var FN002                     : N6 = 0
    
    //WaitForEnter "5"
    
    Call Borra_Array
    BirthDate = ""
    //WaitForEnter "Inicio 11"
    Split Respuesta, "|", NumeroMiembro, Nombre, Activo, Nivel, Organizacion, Nivel_Id, Nivel_nombre, \
                       Contador_Campos,  PromocionId[]: PromocionNombre[]: PromocionUniqueKey[]: \
                                         PromocionDisplayName[]: PromocionStatus[], \
                       Contador_Voucher, VoucherCalStatus[]: VoucherProductId[]: VoucherProductName[]: \
                                         VoucherStatus[]: VoucherTransactionId[]: validDiscountAmount[]:  validDiscountType[]: minimumAmount[], \
                       BirthDate, FavoriteProduct, LastBeverage, LastFood, LifetimePoint1Value, LifetimePoint2Value, \
                       Status, AttributeValue

    If Len(Trim(BirthDate)) > 0
       MesBirthDate_A = Mid(BirthDate, 1, 2)
       MesBirthDate   = MesBirthDate_A
       DiaBirthDate_A = Mid(BirthDate, 4, 2)
       DiaBirthDate   = DiaBirthDate_A
    ENDIF
    Format AnioBirthDate_A as "20", @YEAR{02}
    AnioBirthDate           = AnioBirthDate_A

    Format AnioDate_A as "20", @YEAR{02}
    AnioDate = AnioDate_A
    if @MONTH = MesBirthDate And DiaBirthDate > @DAY
       AnioDate = ( AnioDate )
    Else
    	 AnioDate = ( AnioDate + 1 )
    EndIf
    Format Fecha_Cumplanios_A as "20", @YEAR{02}, "-", @MONTH{02}, "-", @DAY{02}
    Format Fecha_Cumplanios_D as AnioDate, "-", MesBirthDate, "-", DiaBirthDate
    If Len(Trim(AttributeValue)) > 0
       If @MONTH = MesBirthDate And AnioBirthDate_A = Trim(AttributeValue)
          For JJ = 1 to Contador_Campos
              If Trim(PromocionNombre[JJ]) = "Beb Cumple"
                 PromocionNombre[JJ] = " "
              EndIf
          EndFor
          For JJ = 1 to Contador_Voucher
              If Trim(VoucherProductName[JJ]) = "Beb Cumple"
                 VoucherProductName[JJ] = " "
              EndIf
          EndFor
       Else
       EndIf
    Else
       If @MONTH <> MesBirthDate
          For JJ = 1 to Contador_Campos
              If Trim(PromocionNombre[JJ]) = "Beb Cumple"
                 PromocionNombre[JJ] = " "
              EndIf
          EndFor
          For JJ = 1 to Contador_Voucher
              If Trim(VoucherProductName[JJ]) = "Beb Cumple"
                 VoucherProductName[JJ] = " "
              EndIf
          EndFor
       Else
       EndIf
    EndIf
EndSub


Sub Manda_Acreditacion
    
    //WaitForenter MemberNumber
    Split MemberNumber, ":", Miembro_2[1], Miembro_2[2]
    //WaitForenter Miembro_2[1]
    //WaitForenter Miembro_2[2]
    Call Lee_Miembro_O
    Manda = ""
    If Len(Trim(Miembro_2[2])) = 0
       Format Manda, Separador as "ACR", Trim(TicketNumber), Trim(MemberNumber), Nombre_Miembro_O
    Else
       Format Manda, Separador as "ACR", Trim(TicketNumber), Trim(Miembro_2[2]), Nombre_Miembro_O
    EndIf
    
    //06NOV2014
    If Len(Trim(TicketNumber)) = 0 And Len(Trim(MemberNumber)) = 0
       Salir_ACR = "T"
    EndIf
    //06NOV2014
    
    If Salir_ACR = "T"
       Return
    EndIf
    
    TxMsg Manda
    GetRxmsg "Espere... Cliente WOW"
EndSub

Sub Valida_Acreditacion
    Var Campo_01_ : A50 = ""
    Var Campo_02_ : A50 = ""
    Salida_Consulta_Clientes = ""
    Format Campo_01_ as "<WS_TIMEOUT>"
    Format Campo_02_ as "<Success>"
    
	//infomessage Mid(@RxMsg, 1, 52) // 25 ENE 2016 <===== JMG
	
    //WaitForenter "Respuesta Valida_Acreditacion"  //OK
    //WaitForenter Mid(@RxMsg, 1, 32 )              //OK
    If Mid(@RxMsg, 1, 32 ) = "invokeButton_Automatico (Error):"
       //WaitForenter "invokeButton_Automatico (Error):"
       
	   If @RVC = 2
          LoadKyBdMacro Key (19,102)
          Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
          Salida_Consulta_Clientes = "T"
       Else
          Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
       EndIf
       Call Graba_Off_Line_CRM
       FP_Sbux_Aplicado_Off_Line = "T"
       //20Jul2015 ExitCancel
    //20Jul2015 EndIf
    ElseIf Trim(@RxMsg) = "NO EXISTE MIEMBRO"
       //WaitForenter "NO EXISTE MIEMBRO"
       //26Dic2014
       Split @RxMsg, "|", Resp_Red[1], Resp_Red[2]
       //26Dic2014 WaitForEnter Resp_Red[1]
       //26Dic2014 WaitForEnter Resp_Red[2]
       Format Campo_01_ as "<WS_SOA_ERROR>"
       Format Campo_02_ as "<", Resp_Red[2], ">"
       //26Dic2014
       
       If @RVC = 2
          LoadKyBdMacro Key (19,102)
          InfoMessage Mid(@RxMsg, 1, 32 )
          Salida_Consulta_Clientes = "T"
       Else
       	  //26Dic2014
       	  Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
          ExitCancel
          //26Dic2014 
          
       	  //26Dic2014 ExitWithError Mid(@RxMsg, 1, 32 )
       EndIf
    ElseIf Mid(Trim(@RxMsg),1, 26)  = "NO HAY COMUNICACION CON WS"
       //WaitForenter "NO HAY COMUNICACION CON WS"
       //26Dic2014
       Split @RxMsg, "|", Resp_Red[1], Resp_Red[2]
       //26Dic2014 WaitForEnter Resp_Red[1]
       //26Dic2014 WaitForEnter Resp_Red[2]
       //13Ene2015 Format Campo_01_ as "<WS_SOA_ERROR>"
       //13Ene2015 Format Campo_02_ as "<", Resp_Red[2], ">"
       //26Dic2014
       
       If Len(Resp_Red[2]) > 0
          Format Campo_01_ as "<WS_SOA_ERROR>"
          Format Campo_02_ as "<", Resp_Red[2], ">"
       Else
       	  Format Campo_01_ as "<WS_TIMEOUT>"
       	  Format Campo_02_ as "<NO_WS>"
       EndIf
       
       //26Dic2014 Format Campo_01_ as "<WS_TIMEOUT>"
       //26Dic2014 Format Campo_02_ as "<Success>"
       Call Graba_Off_Line_CRM
       FP_Sbux_Aplicado_Off_Line = "T"
       If @RVC = 2
          LoadKyBdMacro Key (19,102)
          Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
          Salida_Consulta_Clientes = "T"
       Else
         	Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
       EndIf
       //20Jul2015 ExitCancel
    ElseIf Mid(Trim(@RxMsg),1, 29)  = "NO SE REALIZO LA ACREDITACION"
       //WaitForenter "NO SE REALIZO LA ACREDITACION"
       //26Dic2014
       Split @RxMsg, "|", Resp_Red[1], Resp_Red[2]
       //26Dic2014 WaitForEnter Resp_Red[1]
       //26Dic2014 WaitForEnter Resp_Red[2]
       Format Campo_01_ as "<WS_SOA_ERROR>"
       Format Campo_02_ as "<", Resp_Red[2], ">"
       //26Dic2014
      
       //26Dic2014 Format Campo_01_ as "<WS_TIMEOUT>"
       //26Dic2014 Format Campo_02_ as "<Success>"
       Call Graba_Off_Line_CRM
       FP_Sbux_Aplicado_Off_Line = "T"
	   
	   //Call Rutina_Borra_Archivos_Temporales    // 25 ENE 2016 <======= JMG 
       //Call Rutina_Borra_Archivos_Temporales_Dll //
	   //Call Rutina_Borra_Archivos_Temporales_Wow //
       //Call Borra_FileMiembro_Soa                //
       If @RVC = 2
          LoadKyBdMacro Key (19,102)
          Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
          Salida_Consulta_Clientes = "T"
       Else
         	Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
       EndIf
	  
       //ExitCancel//20Jul2015 
    ElseIf Mid(Trim(@RxMsg),1, 21)  = "ERROR EN ACREDITACION"
       //WaitForenter "ERROR EN ACREDITACION"
       //26Dic2014
       Split @RxMsg, "|", Resp_Red[1], Resp_Red[2]
       //26Dic2014 WaitForEnter Resp_Red[1]
       //26Dic2014 WaitForEnter Resp_Red[2]
       Format Campo_01_ as "<WS_SOA_ERROR>"
       Format Campo_02_ as "<", Resp_Red[2], ">"
       //26Dic2014
       //26Dic2014 Format Campo_01_ as "<WS_TIMEOUT>"
       //26Dic2014 Format Campo_02_ as "<Success>"
       Call Graba_Off_Line_CRM
       FP_Sbux_Aplicado_Off_Line = "T"
       If @RVC = 2
          LoadKyBdMacro Key (19,102)
          Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
          Salida_Consulta_Clientes = "T"
       Else
         	Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
       EndIf
       //20Jul2015 ExitCancel
    ElseIf Trim(@RxMsg) = "_timeout"
       //WaitForenter "_timeout"
       Format Campo_01_ as "<WS_TIMEOUT>"
       Format Campo_02_ as "<Success>"
       Call Graba_Off_Line_CRM
       FP_Sbux_Aplicado_Off_Line = "T"
	   If @RVC = 2
          LoadKyBdMacro Key (19,102)
          Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
          Salida_Consulta_Clientes = "T"
       Else
       	  Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
       EndIf
       //20Jul2015 ExitCancel
    EndIf
    //WaitForenter "Termino"
    Format Respuesta as "Respuesta Acreditacion:", @RxMsg
    Call   Graba_Archivo_datos(Respuesta)
EndSub

Sub Manda_Ejecuta_Acreditaciones_Redenciones_WS_Soa
    Manda = ""
    Format Manda, Separador as "JARS ", TicketNumber, MemberNumber
    TxMsg Manda
    GetRxmsg "Espere...x"
EndSub

Sub Formatea_Datos_Redenciones
   
    Var Nombre_Mesero : A100 = ""
    Format Nombre_Mesero As @Tremp, " ", Trim(@TREMP_FNAME), " ", Trim(@TREMP_LNAME)
    
    //16Ene2015 AdjustedListPrice = Item_Precio_Seleccionado
    //06Nov2014 WaitForenter AdjustedListPrice
    TransactionType = "Queued"
    
    Format Arma_Cadena_82 as "", MemberNumber, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", id, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", ALSELevelPrice, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", BranchDivision, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", ALSEPaymnetMode, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", ActivityDate, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", AdjustedListPrice, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Amount, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Comments, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", ItemNumber, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", LocationName, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", PartnerName, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", PointName, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Points, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", ProcessDate, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", ProcessingComment, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", ProcessingLog, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Trim(ProductName), "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Quantity, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", IntegrationStatus, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Status, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", SubStatus, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", TicketNumber, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", TransactionChannel, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", TransactionDate, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", TransactionSubType, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", TransactionType, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", VoucherNumber, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", VoucherQty, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", VoucherType, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Organization, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Estatus, "| "
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Bussiness_date, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Centroconsumo, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Cheque, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Transmitido, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Linea_Cheque, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Nombre_Mesero, "|"
Endsub

Sub Formatea_Datos_Acreditaciones
    Var Nombre_Mesero : A100 = ""
    Format Nombre_Mesero As @Tremp, " ", Trim(@TREMP_FNAME), " ", Trim(@TREMP_LNAME)
    
    Format Arma_Cadena_82 as "", MemberNumber, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", id, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", ALSELevelPrice, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", BranchDivision, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", ALSEPaymnetMode, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", ActivityDate, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", AdjustedListPrice, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Amount, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Comments, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", ItemNumber, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", LocationName, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", PartnerName, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", PointName, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Points, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", ProcessDate, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", ProcessingComment, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", ProcessingLog, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", ProductName, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Quantity, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", IntegrationStatus, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Status, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", SubStatus, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", TicketNumber, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", TransactionChannel, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", TransactionDate, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", TransactionSubType, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", TransactionType, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", VoucherNumber, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", VoucherQty, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", VoucherType, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Organization, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Estatus, "| "
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Bussiness_date, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Centroconsumo, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Cheque, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Transmitido, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Linea_Cheque, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Nombre_Mesero, "|"
Endsub

Sub Lee_Miembro_Soa
    Var FN001 : N6 = 0
    Var JJ    : N6 = 0
    
    //WaitForEnter "3"
    
    Call Borra_Array
    
    //24Dic2014 
    Var File1       : A100 = ""
    //Format File1   as FileMiembro_Soa
    //Call Asigna_Path_Archivos(File1)}
    //FOpen  FN001, File1, Read
    ////24Dic2014 
    
    ////24Dic2014 FOpen  FN001, FileMiembro_Soa, Read
    //IF FN001 = 0
    //   Respuesta = ""
    //   Cheque    = 0
    //   //WaitForEnter "3 Sali"
    //   Return
    //ENDIF
    Contador_Redenciones = 10
    //FRead  FN001, Cheque, Respuesta
    //FClose FN001
	
	//-------11 ABR 2016
	Cheque = Cheque_Miembro_Soa
	Format Respuesta as rspst_Miembro_Soa
	//-------
	
    ALL = ""
    If Mid(Respuesta,1,3) = "ALL"
       Split Respuesta, "|", NumeroMiembro, Nombre
       ALL = "ALL"
    Else
       Split Respuesta, "|", NumeroMiembro, Nombre, Activo, Nivel, Organizacion, Nivel_Id, Nivel_nombre, \
                          Contador_Campos,  PromocionId[]: PromocionNombre[]: PromocionUniqueKey[]: \
                                            PromocionDisplayName[]: PromocionStatus[], \
                          Contador_Voucher, VoucherCalStatus[]: VoucherProductId[]: VoucherProductName[]: \
                                            VoucherStatus[]: VoucherTransactionId[]: validDiscountAmount[]:  validDiscountType[]: minimumAmount[], \
                          BirthDate, FavoriteProduct, LastBeverage, LastFood, LifetimePoint1Value, LifetimePoint2Value, \
                          Status, AttributeValue
    EndIf
    
    //WaitForEnter "Aqui"
    //WaitForEnter NumeroMiembro
    //WaitForEnter Nombre
    //WaitForEnter Activo
    //WaitForEnter Nivel
    
    If Len(Trim(BirthDate)) > 0
       MesBirthDate_A = Mid(BirthDate, 1, 2)
       MesBirthDate   = MesBirthDate_A
    ENDIF
    If @MONTH <> MesBirthDate
       For JJ = 1 to Contador_Campos
       //waitforenter PromocionNombre[JJ]
           If Trim(PromocionNombre[JJ]) = "Beb Cumple"
              PromocionNombre[JJ] = ""
           Else
           EndIf
       EndFor
       For JJ = 1 to Contador_Voucher
       //waitforenter VoucherProductName[JJ]
           If Trim(VoucherProductName[JJ]) = "Beb Cumple"
              VoucherProductName[JJ] = ""
           Else
           EndIf
       EndFor
     Else
     	//InfoMessage "Cumpleanios1"
    EndIf
    //Split Respuesta, "|", NumeroMiembro, Nombre, Activo, Nivel, Organizacion, Nivel_Id, Nivel_nombre, \
    //                   Contador_Campos,  PromocionId[]: PromocionNombre[]: PromocionUniqueKey[]: \
    //                                     PromocionDisplayName[]: PromocionStatus[], \
    //                   Contador_Voucher, VoucherCalStatus[]: VoucherProductId[]: VoucherProductName[]: \
    //                                     VoucherStatus[]: VoucherTransactionId[], \
    //                   Contador_Redenciones, Redencion_Aplicada[]
Endsub
//New Crm 2014
Event Inq: 30   ///Pause and Re-charge
      Var Cheque_PR  : N6 = @CKNUM
      
      //Call Lee_Miembro_PR
      
      Call Lee_Miembro
      
      //WaitForEnter All
      //WaitForEnter NumeroMiembro
      
      If Len(Nombre) = 0 And Len(NumeroMiembro) = 0
         InfoMessage "PAUSA RECARGA", "NO ESTA PERMITIDA ESTA ACCION."
         ExitContinue
      EndIf 
      
      Call Lee_FileMiembro_Pausa_R_Cheque
      
      //WaitForEnter Cheque
      //WaitForEnter Respuesta
      
      If Cheque_PR <> 0 //Or Len(Trim(Respuesta)) <> 0
         InfoMessage "PAUSA RECARGA", "NO ESTA PERMITIDA ESTA ACCION"
         ExitContinue
      EndIf
      
      //23Dic2014
      Call Pide_Autorizacion_Pausa_Recarga
      //23Dic2014
      
      If @RVC = 1
         LoadKyBdMacro Key(9, 401)                    //Service Key
         LoadKyBdMacro Key(1, 327681)                 //Open check
         LoadKyBdMacro MakeKeys(@TREMP), @KEY_ENTER
         LoadKyBdMacro MakeKeys("0"), @KEY_ENTER
         LoadKyBdMacro Key (19,102)                   //SLU Tender Media
         //04Jun2013 Call LoadSIMInquire(8, 3)        //Recarga Tarjeta
         //Call Copiar_Archivo(FileMiembro, FileMiembro_Pausa_R) // Comentado el 14 ABR 2016
         
         //27Nov2014 Call Copiar_Archivo(FileLeyendaCRM, FileLeyendaCRM_Cheque)    //20Nov2014
         
         Cheque_PR = @CKNUM
         
         If Cheque_PR = 0
            Cheque_PR = 9999
         EndIf
         Call Graba_FileMiembro_Pausa_R_Cheque(Cheque_PR)              //20Nov2014
         
         Call LoadSIMInquire(8, 300)                  //Recarga Tarjeta  //04Jun2013 
         
      EndIf
EndEvent

Event Pickup_Check_NoUsado   ///Socio Starbucks Rewards  (SI / NO)  DRIVE THRU  Seguir Cuenta
      //WaitForEnter "Inicio 6.Pickup"  //OK
      //08Mar2012 If @RVC = 1
         Call Lee_Miembro_Servicio
         //WaitForEnter Flag_Copiar  //
       
       //04Jun2013
       //WaitForEnter "Verifica_Llama_Cheque_Continuar"  //OK
       Call Lee_Miembro_PR
       If ALL = "ALL" or Len(ALL) > 0
          //WaitForEnter ALL    //4812
          Call Borra_Miembro_PR  //09Dic2014
          Call Continuacion(7, 56)
          Call Borra_Miembro_PR
       EndIf
       //04Jun2013
       
         If Flag_Copiar = "T"
            //Call Copiar_Archivo(FileMiembroServicio,       FileMiembro) // Comentado el 14 ABR 2016
            //Call Copiar_Archivo(FileNivelServicio,         FileNivel) // Comentado el 14 ABR 2016
            //Call Copiar_Archivo(FileNivel_MiembroServicio, FileNivel_Miembro) // Comentado el 14 ABR 2016
            //Call Copiar_Archivo(FileConsultaSaldoServicio, FileConsultaSaldo) // Comentado el 14 ABR 2016
            Call Borra_Miembro_Servicio
            //Call Borra_Privilegios_Nivel_Servicio
            //Call Borra_Privilegios_Nivel_Miembro_Servicio
            //Call Borra_Consulta_Saldo_Servicio // Comentado el 14 ABR 2016
            Call Borra_MiembroCheckInfoServicio
         Else
         	  If @RVC <> 1
               Call Borra_Leyenda_CRM
               Call Socio_Starbucks_Rewards
               If   Flag_Socio_Starbucks_Rewards = "N"
	                  //Continua
               Else
      	            Call LoadSIMInquire(10, 31)   //Miembro
               EndIf
         	  EndIf
         EndIf
         LoadKyBdMacro Key (17,201)//SLU
      //08Mar2012 EndIf
EndEvent

Sub Borra_FileMiembro_Soa
    //Var    FN001   : N6 = 0
    
    ////24Dic2014 
    //Var File1       : A100 = ""
    //Format File1   as FileMiembro_Soa
    //Call Asigna_Path_Archivos(File1)}
    //FOpen  FN001, File1, Write
    ////24Dic2014 
    
    ////24Dic2014 FOpen  FN001, FileMiembro_Soa, Write
    //FWrite FN001, " "
    //FClose FN001
    //System "Del ", FileMiembro_Soa
	
	//-------11 ABR 2016
	Cheque_Miembro_Soa = 0
	rspst_Miembro_Soa = ""
	//-------
	
Endsub

Sub Asigna_Path_Archivos(Ref Archivo_)
    Var Archivo_Paso  : A200 = ""
    If     @WSTYPE = 3  ///WINDOWS CE
       Format Archivo_Paso as Drive_Pos_Win_Ce, Archivo_
    ElseIf @WSTYPE = 1  ///WINDOWS 32 o POSREADY
       Format Archivo_Paso as Drive_Pos_ready, Archivo_
    Else
       Format Archivo_Paso as Drive_Pos_Win_Ce, Archivo_ 	
    EndIf
    Format Archivo_ as Archivo_Paso
EndSub


//New 10Nov2014
Sub Consulta_Cheque_Crm
    Var File_No   : N6 = 0
    Call Version_Micros_Sybase
    Call Conecta_Base_Datos_Sybase
    Format SqlStr as "SELECT Marcado_Crm FROM custom.Crm_users "
    IF Version_Micros = 32 or Version_Micros = 41
       DLLCALL_CDECL hODBCDLL, sqlGetRecordSet(SqlStr)
       SqlStr1 = ""
       DLLCALL_CDECL hODBCDLL, sqlGetLastErrorString(ref SqlStr1)
       IF (SqlStr1 <> "")
           Call Muestra_Error(SqlStr1)
           Call UnloadDLL_SyBase
           ExitCancel
       ENDIF
       SqlStr1 = ""
       DLLCALL_CDECL hODBCDLL, sqlGetFirst(ref SqlStr1)
    ENDIF
    Marcado_Crm = ""
    If Len(SqlStr1) > 0
       If Len(Trim(SqlStr1)) > 0 Then
          Split SqlStr1, ";", Marcado_Crm
       EndIf
    EndIf
    Call UnloadDLL_SyBase
EndSub
Sub Inserta_Crm_users( Ref Numero_Tarjeta_, Ref Cheque_, Ref Saldo_, Ref Marcado_Crm_, Ref stars_, Ref Datos_)
    Var Temp1    : $100  = 0
    Var Datos2_  : A5000 = ""
    Var Saldo2_  : $10   = ""
    //Split Datos_, ",", Temp1, Datos2_
    
    Call Arma_Ticket_Buscar
    
    //17Dic2014 Format Ticket_Unico      as @WSID{01}, @CKNUM{04}, Mid(@Chk_Open_Time,10,2), Mid(@Chk_Open_Time,13,2)
    //17Dic2014 Format Fecha_a_Convertir as "20", Mid(@Chk_Open_Time,7,2), "-", Mid(@Chk_Open_Time,1,2), "-",Mid(@Chk_Open_Time,4,2)
    
    //17Dic2014 WaitForEnter Fecha_a_Convertir
    
    //17Dic2014 Call Consulta_Fecha_Juliana_Micros(Fecha_a_Convertir)
    
    //17Dic2014 WaitForEnter Fecha_Numerica
    
    //17Dic2014 Call Consulta_Business_Date_SyBase
    
    //17Dic2014 Format TicketNumber as Fecha_Numerica{04}, Tienda{05}, Ticket_Unico
    
    Call Muestra_datos_consulta_pantalla
    Saldo2_ = Saldo_Ws
    
    Call Version_Micros_Sybase
    Call Conecta_Base_Datos_Sybase
    Format SqlStr as "INSERT INTO custom.crm_Clientes (Id_Cheque, Cheque, Numero_Tarjeta, Numero_Tarjeta_O, Saldo, Marcado_Crm, Stars, Datos_1 ) VALUES ( "
    Format SqlStr as SqlStr, " '", TicketNumber,      "' ,"
    Format SqlStr as SqlStr, "  ", Cheque_,           "  ,"
    Format SqlStr as SqlStr, " '", Numero_Tarjeta_,   "' ,"
    Format SqlStr as SqlStr, " '", Numero_Tarjeta__O, "' ,"
    Format SqlStr as SqlStr, "  ", Saldo2_{010},      "  ,"
    Format SqlStr as SqlStr, " '", Marcado_Crm_,      "' ,"
    Format SqlStr as SqlStr, "  ", stars_,            "  ,"
    Format SqlStr as SqlStr, " '", Datos_,            "'  "
    Format SqlStr as SqlStr, " )"
    Call Graba_Archivo_datos(SqlStr)
    IF   Version_Micros = 41
       DLLCALL_CDECL hODBCDLL, sqlExecuteQuery(SqlStr)
       SqlStr1 = ""
       DLLCALL_CDECL hODBCDLL, sqlGetLastErrorString(ref SqlStr1)
       IF (SqlStr1 <> "")
           Call Graba_Archivo_datos(SqlStr1)
           InfoMessage "Error 19 C2: ", Mid(SqlStr1,1,30)
           //Call Muestra_Error(SqlStr1)
           //Call Muestra_Error(SqlStr)
       ENDIF
       SqlStr1 = ""
    ENDIF
    IF SqlStr1   = ""
    ELSE
       Split SqlStr1, ";", Temp1
    ENDIF
    Call UnloadDLL_SyBase
EndSub
Sub Lee_Miembro_ws
    Var FN001 : N6 = 0
    Var JJ    : N6 = 0
    
    Call Borra_Array
    
    //24Dic2014 
    Var File1       : A100 = ""
    //Format File1   as FileMiembro
    //Call Asigna_Path_Archivos(File1)}
    //FOpen  FN001, File1, Read
    ////24Dic2014 
    
    ////24Dic2014 FOpen  FN001, FileMiembro, Read
    //IF FN001 = 0
    //   Respuesta = ""
    //   Cheque    = 0
    //   //WaitForEnter "3 Sali"
    //   Return
    //ENDIF
    Contador_Redenciones = 10
    //FRead  FN001, Cheque, Respuesta
    //FClose FN001
    
    //-------11 ABR 2016
	Cheque = Cheque_Miembro
	Format Respuesta as rspst_Miembro
	//-------
	
	ALL = ""
    
    //17Dic2014
    Call Consulta_Cliente
    //17Dic2014
    
    If Mid(Respuesta,1,3) = "ALL"
        Split Respuesta, "|", NumeroMiembro, Nombre
        ALL = "ALL"
     Else
        Split Respuesta, "|", NumeroMiembro, Nombre, Activo, Nivel, Organizacion, Nivel_Id, Nivel_nombre, \
                           Contador_Campos,  PromocionId[]: PromocionNombre[]: PromocionUniqueKey[]: \
                                             PromocionDisplayName[]: PromocionStatus[], \
                           Contador_Voucher, VoucherCalStatus[]: VoucherProductId[]: VoucherProductName[]: \
                                             VoucherStatus[]: VoucherTransactionId[]: validDiscountAmount[]:  validDiscountType[]: minimumAmount[], \
                           BirthDate, FavoriteProduct, LastBeverage, LastFood, LifetimePoint1Value, LifetimePoint2Value, \
                           Status, AttributeValue, ErrorCode, pointAmount
     EndIf
    
    //WaitForEnter "Read"
    //WaitForEnter Activo
    //WaitForEnter Status
    
    If Len(Activo) = 0
       Activo = Status
    EndIf
    
    //10Nov2014
    Var Marcado_Crm  : A1 = "1"  //1=Marcado como CRM
    //10Nov2014
    
    //17Dic2014 WaitForEnter Saldo
    //17Dic2014 WaitForEnter Mid(Datos_1, 1,20)
    //17Dic2014 WaitForEnter LifetimePoint1Value
    Var Cheque_crm  : N6 = @CKNUM
    Call Inserta_Crm_users( NumeroMiembro, Cheque_crm, Saldo_Ws, Marcado_Crm, LifetimePoint2Value, Respuesta )
    //17Dic2014
    
    
    
    If Len(Trim(BirthDate)) > 0
       MesBirthDate_A = Mid(BirthDate, 1, 2)
       MesBirthDate   = MesBirthDate_A
    EndIf
    
    If @MONTH <> MesBirthDate
       For JJ = 1 to Contador_Campos
       //waitforenter PromocionNombre[JJ]
           If Trim(PromocionNombre[JJ]) = "Beb Cumple"
              PromocionNombre[JJ] = ""
           Else
           EndIf
       EndFor
       For JJ = 1 to Contador_Voucher
       //waitforenter VoucherProductName[JJ]
           If Trim(VoucherProductName[JJ]) = "Beb Cumple"
              VoucherProductName[JJ] = ""
           Else
           EndIf
       EndFor
     Else
     	//InfoMessage "Cumpleanios1"
    EndIf
Endsub
Sub Borra_Miembro_O
    //Var    FN001   : N6 = 0
    
    ////24Dic2014 
    //Var File1       : A100 = ""
    //Format File1   as FileMiembro_O
    //Call Asigna_Path_Archivos(File1)}
    //FOpen  FN001, File1, Write
    ////24Dic2014 
    
    ////24Dic2014 FOpen  FN001, FileMiembro_O, Write
    //FWrite FN001, " "
    //FClose FN001
    //System "Del Miembro_O.TxT"
    
    //FileDelete   = File1
    //Call  Delete_File(path_DeleteFile, FileDelete, status_DeleteFile)
	
	//------- 11 ABR 2016
	Miembro_O_Archivo = ""
    //-------
		
Endsub
Sub Lee_Miembro_O //Lee el archivo Miembro_O.txt
    //Var FN001 : N6 = 0
    
    //24Dic2014 
    //Var File1       : A100 = ""
    //Format File1   as FileMiembro_O
    
	//Call Asigna_Path_Archivos(File1)}
    //FOpen  FN001, File1, Read
    //24Dic2014 
    
    //24Dic2014 FOpen  FN001, FileMiembro_O, Read
    //IF FN001 = 0
    //   Nombre_Miembro_O = ""
    //   Return
    //ENDIF
    
	//FClose FN001

	// ----------------- 12 NOV 2015
	//infomessage "Tarjeta_WOW dice:", No_Tarjeta_WOW
	//infomessage "Nombre_Miembro_O dice:", Nombre_Miembro_O
	
	//Format Nombre_Miembro_O as No_Tarjeta_WOW 
	
	//infomessage "Nombre_Miembro_O ahora dice:", Nombre_Miembro_O
	// -----------------
	
    //------- 15 ABR 2016
	Format Nombre_Miembro_O as Miembro_O_Archivo
	//-------
	
Endsub
Sub Graba_Miembro_O(Ref Miembro_O)
    //Var    FN001   : N6 = 0
    
    ////24Dic2014 
    //Var File1       : A100 = ""
    //Format File1   as FileMiembro_O
    //Call Asigna_Path_Archivos(File1)}
    //FOpen  FN001, File1, Write
    ////24Dic2014 
    
    ////24Dic2014 FOpen  FN001, FileMiembro_O, Write
    //FWrite FN001, Miembro_O
    //FClose FN001
	
	// =============================== 10 diciembre 2015
	//Format Nombre_Miembro_O as Miembro_O
	// =============================== 10 diciembre 2015
	
	//------- 15 ABR 2016
	Format Miembro_O_Archivo as Miembro_O
	//-------
	
Endsub
//New 10Nov2014

//New 11Nov2014
Event MI
      Call Lee_Miembro_PR
      If Len(Trim(NumeroMiembro_PR)) > 0
         LoadKyBdMacro Key( 1, 458753 ), Key( 1, 458753 )
         InfoMessage "ERROR", "NO PUEDE MARCAR ARTICULOS"
      EndIf
EndEvent
//New 11Nov2014

//New 17Nov2014
Sub Delete_File(Ref path_DeleteFile_, Ref fileDelete_, Ref status_DeleteFile_)
      Var Valor33           : A100  = ""
      Var Valor33_Error     : A100  = ""
      
      status_DeleteFile_ = ""
      DLLFree hODBCDLL_File

      If @WSTYPE = 1  //PosReady
         Format Valor33 as Drive_Pos_ready, "DFile.DLL"
         Prompt "Pos Ready Read DLL"
      Else
         Format Valor33 as Drive_Pos_Win_Ce, "DFile.DLL"
         Prompt "WinCe Read DLL"
      EndIf
      
      DLLLoad hODBCDLL_File, Valor33
      
      If hODBCDLL_File = 0 
         Format Valor33_Error As "No se Puede Cargar DLL.", Valor33
         status_DeleteFile_ = "Error"
         //04Dic2014 InfoMessage Valor33_Error
         Return
      EndIf

      DLLCALL_CDECL hODBCDLL_File, BorraArchivoWin32(ref path_DeleteFile_, Ref fileDelete_, Ref status_DeleteFile_)
      status_DeleteFile_ = status_DeleteFile_
      DLLFree hODBCDLL_File
      hODBCDLL_File = 0
EndSub

Sub Rutina_Borra_Archivos_Temporales_Dll
    If @WSTYPE = 1  //PosReady
       Format path_DeleteFile as Path_Pos_ready
    Else            //WinCe
       Format path_DeleteFile as Path_Pos_Win_Ce
    EndIf
    
      
    If Borrar = "S"
    
       FileDelete = FileMiembro
       
       //waitforenter "FileMiembro"
       //waitforenter path_DeleteFile
       //waitforenter FileDelete
       
       Call Delete_File(path_DeleteFile, FileDelete, status_DeleteFile)
       
       //waitforenter  status_DeleteFile
       
       FileDelete = FileMiembro_O
       Call Delete_File(path_DeleteFile, FileDelete, status_DeleteFile)
       FileDelete = FileConsultaSaldo
       Call Delete_File(path_DeleteFile, FileDelete, status_DeleteFile)
       FileDelete = FileNivel
       Call Delete_File(path_DeleteFile, FileDelete, status_DeleteFile)
       FileDelete = FileNivel_Miembro
       Call Delete_File(path_DeleteFile, FileDelete, status_DeleteFile)
    EndIf
    
    FileDelete = FileLineaRedChq
    Call Delete_File(path_DeleteFile, FileDelete, status_DeleteFile)
    FileDelete = TicketCancelar
    Call Delete_File(path_DeleteFile, FileDelete, status_DeleteFile)
    FileDelete = FileTemporal
    Call Delete_File(path_DeleteFile, FileDelete, status_DeleteFile)
    FileDelete = FileMiembroScreen
    Call Delete_File(path_DeleteFile, FileDelete, status_DeleteFile)
    FileDelete = FileOff_Line_CRM
    Call Delete_File(path_DeleteFile, FileDelete, status_DeleteFile)
    FileDelete = FileMiembroSolo
    Call Delete_File(path_DeleteFile, FileDelete, status_DeleteFile)
EndSub

Event Inq: 3131  //TEST BORRA ARCHIVO
    
    WaitForEnter "test"
    FileDelete = FileMiembro
    WaitForEnter path_DeleteFile
    WaitForEnter FileDelete
    
    Call Delete_File(path_DeleteFile, FileDelete, status_DeleteFile)
    WaitForEnter status_DeleteFile
EndEvent
//New 17Nov2014

//20Nov2014
Sub Graba_FileMiembro_Pausa_R_Cheque(Ref Cheque_)
    
	//Var    FN001   : N6 = 0
    
    ////24Dic2014 
    //Var File1       : A100 = ""
    //Format File1   as FileMiembro_Pausa_R_Cheque
    //Call Asigna_Path_Archivos(File1)}
    //FOpen  FN001, File1, Write
    ////24Dic2014 
    
    ////24Dic2014 FOpen  FN001, FileMiembro_Pausa_R_Cheque, Write
    //FWrite FN001, Cheque_
    //FClose FN001
	
	//------- 11 ABR 2016
	Miembro_Pausa_R_Cheque = Cheque_
	//-------
	
Endsub

Sub Lee_FileMiembro_Pausa_R_Cheque
    //Var FN001 : N6 = 0
    
    ////24Dic2014 
    //Var File1       : A100 = ""
    //Format File1   as FileMiembro_Pausa_R_Cheque
    //Call Asigna_Path_Archivos(File1)}
    //FOpen  FN001, File1, Read
    //24Dic2014 
    
    //24Dic2014 FOpen  FN001, FileMiembro_Pausa_R_Cheque, Read
    //IF FN001 = 0
    //   Cheque_PR = 0
    //   Return
    //ENDIF
    //FRead  FN001, Cheque_PR
    //FClose FN001
	
	Cheque_PR = Miembro_Pausa_R_Cheque
	
Endsub

Sub Borra_FileMiembro_Pausa_R_Cheque
    //Var    FN001   : N6 = 0
    
    ////24Dic2014 
    //Var File1       : A100 = ""
    //Format File1   as FileMiembro_Pausa_R_Cheque
    //Call Asigna_Path_Archivos(File1)}
    //FOpen  FN001, File1, Write
    ////24Dic2014 
    
    ////24Dic2014 FOpen  FN001, FileMiembro_Pausa_R_Cheque, Write
    //FWrite FN001, "00"
    //FClose FN001
    //System "Del ", FileMiembro_Pausa_R_Cheque
    //FileDelete   = FileMiembro_Pausa_R_Cheque
    //Call  Delete_File(path_DeleteFile, FileDelete, status_DeleteFile)
	
	Miembro_Pausa_R_Cheque = 00
	
    //System "Del ", FileMiembro_Pausa_R
    //FileDelete   = FileMiembro_Pausa_R
    //Call  Delete_File(path_DeleteFile, FileDelete, status_DeleteFile)
	
	rspst_Miembro_PR = ""

    Cheque_Miembro_PR = 0
Endsub

//Sub Lee_Leyenda_CRM_Cheque // Comentada el 14 de ABR 2016, no hay sentencia que la invoque en todo el programa
//    Var FN001      : N6  = 0
//    Var Linea_R    : A45 = ""
//    Var Contador_R : N3  = 0
//    ClearArray Leyenda_CRM_Cheque
    
//    //24Dic2014 
//    Var File1       : A100 = ""
//    Format File1   as FileLeyendaCRM_Cheque
//    Call Asigna_Path_Archivos(File1)}
//    FOpen  FN001, File1, Read
//    //24Dic2014 
    
//    //24Dic2014 FOpen FN001, FileLeyendaCRM_Cheque, Read
//    If FN001 = 0
//       Return
//    EndIf
//    While Not Feof(FN001)
//          FRead  FN001, Linea_R
//          //WaitForEnter Linea_R
//          If Len(Trim(Linea_R)) > 0
//             Contador_R = ( Contador_R + 1 )
//             If Contador_R < 15
//                Leyenda_CRM_Cheque[Contador_R] = Linea_R
//             EndIf
//          EndIf
//    EndWhile
//    FClose FN001
	
	
//    Lineas_Leyenda_Crm_Cheque = Contador_R
//    //WaitForEnter Lineas_Leyenda_Crm
//Endsub


Sub Write_Leyenda_CRM_Cheque(Ref Datos_) // Modificado el 14 ABR 2016
//    Var FN004  : N6 = 0
    Var III    : N3 = 0
    
//    //24Dic2014 
//    Var File1       : A100 = ""
//    Format File1   as FileLeyendaCRM_Cheque
//    Call Asigna_Path_Archivos(File1)}
//    FOpen  FN004, File1, Append
//    //24Dic2014 
    
//    //24Dic2014 FOpen  FN004, FileLeyendaCRM_Cheque, Append
//    FWrite FN004, Datos_
//   FClose FN004
	
	for III = 1 to 15 
	    if Leyenda_CRM_Cheque_Arreglo[III] <> ""
		//No hacer nada
		elseif Leyenda_CRM_Cheque_Arreglo[III]	= ""
		    format Leyenda_CRM_Cheque_Arreglo[III] as Datos_
			break
		endif
	
	endfor
	
	
EndSub

Sub Borra_FileLeyendaCRM_Cheque // Modificado el 14 ABR 2016
//    Var    FN001   : N6 = 0
    
//    //24Dic2014 
//    Var File1       : A100 = ""
//    Format File1   as FileLeyendaCRM_Cheque
//    Call Asigna_Path_Archivos(File1)}
//    FOpen  FN001, File1, Write
//    //24Dic2014 
    
//    //24Dic2014 FOpen  FN001, FileLeyendaCRM_Cheque, Write
//    FWrite FN001, " "
//    FClose FN001
//    System "Del ", FileLeyendaCRM_Cheque
//    FileDelete   = FileLeyendaCRM_Cheque
//    Call Delete_File(path_DeleteFile, FileDelete, status_DeleteFile)
	
	ClearArray Leyenda_CRM_Cheque_Arreglo
	
Endsub
//20Nov2014

//17Dic2014
Sub Consulta_Cliente
    Var File_No   : N6 = 0
    
    Call Arma_Ticket_Buscar
    
    Call Version_Micros_Sybase
    Call Conecta_Base_Datos_Sybase
    Format SqlStr as "SELECT Id_Cheque, Numero_Tarjeta, Numero_Tarjeta_O, Saldo, Marcado_Crm, Datos_1  FROM custom.crm_Clientes WHERE Id_Cheque = '", TicketNumber, "' "
    IF Version_Micros = 32 or Version_Micros = 41
       DLLCALL_CDECL hODBCDLL, sqlGetRecordSet(SqlStr)
       SqlStr1 = ""
       DLLCALL_CDECL hODBCDLL, sqlGetLastErrorString(ref SqlStr1)
       IF (SqlStr1 <> "")
           Call Muestra_Error(SqlStr1)
           Call UnloadDLL_SyBase
           ExitCancel
       ENDIF
       SqlStr1 = ""
       DLLCALL_CDECL hODBCDLL, sqlGetFirst(ref SqlStr1)
    ElseIF Version_Micros = 31
       DLLCall hODBCDLL, sqlGetRecordSet(SqlStr)
       SqlStr1 = ""
       DLLCall hODBCDLL, sqlGetLastErrorString(ref SqlStr1)
       IF SqlStr1 <> ""
          Call Muestra_Error(SqlStr1)
          Call UnloadDLL_SyBase
          ExitCancel
       ENDIF
       SqlStr1 = ""
       DLLCall hODBCDLL, sqlGetFirst(ref SqlStr1)
    ENDIF
    IF SqlStr1 = ""
       Id_Cheque        = ""
       Numero_Tarjeta   = ""
       Numero_Tarjeta_O = ""
       Saldo            = ""
       Marcado_Crm      = ""
       Datos_1          = ""
    Else
       IF Len(Trim(SqlStr1)) > 0 Then
          Split SqlStr1, ";", Id_Cheque, Numero_Tarjeta, Numero_Tarjeta_O, Saldo, Marcado_Crm, Datos_1
       ENDIF
    ENDIF
    Call UnloadDLL_SyBase
    
    If Len(Trim(Datos_1)) > 0
       Respuesta = Datos_1
    EndIf
    
    If Len(Trim(Respuesta)) > 3
       If Mid(Respuesta,1,3) = "ALL"
          Split Respuesta, "|", NumeroMiembro, Nombre
          ALL = "ALL"
       Else
          Split Respuesta, "|", NumeroMiembro, Nombre, Activo, Nivel, Organizacion, Nivel_Id, Nivel_nombre, \
                                Contador_Campos,  PromocionId[]: PromocionNombre[]: PromocionUniqueKey[]: \
                                PromocionDisplayName[]: PromocionStatus[], \
                                Contador_Voucher, VoucherCalStatus[]: VoucherProductId[]: VoucherProductName[]: \
                                VoucherStatus[]: VoucherTransactionId[]: validDiscountAmount[]:  validDiscountType[]: minimumAmount[], \
                                BirthDate, FavoriteProduct, LastBeverage, LastFood, LifetimePoint1Value, LifetimePoint2Value, \
                                Status, AttributeValue
       EndIf
    EndIf
    
EndSub
Sub Arma_Ticket_Buscar
    Format Ticket_Unico      as @WSID{01}, @CKNUM{04}, Mid(@Chk_Open_Time,10,2), Mid(@Chk_Open_Time,13,2)
    Format Fecha_a_Convertir as "20", Mid(@Chk_Open_Time,7,2), "-", Mid(@Chk_Open_Time,1,2), "-",Mid(@Chk_Open_Time,4,2)
    
    //WaitForEnter Fecha_a_Convertir
    
    Call Consulta_Fecha_Juliana_Micros(Fecha_a_Convertir)
    
    //WaitForEnter Fecha_Numerica
    
    Call Consulta_Business_Date_SyBase
    
    Format TicketNumber as Fecha_Numerica{04}, Tienda{05}, Ticket_Unico

EndSub

Sub Actualiza_Stars(Ref stars_)
    Var Campo_01   : A20 = ""
    Var Campo_02   : A20 = ""
    Var Campo_03   : A20 = ""
    Var Vacio      : A20 = ""
    Var File_NO    : N6  = 0

     
    Call Arma_Ticket_Buscar

    Call Version_Micros_Sybase
    Call Conecta_Base_Datos_Sybase
    
    
    Format SqlStr as "UPDATE custom.crm_Clientes Set Stars = ", stars_, " WHERE Id_Cheque = '", TicketNumber, "' "
    
    IF Version_Micros = 41
       DLLCALL_CDECL hODBCDLL, sqlExecuteQuery(SqlStr)
       SqlStr1 = ""
       DLLCALL_CDECL hODBCDLL, sqlGetLastErrorString(ref SqlStr1)
       IF (SqlStr1 <> "")
           Call UnloadDLL_SyBase
           SqlStr1 = ""
       ENDIF
    ENDIF
    Call UnloadDLL_SyBase
EndSub
//17Dic2014

//23Dic2014
Sub Pide_Autorizacion_Pausa_Recarga
Var Numero_Empleado          : N40    = 0
Var Numero_EmpleadoA         : A40    = ""
    Window 1,13, "AUTHORIZATION"
    TouchScreen 3052
      ForEver
         DisplayMSInput 0, 0, Numero_EmpleadoA{-m2,1,1,*}, "AUTHORIZATION IS REQUIRED FOR:"
         WindowInput
         WindowClear
         If @MAGSTATUS = "N"
            //WaitForClear "-NO- PARA CONTINUAR, DESLIZAR TARJETA"
            Numero_EmpleadoA = Numero_EmpleadoA
         Else
         	  Numero_EmpleadoA = mid(Numero_EmpleadoA,4,10)
         EndIf
            If Len(Numero_EmpleadoA) <> 0
               Numero_Empleado = Numero_EmpleadoA
               Call Consulta_Privilegios_Empleados_Pausa_Recarga(Numero_Empleado)
               If emp_bo_class_seq =  1 or emp_bo_class_seq =  8 or emp_bo_class_seq =  12  or emp_bo_class_seq =  14 or emp_bo_class_seq =  4 or emp_bo_class_seq = 13 Or emp_bo_class_seq = 6 Or emp_bo_class_seq = 9 Or emp_bo_class_seq = 2
                  Break
               Else
                  ExitWithError "NO TIENE PRIVILEGIOS"
               EndIf
            EndIf
         //EndIf
      EndFor
      WindowClear
      WindowClose
EndSub
Sub Consulta_Privilegios_Empleados_Pausa_Recarga(Ref Busca_emp)
    Var File_No   : N6 = 0
    Call Version_Micros_Sybase
    Call Conecta_Base_Datos_Sybase
    Format SqlStr as "SELECT emp_bo_class_seq FROM micros.emp_def Where id = '", Busca_emp, "'"
    IF Version_Micros = 32 or Version_Micros = 41
       DLLCALL_CDECL hODBCDLL, sqlGetRecordSet(SqlStr)
       SqlStr1 = ""
       DLLCALL_CDECL hODBCDLL, sqlGetLastErrorString(ref SqlStr1)
       IF (SqlStr1 <> "")
           Call Muestra_Error(SqlStr1)
           Call UnloadDLL_SyBase
           ExitCancel
       ENDIF
       SqlStr1 = ""
       DLLCALL_CDECL hODBCDLL, sqlGetFirst(ref SqlStr1)
    ElseIF Version_Micros = 31
       DLLCall hODBCDLL, sqlGetRecordSet(SqlStr)
       SqlStr1 = ""
       DLLCall hODBCDLL, sqlGetLastErrorString(ref SqlStr1)
       IF SqlStr1 <> ""
          Call Muestra_Error(SqlStr1)
          Call UnloadDLL_SyBase
          ExitCancel
       ENDIF
       SqlStr1 = ""
       DLLCall hODBCDLL, sqlGetFirst(ref SqlStr1)
    ENDIF
    IF SqlStr1 = ""
       emp_bo_class_seq = 0
    Else
       IF Len(Trim(SqlStr1)) > 0 Then
          Split SqlStr1, ";", emp_bo_class_seq
       ENDIF
    ENDIF
    Call UnloadDLL_SyBase
EndSub
//23Dic2014

//08Ene2015
Event Inq: 71   //Inicio de Tender Media
  
		Call Lee_Miembro
		
		If Len(Trim(NumeroMiembro)) = 0
         ExitWithError "NO EXISTE MIEMBRO DE CRM PARA ESTE CHEQUE"
		EndIf
		//Call Verifica_Descuento_Aplicado_Cheque_Wow
		Call Verifica_Aportaciones_Aplicado_Cheque
		If Existe_Aportacion = "T" And Existe_Menu_Item = "T"
			InfoMessage "Aportaciones", "NO SE PERMITE COMBINAR APORTACIONES-PUNTOS WOW"
			ExitCancel
		EndIf
	 
      Var TotalCheque        : $10   = 0  //22Ene2015 @USERENTRY
      Var TotalCheque_N      : N10   = 0
      Var TotalCheque_A      : A12   = ""
      Var Decimales_A        : A3    = ""
      Var Decimales          : $5    = ""
      Var Inicio_A           : N3    = 0
      Var Parciales          : N1    = 0
      Var Descuento_A        : $10   = @DSC
      Var Decimales2         : $5    = ""  //08Abr2015
	  
      TouchScreen 60 //Numero_TouchscreenNueva //tEST 10/08/2015
      Input TotalCheque, "Digite Monto a Pagar en Pesos"
      //TotalCheque = ( TotalCheque * 100 )
       
		if entra_puntos_primera_vez = 0 //tEST 10/08/2015
			Call Busca_Points_Cheque //tEST 10/08/2015
			puntosActuales = Points_Cheque //tEST 10/08/2015
			entra_puntos_primera_vez = 1 //tEST 10/08/2015
		endif //tEST 10/08/2015
		
		 
	  
    If TotalCheque = 0//tEST 10/08/2015
        //ExitWithError "Ingrese monto valido"//tEST 10/08/2015
        TotalCheque = @TTLDUE//tEST 10/08/2015
		
       
        //Call Busca_Points_Cheque
      
        If puntosActuales < TotalCheque//tEST 10/08/2015
		  
          TotalCheque = puntosActuales//tEST 10/08/2015
		  puntosWOW_Global= puntosWOW_Global + puntosActuales 
        Else
			puntosWOW_Global= puntosWOW_Global + TotalCheque
		EndIf //tEST 10/08/2015
		
    Else      
      	 
      	 //WaitForEnter Points_Cheque
      	 //WaitForEnter TotalCheque
		 If puntosActuales < TotalCheque  //tEST 10/08/2015
            //15Jul2015
            InfoMessage "Points", "SALDO INSUFICIENTE"
            ExitCancel
            //TotalCheque = Points_Cheque
            //15Jul2015
         Else
			 puntosActuales = puntosActuales - TotalCheque //tEST 10/08/2015 
			puntosWOW_Global = puntosWOW_Global + TotalCheque//tEST 10/08/2015
			// infoMessage puntosWOW_Global
         EndIf
         //WaitForEnter TotalCheque
         //15Jul2015
         
    EndIf
      
      //22Ene2015
      If TotalCheque > @TTLDUE
         ExitWithError "MONTO NO PERMITIDO, MAYOR A LA CUENTA"
      EndIf
      //22Ene2015
		
      //22Ene2015 WaitForEnter TotalCheque
      
      If Descuento_A <> 0
         Descuento_A = ( Descuento_A * -1 )
      EndIf
      
      If @TTLDUE <= TotalCheque
         Parciales = 0
      Else
      	 Parciales = 1
      EndIf
      
      Call Lee_Configuracion_CRM
      
      //08Feb2015
      Call Busca_Producto_Promocional_Cheques
      //08Feb2015
      
    //  Call Verifica_Descuento_Aplicado_Cheque
    //  If Descuento_Aplicado_Cheque = "T"
    //     Call Verifica_Descuento_WOW
    //     If Flag_Descuento_WOW <> 1
    //        ExitWithError "NO SE PERMITE EL USO DE PUNTOS CON DESCUENTOS"
    //     EndIf
    //  EndIf 


      //22Ene2015 WaitForEnter Parciales
      
      
      
      //WaitForEnter Tender_Crm
      
      LoadKyBdMacro MakeKeys(TotalCheque), Key(9, Tender_Crm)
      //18Ene2015 LoadKyBdMacro Key(9, 27)
EndEvent

Event OLDTMED: *   //TIENE ACREDITACIONES
      Var MemberNumber       : A100 = ""
      Var ALSELevelPrice     : A50  = ""
      Var BranchDivision     : A50  = ""
      Var ALSEPaymnetMode    : A50  = ""
      Var ActivityDate       : A50  = ""
      Var AdjustedListPrice  : A50  = ""
      Var Amount             : A50  = ""	
      Var Comments           : A50  = ""
      Var ItemNumber         : A50  = ""
      Var LocationName       : A50  = ""
      Var PartnerName        : A50  = ""
      Var PointName          : A50  = ""
      Var Points             : A50  = ""	
      Var ProcessDate        : A50  = ""
      Var ProcessingComment  : A50  = ""
      Var ProcessingLog      : A50  = ""
      Var ProductName        : A50  = ""
      Var Quantity           : A50  = ""
      Var TicketNumber       : A50  = ""
      Var TransactionChannel : A50  = ""
      Var TransactionDate    : A50  = ""
      Var TransactionSubType : A50  = ""
      Var TransactionType    : A50  = ""
      Var VoucherNumber      : A50  = ""
      Var VoucherQty         : A50  = "1"
      Var VoucherType        : A50  = ""
      Var Organization       : A50  = ""

      Var Bussiness_date     : A50  = ""
      Var Centroconsumo      : A50  = ""
      Var Cheque             : A50  = ""
      Var Transmitido        : A50  = ""
      Var Arma_Cadena        : A2000 = ""
      Var Arma_Cadena_22     : A2000 = ""
      Var Status_Respuesta   : A1   = ""
      
      Var TotalCheque        : $10   = 0 //18Ene2015 @USERENTRY
      
      TouchScreen Numero_TouchscreenNueva
      Input TotalCheque, "Digite Monto a Pagar en Puntos"
      If TotalCheque = 0
         //18Ene2015 ExitCancel
      EndIf
      
      If TotalCheque = 0
         TotalCheque = @TTLDUE
      EndIf
      

      Call Version_ISL
      
      Call Lee_Configuracion_CRM
      Call Verifica_Descuento_Aplicado
      
      //WaitForEnter "TMED"
      //WaitForEnter Total_Fpago_Cheque
      //WaitForEnter TotalCheque
      
      If Total_Fpago_Cheque = 0
         Total_Fpago_Cheque = TotalCheque
      EndIf
    
      	 If Total_Fpago_Cheque > 0
            Call Ejecuta_Acreditaciones_Main_Wow
         EndIf

      Borrar = "S"
      Call Verifica_Llama_Cheque_Continuar
      Call Rutina_Borra_Archivos_Temporales  //24Dic2014 
      Call Rutina_Borra_Archivos_Temporales_Dll
      Call Borra_FileMiembro_Soa
EndEvent

Sub Ejecuta_Acreditaciones_Main_Wow
    Var CC  : N2 = 0
    
    Call Lee_Miembro
    //Call Lee_Miembro_Solo // Comentado el 13 ABR 2016
    Call Lee_Miembro_Soa

    If Len(Trim(NumeroMiembro)) = 0
       If Len(Trim(NumeroMiembroSolo)) = 0
          //WaitForenter "Numero miembro cero"
          Call Verifica_Llama_Cheque_Continuar
          If Borrar = "S"
             Call Borra_Miembro
          EndIf
          Call Borra_Linea_Item_Redimida
          ExitContinue
       Else
        	NumeroMiembro = NumeroMiembroSolo
       EndIf
    EndIf

    Call Consulta_Business_Date_SyBase
    Call Borra_Miembro

    Format Ticket_Unico      as @WSID{01}, @CKNUM{04}, Mid(@Chk_Open_Time,10,2), Mid(@Chk_Open_Time,13,2)
    Format Fecha_a_Convertir as "20", Mid(@Chk_Open_Time,7,2), "-", Mid(@Chk_Open_Time,1,2), "-", Mid(@Chk_Open_Time,4,2)

    Call Consulta_Fecha_Juliana_Micros(Fecha_a_Convertir)

    Call Main_Busca_Articulos_Enviar_Acreditaciones_Wow
    Call Borra_Linea_Item_Redimida

EndSub
Sub Main_Busca_Articulos_Enviar_Acreditaciones_Wow
    Var SNivel_R        : A6  = ""
    Var Price_R         : $10 = 0
    Var Number_R        : A10 = 0
    Var Nombre_R        : A20 = ""
    Var Unidades_R      : N4  = 0
    Var Cheque_R        : N4  = 0
    Var I               : N4  = 0
    Var Cons_Item       : N4  = 0
    Var Primera_Cero    : A1  = "F"
    Var Total_Ticket    : $10 = @TTLDUE
    Var Contador_OP     : N2  = 0
    Var Total_Paso      : $10 = 0
    Lineas_Acreditas_Contador = 0
    
    //WaitForEnter "Acreditaciones TMed"
    
    SNivel_R   = "NA"
    Number_R   = @TMDNUM
    Unidades_R = 1
    Cheque_R   = @CKNUM
    Price_R    = @TNDTTL
    Linea_Cheque = I
    
    Format TicketNumber as Fecha_Numerica{04}, Tienda{05}, Ticket_Unico
           
    Call Guarda_Campos_Acreditaciones_Cero_Tmed_Wow(SNivel_R , Price_R, Cons_Item, Unidades_R, Cheque_R, Number_R, Total_Fpago_Cheque )
    Call Arma_Registro_Acreditaciones_Redenciones_Tmed_Wow
    Call Manda_Redencion_Tmed_Wow
    Call Valida_Redencion_Tmed_Wow
 
	//WaitForenter Salir_ACR
	//waitforenter FP_Sbux_Aplicado_Off_Line

	If Salir_ACR = "T"
	   Return
	EndIf

		
	//FP_Sbux_Aplicado_Off_Line = "T"


    If Trim(FP_Sbux_Aplicado_Off_Line) = "T"
       Status_Respuesta = "F"
       Call Read_Off_Line_CRM
       If Len(Trim(TicketNumber_Off)) = 0
          If Len(Trim(TicketNumber))  = 0
             Return
          EndIf
       EndIf
       If Len(Trim(TicketNumber)) = 0
          Return
       EndIf
       Call Manda_Ejecuta_Acreditaciones_OffLine_WS_JAR
    EndIf
EndSub
Sub Guarda_Campos_Acreditaciones_Cero_Tmed_Wow(Ref SNivel_ , Ref Price_, Ref Number_, Ref Unidades_, Ref Cheque_, Ref Number_R_, Ref Total_Ticket_ )
    MemberNumber         = NumeroMiembro
    ID                   = "NUEVO"
    ALSELevelPrice       = SNivel_
    BranchDivision       = "WOW"
    ALSEPaymnetMode      = "Puntos WOW"
    ActivityDate         = Business_Date
    AdjustedListPrice    = "0"
    Amount               = Total_Ticket_
    Comments             = ""
    ItemNumber           = "0"
    LocationName         = Tienda
    PartnerName          = PartnerName
    PointName            = ""
    Points               = "0"
    ProcessDate          = Business_Date
    ProcessingComment    = ""
    ProcessingLog        = ""
    ProductName          = "WOW Ticket"
    Quantity             = "0"
    IntegrationStatus    = ""
    Status               = "Acceptable"	
    SubStatus            = ""
    TicketNumber         = TicketNumber
    TransactionChannel   = "Store"
    TransactionDate      = Business_Date
    TransactionSubType   = "Product"
    TransactionType      = "Redemption"
    VoucherNumber        = ""
    VoucherQty           = "1"
    VoucherType          = ""
    Organization         = "BKCASUAL"
    Estatus              = "PEN"
    Bussiness_date       = Business_Date
    Centroconsumo        = @RVC
    Cheque               = Cheque_
    Transmitido          = "F"
    Linea_Cheque         = 1
EndSub
Sub Arma_Registro_Acreditaciones_Redenciones_Tmed_Wow
    Format Id_Ws as ID
    Format Arma_Cadena_2, Separador As MemberNumber, id, ALSELevelPrice, BranchDivision, ALSEPaymnetMode, ActivityDate, AdjustedListPrice, Amount, Comments, ItemNumber, LocationName, PartnerName, PointName, Points, ProcessDate, ProcessingComment, ProcessingLog, ProductName, Quantity, IntegrationStatus, Status, SubStatus, TicketNumber, TransactionChannel, TransactionDate, TransactionSubType, TransactionType, VoucherNumber, VoucherQty, VoucherType, Organization, Estatus, Bussiness_date, Centroconsumo, Cheque, Transmitido, Linea_Cheque
EndSub

Sub Manda_Redencion_Tmed_Wow
    Split MemberNumber, ":", Miembro_2[1], Miembro_2[2]
    Call Lee_Miembro_O
    If Len(Trim(Miembro_2[2])) = 0
       Format Manda, "/" as "TMED_ACR", Trim(TicketNumber), Trim(MemberNumber), Nombre_Miembro_O, Arma_Cadena_2
    Else
       Format Manda, "/" as "TMED_ACR", Trim(TicketNumber), Trim(Miembro_2[2]), Nombre_Miembro_O, Arma_Cadena_2
    EndIf
    If Len(Trim(TicketNumber)) = 0 And Len(Trim(MemberNumber)) = 0
       Salir_ACR = "T"
       Return
    EndIf
    
    TxMsg Manda
    GetRxmsg "Espere... Redencion WOW"
EndSub

Sub Valida_Redencion_Tmed_Wow
    Var Campo_01_ : A50 = ""
    Var Campo_02_ : A50 = ""
    Salida_Consulta_Clientes = ""
    Format Campo_01_ as "<WS_TIMEOUT>"
    Format Campo_02_ as "<Success>"

    If Mid(@RxMsg, 1, 32 ) = "invokeButton_Automatico (Error):"
       If @RVC = 2
          LoadKyBdMacro Key (19,102)
          Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
          Salida_Consulta_Clientes = "T"
       Else
          Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
       EndIf
       Call Graba_Off_Line_CRM
       ExitCancel
    EndIf
    If Trim(@RxMsg) = "NO EXISTE MIEMBRO"
       //26Dic2014
       Split @RxMsg, "|", Resp_Red[1], Resp_Red[2]
       //26Dic2014 WaitForEnter Resp_Red[1]
       //26Dic2014 WaitForEnter Resp_Red[2]
       Format Campo_01_ as "<WS_SOA_ERROR>"
       Format Campo_02_ as "<", Resp_Red[2], ">"
       //26Dic2014
       
       If @RVC = 2
          LoadKyBdMacro Key (19,102)
          InfoMessage Mid(@RxMsg, 1, 32 )
          Salida_Consulta_Clientes = "T"
       Else
       	  //26Dic2014
       	  Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
          ExitCancel
          //26Dic2014 
          
       	  //26Dic2014 ExitWithError Mid(@RxMsg, 1, 32 )
       EndIf
    ElseIf Mid(Trim(@RxMsg),1, 26)  = "NO HAY COMUNICACION CON WS"
       //26Dic2014
       Split @RxMsg, "|", Resp_Red[1], Resp_Red[2]
       //26Dic2014 WaitForEnter Resp_Red[1]
       //26Dic2014 WaitForEnter Resp_Red[2]
       Format Campo_01_ as "<WS_SOA_ERROR>"
       Format Campo_02_ as "<", Resp_Red[2], ">"
       //26Dic2014
       
       //26Dic2014 Format Campo_01_ as "<WS_TIMEOUT>"
       //26Dic2014 Format Campo_02_ as "<Success>"
       Call Graba_Off_Line_CRM
       If @RVC = 2
          LoadKyBdMacro Key (19,102)
          Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
          Salida_Consulta_Clientes = "T"
       Else
         	Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
       EndIf
       ExitCancel
    ElseIf Mid(Trim(@RxMsg),1, 29)  = "NO SE REALIZO LA ACREDITACION"
       //26Dic2014
       Split @RxMsg, "|", Resp_Red[1], Resp_Red[2]
       //26Dic2014 WaitForEnter Resp_Red[1]
       //26Dic2014 WaitForEnter Resp_Red[2]
       If Len(Resp_Red[2]) > 0
          Format Campo_01_ as "<WS_SOA_ERROR>"
          Format Campo_02_ as "<", Resp_Red[2], ">"
       Else
       	  Format Campo_01_ as "<WS_TIMEOUT>"
       	  Format Campo_02_ as "<NO_WS>"
       EndIf
       //26Dic2014
       
       //26Dic2014 Format Campo_01_ as "<WS_TIMEOUT>"
       //26Dic2014 Format Campo_02_ as "<Success>"
       Call Graba_Off_Line_CRM
       If @RVC = 2
          LoadKyBdMacro Key (19,102)
          Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
          Salida_Consulta_Clientes = "T"
       Else
         	Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
       EndIf
       ExitCancel
    ElseIf Mid(Trim(@RxMsg),1, 21)  = "ERROR EN ACREDITACION"
       //26Dic2014
       Split @RxMsg, "|", Resp_Red[1], Resp_Red[2]
       //26Dic2014 WaitForEnter Resp_Red[1]
       //26Dic2014 WaitForEnter Resp_Red[2]
       Format Campo_01_ as "<WS_SOA_ERROR>"
       Format Campo_02_ as "<", Resp_Red[2], ">"
       //26Dic2014
       
       //26Dic2014 Format Campo_01_ as "<WS_TIMEOUT>"
       //26Dic2014 Format Campo_02_ as "<Success>"
       Call Graba_Off_Line_CRM
       If @RVC = 2
          LoadKyBdMacro Key (19,102)
          Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
          Salida_Consulta_Clientes = "T"
       Else
         	Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
       EndIf
       ExitCancel
    ElseIf Trim(@RxMsg) = "_timeout"
       Format Campo_01_ as "<WS_TIMEOUT>"
       Format Campo_02_ as "<Success>"
       Call Graba_Off_Line_CRM
	   If @RVC = 2
          LoadKyBdMacro Key (19,102)
          Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
          Salida_Consulta_Clientes = "T"
       Else
       	  Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
       EndIf
       ExitCancel
    EndIf
    Format Respuesta as @RxMsg
    Call   Graba_Archivo_datos(Respuesta)
EndSub

//08Ene2015


//13Ene2015
Event Tmed: *
      Var MemberNumber       : A100 = ""
      Var ALSELevelPrice     : A50  = ""
      Var BranchDivision     : A50  = ""
      Var ALSEPaymnetMode    : A50  = ""
      Var ActivityDate       : A50  = ""
      Var AdjustedListPrice  : A50  = ""
      Var Amount             : A50  = ""	
      Var Comments           : A50  = ""
      Var ItemNumber         : A50  = ""
      Var LocationName       : A50  = ""
      Var PartnerName        : A50  = ""
      Var PointName          : A50  = ""
      Var Points             : A50  = ""	
      Var ProcessDate        : A50  = ""
      Var ProcessingComment  : A50  = ""
      Var ProcessingLog      : A50  = ""
      Var ProductName        : A50  = ""
      Var Quantity           : A50  = ""
      Var TransactionChannel : A50  = ""
      Var TransactionDate    : A50  = ""
      Var TransactionSubType : A50  = ""
      Var TransactionType    : A50  = ""
      Var VoucherNumber      : A50  = ""
      Var VoucherQty         : A50  = "1"
      Var VoucherType        : A50  = ""
      Var Organization       : A50  = ""
      Var Bussiness_date     : A50  = ""
      Var Centroconsumo      : A50  = ""
      Var Cheque             : A50  = ""
      Var Transmitido        : A50  = ""
      Var Arma_Cadena        : A2000 = ""
      Var Status_Respuesta   : A1   = ""
      Var CC                 : N2   = 0
      Var Campo                   : A78  = ""
      Var Sub_Nivel_Seleccionado  : N3   = ""
      Var Main_Nivel_Seleccionado : N1   = 0
      Var Grupo                   : A1   = ""
      Var Grupo_Numerico          : N2   = 0
      Var Tecla_Descuento         : Key
      Var Numerico                : N6   = 0
      Var Opcion_Redencion        : N2   = 0
      Var Cheque_Red              : N4   = @CKNUM
      Var SNivel                  : A5   = ""
      
      //Var Monto_Pagar        : $10   = 0
      //Input Monto_Pagar, "Digite Monto a Pagar"
      
      Var TotalCheque        : $10   = @TNDTTL
      
      //05Feb2015
      //Var Campo                      : A50   = ""
      Var Mes_Tempo                  : A2    = ""
      Var TTL001                     : $10   = 0
      Var Campo000                   : A20   = ""
      Var Fecha                      : A12   = ""
      Var Fecha1                     : A30   = ""
      Var FN01                       : N6    = 0
      Var Path_Solicita              : A90   = ""
      Var Path_Respuesta             : A90   = ""
      Var File_Solicita              : A190  = ""
      Var File_Respuesta             : A190  = ""
      Var File_Paso                  : A190  = ""
      Var II                         : N6    = 0
      Var Business_Date              : A30   = ""
      Var Numero_Touchscreen         : N6    = 0
      Var Numero_TouchscreenN        : N6    = 0
      Var Imp_Rollo                  : N4    = 0
      Var Terminal                   : N4    = 0
      Var Linea                      : A99   = ""
      Var WSID                       : A3    = ""
      Var TimeOut                    : N3    = 0
      Var Mensaje_Salida             : A90   = ""
      //05Feb2015
      
      //15Jul2015
     
      //15Jul2015
      
      //05Feb2015
      If @TTLDUE < 0
         Format Ticket_Unico as @WSID{01}, @CKNUM{04}, Mid(@Chk_Open_Time, 10, 2) , Mid(@Chk_Open_Time, 13, 2)
         If Len(Trim(@Ckid)) = 0
            CLEARCHKID
            @Ckid        = ( Ticket_Unico )
         EndIf
      Else
         Format Ticket_Unico as @WSID{01}, @CKNUM{04}, Mid(@Chk_Open_Time, 10, 2) , Mid(@Chk_Open_Time, 13, 2)
         If Len(Trim(@Ckid)) = 0
            CLEARCHKID
            @Ckid       = ( Ticket_Unico )
         EndIf
         
         Monto_Original = ( @TNDTTL )
         TTL001         = ( @Ttldue - @TNDTTL )
         Monto_Original = ( @TNDTTL )
   
         If TTL001 <= 0 
            Prompt "Inicia Proceso Inserta Reg"
            If TTL001         <    0
               Monto_Original = ( @Ttldue )
            EndIf
            Call Version_Micros_Sybase
            Call Trae_Status_Facturable_FPago_Repositorio
            Call Verifica_FPago_Facturable
            ClearChkInfo
            Format Campo as "Edofacturacion:", Facturacion_Parcial
            SaveChkInfo Campo
            Format Campo as "EdofacturacionO:", Facturacion_Parcial_Real
            SaveChkInfo Campo
         EndIf
      EndIf
      //05Feb2015
      
      //COMBOS
      Call Inserta_Combo_Count
      //COMBOS
      
      
      
      //WaitForEnter "Demo"
      
      Call Asigna_Ckid
      Call Verifica_Descuento_Aplicado_Cheque_Wow
      
      //WaitForEnter TotalCheque
      //WaitForEnter @TNDTTL
      
      
      
      If TotalCheque = 0
         TotalCheque = @TTLDUE
      EndIf
      
      If TotalCheque = 0
         ExitCancel
      EndIf
      
      Prompt "Espere ...1"
      //15Ene2015 Call Verifica_Item_Seleccionado
      Call Lee_Configuracion_CRM
      
    //  Call Verifica_Descuento_Aplicado_Cheque
    //  If Descuento_Aplicado_Cheque = "T"
    //     Call Verifica_Descuento_WOW
    //     If Flag_Descuento_WOW <> 1
    //        ExitWithError "NO SE PERMITE EL USO DE PUNTOS CON DESCUENTOS"
    //     EndIf
    //  EndIf 
      
      Call Lee_Miembro
      ////Call Lee_Privilegios_Nivel

      If Len(Trim(NumeroMiembro)) = 0
         ExitWithError "NO EXISTE MIEMBRO DE CRM PARA ESTE CHEQUE"
      EndIf
      

Prompt "Espere ...2"
      
      Call Consulta_Business_Date_SyBase
      //Call Lee_Privilegios_Nivel_Miembro
      
Prompt "Espere ...3"

      Format Ticket_Unico      as @WSID{01}, @CKNUM{04}, Mid(@Chk_Open_Time,10,2), Mid(@Chk_Open_Time,13,2)
      Format Fecha_a_Convertir as "20", Mid(@Chk_Open_Time,7,2), "-", Mid(@Chk_Open_Time,1,2), "-",Mid(@Chk_Open_Time,4,2)

      Call Consulta_Fecha_Juliana_Micros(Fecha_a_Convertir)      

      Format SNivel            as "NA" //Nivel_Price[Sub_Nivel_Seleccionado]
      Format TicketNumber as Fecha_Numerica{04}, Tienda{05}, Ticket_Unico

Prompt "Espere ...4"

      //15Ene2015 If Item_Qty_Seleccionado <> 1
         //15Ene2015 InfoMessage "WOW CRM", "SOLO SE PERMITE UN ARTICULO PARA REDIMIR A LA VEZ"
         //15Ene2015 ExitCancel
      //15Ene2015 EndIf

      //Tienda = 38101  //QUITAR //QUITAR //QUITAR //QUITAR
      
      
      //26Ene2015
      Call Busca_Aportacion_Acreditaciones_Soa_Only
      If Total_Aportaciones <> 0
         TotalCheque = ( TotalCheque - Total_Aportaciones )
      EndIf
      //26Ene2015
      
      //15Jul2015
      //Call Busca_Points_Cheque
      //15Jul2015
      
      Call Guarda_Campos_Acreditaciones_Redenciones_Wow(SNivel, Item_Precio_Seleccionado, Item_No_Seleccionado, Item_Qty_Seleccionado, Cheque_Red)
      //Call Arma_Registro_Acreditaciones_Redenciones
      Call Formatea_Datos_Redenciones_Tmed_Wow
      //Call Inserta_Solicitud_Acreditacion_Redencion
      //Call Arma_Registro_Acreditaciones_Redenciones_CRM_Log
      //Call Inserta_Solicitud_Acreditacion_Redencion

//New
Call Manda_Redemption_Tmed_Wow
Call Valida_Redemption_Tmed_Wow
Status_Respuesta = "P"
Tipo_Descuento   = "M"
//New

Prompt "Espere ...5"

      Name_Jar = "REDENCIONES"
      Status_Respuesta = ""   //29Dic2011 

Prompt "Espere ...6"

      Call Graba_Archivo_datos(Status_Respuesta)   //29Dic2011 
      
EndEvent

Sub Guarda_Campos_Acreditaciones_Redenciones_Wow(Ref SNivel_ , Ref Price_, Ref Number_, Ref Unidades_, Ref Cheque_ )
    MemberNumber         = NumeroMiembro
    ID                   = "NUEVO"
    ALSELevelPrice       = "NA"
    BranchDivision       = "BKCASUAL"
    ALSEPaymnetMode      = "Puntos WOW"
    ActivityDate         = Business_Date
    AdjustedListPrice    = "0"
    Amount               = TotalCheque
    Comments             = ""
    ItemNumber           = 0
    LocationName         = Tienda
    PartnerName          = PartnerName
    PointName            = ""  //"Star"
    Points               = "0"  //"0"
    ProcessDate          = Business_Date
    ProcessingComment    = ""
    ProcessingLog        = ""
    ProductName          = "WOW Ticket"  // "22001" // 
    Quantity             = 0   //  Unidades_
    IntegrationStatus    = ""
    Status               = ""	
    SubStatus            = ""
    TicketNumber         = TicketNumber
    TransactionChannel   = "Store"
    TransactionDate      = Business_Date
    TransactionSubType   = "Product"
    TransactionType      = "Redemption"
    VoucherNumber        = ""  //VoucherTransactionId[Opcion_Redencion]
    VoucherQty           = ""
    VoucherType          = ""
    Organization         = "BKCASUAL"
    Estatus              = "PEN"
    Bussiness_date       = Business_Date_Real
    Centroconsumo        = @RVC
    Cheque               = Cheque_
    Transmitido          = "F"
EndSub

Sub Formatea_Datos_Redenciones_Tmed_Wow
    //AdjustedListPrice = TotalCheque
    Var Nombre_Mesero : A100 = ""
    Format Nombre_Mesero As @Tremp, " ", Trim(@TREMP_FNAME), " ", Trim(@TREMP_LNAME)
    Format Arma_Cadena_82, Separador as MemberNumber,id, ALSELevelPrice, BranchDivision, ALSEPaymnetMode, ActivityDate, AdjustedListPrice, Amount, Comments, ItemNumber, LocationName, PartnerName, PointName, Points, ProcessDate, ProcessingComment, ProcessingLog, ProductName, Quantity, IntegrationStatus, Status, SubStatus, TicketNumber, TransactionChannel, TransactionDate, TransactionSubType, TransactionType, VoucherNumber, VoucherQty, VoucherType, Organization, Estatus, Bussiness_date, Centroconsumo, Cheque, Transmitido, Linea_Cheque, Nombre_Mesero
Endsub
Sub Manda_Redemption_Tmed_Wow
    Manda = ""
    
    //20Ene2015
    Call Lee_Miembro_O
    //20Ene2015


    Format Manda, Separador as "REW", Arma_Cadena_82, Nombre_Miembro_O
    TxMsg Manda
    GetRxmsg "Espere... Cliente WOW"
EndSub
Sub Valida_Redemption_Tmed_Wow
    Var Campo_01_   : A50 = ""
    Var Campo_02_   : A50 = ""
    
    Salida_Consulta_Clientes = ""
    Format Campo_01_ as "<WS_TIMEOUT>"
    Format Campo_02_ as "<Success>"
    //WaitForenter @RxMsg
    
    If Mid(@RxMsg, 1, 32 ) = "invokeButton_Automatico (Error):"
       If @RVC = 2
          LoadKyBdMacro Key (19,102)
          Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
          Salida_Consulta_Clientes = "T"
       Else
          Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
       EndIf
       Call Graba_Off_Line_CRM
       ExitCancel
    EndIf
    If Trim(@RxMsg) = "NO EXISTE MIEMBRO"
       //26Dic2014
       Split @RxMsg, "|", Resp_Red[1], Resp_Red[2]
       //26Dic2014 WaitForEnter Resp_Red[1]
       //26Dic2014 WaitForEnter Resp_Red[2]
       Format Campo_01_ as "<WS_SOA_ERROR>"
       Format Campo_02_ as "<", Resp_Red[2], ">"
       //26Dic2014
       
       If @RVC = 2
          LoadKyBdMacro Key (19,102)
          InfoMessage Mid(@RxMsg, 1, 32 )
          Salida_Consulta_Clientes = "T"
       Else
       	  //26Dic2014
       	  Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
          ExitCancel
          //26Dic2014
          
       	  //26Dic2014 ExitWithError Mid(@RxMsg, 1, 32 )
       EndIf
       ExitCancel
    ElseIf Mid(Trim(@RxMsg),1, 26)  = "NO HAY COMUNICACION CON WS"
       //26Dic2014
       Split @RxMsg, "|", Resp_Red[1], Resp_Red[2]
       //26Dic2014 WaitForEnter Resp_Red[1]
       //26Dic2014 WaitForEnter Resp_Red[2]
       Format Campo_01_ as "<WS_SOA_ERROR>"
       Format Campo_02_ as "<", Resp_Red[2], ">"
       //26Dic2014
       
       If Len(Resp_Red[2]) > 0
          Format Campo_01_ as "<WS_SOA_ERROR>"
          Format Campo_02_ as "<", Resp_Red[2], ">"
       Else
       	  Format Campo_01_ as "<WS_TIMEOUT>"
       	  Format Campo_02_ as "<NO_WS>"
       EndIf
       //26Dic2014 Format Campo_01_ as "<WS_TIMEOUT>"
       //26Dic2014 Format Campo_02_ as "<Success>"
       Call Graba_Off_Line_CRM
       If @RVC = 2
          LoadKyBdMacro Key (19,102)
          Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
          Salida_Consulta_Clientes = "T"
       Else
         	Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
       EndIf
       ExitCancel
    ElseIf Mid(Trim(@RxMsg),1, 26)  = "NO SE REALIZO LA REDENCION"
       //26Dic2014
       Split @RxMsg, "|", Resp_Red[1], Resp_Red[2]
       //26Dic2014 WaitForEnter Resp_Red[1]
       //26Dic2014 WaitForEnter Resp_Red[2]
       Format Campo_01_ as "<WS_SOA_ERROR>"
       Format Campo_02_ as "<", Resp_Red[2], ">"
       //26Dic2014
       
       //26Dic2014 Format Campo_01_ as "<REDENCIONES>"
       //26Dic2014 Format Campo_02_ as "<Fail>"
       Call Graba_Off_Line_CRM
       If @RVC = 2
          LoadKyBdMacro Key (19,102)
          Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
          Salida_Consulta_Clientes = "T"
       Else
         	Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
       EndIf
       ExitCancel
    ElseIf Mid(Trim(@RxMsg),1, 18)  = "ERROR EN REDENCION"
       //26Dic2014
       Split @RxMsg, "|", Resp_Red[1], Resp_Red[2]
       //26Dic2014 WaitForEnter Resp_Red[1]
       //26Dic2014 WaitForEnter Resp_Red[2]
       Format Campo_01_ as "<WS_SOA_ERROR>"
       Format Campo_02_ as "<", Resp_Red[2], ">"
       //26Dic2014
       
       //26Dic2014 Format Campo_01_ as "<WS_TIMEOUT>"
       //26Dic2014 Format Campo_02_ as "<Success>"
       Call Graba_Off_Line_CRM
       If @RVC = 2
          LoadKyBdMacro Key (19,102)
          Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
          Salida_Consulta_Clientes = "T"
       Else
         	Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
       EndIf
       ExitCancel
    ElseIf Trim(@RxMsg) = "_timeout"
       Format Campo_01_ as "<WS_TIMEOUT>"
       Format Campo_02_ as "<Success>"
       Call Graba_Off_Line_CRM
	    
       If @RVC = 2
          LoadKyBdMacro Key (19,102)
          Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
          Salida_Consulta_Clientes = "T"
       Else
       	  Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
          Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
       EndIf
       ExitCancel
    EndIf
    Format Respuesta as @RxMsg
    Call   Graba_Archivo_datos(Respuesta)
EndSub

//13Ene2015

//15Ene2015
Sub Verifica_Descuento_WOW
    Var I   : N4 = 0
    For I = 1 to @NUMDTLT
        If @dtl_type[I] = "D" And @dtl_Is_Void[I] = 0
           If     @DTL_OBJECT[I] = Descuento_RedondeoWOW Or @DTL_OBJECT[I] = Descuento_RedondeoWOW
              Flag_Descuento_WOW = 1
           Else
           	  Flag_Descuento_WOW = 0
           EndIf
        EndIf
    EndFor
EndSub
//15Ene2015

//16Ene2015
Sub Verifica_Donacion_WOW(Ref Buscar_)
    Var I   : N4 = 0
    Flag_Donacion_WOW = 0
    For I = 1 to @NUMDTLT
        If @dtl_type[I] = "S" And @dtl_Is_Void[I] = 0
           If Donaciones[I] = Buscar_
              Flag_Donacion_WOW = 1
              Return
           EndIf
        EndIf
    EndFor
EndSub
//16Ene2015

//18Ene2015
Sub Busca_MenuItem_Acreditaciones_Soa
    Var I                    : N4  = 0
    Var Descontar            : $10 = 0
    Var Nombre_Forma_Pago_PM : A20 = ""
    Call Suma_MenuItem_Soa_Only
    For I = 1 to @NUMDTLT
        If @DTL_TYPE[I]   = "M" AND @DTL_IS_VOID[I] = 0
           If @DTL_TTL[I] > 0
              Call Verifica_Linea_Redencion( I )
              If Linea_Redimida_Encontrada = "T"
                 If @Dtl_is_cond[I] = 0     //ITEM
                    Condimento_Aplicado = "T"
                 Else                       //CONDIMENT
                 	  Condimento_Aplicado = ""
                 EndIf
              Else
                 //22Ene2015 If Condimento_Aplicado = "T" And @Dtl_is_cond[I] = 1
                    //No hace Nada
              	 //22Ene2015 Else
              	 	  Condimento_Aplicado = ""
              	 	  
              	 	  //22Ene2015 WaitForEnter @DTL_NAME[I]  //Pepsi GD
              	 	  //22Ene2015 WaitForEnter @MLVL[I]      //2 2
              	 	  //22Ene2015 WaitForEnter @SLVL[I]      //10 10
              	 	  //WaitForEnter Nivel_Price[@MLVL[I]]
              	 	  //22Ene2015 WaitForEnter @Dtl_Plvl[I]  //4 1
              	 	  //22Ene2015 WaitForEnter @dtl_prefix[I]//0 0
              	 	  
                    //22Ene2015 SNivel_R     = Nivel_Price[@MLVL[I]]
                    SNivel_R     = Nivel_Price[@Dtl_Plvl[I]]   //22Ene2015
                    Price_R      = @DTL_TTL[I]
                    Number_R     = @DTL_OBJECT[I]
                    Nombre_R     = @DTL_NAME[I]
                    Unidades_R   = @DTL_QTY[I]
                    Cheque_R     = @CKNUM
                    Linea_Cheque = I
                    Format TicketNumber as Fecha_Numerica{04}, Tienda{05}, Ticket_Unico
                    If Primera_Cero = "F"
                       //WaitForenter "Cero"
                       //18Ene2015 Call Guarda_Campos_Acreditaciones_Cero(SNivel_R , Price_R, Cons_Item, Unidades_R, Cheque_R, Number_R,      Total_Fpago_Cheque )
                       //18Ene2015 Call Arma_Registro_Acreditaciones_Redenciones
                       //18Ene2015 Call Inserta_Solicitud_Acreditacion_Redencion
                       ////Call Arma_Registro_Acreditaciones_Redenciones_CRM_Log
                       ////Call Inserta_Solicitud_Acreditacion_Redencion
                       Primera_Cero = "T"
                    EndIf
                    
                    For Contador_OP = 1 to Unidades_R
                        Cons_Item = ( Cons_Item + 1 )
                        If Unidades_R > 1
                           Total_Paso = ( Price_R / Unidades_R )
                        EndIf
                        //20Ene2015
                        If No_Formas_Pago = 1
                           //21Ene2015 Format Nombre_Forma_Pago_PM   As Nombre_Forma_Pago_Ws
                           Format Nombre_Forma_Pago_PM As ""     //21Ene2015
                        Else
                        	 Format Nombre_Forma_Pago_PM As ""
                        EndIf
                        //20Ene2015
                        
                        //23Ene2015
                        If Total_MenuItem <> 0
                           //8Abr2015 Total_Fpago_Cheque = Total_MenuItem
                        EndIf
                        //23Ene2015
                        
                        If Total_Paso > 0
                           Call Guarda_Campos_Acreditaciones_MenuItem(SNivel_R , Total_Paso, Cons_Item, Unidades_R, Cheque_R, Number_R, Total_Fpago_Cheque, Nombre_Forma_Pago_PM )
                           Total_Paso = 0
                        Else 
						
                           Call Guarda_Campos_Acreditaciones_MenuItem(SNivel_R , Price_R, Cons_Item, Unidades_R, Cheque_R, Number_R, Total_Fpago_Cheque, Nombre_Forma_Pago_PM )
                        EndIf
						
							
							Call Arma_Registro_Acreditaciones_Redenciones							
						
						Call Inserta_Solicitud_Acreditacion_Redencion
						
                    EndFor
                    Lineas_Acreditas_Contador = ( Lineas_Acreditas_Contador + 1 )
                 //22Ene2015 EndIf
              EndIf
           EndIf
        EndIf
    EndFor
	offline=0 //test 10/08/2015
EndSub

Sub Busca_Descuentos_Acreditaciones_Soa
    Var I               : N4  = 0
    Var TTl_Descto_01   : $10 = 0
    Var TTl_Descto_02   : $10 = 0
    For I = 1 to @NUMDTLT
        If @DTL_TYPE[I]   = "D" AND @DTL_IS_VOID[I] = 0
           If @DTL_OBJECT[I] = Descuento_RedondeoWOW //26Ene2015 Or @Dtl_Objnum[I] = Descuento_RedondeoWOW
              SNivel_R     = Nivel_Price[@MLVL[I]]
              Price_R      = @DTL_TTL[I]
              Number_R     = @DTL_OBJECT[I]
              Nombre_R     = @DTL_NAME[I]
              Unidades_R   = @DTL_QTY[I]
              Cheque_R     = @CKNUM
              Linea_Cheque = I
              SNivel_R     = "NA"
              
              //27Ene2015 WaitForEnter Price_R
              If Price_R < 0
                 TTl_Descto_01 = ( Price_R * -1 )
                 TTl_Descto_02 = ( TTl_Descto_02 + TTl_Descto_01 )
              EndIf
              //27Ene2015 WaitForEnter TTl_Descto_02
              
              //26Ene2015 Format TicketNumber as Fecha_Numerica{04}, Tienda{05}, Ticket_Unico
              //26Ene2015 Cons_Item = ( Cons_Item + 1 )
              //26Ene2015 Call Guarda_Campos_Acreditaciones_Descuentos(SNivel_R , Price_R, Cons_Item, Unidades_R, Cheque_R, Number_R, Price_R, Nombre_R )
              //26Ene2015 Call Arma_Registro_Acreditaciones_Redenciones
              //26Ene2015 Call Inserta_Solicitud_Acreditacion_Redencion
              
              //07May2015
              Call Descuento_Acreditacion_Descuento_RedondeoWOW(TTl_Descto_01, TTl_Descto_02 )
              //07May2015
              
           Else    //07May2014  Otros Descuentos
           	  SNivel_R     = Nivel_Price[@MLVL[I]]
              Price_R      = @DTL_TTL[I]
              Number_R     = @DTL_OBJECT[I]
              Nombre_R     = @DTL_NAME[I]
              Unidades_R   = @DTL_QTY[I]
              Cheque_R     = @CKNUM
              Linea_Cheque = I
              SNivel_R     = "NA"
              
              //07May2015  If Price_R < 0
                 //07May2015  TTl_Descto_01 = ( Price_R * -1 )
                 //07May2015  TTl_Descto_02 = ( TTl_Descto_02 + TTl_Descto_01 )
              //07May2015  EndIf   //07May2015
              
              //07May2015
              Call Descuento_Acreditacion_Descuento_Otros(TTl_Descto_01, Price_R)
              //07May2015
              
           EndIf
           //26Ene2015 Lineas_Acreditas_Contador = ( Lineas_Acreditas_Contador + 1 )
        EndIf
        
    EndFor
    
    //26Ene2015
    //07May2015If TTl_Descto_02 <> 0
       //07May2015TTl_Descto_02 = ( TTl_Descto_02 * -1 )
       //07May2015Price_R       = TTl_Descto_02
       //07May2015Format TicketNumber as Fecha_Numerica{04}, Tienda{05}, Ticket_Unico
       //07May2015Cons_Item = ( Cons_Item + 1 )
       //07May2015Call Guarda_Campos_Acreditaciones_Descuentos(SNivel_R , Price_R, Cons_Item, Unidades_R, Cheque_R, Number_R, Price_R, Nombre_R )
       //07May2015Call Arma_Registro_Acreditaciones_Redenciones
       //07May2015Call Inserta_Solicitud_Acreditacion_Redencion
       //07May2015Lineas_Acreditas_Contador = ( Lineas_Acreditas_Contador + 1 )
    //07May2015EndIf
    //26Ene2015
    
EndSub

//07May2015
Sub Descuento_Acreditacion_Descuento_RedondeoWOW(Ref TTl_Descto_01, Ref TTl_Descto_02 )
    If TTl_Descto_02 <> 0
       TTl_Descto_02 = ( TTl_Descto_02 * -1 )
       Price_R       = TTl_Descto_02
       Format TicketNumber as Fecha_Numerica{04}, Tienda{05}, Ticket_Unico
       Cons_Item = ( Cons_Item + 1 )
       Call Guarda_Campos_Acreditaciones_Descuentos(SNivel_R , Price_R, Cons_Item, Unidades_R, Cheque_R, Number_R, Price_R, Nombre_R )
       Call Arma_Registro_Acreditaciones_Redenciones
       Call Inserta_Solicitud_Acreditacion_Redencion
       Lineas_Acreditas_Contador = ( Lineas_Acreditas_Contador + 1 )
    EndIf
EndSub

Sub Descuento_Acreditacion_Descuento_Otros(Ref TTl_Descto_01, Ref TTl_Descto_02 )
    If TTl_Descto_02 <> 0
       //07May2015 TTl_Descto_02 = ( TTl_Descto_02 * -1 )
       //07May2015 Price_R       = TTl_Descto_02
       Format TicketNumber as Fecha_Numerica{04}, Tienda{05}, Ticket_Unico
       Cons_Item = ( Cons_Item + 1 )
       Call Guarda_Campos_Acreditaciones_Descuentos_Otros(SNivel_R , TTl_Descto_02, Cons_Item, Unidades_R, Cheque_R, Number_R, TTl_Descto_02, Nombre_R )
       Call Arma_Registro_Acreditaciones_Redenciones
       Call Inserta_Solicitud_Acreditacion_Redencion
       Lineas_Acreditas_Contador = ( Lineas_Acreditas_Contador + 1 )
    EndIf
EndSub
//07May2015


Sub Busca_Aportacion_Acreditaciones_Soa
    Var I               : N4  = 0
    For I = 1 to @NUMDTLT
        If @DTL_TYPE[I]   = "S" AND @DTL_IS_VOID[I] = 0
           //07Jul2015 If    @DTL_OBJECT[I] = 39 Or @DTL_OBJECT[I] = 40 //26Ene2015 Or @Dtl_Objnum[I] = 39 Or @Dtl_Objnum[I] = 40
           If    Mid(@DTL_NAME[I],1,3) = Prefijo_Aportaciones
                 SNivel_R     = Nivel_Price[@MLVL[I]]
                 Price_R      = @DTL_TTL[I]
                 Number_R     = @DTL_OBJECT[I]
                 Nombre_R     = @DTL_NAME[I]
                 Unidades_R   = @DTL_QTY[I]
                 Cheque_R     = @CKNUM
                 Linea_Cheque = I
                 SNivel_R     = "NA"
                 Unidades_R   = 0
                 Format TicketNumber as Fecha_Numerica{04}, Tienda{05}, Ticket_Unico
                 Cons_Item = ( Cons_Item + 1 )
                 Call Guarda_Campos_Acreditaciones_Aportaciones(SNivel_R , Price_R, Cons_Item, Unidades_R, Cheque_R, Number_R, Price_R, Name_FP )
                 Call Arma_Registro_Acreditaciones_Redenciones
                 Call Inserta_Solicitud_Acreditacion_Redencion
           EndIf
           Lineas_Acreditas_Contador = ( Lineas_Acreditas_Contador + 1 )
        EndIf
    EndFor
EndSub

Sub Busca_Formas_Pago_Acreditaciones_Soa
    Var I                     : N4  = 0
    Var Tender_Efectivo_Ttl_2 : $12 = 0
    No_Formas_Pago       = 0
    Nombre_Forma_Pago_Ws = ""
    SNivel_R2            = ""
    Price_R2             = 0
    Number_R2            = ""
    Nombre_R2            = ""
    Unidades_R2          = 0
    Cheque_R2            = 0
    Tender_Efectivo_Ttl  = 0
    
    //22Ene2015
    Call Busca_Aportacion_Acreditaciones_Soa_Only
    //22Ene2015
    
    //WaitForEnter "Efectivo"  //
    //WaitForEnter Tender_Efectivo_No  //
    
    For I = 1 to @NUMDTLT
        If @DTL_TYPE[I]   = "T" AND @DTL_IS_VOID[I] = 0
        
           //WaitForEnter @DTL_OBJECT[I]  //1
           //WaitForEnter @Dtl_Objnum[I]  //1
           //WaitForEnter Tender_Efectivo_No  //1
           
           If @DTL_OBJECT[I]     = Tender_Crm         //26ENe2015 Or @Dtl_Objnum[I] = Tender_Crm
              Format Name_FP    As Trim(@DTL_NAME[I])
           ElseIf @Dtl_Objnum[I] = Tender_Efectivo_No //26ENe2015 Or @Dtl_Objnum[I] = Tender_Efectivo_No
              //WaitForEnter "Entre"
              SNivel_R2             = Nivel_Price[@MLVL[I]]
              Price_R2              = @DTL_TTL[I]
              Number_R2             = @DTL_OBJECT[I]
              Nombre_R2             = Trim(@DTL_NAME[I])
              Unidades_R2           = @DTL_QTY[I]
              Cheque_R2             = @CKNUM
              Tender_Efectivo_Ttl   = ( Tender_Efectivo_Ttl + Price_R2 )
              Format Name_FP       As Trim(@DTL_NAME[I])
              //WaitForEnter Tender_Efectivo_Ttl
           Else
              SNivel_R             = Nivel_Price[@MLVL[I]]
              Price_R              = @DTL_TTL[I]
              Number_R             = @DTL_OBJECT[I]
              Nombre_R             = Trim(@DTL_NAME[I])
              Unidades_R           = @DTL_QTY[I]
              Cheque_R             = @CKNUM
              Linea_Cheque         = I
              No_Formas_Pago       = ( No_Formas_Pago + 1 )
              SNivel_R             = "NA"
              Nombre_Forma_Pago_Ws = Nombre_R
              Format TicketNumber as Fecha_Numerica{04}, Tienda{05}, Ticket_Unico
              Format Name_FP      As Trim(@DTL_NAME[I])
              Cons_Item = ( Cons_Item + 1 )
              
              //22Ene2015
              //WaitForEnter "Total_Aportaciones"
              //WaitForEnter Total_Aportaciones
			  
			  //infomessage "Tender_Efectivo_Ttl",Tender_Efectivo_Ttl
	          //infomessage "Tender_Efectivo_Ttl_2",Tender_Efectivo_Ttl_2
			  
              If Total_Aportaciones <> 0
                 Tender_Efectivo_Ttl_2 = ( Price_R - Total_Aportaciones )  //22Ene2015
                 Total_Aportaciones = 0                                  //22Ene2015
              Else
              	 Tender_Efectivo_Ttl_2 = ( Price_R )
              EndIf
              //22Ene2015
              If Tender_Efectivo_Ttl <> 0
                 //Tender_Efectivo_Ttl_2 = Tender_Efectivo_Ttl // Comentado el 9 Enero 2016 JMG
              EndIf
              
			  //infomessage "Tender_Efectivo_Ttl_2",Tender_Efectivo_Ttl_2
			  
                 //WaitForEnter "Acreditaciones 2"
                 //WaitForEnter @DTL_OBJECT[I]  //27
                 //WaitForEnter Tender_Crm      //60
                 
              If @DTL_OBJECT[I] = Tender_Crm         //15May2015  No graba
              Else  //15May2015
                 Call Guarda_Campos_Acreditaciones_Formas_Pago(SNivel_R , Price_R, Cons_Item, Unidades_R, Cheque_R, Number_R, Tender_Efectivo_Ttl_2, Nombre_R )
                 Call Arma_Registro_Acreditaciones_Redenciones
                 Call Inserta_Solicitud_Acreditacion_Redencion
                 Lineas_Acreditas_Contador = ( Lineas_Acreditas_Contador + 1 )
				 //infomessage Amount
              EndIf  //15May2015
           EndIf
        EndIf
    EndFor
    
    //WaitForEnter Tender_Efectivo_Ttl  //64 efectivo
    //WaitForEnter SNivel_R2  //Vacio
    //WaitForEnter Number_R2  //0
    //WaitForEnter Nombre_R2  //Vacio
    
    If Len(SNivel_R2) = 0 And Len(Number_R2) = 0 And Len(Nombre_R2) = 0
    Else
    	 Linea_Cheque         = I
       No_Formas_Pago       = ( No_Formas_Pago + 1 )
       SNivel_R2            = "NA"
       Nombre_Forma_Pago_Ws = Nombre_R2
       Format TicketNumber as Fecha_Numerica{04}, Tienda{05}, Ticket_Unico
       Cons_Item = ( Cons_Item + 1 )
       
       //22Ene2015
       //WaitForEnter Total_Aportaciones
	   //infomessage "Tender_Efectivo_Ttl Ronda2",Tender_Efectivo_Ttl
	   //infomessage "Tender_Efectivo_Ttl_2 Ronda2",Tender_Efectivo_Ttl_2
       
	   If Total_Aportaciones    <> 0
          Tender_Efectivo_Ttl_2 = ( Tender_Efectivo_Ttl - Total_Aportaciones )  //22Ene2015
          Total_Aportaciones    = 0                                               //22Ene2015
       Else
       	  Tender_Efectivo_Ttl_2 = ( Tender_Efectivo_Ttl )
       EndIf
       
	   //infomessage "Tender_Efectivo_Ttl_2 Ronda2",Tender_Efectivo_Ttl_2
       //07May2015

       //07May2015
       
       //22Ene2015
       //WaitForEnter Tender_Efectivo_Ttl_2  //64
       
       Call Guarda_Campos_Acreditaciones_Formas_Pago(SNivel_R2 , Price_R2, Cons_Item, Unidades_R2, Cheque_R2, Number_R2, Tender_Efectivo_Ttl_2, Nombre_R2 )
       Call Arma_Registro_Acreditaciones_Redenciones
       Call Inserta_Solicitud_Acreditacion_Redencion
       Lineas_Acreditas_Contador = ( Lineas_Acreditas_Contador + 1 )
	   //infomessage Amount
    EndIf
EndSub


Sub Guarda_Campos_Acreditaciones_Formas_Pago(Ref SNivel_ , Ref Price_, Ref Number_, Ref Unidades_, Ref Cheque_, Ref Number_R_, Ref Total_Ticket_, Ref Nombre_R_ )
    MemberNumber         = NumeroMiembro
    ID                   = "NUEVO"
    ALSELevelPrice       = SNivel_
    BranchDivision       = "WOW"
    ALSEPaymnetMode      = Trim(Nombre_R_)
    ActivityDate         = Business_Date
    AdjustedListPrice    = "0"
    Amount               = Total_Ticket_
    Comments             = ""
    ItemNumber           = Number_
    LocationName         = Tienda
    PartnerName          = PartnerName
    PointName            = ""
    Points               = "0"
    ProcessDate          = Business_Date
    ProcessingComment    = ""
    ProcessingLog        = ""
    ProductName          = "WOW Ticket"
    Quantity             = "0"
    IntegrationStatus    = ""
    Status               = ""	
    SubStatus            = ""
    TicketNumber         = TicketNumber
    TransactionChannel   = "Store"
    TransactionDate      = Business_Date
    TransactionSubType   = "Product"
    TransactionType      = "Accrual"
    VoucherNumber        = ""
    VoucherQty           = "1"
    VoucherType          = ""
    Organization         = "BKCASUAL"
    Estatus              = "PEN"
    Bussiness_date       = Business_Date
    Centroconsumo        = @RVC
    Cheque               = Cheque_
    Transmitido          = "F"
    Linea_Cheque         = 1
EndSub

Sub Guarda_Campos_Acreditaciones_Aportaciones(Ref SNivel_ , Ref Price_, Ref Number_, Ref Unidades_, Ref Cheque_, Ref Number_R_, Ref Total_Ticket_, Ref Nombre_R_ )
    MemberNumber         = NumeroMiembro
    ID                   = "NUEVO"
    ALSELevelPrice       = SNivel_
    BranchDivision       = "WOW"
    ALSEPaymnetMode      = Trim(Nombre_R_)
    ActivityDate         = Business_Date
    AdjustedListPrice    = "0"
    Amount               = Total_Ticket_
    Comments             = ""
    ItemNumber           = Number_
    LocationName         = Tienda
    PartnerName          = PartnerName
    PointName            = ""   //"Star"
    Points               = "0"   //"1"
    ProcessDate          = Business_Date
    ProcessingComment    = ""
    ProcessingLog        = ""
    ProductName          = Trim("Aportacion")  //22Ene2015 "WOW Ticket Donation"
    Quantity             = "0"
    IntegrationStatus    = ""
    Status               = "Queued"	
    SubStatus            = ""
    TicketNumber         = TicketNumber
    TransactionChannel   = "Store"
    TransactionDate      = Business_Date
    TransactionSubType   = "Product"
    TransactionType      = "Accrual"
    VoucherNumber        = ""
    VoucherQty           = "1"
    VoucherType          = ""
    Organization         = "BKCASUAL"
    Estatus              = "PEN"
    Bussiness_date       = Business_Date
    Centroconsumo        = @RVC
    Cheque               = Cheque_
    Transmitido          = "F"
    Linea_Cheque         = 1
EndSub

Sub Guarda_Campos_Acreditaciones_Descuentos(Ref SNivel_ , Ref Price_, Ref Number_, Ref Unidades_, Ref Cheque_, Ref Number_R_, Ref Total_Ticket_, Ref Nombre_R_ )
    MemberNumber         = NumeroMiembro
    ID                   = "NUEVO"
    ALSELevelPrice       = SNivel_
    BranchDivision       = "WOW"
    ALSEPaymnetMode      = "Descuento"   //15Jul2015 Trim(Nombre_R_) //08Jul2015 
    ActivityDate         = Business_Date
    AdjustedListPrice    = "0"
    If Total_Ticket_ > 0
       Total_Ticket_ = ( Total_Ticket_ * -1 )
    EndIf
    Amount               = Total_Ticket_
    Comments             = ""
    ItemNumber           = Number_
    LocationName         = Tienda
    PartnerName          = PartnerName
    PointName            = ""   //"Star"
    Points               = "0"   //"1"
    ProcessDate          = Business_Date
    ProcessingComment    = ""
    ProcessingLog        = ""
    ProductName          = "WOW Dto" //08Jul2015  "WOW Ticket"  //15Jul2015
    Quantity             = "0"     //27Ene2015
    IntegrationStatus    = ""
    Status               = "Queued"	
    SubStatus            = ""
    TicketNumber         = TicketNumber
    TransactionChannel   = "Store"
    TransactionDate      = Business_Date
    TransactionSubType   = "Product"
    TransactionType      = "Accrual"
    VoucherNumber        = ""
    VoucherQty           = "1"
    VoucherType          = ""
    Organization         = "BKCASUAL"
    Estatus              = "PEN"
    Bussiness_date       = Business_Date
    Centroconsumo        = @RVC
    Cheque               = Cheque_
    Transmitido          = "F"
    Linea_Cheque         = 1
EndSub

//07May2015
Sub Guarda_Campos_Acreditaciones_Descuentos_Otros(Ref SNivel_ , Ref Price_, Ref Number_, Ref Unidades_, Ref Cheque_, Ref Number_R_, Ref Total_Ticket_, Ref Nombre_R_ )
    MemberNumber         = NumeroMiembro
    ID                   = "NUEVO"
    ALSELevelPrice       = SNivel_
    BranchDivision       = "WOW"
    ALSEPaymnetMode      = "Descuento"
    ActivityDate         = Business_Date
    AdjustedListPrice    = "0"
    Amount               = Total_Ticket_
    Comments             = ""
    ItemNumber           = Number_
    LocationName         = Tienda
    PartnerName          = PartnerName
    PointName            = ""   //"Star"
    Points               = "0"   //"1"
    ProcessDate          = Business_Date
    ProcessingComment    = ""
    ProcessingLog        = ""
    ProductName          = "WOW Ticket"
    Quantity             = "0"     //27Ene2015
    IntegrationStatus    = ""
    Status               = "Queued"	
    SubStatus            = ""
    TicketNumber         = TicketNumber
    TransactionChannel   = "Store"
    TransactionDate      = Business_Date
    TransactionSubType   = "Product"
    TransactionType      = "Accrual"
    VoucherNumber        = ""
    VoucherQty           = "1"
    VoucherType          = ""
    Organization         = "BKCASUAL"
    Estatus              = "PEN"
    Bussiness_date       = Business_Date
    Centroconsumo        = @RVC
    Cheque               = Cheque_
    Transmitido          = "F"
    Linea_Cheque         = 1
EndSub
//07May2015

Sub Guarda_Campos_Acreditaciones_MenuItem(Ref SNivel_ , Ref Price_, Ref Number_, Ref Unidades_, Ref Cheque_, Ref Number_R_, Ref Total_Ticket_, Ref Nombre_Forma_Pago_PM_ )
    //SE MODIFICO UNIDADES A UNO
    //18ENe2015 Cons_Item = ( Cons_Item + 1 )
    
    //09Abr2015
    If @DTL_OBJECT[I] = Item_Redondeo
       Total_Ticket_         = Price_
       Price_                = 0
       //10Abr2015 Number_               = 1
       Number_R_             = "WOW Ticket"
       Nombre_Forma_Pago_PM_ = "RedondeoWow"
       SNivel_               = "NA"
    EndIf
    //09Abr2015
    
    MemberNumber         = NumeroMiembro
    ID                   = "NUEVO"
    ALSELevelPrice       = SNivel_
    //08Ene2015 BranchDivision       = "SBX"
    //08Ene2015 ALSEPaymnetMode      = "SBUX Card"
    BranchDivision       = "WOW"
    ALSEPaymnetMode      = Nombre_Forma_Pago_PM_  //20Ene2015 ""  //18Ene2015 "Puntos WOW"
    ActivityDate         = Business_Date
    AdjustedListPrice    = Price_         //18Ene2015 Price_ 
    Amount               = Total_Ticket_  //21Ene2015 Price_ 
    Comments             = ""
    ItemNumber           = Number_
    //21Ene2015 ItemNumber           = Cons_Item
    LocationName         = Tienda
    PartnerName          = PartnerName
    PointName            = ""
    Points               = "0"
    ProcessDate          = Business_Date
    ProcessingComment    = ""
    ProcessingLog        = ""
    ProductName          = Number_R_
    //10nOV2014 Quantity             = Unidades_
    Quantity             = 1
    IntegrationStatus    = ""
    Status               = ""	
    SubStatus            = ""
    TicketNumber         = TicketNumber
    TransactionChannel   = "Store"
    TransactionDate      = Business_Date
    TransactionSubType   = "Product"
    TransactionType      = "Accrual"
    VoucherNumber        = ""
    VoucherQty           = "1"
    VoucherType          = ""
    Organization         = "BKCASUAL"
    Estatus              = "PEN"
    Bussiness_date       = Business_Date
    Centroconsumo        = @RVC
    Cheque               = Cheque_
    Transmitido          = "F"
    Valor_Item_Redimido  = 0
EndSub
Sub Verifica_Descuento_Aplicado_Cheque_Wow
    Var I : N4 = 0
    //WaitForEnter "Inicio"
    For I = 1 to @NumDTLT
        If @dtl_type[I] = "S" and @dtl_Is_Void[I] = 0
           //07Jul2015 If @DTL_OBJECT[I] = 39 Or @Dtl_Objnum[I] = 39 Or @DTL_OBJECT[I] = 40 Or @Dtl_Objnum[I] = 40
           If    Mid(@DTL_NAME[I],1,3) = Prefijo_Aportaciones
              //WaitForEnter @TNDTTL   // 
              //WaitForEnter @TTLDUE   // 
              If @TNDTTL < @TTLDUE
                 ExitWithError "PAGO DE APORTACIONES CON PUNTOS DEBE SER TOTAL, NO PARCIAL"
              EndIf
           EndIf
        EndIf
    EndFor
EndSub
//18Ene2015

//19Ene2015
Sub Asigna_Ckid
    IF Len(Trim(@Ckid)) = 0
       Format Ticket_Unico as @WSID{01}, @CKNUM{04}, Mid(@Chk_Open_Time, 10, 2) , Mid(@Chk_Open_Time, 13, 2)
       CLEARCHKID
       @Ckid = ( Ticket_Unico )
    ENDIF
EndSub
Sub Rutina_Tmed_Facturacion_Electronica
    Prompt "Generando Ticket"
    Format Ticket_Unico as @WSID{01}, @CKNUM{04}, Mid(@Chk_Open_Time, 10, 2) , Mid(@Chk_Open_Time, 13, 2)
    If Len(Trim(@Ckid)) = 0
       CLEARCHKID
       @Ckid          = ( Ticket_Unico )
    EndIf
    Monto_Original = ( @TNDTTL )
    TTL001         = ( @Ttldue - @TNDTTL )
    Monto_Original = ( @TNDTTL )
   
    If TTL001 <= 0 
       Prompt "Inicia Proceso Inserta Reg"
       IF TTL001         <    0
          Monto_Original = ( @Ttldue )
       ENDIF
       Call Version_Micros_Sybase
       Call Trae_Status_Facturable_FPago_Repositorio
       Call Verifica_FPago_Facturable
       ClearChkInfo
       Format Campo as "Edofacturacion:", Facturacion_Parcial
       SaveChkInfo Campo
       Format Campo as "EdofacturacionO:", Facturacion_Parcial_Real
       SaveChkInfo Campo
    EndIf
EndSub

Sub Trae_Status_Facturable_FPago_Repositorio
    Call Conecta_Base_Datos_Facturacion
    Call Version_Micros_Sybase
    ClearArray FPago_Status_Facturable
    Format SqlStr as "SELECT id_fpago_micros, Facturable FROM facturacion.cfd_cfg_FPago"
    IF Version_Micros = 32 OR Version_Micros = 41 or Version_Micros = 47
       DLLCALL_CDECL hODBCDLL_CFD, sqlGetRecordSet(SqlStr)
       SqlStr1 = ""
       DLLCALL_CDECL hODBCDLL_CFD, sqlGetLastErrorString(ref SqlStr1)
//       IF (SqlStr1 <> "")
//           Call UnloadDLL_Facturacion
//           ExitWithError "Error 21: ", Mid(SqlStr1, 1, 15)
//       ENDIF
       SqlStr1 = ""
       DLLCALL_CDECL hODBCDLL_CFD, sqlGetFirst(ref SqlStr1)
    ELSEIF Version_Micros = 31
       DLLCall hODBCDLL_CFD, sqlGetRecordSet(SqlStr)
       SqlStr1 = ""
       DLLCall hODBCDLL_CFD, sqlGetLastErrorString(ref SqlStr1)
       IF SqlStr1 <> ""
          Call UnloadDLL_Facturacion
          ExitWithError "Error 22: ", Mid(SqlStr1, 1, 15)
       ENDIF
       SqlStr1 = ""
       DLLCall hODBCDLL_CFD, sqlGetFirst(ref SqlStr1)
    ENDIF
    WHILE Len(Trim(SqlStr1)) > 0
          //TEST Window 2,78
                 //TEST Display 1,1, Mid(SqlStr1,1,70)
          //TEST WaitForEnter
          //TEST WindowClose
          Split SqlStr1, ";", Numero_F, Cuenta_F
          //TEST WaitForEnter Numero_F
          //TEST WaitForEnter Cuenta_F
          FPago_Status_Facturable[Numero_F] = Cuenta_F
          //TEST WaitForEnter FPago_Status_Facturable[Numero_F]
          DLLCALL_CDECL hODBCDLL_CFD, sqlGetNext(ref SqlStr1)
    ENDWHILE
    Call Cierra_Base_Datos_Facturacion
EndSub
Sub Conecta_Base_Datos_Facturacion
    Call Load_ODBC_DLL_Facturacion
    Call ConnectDB_Facturacion
EndSub
Sub Load_ODBC_DLL_Facturacion
    IF hODBCDLL_CFD <> 0
       hODBCDLL_CFD  = 0
    ENDIF
    IF hODBCDLL_CFD = 0
        IF Version_Micros = 32 OR Version_Micros = 41 or Version_Micros = 47
           IF @WSTYPE = 3
              DLLFree hODBCDLL_CFD
              DLLLoad hODBCDLL_CFD, "\cf\micros\bin\MDSSysUtilsProxy.dll"
           ELSE
              DLLFree hODBCDLL_CFD
              DLLLoad hODBCDLL_CFD, "..\bin\MDSSysUtilsProxy.dll"
           ENDIF
        ELSEIF Version_Micros = 31
           DLLLoad hODBCDLL_CFD, "SimODBC.DLL"
        ENDIF
    ENDIF
    IF     hODBCDLL_CFD = 0 and Version_Micros = 32
       ExitWithError "No se Puede Cargar DLL (MDSSysUtilsProxy.DLL  [CFD])"
    ELSEIF hODBCDLL_CFD = 0 and Version_Micros = 41
       ExitWithError "No se Puede Cargar DLL (MDSSysUtilsProxy.DLL  [CFD])"
    ELSEIF hODBCDLL_CFD = 0 and Version_Micros = 31
       ExitWithError "No se Puede Cargar DLL (SimODBC.DLL  [CFD])"
    ENDIF
EndSub
Sub ConnectDB_Facturacion
    Var Status_cfd : N1
    IF Version_Micros = 32
       DLLCALL_CDECL hODBCDLL_CFD, sqlIsConnectionOpen(ref Status_cfd)
       IF Status_cfd =  0
          DLLCALL_CDECL hODBCDLL_CFD, sqlInitConnection("cfd_facturacion","ODBC;UID=dba;PWD=cfdmicros3700v31", "")
       ENDIF
    ELSEIF Version_Micros = 41
       DLLCALL_CDECL hODBCDLL_CFD, sqlIsConnectionOpen(ref Status_cfd)
       IF Status_cfd =  0
          DLLCALL_CDECL hODBCDLL_CFD, sqlInitConnection("cfd_facturacion","ODBC;UID=dba;PWD=cfdmicros3700v31", "")
       ENDIF
    ELSEIF  Version_Micros = 31
       DLLCall hODBCDLL_CFD, sqlIsConnectionOpen(ref Status_cfd)
       IF Status_cfd =  0
          DLLCall hODBCDLL_CFD, sqlInitConnection("cfd_facturacion","UID=dba;PWD=cfdmicros3700v31")
       ENDIF
    ENDIF
EndSub
Sub Cierra_Base_Datos_Facturacion
    Call UnloadDLL_Facturacion
EndSub
Sub UnloadDLL_Facturacion
    IF Version_Micros = 32
       DLLCALL_CDECL hODBCDLL_CFD, sqlCloseConnection()
    ELSEIF Version_Micros = 31
       DLLCall hODBCDLL_CFD, sqlCloseConnection()
    ELSEIF Version_Micros = 41
       DLLCALL_CDECL hODBCDLL_CFD, sqlCloseConnection()
    ENDIF
    DLLFree  hODBCDLL_CFD
    hODBCDLL_CFD = 0
EndSub
Sub Verifica_FPago_Facturable
    Var Con_1   : N3  = 0
    Var Objecto : N8 = 0
    Suma_Total_Facturar   = 0
    Suma_Total_No_Factura = 0
    For Con_1 = 1 to @NUMDTLT
        IF @DTL_TYPE[Con_1]      = "T" and @DTL_IS_VOID[Con_1] = 0
           Objecto = @DTL_OBJECT[Con_1]
           IF FPago_Status_Facturable[Objecto] = 1
              Suma_Total_Facturar = ( Suma_Total_Facturar + @DTL_TTL[Con_1] )
           ELSEIF @DTL_TTL[Con_1] > 0
              Suma_Total_No_Factura = ( Suma_Total_No_Factura + @DTL_TTL[Con_1] )
           ENDIF
        ENDIF
    EndFor
      Objecto = @TMDNUM
      //NEW IF FPago_Status_Facturable[Objecto] = 1
         //NEW Suma_Total_Facturar = ( Suma_Total_Facturar + @TNDTTL )
      //NEW ELSEIF @TNDTTL > 0
         //NEW Suma_Total_No_Factura = ( Suma_Total_No_Factura + @TNDTTL )
      //NEW ENDIF
      IF Objecto > 0 
         IF FPago_Status_Facturable[Objecto] = 1
            Suma_Total_Facturar = ( Suma_Total_Facturar + Monto_Original )
         ELSEIF Monto_Original > 0
            Suma_Total_No_Factura = ( Suma_Total_No_Factura +Monto_Original )
         ENDIF
      ENDIF
    //NEW IF Suma_Total_Facturar > 0
    IF ( Suma_Total_Facturar + Suma_Total_No_Factura ) > 0
       //NEW Neto  = ( ( Suma_Total_Facturar + Suma_Total_No_Factura ) / 1.16 )
       //NEW Iva   = ( ( Suma_Total_Facturar + Suma_Total_No_Factura ) - Neto )
       //NEW Total = ( Neto                + Iva  )
       //Facturacion_Parcial      = 4
    //NEW ELSEIF Suma_Total_Facturar = 0
       //Facturacion_Parcial      = 5
    //NEW ENDIF
    ENDIF
		IF Suma_Total_No_Factura     > 0 and Suma_Total_Facturar > 0
       Facturacion_Parcial       = 5
       Facturacion_Parcial_Real  = 4
    ELSEIF Suma_Total_No_Factura > 0 and Suma_Total_Facturar = 0
       Facturacion_Parcial       = 5
       Facturacion_Parcial_Real  = 5
    ELSEIF Suma_Total_No_Factura = 0 and Suma_Total_Facturar > 0
       Facturacion_Parcial       = 1
       Facturacion_Parcial_Real  = 2
    ENDIF
    //NEW WaitForEnter 
    //WaitForEnter Suma_Total_Facturar
    //WaitForEnter Suma_Total_No_Factura
EndSub
//19Ene2015

//20Ene2015
Event Inq: 72   //Tecla Void Micros
	Var i_count_disc : N4 = 0
	var gDescCupon :N1= 0 //Cupones 14-03-2016
	var gCuponProducto:N1=0
	var gItemLigado:N7=0
	var gDescPorcen:N7=0
	var gDescValor:N7=0
	var gFormaPago:N7=0
	var gProductoLig:A100=""
	var gFamiliaLig:A100=""
	Var gBorraCupon:N1=0
	var gEstaLigCupon:N1=0
	var gEsCupon:A1="N"
	var gHayFormadePago:N1=0 //28/04/2016
	var gFPSelect:N1=0
	var gTipoPromo:N1=0
	var gFIIDPROMO:N1=0
	var gFolioCupon:A10=""
	For i_count_disc = 1 to @numdtlt
		if mid(@dtl_name[i_count_disc],1,10) = "Puntos WOW" 
			ExitWithError "NO SE PERMITE BORRAR ARTICULOS O FORMA DE PAGOS" 
		endif
	endfor
  call ValFormaPago
	  if gFPSelect=0 and gHayFormadePago=1
	     ExitWithError "No se permite borrar cupones con forma de pago" 
	  EndIf
      Var Key_Void  : Key
      Call Lee_Configuracion_CRM
      Call Verifica_Item_Seleccionado_Void
      Key_Void = Key(1,458753)
      If Flag_Descuento_Wow_Void = 0 And Flag_FPago_Wow_Void = 0
	  //Validacion de cupon a Borrar******
        call ValidaCuponSelec 
	     if gDescCupon=1
		    
    		call UpdateCuponPos(1,gTipoPromo)
		 else   
		  call ValDatLigCupon	 
		  if  gEstaLigCupon=1
		     exitwitherror "Producto/Familia ligada a cupon"
		  Endif
		endif 
		//*********************************
         LoadKyBdMacro Key_Void  //Void Key
      Else
      	 InfoMessage "VOID WOW", "NO SE PERMITE ESTA OPERACION (WOW)"
      EndIf
EndEvent
//*******************************************************************************//
//                                   VALIDA CAMBIOS EN EL DETALLE DEL CHEQUE                         //  
//   Modificacion VICTOR
//                                                    //
//                                                                               //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
Event Dtl_Changed
    var gDescCupon :N1= 0 //Cupones 14-03-2016
	var gCuponProducto:N1=0
	var gItemLigado:N7=0
	var gDescPorcen:N7=0
	var gDescValor:N7=0
	var gFormaPago:N7=0
	var gProductoLig:A100=""
	var gFamiliaLig:A100=""
	Var gBorraCupon:N1=0
	var gEstaLigCupon:N1=0
	var gEsCupon:A1="N"
	var gHayFormadePago:N1=0 //28/04/2016
	var gFPSelect:N1=0
	var gTipoPromo:N1=0
	var gFIIDPROMO:N1=0
	var gFolioCupon:A10=""
    call ConsdatosLig(2)
	if gCuponProducto=1
		 call BuscarCupon
		 call Consultapromo
		 if gTipoPromo=gFIIDPROMO
			call UpdateCuponPos(2,gFIIDPROMO)
		 else  	  
			Call UpdateCuponPos(3,gFIIDPROMO)
		 Endif
	EndIf
EndEvent

Sub Verifica_Item_Seleccionado_Void
    Var I              : N4 = 0
    Flag_Descuento_Wow_Void = 0
    Flag_FPago_Wow_Void     = 0
    For I = 1 To @NumDTLT
    //WaitForenter @DTL_OBJECT[I]
    //WaitForenter @Dtl_Objnum[I]
        If @dtl_Selected[I] = 1
           If     @dtl_type[I] = "D" and @dtl_Is_Void[I] = 0
                  If @DTL_OBJECT[I] = Descuento_RedondeoWOW   Or @Dtl_Objnum[I] = Descuento_RedondeoWOW   Or \\
                     @DTL_OBJECT[I] = Descuento_Redenciones_P Or @Dtl_Objnum[I] = Descuento_Redenciones_P Or \\
                     @DTL_OBJECT[I] = Descuento_Redenciones_M Or @Dtl_Objnum[I] = Descuento_Redenciones_M
                     Flag_Descuento_Wow_Void = 1
                  EndIf
           ElseIf @dtl_type[I] = "T" and @dtl_Is_Void[I] = 0
                  If @DTL_OBJECT[I] = Tender_Crm   //23Ene2015 Or @Dtl_Objnum[I] = Tender_Crm
                     Flag_FPago_Wow_Void = 1
                  EndIf
           EndIf
        EndIf
    EndFor
EndSub
//20Ene2015

//22Ene2015
Sub Busca_Aportacion_Acreditaciones_Soa_Only
    Var I               : N4  = 0
    Var Buscar          : N8  = 0
    Total_Aportaciones        = 0
    For I = 1 to @NUMDTLT
        If @DTL_TYPE[I]   = "S" AND @DTL_IS_VOID[I] = 0
           Buscar = @DTL_OBJECT[I]
           Call Buca_ServiceCharge_Donacion(Buscar, Flag_Busca_Donacion)
           //27Ene2015 WaitForEnter Flag_Busca_Donacion  //T
           If Flag_Busca_Donacion = "T"
              Total_Aportaciones = ( Total_Aportaciones + @DTL_TTL[I] )
           Else
           	  //26Ene2015 Buscar = @Dtl_Objnum[I]
              //26Ene2015 Call Buca_ServiceCharge_Donacion(Buscar, Flag_Busca_Donacion)
              //26Ene2015 If Flag_Busca_Donacion = "T"
                 //26Ene2015 Total_Aportaciones = ( Total_Aportaciones + @DTL_TTL[I] )
              //26Ene2015 EndIf
           EndIf
           //22Ene2015 If @DTL_OBJECT[I] = 39 Or @DTL_OBJECT[I] = 40 Or @Dtl_Objnum[I] = 39 Or @Dtl_Objnum[I] = 40
              //22Ene2015 Total_Aportaciones = ( Total_Aportaciones + @DTL_TTL[I] )
           //22Ene2015 EndIf
        EndIf
    EndFor
EndSub
Sub Buca_ServiceCharge_Donacion(Ref Buscar_, Ref Flag_Busca_Donacion_)
    Var I : N2 = 0
    Flag_Busca_Donacion_ = ""
    For I = 1 To CC_Donaciones
        If Donaciones[I] = Buscar_
           Flag_Busca_Donacion_ = "T"
           Return
        EndIf
    EndFor
EndSub
//22Ene2015

//23Ene2015
Sub Suma_MenuItem_Soa_Only
    Var I    : N4  = 0
    Total_MenuItem = 0
    For I = 1 to @NUMDTLT
        If @DTL_TYPE[I]   = "M" AND @DTL_IS_VOID[I] = 0
           Total_MenuItem = ( Total_MenuItem + @DTL_TTL[I] )
        EndIf
    EndFor
EndSub
//23Ene2015

//24Ene2015
Sub Verifica_Tmed_Soa_Only
    Var I    : N4  = 0
    For I = 1 to @NUMDTLT
        If @DTL_TYPE[I]   = "T" AND @DTL_IS_VOID[I] = 0
           //WaitForEnter @DTL_OBJECT[I] //correcto
           //WaitForEnter @Dtl_Objnum[I] //seq
           //WaitForEnter @DTL_NAME[I]   //name
        EndIf
    EndFor
EndSub
//24Ene2015

//26Ene2015
Sub Busca_Aportacion_Redencion_Soa
    Var I               : N4  = 0
    Var Cons_Item_R     : N1  = 0
    //WaitForEnter "Busca_Aportacion_Redencion_Soa"
    //WaitForenter Prefijo_Aportaciones
    For I = 1 to @NUMDTLT
        If @DTL_TYPE[I]   = "S" AND @DTL_IS_VOID[I] = 0
           //07Jul2015 If    @DTL_OBJECT[I] = 39 Or @DTL_OBJECT[I] = 40 //26Ene2015 Or @Dtl_Objnum[I] = 39 Or @Dtl_Objnum[I] = 40
           If    Mid(@DTL_NAME[I],1,3) = Prefijo_Aportaciones
                 SNivel_R     = Nivel_Price[@MLVL[I]]
                 Price_R      = @DTL_TTL[I]
                 Number_R     = @DTL_OBJECT[I]
                 Nombre_R     = @DTL_NAME[I]
                 Unidades_R   = @DTL_QTY[I]
                 Cheque_R     = @CKNUM
                 Linea_Cheque = I
                 SNivel_R     = "NA"
                 Unidades_R   = 0 //27Ene2015
                 Opcion_Redencion      = 1
                 VoucherProductName[1] = ""
                 AdjustedListPrice     = "0"
                 Flag_Final_Tender     = "AF"
                 
                 //WaitForEnter "Aportacion Redencion II"
                 
                 Format TicketNumber as Fecha_Numerica{04}, Tienda{05}, Ticket_Unico
                 //27Ene2015 Call Guarda_Campos_Acreditaciones_Redenciones_Aportacion(SNivel_R, Price_R, Nombre_R, Unidades_R, Cheque_R, Number_R)
                 Call Guarda_Campos_Acreditaciones_Redenciones_Aportacion(SNivel_R, Price_R, Nombre_R, Unidades_R, Cheque_R, Number_R, Name_FP)  //27Ene2015
                 //26Ene2015 Call Arma_Registro_Acreditaciones_Redenciones
                 Call Formatea_Datos_Redenciones
                 //26Ene2015 Call Inserta_Solicitud_Acreditacion_Redencion
                 Call Manda_Redemption
                 Call Valida_Redemption
           EndIf
           //Lineas_Acreditas_Contador = ( Lineas_Acreditas_Contador + 1 )
        EndIf
    EndFor
EndSub
//26Ene2015

//27Ene2015
Sub Manda_Redemption_Miembro_O
    Manda = ""
    Format Manda, Separador as "RED", Arma_Cadena_82, Nombre_Miembro_O
    TxMsg Manda
    GetRxmsg "Espere... Cliente WOW"
EndSub

Sub Guarda_Campos_Acreditaciones_Redenciones_Aportacion(Ref SNivel_ , Ref Price_, Ref Number_, Ref Unidades_, Ref Cheque_, Ref ItemNumber_, Ref Name_FP_ )
    Var V_Qty  : A1 = ""
    //WaitForEnter Item_Precio_Seleccionado
    
    //26Ene2015
    If Trim(Number_) = "WOW Ticket"
       Unidades_ = 0
    EndIf
    
    //27Ene2015 
    Quantity = Unidades_
    //27Ene2015 
      
    If Flag_Final_Tender = "AF"
       V_Qty     = ""
    Else
    	 V_Qty     = "1"
    EndIf
    //waitforenter Flag_Final_Tender  //AF
    //waitforenter V_Qty              //bco
    
    //26Ene2015
    
    MemberNumber         = NumeroMiembro
    ID                   = "NUEVO"
    ALSELevelPrice       = SNivel_
    //08Ene2015 BranchDivision       = "SBX"
    //08Ene2015 ALSEPaymnetMode      = "SBUX Card"
    BranchDivision       = "BKCASUAL"
    ALSEPaymnetMode      = Trim(Name_FP_)   //27Ene2015 "Puntos WOW"
    ActivityDate         = Business_Date
    AdjustedListPrice    = Price_   //08Jul2015 
    Amount               = "0"	//Price_ IVAN
    Comments             = ""
    ItemNumber           = ItemNumber_
    LocationName         = Tienda
    PartnerName          = PartnerName
    PointName            = ""  //"Star"
    Points               = "0"  //"0"
    ProcessDate          = Business_Date
    ProcessingComment    = ""
    ProcessingLog        = ""
    ProductName          = "Aportacion"  //08Jul2015   Number_ // "22001" // 
    Quantity             = Unidades_
    IntegrationStatus    = ""
    Status               = ""	
    SubStatus            = ""
    TicketNumber         = TicketNumber
    TransactionChannel   = "Store"
    TransactionDate      = Business_Date
    TransactionSubType   = "Product"
    TransactionType      = "Redemption"
    VoucherNumber        = ""  //VoucherTransactionId[Opcion_Redencion]
    VoucherQty           = V_Qty
    VoucherType          = VoucherProductName[Opcion_Redencion]  //Aportacion = debe ir vacio
    Organization         = "BKCASUAL"
    Estatus              = "PEN"
    Bussiness_date       = Business_Date_Real
    Centroconsumo        = @RVC
    Cheque               = Cheque_
    Transmitido          = "F"
EndSub
//27Ene2015

//04Feb2015
Sub Busca_Points_Cheque
    Var Camp_01   : A20 = ""
    Var Camp_02   : A20 = ""
    For I = 1 to @NumDtlT
        If @dtl_type[I] = "I" And Len(Trim(@DTL_NAME[I])) > 0
           //21Jul2015 If Mid(Trim(@DTL_NAME[I]),1,5) = "Point"
           If Mid(Trim(@DTL_NAME[I]),1,5) = "Pesos"
              Format Points_Cheque_A As Mid(@DTL_NAME[I], 9, 10)
              Points_Cheque = Points_Cheque_A
              
              //28Jul2015
              Split @DTL_NAME[I], ":", Camp_01, Camp_02
              //WaitForEnter Camp_01  //
              //WaitForEnter Camp_02  //
              Points_Cheque = Camp_02
              //WaitForEnter Points_Cheque
              //28Jul2015
              
           EndIf
        EndIf 
    EndFor
EndSub
//04Feb2015

//06Feb2015
Sub Busca_Producto_Promocional_Cheques
    For I = 1 to @NUMDTLT
        If @DTL_TYPE[I]  = "M" AND @DTL_IS_VOID[I] = 0
           //02Jun2015 Objeto_Buscar = @DTL_OBJECT[I]
           Objeto_Buscar = @DTL_FAMGRP_OBJNUM[I]
           //02Jun2015 WaitForEnter Objeto_Buscar
           //02Jun2015 Call Consulta_Producto_Promocional(Objeto_Buscar)
           //02Jun2015 WaitForenter Objeto_Buscar            //6012
           //02Jun2015 WaitForenter No_Producto_Promocional  //7592 ref2
           //02Jun2015 Call Verifica_Si_Aplica_Promocion(No_Producto_Promocional)
           Call Verifica_Si_Aplica_Promocion(Objeto_Buscar)
        EndIf 
    EndFor
EndSub

Sub Verifica_Si_Aplica_Promocion(Ref Verifica_)
    Var IO : N4 = 0
    //WaitForenter CC_Promocional  //2
    //WaitForenter Verifica_       //6592
    For IO = 1 to CC_Promocional
        //WaitForenter Producto_Promocional[IO]  //
        If Producto_Promocional[IO] = Verifica_
           InfoMessage "ARTICULO(S) PROMOCIONAL(ES)", "PROMOCION NO PERMITIDA CON ESTE(OS) ARTICULO(S)"
           ExitCancel
        EndIf
    EndFor
EndSub

Sub Consulta_Producto_Promocional(Ref Promocional_)
    No_Producto_Promocional = 0
    Call Version_Micros_Sybase
    Call Conecta_Base_Datos_Sybase
    Format SqlStr as "SELECT ISNULL(cross_ref2,' ') as Ref2 FROM micros.mi_def WHERE obj_num = ", Promocional_
    IF Version_Micros = 32 or Version_Micros = 41
       DLLCALL_CDECL hODBCDLL, sqlGetRecordSet(SqlStr)
       SqlStr1 = ""
       DLLCALL_CDECL hODBCDLL, sqlGetLastErrorString(ref SqlStr1)
       IF (SqlStr1 <> "")
           Call Muestra_Error(SqlStr1)
           Call UnloadDLL_SyBase
           ExitCancel
       ENDIF
       SqlStr1 = ""
       DLLCALL_CDECL hODBCDLL, sqlGetFirst(ref SqlStr1)
    ENDIF
    If Len(Trim(SqlStr1))      = 0
       No_Producto_Promocional = 0
    Else
       Split SqlStr1, ";", No_Producto_Promocional
    EndIf
    Call UnloadDLL_SyBase
EndSub

//COMBOS
Sub Inserta_Combo_Count
	Var counter : N3 = 1 
	Call Version_Micros_Sybase
	//WaitForEnter 
	//WaitForEnter "ConnectDB"
	CALL ConnectDB
	//WaitForEnter "Query_Business_Date"
	CALL Query_Business_Date
	//WaitForEnter "For"
	for counter = 1 to @NUMDTLT Step 1 
		if ( @dtl_is_combo_parent[counter] ) 
		  //WaitForEnter "dtl_is_combo_parent"
			v_mi_seq_cmb=@Dtl_Sequence[counter]
		endif
		if ( @dtl_is_combo_side[counter] ) 
		   //WaitForEnter "dtl_is_combo_side"
		   //Errormessage "Side item of a combo, Detail: mi_seq ", @Dtl_Sequence[counter], " obj_num ",			@Dtl_Object[counter]
		   v_mi_seq = @Dtl_Sequence[counter]
		   v_obj_num=@Dtl_Object[counter]
		   v_menu_lvl = @Dtl_mlvl[counter]
		   v_qty = @Dtl_Qty[counter]
		   Call Insert_Combo	
		endif
		if ( @dtl_is_combo_main[counter] ) 
		   //WaitForEnter "dtl_is_combo_main"
		   //Errormessage "main item of a combo meal, Detail: mi_seq ", @Dtl_Sequence[counter], " obj_num ", @Dtl_Object[counter]
		   v_mi_seq = @Dtl_Sequence[counter]
		   v_obj_num=@Dtl_Object[counter]
		   v_menu_lvl = @Dtl_mlvl[counter]
		   v_qty = @Dtl_Qty[counter]
		   Call Insert_Combo
		endif
	endfor
	//WaitForEnter "DisconnetDB"
	CALL DisconnetDB

EndSub
  
Sub Insert_Combo
	SqlStrA=""
	Format SqlStrA as "INSERT INTO custom.combo_count (business_date,mi_seq,obj_num,mi_seq_cmb, menu_lvl,status, qty, rvc)  VALUES ('",v_business_date, "','",v_mi_seq,"','",v_obj_num,"','",v_mi_seq_cmb,"','",v_menu_lvl,"','",v_status,"','",v_qty,"','",@RVC,"')"
	DLLCALL_CDECL hODBCDLL, sqlExecuteQuery(SqlStrA)
EndSub
Sub Query_Business_Date
	SqlStrA=""
	Format SqlStrA as "SELECT business_date FROM micros.rest_status WITH (NOLOCK)"
	DLLCALL_CDECL hODBCDLL, sqlGetRecordSet(ref SqlStrA) 
	DLLCALL_CDECL hODBCDLL, sqlGetFirst(ref SqlStr1)
	Split SqlStr1, ";", v_business_date
EndSub
Sub ConnectDB
	Var constatus : N9
	DLLLoad hODBCDLL, "..\bin\MDSSysUtilsProxy.dll"
	DLLCALL_CDECL hODBCDLL, sqlIsConnectionOpen(ref constatus)
	DLLCALL_CDECL hODBCDLL, sqlInitConnection("micros","ODBC;UID=dba;PWD=Password1", "")
EndSub

Sub DisconnetDB
	DLLCALL_CDECL hODBCDLL, sqlCloseConnection()
	DLLFree  hODBCDLL
	hODBCDLL = 0
Endsub 
//COMBOS

//06Feb2015

//11FEB2015
//11FEB2015Include "Pms6_Pagatodo.Isl"
//11FEB2015

//29ABr2015
Event Inq: 63    ////Consulta de Miembro CRM Wow (Siebel) Pantalla  Old 33
      banderainfowow = 1// test 10/08/2015
	  Var Campo_VR    : A200 = ""
      Var Paso_01     : A30  = ""
      Consulta_Pantalla = "S"
      //WaitForEnter "Inq 63"
	  infoSocio = 1
      Call Rutina_Borra_Archivos_Temporales_Dll
      Call Rutina_Principal_Busca_Miembro
      Call Carga_Miembro_Cheque
	call Imprime_Ticket_info_socio_wow
	  TouchScreen Numero_TouchscreenNueva
      Input Paso_01, "Enter/Clear Para Continuar"
	 // call Delete_Miembro_Ocupa_Pos(Wsid_wow)
      WindowClose
	 
	 banderainfowow = 0// test 10/08/2015
EndEvent
//29Abr2015
Sub Imprime_Ticket_info_socio_wow // test 10/08/2015
startprint @CHK//// test 10/08/2015
		
		printline "~~bklogo1.bmp"
		printline "Gracias ", Mid(Nombre, 1, 24)
        printline "por ser socio WOW Rewards "
		printline Trim(Activo), "/", Trim(Nivel), "", Mid(BirthDate,1,5) 
        printline "Actualmente tienes: "
        printline "Points ", puntoswow
		printline "Pesos: ", pointAmount{10}
		
		
		
         
    endprint

endsub
//02Jun2015
Event Inq: 102   ///Limpiar de Miembro en Cheque
      Call Lee_Configuracion_CRM
      Call Verifica_Descuento_Tmed_Wow
      ClearChkInfo
      Call Rutina_Borra_Archivos_Temporales_Dll
      Call Rutina_Borra_Archivos_Temporales_Wow
EndEvent


Sub Rutina_Borra_Archivos_Temporales_Wow
    Call Borra_Miembro
    //Call Borra_Consulta_Saldo // Comentado el 14 ABR 2016
    //Call Borra_Privilegios_Nivel
    Call Borra_Privilegios_Nivel_Miembro
    
    Call Borra_Miembro_O
    Call Borra_Leyenda_CRM
    Call Borra_Linea_Item_Redimida
    Call Borra_Leyenda_CRM
    Call Borra_Ticket_Cancelar
    //Call Borra_Temporal // Comentado el 14 ABR 2016
    Call Borra_Consulta_Miembro_Screen
    CalL Borra_Off_Line_CRM
    Call Borra_Lee_Miembro_Solo
    
    Call Borra_Miembro_Servicio
    //Call Borra_Privilegios_Nivel_Servicio
    //Call Borra_Privilegios_Nivel_Miembro_Servicio
    //Call Borra_Consulta_Saldo_Servicio // Comentado el 14 ABR 2016
    
    Call Borra_Miembro
    //Call Borra_Privilegios_Nivel
    Call Borra_Privilegios_Nivel_Miembro
    //Call Borra_Consulta_Saldo // Comentado el 14 ABR 2016
    Call Borra_FileMiembro_Soa
    
    //03Jun2015
    Call Delete_Miembro_Ocupa_Pos(Wsid_wow)
    //03Jun2015
    
EndSub
Sub Verifica_Descuento_Tmed_Wow
    Var II   : N4 = 0
    For II = 1 to @NUMDTLT
        If @dtl_type[II] = "D" And @dtl_Is_Void[II] = 0 And @dtl_Ttl[II] <> 0
           If @DTL_OBJECT[II] = Descuento_Redenciones_P Or @DTL_OBJECT[II] = Descuento_Redenciones_M
              InfoMessage "WOW CRM", "NO SE PERMITE DESLIGAR TARJETA"
              ExitCancel
           EndIf
        ElseIf @dtl_type[II] = "T" And @dtl_Is_Void[II] = 0 And @dtl_Ttl[II] <> 0
           If @DTL_OBJECT[II] = Tender_Crm
              InfoMessage "WOW CRM", "NO SE PERMITE DESLIGAR TARJETA"
              ExitCancel
           EndIf
        EndIf
    EndFor
EndSub

Sub Conecta_Base_Datos_Sybase_Wow
    Call Version_Micros_Sybase
    Call Load_ODBC_DLL_SyBase
    Call ConnectDB_SyBase_Wow
EndSub
Sub ConnectDB_SyBase_Wow
    Var Status : N1
    IF Version_Micros = 32
       DLLCALL_CDECL hODBCDLL, sqlIsConnectionOpen(ref Status)
       IF Status =  0
          DLLCALL_CDECL hODBCDLL, sqlInitConnection("micros","ODBC;UID=dba;PWD=Password1", "")
       ENDIF
    ElseIF Version_Micros = 41
       DLLCALL_CDECL hODBCDLL, sqlIsConnectionOpen(ref Status)
       IF Status =  0
          DLLCALL_CDECL hODBCDLL, sqlInitConnection("micros","ODBC;UID=dba;PWD=Password1", "")
       ENDIF
    ElseIF  Version_Micros = 31
       DLLCall hODBCDLL, sqlIsConnectionOpen(ref Status)
       IF Status =  0
          DLLCall hODBCDLL, sqlInitConnection("micros","UID=dba;PWD=Password1")
       ENDIF
    ENDIF
EndSub
Sub Inserta_Miembro_Ocupa_Pos(Ref Numero_Tarjeta_, Ref Wsid_)
    Var Temp1    : $100  = 0

    Call Conecta_Base_Datos_Sybase_Wow
    
    Format SqlStr as "INSERT INTO micros.configuracion_Micros_Wow (numero_Pos, accion, datos ) VALUES ( "
    Format SqlStr as SqlStr, "  ", Wsid_,      ","
    Format SqlStr as SqlStr, " 'Miembro_Crm',"
    Format SqlStr as SqlStr, " '", Numero_Tarjeta_,   "' "
    Format SqlStr as SqlStr, " )"
    Call Graba_Archivo_datos(SqlStr)
    IF   Version_Micros = 41
       DLLCALL_CDECL hODBCDLL, sqlExecuteQuery(SqlStr)
       SqlStr1 = ""
       DLLCALL_CDECL hODBCDLL, sqlGetLastErrorString(ref SqlStr1)
       IF (SqlStr1 <> "")
           Call Graba_Archivo_datos(SqlStr1)
           InfoMessage "Error Miembro: ", Mid(SqlStr1,1,30)
       ENDIF
       SqlStr1 = ""
    ENDIF
    IF SqlStr1   = ""
    ELSE
       Split SqlStr1, ";", Temp1
    ENDIF
    Call UnloadDLL_SyBase
EndSub
Sub Delete_Miembro_Ocupa_Pos(Ref Wsid_)
    Var Temp1    : $100  = 0

    Call Conecta_Base_Datos_Sybase_Wow
    
    Format SqlStr as "DELETE micros.configuracion_Micros_Wow WHERE numero_Pos = ", Wsid_
    //03Jun2015 Format SqlStr as SqlStr, "  And datos = '", Numero_Tarjeta_, "'"

    Call Graba_Archivo_datos(SqlStr)
    IF   Version_Micros = 41
       DLLCALL_CDECL hODBCDLL, sqlExecuteQuery(SqlStr)
       SqlStr1 = ""
       DLLCALL_CDECL hODBCDLL, sqlGetLastErrorString(ref SqlStr1)
       IF (SqlStr1 <> "")
           Call Graba_Archivo_datos(SqlStr1)
           InfoMessage "Error Miembro: ", Mid(SqlStr1,1,30)
       ENDIF
       SqlStr1 = ""
    ENDIF
    IF SqlStr1   = ""
    ELSE
       Split SqlStr1, ";", Temp1
    ENDIF
    Call UnloadDLL_SyBase
	
EndSub
Sub Consulta_Miembro_Ocupa_Pos(Ref Numero_Tarjeta_) 
    Var File_No   : N6 = 0
    Call Conecta_Base_Datos_Sybase_Wow
    
    Format SqlStr as "SELECT numero_Pos, accion, datos FROM micros.configuracion_Micros_Wow Where datos = '", Numero_Tarjeta_, "'"
    Call Graba_Archivo_datos(SqlStr)
    IF Version_Micros = 32 or Version_Micros = 41
       DLLCALL_CDECL hODBCDLL, sqlGetRecordSet(SqlStr)
       SqlStr1 = ""
       DLLCALL_CDECL hODBCDLL, sqlGetLastErrorString(ref SqlStr1)
       IF (SqlStr1 <> "")
           Call Muestra_Error(SqlStr1)
           Call UnloadDLL_SyBase
           ExitCancel
       ENDIF
       SqlStr1 = ""
       DLLCALL_CDECL hODBCDLL, sqlGetFirst(ref SqlStr1)
    ElseIF Version_Micros = 31
       DLLCall hODBCDLL, sqlGetRecordSet(SqlStr)
       SqlStr1 = ""
       DLLCall hODBCDLL, sqlGetLastErrorString(ref SqlStr1)
       IF SqlStr1 <> ""
          Call Muestra_Error(SqlStr1)
          Call UnloadDLL_SyBase
          ExitCancel
       ENDIF
       SqlStr1 = ""
       DLLCall hODBCDLL, sqlGetFirst(ref SqlStr1)
    ENDIF
    IF SqlStr1 = ""
       numero_Pos = 0
       accion     = ""
       datos_wsid = ""
    Else
       Split SqlStr1, ";", numero_Pos, accion, datos_wsid
    EndIf
    Call UnloadDLL_SyBase
EndSub
Event Inq: 103  //Bloquea Pos
      Call Inserta_Miembro_Ocupa_Pos(Tarjeta_wow, Wsid_wow)
      //WaitForEnter "Inserto"
      Call Consulta_Miembro_Ocupa_Pos(Tarjeta_wow) 
      //WaitForEnter "Lectura"
      //WaitForEnter numero_Pos
      //WaitForEnter accion
      //WaitForEnter datos_wsid
      Call Delete_Miembro_Ocupa_Pos(Tarjeta_wow, Wsid_wow)
      //WaitForEnter "Delete"
EndEvent
//02Jun2015

//07Jul2015
Sub Consolidado_Descuentos_Acreditaciones_Soa_Wow_2
    Var I               : N4  = 0
    Var TTl_Descto_01   : $10 = 0
    Var TTl_Descto_02   : $10 = 0
    
    For I = 1 to @NUMDTLT
        If @DTL_TYPE[I] = "D" AND @DTL_IS_VOID[I] = 0
           Busca_Descto = @DTL_OBJECT[I]
           Call Verifca_Si_Existe_Descto(Busca_Descto, Linea_Encontrado_D)
           If Si_Existe_Descuento = "T"
              If @DTL_TTL[I] < 0
                 TTl_Descto_01 = ( @DTL_TTL[I] * -1 )
              Else
              	 TTl_Descto_01 = ( @DTL_TTL[I] )
              EndIf
              Precio_Descto[Contador_Descto]   = Precio_Descto[Contador_Descto]   + TTl_Descto_01
              Unidades_Descto[Contador_Descto] = Unidades_Descto[Contador_Descto] + @DTL_QTY[I]
           Else
              Contador_Descto = ( Contador_Descto + 1 )
              If @DTL_TTL[I] < 0
                 TTl_Descto_01 = ( @DTL_TTL[I] * -1 )
              Else
              	 TTl_Descto_01 = ( @DTL_TTL[I] )
              EndIf
              Objeto_Descto[Contador_Descto]   = @DTL_OBJECT[I]
              Nivel_Descto[Contador_Descto]    = Nivel_Price[@MLVL[I]]
              Precio_Descto[Contador_Descto]   = TTl_Descto_01
              Nombre_Descto[Contador_Descto]   = @DTL_NAME[I]
              Unidades_Descto[Contador_Descto] = @DTL_QTY[I]
              Linea_Descto[Contador_Descto]    = I
           EndIf
        EndIf
    EndFor

    For I = 1 to Contador_Descto
        Price_R       = Precio_Descto[Contador_Descto]
        Number_R      = Objeto_Descto[Contador_Descto]
        Nombre_R      = Nombre_Descto[Contador_Descto]
        Unidades_R    = Unidades_Descto[Contador_Descto]
        Cheque_R      = Cheque_Descto
        Linea_Cheque  = Linea_Descto[Contador_Descto]
        SNivel_R      = "NA"
        TTl_Descto_01 = Price_R
        TTl_Descto_02 = Price_R
        Call Consolidado_Descuento_Acreditacion_Descuento_RedondeoWOW_Wow_2(TTl_Descto_01, TTl_Descto_02 )
    EndFor
EndSub
Sub Consolidado_Descuento_Acreditacion_Descuento_RedondeoWOW_Wow_2(Ref TTl_Descto_01, Ref TTl_Descto_02 )
    If TTl_Descto_02 <> 0
       Format TicketNumber as Fecha_Numerica{04}, Tienda{05}, Ticket_Unico
       Cons_Item = ( Cons_Item + 1 )
       Call Guarda_Campos_Acreditaciones_Descuentos(SNivel_R , Price_R, Cons_Item, Unidades_R, Cheque_R, Number_R, Price_R, Nombre_R )
       Call Arma_Registro_Acreditaciones_Redenciones
       Call Inserta_Solicitud_Acreditacion_Redencion
       Lineas_Acreditas_Contador = ( Lineas_Acreditas_Contador + 1 )
    EndIf
EndSub
Sub Verifca_Si_Existe_Descto(Ref Buscar_, Ref Linea_Encontrado_D_)
    Var JJ : N3 = 0
    Si_Existe_Descuento = ""
    Linea_Encontrado_D_ = 0
    For JJ = 1 To Contador_Descto
        If Objeto_Descto[JJ] = Buscar_
           Si_Existe_Descuento = "T"
           Linea_Encontrado_D_ = Linea_Descto[JJ]
           Return
        EndIf
    EndFor
EndSub
//07Jul2015

//08Jul2015
Sub Guarda_Campos_Acreditaciones_Redenciones_Only_Aportaciones(Ref SNivel_ , Ref Price_, Ref Number_, Ref Unidades_, Ref Cheque_, Ref ItemNumber_ )
    Var V_Qty  : A1 = ""
    //WaitForEnter Item_Precio_Seleccionado
    
    //26Ene2015
    If Trim(Number_) = "WOW Ticket"
       Unidades_ = 0
    EndIf
    
    //27Ene2015 
    Quantity = Unidades_
    //27Ene2015 
      
    If Flag_Final_Tender = "AF"
       V_Qty     = ""
    Else
    	 V_Qty     = "1"
    EndIf
    //waitforenter Flag_Final_Tender  //AF
    //waitforenter V_Qty              //bco
    
    //26Ene2015
    
    MemberNumber         = NumeroMiembro
    ID                   = "NUEVO"
    ALSELevelPrice       = SNivel_
    //08Ene2015 BranchDivision       = "SBX"
    //08Ene2015 ALSEPaymnetMode      = "SBUX Card"
    BranchDivision       = "BKCASUAL"
    ALSEPaymnetMode      = "Puntos WOW"
    ActivityDate         = Business_Date
    AdjustedListPrice    = Price_  //08Jul2015 AdjustedListPrice
    Amount               = Price_	// IVAN
    Comments             = ""
    ItemNumber           = ItemNumber_
    LocationName         = Tienda
    PartnerName          = PartnerName
    PointName            = ""  //"Star"
    Points               = "0"  //"0"
    ProcessDate          = Business_Date
    ProcessingComment    = ""
    ProcessingLog        = ""
    ProductName          = "Aportacion"   //08Jul2015  Number_  // "22001" // 
    Quantity             = Unidades_
    IntegrationStatus    = ""
    Status               = ""	
    SubStatus            = ""
    TicketNumber         = TicketNumber
    TransactionChannel   = "Store"
    TransactionDate      = Business_Date
    TransactionSubType   = "Product"
    TransactionType      = "Redemption"
    VoucherNumber        = ""  //VoucherTransactionId[Opcion_Redencion]
    VoucherQty           = V_Qty
    VoucherType          = VoucherProductName[Opcion_Redencion]  //Aportacion = debe ir vacio
    Organization         = "BKCASUAL"
    Estatus              = "PEN"
    Bussiness_date       = Business_Date_Real
    Centroconsumo        = @RVC
    Cheque               = Cheque_
    Transmitido          = "F"
EndSub
//08Jul2015

//15Jul2015
Sub Verifica_Aportaciones_Aplicado_Cheque
    Var I : N4 = 0
    Existe_Aportacion = ""
    Existe_Menu_Item  = ""
    For I = 1 to @NumDTLT
        If @dtl_type[I] = "S" and @dtl_Is_Void[I] = 0
           If    Mid(@DTL_NAME[I],1,3) = Prefijo_Aportaciones
              Existe_Aportacion = "T"
           EndIf
        ElseIf @DTL_TYPE[I] = "M" AND @DTL_IS_VOID[I] = 0
           Existe_Menu_Item = "T"
        EndIf
    EndFor
EndSub
Sub Busca_Forma_en_Cheque
    Forma_Pago_en_Cheque = ""
    For I = 1 to @NumDTLT
        If @dtl_type[I] = "T" and @dtl_Is_Void[I] = 0
           Forma_Pago_en_Cheque = "T"
        EndIf
    EndFor
EndSub
//15Jul2015

//26Ago2015
Sub Verifica_Monto_Total_Cheque
    Var II                      : N3  = 0
    Var MontoTtlChequeDescuento : $12 = 0
    MontoTtlCheque = 0
    For II = 1 to @NUMDTLT
        If     @dtl_type[II] = "M" And @dtl_Is_Void[II] = 0 And @dtl_Ttl[II] <> 0
           MontoTtlCheque = ( MontoTtlCheque + @DTL_TTL[II] )
        ElseIf @dtl_type[II] = "D" And @dtl_Is_Void[II] = 0 And @dtl_Ttl[II] <> 0
           MontoTtlChequeDescuento = ( MontoTtlChequeDescuento + @DTL_TTL[II] )
        EndIf
    EndFor
    MontoTtlCheque = ( MontoTtlCheque + MontoTtlChequeDescuento )
Endsub
//26Ago2015
//NumeroMiembro_PR  = Pausa Recarga
//Soporte TI  ~12BkM3247
//tarj 37 ptos 128.99(cuenta)
//YA EXISTE TARJETA LIGADA A UN CHEQUE (EN ESTE MOMENTO)
//EMPLEADO 4,  741101 (Server)

sub Manda_Datos_Tarjeta_Offline  //Test 10/08/2015
	var Tecla_Descuento : key  //Test 10/08/2015
	Var IO         : N2  = 0
    Var No_tarjeta : A30 = "0"
    Var Campo_01_  : A80 = ""
    Var Campo_02_  : A80 = ""
    Var Campo_000  : A80 = ""
	
	TouchScreen 3057
	Window  1, 4, "WOW"
                DisplayMsInput 0,0,Clave_ID{-m2,*},"Deslize Tarjeta CRM WOW2:", \
                               0,0,Clave_ID_TrackI{-m1,*},"Deslize Tarjeta CRM WOW3:", \ //Test 10/08/2015
							   0,0,Clave_ID_TrackII{m2, 1, 1, *},"Deslize Tarjeta CRM WOW4:" //Test 10/08/2015
			
          WindowInput
          WindowClose

          If @MAGSTATUS = "Y"
             call Delete_Miembro_Ocupa_Pos(Wsid_wow) 
            metodo_entrada = "Tarjeta"

			Format Clave_ID_TrackII_Origin as Clave_ID_TrackII //test 10/08/2015
			mid(Clave_ID_TrackII,1,12) = "XXXXXXXXXXXX" //test 10/08/2015
			Format Campo as "", Clave_ID_TrackII  //test 10/08/2015
			Call Carga_Screen_Cheque
		
			Split Clave_ID, "=", Clave_ID_Paso1, Clave_ID_Paso2
             
             ClearArray Clave_Id_Track_II
             ClearArray Clave_Id_Track_II_1
             
             Split Clave_ID_TrackI, "^", Clave_Id_Track_II[1], Clave_Id_Track_II[2], Clave_Id_Track_II[3]
             If Len(Clave_Id_Track_II[2]) > 0
                Split Clave_Id_Track_II[2], "/", Clave_Id_Track_II_1[1], Clave_Id_Track_II_1[2], Clave_Id_Track_II_1[3]
             EndIf
			
             if Clave_Id_Track_II_1[3] = "ALSEAWOW" Or Clave_Id_Track_II_1[3] = "ALSE" // test 10/08/2015
                else
                    InfoMessage "Tarjeta no es WOW" //Test 10/08/2015          
                    ExitCancel
                endif // test 10/08/2015
                 
             If Mid(Clave_ID,1,1) = ";" And Mid(Clave_ID,16,1) = "?"
                Format No_tarjeta     As Mid(Clave_ID,2,14)
                Format Campo_Busqueda As No_tarjeta
             ElseIf Mid(Clave_ID,1,1) = ";" And Len(Clave_ID_Paso2) > 0
                Format No_tarjeta     As Mid(Clave_ID,2,16)
                Format Campo_Busqueda As No_tarjeta
             ElseIf Clave_Id_Track_II_1[3] = "ALSEAWOW" Or Clave_Id_Track_II_1[3] = "ALSE"
                Var Len1  : N4 = 0
                Len1 = ( Len(Clave_ID_Paso1) - 1 )
                Format No_tarjeta     As Clave_ID_Paso1
                Format Campo_Busqueda As No_tarjeta
             Else
                Format No_tarjeta     As Mid(Clave_ID,30,1)
                Format No_tarjeta     As No_tarjeta, Mid(Clave_ID,32,2)
                Format No_tarjeta     As No_tarjeta, Mid(Clave_ID, 7,1)
                Format No_tarjeta     As No_tarjeta, Mid(Clave_ID, 8,8)
                Format No_tarjeta     As No_tarjeta, Mid(Clave_ID,34,4)
                Format Campo_Busqueda As No_tarjeta
             EndIf
          Else
          metodo_entrada = "Smartphone" // test 10/08/2015
             Campo_Busqueda = Clave_ID
			 mid(Clave_ID,1,12) = "XXXXXXXXXXXX" //test 10/08/2015
			Format Campo as "", Clave_ID //test 10/08/2015
			Call Carga_Screen_Cheque
          EndIf
	Tecla_Descuento = Key(5,107)
	LoadKyBdMacro Tecla_Descuento, makekeys( Clave_ID_TrackII ),@KEY_ENTER //Test 10/08/2015
endsub //Test 10/08/2015

event inq: 1000  //Test 10/08/2015
		call Manda_Datos_Tarjeta_Offline //Test 10/08/2015
endevent  //Test 10/08/2015

Sub msgbox_BK
    Var Msj              : A80  = ""
    Var SqlString        : A500 = ""
	Var SqlResp          : A300 = ""
	Call Version_Micros_Sybase
    Call Conecta_Base_Datos_Sybase
    Format SqlString as "select ISNULL(addr_line_1,'') as MSJ from micros.rest_def;"
	  IF Version_Micros = 41
       DLLCALL_CDECL hODBCDLL, sqlGetRecordSet(SqlString)
       SqlResp = ""
       DLLCALL_CDECL hODBCDLL, sqlGetLastErrorString(ref SqlResp)
       IF (SqlResp <> "")
           Call Muestra_Error(SqlResp)
           Call UnloadDLL_SyBase
           ExitCancel
       ENDIF
       SqlResp = ""
       DLLCALL_CDECL hODBCDLL, sqlGetFirst(ref SqlResp)
       ElseIF Version_Micros = 31
       DLLCall hODBCDLL, sqlGetRecordSet(SqlString)
       SqlResp = ""
       DLLCall hODBCDLL, sqlGetLastErrorString(ref SqlResp)
       IF SqlResp <> ""
          Call Muestra_Error(SqlResp)
          Call UnloadDLL_SyBase
          ExitCancel
       ENDIF
       SqlResp = ""
       DLLCall hODBCDLL, sqlGetFirst(ref SqlResp)
    ENDIF	  
	  IF Len(Trim(SqlResp)) > 1 and Len(Trim(SqlResp)) <= 81 Then
       Split SqlResp, ";", Msj
		   InfoMessage "Mensaje", Msj
    EndIF
	  Call UnloadDLL_SyBase
EndSub