//ContinueOnCancel
DiscardGlobalVar
UseCompatFormat
UseISLTimeOuts
SetSignOnLeft
//########################################################
//                   ARCHIVOS INCLUIDOS EN EL ISL  
//########################################################
Include "Pms6_SubRutinas.Isl"
Include "ODBC_Cupones_SubRutinas.Isl"
//########################################################
//=====================================VARIABLES==================================================
Var MontoTtlCheque              : $12 = 0
Var pointAmount               : A10 = ""
Var Forma_Pago_en_Cheque         : A1  = ""
Var Existe_Aportacion           : A1  = ""
Var Existe_Menu_Item             : A1  = ""
Var Prefijo_Aportaciones         : A3  = "AP-"
Var Linea_Encontrado_D : N3 = 0
Var Contador_Descto  : N3 = 0
Var Si_Existe_Descuento : A1 = ""
Var Busca_Descto        : N8 = 0
Var Objeto_Descto[10]   : N8
Var Nivel_Descto[10]    : N8
Var Precio_Descto[10]   : $10
Var Nombre_Descto[10]   : A20
Var Unidades_Descto[10] : N6
Var Linea_Descto[10]    : N3
Var Wsid_wow                    : N6    = @WSID
Var Tarjeta_wow              : A20  = ""
Var Cupon_Desc_P1               : N6    = 0
Var Cupon_Desc_P2               : N6    = 0
Var Descuento_Valido            : $10   = 0
Var numero_Pos               : N6   = 0
Var accion                   : A20  = ""
Var datos_wsid               : A100 = ""
Var Ttl_Calculo                 : $10   = 0
Var Descuento_aplicar            : $10  = 0
Var Tipo_Sub_Descuento          : A1    = ""
Var v_business_date             : A30   = ""
Var v_mi_seq                    : A10   = ""
Var v_obj_num                   : A10   = ""
Var v_status                    : A10   = "I"
Var v_mi_seq_cmb                : A10   = "1"
Var SqlStrA                     : A5000 = ""
var v_menu_lvl                  : N1
var v_qty                       : N5
Var Points_Cheque_A             : A10   = ""
Var Points_Cheque               : $10   = 0 
Var Flag_Final_Tender           : A2    = ""
Var AdjustedListPrice           : A50   = ""
Var Opcion_Redencion            : N2    = 0 
Var Total_MenuItem              : $10   = 0
Var Flag_Busca_Donacion         : A1    = ""
Var Total_Aportaciones          : $10   = 0
Var Name_FP                     : A90   = ""
Var Tender_Efectivo_Ttl         : $10   = 0
Var Tender_Efectivo_No          : N6    = 0
Var Flag_Descuento_Wow_Void     : N1    = 0
Var Flag_FPago_Wow_Void         : N1    = 0
Var Nombre_Forma_Pago_Ws        : A20   = ""
Var No_Formas_Pago              : N2    = 0
Var Numero_F                    : N6    = 0
Var Cuenta_F                    : A10   = ""
Var Facturacion_Parcial         : N1    = 0
Var Facturacion_Parcial_Real    : N1    = 0
Var Monto_Original              : $10   = 0
Var FPago_Status_Facturable[100] : N1
Var Suma_Total_Facturar : $12   = 0
Var Suma_Total_No_Factura : $10 = 0
Var hODBCDLL_CFD        : N12   = 0
Var Descuento_RedondeoWOW : N6= 0
Var Donaciones[10]  : N6
Var PtsAniv_monto       : A10   = 0
Var PtsAniv_ProductName : A99   = ""
Var Name_Cajero     : A100= ""
Var Tender_Crm      : N6    = 0
Var Resp_Red[5]     : A50
Var ErrorCode           : A50   = ""
Var Id_Cheque           : A30   = ""
Var Numero_Tarjeta    : A30 = ""
Var Numero_Tarjeta_O    : A30   = ""
Var Datos_1          : A5000 = ""
Var Descuento_Calculado : $12   = 0
Var Descuento_P_Cal  : $10  = 0
Var Miembro_2[2]        : A100
Var Arma_Cadena_82    : A1000 = ""
Var Arma_Cadena_2       : A1000 = ""
Var ID                : A50 = ""
Var Salir_ACR           : A1    = ""
Var Condimento_Aplicado : A1    = ""
Var Marcado_Crm      : A1   = ""
Var Ticket_Unico        : A20   = ""
Var Fecha_a_Convertir   : A20   = ""
Var Fecha_Juliana_M  : A20  = ""
Var Fecha_Numerica    : N6  = 0
Var Saldo_Ws            : A30   = ""
Var Numero_Tarjeta__O   : A50   = ""
Var Nombre_Miembro_O    : A50   = ""
Var ALL                      : A4  = ""
Var Borrar                    : A1  = ""
Var Consulta_Pantalla           : A1  = ""
Var Error_Time_Out_SbuxCard  : A2  = ""
Var TicketNumber                : A50 = ""
Var TicketNumber_Off            : A50 = ""
Var G                           : A20 = ""
Var Salida_Consulta_Clientes    : A1    = ""
Var Lineas_Acreditas_Contador   : N2    = 0
Var IntegrationStatus           : A500 = ""
Var Status                    : A50  = ""   
Var SubStatus                   : A50  = ""
Var Estatus                  : A50  = ""
Var CC01                        : N3    = 0
Var Errores_Mostrar[12]      : A99
Var Leidos_Consulta          : N3   = 0
Var Fecha_Cumplanios_A        : A15 = ""
Var Fecha_Cumplanios_D        : A15 = ""
Var Clave_ID                    : A40   = ""
Var Clave_ID_TrackI          : A200  = ""
Var Clave_ID_Paso1            : A40 = ""
Var Clave_ID_Paso2            : A40 = ""
Var Clave_ID_TrackII_Origin     : A20 
Var Clave_ID_TrackII            : A20 
Var Clave_Id_Track_II[5]        : A40
Var Clave_Id_Track_II_1[5]    : A40
Var Campo_Busqueda            : A99 = ""
Var Saldo                       : A30   = ""
Var Manda                       : A2000 = ""
Var Respuesta                   : A5000 = ""
Var Separador                   : A1    = "|"
Var Nombre                    : A100  = ""
Var NumeroMiembro               : A100  = ""
Var NumeroMiembroSolo           : A100  = ""
Var Activo                    : A30 = ""
Var Nivel_Price[10]          : A10
    Nivel_Price[1]                    = "Kids"
    Nivel_Price[2]                    = "Chico"
    Nivel_Price[3]                    = "Mediano"
    Nivel_Price[4]                    = "Grande"
    Nivel_Price[5]                    = "King"
    Nivel_Price[6]                    = "NA"
    Nivel_Price[7]                    = "NA"
    Nivel_Price[8]                    = "NA"
    Nivel_Price[9]                    = "NA"
    Nivel_Price[10]                  = "NA"
Var Nivel                       : A20   = ""
Var Nivel_Id                    : A20   = ""
Var Nivel_nombre                : A50   = ""
Var Organizacion                : A10   = ""
Var FavoriteProduct          : A100  = ""
Var LastBeverage                : A100  = ""
Var LastFood                    : A100  = ""
Var AttributeValue            : A10 = ""
Var PromocionId[30]          : A10
Var PromocionNombre[30]      : A100
Var PromocionStatus[30]      : A24
Var PromocionUniqueKey[30]    : A24
Var PromocionDisplayName[30]    : A24
Var VoucherCalStatus[30]        : A34
Var VoucherProductId[30]        : A34
Var VoucherProductName[30]    : A70
Var VoucherStatus[30]           : A34
Var VoucherTransactionId[30]    : A34
Var validDiscountAmount[30]  : A34
Var validDiscountType[30]       : A34
Var minimumAmount[30]           : A20
Var Version_Micros            : A2  = 0
Var hODBCDLL                    : N12   = 0
Var Campo                       : A5000 = ""
Var Campo_                    : A100  = ""
Var SqlStr                    : A3000 = ""
Var SqlStr1                  : A3000 = ""
Var Business_Date               : A30   = ""
Var Business_Date_Real        : A40 = ""
Var Tienda                    : N6  = 0
Var DiaB                        : A2    = ""
Var MesB                        : A2    = ""
Var AnioB                       : A4    = ""
Var Fecha                       : A12   = ""
Var Contador_Campos          : N3   = 0
Var Contador_Voucher            : N3    = 0
Var I                           : N3    = 0
Var II                        : N4  = 0
Var Cheque                    : N4  = 0
Var BirthDate                   : A30   = ""
Var DiaBirthDate                : N2    = 0
Var MesBirthDate                : N2    = 0
Var AnioBirthDate               : N4    = 0
Var AnioBirthDate_A          : A4   = ""
Var Numero_TouchscreenN      : N6   = 0
Var PartnerName              : A100  = ""
Var Name_Jar                    : A20   = ""
Var Descuento_Redenciones_P  : N6   = 0
Var Descuento_Redenciones_M  : N6   = 0
Var FP_Sbux_Aplicado_Off_Line   : A1    = ""
Var Unid_Seleccionado           : N10   = 0
Var Item_No_Seleccionado        : N10   = 0
Var Item_Nombre_Seleccionado    : A20   = ""
Var Item_Precio_Seleccionado    : $10   = 0
Var Item_Qty_Seleccionado       : N4    = 0
Var Item_Linea_Seleccionado  : N3   = 0
Var Contador_Redenciones        : N3    = 0
Var Total_Fpago_Cheque        : $10 = 0
Var Condimentos              : N3   = 0
Var MenuItems                   : N3    = 0
Var Lineas_Leidas_Redimidas  : N3   = 0
Var Cheque_Leido                : N4    = 0
Var Linea_Redimida_Encontrada   : A1    = ""
Var Linea_Cheque                : N3    = 0
Var Tipo_Descuento            : A1  = ""
Var Tipo_Descuento_Valor_P    : $10 = 0
Var Tipo_Descuento_Valor_M    : $10 = 0
Var LifetimePoint1Value      : N6   = 0
Var LifetimePoint2Value      : N6   = 0
Var Arma_Cadena_22            : A2000 = ""
Var CC_Donaciones               : N2    = 0
Var Contador_Lineas_Pantalla    : N2    = 0
Var Linea_Redimida[100]      : N3
Var Redencion_Aplicada[12]    : A20
Var RewardName_A[200]           : A50
Var RewardTypeDiscount_P_M_A[200]: A50
Var RewardPoints[200]           : N3

//===DevWorms - JMG
Var No_Tarjeta_WOW              : A30 
Var No_Tarjeta_WOW_anterior   : A30
Var LeyendaCRM_WOW[10]          : A31
Var Miembro_O_Archivo           : A99
Var rspst_Miembro               : A5000 
Var Cheque_Miembro              : N4    
Var rspst_Miembro_Soa           : A5000 
Var Cheque_Miembro_Soa          : N4
Var rspst_Miembro_PR             : A5000 
Var Cheque_Miembro_PR           : N4
Var Redencion_Aplicada_MNM[12]  : A20
Var Contador_Redenciones_MNM     : N3
Var FileOff_Line_CRM_string   : A99
Var Leyenda_CRM_Cheque_Arreglo[15] : A45
Var Chq[100]                     : N4
Var Linea_Red[100]              : N3
var infoSocio :N1  
var offline :N1  
var puntosWOW_Global : $20 
var metodo_entrada : a20 
var entra_puntos_primera_vez : n1 
Var puntosActuales   : $10  
VAR minicostosuma: N20 
var banderamini: N2 
var banderaentro: N2 
var banderainfowow : N2 
var puntoswow: a20 
var desligo: A1
var Etiqueta_A_Buscar : A30
var nombreSocio : A50

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       Evento Inq 601                                          //
// ASIGNA DATOS DE MIEMBRO WOW A CHEQUE                                          //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
Event Inq: 601
    Var Campo_VR  : A200 = ""
    
    Call Verifica_Descuento_Tmed_Wow
    //LoadKyBdMacro Key(1, 196612) //pantalla siguiente
    Call Rutina_Principal_Busca_Miembro

    If @CKNUM = 0
        Call Graba_Miembro
        
        LoadKyBdMacro Key(1, 327681), MakeKeys(@TREMP), Key(1,65549), MakeKeys("0"), Key(1,65549), Key(4, 28),Key(1,65549)
        
        LoadKyBdMacro Key(1, 196611) //pantalla anterior
        
     
        Call Continuacion(16, 602)

    Else
        Var i_count : N4 = 0
        Var banderaTarjetaAsig : A1 = "N"
       
       For i_count = 1 to @numdtlt
      // infomessage "Producto", mid(@dtl_name[i_count_disc],1,11)

         if mid(@dtl_name[i_count],1,11) = "Tarjeta Wow"
               banderaTarjetaAsig = "S"
            endif
        endfor
        if  banderaTarjetaAsig = "N" or desligo ="S"
         
           LoadKyBdMacro Key(4, 28)
		   LoadKyBdMacro Key(1, 196611) //pantalla anterior
        endif

        infoSocio = 0
        Call Graba_Miembro
        
        Call Carga_Miembro_Cheque
    ENDIF     
EndEvent

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       Evento Inq 602                                          //
// Continuacion consulta de miembro cuando no se ha abierto un cheque (Inq 602)  //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
Event Inq: 602
    Call Lee_Miembro_ws
    Call Carga_Miembro_Cheque
EndEvent

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       Evento Inq 603                                          //
// Funcion de borrar datos al comenzar una venta sin tarjeta ligada              //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
Event Inq: 603
    Call msgbox_BK
    Call Rutina_Borra_Archivos_Temporales
EndEvent

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       Evento Inq 604                                          //
// Redencion de cupones WOW                                                      //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
Event Inq: 604
    Var i_count_disc : N4 = 0

    For i_count_disc = 1 to @numdtlt
        if mid(@dtl_name[i_count_disc],1,8) = "15% Desc" or \\
            mid(@dtl_name[i_count_disc],1,8) = "10% Desc"
            ExitWithError "DESCUENTO YA APLICADO EN CHEQUE" 
        endif
    endfor

    Var MemberNumber        : A100 = ""
    Var ALSELevelPrice   : A50  = ""
    Var BranchDivision   : A50  = ""
    Var ALSEPaymnetMode : A50  = ""
    Var ActivityDate        : A50  = ""
    Var Amount           : A50  = ""    
    Var Comments            : A50  = ""
    Var ItemNumber       : A50  = ""
    Var LocationName        : A50  = ""
    Var PartnerName     : A50  = ""
    Var PointName         : A50  = ""
    Var Points           : A50  = ""    
    Var ProcessDate     : A50  = ""
    Var ProcessingComment  : A50  = ""
    Var ProcessingLog     : A50  = ""
    Var ProductName     : A50  = ""
    Var Quantity            : A50  = ""
    Var TransactionChannel : A50  = ""
    Var TransactionDate : A50  = ""
    Var TransactionSubType : A50  = ""
    Var TransactionType : A50  = ""
    Var VoucherNumber     : A50  = ""
    Var VoucherQty       : A50  = "1"
    Var VoucherType     : A50  = ""
    Var Organization        : A50  = ""
    Var Bussiness_date   : A50  = ""
    Var Centroconsumo     : A50  = ""
    Var Cheque           : A50  = ""
    Var Transmitido     : A50  = ""
    Var Arma_Cadena     : A2000 = ""
    Var Status_Respuesta    : A1    = ""
    Var CC               : N2   = 0
    Var Campo                   : A78  = ""
    Var Sub_Nivel_Seleccionado  : N3    = ""
    Var Main_Nivel_Seleccionado : N1    = 0
    Var Grupo                   : A1    = ""
    Var Grupo_Numerico        : N2  = 0
    Var Tecla_Descuento      : Key
    Var Numerico                : N6    = 0
    Var Cheque_Red            : N4  = @CKNUM
    Var SNivel                : A5  = ""
     
    Call Busca_Forma_en_Cheque

    If Forma_Pago_en_Cheque = "T"
        InfoMessage "Redencion", "NO SE PERMITE REDENCION, CON FORMAS DE PAGO"
        ExitCancel
    EndIf
      
    Prompt "Espere ...1"
    
    Format Etiqueta_A_Buscar as "PartnerName"
    Call Consulta_Configuracion_CRM( Etiqueta_A_Buscar )
    Split SqlStr1, ";", SqlStr1
    PartnerName = SqlStr1

    Format Etiqueta_A_Buscar as "Descuento_Redenciones$"
    Call Consulta_Configuracion_CRM( Etiqueta_A_Buscar )
    Descuento_Redenciones_M = SqlStr1

    Format Etiqueta_A_Buscar as "Cupon_Desc_P1"
    Call Consulta_Configuracion_CRM( Etiqueta_A_Buscar )
    Cupon_Desc_P1 = SqlStr1

    Format Etiqueta_A_Buscar as "Cupon_Desc_P2"
    Call Consulta_Configuracion_CRM( Etiqueta_A_Buscar )
    Cupon_Desc_P2 = SqlStr1

    Call Lee_Miembro
      
    If Len(Trim(NumeroMiembro)) = 0
        ExitWithError "NO EXISTE MIEMBRO DE CRM PARA ESTE CHEQUE"
    EndIf

    /////////////////CUPONES ALSEA////////////////////////////////
    //VERIFICA LA FORMA DE PAGO DE LAS PROMOCIONES CUPONES ALSEA//
    Var AplicaMSR : N8 = 0
    Var AplicaWOW : N8 = 0
    CALL VerificaVntaCupon("WOW")
      
    IF AplicaWOW = 0
        ExitWithError "NO SE PERMITE EL USO DE PUNTOS CON CUPONES"
    ENDIF
    //////////////////////////////////////////////////////////////

    Prompt "Espere ...2"
    Call Consulta_Business_Date_SyBase
    Call Lee_Privilegios_Nivel_Miembro
    Call Escoge_Voucher_Redencion 

    If Contador_Voucher = 0
        ExitCancel  //27Dic2011 
    EndIf

    Prompt "Espere ...3"

    If Tipo_Descuento = "T"
         If LifetimePoint2Value < 0
            ExitWithError "NO TIENE PUNTOS SUFICIENTES"
        EndIf
    EndIf

    Format Ticket_Unico   as @WSID{01}, @CKNUM{04}, Mid(@Chk_Open_Time,10,2), Mid(@Chk_Open_Time,13,2)
    Format Fecha_a_Convertir as "20", Mid(@Chk_Open_Time,7,2), "-", Mid(@Chk_Open_Time,1,2), "-",Mid(@Chk_Open_Time,4,2)

    Call Consulta_Fecha_Juliana_Micros(Fecha_a_Convertir)     

    Format TicketNumber as Fecha_Numerica{04}, Tienda{05}, Ticket_Unico

    Prompt "Espere ...4"

    If Item_Qty_Seleccionado = 0
        Item_Qty_Seleccionado = 1
    EndIf

    Call Verifica_Monto_Total_Cheque

    if banderamini=1 and banderaentro=0
        minicostosuma= minicostosuma+minimumAmount[Opcion_Redencion]
        banderaentro=1
    elseif banderamini=0 and banderaentro=0
        minicostosuma=minimumAmount[Opcion_Redencion]
    endif

    If MontoTtlCheque >= minicostosuma
    ElseIf Len(Trim(minimumAmount[Opcion_Redencion])) = 0
    Else
        InfoMessage "NO APLICA ESTA REDENCION A ESTE CHEQUE","Monto Minimo Requerido $" ,minicostosuma   
        ExitCancel
    EndIf

    If Trim(VoucherProductName[Opcion_Redencion]) = "100pts Aniv"
        PtsAniv_monto        = validDiscountAmount[Opcion_Redencion]
        PtsAniv_monto        = "50"
        PtsAniv_ProductName = "WOW Ticket"
        Item_Qty_Seleccionado = 0
        SNivel              = "NA"
        AdjustedListPrice    = "50"  
        ItemNumber          = 0
    
    ElseIf Trim(VoucherProductName[Opcion_Redencion]) = "Cono Cumple"
        Call Verifica_Item_Seleccionado 
        
        If @TTLDUE - Item_Precio_Seleccionado < minimumAmount[Opcion_Redencion] and Item_No_Seleccionado = 8001 
                ExitWithError "EL MONTOMINIMO DEBE DE SER SIN EL ARTICULO PARA REDIMIR ESTE CUPON"
        EndIf
     
        If Item_Qty_Seleccionado <> 1
            InfoMessage "WOW CRM", "SOLO SE PERMITE UN ARTICULO PARA REDIMIR A LA VEZ"
            ExitCancel
        EndIf
     
        Format SNivel as Nivel_Price[Sub_Nivel_Seleccionado]
     
        PtsAniv_monto        = Item_Precio_Seleccionado
        PtsAniv_ProductName = Item_No_Seleccionado
        AdjustedListPrice    = Item_Precio_Seleccionado
        Item_Qty_Seleccionado = 0
     
    ElseIf Trim(VoucherProductName[Opcion_Redencion]) = "BK 10% off"
        Call Verifica_Item_Seleccionado
    
        if Item_Qty_Seleccionado <> 1
            InfoMessage "WOW CRM", "SOLO SE PERMITE UN ARTICULO PARA REDIMIR A LA VEZ"
            ExitCancel
        EndIf

        Descuento_aplicar = .10
        Ttl_Calculo = ( Item_Precio_Seleccionado * Descuento_aplicar )
        Format SNivel as Nivel_Price[Sub_Nivel_Seleccionado]
 
        PtsAniv_monto        = Ttl_Calculo  
        PtsAniv_ProductName = Item_No_Seleccionado
        ItemNumber          = 0
        AdjustedListPrice    = Item_Precio_Seleccionado  

    ///////////////////////////////// Descuentos Progresivos     06Mar2017

    ElseIf Trim(VoucherProductName[Opcion_Redencion]) = "WOW 10% BK"
        Var Total_Tck  : $10 = @TTLDUE
        Descuento_aplicar = .10
        Ttl_Calculo     = ( Total_Tck * Descuento_aplicar )
        Format SNivel    as "NA"
        PtsAniv_monto        = Ttl_Calculo
        PtsAniv_ProductName = "WOW Ticket"
        ItemNumber          = 0
        AdjustedListPrice    = Ttl_Calculo

    ElseIf Trim(VoucherProductName[Opcion_Redencion]) = "WOW 15% BK"
        Var Total_Tck  : $10 = @TTLDUE 
        Descuento_aplicar = .15
        Ttl_Calculo     = ( Total_Tck * Descuento_aplicar )
        Format SNivel    as "NA"
        PtsAniv_monto        = Ttl_Calculo
        PtsAniv_ProductName = "WOW Ticket"
        ItemNumber          = 0
        AdjustedListPrice    = Ttl_Calculo

    ElseIf Trim(VoucherProductName[Opcion_Redencion]) = "WOW 20% BK"
        Var Total_Tck  : $10 = @TTLDUE  
        Descuento_aplicar = .20
        Ttl_Calculo     = ( Total_Tck * Descuento_aplicar )
        Format SNivel    as "NA"
        PtsAniv_monto        = Ttl_Calculo
        PtsAniv_ProductName = "WOW Ticket"
        ItemNumber          = 0
        AdjustedListPrice    = Ttl_Calculo  
                    
    ElseIf Trim(VoucherProductName[Opcion_Redencion]) = "WOW 25% BK"
        Var Total_Tck  : $10 = @TTLDUE  
        Descuento_aplicar = .25
        Ttl_Calculo     = ( Total_Tck * Descuento_aplicar )
        Format SNivel    as "NA"
        PtsAniv_monto        = Ttl_Calculo
        PtsAniv_ProductName = "WOW Ticket"
        ItemNumber          = 0
        AdjustedListPrice    = Ttl_Calculo  
        
    ElseIf Trim(VoucherProductName[Opcion_Redencion]) = "Free Postre BK"
        Call Verifica_Item_Seleccionado

        If Item_Qty_Seleccionado <> 1
            InfoMessage "WOW CRM", "SOLO SE PERMITE UN ARTICULO PARA REDIMIR A LA VEZ"
            ExitCancel
        EndIf

        Format SNivel           as Nivel_Price[Sub_Nivel_Seleccionado]  
        PtsAniv_monto        = Item_Precio_Seleccionado
        PtsAniv_ProductName = Item_No_Seleccionado
        AdjustedListPrice    = Item_Precio_Seleccionado
        Item_Qty_Seleccionado = 0

    //////////////////////////////// Descuentos Progresivos

    ElseIf Trim(VoucherProductName[Opcion_Redencion]) = "30%Desc BK"
        Var Total_Tck  : $10 = @TTLDUE  
        Descuento_aplicar = .30
        Ttl_Calculo     = ( Total_Tck * Descuento_aplicar )  
        Format SNivel    as "NA"  
        PtsAniv_monto        = Ttl_Calculo  
        PtsAniv_ProductName = "WOW Ticket"
        ItemNumber          = 0
        AdjustedListPrice    = Ttl_Calculo
  
    Else
        Call Verifica_Item_Seleccionado

        If Item_Qty_Seleccionado <> 1
            InfoMessage "WOW CRM", "SOLO SE PERMITE UN ARTICULO PARA REDIMIR A LA VEZ"
            ExitCancel
        EndIf
     
        Format SNivel           as Nivel_Price[Sub_Nivel_Seleccionado]
        PtsAniv_monto        = Item_Precio_Seleccionado
        PtsAniv_ProductName = Item_No_Seleccionado
        ItemNumber          = 0
        AdjustedListPrice    = Item_Precio_Seleccionado
    EndIf
  
    Call Guarda_Campos_Acreditaciones_Redenciones(SNivel, PtsAniv_monto, PtsAniv_ProductName, Item_Qty_Seleccionado, Cheque_Red, ItemNumber)
    Call Arma_Registro_Acreditaciones_Redenciones
    Call Formatea_Datos_Redenciones
    Call Inserta_Solicitud_Acreditacion_Redencion
    Call Arma_Registro_Acreditaciones_Redenciones_CRM_Log
    Call Inserta_Solicitud_Acreditacion_Redencion
    Call Lee_Miembro_O
    Call Manda_Redemption_Miembro_O
    Call Valida_Redemption

    Status_Respuesta = "P"
    Tipo_Descuento  = "M"

    Prompt "Espere ...5" 

    Name_Jar = "REDENCIONES"
    Status_Respuesta = ""

    Prompt "Espere ...6"

    Status_Respuesta    = "P"
    Tipo_Descuento   = "M"
    Tipo_Sub_Descuento = "" 
  
    If Mid(validDiscountType[Opcion_Redencion],1,26) = "Discount Percentage Ticket"
        Tipo_Descuento       = "P"
        Tipo_Sub_Descuento   = "1"
        Descuento_Valido        = validDiscountAmount[Opcion_Redencion]
        Tipo_Descuento_Valor_P = ( ( validDiscountAmount[Opcion_Redencion] / 100 ) * ( @TTLDUE ) )
     
    ElseIf Mid(validDiscountType[Opcion_Redencion],1,24) = "Discount Percentage Item"
        Tipo_Descuento       = "P"
        Tipo_Sub_Descuento   = "2"
        Descuento_Valido        = validDiscountAmount[Opcion_Redencion]
        Tipo_Descuento_Valor_P = ( ( Descuento_Valido / 100 ) * ( Item_Precio_Seleccionado ) )

    ElseIf Mid(validDiscountType[Opcion_Redencion],1,19) = "Discount Percentage"
        Tipo_Descuento       = "P"
        Tipo_Sub_Descuento   = "3"
        Descuento_Valido        = validDiscountAmount[Opcion_Redencion]
        Tipo_Descuento_Valor_P = ( ( validDiscountAmount[Opcion_Redencion] / 100 ) * ( @TTLDUE ) )

    ElseIf Mid(validDiscountType[Opcion_Redencion],1,15) = "Discount Amount" Or Mid(validDiscountType[Opcion_Redencion],1,14) = "Discount Amout"
        Tipo_Descuento       = "M"
        Tipo_Sub_Descuento   = "1"
        Tipo_Descuento_Valor_M = validDiscountAmount[Opcion_Redencion]
      
    ElseIf Mid(validDiscountType[Opcion_Redencion],1,5) = "Vacio"
        Tipo_Descuento       = "A"
        Tipo_Sub_Descuento   = "1"
        Tipo_Descuento_Valor_M = Item_Precio_Seleccionado
    
    Else
        Tipo_Descuento       = "A"
        Tipo_Sub_Descuento   = "2"
        
        If Tipo_Descuento_Valor_M = 0
            Tipo_Descuento_Valor_M = Item_Precio_Seleccionado
        EndIf
    EndIf

    If  ( Status_Respuesta = "T" )  \\
        Or ( Status_Respuesta = "O" )  \\
        Or ( Status_Respuesta = "P" )  \\
        Or ( Status_Respuesta = "S" )
     
        If Trim(VoucherProductName[Opcion_Redencion]) <> "50 Pts Aniv"
            Call Graba_Linea_Item_Redimida( Cheque_Red, Item_Linea_Seleccionado )
        EndIf                                                       
     
        Format Campo as Mid(VoucherProductName[Opcion_Redencion],1,16)
     
        If Tipo_Descuento = "P"
            If Tipo_Descuento_Valor_P <> 0
                Var Tempo  : $10 = 0
                Descuento_P_Cal = ( Tipo_Descuento_Valor_P / 100 )
                Tempo = Item_Precio_Seleccionado  // validDiscountAmount[Opcion_Redencion]
                Descuento_Calculado = (  Tempo * Descuento_P_Cal )
            EndIf

            If Tipo_Descuento_Valor_P = 0
                Tipo_Descuento_Valor_P = validDiscountAmount[Opcion_Redencion]
            EndIf
    
            If Tipo_Sub_Descuento = 0
                LoadKyBdMacro MakeKeys(Descuento_Calculado), Key(5, Descuento_Redenciones_M)
                LoadKyBdMacro MakeKeys(Campo), @KEY_ENTER
            
            ElseIf Tipo_Sub_Descuento = 1
                LoadKyBdMacro MakeKeys(Tipo_Descuento_Valor_P), Key(5, Cupon_Desc_P1)
                LoadKyBdMacro MakeKeys(Campo), @KEY_ENTER

            ElseIf Tipo_Sub_Descuento = 2
                LoadKyBdMacro MakeKeys(Tipo_Descuento_Valor_P), Key(5, Cupon_Desc_P2)
                LoadKyBdMacro MakeKeys(Campo), @KEY_ENTER
            
            ElseIf Tipo_Sub_Descuento = 3
                LoadKyBdMacro MakeKeys(Descuento_Calculado), Key(5, Descuento_Redenciones_M)
                LoadKyBdMacro MakeKeys(Campo), @KEY_ENTER
            EndIf

        ElseIf Tipo_Descuento = "M"
            If  Tipo_Sub_Descuento = 0
                LoadKyBdMacro MakeKeys(Item_Precio_Seleccionado), Key(5, Descuento_Redenciones_M)
                LoadKyBdMacro MakeKeys(Campo), @KEY_ENTER
            ElseIf  Tipo_Sub_Descuento = 1
                If Tipo_Descuento_Valor_M <> 0
                    LoadKyBdMacro MakeKeys(Tipo_Descuento_Valor_M), Key(5, Descuento_Redenciones_M)
                Else
                    LoadKyBdMacro MakeKeys(Item_Precio_Seleccionado), Key(5, Descuento_Redenciones_M)
                EndIf
    
                LoadKyBdMacro MakeKeys(Campo), @KEY_ENTER
            EndIf
 
        ElseIf Tipo_Descuento = "A"
                LoadKyBdMacro MakeKeys(Tipo_Descuento_Valor_M), Key(5, Descuento_Redenciones_M)
                LoadKyBdMacro MakeKeys(Campo), @KEY_ENTER
        EndIf
 
        banderamini=1

    Else
        ExitWithError "EL CUPON NO APLICA PARA EL PRODUCTO SELECCIONADO"
    EndIf
EndEvent

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       Evento Inq 605                                          //
// Muestra la informacion de un socio WOW Rewards                                //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
Event Inq: 605
    banderainfowow = 1
    Var Campo_VR    : A200 = ""
    Var Paso_01  : A30  = ""
    Consulta_Pantalla = "S"
    infoSocio = 1
    Call Rutina_Principal_Busca_Miembro
    Call Carga_Miembro_Cheque
    call Imprime_Ticket_info_socio_wow
    TouchScreen 0
    Input Paso_01, "Enter/Clear Para Continuar"
    WindowClose
    banderainfowow = 0
EndEvent

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       Evento Inq 606                                          //
// Redencion de puntos WOW                                                       //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
Event Inq: 606
    Call Lee_Miembro    
    nombreSocio = mid(@dtl_name[1],1,32)
    If Len(Trim(NumeroMiembro)) = 0
        ExitWithError "NO EXISTE MIEMBRO DE CRM PARA ESTE CHEQUE"
    EndIf
    Call Verifica_Aportaciones_Aplicado_Cheque
    
    If Existe_Aportacion = "T" And Existe_Menu_Item = "T"
        InfoMessage "Aportaciones", "NO SE PERMITE COMBINAR APORTACIONES-PUNTOS WOW"
        ExitCancel
    EndIf

    Var TotalCheque     : $10   = 0 
    Var TotalCheque_N     : N10 = 0
    Var TotalCheque_A     : A12 = ""
    Var Decimales_A     : A3    = ""
    Var Decimales         : $5  = ""
    Var Inicio_A            : N3    = 0
    Var Parciales         : N1  = 0
    Var Descuento_A     : $10   = @DSC
    Var Decimales2       : $5   = ""
    
    TouchScreen 60 
    Input TotalCheque, "Digite Monto a Pagar en Pesos"

    if entra_puntos_primera_vez = 0 
        Call Busca_Points_Cheque 
        puntosActuales = Points_Cheque 
        entra_puntos_primera_vez = 1 
    endif 
        
    If TotalCheque = 0
        TotalCheque = @TTLDUE
        
        If puntosActuales < TotalCheque
            TotalCheque = puntosActuales
            puntosWOW_Global= puntosWOW_Global + puntosActuales 
        Else
            puntosWOW_Global= puntosWOW_Global + TotalCheque
        EndIf
    Else      
        If puntosActuales < TotalCheque  
            InfoMessage "Points", "SALDO INSUFICIENTE"
            ExitCancel
        Else
            puntosActuales = puntosActuales - TotalCheque
            puntosWOW_Global = puntosWOW_Global + TotalCheque
        EndIf
    EndIf

    If TotalCheque > @TTLDUE
        ExitWithError "MONTO NO PERMITIDO, MAYOR A LA CUENTA"
    EndIf

    If Descuento_A <> 0
        Descuento_A = ( Descuento_A * -1 )
    EndIf
    
    If @TTLDUE <= TotalCheque
        Parciales = 0
    Else
        Parciales = 1
    EndIf

    /////////////////CUPONES ALSEA////////////////////////////////
    //VERIFICA LA FORMA DE PAGO DE LAS PROMOCIONES CUPONES ALSEA//
    Var AplicaMSR : N8 = 0
    Var AplicaWOW : N8 = 0
    CALL VerificaVntaCupon("WOW")
      
    IF AplicaWOW = 0
        ExitWithError "NO SE PERMITE EL USO DE PUNTOS CON CUPONES"
    ENDIF
    //////////////////////////////////////////////////////////////

    LoadKyBdMacro MakeKeys(TotalCheque), Key(9, 60) //Tender crm
EndEvent

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       Evento Inq 607                                          //
// Borra item del cheque                                                         //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
Event Inq: 607
    Var i_count_disc : N4 = 0
    var gDescCupon :N1= 0 
    var gCuponProducto:N1=0
    var gItemLigado:N7=0
    var gDescPorcen:N7=0
    var gDescValor:N7=0
    var gFormaPago:N7=0
    var gProductoLig:A100=""
    var gFamiliaLig:A100=""
    Var gBorraCupon:N1=0
    var gEstaLigCupon:N1=0
    var gEsCupon:A1="N"
    var gHayFormadePago:N1=0 
    var gFPSelect:N1=0
    var gTipoPromo:N1=0
    var gFIIDPROMO:N1=0
    var gFolioCupon:A10=""
    
    
    For i_count_disc = 1 to @numdtlt
        if mid(@dtl_name[i_count_disc],1,10) = "Puntos WOW"  
           ExitWithError "NO SE PERMITE BORRAR ARTICULOS O FORMA DE PAGOS" 
        endif
    endfor
    
    Var Key_Void  : Key
    
    Call Verifica_Item_Seleccionado_Void
    Key_Void = Key(1,458753)
    If Flag_Descuento_Wow_Void = 0 And Flag_FPago_Wow_Void = 0

       /////////////////CUPONES ALSEA////////////////////////////////
       //VALIDA LA SELECCION DE LOS CUPONES O ARTICULOS A BORRAR   // 
       CALL ValidaCuponSelec         
       //////////////////////////////////////////////////////////////
    
       LoadKyBdMacro Key_Void  //Void Key

    Else
       InfoMessage "VOID WOW", "NO SE PERMITE ESTA OPERACION (WOW)"
    EndIf
EndEvent

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       Evento Inq 608                                          //
// Desliga un miembro WOW Rewards del cheque                                     //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
Event Inq: 608
    var contador : N2 = 0
    
    For contador = 1 to @NumDtlt
     
            If(mid(@dtl_name[contador],1,11) = "Tarjeta Wow")
            desligo ="S"
            voidDetail(contador)
        endif
    endfor
    
    Call Verifica_Descuento_Tmed_Wow
    
    ClearChkInfo

    Call Rutina_Borra_Archivos_Temporales
EndEvent

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       Evento Inq 609                                          //
// Realiza un descuento del 5% por estar en modo offline                         //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
Event inq: 609
    var Tecla_Descuento : key  
    Var IO       : N2  = 0
    Var No_tarjeta : A30 = "0"
    Var Campo_01_  : A80 = ""
    Var Campo_02_  : A80 = ""
    Var Campo_000  : A80 = ""
    
    TouchScreen 3057
    Window 1, 4, "WOW"
        DisplayMsInput 0,0,Clave_ID{-m2,*},"Deslize Tarjeta CRM WOW2:", \
        0,0,Clave_ID_TrackI{-m1,*},"Deslize Tarjeta CRM WOW3:", \ 
        0,0,Clave_ID_TrackII{m2, 1, 1, *},"Deslize Tarjeta CRM WOW4:" 
    WindowInput
    WindowClose

    If @MAGSTATUS = "Y"
        call Delete_Miembro_Ocupa_Pos(Wsid_wow) 
        metodo_entrada = "Tarjeta"

        Format Clave_ID_TrackII_Origin as Clave_ID_TrackII
        mid(Clave_ID_TrackII,1,12) = "XXXXXXXXXXXX"
        Format Campo as "", Clave_ID_TrackII 
        Call Carga_Screen_Cheque
        Split Clave_ID, "=", Clave_ID_Paso1, Clave_ID_Paso2
        ClearArray Clave_Id_Track_II
        ClearArray Clave_Id_Track_II_1
        Split Clave_ID_TrackI, "^", Clave_Id_Track_II[1], Clave_Id_Track_II[2], Clave_Id_Track_II[3]
        
        If Len(Clave_Id_Track_II[2]) > 0
            Split Clave_Id_Track_II[2], "/", Clave_Id_Track_II_1[1], Clave_Id_Track_II_1[2], Clave_Id_Track_II_1[3]
        EndIf

        if Clave_Id_Track_II_1[3] = "ALSEAWOW" Or Clave_Id_Track_II_1[3] = "ALSE" 
        else
            ExitWithError "Tarjeta no es WOW" 
            
        endif
            
        If Mid(Clave_ID,1,1) = ";" And Mid(Clave_ID,16,1) = "?"
            Format No_tarjeta    As Mid(Clave_ID,2,14)
            Format Campo_Busqueda As No_tarjeta

        ElseIf Mid(Clave_ID,1,1) = ";" And Len(Clave_ID_Paso2) > 0
            Format No_tarjeta    As Mid(Clave_ID,2,16)
            Format Campo_Busqueda As No_tarjeta
        
        ElseIf Clave_Id_Track_II_1[3] = "ALSEAWOW" Or Clave_Id_Track_II_1[3] = "ALSE"
            Var Len1  : N4 = 0
            Len1 = ( Len(Clave_ID_Paso1) - 1 )
            Format No_tarjeta    As Clave_ID_Paso1
            Format Campo_Busqueda As No_tarjeta
        Else
            Format No_tarjeta    As Mid(Clave_ID,30,1)
            Format No_tarjeta    As No_tarjeta, Mid(Clave_ID,32,2)
            Format No_tarjeta    As No_tarjeta, Mid(Clave_ID, 7,1)
            Format No_tarjeta    As No_tarjeta, Mid(Clave_ID, 8,8)
            Format No_tarjeta    As No_tarjeta, Mid(Clave_ID,34,4)
            Format Campo_Busqueda As No_tarjeta
        EndIf
     infomessage mid(Campo_Busqueda,1,2)
    
    Else
        metodo_entrada = "Smartphone" 
            Campo_Busqueda = Clave_ID
            infomessage Clave_ID
            if  mid(Clave_ID,1,2) <> "60" Or len(trim(Clave_ID)) <> 16
                ExitWithError "Tarjeta no es Wow"
            endif
            mid(Clave_ID,1,12) = "XXXXXXXXXXXX" 
            Format Campo as "", Clave_ID 
            Call Carga_Screen_Cheque
    EndIf

    
    Tecla_Descuento = Key(5,107)
    LoadKyBdMacro Tecla_Descuento, makekeys( Clave_ID_TrackII ),@KEY_ENTER 
EndEvent
  
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       Evento Final_Tender                                     //
// Manda la acreditacion de la cuenta                                            //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
Event Final_Tender
    Var MemberNumber        : A100 = ""
    Var ALSELevelPrice   : A50  = ""
    Var BranchDivision   : A50  = ""
    Var ALSEPaymnetMode : A50  = ""
    Var ActivityDate        : A50  = ""
    Var Amount           : A50  = ""    
    Var Comments            : A50  = ""
    Var ItemNumber       : A50  = ""
    Var LocationName        : A50  = ""
    Var PartnerName     : A50  = ""
    Var PointName         : A50  = ""
    Var Points           : A50  = ""    
    Var ProcessDate     : A50  = ""
    Var ProcessingComment  : A50  = ""
    Var ProcessingLog     : A50  = ""
    Var ProductName     : A50  = ""
    Var Quantity            : A50  = ""
    Var TicketNumber        : A50  = ""
    Var TransactionChannel : A50  = ""
    Var TransactionDate : A50  = ""
    Var TransactionSubType : A50  = ""
    Var TransactionType : A50  = ""
    Var VoucherNumber     : A50  = ""
    Var VoucherQty       : A50  = "1"
    Var VoucherType     : A50  = ""
    Var Organization        : A50  = ""
    Var Bussiness_date   : A50  = ""
    Var Centroconsumo     : A50  = ""
    Var Cheque           : A50  = ""
    Var Transmitido     : A50  = ""
    Var Arma_Cadena     : A2000 = ""
    Var Arma_Cadena_22   : A2000 = ""
    Var Status_Respuesta    : A1    = ""

    Call Version_ISL
    
    Call Verifica_Descuento_Aplicado

    If Total_Fpago_Cheque > 0
        Call Ejecuta_Acreditaciones_Main
    EndIf

    if puntosWOW_Global > 0 
        Call imprime_ticket_redencion
    endif  

    puntosWOW_Global = 0 
    entra_puntos_primera_vez = 0 

    Borrar = "S"
    
    Call Rutina_Borra_Archivos_Temporales
EndEvent

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       Evento Print_Trailer: Impresion_WOW                     //
// Imprime el trailer Impresion_WOW con los datos del cliente                    //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
Event Print_Trailer: Impresion_WOW
    Var I_II : N3  = 0
    Var C_II : N3  = 0
    Var Leng : N3  = 0
    Var CCC  : A99 = ""
    Var St_A : A4  = ""
    Var St_N : N6  = 0
    Var Ly[2]: A99
    var nu : N6 =0
    var name : A99 = ""
    var nameTeporal : A99 = ""
    var nameSpace : A99 = ""
    Var i : N10
    
    For i = 1 to 10
    
    if i=3
        
        nameTeporal = mid(@dtl_name[1],1,32)
        Split nameTeporal ,  " ", name
        Format nameSpace as "            ", name
        
        if nombreSocio = ""
            @TRAILER[i] = nameSpace
            nu = nu + 1
        else
     
            @TRAILER[i] = nombreSocio
            nu = nu + 1
        endif
    else
        if LeyendaCRM_WOW[i] <> ""
            Format Campo as LeyendaCRM_WOW[i]
            @TRAILER[i] = Campo
            nu = nu + 1
        endif
    endif
    EndFor
    
    @TRAILER[nu+1] = " "
        
    Call Borra_Leyenda_CRM
    Call Borra_FileMiembro_Pausa_R_Cheque
    Call Borra_FileLeyendaCRM_Cheque
EndEvent

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       Evento Tmed: *                                          //
//                                                                               //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
Event Tmed: *
    Var MemberNumber        : A100 = ""
    Var ALSELevelPrice   : A50  = ""
    Var BranchDivision   : A50  = ""
    Var ALSEPaymnetMode : A50  = ""
    Var ActivityDate        : A50  = ""
    Var AdjustedListPrice  : A50  = ""
    Var Amount           : A50  = ""    
    Var Comments            : A50  = ""
    Var ItemNumber       : A50  = ""
    Var LocationName        : A50  = ""
    Var PartnerName     : A50  = ""
    Var PointName         : A50  = ""
    Var Points           : A50  = ""    
    Var ProcessDate     : A50  = ""
    Var ProcessingComment  : A50  = ""
    Var ProcessingLog     : A50  = ""
    Var ProductName     : A50  = ""
    Var Quantity            : A50  = ""
    Var TransactionChannel : A50  = ""
    Var TransactionDate : A50  = ""
    Var TransactionSubType : A50  = ""
    Var TransactionType : A50  = ""
    Var VoucherNumber     : A50  = ""
    Var VoucherQty       : A50  = "1"
    Var VoucherType     : A50  = ""
    Var Organization        : A50  = ""
    Var Bussiness_date   : A50  = ""
    Var Centroconsumo     : A50  = ""
    Var Cheque           : A50  = ""
    Var Transmitido     : A50  = ""
    Var Arma_Cadena     : A2000 = ""
    Var Status_Respuesta    : A1    = ""
    Var CC               : N2   = 0
    Var Campo                   : A78  = ""
    Var Sub_Nivel_Seleccionado  : N3    = ""
    Var Main_Nivel_Seleccionado : N1    = 0
    Var Grupo                   : A1    = ""
    Var Grupo_Numerico        : N2  = 0
    Var Tecla_Descuento      : Key
    Var Numerico                : N6    = 0
    Var Opcion_Redencion        : N2    = 0
    Var Cheque_Red            : N4  = @CKNUM
    Var SNivel                : A5  = ""
    Var TotalCheque     : $10   = @TNDTTL
    Var Mes_Tempo                 : A2  = ""
    Var TTL001                   : $10  = 0
    Var Campo000                    : A20   = ""
    Var Fecha                     : A12 = ""
    Var FN01                        : N6    = 0
    Var Path_Solicita             : A90 = ""
    Var Path_Respuesta           : A90  = ""
    Var File_Solicita             : A190  = ""
    Var File_Respuesta           : A190  = ""
    Var File_Paso                 : A190  = ""
    Var II                       : N6   = 0
    Var Business_Date             : A30 = ""
    Var Numero_TouchscreenN     : N6    = 0
    Var Imp_Rollo                 : N4  = 0
    Var Terminal                    : N4    = 0
    Var Linea                     : A99 = ""
    Var WSID                        : A3    = ""
    Var TimeOut                 : N3    = 0
    Var Mensaje_Salida           : A90  = ""
    
    If @TTLDUE < 0
        Format Ticket_Unico as @WSID{01}, @CKNUM{04}, Mid(@Chk_Open_Time, 10, 2) , Mid(@Chk_Open_Time, 13, 2)
        If Len(Trim(@Ckid)) = 0
            CLEARCHKID
            @Ckid       = ( Ticket_Unico )
        EndIf
    Else
        Format Ticket_Unico as @WSID{01}, @CKNUM{04}, Mid(@Chk_Open_Time, 10, 2) , Mid(@Chk_Open_Time, 13, 2)
        If Len(Trim(@Ckid)) = 0
            CLEARCHKID
            @Ckid       = ( Ticket_Unico )
        EndIf
        
        Monto_Original = ( @TNDTTL )
        TTL001       = ( @Ttldue - @TNDTTL )
        Monto_Original = ( @TNDTTL )
    
        If TTL001 <= 0 
            Prompt "Inicia Proceso Inserta Reg"
            If TTL001        <  0
                Monto_Original = ( @Ttldue )
            EndIf
            Call Version_Micros_Sybase
            Call Trae_Status_Facturable_FPago_Repositorio
            Call Verifica_FPago_Facturable
            ClearChkInfo
            Format Campo as "Edofacturacion:", Facturacion_Parcial
            SaveChkInfo Campo
            Format Campo as "EdofacturacionO:", Facturacion_Parcial_Real
            SaveChkInfo Campo
        EndIf
    EndIf

    Call Inserta_Combo_Count
    Call Asigna_Ckid
    Call Verifica_Descuento_Aplicado_Cheque_Wow
    
    If TotalCheque = 0
        TotalCheque = @TTLDUE
    EndIf
    
    If TotalCheque = 0
        ExitCancel
    EndIf
    
    Prompt "Espere ...1"
    
    Format Etiqueta_A_Buscar as "PartnerName"
    Call Consulta_Configuracion_CRM( Etiqueta_A_Buscar )
    Split SqlStr1, ";", SqlStr1
    PartnerName = SqlStr1

    /////////////////CUPONES ALSEA////////////////////////////////
    //VERIFICA LA FORMA DE PAGO DE LAS PROMOCIONES CUPONES ALSEA//
    Var AplicaMSR : N8 = 0
    Var AplicaWOW : N8 = 0
    CALL VerificaVntaCupon("WOW")
      
    IF AplicaWOW = 0
        ExitWithError "NO SE PERMITE EL USO DE PUNTOS CON CUPONES"
    ENDIF
    //////////////////////////////////////////////////////////////

    Call Lee_Miembro
    
    If Len(Trim(NumeroMiembro)) = 0
        ExitWithError "NO EXISTE MIEMBRO DE CRM PARA ESTE CHEQUE"
    EndIf
    
    Prompt "Espere ...2"
    Call Consulta_Business_Date_SyBase
    Prompt "Espere ...3"

    Format Ticket_Unico   as @WSID{01}, @CKNUM{04}, Mid(@Chk_Open_Time,10,2), Mid(@Chk_Open_Time,13,2)
    Format Fecha_a_Convertir as "20", Mid(@Chk_Open_Time,7,2), "-", Mid(@Chk_Open_Time,1,2), "-",Mid(@Chk_Open_Time,4,2)

    Call Consulta_Fecha_Juliana_Micros(Fecha_a_Convertir)     

    Format SNivel as "NA" //Nivel_Price[Sub_Nivel_Seleccionado]
    Format TicketNumber as Fecha_Numerica{04}, Tienda{05}, Ticket_Unico
    Prompt "Espere ...4"

    Call Busca_Aportacion_Acreditaciones_Soa_Only
    If Total_Aportaciones <> 0
        TotalCheque = ( TotalCheque - Total_Aportaciones )
    EndIf

    Call Guarda_Campos_Acreditaciones_Redenciones_Wow(SNivel, Item_Precio_Seleccionado, Item_No_Seleccionado, Item_Qty_Seleccionado, Cheque_Red)
    Call Formatea_Datos_Redenciones_Tmed_Wow
    Call Manda_Redemption_Tmed_Wow
    Call Valida_Redemption_Tmed_Wow
    
    Status_Respuesta = "P"
    Tipo_Descuento  = "M"
    Prompt "Espere ...5"
    Name_Jar = "REDENCIONES"
    Status_Respuesta = ""  
    Prompt "Espere ...6"
EndEvent

