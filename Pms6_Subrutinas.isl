//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                      Subrutina ValidaDescSelec                                //
// VERIFICA SI ES POSIBLE BORRAR EL CUPON APLICADO                               //
// MODIFICACION Alsea                                                            //
//                                                                               //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
Sub ValidaCuponSelec
    
  //VARIABLES PARA TRANSACCIONES
  var FIIDTicket                  : A12  = ""
  var FCEstacionTrabajo           : A10   = ""
  Var FCIDFolioCupon              : A10  = ""
  var FIIDTienda                  : N8   = 0    
  var FIIDMarca                   : N8   = 0    
  var FIIDCodigoEstatus           : N8   = 0    
  var FCDescripcionEstatus        : A500 = ""        
  var FIIDCampana                 : N8   = 0    
  var FIIDTipoPromocion           : N8   = 0    
  var FCValorCupon                : A10  = ""    
  var FNMontoLigado               : $12  = 0 
  var FIContieneCuponLigado       : N8   = 0    
  var FITipoCuponID               : N8   = 0    
  var FIGeneralParcial            : N8   = 0    
  var FIAplicaWOW                 : N8   = 0    
  var FIAplicaMSR                 : N8   = 0        
  var FIFormaPagLigada            : A8   = ""       
  var FISolicitaTarjeta           : N8   = 0    
  var FITarjetaUlt4Num            : A16  = ""    
  var FISolicitaEmail             : N8   = 0    
  var FNMontoMax                  : $12  = 0     
  var FCNombreEmpleado            : A50  = ""    
  var FCFolioCuponLigadoID        : A10  = ""    
  var FCLeyendaCupon              : A150 = ""
  var FIIDColaborador             : A150 = ""
  var FIMontoTotalVenta           : $12  = 0 
  var FCEmailCliente              : A150 = ""
  var FITiposOrdenAplican         : N3   = 0
  var FNFormaPagoMarca            : N10  = 0

  //VARIABLES PARA RESPUESTA CONSULTA
  Var Band_FamDescartadas         : A1   = ""
  Var Band_FamiliasLigadas        : A1   = ""
  Var Band_Familias2x1            : A1   = ""
  Var Band_ProdDescartados        : A1   = ""
  Var Band_Productos2x1           : A1   = ""
  Var Band_ProductosLigados       : A1   = ""
  Var FCTipoPromocionConf         : A100 = ""

  //VARIABLE PARA VALIDACIONES CUPONES
  Var CUPONES_ALSEA_RESP          : A200 = ""
  var DFuncSQL                    : A30  = ""
  Var IDFolioCupon                : A10  = ""
  var ParamCons                   : A20  = 0
  Var IDTicket                    : A12  = ""
  Var EstacionTrabajo             : A4   = ""
  Var CuponExiste                 : A1   = "F"
  Var ExistCuponAplica            : A1   = ""
  Var countFamiliasLigadas        : N4    = 0
  Var countProductosLigados       : N4    = 0 
    
      //OBTIENE NUMERO DE ESTACION DE TRABAJO
      EstacionTrabajo = @WSID
    
      //OBTIENE NUMERO DE TICKET
    CALL Calcula_Numero_Ticket
    IDTicket = FIIDTicket
    
      //FUNCION PARA REALIZAR LA CAPTURA DEL FOLIO DEL CUPON
      CALL ObtFolioCupon(FIIDTicket)
      IDFolioCupon     = CUPONES_ALSEA_RESP
    
    //VERIFICA SI EL CUPON YA HA SIDO APLICADO EN ESTE POS
      CALL ConsultaCupon(IDFolioCupon, IDTicket, EstacionTrabajo) 
    
      IF CuponExiste = "T"       
         Var I                   : N4    = 0
         VAR TipoPromocionExist  : N10
            
         //OBTIENE EL NUMERO DEL DESCUENTO
         CALL obtTipoCupConf(FIIDTipoPromocion)
         TipoPromocionExist = CUPONES_ALSEA_RESP
         
       IF FCValorCupon = 0 AND FITipoCuponID = 2 AND FIIDTipoPromocion <> 6
          ExistCuponAplica = "F"
         ELSEIF FIIDTipoPromocion = 1 OR FIIDTipoPromocion = 3 OR FIIDTipoPromocion = 5 OR FIIDTipoPromocion = 6 OR FIIDTipoPromocion = 7
            FOR I = 1 TO @NumDTLT
                IF @DTL_TYPE[I] = "D" AND @DTL_IS_VOID[I] = 0 AND @dtl_Selected[I] = 1
                   IF TipoPromocionExist = @DTL_OBJECT[I]
                      ExistCuponAplica = "T"
                      BREAK
                   ELSE
                      ExistCuponAplica = "F"
                   ENDIF              
                ENDIF
            ENDFOR
         ELSEIF FIIDTipoPromocion = 2
          FOR I = 1 TO @NumDTLT
                IF @DTL_TYPE[I] = "M" AND @DTL_IS_VOID[I] = 0 AND @dtl_Selected[I] = 1
                    IF FCValorCupon = @DTL_OBJECT[I]
                       ExistCuponAplica = "T" 
                       BREAK
                    ELSE
                       ExistCuponAplica = "F"
                    ENDIF
                ENDIF   
            ENDFOR      
         ELSEIF FIIDTipoPromocion = 4
            FOR I = 1 TO @NumDTLT        
                IF @DTL_TYPE[I] = "T" AND @DTL_IS_VOID[I] = 0 AND @dtl_Selected[I] = 1
                   IF TipoPromocionExist = @DTL_OBJECT[I]
                      ExistCuponAplica = "T"
                      BREAK
                   ELSE
                      ExistCuponAplica = "F"
                   ENDIF
                ENDIF
            ENDFOR 
         ENDIF
      ENDIF
    
    IF CuponExiste = "T" AND ExistCuponAplica = "T"
       FOR I = 1 TO @NumDTLT
             IF @DTL_TYPE[I] = "T" AND @DTL_IS_VOID[I] = 0 AND TipoPromocionExist <> @DTL_OBJECT[I]
                exitwitherror "YA EXISTE UNA FORMA DE PAGO APLICADA, NO ES POSIBLE BORRAR CUPONES."
             ENDIF
         ENDFOR 
       IF FIIDCodigoEstatus = 3
            exitwitherror "NO ES POSIBLE BORRAR ESTE CUPON"
       ENDIF
       CALL DeleteCupon(IDFolioCupon,0)
    ELSEIF CuponExiste = "T" AND ExistCuponAplica = ""
       //OBTIENE EL NUMERO DEL DESCUENTO
         CALL obtTipoCupConf(4)
         TipoPromocionExist = CUPONES_ALSEA_RESP
       FOR I = 1 TO @NumDTLT
             IF @DTL_TYPE[I] = "T" AND @DTL_IS_VOID[I] = 0 AND TipoPromocionExist = @DTL_OBJECT[I]
                exitwitherror "YA EXISTE UNA FORMA DE PAGO APLICADA, NO ES POSIBLE BORRAR CUPONES."
             ENDIF
         ENDFOR
    ELSEIF CuponExiste = "T" AND ExistCuponAplica = "F" AND FCValorCupon = 0 AND FITipoCuponID = 2 AND FIIDTipoPromocion <> 6
       exitwitherror "NO ES POSIBLE BORRAR VENTA CON CUPON FRECUENCIA."
    ENDIF 
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       SUBRUTINA verFamProdLigadosVOID                         //
// VERIFICA SI QUIEREN BORRAR LOS PRODUCTOS O FAM QUE SE ENCUENTRAN              // 
// LIGADOS AL CUPON SIN INTENTAR BORRAR EL CUPON                                 //
// FECHA MODIFICACION:                                                           //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
Sub verFamProdLigadosVOID
  Var Item_excluido                   : N1    = 0
  Var I                               : N4    = 0

       FOR I=1 TO @NUMDTLT
           IF (@DTL_TYPE[I] = "M" OR @DTL_TYPE[I] = "S") AND @DTL_IS_VOID[I] = 0 AND @dtl_Selected[I] = 1
              IF @DTL_IS_COND[I] > 0   //  
              ELSE // MENU ITEM
                 // VERIFICA SI EXISTE LA FAMILIA QUE SE INCLUYE
                 DFuncSQL  = "FamInc"
                 ParamCons = @DTL_FAMGRP_OBJNUM[I]
                 CALL VALIDA_CUPONES_ALSEA(DFuncSQL, IDFolioCupon, ParamCons, IDTicket, EstacionTrabajo) 
                 IF CUPONES_ALSEA_RESP = "1" 
            countFamiliasLigadas = countFamiliasLigadas + 1
                    Item_excluido = 0
                    BREAK
                 ELSE
                    Item_excluido = 1
                 ENDIF
              ENDIF
           ENDIF
       ENDFOR
 
       FOR I=1 TO @NUMDTLT    
           IF @DTL_TYPE[I] = "M" AND @DTL_IS_VOID[I] = 0 AND @dtl_Selected[I] = 1
              IF @DTL_IS_COND[I] > 0   //  
              ELSE // MENU ITEM
                 // VERIFICA SI EXISTE EL PRODUCTO QUE SE INCLUYE
                 DFuncSQL  = "SKUInc"
                 ParamCons = @DTL_OBJECT[I]
                 CALL VALIDA_CUPONES_ALSEA(DFuncSQL, IDFolioCupon, ParamCons, IDTicket, EstacionTrabajo) 
                 IF CUPONES_ALSEA_RESP = "1"
                    countProductosLigados = countProductosLigados + 1 
                    Item_excluido = 0
                    BREAK
                 ELSE
                    Item_excluido = 1
                 ENDIF
               ENDIF
           ENDIF
       ENDFOR
ENDSUB

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       SUBRUTINA Rutina_Principal_Busca_Miembro                //
// RUTINA ENCARGADA PARA OBTENER DATOS DE MIEMBRO WOW                            //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
Sub Rutina_Principal_Busca_Miembro
    If Len(Campo_Busqueda) = 0 Or Leidos_Consulta < 2
        
        Numero_TouchscreenN = 3057

        Call Lee_Tarjeta_Magnetica_Wow_Card
        
    EndIf
    
    Call Consulta_Business_Date_SyBase
    Call Manda_Cliente_Nombre
    Call Valida_Respuesta
    
    if offline=1
        ExitContinue
    endif

    If Salida_Consulta_Clientes = "T"
        ExitContinue 
    EndIf

    Call Separa_Campos
    
    If Trim(Status) <> "Active"
        Format Campo_VR as "MIEMBRO BK[", Trim(Clave_ID), "]  NO ACTIVO [", Trim(Activo), "][", Trim(Status), "]"
        ExitWithError Campo_VR
    EndIf
    
    Format No_Tarjeta_WOW_anterior as No_Tarjeta_WOW 
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       SUBRUTINA imprime_ticket_tarjeta_no_inscrita            //
// IMPRIME TICKET EN CASO DE QUE LA TARJETA NO ESTE INCRITA                      //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
Sub imprime_ticket_tarjeta_no_inscrita
    Var tarjeta_invalida : a20 = Clave_ID_TrackII
    startprint @vald
        printline "Tarjeta con terminacion ",Mid(tarjeta_invalida,13,16)
        printline "      NO esta activa"
        printline "  Activala en www.wowrewards.com"
        printline "Fecha: ", @Day,"/",@Month,"/",@Year
        printline "Hora: ",@HOUR,":",@Minute,":",@Second
    endprint
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       SUBRUTINA imprime_ticket_redencion                      //
// IMPRIME TICKET EN CASO DE QUE SE HAYA HECHO UNA REDENCION                     //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
Sub imprime_ticket_redencion
    startprint @vald
        printline "Empleado:  ",@TREMP_CHKNAME
        printline "Fecha: ", @Day,"/",@Month,"/",@Year
        printline "Hora: ",@HOUR,":",@Minute,":",@Second
        printline "Folio: ", Ticket_Unico
        printline ""
        printline " Membresia WOW"
        printline " ",Clave_ID_TrackII
        printline " Metodo de entrada: ", metodo_entrada
        printline " Autorizado"
        printline ""
        printline "Puntos utilizados en pesos: $",puntosWOW_Global//tmed: *, inq:71
        printline ""
        printline ""
        printline "  Firma X____________________"
        printline ""
        printline "   GRACIAS POR SU VISITA "
        printline ""
        printline "      COPIA DE NEGOCIO"
        printline ""
    endprint
endsub 

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       SUBRUTINA imprime_ticket_redencion                      //
// ELIMINA VARIABLES GLOBALES USADAS EN TODO EL FLUJO DE WOW REWARDS             //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
Sub Rutina_Borra_Archivos_Temporales
    Call Borra_Miembro
    Call Borra_Privilegios_Nivel_Miembro
    
    Miembro_O_Archivo = ""    
    Call Borra_Linea_Item_Redimida
    FileOff_Line_CRM_string = ""
    
   // Call Borra_Leyenda_CRM
    
    Cheque_Miembro_Soa = 0
    rspst_Miembro_Soa = ""

    Call Delete_Miembro_Ocupa_Pos(Wsid_wow)
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       SUBRUTINA Ejecuta_Acreditaciones_Main                   //
// EMPIEZA FLUJO DE ACREDITACIÓN DESDE EL EVENTO FINAL_TENDER                    //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
Sub Ejecuta_Acreditaciones_Main
    Var CC  : N2 = 0

    Call Lee_Miembro
    Call Lee_Miembro_Soa

    If Len(Trim(NumeroMiembro)) = 0
        If Len(Trim(NumeroMiembroSolo)) = 0 And offline = 0 
            If Borrar = "S"
                Call Borra_Miembro
            EndIf

            Call Borra_Linea_Item_Redimida
            ExitContinue
            
        Else
            NumeroMiembro = NumeroMiembroSolo
        EndIf
    EndIf

    Call Consulta_Business_Date_SyBase
    Call Borra_Miembro

    Format Ticket_Unico   as @WSID{01}, @CKNUM{04}, Mid(@Chk_Open_Time,10,2), Mid(@Chk_Open_Time,13,2)
    Format Fecha_a_Convertir as "20", Mid(@Chk_Open_Time,7,2), "-", Mid(@Chk_Open_Time,1,2), "-", Mid(@Chk_Open_Time,4,2)

    Call Consulta_Fecha_Juliana_Micros(Fecha_a_Convertir)
    Call Main_Busca_Articulos_Enviar_Acreditaciones
    Call Borra_Linea_Item_Redimida
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       SUBRUTINA Lee_Tarjeta_Magnetica_Wow_Card                //
// LEE LOS TRACKS DE UNA TARJETA Y DECIDE SI ES WOW REWARDS                      //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
Sub Lee_Tarjeta_Magnetica_Wow_Card
    Var IO       : N2  = 0
    Var No_tarjeta : A30 = "0"
    Var Campo_01_  : A80 = ""
    Var Campo_02_  : A80 = ""
    Var Campo_000  : A80 = ""

    If Error_Time_Out_SbuxCard = "E" or Error_Time_Out_SbuxCard = "F"
        Format Campo_01_ as "<WS_OFFLINE>"
        Format Campo_02_ as "<Success>"

        Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)

        If @RVC = 1  Or @RVC = 2 Or @RVC = 4  Or @RVC = 5
            TouchScreen Numero_TouchscreenN
            Window  (CC01 + 1 ), 78, "CRM WOW"  

            For IO = 1 to CC01
                If IO = 3 Or IO = 4
                    DisplayInverse  IO, 1, Mid(Errores_Mostrar[IO],1,77)
                Else
                    Display  IO, 1, Mid(Errores_Mostrar[IO],1,77)
                EndIf
            EndFor

            DisplayMsInput 0,0,Clave_ID{-m2,*},"Deslize Tarjeta CRM WOW", \
            0,0,Clave_ID_TrackI{-m1,*},"Deslize Tarjeta CRM WOW1:"

            WindowInput
            WindowClose

            If @MAGSTATUS = "Y"
                call Delete_Miembro_Ocupa_Pos(Wsid_wow)

                Split Clave_ID, "=", Clave_ID_Paso1, Clave_ID_Paso2
                ClearArray Clave_Id_Track_II
                ClearArray Clave_Id_Track_II_1
                Split Clave_ID_TrackI, "^", Clave_Id_Track_II[1], Clave_Id_Track_II[2], Clave_Id_Track_II[3]

                If Len(Clave_Id_Track_II[2]) > 0
                    Split Clave_Id_Track_II[2], "/", Clave_Id_Track_II_1[1], Clave_Id_Track_II_1[2], Clave_Id_Track_II_1[3]
                EndIf

                If   Mid(Clave_ID,1,1) = "A;" And Mid(Clave_ID,16,1) = "B?"  //No se usa quitar
                ElseIf Mid(Clave_ID_Paso1,1,1) = ";" And Len(Clave_ID_Paso2) > 0
                    Var Len1  : N4 = 0
                    Len1 = ( Len(Clave_ID_Paso1) - 1 )
                    Format No_tarjeta    As Mid(Clave_ID_Paso1,2,Len1)
                    Format Campo_Busqueda As No_tarjeta
                ElseIf Clave_Id_Track_II_1[3] = "ALSEAWOW" Or Clave_Id_Track_II_1[3] = "ALSE"
                    Var Len1  : N4 = 0
                    Len1 = ( Len(Clave_ID_Paso1) - 1 )
                    Format No_tarjeta    As Clave_ID_Paso1
                    Format Campo_Busqueda As No_tarjeta
                Else
                    Format No_tarjeta    As Mid(Clave_ID,30,1)
                    Format No_tarjeta    As No_tarjeta, Mid(Clave_ID,32,2)
                    Format No_tarjeta    As No_tarjeta, Mid(Clave_ID, 7,1)
                    Format No_tarjeta    As No_tarjeta, Mid(Clave_ID, 8,8)
                    Format No_tarjeta    As No_tarjeta, Mid(Clave_ID,34,4)
                    Format Campo_Busqueda As No_tarjeta
                EndIf
            
            Else
                Campo_Busqueda = Clave_ID
            EndIf
        EndIf
    Else
        If @RVC = 1  Or @RVC = 2 Or @RVC = 4  Or @RVC = 5
            TouchScreen Numero_TouchscreenN
            Window  1, 4, "WOW"
            DisplayMsInput 0,0,Clave_ID{-m2,*},"Deslize Tarjeta CRM WOW2:", \
            0,0,Clave_ID_TrackI{-m1,*},"Deslize Tarjeta CRM WOW3:", \ 
            0,0,Clave_ID_TrackII{m2, 1, 1, *},"Deslize Tarjeta CRM WOW4:" 

            WindowInput
            WindowClose

            If @MAGSTATUS = "Y"
                call Delete_Miembro_Ocupa_Pos(Wsid_wow) 
                metodo_entrada = "Tarjeta"

                Format Clave_ID_TrackII_Origin as Clave_ID_TrackII 
                mid(Clave_ID_TrackII,1,12) = "XXXXXXXXXXXX"

                Split Clave_ID, "=", Clave_ID_Paso1, Clave_ID_Paso2

                ClearArray Clave_Id_Track_II
                ClearArray Clave_Id_Track_II_1

                Split Clave_ID_TrackI, "^", Clave_Id_Track_II[1], Clave_Id_Track_II[2], Clave_Id_Track_II[3]
                    If Len(Clave_Id_Track_II[2]) > 0
                        Split Clave_Id_Track_II[2], "/", Clave_Id_Track_II_1[1], Clave_Id_Track_II_1[2], Clave_Id_Track_II_1[3]
                    EndIf

                    if Clave_Id_Track_II_1[3] = "ALSEAWOW" Or Clave_Id_Track_II_1[3] = "ALSE" 
                    else
                        InfoMessage "Tarjeta no es WOW"       
                        ExitCancel
                    endif 

                    If Mid(Clave_ID,1,1) = ";" And Mid(Clave_ID,16,1) = "?"
                        Format No_tarjeta    As Mid(Clave_ID,2,14)
                        Format Campo_Busqueda As No_tarjeta
                    ElseIf Mid(Clave_ID,1,1) = ";" And Len(Clave_ID_Paso2) > 0
                        Format No_tarjeta    As Mid(Clave_ID,2,16)
                        Format Campo_Busqueda As No_tarjeta
                    ElseIf Clave_Id_Track_II_1[3] = "ALSEAWOW" Or Clave_Id_Track_II_1[3] = "ALSE"
                        Var Len1  : N4 = 0
                        Len1 = ( Len(Clave_ID_Paso1) - 1 )
                        Format No_tarjeta    As Clave_ID_Paso1
                        Format Campo_Busqueda As No_tarjeta
                    Else
                        Format No_tarjeta    As Mid(Clave_ID,30,1)
                        Format No_tarjeta    As No_tarjeta, Mid(Clave_ID,32,2)
                        Format No_tarjeta    As No_tarjeta, Mid(Clave_ID, 7,1)
                        Format No_tarjeta    As No_tarjeta, Mid(Clave_ID, 8,8)
                        Format No_tarjeta    As No_tarjeta, Mid(Clave_ID,34,4)
                        Format Campo_Busqueda As No_tarjeta
                    EndIf
                Else
                    metodo_entrada = "Smartphone" 
                    Campo_Busqueda = Clave_ID
                EndIf

                ElseIf @RVC = 5  
                    ForEver
                    TouchScreen Numero_TouchscreenN
                    Input Clave_ID, "Digite # Tarjeta CRM WOW"
                If @MAGSTATUS = "Y"
                    InfoMessage "ERROR DE CAPTURA", "DIGITE MANUALMENTE EL NUMERO DE TARJETA"
                Else
                    Campo_Busqueda = Clave_ID
                    Break
                EndIf
            EndFor
        EndIf
    EndIf

    If Len(Trim(Campo_Busqueda)) = 0
        ExitContinue
    EndIf

    Tarjeta_wow = Campo_Busqueda
    Call Consulta_Miembro_Ocupa_Pos(Tarjeta_wow) 

    If numero_Pos = 0 and banderainfowow = 0
        Call Inserta_Miembro_Ocupa_Pos(Tarjeta_wow, Wsid_wow)
    ElseIf Wsid_wow = numero_Pos or banderainfowow = 1
    Else
        InfoMessage "CRM WOW", "YA EXISTE TARJETA LIGADA A UN CHEQUE EN ESTE MOMENTO"
        ExitCancel
    EndIf

    if No_tarjeta = "0"
        format No_Tarjeta_WOW as Clave_ID
    else
        format No_Tarjeta_WOW as No_tarjeta
    endif
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       SUBRUTINA Manda_Cliente_Nombre                          //
// MANDA DATOS A SOA PARA INFO SOCIO                                             //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
Sub Manda_Cliente_Nombre
    Manda = ""
    
    Format Miembro_O_Archivo as Campo_Busqueda
    
    Numero_Tarjeta = Campo_Busqueda
    Format Manda, Separador as "CLN", Campo_Busqueda, Business_Date_Real, Tienda, @RVC, @CKNUM
    TxMsg Manda
    GetRxmsg "Esperando Info. Cliente WOW"
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       SUBRUTINA Valida_Respuesta                              //
// VALIDA LA RESPUESTA DE SOA                                                    //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Valida_Respuesta
    Var Campo_01_ : A50 = ""
    Var Campo_02_ : A50 = ""
    Salida_Consulta_Clientes = ""
    Format Campo_01_ as "<WS_TIMEOUT>"
    Format Campo_02_ as "<Success>"

    If Error_Time_Out_SbuxCard = "E" or Error_Time_Out_SbuxCard = "F"
    EndIf

    If Mid(@RxMsg, 1, 32 ) = "invokeButton_Automatico (Error):" 
        If @RVC = 2
            LoadKyBdMacro Key (19,102)
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
            Salida_Consulta_Clientes = "T"
        Else
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
        EndIf
        Call Graba_Off_Line_CRM
        ExitCancel
    EndIf

    If Mid(Trim(@RxMsg),1,17) = "NO EXISTE MIEMBRO"
    
        Split @RxMsg, "|", Resp_Red[1], Resp_Red[2]
        Format Campo_01_ as "<WS_SOA_ERROR>"
        Format Campo_02_ as "<", Resp_Red[2], ">"

        If @RVC = 2
            LoadKyBdMacro Key (19,102)
            InfoMessage Mid(@RxMsg, 1, 32 )
            Salida_Consulta_Clientes = "T"
        Else
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
            ExitCancel
        EndIf

    ElseIf Mid(Trim(@RxMsg),1, 26)  = "NO HAY COMUNICACION CON WS"
	
        Format No_Tarjeta_WOW as No_Tarjeta_WOW_anterior
        Split @RxMsg, "|", Resp_Red[1], Resp_Red[2]

        if Resp_Red[2] = ""

            Format Campo_01_ as "<WS_OFFLINE>"
            Format Campo_02_ as "<Success>"

            Call Graba_Off_Line_CRM

            if infoSocio = 0
                offline=1
                ClearChkInfo 
                LoadKyBdMacro Key(1, 327681) //manda pantalla compra
                Format Campo as " Offline "
                SaveChkInfo Campo
                Format Campo as "off ",Clave_ID_TrackII
                SaveChkInfo Campo
                infoMessage "Modo Offline"
            endif

            ExitContinue 

            If @RVC = 2
                    LoadKyBdMacro Key (19,102)
                    Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
                    Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
                    Salida_Consulta_Clientes = "T"
            Else
                Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
                Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
            EndIf

        else
            Format Campo_01_ as "<WS_SOA_ERROR>"
            Format Campo_02_ as "<", Resp_Red[2], ">"

            call imprime_ticket_tarjeta_no_inscrita 

            If @RVC = 2
                LoadKyBdMacro Key (19,102)
                InfoMessage Mid(@RxMsg, 1, 32 )
                Salida_Consulta_Clientes = "T"
            Else
                Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
                Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
                ExitCancel
            EndIf
        endif 

    ElseIf Trim(@RxMsg) = "_timeout"
    Format No_Tarjeta_WOW as No_Tarjeta_WOW_anterior
        Format Campo_01_ as "<WS_TIMEOUT>"
        Format Campo_02_ as "<Success>"
        Call Graba_Off_Line_CRM
        
        If @RVC = 2
            LoadKyBdMacro Key (19,102)
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
            Salida_Consulta_Clientes = "T"
        Else
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
        EndIf
        
        if infoSocio = 0
            offline=1
            ClearChkInfo 
            LoadKyBdMacro Key(1, 327681) //manda pantalla compra
            Format Campo as " Offline "
            SaveChkInfo Campo
            Format Campo as "off ",Clave_ID_TrackII
            SaveChkInfo Campo
        endif
        ExitCancel 

    ElseIf Trim(@RxMsg) = "No es posible leer los datos de la tarjeta."
        if len(No_Tarjeta_WOW_anterior) > 0
          Format No_Tarjeta_WOW as No_Tarjeta_WOW_anterior
        endif
        Split @RxMsg, "|", Resp_Red[1], Resp_Red[2]
        Format Campo_01_ as "<WS_SOA_ERROR>"
        Format Campo_02_ as "<", Resp_Red[2], ">"

        If @RVC = 2
            LoadKyBdMacro Key (19,102)
            InfoMessage Mid(@RxMsg, 1, 32 )
            Salida_Consulta_Clientes = "T"
        Else
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
            ExitCancel
        EndIf
    EndIf
    
    Format Respuesta as @RxMsg
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       SUBRUTINA Separa_Campos                                 //
// SEPARA LOS DATOS DE SOA EN VARIABLES                                          //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Separa_Campos

    Var JJ                      : N3 = 0
    Var KK                      : N3 = 0
    Var AnioDate                  : N4 = 0
    Var AnioDate_A              : A4 = ""
    Var BirthDate2              : A30 = ""
    Var FN002                    : N6 = 0
    Call Borra_Array
    BirthDate = ""

    Split Respuesta, "|", NumeroMiembro, Nombre, Activo, Nivel, Organizacion, Nivel_Id, Nivel_nombre, \
    Contador_Campos,  PromocionId[]: PromocionNombre[]: PromocionUniqueKey[]: \
    PromocionDisplayName[]: PromocionStatus[], \
    Contador_Voucher, VoucherCalStatus[]: VoucherProductId[]: VoucherProductName[]: \
    VoucherStatus[]: VoucherTransactionId[]: validDiscountAmount[]:  validDiscountType[]: minimumAmount[], \
    BirthDate, FavoriteProduct, LastBeverage, LastFood, LifetimePoint1Value, LifetimePoint2Value, \
    Status, AttributeValue, ErrorCode, pointAmount

    If Len(Activo) = 0
        Activo = Status
    EndIf

    puntoswow= LifetimePoint2Value

    AttributeValue = "Always" 

    If Len(Trim(BirthDate)) > 0
        MesBirthDate = Mid(BirthDate, 1, 2)
        DiaBirthDate = Mid(BirthDate, 4, 2)
    ENDIF

    Format AnioBirthDate_A as "20", @YEAR{02}
    AnioBirthDate           = AnioBirthDate_A

    Format AnioDate_A as "20", @YEAR{02}
    AnioDate = AnioDate_A
    if @MONTH = MesBirthDate And DiaBirthDate > @DAY
        AnioDate = ( AnioDate )
    Else
        AnioDate = ( AnioDate + 1 )
    EndIf
    Format Fecha_Cumplanios_A as "20", @YEAR{02}, "-", @MONTH{02}, "-", @DAY{02}
    Format Fecha_Cumplanios_D as AnioDate, "-", MesBirthDate, "-", DiaBirthDate

    If Len(Trim(AttributeValue)) > 0
        If AnioBirthDate_A = Trim(AttributeValue)
            For JJ = 1 to Contador_Campos
                If Trim(PromocionNombre[JJ]) = "Beb Cumple"
                EndIf
            EndFor
            For JJ = 1 to Contador_Voucher
                If Trim(VoucherProductName[JJ]) = "Beb Cumple"
                EndIf
            EndFor
        Else
    EndIf
    Else
        If @MONTH <> MesBirthDate
            For JJ = 1 to Contador_Campos
                If Trim(PromocionNombre[JJ]) = "Beb Cumple"
                EndIf
            EndFor
            For JJ = 1 to Contador_Voucher 
                If Trim(VoucherProductName[JJ]) = "Beb Cumple"
                EndIf
            EndFor
        Else
        EndIf
    EndIf
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       SUBRUTINA Carga_Miembro_Cheque                          //
// CARGA LOS DATOS DEL MIENBRO EN EL TICKET                                      //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Carga_Miembro_Cheque
    Var Conta  : N3  = 0
    Var Puntos_: N3  = 0

    If Consulta_Pantalla <> "S"
        ClearChkInfo
    Else
        Window 12,48
    EndIf

    Format Campo as Mid(Nombre, 1, 24)
    Call Carga_Screen_Cheque

    Format Campo as Trim(Activo), "/", Trim(Nivel), "", Mid(BirthDate,1,5) 
    Call Carga_Screen_Cheque

    If Len(trim(Saldo)) > 0
        Format Campo as "Saldo ", Saldo
        Call Carga_Screen_Cheque
    EndIf

    Format Campo as "Points antes de esta compra; ", LifetimePoint2Value
    Call Carga_Screen_Cheque
    Call Write_Leyenda_CRM_Cheque(Campo)
    
    Format Campo as "Pesos: ", pointAmount
    Call Carga_Screen_Cheque
    LifetimePoint2Value = pointAmount

    If Len(Trim(FavoriteProduct)) > 0
    EndIf

    If Len(Trim(LastBeverage)) > 0
        Format Campo as "B:", Mid(LastBeverage, 1, 22)
        Call Carga_Screen_Cheque
    EndIf

    If Len(Trim(LastFood)) > 0
        Format Campo as "F:", Mid(LastFood,     1, 22)
        Call Carga_Screen_Cheque
    EndIf

    Contador_Redenciones = 0
    ClearArray Redencion_Aplicada

        For I = 1 to 11

            If Trim(VoucherStatus[I]) = "Available" And Len(Trim(VoucherProductName[I])) > 0 
                Format Campo as " ", Mid(VoucherProductName[I], 1, 17)
                Format Campo_ as Mid(VoucherProductName[I], 1, 16)
                Format AnioBirthDate_A as "20", @YEAR{02}
                Call Guarda_en_Cheque
            ENDIF
            Else
                Puntos_ = 0
                Call Carga_Miembro_Cheque_Nivel_Screen(Campo_, Puntos_)  
                If Puntos_ = 0
                    Call Guarda_en_Cheque
                    EndIf
                ENDIF
            ENDIF
        EndFor

        If Conta > 0 
            Contador_Redenciones = Conta
            Call Graba_Privilegios_Nivel_Miembro
        EndIf
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       SUBRUTINA Carga_Screen_Cheque                           //
// GENERA LA LEYENDA DEL TRAILER IMPRESION_WO                                    //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//


Sub Carga_Screen_Cheque
    Var Campo_  : A100 = ""
    Var Campo2  : A100 = ""
    Var Camp_01 : A20  = ""
    Var Camp_02 : A20  = ""
    If Mid(Trim(Campo),1,5) = "Stars" Or Mid(Trim(Campo),1,5) = "Point"
        Format Campo_ As Mid(Campo, 1, 6), ":", Trim(Mid(Campo, 29, 10))
        Campo = Campo_

        Call Borra_Leyenda_CRM
        Format Campo2 as "==============================="
        Call Write_Leyenda_CRM(Campo2)
        Format Campo2 as "            Gracias"
        Call Write_Leyenda_CRM(Campo2)
        Format Campo2 as "          por ser socio"
        Call Write_Leyenda_CRM(Campo2)
        Format Campo2 as "          por ser socio"
        Call Write_Leyenda_CRM(Campo2)
        Format Campo2 as "           WOW Rewards"
        Call Write_Leyenda_CRM(Campo2)
        Format Campo2 as "   Gana 1 punto WOW por cada"
        Call Write_Leyenda_CRM(Campo2)
        Format Campo2 as "         $10 de consumo"
        Call Write_Leyenda_CRM(Campo2)
        Format Campo2 as "==============================="
        Call Write_Leyenda_CRM(Campo2)
    EndIf

    If Consulta_Pantalla <> "S"
        SaveChkInfo Mid(Trim(Campo),1,20)
    Else
        Call Muestra_Datos_Pantalla(Campo)
    EndIf
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       SUBRUTINA Muestra_Datos_Pantalla                        //
// MUESTRA EN PANTALLA LOS DATOS DEL INFO SOCIO                                  //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Muestra_Datos_Pantalla(Ref datos_)
    Contador_Lineas_Pantalla = ( Contador_Lineas_Pantalla + 1 )

    If Contador_Lineas_Pantalla < 13
        Display Contador_Lineas_Pantalla, 1, datos_
    EndIf
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       SUBRUTINA Guarda_en_Cheque                              //
// ELIGE QUE PANTALLA MOSTRARAS, SEA INFO SOCIO O REDENCION                      //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Guarda_en_Cheque
    Conta = ( Conta + 1 )
    If Consulta_Pantalla = "S"
        Call Muestra_Datos_Pantalla(Campo)
    Else
        If Conta < 12
            Redencion_Aplicada[Conta] = Campo_
            SaveChkInfo Campo
        EndIf
    EndIf
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       SUBRUTINA Graba_Miembro                                 //
// GUARDA LOS DATOS DE RESPUESTA DE SOA DEL CONSULTA SOCIO                       //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Graba_Miembro
    Var FN001   : N6 = 0
    Var File1   : A100 = ""
    
    Cheque_Miembro = @CKNUM
    Format rspst_Miembro as Respuesta
    Cheque_Miembro_Soa = @CKNUM
    Format rspst_Miembro_Soa as Respuesta
Endsub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       SUBRUTINA Lee_Miembro                                   //
// SEPARA LOS DATOS DE LA RESPUESTA SOA DE CONSULTA SOCIO                        //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Lee_Miembro
    Var FN001 : N6 = 0
    Var JJ  : N6 = 0

    Call Borra_Array

    Var File1       : A100 = ""
    Contador_Redenciones = 10
    Cheque = Cheque_Miembro
    Format Respuesta as rspst_Miembro

    ALL = ""

    If Mid(Respuesta,1,3) = "ALL" 
        Split Respuesta, "|", NumeroMiembro, Nombre
        ALL = "ALL"
    Else
        Split Respuesta, "|", NumeroMiembro, Nombre, Activo, Nivel, Organizacion, Nivel_Id, Nivel_nombre, \
        Contador_Campos,  PromocionId[]: PromocionNombre[]: PromocionUniqueKey[]: \
        PromocionDisplayName[]: PromocionStatus[], \
        Contador_Voucher, VoucherCalStatus[]: VoucherProductId[]: VoucherProductName[]: \
        VoucherStatus[]: VoucherTransactionId[]: validDiscountAmount[]:  validDiscountType[] : minimumAmount[], \
        BirthDate, FavoriteProduct, LastBeverage, LastFood, LifetimePoint1Value, LifetimePoint2Value, \
        Status, AttributeValue, ErrorCode, pointAmount
    EndIf

    If Len(Trim(BirthDate)) > 0
        MesBirthDate = Mid(BirthDate, 1, 2)
    ENDIF
    If @MONTH <> MesBirthDate
        For JJ = 1 to Contador_Campos
            If Trim(PromocionNombre[JJ]) = "Beb Cumple"
                PromocionNombre[JJ] = ""
            Else
            EndIf
        EndFor
        For JJ = 1 to Contador_Voucher
            If Trim(VoucherProductName[JJ]) = "Beb Cumple"
                VoucherProductName[JJ] = ""
            Else
            EndIf
        EndFor
    Else
    EndIf
Endsub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       SUBRUTINA Borra_Miembro                                 //
// BORRA LOS DATOS DE LA RESPUESTA SOA                                           //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Borra_Miembro
    Cheque_Miembro = 0
    rspst_Miembro = ""
Endsub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       SUBRUTINA Consulta_Business_Date_SyBase                 //
// CONSULTA LA BUSINESS DATE DE LA BASE DE DATOS                                 //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Consulta_Business_Date_SyBase
    Format SqlStr as "SELECT  business_date, (SELECT obj_num  FROM micros.rest_def) as rest_def  FROM micros.rest_status"
    Call Consulta_Sql

    IF SqlStr1 = ""
        Business_Date = ""
    Else
        IF Len(Trim(SqlStr1)) > 0 Then
            Split SqlStr1, ";", Business_Date, Tienda
            IF Version_Micros = 31
                Format DiaB  as Mid(Business_Date, 4, 2)
                Format MesB  as Mid(Business_Date, 1, 2)
                Format AnioB as Mid(Business_Date, 7, 4)
                Format Business_Date_Real as Mid(Business_Date,1,10)
                Format Business_Date      as AnioB, "-" , MesB, "-" , DiaB
                Format Business_Date      as MesB, "/" , DiaB, "/" , AnioB, " ", @HOUR{02}, ":", @MINUTE{02}, ":", @SECOND{02}
            Else
                Format DiaB  as Mid(Business_Date, 9, 2)
                Format MesB  as Mid(Business_Date, 6, 2)
                Format AnioB as Mid(Business_Date, 1, 4)
                Format Business_Date_Real as Mid(Business_Date,1,10)
                Format Business_Date      as AnioB, "-" , MesB, "-" , DiaB
                Format Business_Date      as MesB, "/" , DiaB, "/" , AnioB, " ", @HOUR{02}, ":", @MINUTE{02}, ":", @SECOND{02}
            ENDIF
        ENDIF
    ENDIF

    Call UnloadDLL_SyBase
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       SUBRUTINA Continuacion                                  //
// FUNCION PARA LLAMAR UN INQ                                                    //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Continuacion(var A: N5, var B: N11)
    LoadKyBdMacro Key(24, (16384 * B) + A)
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       SUBRUTINA Version_Micros_SyBase                         //
// CHECA LA VERSION DE MICROS USADA                                              //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Version_Micros_SyBase
    Version_Micros = 0
    Format Version_Micros as Mid(Trim(@version), 1,1), Mid(Trim(@version), 3,1)
    
    If Version_Micros = 52
        Version_Micros = 41
    Else
        Version_Micros = 41
    EndIf
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       SUBRUTINA Conecta_Base_Datos_Sybase                     //
// ABRE LA CONEXION DE LA BASE DE DATOS                                          //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Conecta_Base_Datos_Sybase
    Call Version_Micros_Sybase
    Call Load_ODBC_DLL_SyBase
    Call ConnectDB_SyBase
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       SUBRUTINA Load_ODBC_DLL_SyBase                          //
// BUSCA LA DLL PARA LA CONEXION A LA BASE DE DATOS                              //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//


Sub Load_ODBC_DLL_SyBase
    IF hODBCDLL <> 0
        hODBCDLL  = 0
    ENDIF
    IF hODBCDLL = 0
        IF Version_Micros = 32 OR Version_Micros = 41
            IF @WSTYPE = 3
                DLLFree hODBCDLL
                DLLLoad hODBCDLL, "\cf\micros\bin\MDSSysUtilsProxy.dll"
            Else
                DLLFree hODBCDLL
                DLLLoad hODBCDLL, "MDSSysUtilsProxy.dll"
            ENDIF
        ElseIF Version_Micros = 31
            DLLLoad hODBCDLL, "SimODBC.DLL"
        ENDIF
    ENDIF

    IF   hODBCDLL = 0 and Version_Micros = 32
        ExitWithError "No se Puede Cargar DLL (MDSSysUtilsProxy.DLL)"
    ElseIF hODBCDLL = 0 and Version_Micros = 41
        ExitWithError "No se Puede Cargar DLL (MDSSysUtilsProxy.DLL)"
    ElseIF hODBCDLL = 0 and Version_Micros = 31
        ExitWithError "No se Puede Cargar DLL (SimODBC.DLL)"
    ENDIF
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       SUBRUTINA ConnectDB_SyBase                          //
// CONECTA A LA BASE DE DATOS PASANDOLE LAS CREDENCIALES                         //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//


Sub ConnectDB_SyBase
    Var Status : N1

    IF Version_Micros = 32
        DLLCALL_CDECL hODBCDLL, sqlIsConnectionOpen(ref Status)
        IF Status =  0
            DLLCALL_CDECL hODBCDLL, sqlInitConnection("micros","ODBC;UID=dba;PWD=Password1", "")
        ENDIF
    ElseIF Version_Micros = 41
        DLLCALL_CDECL hODBCDLL, sqlIsConnectionOpen(ref Status)
        IF Status =  0
            DLLCALL_CDECL hODBCDLL, sqlInitConnection("micros","ODBC;UID=dba;PWD=Password1", "")
        ENDIF
    ElseIF  Version_Micros = 31
        DLLCall hODBCDLL, sqlIsConnectionOpen(ref Status)
        IF Status =  0
            DLLCall hODBCDLL, sqlInitConnection("micros","UID=dba;PWD=Password1")
        ENDIF
    ENDIF
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       SUBRUTINA UnloadDLL_SyBase                              //
// CIERRA LA CONEXION A LA BASE DE DATOS                                         //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub UnloadDLL_SyBase

    IF   Version_Micros = 32
        DLLCALL_CDECL hODBCDLL, sqlCloseConnection()
    ElseIF Version_Micros = 31
        DLLCall hODBCDLL, sqlCloseConnection()
    ElseIF Version_Micros = 41
        DLLCALL_CDECL hODBCDLL, sqlCloseConnection()
    ENDIF
    DLLFree  hODBCDLL
    hODBCDLL = 0
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       SUBRUTINA Muestra_Error                                 //
// CREA LA VENTANA QUE MUESTRA EL MENSAJE DE ERROR                               //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Muestra_Error(Ref Datos_w)
    Window 5,78
    Display 1, 1, Mid(Datos_w,  1, 78)
    Display 2, 1, Mid(Datos_w,  79, 78)
    Display 3, 1, Mid(Datos_w, 156, 78)
    Display 4, 1, Mid(Datos_w, 234, 78)
    TouchScreen Numero_TouchscreenN 
    Input G, "[Enter] para Continuar"
    WindowClose
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       SUBRUTINA Borra_Array                                   //
// BORRA LA INFORMACION DE LOS ARREGLOS                                          //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Borra_Array
    ClearArray PromocionId
    ClearArray PromocionNombre
    ClearArray PromocionUniqueKey
    ClearArray PromocionDisplayName
    ClearArray PromocionStatus

    ClearArray VoucherCalStatus
    ClearArray VoucherProductId
    ClearArray VoucherProductName
    ClearArray VoucherStatus
    ClearArray VoucherTransactionId
    ClearArray validDiscountAmount
    ClearArray validDiscountType
    ClearArray minimumAmount
    ClearArray Redencion_Aplicada
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       SUBRUTINA Verifica_Item_Seleccionado                    //
// VERIFICA EL ITEM SELECCIONADO EN EL CHEQUE                                    //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//


Sub Verifica_Item_Seleccionado
    Condimentos       = 0
    MenuItems           = 0

    For I=1 to @NumDTLT
        Grupo         = mid(@DTL_STATUS[I],5,1)
        If Grupo = "A"
            Grupo_Numerico = 10
        ElseIF Grupo = "B"
            Grupo_Numerico = 11
        ElseIF Grupo = "C"
            Grupo_Numerico = 12
        ElseIF Grupo = "D"
            Grupo_Numerico = 13
        ElseIF Grupo = "E"
            Grupo_Numerico = 14
        ElseIF Grupo = "F"
            Grupo_Numerico = 15
        Else
            Grupo_Numerico = Grupo
        EndIf
        If @dtl_type[I] = "M" and @dtl_Is_Void[I] = 0
            If @dtl_Selected[I] = 1
                If @Dtl_Is_Cond[I]  = 0  //MENU ITEM
                    If @Dtl_Ttl[I]  > 0
                        Unid_Seleccionado = ( Unid_Seleccionado + 1 )
                        MenuItems = ( MenuItems + 1 )
                        Linea_Cheque = I
                        Call Acumula_Item_Seleccionado
                    Else
                    EndIf
                Else                         //CONDIMENTO
                    If @Dtl_Is_Cond[I]  = 1 and @Dtl_Ttl[I] > 0
                        Unid_Seleccionado = ( Unid_Seleccionado + 1 )
                        Condimentos     = ( Condimentos + 1 )
                        Linea_Cheque = I
                        Call Acumula_Item_Seleccionado
                    Else
                        If MenuItems = 0
                            If Condimentos = 1
                            Else
                            EndIf
                        EndIf
                    EndIf
                EndIf
            EndIf
        EndIf
    EndFor

    If MenuItems    > 1
        ExitWithError "SOLO SE PERMITE SELECCIONAR UN SOLO ARTICULO"
    EndIf

    If MenuItems    = 0 And Condimentos > 1
        ExitWithError "SOLO SE PERMITE SELECCIONAR UN SOLO MODIFICADOR"
    EndIf

    If Unid_Seleccionado = 0
        ExitWithError "NO HAY ARTICULO SELECCIONADO"
    EndIf
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       SUBRUTINA Acumula_Item_Seleccionado                     //
// ACUMULA LOS ITEMS SELECCIONADOS                                               //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Acumula_Item_Seleccionado
    If    MenuItems = 0 and Condimentos = 1
        Call Acumula_Item_Seleccionado_Item
    ElseIf  MenuItems = 1 and Condimentos = 0
        Call Acumula_Item_Seleccionado_Item
    ElseIf  MenuItems = 1 and Condimentos > 0
        Item_Precio_Seleccionado = ( Item_Precio_Seleccionado + @dtl_TTL[I] )
    EndIf
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       SUBRUTINA Acumula_Item_Seleccionado_Item                //
// GUARDA LOS DATOS DEL ITEM SELECIONADO EN VARIABLES                            //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Acumula_Item_Seleccionado_Item
    Item_No_Seleccionado     = ( @dtl_object[I] )
    Item_Nombre_Seleccionado = ( @dtl_name[I]   )
    Item_Qty_Seleccionado   = ( @dtl_Qty[I] )
    Item_Precio_Seleccionado = ( @dtl_TTL[I]    )
    Sub_Nivel_Seleccionado  = ( @dtl_Slvl[I]    )
    Main_Nivel_Seleccionado  = ( @Dtl_Mlvl[I]   )
    Item_Linea_Seleccionado  = ( I            )
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       SUBRUTINA Consulta_Configuracion_CRM                    //
// CONSULTA LOS DATOS DE LA TABLA DE CONFIGURACION CRM                           //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Consulta_Configuracion_CRM( ref Etiqueta_Crm )
  Format SqlStr as "SELECT Dato_Crm FROM custom.configuracion_crm WHERE Etiqueta_Crm = '",Etiqueta_Crm, "'"
  Call Consulta_Sql
  
  Call UnloadDLL_SyBase
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//               SUBRUTINA Main_Busca_Articulos_Enviar_Acreditaciones            //
// EMPIEZA EL FLUJO DE ACREDITACION                                              //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//


Sub Main_Busca_Articulos_Enviar_Acreditaciones
    Var SNivel_R        : A10  = ""
    Var Price_R      : $10 = 0
    Var Number_R        : A10 = 0
    Var Nombre_R        : A20 = ""
    Var Unidades_R    : N4  = 0
    Var Cheque_R        : N4  = 0
    Var SNivel_R2       : A10  = ""
    Var Price_R2        : $10 = 0
    Var Number_R2       : A10 = 0
    Var Nombre_R2       : A20 = ""
    Var Unidades_R2  : N4  = 0
    Var Cheque_R2       : N4  = 0
    Var I               : N4  = 0
    Var Cons_Item       : N4  = 0
    Var Primera_Cero    : A1  = "F"
    Var Total_Ticket    : $10 = @TTLDUE
    Var Contador_OP  : N2  = 0
    Var Total_Paso    : $10 = 0
    Lineas_Acreditas_Contador = 0

    Condimento_Aplicado = "F"
    Name_FP           = ""

    Call Busca_Formas_Pago_Acreditaciones_Soa
    Call Busca_Aportacion_Redencion_Soa 

    ClearArray Objeto_Descto
    ClearArray Nivel_Descto
    ClearArray Precio_Descto
    ClearArray Nombre_Descto
    ClearArray Unidades_Descto
    ClearArray Linea_Descto
    Call Consolidado_Descuentos_Acreditaciones_Soa_Wow_2
    Call Busca_MenuItem_Acreditaciones_Soa

    If Lineas_Acreditas_Contador = 0  //NO HAY NADA QUE ENVIAR --> CONTINUA
        ExitContinue
    EndIf

    Call Manda_Acreditacion

    If Salir_ACR = "T"
        Return
    EndIf

    Call Valida_Acreditacion

    If Trim(FP_Sbux_Aplicado_Off_Line) = "T"
        Status_Respuesta = "F"
        Call Read_Off_Line_CRM
        If Len(Trim(TicketNumber_Off)) = 0
            If Len(Trim(TicketNumber))  = 0
                Return
            EndIf
        EndIf

    If Len(Trim(TicketNumber)) = 0
        Return
    EndIf
        Call Manda_Ejecuta_Acreditaciones_OffLine_WS_JAR
    ElseIf Lineas_Acreditas_Contador = 0
    Else
        Name_Jar = "ACREDITACIONES"
    EndIf
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//               SUBRUTINA Arma_Registro_Acreditaciones_Redenciones              //
// ARMA LOS DATOS DE ACREDITACION PARA SER ENVIADOS A SOA                        //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//


Sub Arma_Registro_Acreditaciones_Redenciones
    Var Arma_Cadena_1  : A1000 = ""
    var tabla: A50=""
    
    Format Name_Cajero As @Tremp, " ", Trim(@TREMP_FNAME), " ", Trim(@TREMP_LNAME)
    
    If offline=1
        format tabla as "custom.LOYTRANSACCION_OFFLINE" 
    Else
        format tabla as "custom.LOYTRANSACCION"             
    Endif

    Format Arma_Cadena_1 as "INSERT INTO ",tabla," ( MemberNumber, id,ALSELevelPrice,BranchDivision,ALSEPaymnetMode,ActivityDate,AdjustedListPrice,Amount,Comments,ItemNumber,LocationName,PartnerName,PointName,Points,ProcessDate,ProcessingComment,ProcessingLog,ProductName,Quantity,IntegrationStatus,Status,SubStatus,TicketNumber,TransactionChannel,TransactionDate,TransactionSubType,TransactionType,VoucherNumber,VoucherQty,VoucherType,Organization,Estatus, Bussiness_date, centroconsumo, Cheque, Transmitido, NumeroLineaMicros, cardNumber, Cajero ) VALUES ( "
    Format Arma_Cadena_2 as "'", MemberNumber, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", id, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", ALSELevelPrice, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", BranchDivision, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", ALSEPaymnetMode, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", ActivityDate, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", AdjustedListPrice, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Amount, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Comments, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", ItemNumber, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", LocationName, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", PartnerName, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", PointName, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Points, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", ProcessDate, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", ProcessingComment, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", ProcessingLog, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", ProductName, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Quantity, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", IntegrationStatus, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Status, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", SubStatus, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", TicketNumber, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", TransactionChannel, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", TransactionDate, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", TransactionSubType, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", TransactionType, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", VoucherNumber, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", VoucherQty, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", VoucherType, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Organization, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Estatus, "', "
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Bussiness_date, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Centroconsumo, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Cheque, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Transmitido, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Linea_Cheque, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Clave_ID_TrackII_Origin, "'," //test 10/08/2015
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Name_Cajero, "' "
    Format Arma_Cadena_2 as Arma_Cadena_2, " )"
    Format Arma_Cadena  as Arma_Cadena_1, Arma_Cadena_2
    Format Arma_Cadena_22 as Arma_Cadena_2
EndSub
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Arma_Registro_Acreditaciones_Redenciones_CRM_Log              //
// ARMA LOS DATOS DE ACREDITACION PARA SER GUARDADOS LA TABLA CRM_Acreditacion_WS//
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Arma_Registro_Acreditaciones_Redenciones_CRM_Log
    Var Arma_Cadena_1  : A1000 = ""
    Format Name_Cajero As @Tremp, " ", Trim(@TREMP_FNAME), " ", Trim(@TREMP_LNAME)
    Format Arma_Cadena_1 as "INSERT INTO custom.CRM_Acreditacion_WS ( MemberNumber, id,ALSELevelPrice,BranchDivision,ALSEPaymnetMode,ActivityDate,AdjustedListPrice,Amount,Comments,ItemNumber,LocationName,PartnerName,PointName,Points,ProcessDate,ProcessingComment,ProcessingLog,ProductName,Quantity,IntegrationStatus,Status,SubStatus,TicketNumber,TransactionChannel,TransactionDate,TransactionSubType,TransactionType,VoucherNumber,VoucherQty,VoucherType,Organization,Estatus, Cajero ) VALUES ( "
    Format Arma_Cadena_2 as "'", MemberNumber, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", id, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", ALSELevelPrice, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", BranchDivision, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", ALSEPaymnetMode, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", ActivityDate, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", AdjustedListPrice, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Amount, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Comments, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", ItemNumber, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", LocationName, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", PartnerName, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", PointName, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Points, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", ProcessDate, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", ProcessingComment, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", ProcessingLog, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", ProductName, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Quantity, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", IntegrationStatus, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Status, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", SubStatus, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", TicketNumber, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", TransactionChannel, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", TransactionDate, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", TransactionSubType, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", TransactionType, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", VoucherNumber, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", VoucherQty, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", VoucherType, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Organization, "',"
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Estatus, "', "
    Format Arma_Cadena_2 as Arma_Cadena_2, "'", Name_Cajero, "' "
    Format Arma_Cadena_2 as Arma_Cadena_2, " )"
    Format Arma_Cadena as Arma_Cadena_1, Arma_Cadena_2
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Inserta_Solicitud_Acreditacion_Redencion                      //
// INSERTA LOS DATOS A ENVIAR EN LA TABLA LOYTRANSACCION                         //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Inserta_Solicitud_Acreditacion_Redencion
    Var  Temp1  : $10 = 0
    Call Conecta_Base_Datos_Sybase

    IF  Version_Micros = 41
        DLLCALL_CDECL hODBCDLL, sqlExecuteQuery(Arma_Cadena)
        SqlStr1 = ""
        DLLCALL_CDECL hODBCDLL, sqlGetLastErrorString(ref SqlStr1)

        IF (SqlStr1 <> "")
            InfoMessage "Error 19 A1: ", Mid(SqlStr1,1,30)
        ENDIF
        SqlStr1 = ""
    ENDIF

    IF SqlStr1  = ""
    
    ELSE
        Split SqlStr1, ";", Temp1
    ENDIF
    
    Call UnloadDLL_SyBase
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Consulta_Fecha_Juliana_Micros                                 //
// CONSULTA LA FECHA JULIANA DE MICROS                                           //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//


Sub Consulta_Fecha_Juliana_Micros(ref Fecha_C)
    Format SqlStr as "SELECT datediff( day, '2010-01-01', ' ", Trim(Fecha_C), " ') + 1 as FechaJuliana"
    
    Call Consulta_Sql

    IF SqlStr1 = ""
        Fecha_Juliana_M = ""
        Fecha_Numerica  = 0
    ELSE
        IF Len(Trim(SqlStr1)) > 0 Then
          Split SqlStr1, ";", Fecha_Juliana_M
          Fecha_Numerica =  Fecha_Juliana_M
        ENDIF
    ENDIF

    Call UnloadDLL_SyBase
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Escoge_Voucher_Redencion                                      //
// CONSULTA LA FECHA JULIANA DE MICROS                                           //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Escoge_Voucher_Redencion
    Var Camp01  : A30 = ""
    Var Camp02  : A30 = ""
    Var Camp03  : A30 = ""
    
    If Contador_Voucher = 0
        Format Camp01  as "Success"
        Format Camp02  as "Processed"
        Format Camp03  as "No Promotions Qualified"
        Return
    EndIf

    TouchScreen 60
    Window Contador_Voucher, 30
            ListInput 1, 1, Contador_Voucher, VoucherProductName, Opcion_Redencion, "Escoja Redencion"
    WindowClose
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Guarda_Campos_Acreditaciones_Redenciones                      //
// GUARDA LOS DATOS DE LA ACREDITACION EN VARIABLES                              //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Guarda_Campos_Acreditaciones_Redenciones(Ref SNivel_ , Ref Price_, Ref Number_, Ref Unidades_, Ref Cheque_, Ref ItemNumber_ )
    Var V_Qty  : A1 = ""

    If Trim(Number_) = "WOW Ticket"
        Unidades_ = 0
    EndIf
    
    Quantity = Unidades_

    If Flag_Final_Tender = "AF"
        V_Qty    = ""
    Else
         V_Qty   = "1"
    EndIf
    
    MemberNumber         = NumeroMiembro
    ID                  = "NUEVO"
    ALSELevelPrice      = SNivel_
    BranchDivision      = "BKCASUAL"
    ALSEPaymnetMode   = "Puntos WOW"
    ActivityDate         = Business_Date
    AdjustedListPrice   = AdjustedListPrice
    Amount              = Price_
    Comments             = ""
    ItemNumber          = ItemNumber_
    LocationName         = Tienda
    PartnerName       = PartnerName
    PointName           = ""  
    Points              = "0"  
    ProcessDate       = Business_Date
    ProcessingComment   = ""
    ProcessingLog       = ""
    ProductName       = Number_  
    Quantity             = Unidades_
    IntegrationStatus   = ""
    Status              = ""    
    SubStatus           = ""
    TicketNumber         = TicketNumber
    TransactionChannel  = "Store"
    TransactionDate   = Business_Date
    TransactionSubType  = "Product"
    TransactionType   = "Redemption"
    VoucherNumber       = "" 
    VoucherQty          = V_Qty
    VoucherType       = VoucherProductName[Opcion_Redencion]  
    Organization         = "BKCASUAL"
    Estatus           = "PEN"
    Bussiness_date      = Business_Date_Real
    Centroconsumo       = @RVC
    Cheque              = Cheque_
    Transmitido       = "F"
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Verifica_Descuento_Aplicado                                   //
// VERIFICA SI HAY UN DESCUENTO APLICADO EN EL CHEQUE ACTUAL                     //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Verifica_Descuento_Aplicado
    FP_Sbux_Aplicado_Off_Line = ""

    For I = 1 to @NumDTLT
        If @dtl_type[I] = "D" and @dtl_Is_Void[I] = 0

            Format Etiqueta_A_Buscar as "Descuento_Redenciones$"
            Call Consulta_Configuracion_CRM( Etiqueta_A_Buscar )
            Descuento_Redenciones_M = SqlStr1

            Format Etiqueta_A_Buscar as "Descuento_Redenciones%"
            Call Consulta_Configuracion_CRM( Etiqueta_A_Buscar )
            Descuento_Redenciones_P = SqlStr1

        ElseIf @dtl_type[I] = "T" and @dtl_Is_Void[I] = 0
            Total_Fpago_Cheque = ( Total_Fpago_Cheque + @DTL_TTL[I] )
        EndIf
    EndFor
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Graba_Linea_Item_Redimida                                     //
// GUARDA LA LINEA DONDE SE ENCUENTRA EL PRODUCTO REDIMIDO                       //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
Sub Graba_Linea_Item_Redimida(Ref Cheque_, Ref Linea_Red_)
    Var IJ  : N3 = 0

    For IJ = 1 to 100
        if Linea_Red[IJ] = 0
            Chq[IJ] = Cheque_
            Linea_Red[IJ] = Linea_Red_
            break
        endif
    EndFor
Endsub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Lee_Linea_Item_Redimida                                       //
// BUSCA LA LINEA DEL ITEM REDIMIDA                                              //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Lee_Linea_Item_Redimida
    Var Contador_R : N3 = 0
    ClearArray Linea_Redimida

    Lineas_Leidas_Redimidas = 0
    For Contador_R = 1 to 100
        if Linea_Red[Contador_R] > 0
            Cheque_Leido                = Chq[Contador_R]
            Linea_Redimida[Contador_R] = Linea_Red[Contador_R]
            Lineas_Leidas_Redimidas = ( Lineas_Leidas_Redimidas + 1 )
        endif
    EndFor
Endsub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Borra_Linea_Item_Redimida                                     //
// BORRA EL DATO DE LA LINEA D EL ITEM REDIMIDO                                  //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Borra_Linea_Item_Redimida
    ClearArray Chq
    ClearArray Linea_Red
Endsub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Verifica_Linea_Redencion                                      //
// VERIFICA LA LINEA DEL ITEM REDIMIDO                                           //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Verifica_Linea_Redencion( Ref Linea_Redimida_ )
    Var I_  : N3 = 0
    Linea_Redimida_Encontrada = ""
    Call Lee_Linea_Item_Redimida

    IF @CKNUM <> Cheque_Leido
        Return
    EndIf
    
    For I_ = 1 to Lineas_Leidas_Redimidas
        If Linea_Redimida[I_] <> 0
            If Linea_Redimida[I_] = Linea_Redimida_
                Linea_Redimida_Encontrada = "T"
                Return
            EndIf
        EndIf
    EndFor
EndSub


//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Borra_Leyenda_CRM                                             //
// BORRA LA LEYENDA QUE SE IMPRIME EN EL TICKET                                  //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Borra_Leyenda_CRM
    var i : N10
        
    For i = 1 to 10
        LeyendaCRM_WOW[i] = ""
    EndFor  
Endsub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Write_Leyenda_CRM                                             //
// GUARDA LA LEYENDA QUE SE IMPRIME EN EL TICKET                                 //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Write_Leyenda_CRM(Ref Ticket_)
    
    var i : N10
       
    For i = 1 to 10
        
        if LeyendaCRM_WOW[i] = ""
            LeyendaCRM_WOW[i] = Ticket_
            break
        endif
    EndFor      
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Graba_Privilegios_Nivel_Miembro                               //
// CUENTA EL NUMERO DE REDENCIONES EN EL CHEQUE                                  //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Graba_Privilegios_Nivel_Miembro
    Var IJ  : N2 = 0    
    
    Contador_Redenciones_MNM = Contador_Redenciones
    For IJ = 1 to Contador_Redenciones
        Redencion_Aplicada_MNM[IJ] = Redencion_Aplicada[IJ]
    EndFor
Endsub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Borra_Privilegios_Nivel_Miembro //
// CUENTA EL NUMERO DE REDENCIONES EN EL CHEQUE                                  //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Borra_Privilegios_Nivel_Miembro 
    ClearArray Redencion_Aplicada_MNM
    Contador_Redenciones_MNM = 0
Endsub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Lee_Privilegios_Nivel_Miembro                                 //
// LEE EL NUMERO DE REDENCIONES EN EL CHEQUE                                     //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Lee_Privilegios_Nivel_Miembro
    Var IJ  : N2 = 0
        
    Contador_Voucher = Contador_Redenciones_MNM
    For IJ = 1 to Contador_Redenciones_MNM
        VoucherProductName[IJ] = Redencion_Aplicada_MNM[IJ]
    EndFor
Endsub


//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Version_ISL                                                   //
// VERSION DE ISL                                                                //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Version_ISL
    Var Demo : A40 = ""
    Format Demo as "VERSION MICROS ISL 1.00.01"
    Prompt Demo
    MSleep(1000)
EndSub


//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Pantalla_Error_Redencion_Acreditacion_Nueva                   //
// CREA LA VENTANA DE ERROR DE REDENCION                                         //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Pantalla_Error_Redencion_Acreditacion_Nueva(Ref Titulo_)
    Var I  : N3 = 0
    If CC01 > 0 and CC01 < 13
        Window CC01,78, Titulo_
            For I = 1 to CC01
                Display  I, 1, Mid(Errores_Mostrar[I],1,77)
            EndFor
            TouchScreen Numero_TouchscreenN 
            Input G, "[Enter] para Continuar"
    WindowClose
    EndIf
EndSub


//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Consulta_Sql                                                  //
// CONSULTA LA BASE DE DATOS                                                     //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Consulta_Sql
  Call Conecta_Base_Datos_Sybase

    IF Version_Micros = 32 or Version_Micros = 41
        DLLCALL_CDECL hODBCDLL, sqlGetRecordSet(SqlStr)
        SqlStr1 = ""
        DLLCALL_CDECL hODBCDLL, sqlGetLastErrorString(ref SqlStr1)
        IF (SqlStr1 <> "")
            Call Muestra_Error(SqlStr1)
            Call UnloadDLL_SyBase
            ExitCancel
        ENDIF

        SqlStr1 = ""
        // si no obtiene error hace la consulta
        // regresa el valor en SqlStr1 
        DLLCALL_CDECL hODBCDLL, sqlGetFirst(ref SqlStr1)

    ElseIF Version_Micros = 31
        DLLCall hODBCDLL, sqlGetRecordSet(SqlStr)
        SqlStr1 = ""
        DLLCall hODBCDLL, sqlGetLastErrorString(ref SqlStr1)
        IF SqlStr1 <> ""
            Call Muestra_Error(SqlStr1)
            Call UnloadDLL_SyBase
            ExitCancel
        ENDIF

        SqlStr1 = ""
        // si no obtiene error hace la consulta
        // regresa el valor en SqlStr1 
        DLLCall hODBCDLL, sqlGetFirst(ref SqlStr1)
    ENDIF
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM         //
// CONSULTA LA TABLA DEL CATALOGO DE ERRORES                                     //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Ref Error1_, Ref Error2_)
    ClearArray Errores_Mostrar

    Format SqlStr as "SELECT Mensaje FROM custom.catalogo_errores_crm WHERE Error = '",Error1_,"' AND Tipo = '",Error2_,"'"
    Call Consulta_Sql

    CC01 = 0

    WHILE Len(Trim(SqlStr1)) > 0
    CC01 = ( CC01 + 1 )
    Split SqlStr1, ";", SqlStr1
    Errores_Mostrar[CC01] = SqlStr1
    DLLCALL_CDECL hODBCDLL, sqlGetNext(ref SqlStr1)
    ENDWHILE

    Call UnloadDLL_SyBase
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Carga_Miembro_Cheque_Nivel_Screen                             //
// GUARDA LOS PUNTOS WOW                                                         //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Carga_Miembro_Cheque_Nivel_Screen(ref Campo__, ref Puntos__)
    Var IO  : N3 = 0
    Var Ptos : N3 = 0
    Puntos__      = 0

    For IO = 1 to 30
        If Len(Trim(RewardName_A[IO])) > 0
            If Trim(RewardName_A[IO]) = Campo__
                If Trim(RewardTypeDiscount_P_M_A[IO]) = "Redeem"
                    Ptos = RewardPoints[IO]
                    If LifetimePoint2Value >= Ptos
                        Format Campo  as "", Mid(RewardName_A[IO], 1, 16)
                        Format Campo_ as Mid(RewardName_A[IO], 1, 16)
                        Puntos__ = Ptos
                        Call Guarda_en_Cheque
                        Return
                    Else
                        If LifetimePoint2Value = 0
                            Puntos__ = 1
                        Else
                            Puntos__ = LifetimePoint2Value
                        EndIf
                    EndIf
                ElseIf Trim(RewardTypeDiscount_P_M_A[IO]) = "Frecuency" And Campo__ = "Beb Gold"
                    Puntos__ = 1
                Else
                EndIf
            Else
            EndIf
        EndIf
    EndFor
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Graba_Off_Line_CRM                                            //
// GUARDA EL DATOS DE QUE ES OFFLINE                                             //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Graba_Off_Line_CRM
    Format FileOff_Line_CRM_string as Campo_Busqueda
Endsub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Read_Off_Line_CRM                                             //
// LEE EL DATOS DE QUE ES OFFLINE                                                //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Read_Off_Line_CRM
    Format TicketNumber_Off as FileOff_Line_CRM_string
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Manda_Ejecuta_Acreditaciones_OffLine_WS_JAR                   //
// EJECUTA LA TRANSACCION OFFLINE                                                //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Manda_Ejecuta_Acreditaciones_OffLine_WS_JAR
    Manda = ""
    Format Manda, Separador as "OFF", TicketNumber, Trim(Miembro_2[2]), Nombre_Miembro_O    
    TxMsg Manda
    GetRxmsg "Espere...OFF"
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Manda_Redemption                                              //
// EJECUTA LA REDENCION WOW                                                      //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Manda_Redemption
    Call Lee_Miembro_O  
    Manda = ""
    Format Manda, Separador as "RED", Arma_Cadena_82, Nombre_Miembro_O
    TxMsg Manda
    GetRxmsg "Espere... Cliente WOW"
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Valida_Redemption                                             //
// VALIDA RESPUESTA SOA DE REDEMCION                                             //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Valida_Redemption
    Var Campo_01_   : A50 = ""
    Var Campo_02_   : A50 = ""
    
    Salida_Consulta_Clientes = ""
    Format Campo_01_ as "<WS_TIMEOUT>"
    Format Campo_02_ as "<Success>"
    
    If Mid(@RxMsg, 1, 32 ) = "invokeButton_Automatico (Error):"
        If @RVC = 2
            LoadKyBdMacro Key (19,102)
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
            Salida_Consulta_Clientes = "T"
        Else
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
        EndIf
        Call Graba_Off_Line_CRM
        ExitCancel
    EndIf
    
    If Trim(@RxMsg) = "NO EXISTE MIEMBRO"
        Split @RxMsg, "|", Resp_Red[1], Resp_Red[2]
        Format Campo_01_ as "<WS_SOA_ERROR>"
        Format Campo_02_ as "<", Resp_Red[2], ">"
        
        If @RVC = 2
            LoadKyBdMacro Key (19,102)
            InfoMessage Mid(@RxMsg, 1, 32 )
            Salida_Consulta_Clientes = "T"
        Else
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
            ExitCancel
        EndIf
    
    ElseIf Mid(Trim(@RxMsg),1, 26)  = "NO HAY COMUNICACION CON WS"
        Split @RxMsg, "|", Resp_Red[1], Resp_Red[2]
        Format Campo_01_ as "<WS_SOA_ERROR>"
        Format Campo_02_ as "<", Resp_Red[2], ">"
        Call Graba_Off_Line_CRM
        
        If @RVC = 2
            LoadKyBdMacro Key (19,102)
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
            Salida_Consulta_Clientes = "T"
        Else
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
        EndIf
        ExitCancel

    ElseIf Mid(Trim(@RxMsg),1, 26)  = "NO SE REALIZO LA REDENCION"
        Split @RxMsg, "|", Resp_Red[1], Resp_Red[2]
        Format Campo_01_ as "<WS_SOA_ERROR>"
        Format Campo_02_ as "<", Resp_Red[2], ">"
        Call Graba_Off_Line_CRM

        If @RVC = 2
            LoadKyBdMacro Key (19,102)
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
            Salida_Consulta_Clientes = "T"
        Else
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
        EndIf
        ExitCancel

    ElseIf Mid(Trim(@RxMsg),1, 18)  = "ERROR EN REDENCION"
        Split @RxMsg, "|", Resp_Red[1], Resp_Red[2]
        Format Campo_01_ as "<WS_SOA_ERROR>"
        Format Campo_02_ as "<", Resp_Red[2], ">"
        Call Graba_Off_Line_CRM

        If @RVC = 2
            LoadKyBdMacro Key (19,102)
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
            Salida_Consulta_Clientes = "T"
        Else
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
        EndIf
        ExitCancel

    ElseIf Trim(@RxMsg) = "_timeout"
        Format Campo_01_ as "<WS_TIMEOUT>"
        Format Campo_02_ as "<Success>"
        Call Graba_Off_Line_CRM
        If @RVC = 2
            LoadKyBdMacro Key (19,102)
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
            Salida_Consulta_Clientes = "T"
        Else
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
        EndIf
        ExitCancel
    EndIf

    Format Respuesta as @RxMsg  
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Manda_Acreditacion                                            //
// EJECUTA LA ACREDITACION                                                       //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Manda_Acreditacion
    Split MemberNumber, ":", Miembro_2[1], Miembro_2[2]
    Call Lee_Miembro_O
    Manda = ""
	Nombre_Miembro_O = No_Tarjeta_WOW
    If Len(Trim(Miembro_2[2])) = 0
        Format Manda, Separador as "ACR", Trim(TicketNumber), Trim(MemberNumber), Nombre_Miembro_O
    Else
        Format Manda, Separador as "ACR", Trim(TicketNumber), Trim(Miembro_2[2]), Nombre_Miembro_O
    EndIf
    
    If Len(Trim(TicketNumber)) = 0 And Len(Trim(MemberNumber)) = 0
        Salir_ACR = "T"
    EndIf
    
    If Salir_ACR = "T"
        Return
    EndIf
    
    TxMsg Manda
    GetRxmsg "Espere... Cliente WOW"
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Valida_Acreditacion                                           //
// VALIDA LA RESPUESTA SOA DE LA ACREDITACION                                    //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Valida_Acreditacion
    Var Campo_01_ : A50 = ""
    Var Campo_02_ : A50 = ""
    Salida_Consulta_Clientes = ""
    Format Campo_01_ as "<WS_TIMEOUT>"
    Format Campo_02_ as "<Success>"
    
    If Mid(@RxMsg, 1, 32 ) = "invokeButton_Automatico (Error):"
        If @RVC = 2
            LoadKyBdMacro Key (19,102)
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
            Salida_Consulta_Clientes = "T"
        Else
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
        EndIf
        Call Graba_Off_Line_CRM
        FP_Sbux_Aplicado_Off_Line = "T"

    ElseIf Trim(@RxMsg) = "NO EXISTE MIEMBRO"
        Split @RxMsg, "|", Resp_Red[1], Resp_Red[2]
        Format Campo_01_ as "<WS_SOA_ERROR>"
        Format Campo_02_ as "<", Resp_Red[2], ">"
        
        If @RVC = 2
            LoadKyBdMacro Key (19,102)
            InfoMessage Mid(@RxMsg, 1, 32 )
            Salida_Consulta_Clientes = "T"
        Else
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
            ExitCancel
        EndIf

    ElseIf Mid(Trim(@RxMsg),1, 26)  = "NO HAY COMUNICACION CON WS"
        Split @RxMsg, "|", Resp_Red[1], Resp_Red[2]
        
        If Len(Resp_Red[2]) > 0
            Format Campo_01_ as "<WS_SOA_ERROR>"
            Format Campo_02_ as "<", Resp_Red[2], ">"
        Else
            Format Campo_01_ as "<WS_TIMEOUT>"
            Format Campo_02_ as "<NO_WS>"
        EndIf

        Call Graba_Off_Line_CRM
        FP_Sbux_Aplicado_Off_Line = "T"
        
        If @RVC = 2
            LoadKyBdMacro Key (19,102)
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
            Salida_Consulta_Clientes = "T"
        Else
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
        EndIf

    ElseIf Mid(Trim(@RxMsg),1, 29)  = "NO SE REALIZO LA ACREDITACION"
        Split @RxMsg, "|", Resp_Red[1], Resp_Red[2]
        Format Campo_01_ as "<WS_SOA_ERROR>"
        Format Campo_02_ as "<", Resp_Red[2], ">"
        Call Graba_Off_Line_CRM
        FP_Sbux_Aplicado_Off_Line = "T"
        
        If @RVC = 2
            LoadKyBdMacro Key (19,102)
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
            Salida_Consulta_Clientes = "T"
        Else
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
        EndIf
         
    ElseIf Mid(Trim(@RxMsg),1, 21)  = "ERROR EN ACREDITACION"
        Split @RxMsg, "|", Resp_Red[1], Resp_Red[2]
        Format Campo_01_ as "<WS_SOA_ERROR>"
        Format Campo_02_ as "<", Resp_Red[2], ">"
        Call Graba_Off_Line_CRM
        FP_Sbux_Aplicado_Off_Line = "T"
        If @RVC = 2
            LoadKyBdMacro Key (19,102)
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
            Salida_Consulta_Clientes = "T"
        Else
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
        EndIf

    ElseIf Trim(@RxMsg) = "_timeout"
        Format Campo_01_ as "<WS_TIMEOUT>"
        Format Campo_02_ as "<Success>"
        Call Graba_Off_Line_CRM
        FP_Sbux_Aplicado_Off_Line = "T"

        If @RVC = 2
            LoadKyBdMacro Key (19,102)
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
            Salida_Consulta_Clientes = "T"
        Else
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
        EndIf
    EndIf

    Format Respuesta as "Respuesta Acreditacion:", @RxMsg
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Formatea_Datos_Redenciones                                    //
// ARMA LOS DATOS DE REDEMCION PARA SER ENVIADOS                                 //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Formatea_Datos_Redenciones
    Var Nombre_Mesero : A100 = ""
    Format Nombre_Mesero As @Tremp, " ", Trim(@TREMP_FNAME), " ", Trim(@TREMP_LNAME)
    TransactionType = "Queued"
    
    Format Arma_Cadena_82 as "", MemberNumber, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", id, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", ALSELevelPrice, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", BranchDivision, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", ALSEPaymnetMode, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", ActivityDate, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", AdjustedListPrice, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Amount, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Comments, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", ItemNumber, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", LocationName, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", PartnerName, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", PointName, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Points, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", ProcessDate, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", ProcessingComment, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", ProcessingLog, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Trim(ProductName), "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Quantity, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", IntegrationStatus, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Status, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", SubStatus, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", TicketNumber, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", TransactionChannel, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", TransactionDate, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", TransactionSubType, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", TransactionType, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", VoucherNumber, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", VoucherQty, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", VoucherType, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Organization, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Estatus, "| "
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Bussiness_date, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Centroconsumo, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Cheque, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Transmitido, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Linea_Cheque, "|"
    Format Arma_Cadena_82 as Arma_Cadena_82, "", Nombre_Mesero, "|"
Endsub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Lee_Miembro_Soa                                               //
// GUARDA DATOS DE LA RESPUESTA DE SOA DEL MIEMBRO                               //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Lee_Miembro_Soa
    Var FN001 : N6 = 0
    Var JJ  : N6 = 0    
    Call Borra_Array
    
    Var File1       : A100 = ""
    Contador_Redenciones = 10
    Cheque = Cheque_Miembro_Soa
    Format Respuesta as rspst_Miembro_Soa
    
    ALL = ""
    If Mid(Respuesta,1,3) = "ALL"
        Split Respuesta, "|", NumeroMiembro, Nombre
        ALL = "ALL"
    Else
        Split Respuesta, "|", NumeroMiembro, Nombre, Activo, Nivel, Organizacion, Nivel_Id, Nivel_nombre, \
        Contador_Campos,  PromocionId[]: PromocionNombre[]: PromocionUniqueKey[]: \
        PromocionDisplayName[]: PromocionStatus[], \
        Contador_Voucher, VoucherCalStatus[]: VoucherProductId[]: VoucherProductName[]: \
        VoucherStatus[]: VoucherTransactionId[]: validDiscountAmount[]:  validDiscountType[]: minimumAmount[], \
        BirthDate, FavoriteProduct, LastBeverage, LastFood, LifetimePoint1Value, LifetimePoint2Value, \
        Status, AttributeValue
    EndIf
    
    If Len(Trim(BirthDate)) > 0
        MesBirthDate = Mid(BirthDate, 1, 2)
    ENDIF
    If @MONTH <> MesBirthDate
        For JJ = 1 to Contador_Campos
            If Trim(PromocionNombre[JJ]) = "Beb Cumple"
                PromocionNombre[JJ] = ""
            Else
            EndIf
        EndFor
        For JJ = 1 to Contador_Voucher
            If Trim(VoucherProductName[JJ]) = "Beb Cumple"
                VoucherProductName[JJ] = ""
            Else
            EndIf
        EndFor
    Else

    EndIf
Endsub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Inserta_Crm_users                                             //
// INSERTA LOS DATOS DE MIEMBRO EN LA BASE DE DATOS                              //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Inserta_Crm_users( Ref Numero_Tarjeta_, Ref Cheque_, Ref Saldo_, Ref Marcado_Crm_, Ref stars_, Ref Datos_)
    Var Temp1   : $100  = 0
    Var Datos2_  : A5000 = ""
    Var Saldo2_  : $10  = ""

    Call Arma_Ticket_Buscar
    
    Saldo2_ = Saldo_Ws
    
    Call Conecta_Base_Datos_Sybase
    Format SqlStr as "INSERT INTO custom.crm_Clientes (Id_Cheque, Cheque, Numero_Tarjeta, Numero_Tarjeta_O, Saldo, Marcado_Crm, Stars, Datos_1 ) VALUES ( "
    Format SqlStr as SqlStr, " '", TicketNumber,      "' ,"
    Format SqlStr as SqlStr, "  ", Cheque_,         "  ,"
    Format SqlStr as SqlStr, " '", Numero_Tarjeta_, "' ,"
    Format SqlStr as SqlStr, " '", Numero_Tarjeta__O, "' ,"
    Format SqlStr as SqlStr, "  ", Saldo2_{010},      "  ,"
    Format SqlStr as SqlStr, " '", Marcado_Crm_,      "' ,"
    Format SqlStr as SqlStr, "  ", stars_,          "  ,"
    Format SqlStr as SqlStr, " '", Datos_,          "'  "
    Format SqlStr as SqlStr, " )"
    
    IF  Version_Micros = 41
        DLLCALL_CDECL hODBCDLL, sqlExecuteQuery(SqlStr)
        SqlStr1 = ""
        DLLCALL_CDECL hODBCDLL, sqlGetLastErrorString(ref SqlStr1)
        IF (SqlStr1 <> "")
            InfoMessage "Error 19 C2: ", Mid(SqlStr1,1,30)
        ENDIF
        SqlStr1 = ""
    ENDIF
    
    IF SqlStr1  = ""
    ELSE
        Split SqlStr1, ";", Temp1
    ENDIF
    Call UnloadDLL_SyBase
EndSub


//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Lee_Miembro_ws                                                //
//  GUARDA DATOS DE LA RESPUESTA DE SOA DEL MIEMBRO                              //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//


Sub Lee_Miembro_ws
    Var FN001 : N6 = 0
    Var JJ  : N6 = 0
    
    Call Borra_Array
    Var File1       : A100 = ""
    Contador_Redenciones = 10
    Cheque = Cheque_Miembro
    Format Respuesta as rspst_Miembro
    ALL = ""

    Call Consulta_Cliente
    
    If Mid(Respuesta,1,3) = "ALL"
        Split Respuesta, "|", NumeroMiembro, Nombre
        ALL = "ALL"

    Else
        Split Respuesta, "|", NumeroMiembro, Nombre, Activo, Nivel, Organizacion, Nivel_Id, Nivel_nombre, \
        Contador_Campos,  PromocionId[]: PromocionNombre[]: PromocionUniqueKey[]: \
        PromocionDisplayName[]: PromocionStatus[], \
        Contador_Voucher, VoucherCalStatus[]: VoucherProductId[]: VoucherProductName[]: \
        VoucherStatus[]: VoucherTransactionId[]: validDiscountAmount[]:  validDiscountType[]: minimumAmount[], \
        BirthDate, FavoriteProduct, LastBeverage, LastFood, LifetimePoint1Value, LifetimePoint2Value, \
        Status, AttributeValue, ErrorCode, pointAmount
    EndIf
    
    If Len(Activo) = 0
        Activo = Status
    EndIf

    Var Marcado_Crm  : A1 = "1" 
    Var Cheque_crm  : N6 = @CKNUM
    Call Inserta_Crm_users( NumeroMiembro, Cheque_crm, Saldo_Ws, Marcado_Crm, LifetimePoint2Value, Respuesta )
    
    If Len(Trim(BirthDate)) > 0
        MesBirthDate = Mid(BirthDate, 1, 2)
    EndIf
    
    If @MONTH <> MesBirthDate
        For JJ = 1 to Contador_Campos
            If Trim(PromocionNombre[JJ]) = "Beb Cumple"
                PromocionNombre[JJ] = ""
            Else
            EndIf
        EndFor
        For JJ = 1 to Contador_Voucher
            If Trim(VoucherProductName[JJ]) = "Beb Cumple"
                VoucherProductName[JJ] = ""
            Else
            EndIf
        EndFor
    Else
    EndIf
Endsub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Lee_Miembro_O                                                 //
// LEE EL NUMERO DE TARJETA DEL MIEMBRO                                          //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Lee_Miembro_O
    Format Nombre_Miembro_O as Miembro_O_Archivo
Endsub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Borra_FileMiembro_Pausa_R_Cheque                              //
//                                                                               //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Borra_FileMiembro_Pausa_R_Cheque
    rspst_Miembro_PR = ""
    Cheque_Miembro_PR = 0
Endsub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Write_Leyenda_CRM_Cheque                                      //
// GUARDA LOS DATOS DE LA LEYENDA CRM                                            //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Write_Leyenda_CRM_Cheque(Ref Datos_) 
    Var III : N3 = 0
    
    for III = 1 to 15 
        if Leyenda_CRM_Cheque_Arreglo[III]  = ""
            format Leyenda_CRM_Cheque_Arreglo[III] as Datos_
            break
        endif
    endfor
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Borra_FileLeyendaCRM_ChequE                                   //
// BORRA LOS DATOS DE LA LEYENDA CRM                                            //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Borra_FileLeyendaCRM_Cheque 
    ClearArray Leyenda_CRM_Cheque_Arreglo
Endsub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Consulta_Cliente                                              //
// CONSULTA LOS DATOS DE MIEMBRO DE LA BASE DE DATOS                             //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Consulta_Cliente
    Call Arma_Ticket_Buscar 
    
    Format SqlStr as "SELECT Id_Cheque, Numero_Tarjeta, Numero_Tarjeta_O, Saldo, Marcado_Crm, Datos_1  FROM custom.crm_Clientes WHERE Id_Cheque = '", TicketNumber, "' "
    Call Consulta_Sql

    IF SqlStr1 = ""
        Id_Cheque       = ""
        Numero_Tarjeta  = ""
        Numero_Tarjeta_O = ""
        Saldo           = ""
        Marcado_Crm   = ""
        Datos_1       = ""
    Else
        IF Len(Trim(SqlStr1)) > 0 Then
            Split SqlStr1, ";", Id_Cheque, Numero_Tarjeta, Numero_Tarjeta_O, Saldo, Marcado_Crm, Datos_1
        ENDIF
    ENDIF

    Call UnloadDLL_SyBase
    
    If Len(Trim(Datos_1)) > 0
        Respuesta = Datos_1
    EndIf
    
    If Len(Trim(Respuesta)) > 3
        If Mid(Respuesta,1,3) = "ALL"
            Split Respuesta, "|", NumeroMiembro, Nombre
            ALL = "ALL"
        Else
            Split Respuesta, "|", NumeroMiembro, Nombre, Activo, Nivel, Organizacion, Nivel_Id, Nivel_nombre, \
            Contador_Campos,  PromocionId[]: PromocionNombre[]: PromocionUniqueKey[]: \
            PromocionDisplayName[]: PromocionStatus[], \
            Contador_Voucher, VoucherCalStatus[]: VoucherProductId[]: VoucherProductName[]: \
            VoucherStatus[]: VoucherTransactionId[]: validDiscountAmount[]:  validDiscountType[]: minimumAmount[], \
            BirthDate, FavoriteProduct, LastBeverage, LastFood, LifetimePoint1Value, LifetimePoint2Value, \
            Status, AttributeValue
        EndIf
    EndIf   
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Arma_Ticket_Buscar                                            //
// ARMA EL NUMERO DE TICKET                                                      //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Arma_Ticket_Buscar
    Format Ticket_Unico   as @WSID{01}, @CKNUM{04}, Mid(@Chk_Open_Time,10,2), Mid(@Chk_Open_Time,13,2)
    Format Fecha_a_Convertir as "20", Mid(@Chk_Open_Time,7,2), "-", Mid(@Chk_Open_Time,1,2), "-",Mid(@Chk_Open_Time,4,2)
    
    Call Consulta_Fecha_Juliana_Micros(Fecha_a_Convertir)
    Call Consulta_Business_Date_SyBase
    Format TicketNumber as Fecha_Numerica{04}, Tienda{05}, Ticket_Unico
EndSub


//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Guarda_Campos_Acreditaciones_Redenciones_Wow                  //
// GUARDA LOS DATOS EN VARIBLES                                                  //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Guarda_Campos_Acreditaciones_Redenciones_Wow(Ref SNivel_ , Ref Price_, Ref Number_, Ref Unidades_, Ref Cheque_ )
    MemberNumber         = NumeroMiembro
    ID                  = "NUEVO"
    ALSELevelPrice      = "NA"
    BranchDivision      = "BKCASUAL"
    ALSEPaymnetMode   = "Puntos WOW"
    ActivityDate         = Business_Date
    AdjustedListPrice   = "0"
    Amount              = TotalCheque
    Comments             = ""
    ItemNumber          = 0
    LocationName         = Tienda
    PartnerName       = PartnerName
    PointName           = ""  //"Star"
    Points              = "0"  //"0"
    ProcessDate       = Business_Date
    ProcessingComment   = ""
    ProcessingLog       = ""
    ProductName       = "WOW Ticket"  
    Quantity             = 0    //  Unidades_
    IntegrationStatus   = ""
    Status              = ""    
    SubStatus           = ""
    TicketNumber         = TicketNumber
    TransactionChannel  = "Store"
    TransactionDate   = Business_Date
    TransactionSubType  = "Product"
    TransactionType   = "Redemption"
    VoucherNumber       = ""  //VoucherTransactionId[Opcion_Redencion]
    VoucherQty          = ""
    VoucherType       = ""
    Organization         = "BKCASUAL"
    Estatus           = "PEN"
    Bussiness_date      = Business_Date_Real
    Centroconsumo       = @RVC
    Cheque              = Cheque_
    Transmitido       = "F"
EndSub


//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Formatea_Datos_Redenciones_Tmed_Wow                           //
// DA FORMATO DE LOS DATOS DE REDENCION                                          //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Formatea_Datos_Redenciones_Tmed_Wow
    Var Nombre_Mesero : A100 = ""
    Format Nombre_Mesero As @Tremp, " ", Trim(@TREMP_FNAME), " ", Trim(@TREMP_LNAME)
    Format Arma_Cadena_82, Separador as MemberNumber,id, ALSELevelPrice, BranchDivision, ALSEPaymnetMode, ActivityDate, AdjustedListPrice, Amount, Comments, ItemNumber, LocationName, PartnerName, PointName, Points, ProcessDate, ProcessingComment, ProcessingLog, ProductName, Quantity, IntegrationStatus, Status, SubStatus, TicketNumber, TransactionChannel, TransactionDate, TransactionSubType, TransactionType, VoucherNumber, VoucherQty, VoucherType, Organization, Estatus, Bussiness_date, Centroconsumo, Cheque, Transmitido, Linea_Cheque, Nombre_Mesero
Endsub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Manda_Redemption_Tmed_Wow                                     //
// EJECUTA LA REDENCION                                                          //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Manda_Redemption_Tmed_Wow
    Manda = ""
    Call Lee_Miembro_O
    Format Manda, Separador as "REW", Arma_Cadena_82, Nombre_Miembro_O
    TxMsg Manda
    GetRxmsg "Espere... Cliente WOW"
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Valida_Redemption_Tmed_Wow                                    //
// VALIDA LA RESPUESTA DE REDENCION DE SOA                                       //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Valida_Redemption_Tmed_Wow
    Var Campo_01_   : A50 = ""
    Var Campo_02_   : A50 = ""
    
    Salida_Consulta_Clientes = ""
    Format Campo_01_ as "<WS_TIMEOUT>"
    Format Campo_02_ as "<Success>"
    
    If Mid(@RxMsg, 1, 32 ) = "invokeButton_Automatico (Error):"
        If @RVC = 2
            LoadKyBdMacro Key (19,102)
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
            Salida_Consulta_Clientes = "T"
        Else
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
        EndIf
        Call Graba_Off_Line_CRM
        ExitCancel
    EndIf

    If Trim(@RxMsg) = "NO EXISTE MIEMBRO"
        Split @RxMsg, "|", Resp_Red[1], Resp_Red[2]
        Format Campo_01_ as "<WS_SOA_ERROR>"
        Format Campo_02_ as "<", Resp_Red[2], ">"

        If @RVC = 2
            LoadKyBdMacro Key (19,102)
            InfoMessage Mid(@RxMsg, 1, 32 )
            Salida_Consulta_Clientes = "T"
        Else
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
            ExitCancel
        EndIf

        ExitCancel

    ElseIf Mid(Trim(@RxMsg),1, 26)  = "NO HAY COMUNICACION CON WS"
        Split @RxMsg, "|", Resp_Red[1], Resp_Red[2]
        Format Campo_01_ as "<WS_SOA_ERROR>"
        Format Campo_02_ as "<", Resp_Red[2], ">"
        
        If Len(Resp_Red[2]) > 0
            Format Campo_01_ as "<WS_SOA_ERROR>"
            Format Campo_02_ as "<", Resp_Red[2], ">"
        Else
            Format Campo_01_ as "<WS_TIMEOUT>"
            Format Campo_02_ as "<NO_WS>"
        EndIf

        Call Graba_Off_Line_CRM

        If @RVC = 2
            LoadKyBdMacro Key (19,102)
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
            Salida_Consulta_Clientes = "T"
        Else
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
        EndIf
        ExitCancel

    ElseIf Mid(Trim(@RxMsg),1, 26)  = "NO SE REALIZO LA REDENCION"
        Split @RxMsg, "|", Resp_Red[1], Resp_Red[2]
        Format Campo_01_ as "<WS_SOA_ERROR>"
        Format Campo_02_ as "<", Resp_Red[2], ">"
        Call Graba_Off_Line_CRM

        If @RVC = 2
            LoadKyBdMacro Key (19,102)
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
            Salida_Consulta_Clientes = "T"
        Else
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
        EndIf
        ExitCancel

    ElseIf Mid(Trim(@RxMsg),1, 18)  = "ERROR EN REDENCION"
        Split @RxMsg, "|", Resp_Red[1], Resp_Red[2]
        Format Campo_01_ as "<WS_SOA_ERROR>"
        Format Campo_02_ as "<", Resp_Red[2], ">"
        Call Graba_Off_Line_CRM
    
        If @RVC = 2
            LoadKyBdMacro Key (19,102)
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
            Salida_Consulta_Clientes = "T"
        Else
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
        EndIf
        ExitCancel
    
    ElseIf Trim(@RxMsg) = "_timeout"
        Format Campo_01_ as "<WS_TIMEOUT>"
        Format Campo_02_ as "<Success>"
        Call Graba_Off_Line_CRM
        
        If @RVC = 2
            LoadKyBdMacro Key (19,102)
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
            Salida_Consulta_Clientes = "T"
        Else
            Call Lee_Catalogo_Errores_Acreditaciones_Cancelaciones_CRM(Campo_01_, Campo_02_)
            Call Pantalla_Error_Redencion_Acreditacion_Nueva(Campo_01_)
        EndIf
        ExitCancel
    EndIf
    
    Format Respuesta as @RxMsg
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Busca_MenuItem_Acreditaciones_SOA                             //
// ORDENA LOS ITEMS DE LA ACREDITACION                                            //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Busca_MenuItem_Acreditaciones_Soa
    Var I                   : N4  = 0
    Var Descontar           : $10 = 0
    Var Nombre_Forma_Pago_PM : A20 = ""
    Call Suma_MenuItem_Soa_Only
    For I = 1 to @NUMDTLT
        If @DTL_TYPE[I] = "M" AND @DTL_IS_VOID[I] = 0
            If @DTL_TTL[I] > 0
                Call Verifica_Linea_Redencion( I )
                If Linea_Redimida_Encontrada = "T"
                    If @Dtl_is_cond[I] = 0   //ITEM
                        Condimento_Aplicado = "T"
                    Else                        //CONDIMENT
                        Condimento_Aplicado = ""
                    EndIf
                Else
                    Condimento_Aplicado = ""
                    SNivel_R     = Nivel_Price[@Dtl_Plvl[I]]   
                    Price_R   = @DTL_TTL[I]
                    Number_R     = @DTL_OBJECT[I]
                    Nombre_R     = @DTL_NAME[I]
                    Unidades_R  = @DTL_QTY[I]
                    Cheque_R     = @CKNUM
                    Linea_Cheque = I
                    Format TicketNumber as Fecha_Numerica{04}, Tienda{05}, Ticket_Unico
                    
                    If Primera_Cero = "F"
                       
                        Primera_Cero = "T"
                    EndIf
                    
                    For Contador_OP = 1 to Unidades_R
                        Cons_Item = ( Cons_Item + 1 )
                        If Unidades_R > 1
                            Total_Paso = ( Price_R / Unidades_R )
                        EndIf
                        
                        If No_Formas_Pago = 1
                            Format Nombre_Forma_Pago_PM As ""    
                        Else
                            Format Nombre_Forma_Pago_PM As ""
                        EndIf
                        
                        If Total_MenuItem <> 0
                        EndIf
                        
                        If Total_Paso > 0
                            Call Guarda_Campos_Acreditaciones_MenuItem(SNivel_R , Total_Paso, Cons_Item, Unidades_R, Cheque_R, Number_R, Total_Fpago_Cheque, Nombre_Forma_Pago_PM )
                            Total_Paso = 0
                        Else                        
                            Call Guarda_Campos_Acreditaciones_MenuItem(SNivel_R , Price_R, Cons_Item, Unidades_R, Cheque_R, Number_R, Total_Fpago_Cheque, Nombre_Forma_Pago_PM )
                        EndIf
                        
                        Call Arma_Registro_Acreditaciones_Redenciones                           
                        Call Inserta_Solicitud_Acreditacion_Redencion
                        
                    EndFor
                    
                    Lineas_Acreditas_Contador = ( Lineas_Acreditas_Contador + 1 )
                
                EndIf
            EndIf
        EndIf
    EndFor
    offline=0
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Busca_Formas_Pago_Acreditaciones_Soa                          //
// ORDENA LAS FORMAS DE PAGO PARA MANDAR LA ACREDITACION                         //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Busca_Formas_Pago_Acreditaciones_Soa
    Var I                    : N4  = 0
    Var Tender_Efectivo_Ttl_2 : $12 = 0
    No_Formas_Pago      = 0
    Nombre_Forma_Pago_Ws = ""
    SNivel_R2           = ""
    Price_R2             = 0
    Number_R2           = ""
    Nombre_R2           = ""
    Unidades_R2       = 0
    Cheque_R2           = 0
    Tender_Efectivo_Ttl  = 0
    
    Call Busca_Aportacion_Acreditaciones_Soa_Only
    
    For I = 1 to @NUMDTLT
        If @DTL_TYPE[I] = "T" AND @DTL_IS_VOID[I] = 0

            If @DTL_OBJECT[I]    = Tender_Crm
                Format Name_FP  As Trim(@DTL_NAME[I])
            ElseIf @Dtl_Objnum[I] = Tender_Efectivo_No 
                SNivel_R2            = Nivel_Price[@MLVL[I]]
                Price_R2              = @DTL_TTL[I]
                Number_R2            = @DTL_OBJECT[I]
                Nombre_R2            = Trim(@DTL_NAME[I])
                Unidades_R2         = @DTL_QTY[I]
                Cheque_R2            = @CKNUM
                Tender_Efectivo_Ttl = ( Tender_Efectivo_Ttl + Price_R2 )
                Format Name_FP      As Trim(@DTL_NAME[I])

            Else
                SNivel_R             = Nivel_Price[@MLVL[I]]
                Price_R           = @DTL_TTL[I]
                Number_R             = @DTL_OBJECT[I]
                Nombre_R             = Trim(@DTL_NAME[I])
                Unidades_R          = @DTL_QTY[I]
                Cheque_R             = @CKNUM
                Linea_Cheque         = I
                No_Formas_Pago      = ( No_Formas_Pago + 1 )
                SNivel_R             = "NA"
                Nombre_Forma_Pago_Ws = Nombre_R
                Format TicketNumber as Fecha_Numerica{04}, Tienda{05}, Ticket_Unico
                Format Name_FP    As Trim(@DTL_NAME[I])
                Cons_Item = ( Cons_Item + 1 )

                If Total_Aportaciones <> 0
                    Tender_Efectivo_Ttl_2 = ( Price_R - Total_Aportaciones )  
                    Total_Aportaciones = 0
                Else
                    Tender_Efectivo_Ttl_2 = ( Price_R )
                EndIf

                If Tender_Efectivo_Ttl <> 0
                EndIf

                If @DTL_OBJECT[I] = Tender_Crm  
                Else  
                    Call Guarda_Campos_Acreditaciones_Formas_Pago(SNivel_R , Price_R, Cons_Item, Unidades_R, Cheque_R, Number_R, Tender_Efectivo_Ttl_2, Nombre_R )
                    Call Arma_Registro_Acreditaciones_Redenciones
                    Call Inserta_Solicitud_Acreditacion_Redencion
                    Lineas_Acreditas_Contador = ( Lineas_Acreditas_Contador + 1 )
                EndIf
            EndIf
        EndIf
    EndFor
    
    If Len(SNivel_R2) = 0 And Len(Number_R2) = 0 And Len(Nombre_R2) = 0
    Else
        Linea_Cheque         = I
        No_Formas_Pago      = ( No_Formas_Pago + 1 )
        SNivel_R2           = "NA"
        Nombre_Forma_Pago_Ws = Nombre_R2
        Format TicketNumber as Fecha_Numerica{04}, Tienda{05}, Ticket_Unico
        Cons_Item = ( Cons_Item + 1 )
        
        If Total_Aportaciones   <> 0
            Tender_Efectivo_Ttl_2 = ( Tender_Efectivo_Ttl - Total_Aportaciones )  
            Total_Aportaciones  = 0                                             
        Else
            Tender_Efectivo_Ttl_2 = ( Tender_Efectivo_Ttl )
        EndIf
        
        Call Guarda_Campos_Acreditaciones_Formas_Pago(SNivel_R2 , Price_R2, Cons_Item, Unidades_R2, Cheque_R2, Number_R2, Tender_Efectivo_Ttl_2, Nombre_R2 )
        Call Arma_Registro_Acreditaciones_Redenciones
        Call Inserta_Solicitud_Acreditacion_Redencion
        Lineas_Acreditas_Contador = ( Lineas_Acreditas_Contador + 1 )
    EndIf
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Guarda_Campos_Acreditaciones_Formas_Pago                      //
// GUARDA LOS DATOS DE LA ACREDITACION DE FORMAS DE PAGO EN VARIABLES            //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Guarda_Campos_Acreditaciones_Formas_Pago(Ref SNivel_ , Ref Price_, Ref Number_, Ref Unidades_, Ref Cheque_, Ref Number_R_, Ref Total_Ticket_, Ref Nombre_R_ )
    MemberNumber         = NumeroMiembro
    ID                  = "NUEVO"
    ALSELevelPrice      = SNivel_
    BranchDivision      = "WOW"
    ALSEPaymnetMode   = Trim(Nombre_R_)
    ActivityDate         = Business_Date
    AdjustedListPrice   = "0"
    Amount              = Total_Ticket_
    Comments             = ""
    ItemNumber          = Number_
    LocationName         = Tienda
    PartnerName       = PartnerName
    PointName           = ""
    Points              = "0"
    ProcessDate       = Business_Date
    ProcessingComment   = ""
    ProcessingLog       = ""
    ProductName       = "WOW Ticket"
    Quantity             = "0"
    IntegrationStatus   = ""
    Status              = ""    
    SubStatus           = ""
    TicketNumber         = TicketNumber
    TransactionChannel  = "Store"
    TransactionDate   = Business_Date
    TransactionSubType  = "Product"
    TransactionType   = "Accrual"
    VoucherNumber       = ""
    VoucherQty          = "1"
    VoucherType       = ""
    Organization         = "BKCASUAL"
    Estatus           = "PEN"
    Bussiness_date      = Business_Date
    Centroconsumo       = @RVC
    Cheque              = Cheque_
    Transmitido       = "F"
    Linea_Cheque         = 1
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Guarda_Campos_Acreditaciones_Descuentos                       //
// GUARDA LOS DATOS DE LA ACREDITACION DE DESCUENTOS EN VARIABLES                //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Guarda_Campos_Acreditaciones_Descuentos(Ref SNivel_ , Ref Price_, Ref Number_, Ref Unidades_, Ref Cheque_, Ref Number_R_, Ref Total_Ticket_, Ref Nombre_R_ )
    MemberNumber         = NumeroMiembro
    ID                  = "NUEVO"
    ALSELevelPrice      = SNivel_
    BranchDivision      = "WOW"
    ALSEPaymnetMode   = "Descuento"
    ActivityDate         = Business_Date
    AdjustedListPrice   = "0"
    
    If Total_Ticket_ > 0
        Total_Ticket_ = ( Total_Ticket_ * -1 )
    EndIf
    
    Amount              = Total_Ticket_
    Comments             = ""
    ItemNumber          = Number_
    LocationName         = Tienda
    PartnerName       = PartnerName
    PointName           = ""    
    Points              = "0"   
    ProcessDate       = Business_Date
    ProcessingComment   = ""
    ProcessingLog       = ""
    ProductName       = "WOW Dto"
    Quantity             = "0"   
    IntegrationStatus   = ""
    Status              = "Queued"  
    SubStatus           = ""
    TicketNumber         = TicketNumber
    TransactionChannel  = "Store"
    TransactionDate   = Business_Date
    TransactionSubType  = "Product"
    TransactionType   = "Accrual"
    VoucherNumber       = ""
    VoucherQty          = "1"
    VoucherType       = ""
    Organization         = "BKCASUAL"
    Estatus           = "PEN"
    Bussiness_date      = Business_Date
    Centroconsumo       = @RVC
    Cheque              = Cheque_
    Transmitido       = "F"
    Linea_Cheque         = 1
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Guarda_Campos_Acreditaciones_MenuItem                         //
// GUARDA LOS DATOS DE LA ACREDITACION DE ITEMS EN VARIABLES                     //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Guarda_Campos_Acreditaciones_MenuItem(Ref SNivel_ , Ref Price_, Ref Number_, Ref Unidades_, Ref Cheque_, Ref Number_R_, Ref Total_Ticket_, Ref Nombre_Forma_Pago_PM_ )    
    MemberNumber         = NumeroMiembro
    ID                  = "NUEVO"
    ALSELevelPrice      = SNivel_
    BranchDivision      = "WOW"
    ALSEPaymnetMode   = Nombre_Forma_Pago_PM_
    ActivityDate         = Business_Date
    AdjustedListPrice   = Price_         
    Amount              = Total_Ticket_  
    Comments             = ""
    ItemNumber          = Number_
    LocationName         = Tienda
    PartnerName       = PartnerName
    PointName           = ""
    Points              = "0"
    ProcessDate       = Business_Date
    ProcessingComment   = ""
    ProcessingLog       = ""
    ProductName       = Number_R_
    Quantity             = 1
    IntegrationStatus   = ""
    Status              = ""    
    SubStatus           = ""
    TicketNumber         = TicketNumber
    TransactionChannel  = "Store"
    TransactionDate   = Business_Date
    TransactionSubType  = "Product"
    TransactionType   = "Accrual"
    VoucherNumber       = ""
    VoucherQty          = "1"
    VoucherType       = ""
    Organization         = "BKCASUAL"
    Estatus           = "PEN"
    Bussiness_date      = Business_Date
    Centroconsumo       = @RVC
    Cheque              = Cheque_
    Transmitido       = "F"
EndSub


//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Verifica_Descuento_Aplicado_Cheque_Wow                        //
// VERIFICA LOS DESCUENTOS APLICADOS EN EL CHEQUE                                //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Verifica_Descuento_Aplicado_Cheque_Wow
    Var I : N4 = 0

    For I = 1 to @NumDTLT
        If @dtl_type[I] = "S" and @dtl_Is_Void[I] = 0
            If  Mid(@DTL_NAME[I],1,3) = Prefijo_Aportaciones
                If @TNDTTL < @TTLDUE
                    ExitWithError "PAGO DE APORTACIONES CON PUNTOS DEBE SER TOTAL, NO PARCIAL"
                EndIf
            EndIf
        EndIf
    EndFor
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Asigna_Ckid                                                   //
// ASIGNA EL TICKET UNICO AL CKID                                                //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Asigna_Ckid
    IF Len(Trim(@Ckid)) = 0
        Format Ticket_Unico as @WSID{01}, @CKNUM{04}, Mid(@Chk_Open_Time, 10, 2) , Mid(@Chk_Open_Time, 13, 2)
        CLEARCHKID
        @Ckid = ( Ticket_Unico )
    ENDIF
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Trae_Status_Facturable_FPago_Repositorio                      //
//                                                                               //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Trae_Status_Facturable_FPago_Repositorio
    Call Conecta_Base_Datos_Facturacion
    Call Version_Micros_Sybase
    ClearArray FPago_Status_Facturable
    Format SqlStr as "SELECT id_fpago_micros, Facturable FROM facturacion.cfd_cfg_FPago"

    IF Version_Micros = 32 OR Version_Micros = 41 or Version_Micros = 47
        DLLCALL_CDECL hODBCDLL_CFD, sqlGetRecordSet(SqlStr)
        SqlStr1 = ""
        DLLCALL_CDECL hODBCDLL_CFD, sqlGetLastErrorString(ref SqlStr1)
        SqlStr1 = ""
        DLLCALL_CDECL hODBCDLL_CFD, sqlGetFirst(ref SqlStr1)
    ELSEIF Version_Micros = 31
        DLLCall hODBCDLL_CFD, sqlGetRecordSet(SqlStr)
        SqlStr1 = ""
        DLLCall hODBCDLL_CFD, sqlGetLastErrorString(ref SqlStr1)
        IF SqlStr1 <> ""
            Call UnloadDLL_Facturacion
            ExitWithError "Error 22: ", Mid(SqlStr1, 1, 15)
        ENDIF
        SqlStr1 = ""
        DLLCall hODBCDLL_CFD, sqlGetFirst(ref SqlStr1)
    ENDIF

    WHILE Len(Trim(SqlStr1)) > 0
        Split SqlStr1, ";", Numero_F, Cuenta_F
        FPago_Status_Facturable[Numero_F] = Cuenta_F
        DLLCALL_CDECL hODBCDLL_CFD, sqlGetNext(ref SqlStr1)
    ENDWHILE
    Call Cierra_Base_Datos_Facturacion
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Conecta_Base_Datos_Facturacion                                //
// CONECTA A LA BASE DE DATOS DE FACTURACION                                     //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Conecta_Base_Datos_Facturacion
    Call Load_ODBC_DLL_Facturacion
    Call ConnectDB_Facturacion
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Load_ODBC_DLL_Facturacion                                     //
// BUSCA LOS DLL PARA LA CONEXION A A BD DE FACTURACION                          //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Load_ODBC_DLL_Facturacion
    IF hODBCDLL_CFD <> 0
        hODBCDLL_CFD  = 0
    ENDIF

    IF hODBCDLL_CFD = 0
        IF Version_Micros = 32 OR Version_Micros = 41 or Version_Micros = 47
            IF @WSTYPE = 3
                DLLFree hODBCDLL_CFD
                DLLLoad hODBCDLL_CFD, "\cf\micros\bin\MDSSysUtilsProxy.dll"
            ELSE
                DLLFree hODBCDLL_CFD
                DLLLoad hODBCDLL_CFD, "..\bin\MDSSysUtilsProxy.dll"
            ENDIF
        ELSEIF Version_Micros = 31
            DLLLoad hODBCDLL_CFD, "SimODBC.DLL"
        ENDIF
    ENDIF

    IF hODBCDLL_CFD = 0 and Version_Micros = 32
        ExitWithError "No se Puede Cargar DLL (MDSSysUtilsProxy.DLL  [CFD])"
    ELSEIF hODBCDLL_CFD = 0 and Version_Micros = 41
        ExitWithError "No se Puede Cargar DLL (MDSSysUtilsProxy.DLL  [CFD])"
    ELSEIF hODBCDLL_CFD = 0 and Version_Micros = 31
        ExitWithError "No se Puede Cargar DLL (SimODBC.DLL  [CFD])"
    ENDIF
EndSub


//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA ConnectDB_Facturacion                                         //
// CONECTA A LA BASE DE DATOS DE FACTURACION                                     //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub ConnectDB_Facturacion
    Var Status_cfd : N1

    IF Version_Micros = 32
        DLLCALL_CDECL hODBCDLL_CFD, sqlIsConnectionOpen(ref Status_cfd)
        IF Status_cfd =  0
            DLLCALL_CDECL hODBCDLL_CFD, sqlInitConnection("cfd_facturacion","ODBC;UID=dba;PWD=cfdmicros3700v31", "")
        ENDIF
    ELSEIF Version_Micros = 41
        DLLCALL_CDECL hODBCDLL_CFD, sqlIsConnectionOpen(ref Status_cfd)
        IF Status_cfd =  0
            DLLCALL_CDECL hODBCDLL_CFD, sqlInitConnection("cfd_facturacion","ODBC;UID=dba;PWD=cfdmicros3700v31", "")
        ENDIF
    ELSEIF Version_Micros = 31
        DLLCall hODBCDLL_CFD, sqlIsConnectionOpen(ref Status_cfd)
        IF Status_cfd =  0
            DLLCall hODBCDLL_CFD, sqlInitConnection("cfd_facturacion","UID=dba;PWD=cfdmicros3700v31")
        ENDIF
    ENDIF
EndSub


//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Cierra_Base_Datos_Facturacion                                 //
// CIERRA LA CONEXION DE LA BASE DE DATOS                                        //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Cierra_Base_Datos_Facturacion
    Call UnloadDLL_Facturacion
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA UnloadDLL_Facturacion                                         //
// CIERRA LA CONEXION DE LA BASE DE DATOS                                        //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub UnloadDLL_Facturacion
    IF Version_Micros = 32
        DLLCALL_CDECL hODBCDLL_CFD, sqlCloseConnection()
    ELSEIF Version_Micros = 31
        DLLCall hODBCDLL_CFD, sqlCloseConnection()
    ELSEIF Version_Micros = 41
        DLLCALL_CDECL hODBCDLL_CFD, sqlCloseConnection()
    ENDIF
    DLLFree  hODBCDLL_CFD
    hODBCDLL_CFD = 0
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Verifica_FPago_Facturable                                     //
//                                                                               //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Verifica_FPago_Facturable
    Var Con_1   : N3  = 0
    Var Objecto : N8 = 0
    Suma_Total_Facturar = 0
    Suma_Total_No_Factura = 0
    For Con_1 = 1 to @NUMDTLT
        IF @DTL_TYPE[Con_1]   = "T" and @DTL_IS_VOID[Con_1] = 0
            Objecto = @DTL_OBJECT[Con_1]
            IF FPago_Status_Facturable[Objecto] = 1
                Suma_Total_Facturar = ( Suma_Total_Facturar + @DTL_TTL[Con_1] )
            ELSEIF @DTL_TTL[Con_1] > 0
                Suma_Total_No_Factura = ( Suma_Total_No_Factura + @DTL_TTL[Con_1] )
            ENDIF
        ENDIF
    EndFor
    Objecto = @TMDNUM

    IF Objecto > 0 
        IF FPago_Status_Facturable[Objecto] = 1
            Suma_Total_Facturar = ( Suma_Total_Facturar + Monto_Original )
        ELSEIF Monto_Original > 0
            Suma_Total_No_Factura = ( Suma_Total_No_Factura +Monto_Original )
        ENDIF
    ENDIF

    IF ( Suma_Total_Facturar + Suma_Total_No_Factura ) > 0

    ENDIF
        IF Suma_Total_No_Factura     > 0 and Suma_Total_Facturar > 0
        Facturacion_Parcial     = 5
        Facturacion_Parcial_Real  = 4
    ELSEIF Suma_Total_No_Factura > 0 and Suma_Total_Facturar = 0
        Facturacion_Parcial     = 5
        Facturacion_Parcial_Real  = 5
    ELSEIF Suma_Total_No_Factura = 0 and Suma_Total_Facturar > 0
        Facturacion_Parcial     = 1
        Facturacion_Parcial_Real  = 2
    ENDIF
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Verifica_Item_Seleccionado_Void                               //
// VERIFICA EL ITEM QUE QUIERES SER BORRADO                                      //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Verifica_Item_Seleccionado_Void
    Var I             : N4 = 0
    Flag_Descuento_Wow_Void = 0
    Flag_FPago_Wow_Void  = 0
    For I = 1 To @NumDTLT
         If @dtl_Selected[I] = 1
            If mid(@dtl_name[I],1,11) = "Tarjeta Wow" 
                ExitWithError "No puedes borrar este articulo"
            endif
            If @dtl_type[I] = "D" and @dtl_Is_Void[I] = 0

                Format Etiqueta_A_Buscar as "Descuento_Redenciones$"
                Call Consulta_Configuracion_CRM( Etiqueta_A_Buscar )
                Descuento_Redenciones_M = SqlStr1

                Format Etiqueta_A_Buscar as "Descuento_Redenciones%"
                Call Consulta_Configuracion_CRM( Etiqueta_A_Buscar )
                Descuento_Redenciones_P = SqlStr1

                Format Etiqueta_A_Buscar as "Descuento_RedondeoWOW"
                Call Consulta_Configuracion_CRM( Etiqueta_A_Buscar )
                Descuento_RedondeoWOW = SqlStr1

                If @DTL_OBJECT[I] = Descuento_RedondeoWOW   Or @Dtl_Objnum[I] = Descuento_RedondeoWOW   Or \\
                    @DTL_OBJECT[I] = Descuento_Redenciones_P Or @Dtl_Objnum[I] = Descuento_Redenciones_P Or \\
                    @DTL_OBJECT[I] = Descuento_Redenciones_M Or @Dtl_Objnum[I] = Descuento_Redenciones_M
                    Flag_Descuento_Wow_Void = 1
                EndIf
            ElseIf @dtl_type[I] = "T" and @dtl_Is_Void[I] = 0

                Format Etiqueta_A_Buscar as "Tender_Crm"
                Call Consulta_Configuracion_CRM( Etiqueta_A_Buscar )
                Tender_Crm = SqlStr1

                If @DTL_OBJECT[I] = Tender_Crm  
                    Flag_FPago_Wow_Void = 1
                EndIf
            EndIf
        EndIf
    EndFor
EndSub
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Busca_Aportacion_Acreditaciones_Soa_Only                      //
// CUENTA LAS DONACIONES EN EL CHEQUE                                            //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Busca_Aportacion_Acreditaciones_Soa_Only
    Var I               : N4  = 0
    Var Buscar        : N8  = 0
    Total_Aportaciones      = 0
    For I = 1 to @NUMDTLT
        If @DTL_TYPE[I] = "S" AND @DTL_IS_VOID[I] = 0
            Buscar = @DTL_OBJECT[I]
            Call Buca_ServiceCharge_Donacion(Buscar, Flag_Busca_Donacion)
            If Flag_Busca_Donacion = "T"
                Total_Aportaciones = ( Total_Aportaciones + @DTL_TTL[I] )
            Else
            EndIf
        EndIf
    EndFor
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Buca_ServiceCharge_Donacion                                   //
// BUSCA DONACIONES EN EL CHEQUE                                                 //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Buca_ServiceCharge_Donacion(Ref Buscar_, Ref Flag_Busca_Donacion_)
    Var I : N2 = 0
    Flag_Busca_Donacion_ = ""
    For I = 1 To CC_Donaciones
        If Donaciones[I] = Buscar_
            Flag_Busca_Donacion_ = "T"
            Return
        EndIf
    EndFor
EndSub


//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Suma_MenuItem_Soa_Only                                        //
//                                                                               //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Suma_MenuItem_Soa_Only
    Var I   : N4  = 0
    Total_MenuItem = 0
    For I = 1 to @NUMDTLT
        If @DTL_TYPE[I] = "M" AND @DTL_IS_VOID[I] = 0
            Total_MenuItem = ( Total_MenuItem + @DTL_TTL[I] )
        EndIf
    EndFor
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Busca_Aportacion_Redencion_Soa                                //
// BUSCA APORTACIONES EN LA REDENCION                                            //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Busca_Aportacion_Redencion_Soa
    Var I               : N4  = 0
    Var Cons_Item_R  : N1  = 0

    For I = 1 to @NUMDTLT
        If @DTL_TYPE[I] = "S" AND @DTL_IS_VOID[I] = 0
            
            If  Mid(@DTL_NAME[I],1,3) = Prefijo_Aportaciones
                SNivel_R     = Nivel_Price[@MLVL[I]]
                Price_R   = @DTL_TTL[I]
                Number_R     = @DTL_OBJECT[I]
                Nombre_R     = @DTL_NAME[I]
                Unidades_R  = @DTL_QTY[I]
                Cheque_R     = @CKNUM
                Linea_Cheque = I
                SNivel_R     = "NA"
                Unidades_R  = 0 
                Opcion_Redencion      = 1
                VoucherProductName[1] = ""
                AdjustedListPrice    = "0"
                Flag_Final_Tender    = "AF"

                Format TicketNumber as Fecha_Numerica{04}, Tienda{05}, Ticket_Unico
                Nombre_R, Unidades_R, Cheque_R, Number_R)
                Call Guarda_Campos_Acreditaciones_Redenciones_Aportacion(SNivel_R, Price_R, Nombre_R, Unidades_R, Cheque_R, Number_R, Name_FP)  //27Ene2015
                Call Formatea_Datos_Redenciones
                Call Manda_Redemption
                Call Valida_Redemption
            EndIf
        EndIf
    EndFor
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Manda_Redemption_Miembro_O                                    //
// EJECUTA LA REDENCION                                                          //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//


Sub Manda_Redemption_Miembro_O
    Manda = ""
    Format Manda, Separador as "RED", Arma_Cadena_82, Nombre_Miembro_O
    TxMsg Manda
    GetRxmsg "Espere... Cliente WOW"
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Guarda_Campos_Acreditaciones_Redenciones_Aportacion           //
// GUARDA LOS CAMPOS DE ACREDITACIO Y REDENCION EN VARIABLES                     //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Guarda_Campos_Acreditaciones_Redenciones_Aportacion(Ref SNivel_ , Ref Price_, Ref Number_, Ref Unidades_, Ref Cheque_, Ref ItemNumber_, Ref Name_FP_ )
    Var V_Qty  : A1 = ""

    If Trim(Number_) = "WOW Ticket"
        Unidades_ = 0
    EndIf
    Quantity = Unidades_
      
    If Flag_Final_Tender = "AF"
        V_Qty    = ""
    Else
        V_Qty    = "1"
    EndIf
    
    MemberNumber        = NumeroMiembro
    ID                  = "NUEVO"
    ALSELevelPrice      = SNivel_
    BranchDivision      = "BKCASUAL"
    ALSEPaymnetMode     = Trim(Name_FP_)    //27Ene2015 "Puntos WOW"
    ActivityDate        = Business_Date
    AdjustedListPrice   = Price_    //08Jul2015 
    Amount              = "0"   //Price_ IVAN
    Comments            = ""
    ItemNumber          = ItemNumber_
    LocationName        = Tienda
    PartnerName         = PartnerName
    PointName           = ""  //"Star"
    Points              = "0"  //"0"
    ProcessDate         = Business_Date
    ProcessingComment   = ""
    ProcessingLog       = ""
    ProductName         = "Aportacion"  
    Quantity            = Unidades_
    IntegrationStatus   = ""
    Status              = ""    
    SubStatus           = ""
    TicketNumber        = TicketNumber
    TransactionChannel  = "Store"
    TransactionDate     = Business_Date
    TransactionSubType  = "Product"
    TransactionType     = "Redemption"
    VoucherNumber       = ""  //VoucherTransactionId[Opcion_Redencion]
    VoucherQty          = V_Qty
    VoucherType         = VoucherProductName[Opcion_Redencion]  //Aportacion = debe ir vacio
    Organization        = "BKCASUAL"
    Estatus             = "PEN"
    Bussiness_date      = Business_Date_Real
    Centroconsumo       = @RVC
    Cheque              = Cheque_
    Transmitido         = "F"
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Busca_Points_Cheque                                           //
// BUSCA LOS PUNTOS Y PESOS DEL MIEMBRO WOW                                      //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Busca_Points_Cheque
    Var Camp_01 : A20 = ""
    Var Camp_02 : A20 = ""
    For I = 1 to @NumDtlT
        If @dtl_type[I] = "I" And Len(Trim(@DTL_NAME[I])) > 0
            If Mid(Trim(@DTL_NAME[I]),1,5) = "Pesos"
                Format Points_Cheque_A As Mid(@DTL_NAME[I], 9, 10)
                Points_Cheque = Points_Cheque_A
                Split @DTL_NAME[I], ":", Camp_01, Camp_02
                Points_Cheque = Camp_02
            EndIf
        EndIf 
    EndFor
EndSub
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Inserta_Combo_Count                                           //
//                                                                               //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
Sub Inserta_Combo_Count
    Var counter : N3 = 1 
    Call Version_Micros_Sybase
    CALL ConnectDB
    CALL Query_Business_Date

    for counter = 1 to @NUMDTLT Step 1 
        if ( @dtl_is_combo_parent[counter] ) 
            v_mi_seq_cmb=@Dtl_Sequence[counter]
        endif
        if ( @dtl_is_combo_side[counter] ) 
            v_mi_seq = @Dtl_Sequence[counter]
            v_obj_num=@Dtl_Object[counter]
            v_menu_lvl = @Dtl_mlvl[counter]
            v_qty = @Dtl_Qty[counter]
            Call Insert_Combo   
        endif

        if ( @dtl_is_combo_main[counter] ) 
         
            v_mi_seq = @Dtl_Sequence[counter]
            v_obj_num=@Dtl_Object[counter]
            v_menu_lvl = @Dtl_mlvl[counter]
            v_qty = @Dtl_Qty[counter]
            Call Insert_Combo
        endif
    endfor
    CALL DisconnetDB
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Inserta_Combo                                                 //
// INSERTA CMBOS EN LA BASE DE DATOS                                             //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Insert_Combo
    SqlStrA=""
    Format SqlStrA as "INSERT INTO custom.combo_count (business_date,mi_seq,obj_num,mi_seq_cmb, menu_lvl,status, qty, rvc)  VALUES ('",v_business_date, "','",v_mi_seq,"','",v_obj_num,"','",v_mi_seq_cmb,"','",v_menu_lvl,"','",v_status,"','",v_qty,"','",@RVC,"')"
    DLLCALL_CDECL hODBCDLL, sqlExecuteQuery(SqlStrA)
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Query_Business_Date                                           //
// SELECIONA EL BUSINESS DATE                                                    //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Query_Business_Date
    SqlStrA=""
    Format SqlStrA as "SELECT business_date FROM micros.rest_status WITH (NOLOCK)"
    DLLCALL_CDECL hODBCDLL, sqlGetRecordSet(ref SqlStrA) 
    DLLCALL_CDECL hODBCDLL, sqlGetFirst(ref SqlStr1)
    Split SqlStr1, ";", v_business_date
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA ConnectDB                                                     //
// CONECTA A LA BASE DE DATOS                                                    //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub ConnectDB
    Var constatus : N9
    DLLLoad hODBCDLL, "..\bin\MDSSysUtilsProxy.dll"
    DLLCALL_CDECL hODBCDLL, sqlIsConnectionOpen(ref constatus)
    DLLCALL_CDECL hODBCDLL, sqlInitConnection("micros","ODBC;UID=dba;PWD=Password1", "")
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA DisconnetDB                                                   //
// DESCONECTA DE LA BASE DE DATOS                                                //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
Sub DisconnetDB
    DLLCALL_CDECL hODBCDLL, sqlCloseConnection()
    DLLFree  hODBCDLL
    hODBCDLL = 0
Endsub 

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Imprime_Ticket_info_socio_wow                                 //
// IMPRIME TICKET DE INFO SOCIO                                                  //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Imprime_Ticket_info_socio_wow
    startprint @CHK
        printline "~~bklogo1.bmp"
        printline "Gracias ", Mid(Nombre, 1, 24)
        printline "por ser socio WOW Rewards "
        printline Trim(Activo), "/", Trim(Nivel), "", Mid(BirthDate,1,5) 
        printline "Actualmente tienes: "
        printline "Points ", puntoswow
        printline "Pesos: ", pointAmount{10} 
    endprint
endsub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Verifica_Descuento_Tmed_Wow                                   //
//                                                                               //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Verifica_Descuento_Tmed_Wow
    Var II  : N4 = 0
    For II = 1 to @NUMDTLT
        If @dtl_type[II] = "D" And @dtl_Is_Void[II] = 0 And @dtl_Ttl[II] <> 0

            Format Etiqueta_A_Buscar as "Descuento_Redenciones$"
            Call Consulta_Configuracion_CRM( Etiqueta_A_Buscar )
            Descuento_Redenciones_M = SqlStr1

            Format Etiqueta_A_Buscar as "Descuento_Redenciones%"
            Call Consulta_Configuracion_CRM( Etiqueta_A_Buscar )
            Descuento_Redenciones_P = SqlStr1

            If @DTL_OBJECT[II] = Descuento_Redenciones_P Or @DTL_OBJECT[II] = Descuento_Redenciones_M
                InfoMessage "WOW CRM", "NO SE PERMITE DESLIGAR TARJETA"
                ExitCancel
            EndIf
        ElseIf @dtl_type[II] = "T" And @dtl_Is_Void[II] = 0 And @dtl_Ttl[II] <> 0

            Format Etiqueta_A_Buscar as "Tender_Crm"
            Call Consulta_Configuracion_CRM( Etiqueta_A_Buscar )
            Tender_Crm = SqlStr1

            If @DTL_OBJECT[II] = Tender_Crm
                InfoMessage "WOW CRM", "NO SE PERMITE DESLIGAR TARJETA"
                ExitCancel
            EndIf
        EndIf
    EndFor
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Inserta_Miembro_Ocupa_Pos                                     //
// INSERTA EL MIEBRO QUE ESTA OCUPADO                                            //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Inserta_Miembro_Ocupa_Pos(Ref Numero_Tarjeta_, Ref Wsid_)
    Var Temp1   : $100  = 0
    Call Conecta_Base_Datos_Sybase
    Format SqlStr as "INSERT INTO micros.configuracion_Micros_Wow (numero_Pos, accion, datos ) VALUES ( "
    Format SqlStr as SqlStr, "  ", Wsid_,     ","
    Format SqlStr as SqlStr, " 'Miembro_Crm',"
    Format SqlStr as SqlStr, " '", Numero_Tarjeta_, "' "
    Format SqlStr as SqlStr, " )"

    IF Version_Micros = 41
        DLLCALL_CDECL hODBCDLL, sqlExecuteQuery(SqlStr)
        SqlStr1 = ""
        DLLCALL_CDECL hODBCDLL, sqlGetLastErrorString(ref SqlStr1)
        IF (SqlStr1 <> "")
            InfoMessage "Error Miembro: ", Mid(SqlStr1,1,30)
        ENDIF
        SqlStr1 = ""
    ENDIF

    IF SqlStr1  = ""
    ELSE
        Split SqlStr1, ";", Temp1
    ENDIF
    Call UnloadDLL_SyBase
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Delete_Miembro_Ocupa_Pos                                      //
// ELIMINA EL MIEBRO QUE ESTA OCUPADO                                            //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
Sub Delete_Miembro_Ocupa_Pos(Ref Wsid_)
    Var Temp1   : $100  = 0
    Call Conecta_Base_Datos_Sybase
    Format SqlStr as "DELETE micros.configuracion_Micros_Wow WHERE numero_Pos = ", Wsid_
    
    IF  Version_Micros = 41
        DLLCALL_CDECL hODBCDLL, sqlExecuteQuery(SqlStr)
        SqlStr1 = ""
        DLLCALL_CDECL hODBCDLL, sqlGetLastErrorString(ref SqlStr1)
    
        IF (SqlStr1 <> "")
            InfoMessage "Error Miembro: ", Mid(SqlStr1,1,30)
        ENDIF
        SqlStr1 = ""
    ENDIF
    
    IF SqlStr1  = ""
    ELSE
        Split SqlStr1, ";", Temp1
    ENDIF
    Call UnloadDLL_SyBase
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Consulta_Miembro_Ocupa_Pos                                    //
// CONSULTA EL MIEBRO QUE ESTA OCUPADO                                           //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Consulta_Miembro_Ocupa_Pos(Ref Numero_Tarjeta_)
    Var File_No : N6 = 0
    Call Conecta_Base_Datos_Sybase
    
    Format SqlStr as "SELECT numero_Pos, accion, datos FROM micros.configuracion_Micros_Wow Where datos = '", Numero_Tarjeta_, "'"
    
    IF Version_Micros = 32 or Version_Micros = 41
        DLLCALL_CDECL hODBCDLL, sqlGetRecordSet(SqlStr)
        SqlStr1 = ""
        DLLCALL_CDECL hODBCDLL, sqlGetLastErrorString(ref SqlStr1)
        IF (SqlStr1 <> "")
            Call Muestra_Error(SqlStr1)
            Call UnloadDLL_SyBase
            ExitCancel
        ENDIF
        SqlStr1 = ""
        DLLCALL_CDECL hODBCDLL, sqlGetFirst(ref SqlStr1)

    ElseIF Version_Micros = 31
        DLLCall hODBCDLL, sqlGetRecordSet(SqlStr)
        SqlStr1 = ""
        DLLCall hODBCDLL, sqlGetLastErrorString(ref SqlStr1)
        IF SqlStr1 <> ""
            Call Muestra_Error(SqlStr1)
            Call UnloadDLL_SyBase
            ExitCancel
        ENDIF
        SqlStr1 = ""
        DLLCall hODBCDLL, sqlGetFirst(ref SqlStr1)
    ENDIF

    IF SqlStr1 = ""
        numero_Pos = 0
        accion   = ""
        datos_wsid = ""
    Else
        Split SqlStr1, ";", numero_Pos, accion, datos_wsid
    EndIf
    Call UnloadDLL_SyBase
EndSub
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Consolidado_Descuentos_Acreditaciones_Soa_Wow_2               //
//                                                                               //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
Sub Consolidado_Descuentos_Acreditaciones_Soa_Wow_2
    Var I               : N4  = 0
    Var TTl_Descto_01   : $10 = 0
    Var TTl_Descto_02   : $10 = 0
    
    For I = 1 to @NUMDTLT
        If @DTL_TYPE[I] = "D" AND @DTL_IS_VOID[I] = 0
            Busca_Descto = @DTL_OBJECT[I]
            Call Verifca_Si_Existe_Descto(Busca_Descto, Linea_Encontrado_D)

            If Si_Existe_Descuento = "T"
                If @DTL_TTL[I] < 0
                    TTl_Descto_01 = ( @DTL_TTL[I] * -1 )
                Else
                    TTl_Descto_01 = ( @DTL_TTL[I] )
                EndIf
                Precio_Descto[Contador_Descto]  = Precio_Descto[Contador_Descto]    + TTl_Descto_01
                Unidades_Descto[Contador_Descto] = Unidades_Descto[Contador_Descto] + @DTL_QTY[I]
            Else
                Contador_Descto = ( Contador_Descto + 1 )
                If @DTL_TTL[I] < 0
                    TTl_Descto_01 = ( @DTL_TTL[I] * -1 )
                Else
                    TTl_Descto_01 = ( @DTL_TTL[I] )
                EndIf
                Objeto_Descto[Contador_Descto]  = @DTL_OBJECT[I]
                Nivel_Descto[Contador_Descto]   = Nivel_Price[@MLVL[I]]
                Precio_Descto[Contador_Descto]  = TTl_Descto_01
                Nombre_Descto[Contador_Descto]  = @DTL_NAME[I]
                Unidades_Descto[Contador_Descto] = @DTL_QTY[I]
                Linea_Descto[Contador_Descto]   = I
            EndIf
        EndIf
    EndFor

    For I = 1 to Contador_Descto
        Price_R     = Precio_Descto[Contador_Descto]
        Number_R      = Objeto_Descto[Contador_Descto]
        Nombre_R      = Nombre_Descto[Contador_Descto]
        Unidades_R  = Unidades_Descto[Contador_Descto]
        Cheque_R      = @CKNUM
        Linea_Cheque  = Linea_Descto[Contador_Descto]
        SNivel_R      = "NA"
        TTl_Descto_01 = Price_R
        TTl_Descto_02 = Price_R
        Call Consolidado_Descuento_Acreditacion_Descuento_RedondeoWOW_Wow_2(TTl_Descto_01, TTl_Descto_02 )
    EndFor
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Consolidado_Descuento_Acreditacion_Descuento_RedondeoWOW_Wow_2//
//                                                                               //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Consolidado_Descuento_Acreditacion_Descuento_RedondeoWOW_Wow_2(Ref TTl_Descto_01, Ref TTl_Descto_02 )
    If TTl_Descto_02 <> 0
        Format TicketNumber as Fecha_Numerica{04}, Tienda{05}, Ticket_Unico
        Cons_Item = ( Cons_Item + 1 )
        Call Guarda_Campos_Acreditaciones_Descuentos(SNivel_R , Price_R, Cons_Item, Unidades_R, Cheque_R, Number_R, Price_R, Nombre_R )
        Call Arma_Registro_Acreditaciones_Redenciones
        Call Inserta_Solicitud_Acreditacion_Redencion
        Lineas_Acreditas_Contador = ( Lineas_Acreditas_Contador + 1 )
    EndIf
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Verifca_Si_Existe_Descto                                      //
// VERIFICA SI EXISTE UN DESCUENTO                                               //                                                                            
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Verifca_Si_Existe_Descto(Ref Buscar_, Ref Linea_Encontrado_D_)
    Var JJ : N3 = 0
    Si_Existe_Descuento = ""
    Linea_Encontrado_D_ = 0
    For JJ = 1 To Contador_Descto
        If Objeto_Descto[JJ] = Buscar_
            Si_Existe_Descuento = "T"
            Linea_Encontrado_D_ = Linea_Descto[JJ]
            Return
        EndIf
    EndFor
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Verifica_Aportaciones_Aplicado_Cheque                         //
// VERIFICA SI HAY UNA APORTACION EN E CHEQUE                                    //                                                                            
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Verifica_Aportaciones_Aplicado_Cheque
    Var I : N4 = 0
    Existe_Aportacion = ""
    Existe_Menu_Item  = ""
    For I = 1 to @NumDTLT
        If @dtl_type[I] = "S" and @dtl_Is_Void[I] = 0
            If  Mid(@DTL_NAME[I],1,3) = Prefijo_Aportaciones
                Existe_Aportacion = "T"
            EndIf
        ElseIf @DTL_TYPE[I] = "M" AND @DTL_IS_VOID[I] = 0
            Existe_Menu_Item = "T"
        EndIf
    EndFor
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Busca_Forma_en_Cheque                                         //
// VERIFICA SI HAY UNA FORMA DE PAGO                                             //                                                                            
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Busca_Forma_en_Cheque
    Forma_Pago_en_Cheque = ""
    For I = 1 to @NumDTLT
        If @dtl_type[I] = "T" and @dtl_Is_Void[I] = 0
            Forma_Pago_en_Cheque = "T"
        EndIf
    EndFor
EndSub


//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Verifica_Monto_Total_Cheque                                   //
// VERIFICA EL MONTO TOTAL DE CHEQUE                                             //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Verifica_Monto_Total_Cheque
    Var II                    : N3  = 0
    Var MontoTtlChequeDescuento : $12 = 0
    MontoTtlCheque = 0

    For II = 1 to @NUMDTLT
        If   @dtl_type[II] = "M" And @dtl_Is_Void[II] = 0 And @dtl_Ttl[II] <> 0
            MontoTtlCheque = ( MontoTtlCheque + @DTL_TTL[II] )
        ElseIf @dtl_type[II] = "D" And @dtl_Is_Void[II] = 0 And @dtl_Ttl[II] <> 0
            MontoTtlChequeDescuento = ( MontoTtlChequeDescuento + @DTL_TTL[II] )
        EndIf
    EndFor
    MontoTtlCheque = ( MontoTtlCheque + MontoTtlChequeDescuento )
Endsub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA msgbox_BK                                                     //
//                                                                               //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub msgbox_BK
    Var Msj           : A80  = ""
    
    Format SqlStr as "select ISNULL(addr_line_1,'') as MSJ from micros.rest_def;"
    Call Consulta_Sql
    
    IF Len(Trim(SqlStr1)) > 1 and Len(Trim(SqlStr1)) <= 81 Then
        Split SqlStr1, ";", Msj
        InfoMessage "Mensaje", Msj
    EndIF

    Call UnloadDLL_SyBase
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA Conecta_Base_Datos_Sybase_Wow                                 //
// CONECTA A A BASSA DE DATOS                                                    //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//

Sub Conecta_Base_Datos_Sybase_Wow
    Call Version_Micros_Sybase
    Call Load_ODBC_DLL_SyBase
    Call ConnectDB_SyBase_Wow
EndSub
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//       SUBRUTINA ConnectDB_SyBase_Wow                                          //
// CONECTA A A BASSA DE DATOS                                                    //
// FECHA MODIFICACION: 04/07/2017                                                //
// MODIFICACION: DEVWORMS                                                        //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
Sub ConnectDB_SyBase_Wow
    Var Status : N1

    IF Version_Micros = 32
        DLLCALL_CDECL hODBCDLL, sqlIsConnectionOpen(ref Status)
        IF Status =  0
            DLLCALL_CDECL hODBCDLL, sqlInitConnection("micros","ODBC;UID=dba;PWD=Password1", "")
        ENDIF
    ElseIF Version_Micros = 41
        DLLCALL_CDECL hODBCDLL, sqlIsConnectionOpen(ref Status)
        IF Status =  0
            DLLCALL_CDECL hODBCDLL, sqlInitConnection("micros","ODBC;UID=dba;PWD=Password1", "")
        ENDIF
    ElseIF Version_Micros = 31
        DLLCall hODBCDLL, sqlIsConnectionOpen(ref Status)
        IF Status =  0
            DLLCall hODBCDLL, sqlInitConnection("micros","UID=dba;PWD=Password1")
        ENDIF
    ENDIF
EndSub
