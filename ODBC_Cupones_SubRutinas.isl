//DESADEP26052017
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                           SubRutina AbreConexion                              //
// RUTINA DE CONEXION A BD                                                       //
// MODIFICACION ALSEA                                                            //
// FECHA MODIFICACION: 2017-05-26                                                //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
Var hODBCDLLCUP   : N12   = 0
Sub AbreConexion
    var Status       :N1
    var CG_USU       : A15  = "dba"
    var CG_PASSWORD  : A25  = "Password1"
	var CG_CADCONEX  : A100
    var CG_SENDORDER :N3   = 409

	IF hODBCDLLCUP <> 0
       hODBCDLLCUP  = 0
    ENDIF
    IF hODBCDLLCUP = 0
	    IF Version_Micros_Cup = 31
           DLLLoad hODBCDLLCUP, "SimODBC.DLL"
        ELSE 
           IF @WSTYPE = 3
              DLLFree hODBCDLLCUP
              DLLLoad hODBCDLLCUP, "\cf\micros\bin\MDSSysUtilsProxy.dll"
           Else
              DLLFree hODBCDLLCUP
              DLLLoad hODBCDLLCUP, "MDSSysUtilsProxy.dll"
           ENDIF
        ENDIF
    ENDIF
	IF hODBCDLLCUP = 0 and Version_Micros_Cup = 31
       ExitWithError "No se Puede Cargar DLL (SimODBC.DLL)"
    ELSEIF hODBCDLLCUP = 0
       ExitWithError "No se Puede Cargar DLL (MDSSysUtilsProxy.DLL)"
    ENDIF

	IF  Version_Micros_Cup = 31
       DLLCall hODBCDLLCUP, sqlIsConnectionOpen(ref Status)
       IF Status =  0
             FORMAT CG_CADCONEX AS "ODBC;UID=",CG_USU,";PWD=",CG_PASSWORD
             DLLCall hODBCDLLCUP, sqlInitConnection("micros",CG_CADCONEX, "")
       ENDIF
	ELSE
       DLLCALL_CDECL hODBCDLLCUP, sqlIsConnectionOpen(ref Status)
       IF Status =  0
             FORMAT CG_CADCONEX AS "ODBC;UID=",CG_USU,";PWD=",CG_PASSWORD
             DLLCALL_CDECL hODBCDLLCUP, sqlInitConnection("micros",CG_CADCONEX, "")
       ENDIF
    ENDIF
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                           SubRutina CierraOdbc                                //
// RUTINA DE CIERRE PARA CONEXION A BD                                           //
// MODIFICACION ALSEA                                                            //
// FECHA MODIFICACION: 2017-05-26                                                //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
Sub CierraOdbc
    IF Version_Micros_Cup = 31
       DLLCall hODBCDLLCUP, sqlCloseConnection()
    Else
       DLLCALL_CDECL hODBCDLLCUP, sqlCloseConnection()
    ENDIF
    DLLFree  hODBCDLLCUP
    hODBCDLLCUP = 0
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                           SubRutina Muestra_Error                             //
// MUESTRA UN MENSAJE EN PANTALLA CON EL DETALLE DEL ERROR                       //
// FECHA MODIFICACION: 2017-05-26                                                //
//                                                                               //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
Sub Muestra_Error(Ref Datos_w)
    Window 5,78
    Display 1, 1, MID(Datos_w,   1, 78)
    Display 2, 1, MID(Datos_w,  79, 78)
    Display 3, 1, MID(Datos_w, 156, 78)
    Display 4, 1, MID(Datos_w, 234, 78)
    WaitForEnter
    WindowClose
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       SUBRUTINA DeleteCupon                                   //
// BORRA EL CUPON DE LA TABLA CUANDO HAY UN ERROR,DEVOLUCION O POLITICAS         //
// MODIFICACION ALSEA                                                            //
// FECHA MODIFICACION: 2017-05-26                                                //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
Sub DeleteCupon(var IDFolioCupon: A10, var Force:N1)
    CALL Version_Micros_Cup_SyBase
    CALL AbreConexion    
    Var pSqlString        : A500 = ""
    Var pSqlResp          : A500 = ""
    
    IF len(IDFolioCupon)>5 AND Force = 0
       FORMAT pSqlString as "Delete from custom.TADatosCupon  WHERE FIIDTicket = ", FIIDTicket, " AND (FIIDCodigoEstatus = 1 OR FIIDCodigoEstatus = 0) AND FCIDFolioCupon='",IDFolioCupon,"'"
	ELSEIF len(IDFolioCupon)>5 AND Force = 1
       FORMAT pSqlString as "Delete from custom.TADatosCupon  WHERE FIIDTicket = ", FIIDTicket, " AND FCIDFolioCupon='",IDFolioCupon,"'"
    ELSE
       FORMAT pSqlString as "Delete from custom.TADatosCupon  WHERE FIIDCodigoEstatus = 0 AND FIIDTicket = ", FIIDTicket
   
    ENDIF    
    IF Version_Micros_Cup = 31
       DLLCALL hODBCDLLCUP, sqlExecuteQuery(pSqlString)
       pSqlResp = ""
       DLLCALL hODBCDLLCUP, sqlGetLastErrorString(ref pSqlResp) 
    ELSE
       DLLCALL_CDECL hODBCDLLCUP, sqlExecuteQuery(pSqlString)
       pSqlResp = ""
       DLLCALL_CDECL hODBCDLLCUP, sqlGetLastErrorString(ref pSqlResp) 
    ENDIF
    
    IF (pSqlResp <> "")
       CALL Muestra_Error(pSqlResp)
	   // CIERRA LA CONEXION
       CALL CierraOdbc
       EXITCANCEL
    ENDIF
	
	// CIERRA LA CONEXION
    CALL CierraOdbc
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       SUBRUTINA ActualizarStatusPOS                           //
// ACTUALIZA LA BANDERA DEL STATUS DEL CUPON PARA PODER BORRARLO                 //
// MODIFICACION ALSEA                                                            //
// FECHA MODIFICACION: 2017-05-26                                                //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
Sub ActualizarStatusPOS(var status :N2, var IDFolioCupon: A10, var IDTicket: A12)
    Var pSqlString        : A500 = ""
    Var pSqlResp          : A500 = ""
	
    CALL Version_Micros_Cup_SyBase
	
    CALL AbreConexion
    
	If status = 3
	   FORMAT pSqlString as "Update custom.TADatosCupon Set FIIDCodigoEstatus=",status,", FCDescripcionEstatus = 'EXITOSO' WHERE FIIDTicket=", IDTicket," AND FCIDFolioCupon='",IDFolioCupon,"'"
	ELSE
       FORMAT pSqlString as "Update custom.TADatosCupon Set FIIDCodigoEstatus=",status," WHERE FIIDTicket=", IDTicket," AND FCIDFolioCupon='",IDFolioCupon,"'"
    ENDIF 
	
	IF Version_Micros_Cup = 31
       DLLCALL hODBCDLLCUP, sqlExecuteQuery(pSqlString)
       pSqlResp = ""
       DLLCALL hODBCDLLCUP, sqlGetLastErrorString(ref pSqlResp)    
	ELSE
       DLLCALL_CDECL hODBCDLLCUP, sqlExecuteQuery(pSqlString)
       pSqlResp = ""
       DLLCALL_CDECL hODBCDLLCUP, sqlGetLastErrorString(ref pSqlResp) 
    ENDIF
	
    IF (pSqlResp <> "")
         CALL Muestra_Error(pSqlResp)
		 // CIERRA LA CONEXION
         CALL CierraOdbc
         EXITCANCEL
    ENDIF
    
	// CIERRA LA CONEXION
	CALL CierraOdbc
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       SUBRUTINA ValidaCupon                                   //
// ACTUALIZA LA BANDERA DEL STATUS DEL CUPON PARA PODER BORRARLO                 //
// MODIFICACION ALSEA                                                            //
// FECHA MODIFICACION: 2017-05-26                                                //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
Sub ValidaCupon(ref IDFolioCupon,var FormaPago:N1)
    Var pSqlString        : A500 = ""
    Var pSqlResp          : A500 = ""
	
    CALL Version_Micros_Cup_SyBase
	
    CALL AbreConexion
    
    IF FormaPago = 0
        FORMAT pSqlString as "Select FIIDCodigoEstatus,FCIDFolioCupon from custom.TADatosCupon where FIIDCodigoEstatus=1 AND FIIDTicket=", @CKNUM
    ELSEIF FormaPago = 1
        FORMAT pSqlString as "Select FIIDCodigoEstatus,FCIDFolioCupon from custom.TADatosCupon where FIIDCodigoEstatus=1 AND FIIDTipoPromocion = 4 AND FIIDTicket=", @CKNUM
    ENDIF
	
    IF Version_Micros_Cup = 31
       DLLCALL hODBCDLLCUP, sqlGetRecordSet(pSqlString)
       pSqlResp = ""
       DLLCALL hODBCDLLCUP, sqlGetLastErrorString(ref pSqlResp)
       IF (pSqlResp <> "")
           CALL Muestra_Error(pSqlResp)
		   // CIERRA LA CONEXION
           CALL CierraOdbc
           EXITCANCEL
       ENDIF
       pSqlResp = ""
       DLLCALL hODBCDLLCUP, sqlGetFirst(ref pSqlResp)
    ELSE
       DLLCALL_CDECL hODBCDLLCUP, sqlGetRecordSet(pSqlString)
       pSqlResp = ""
       DLLCALL_CDECL hODBCDLLCUP, sqlGetLastErrorString(ref pSqlResp)
       IF (pSqlResp <> "")
           CALL Muestra_Error(pSqlResp)
		   // CIERRA LA CONEXION
           CALL CierraOdbc
           EXITCANCEL
       ENDIF
       pSqlResp = ""
       DLLCALL_CDECL hODBCDLLCUP, sqlGetFirst(ref pSqlResp)
    ENDIF
    
    IF pSqlResp = ""
       gExisteCupon = ""
       IDFolioCupon=""
    ELSE
       IF Len(Trim(pSqlResp)) > 0 Then
          SPLIT pSqlResp, ";", gExisteCupon,IDFolioCupon
       ENDIF
    ENDIF
	
	// CIERRA LA CONEXION
    CALL CierraOdbc
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       SUBRUTINA ConsultaPlataforma                            //
// VERIFICA SI EL TIPO DE CUPON SE ENCUENTRA ACTIVO PARA ESTA PLATAFORMA         //
// MODIFICACION ALSEA                                                            //
// FECHA MODIFICACION: 2017-05-26                                                //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
Sub ConsultaPlataforma
    Var pSqlString        : A500 = ""
    Var pSqlResp          : A500 = ""
    CALL Version_Micros_Cup_SyBase
    CALL AbreConexion
    
    FORMAT pSqlString as "Select esactivo,idMarca from custom.TATipoCuponConfg"
	
	 IF Version_Micros_Cup = 31
       DLLCALL hODBCDLLCUP, sqlGetRecordSet(pSqlString)
       pSqlResp = ""
       DLLCALL hODBCDLLCUP, sqlGetLastErrorString(ref pSqlResp)
       IF pSqlResp <> ""
          CALL Muestra_Error(pSqlResp)
		  // CIERRA LA CONEXION
          CALL CierraOdbc
          EXITCANCEL
       ENDIF
       pSqlResp = ""
       DLLCALL hODBCDLLCUP, sqlGetFirst(ref pSqlResp)
    ELSE
       DLLCALL_CDECL hODBCDLLCUP, sqlGetRecordSet(pSqlString)
       pSqlResp = ""
       DLLCALL_CDECL hODBCDLLCUP, sqlGetLastErrorString(ref pSqlResp)
       IF (pSqlResp <> "")
           CALL Muestra_Error(pSqlResp)
		   // CIERRA LA CONEXION
           CALL CierraOdbc
           EXITCANCEL
       ENDIF
       pSqlResp = ""
       DLLCALL_CDECL hODBCDLLCUP, sqlGetFirst(ref pSqlResp)
    ENDIF
	 
    IF pSqlResp = ""
       gPlataformaActiva = ""
    ELSE
       IF Len(Trim(pSqlResp)) > 0 Then
          SPLIT pSqlResp, ";", gPlataformaActiva,gMarca
          
       ENDIF
    ENDIF
	// CIERRA LA CONEXION
    CALL CierraOdbc
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                     SUBRUTINA        ConsultaCuponLigado                      //
// OBTIENE EL CUPON LIGADO                                                       //
// MODIFICACION ALSEA                                                            //
// FECHA MODIFICACION: 2017-05-26                                                //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
Sub ConsultaCuponLigado(var IDFolioCupon: A10, Var IDTicket: A12)
VAR pSqlString        : A500 = ""
    Var pSqlResp          : A500 = ""
	
    // OBTIENE VERSION DE MICROS
    CALL Version_Micros_Cup_SyBase
        
    // CONECTA A BASE DE DATOS
    CALL AbreConexion
    // CONJUNTO DE RUTINAS EN SP PARA CONFIGURACION DE PROMOCIONES

    FORMAT pSqlString as "CALL custom.SPObtFolioCuponLigado(paIDFolioCupon = '",IDFolioCupon,"', paIDTicket ='",IDTicket,"')"
    
    IF Version_Micros_Cup = 31
       DLLCALL hODBCDLLCUP, sqlGetRecordSet(pSqlString)
       pSqlResp = ""
       DLLCALL hODBCDLLCUP, sqlGetLastErrorString(ref pSqlResp)
       IF pSqlResp <> ""
          CALL Muestra_Error(pSqlResp)
		  // CIERRA LA CONEXION
          CALL CierraOdbc
          EXITCANCEL
       ENDIF
       pSqlResp = ""
       DLLCALL hODBCDLLCUP, sqlGetFirst(ref pSqlResp)
    ELSE
       DLLCALL_CDECL hODBCDLLCUP, sqlGetRecordSet(pSqlString)
       pSqlResp = ""
       DLLCALL_CDECL hODBCDLLCUP, sqlGetLastErrorString(ref pSqlResp)
       IF (pSqlResp <> "")
           CALL Muestra_Error(pSqlResp)
		   // CIERRA LA CONEXION
           CALL CierraOdbc
           EXITCANCEL
       ENDIF
       pSqlResp = ""
       DLLCALL_CDECL hODBCDLLCUP, sqlGetFirst(ref pSqlResp)
    ENDIF
    
    // VERIFICA QUE LA CONSULTA CONTENGA DATOS
    IF Len(Trim(pSqlResp)) > 1 Then
       SPLIT pSqlResp, ";", FCLeyendaCupon, FCFolioCuponLigadoID
    ENDIF
	
	// CIERRA LA CONEXION
    CALL CierraOdbc
EndSub 

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//              BUSCA TILL DEL EMPLEADO DE LA TERMINAL DE TRABAJO                //
// RUTINA PRINCIPAL TILL                                                         //
// MODIFICACION ALSEA                                                            //
// FECHA MODIFICACION: 2017-05-26                                                //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
SUB Main_Busca_Till_en_Terminal_Empleados
    Var Till_Asignado_Continua      : A1   = ""
    Var Till_Asignado_Terminal      : $12  = 0
    Var Till_Asignado_Terminal2     : $12  = 0
    Var Till_Asignado_Empleado      : A2   = ""
    CALL Busca_Till_en_Terminal
        
    IF Till_Asignado_Continua <> "T"
       CALL CierraOdbc
       ExitWithError "NO HAY TILL ASIGNADO A TERMINAL"
    ENDIF
        
    CALL Busca_Till_en_Empleados
        
    IF Till_Asignado_Continua <> "T"
       // CIERRA LA CONEXION
       CALL CierraOdbc
       ExitWithError "NO HAY TILL ASIGNADO A EMPLEADO"
    ENDIF
    
ENDSUB

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                        BUSCA TILL DE TRABAJO EN EMPEADOS                      //
// VERIFICA SI EL EMPLEADO ESTA ASIGNADO A UN TILL                               //
// MODIFICACION ALSEA                                                            //
// FECHA MODIFICACION: 2017-05-26                                                //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
SUB Busca_Till_en_Empleados
    Var SqlStr                      : A500 = ""
    Var SqlStr1                     : A500 = ""

    // OBTIENE VERSION DE MICROS
    CALL Version_Micros_Cup_SyBase
        
    // CONECTA A BASE DE DATOS
    CALL AbreConexion
    
	Till_Asignado_Continua = ""
    FORMAT SqlStr as "SELECT  ASG.employee_assigned "
    FORMAT SqlStr as SqlStr, "FROM    micros.cm_employee_receptacle_assignment_dtl ASG, micros.emp_def EMP "
    FORMAT SqlStr as SqlStr, "WHERE   ASG.employee_seq = EMP.emp_seq "
    FORMAT SqlStr as SqlStr, "AND     ASG.employee_assigned = 'T'    "
    FORMAT SqlStr as SqlStr, "AND     EMP.obj_num =  ", @TREMP, "    "
    FORMAT SqlStr as SqlStr, "AND   ( ASG.receptacle_seq = ", Till_Asignado_Terminal, " OR ASG.receptacle_seq = ", Till_Asignado_Terminal2, " ) "

    IF Version_Micros_Cup = 31
       DLLCALL hODBCDLLCUP, sqlGetRecordSet(SqlStr)
       SqlStr1 = ""
       DLLCALL hODBCDLLCUP, sqlGetLastErrorString(ref SqlStr1)
       IF (SqlStr1 <> "")
           CALL CierraOdbc
           ExitWithError "Error TILL_E: ", MID(SqlStr1, 1, 15)
       ENDIF
       SqlStr1 = ""
       DLLCALL hODBCDLLCUP, sqlGetFirst(ref SqlStr1)       
    ELSE
       DLLCALL_CDECL hODBCDLLCUP, sqlGetRecordSet(SqlStr)
       SqlStr1 = ""
       DLLCALL_CDECL hODBCDLLCUP, sqlGetLastErrorString(ref SqlStr1)
       IF (SqlStr1 <> "")
           CALL CierraOdbc
           ExitWithError "Error TILL_E: ", MID(SqlStr1, 1, 15)
       ENDIF
       SqlStr1 = ""
       DLLCALL_CDECL hODBCDLLCUP, sqlGetFirst(ref SqlStr1)
    ENDIF
        
    IF SqlStr1 = ""
       Till_Asignado_Continua = ""
       Till_Asignado_Empleado = 0
    ELSE
       IF Len(Trim(SqlStr1)) > 0 Then
          SPLIT SqlStr1, ";", Till_Asignado_Empleado
       ENDIF
    ENDIF
        
    IF Len(Trim(Till_Asignado_Empleado)) > 0  
       Till_Asignado_Continua = "T"
    ENDIF

	// CIERRA LA CONEXION
    CALL CierraOdbc
ENDSUB

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                          Version_Micros_Cup                                   //
// OBTIENE LA VERSION DE MICROS                                                  //
// MODIFICACION ALSEA                                                            //
// FECHA MODIFICACION: 2017-05-26                                                //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
Var Version_Micros_Cup    : N2   = 0
Var Micros_Ver_A_Cup      : A4   = ""
Sub Version_Micros_Cup_SyBase
    FORMAT Micros_Ver_A_Cup as MID(Trim(@version), 1,1)
    Version_Micros_Cup    = Micros_Ver_A_Cup
    IF Version_Micros_Cup = 4
       Version_Micros_Cup = 41
    ENDIF
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                         SubRutina ConsultaEmpleado                            //
// OBTIENE EL NOMBRE DEL EMPLEADO PARA CUPON EMPLEADO                            //
// MODIFICACION ALSEA                                                            //
// FECHA MODIFICACION: 2017-05-26                                                //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
Sub ConsultaNombreEmpleado(var IDFolioCupon: A12)
    Var pSqlString        : A500 = ""
    Var pSqlResp          : A500 = ""
    CALL Version_Micros_Cup_SyBase
    CALL AbreConexion

    FORMAT pSqlString as "SELECT FCLeyendaCupon FROM custom.TADatosCupon WHERE FIIDTicket=", @CKNUM,\
                      " AND FCIDFolioCupon='",IDFolioCupon,"' ORDER BY FDDOB DESC"
    
    IF Version_Micros_Cup = 31
       DLLCALL hODBCDLLCUP, sqlGetRecordSet(pSqlString)
       pSqlResp = ""
       DLLCALL hODBCDLLCUP, sqlGetLastErrorString(ref pSqlResp)
       IF (pSqlResp <> "")
           CALL Muestra_Error(pSqlResp)
		   // CIERRA LA CONEXION
           CALL CierraOdbc
           EXITCANCEL
       ENDIF
       pSqlResp = ""
       DLLCALL hODBCDLLCUP, sqlGetFirst(ref pSqlResp)   
    ELSE
       DLLCALL_CDECL hODBCDLLCUP, sqlGetRecordSet(pSqlString)
       pSqlResp = ""
       DLLCALL_CDECL hODBCDLLCUP, sqlGetLastErrorString(ref pSqlResp)
       IF (pSqlResp <> "")
           CALL Muestra_Error(pSqlResp)
		   // CIERRA LA CONEXION
           CALL CierraOdbc
           EXITCANCEL
       ENDIF
       pSqlResp = ""
       DLLCALL_CDECL hODBCDLLCUP, sqlGetFirst(ref pSqlResp)
    ENDIF
    
    IF pSqlResp = ""
       FCLeyendaCupon=""
    ELSE
       IF Len(Trim(pSqlResp)) > 0 Then
          SPLIT pSqlResp, ";",FCLeyendaCupon
       ENDIF
    ENDIF
	// CIERRA LA CONEXION
    CALL CierraOdbc
EndSub 

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       BUSCA TILL DE TRABAJO DE LA TERMINAL                    //
// RUTINA QUE BUSCA SI SE HA ASIGNADO UN TILL AL POS                             //
// MODIFICACION ALSEA                                                            //
// FECHA MODIFICACION: 2017-05-26                                                //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
Sub Busca_Till_en_Terminal
       Var pSqlString        : A500 = ""
    Var pSqlResp          : A500 = ""
    
    CALL Version_Micros_Cup_SyBase
    CALL AbreConexion

    FORMAT pSqlString as "SELECT ISNULL(DRW.cm_drawer_1_till_assigned,0) as cm_drawer_1_till_assigned, ISNULL(DRW.cm_drawer_2_till_assigned,0) as cm_drawer_2_till_assigned "
    FORMAT pSqlString as pSqlString, "FROM micros.uws_status DRW, micros.uws_def UWS "
    FORMAT pSqlString as pSqlString, "WHERE DRW.uws_seq = UWS.uws_seq "
    FORMAT pSqlString as pSqlString, "AND   UWS.obj_num = ", @WSID
    
    IF Version_Micros_Cup = 31
       DLLCALL hODBCDLLCUP, sqlGetRecordSet(pSqlString)
       pSqlResp = ""
       DLLCALL hODBCDLLCUP, sqlGetLastErrorString(ref pSqlResp)
       IF (pSqlResp <> "")
           CALL Muestra_Error(pSqlResp)
		   // CIERRA LA CONEXION
           CALL CierraOdbc
           EXITCANCEL
       ENDIF
       pSqlResp = ""
       DLLCALL hODBCDLLCUP, sqlGetFirst(ref pSqlResp)   
    ELSE
       DLLCALL_CDECL hODBCDLLCUP, sqlGetRecordSet(pSqlString)
       pSqlResp = ""
       DLLCALL_CDECL hODBCDLLCUP, sqlGetLastErrorString(ref pSqlResp)
       IF (pSqlResp <> "")
           CALL Muestra_Error(pSqlResp)
		   // CIERRA LA CONEXION
           CALL CierraOdbc
           EXITCANCEL
       ENDIF
       pSqlResp = ""
       DLLCALL_CDECL hODBCDLLCUP, sqlGetFirst(ref pSqlResp)
    ENDIF
        
    IF pSqlResp = ""
       Till_Asignado_Continua  = ""
       Till_Asignado_Terminal  = 0
       Till_Asignado_Terminal2 = 0
    ELSE
       IF Len(Trim(pSqlResp)) > 0 Then
          IF Len(Trim(pSqlResp)) > 0 Then
             SPLIT pSqlResp, ";", Till_Asignado_Terminal, Till_Asignado_Terminal2
          ENDIF
       ENDIF
    ENDIF
    
    IF Till_Asignado_Terminal > 0  OR Till_Asignado_Terminal2 > 0
       Till_Asignado_Continua = "T"
    ENDIF
	// CIERRA LA CONEXION
    CALL CierraOdbc 
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                      Subrutina VALIDA_CUPONES_ALSEA                           //
// FUNCION GENERICA PARA PARAMETRIZACION DE PROMOCIONES                          //
// MODIFICACION ALSEA                                                            //
// FECHA MODIFICACION: 2017-05-26                                                //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
Sub VALIDA_CUPONES_ALSEA(Var DFuncSQL: A30, Var IDFolioCupon: A10, Var ParamCons: A20, Var IDTicket: A12, Var EstacionTrabajo: A4)    
    Var pResp             : A500 = ""
    VAR pSqlString        : A500 = ""
    Var pSqlResp          : A500 = ""
    
    CUPONES_ALSEA_RESP = ""
    
    // OBTIENE VERSION DE MICROS
    CALL Version_Micros_Cup_SyBase
    
    // CONECTA A BASE DE DATOS
    CALL AbreConexion
    // CONJUNTO DE RUTINAS EN SP PARA CONFIGURACION DE PROMOCIONES
    IF DFuncSQL="FamInc"               // VERIFICA SI EXISTE LA FAMILIA QUE SE INCLUYE    
       FORMAT pSqlString as "CALL custom.SPCuponVerFamInc(paIDFolioCupon = '", IDFolioCupon,"' ,paFamInc = '",ParamCons,"')"    
    ELSEIF DFuncSQL="FamInc2x1"        // VERIFICA SI EXISTE LA FAMILIA QUE SE INCLUYE    
       FORMAT pSqlString as "CALL custom.SPCuponVerFamInc2x1(paIDFolioCupon = '", IDFolioCupon,"' ,paFamInc = '",ParamCons,"')"    
    ELSEIF DFuncSQL="FamExc"           // VERIFICA SI EXISTE LA FAMILIA QUE SE EXCLUYE
       FORMAT pSqlString as "CALL custom.SPCuponVerFamExc(paIDFolioCupon = '", IDFolioCupon,"' ,paFamExc = '",ParamCons,"')"    
    ELSEIF DFuncSQL="SKUInc"           // VERIFICA EL SKU QUE SE INCLUYE
       FORMAT pSqlString as "CALL custom.SPCuponVerSkuInc(paIDFolioCupon = '", IDFolioCupon,"' ,paSkuInc = '",ParamCons,"')"    
    ELSEIF DFuncSQL="SKUInc2x1"        // VERIFICA EL SKU QUE SE INCLUYE
       FORMAT pSqlString as "CALL custom.SPCuponVerSkuInc2x1(paIDFolioCupon = '", IDFolioCupon,"' ,paSkuInc = '",ParamCons,"')"    
    ELSEIF DFuncSQL="SKUExc"           // VERIFICA EL SKU QUE SE EXCLUYE
       FORMAT pSqlString as "CALL custom.SPCuponVerSkuExc(paIDFolioCupon = '", IDFolioCupon,"' ,paSkuExc = '",ParamCons,"')"
    ELSEIF DFuncSQL="verOrderType"     // VERIFICA SI EL TIPO DE ORDEN
       FORMAT pSqlString as "CALL custom.SPCuponVerOrderType(paIDFolioCupon = '", IDFolioCupon,"' ,paOdrTyp = '",ParamCons,"')"
    ELSEIF DFuncSQL="obtTipoCupConf"   // OBTIENE LA CONFIGURACION DEL TIPO DEL CUPON
       FORMAT pSqlString as "CALL custom.SPObtTipoCuponConfg(paFITipoDesc = ",ParamCons,")"
	ELSEIF DFuncSQL="verTarjetaCupon"  // VERIFICA SI EL CUPON TIENE UNA TARJETA LIGADA
       FORMAT pSqlString as "CALL custom.SPverTarjetaCupon(paIDFolioCupon = '", IDFolioCupon,"',paIDTicket = '", IDTicket,"'  ,paBin = ",ParamCons,")"
   ELSE    
       EXITCANCEL
    ENDIF
    
    IF Version_Micros_Cup = 31
       DLLCALL hODBCDLLCUP, sqlGetRecordSet(pSqlString)
       pSqlResp = ""
       DLLCALL hODBCDLLCUP, sqlGetLastErrorString(ref pSqlResp)
       IF pSqlResp <> ""
	      CALL DeleteCupon(IDFolioCupon,0)
          CALL Muestra_Error(pSqlResp)
		  // CIERRA LA CONEXION
          CALL CierraOdbc
          EXITCANCEL
       ENDIF
       pSqlResp = ""
       DLLCALL hODBCDLLCUP, sqlGetFirst(ref pSqlResp)
    ELSE
       DLLCALL_CDECL hODBCDLLCUP, sqlGetRecordSet(pSqlString)
       pSqlResp = ""
       DLLCALL_CDECL hODBCDLLCUP, sqlGetLastErrorString(ref pSqlResp)
       IF (pSqlResp <> "")
		  CALL DeleteCupon(IDFolioCupon,0)
          CALL Muestra_Error(pSqlResp)
		  // CIERRA LA CONEXION
          CALL CierraOdbc
          EXITCANCEL
       ENDIF
       pSqlResp = ""
       DLLCALL_CDECL hODBCDLLCUP, sqlGetFirst(ref pSqlResp)
    ENDIF
    
    // VERIFICA QUE LA CONSULTA CONTENGA DATOS
    IF Len(Trim(pSqlResp)) > 1 Then
          SPLIT pSqlResp, ";", pResp
          CUPONES_ALSEA_RESP = pResp          
    ENDIF    
    
    // CIERRA LA CONEXION
    CALL CierraOdbc
    
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                      Subrutina VALIDA_CUPONES_ALSEA                           //
// FUNCION GENERICA PARA PARAMETRIZACION DE PROMOCIONES                          //
// MODIFICACION ALSEA                                                            //
// FECHA MODIFICACION: 2017-05-26                                                //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
Sub obtTipoCupConf(Var ParamCons: A20)    
    Var pResp             : A500 = ""
    VAR pSqlString        : A500 = ""
    Var pSqlResp          : A500 = ""
    
    CUPONES_ALSEA_RESP = ""
    
    // OBTIENE VERSION DE MICROS
    CALL Version_Micros_Cup_SyBase
    
    // CONECTA A BASE DE DATOS
    CALL AbreConexion
    // OBTIENE LA CONFIGURACION DEL TIPO DEL CUPON
    FORMAT pSqlString as "CALL custom.SPObtTipoCuponConfg(paFITipoDesc = ",ParamCons,")"
	    
    IF Version_Micros_Cup = 31
       DLLCALL hODBCDLLCUP, sqlGetRecordSet(pSqlString)
       pSqlResp = ""
       DLLCALL hODBCDLLCUP, sqlGetLastErrorString(ref pSqlResp)
       IF pSqlResp <> ""
	      CALL DeleteCupon(IDFolioCupon,0)
          CALL Muestra_Error(pSqlResp)
		  // CIERRA LA CONEXION
          CALL CierraOdbc
          EXITCANCEL
       ENDIF
       pSqlResp = ""
       DLLCALL hODBCDLLCUP, sqlGetFirst(ref pSqlResp)
    ELSE
       DLLCALL_CDECL hODBCDLLCUP, sqlGetRecordSet(pSqlString)
       pSqlResp = ""
       DLLCALL_CDECL hODBCDLLCUP, sqlGetLastErrorString(ref pSqlResp)
       IF (pSqlResp <> "")
		  CALL DeleteCupon(IDFolioCupon,0)
          CALL Muestra_Error(pSqlResp)
		  // CIERRA LA CONEXION
          CALL CierraOdbc
          EXITCANCEL
       ENDIF
       pSqlResp = ""
       DLLCALL_CDECL hODBCDLLCUP, sqlGetFirst(ref pSqlResp)
    ENDIF
    
    // VERIFICA QUE LA CONSULTA CONTENGA DATOS
    IF Len(Trim(pSqlResp)) > 1 Then
          SPLIT pSqlResp, ";", pResp
          CUPONES_ALSEA_RESP = pResp          
    ENDIF    
    
    // CIERRA LA CONEXION
    CALL CierraOdbc
    
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                      Subrutina FPagoCuponInc                                  //
// FUNCION GENERICA PARA PARAMETRIZACION DE PROMOCIONES                          //
// MODIFICACION ALSEA                                                            //
// FECHA MODIFICACION: 2017-05-26                                                //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
Sub FPagoCuponInc(Var IDFolioCupon: A10, Var Fpago: A20, Var IDTicket: A12)    
    Var pResp             : A500 = ""
    VAR pSqlString        : A500 = ""
    Var pSqlResp          : A500 = ""
    
    CUPONES_ALSEA_RESP = ""
    
    // OBTIENE VERSION DE MICROS
    CALL Version_Micros_Cup_SyBase
    
    // CONECTA A BASE DE DATOS
    CALL AbreConexion
    // CONJUNTO DE RUTINAS EN SP PARA CONFIGURACION DE PROMOCIONES
    FORMAT pSqlString as "CALL custom.SPVerificaFPagoCupon(paIDFolioCupon = '",IDFolioCupon,"', paIDTicket ='",IDTicket,"',paFPago ='",Fpago,"')"   
	    
    IF Version_Micros_Cup = 31
       DLLCALL hODBCDLLCUP, sqlGetRecordSet(pSqlString)
       pSqlResp = ""
       DLLCALL hODBCDLLCUP, sqlGetLastErrorString(ref pSqlResp)
       IF pSqlResp <> ""
	      CALL DeleteCupon(IDFolioCupon,0)
          CALL Muestra_Error(pSqlResp)
		  // CIERRA LA CONEXION
          CALL CierraOdbc
          EXITCANCEL
       ENDIF
       pSqlResp = ""
       DLLCALL hODBCDLLCUP, sqlGetFirst(ref pSqlResp)
    ELSE
       DLLCALL_CDECL hODBCDLLCUP, sqlGetRecordSet(pSqlString)
       pSqlResp = ""
       DLLCALL_CDECL hODBCDLLCUP, sqlGetLastErrorString(ref pSqlResp)
       IF (pSqlResp <> "")
		  CALL DeleteCupon(IDFolioCupon,0)
          CALL Muestra_Error(pSqlResp)
		  // CIERRA LA CONEXION
          CALL CierraOdbc
          EXITCANCEL
       ENDIF
       pSqlResp = ""
       DLLCALL_CDECL hODBCDLLCUP, sqlGetFirst(ref pSqlResp)
    ENDIF
    
    // VERIFICA QUE LA CONSULTA CONTENGA DATOS
    IF Len(Trim(pSqlResp)) > 1 Then
       SPLIT pSqlResp, ";", pResp
       CUPONES_ALSEA_RESP = pResp          
    ENDIF    
    
    // CIERRA LA CONEXION
    CALL CierraOdbc
    
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                      Subrutina ObtFolioCupon                                  //
// FUNCION GENERICA PARA PARAMETRIZACION DE PROMOCIONES                          //
// MODIFICACION ALSEA                                                            //
// FECHA MODIFICACION: 2017-05-26                                                //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
Sub ObtFolioCupon(Var IDTicket: A12)    
    Var pResp             : A500 = ""
    VAR pSqlString        : A500 = ""
    Var pSqlResp          : A500 = ""
    Var EstacionTrabajo   : A4   = ""
	
	EstacionTrabajo = @WSID
    CUPONES_ALSEA_RESP = ""
    
    // OBTIENE VERSION DE MICROS
    CALL Version_Micros_Cup_SyBase
    
    // CONECTA A BASE DE DATOS
    CALL AbreConexion
    
	FORMAT pSqlString as "CALL custom.SPObtFolioCupon(paFIIDTicket = '", IDTicket,"')"
	 
    IF Version_Micros_Cup = 31
       DLLCALL hODBCDLLCUP, sqlGetRecordSet(pSqlString)
       pSqlResp = ""
       DLLCALL hODBCDLLCUP, sqlGetLastErrorString(ref pSqlResp)
       IF pSqlResp <> ""
          CALL Muestra_Error(pSqlResp)
		  // CIERRA LA CONEXION
          CALL CierraOdbc
          EXITCANCEL
       ENDIF
       pSqlResp = ""
       DLLCALL hODBCDLLCUP, sqlGetFirst(ref pSqlResp)
    ELSE
       DLLCALL_CDECL hODBCDLLCUP, sqlGetRecordSet(pSqlString)
       pSqlResp = ""
       DLLCALL_CDECL hODBCDLLCUP, sqlGetLastErrorString(ref pSqlResp)
       IF (pSqlResp <> "")
           CALL Muestra_Error(pSqlResp)
		   // CIERRA LA CONEXION
           CALL CierraOdbc
           EXITCANCEL
       ENDIF
       pSqlResp = ""
       DLLCALL_CDECL hODBCDLLCUP, sqlGetFirst(ref pSqlResp)
    ENDIF
    
    // VERIFICA QUE LA CONSULTA CONTENGA DATOS
    IF Len(Trim(pSqlResp)) > 1 Then
          SPLIT pSqlResp, ";", pResp
          CUPONES_ALSEA_RESP = pResp          
    ENDIF    
    
    // CIERRA LA CONEXION
    CALL CierraOdbc
    
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                      Subrutina ConsultaCupon                                  //
// CONSULTA LOS DATOS DEL CUPON POR APLICAR O APLICADO                           //
// MODIFICACION ALSEA                                                            //
// FECHA MODIFICACION: 2017-05-26                                                //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
Sub ConsultaCupon(var IDFolioCupon : A10, var IDTicket: A12, var EstacionTrabajo:N4)    
    VAR pSqlString        : A500 = ""
    Var pSqlResp          : A500 = ""
	
    // OBTIENE VERSION DE MICROS
    CALL Version_Micros_Cup_SyBase
        
    // CONECTA A BASE DE DATOS
    CALL AbreConexion
    // CONJUNTO DE RUTINAS EN SP PARA CONFIGURACION DE PROMOCIONES

    FORMAT pSqlString as "CALL custom.SPConsultaCupon(paIDFolioCupon = '",IDFolioCupon,"', paIDTicket = '",IDTicket,"', paEstacionTrabajo = '", EstacionTrabajo,"')"
    
    IF Version_Micros_Cup = 31
       DLLCALL hODBCDLLCUP, sqlGetRecordSet(pSqlString)
       pSqlResp = ""
       DLLCALL hODBCDLLCUP, sqlGetLastErrorString(ref pSqlResp)
       IF pSqlResp <> ""
          CALL Muestra_Error(pSqlResp)
		  // CIERRA LA CONEXION
          CALL CierraOdbc
          EXITCANCEL
       ENDIF
       pSqlResp = ""
       DLLCALL hODBCDLLCUP, sqlGetFirst(ref pSqlResp)
    ELSE
       DLLCALL_CDECL hODBCDLLCUP, sqlGetRecordSet(pSqlString)
       pSqlResp = ""
       DLLCALL_CDECL hODBCDLLCUP, sqlGetLastErrorString(ref pSqlResp)
       IF (pSqlResp <> "")
           CALL Muestra_Error(pSqlResp)
		   // CIERRA LA CONEXION
           CALL CierraOdbc
           EXITCANCEL
       ENDIF
       pSqlResp = ""
       DLLCALL_CDECL hODBCDLLCUP, sqlGetFirst(ref pSqlResp)
    ENDIF
    
    // VERIFICA QUE LA CONSULTA CONTENGA DATOS
    IF Len(Trim(pSqlResp)) > 1 Then
       SPLIT pSqlResp, ";", FIIDTicket, FCEstacionTrabajo ,FCIDFolioCupon ,FIIDTienda \
                         ,FIIDMarca, FIIDCodigoEstatus, FCDescripcionEstatus, FIIDCampana \
                         ,FIIDTipoPromocion, FCValorCupon, FNMontoLigado, FIContieneCuponLigado \
                         ,FITipoCuponID, FIGeneralParcial, FIAplicaWOW, FIAplicaMSR, FIFormaPagLigada \
                         ,FISolicitaTarjeta, FITarjetaUlt4Num, FISolicitaEmail, FNMontoMax \
                         ,FCNombreEmpleado, FCFolioCuponLigadoID, FCLeyendaCupon, FIIDColaborador \
                         ,FIMontoTotalVenta, FCEmailCliente ,FNFormaPagoMarca
       CuponExiste = "T"
    ELSE
       CuponExiste = "F"
    ENDIF

    // CIERRA LA CONEXION
    CALL CierraOdbc
    
EndSub


//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                      Subrutina ConsultaCuponesExistentes                      //
// CONSULTA LOS DATOS DEL CUPON POR APLICAR O APLICADO                           //
// MODIFICACION ALSEA                                                            //
// FECHA MODIFICACION: 2017-05-26                                                //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
Sub ConsultaCuponesExistentes(var IDTicket: A12)    
    VAR pSqlString        : A500 = ""
    Var pSqlResp          : A500 = "" 
	
    // OBTIENE VERSION DE MICROS
    CALL Version_Micros_Cup_SyBase
        
    // CONECTA A BASE DE DATOS
    CALL AbreConexion
    // CONJUNTO DE RUTINAS EN SP PARA CONFIGURACION DE PROMOCIONES

    FORMAT pSqlString as "CALL custom.SPConsultaCuponesExistentes(paIDTicket = '",IDTicket,"')"
    
    IF Version_Micros_Cup = 31
       DLLCALL hODBCDLLCUP, sqlGetRecordSet(pSqlString)
       pSqlResp = ""
       DLLCALL hODBCDLLCUP, sqlGetLastErrorString(ref pSqlResp)
       IF pSqlResp <> ""
          CALL Muestra_Error(pSqlResp)
		  // CIERRA LA CONEXION
          CALL CierraOdbc
          EXITCANCEL
       ENDIF
       pSqlResp = ""
       DLLCALL hODBCDLLCUP, sqlGetFirst(ref pSqlResp)
    ELSE
       DLLCALL_CDECL hODBCDLLCUP, sqlGetRecordSet(pSqlString)
       pSqlResp = ""
       DLLCALL_CDECL hODBCDLLCUP, sqlGetLastErrorString(ref pSqlResp)
       IF (pSqlResp <> "")
           CALL Muestra_Error(pSqlResp)
		   // CIERRA LA CONEXION
           CALL CierraOdbc
           EXITCANCEL
       ENDIF
       pSqlResp = ""
       DLLCALL_CDECL hODBCDLLCUP, sqlGetFirst(ref pSqlResp)
    ENDIF
	
	// VERIFICA QUE LA CONSULTA CONTENGA DATOS
    WHILE  pSqlResp <> ""
	      ContCupon = ContCupon + 1
		  
          SPLIT pSqlResp, ";", ArrayCupones[ContCupon]						 
		  
		  pSqlResp=""
		  
		  IF Version_Micros_Cup = 31
             DLLCALL hODBCDLLCUP,sqlGetNext(ref pSqlResp)
		  ELSE
		     DLLCALL_CDECL hODBCDLLCUP,sqlGetNext(ref pSqlResp)
		  ENDIF
    ENDWHILE

    // CIERRA LA CONEXION
    CALL CierraOdbc
    
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                      Subrutina VerIFicaCuponExist                             //
// CONSULTA SI EL CUPON YA HA SIDO APLICADO EN OTRO POS                          //
// MODIFICACION ALSEA                                                            //
// FECHA MODIFICACION: 2017-05-26                                                //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
Sub VerIFicaCuponExist(var IDFolioCupon : A10, var EstacionTrabajo :N4)    
    VAR pSqlString        : A500 = ""
    Var pSqlResp          : A500 = ""
    
    CUPONES_ALSEA_RESP = ""    
    
    // OBTIENE VERSION DE MICROS
    CALL Version_Micros_Cup_SyBase
        
    // CONECTA A BASE DE DATOS
    CALL AbreConexion
    // CONJUNTO DE RUTINAS EN SP PARA CONFIGURACION DE PROMOCIONES
    
    FORMAT pSqlString as "CALL custom.SPVerCuponExist(paIDFolioCupon = '",IDFolioCupon,"', paEstacionTrabajo = '",EstacionTrabajo,"')"

    IF Version_Micros_Cup = 31
       DLLCALL hODBCDLLCUP, sqlGetRecordSet(pSqlString)
       pSqlResp = ""
       DLLCALL hODBCDLLCUP, sqlGetLastErrorString(ref pSqlResp)
       IF pSqlResp <> ""
          CALL Muestra_Error(pSqlResp)
		  // CIERRA LA CONEXION
          CALL CierraOdbc
          EXITCANCEL
       ENDIF
       pSqlResp = ""
       DLLCALL hODBCDLLCUP, sqlGetFirst(ref pSqlResp)
    ELSE
       DLLCALL_CDECL hODBCDLLCUP, sqlGetRecordSet(pSqlString)
       pSqlResp = ""
       DLLCALL_CDECL hODBCDLLCUP, sqlGetLastErrorString(ref pSqlResp)
       IF (pSqlResp <> "")
           CALL Muestra_Error(pSqlResp)
		   // CIERRA LA CONEXION
           CALL CierraOdbc
           EXITCANCEL
       ENDIF
       pSqlResp = ""
       DLLCALL_CDECL hODBCDLLCUP, sqlGetFirst(ref pSqlResp)
    ENDIF
    
    // VERIFICA QUE LA CONSULTA CONTENGA DATOS
    IF Len(Trim(pSqlResp)) > 1 Then
       SPLIT pSqlResp, ";", FITipoCuponID
	   CuponExisteOtroPOS = "T"
    ELSE
       CuponExisteOtroPOS = "F"    
    ENDIF    
    
    // CIERRA LA CONEXION
    CALL CierraOdbc
    
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                      Subrutina VerIFicaCuponEMPExist                          //
// CONSULTA SI EL CUPON EMPLEADO YA HA SIDO APLICADO EN OTRO POS                 //
// MODIFICACION ALSEA                                                            //
// FECHA MODIFICACION: 2017-05-26                                                //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
Sub VerIFicaCuponEMPExist(var IDFolioCupon : A10, var EstacionTrabajo :N4)    
    VAR pSqlString        : A500 = ""
    Var pSqlResp          : A500 = ""
    
    CUPONES_ALSEA_RESP = ""    
    
    // OBTIENE VERSION DE MICROS
    CALL Version_Micros_Cup_SyBase
        
    // CONECTA A BASE DE DATOS
    CALL AbreConexion
    // CONJUNTO DE RUTINAS EN SP PARA CONFIGURACION DE PROMOCIONES
    
    FORMAT pSqlString as "CALL custom.SPVerCuponEMPExist(paIDFolioCupon = '",IDFolioCupon,"', paEstacionTrabajo = '",EstacionTrabajo,"')"

    IF Version_Micros_Cup = 31
       DLLCALL hODBCDLLCUP, sqlGetRecordSet(pSqlString)
       pSqlResp = ""
       DLLCALL hODBCDLLCUP, sqlGetLastErrorString(ref pSqlResp)
       IF pSqlResp <> ""
          CALL Muestra_Error(pSqlResp)
		  // CIERRA LA CONEXION
          CALL CierraOdbc
          EXITCANCEL
       ENDIF
       pSqlResp = ""
       DLLCALL hODBCDLLCUP, sqlGetFirst(ref pSqlResp)
    ELSE
       DLLCALL_CDECL hODBCDLLCUP, sqlGetRecordSet(pSqlString)
       pSqlResp = ""
       DLLCALL_CDECL hODBCDLLCUP, sqlGetLastErrorString(ref pSqlResp)
       IF (pSqlResp <> "")
           CALL Muestra_Error(pSqlResp)
		   // CIERRA LA CONEXION
           CALL CierraOdbc
           EXITCANCEL
       ENDIF
       pSqlResp = ""
       DLLCALL_CDECL hODBCDLLCUP, sqlGetFirst(ref pSqlResp)
    ENDIF
    
    // VERIFICA QUE LA CONSULTA CONTENGA DATOS
    IF Len(Trim(pSqlResp)) > 1 Then
	   CuponExisteEMPOtroPOS = "T"
    ELSE
       CuponExisteEMPOtroPOS = "F"    
    ENDIF    
    
    // CIERRA LA CONEXION
    CALL CierraOdbc
    
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                      SUBRUTINA Calcula_Numero_Ticket                          //
// RUTINA QUE CALCULA EL NUMERO DEL TICKET                                       //
// MODIFICACION ALSEA                                                            //
// FECHA MODIFICACION: 2017-05-26                                                //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
SUB Calcula_Numero_Ticket
    Prompt "CALCULANDO NUMERO DE TICKET"
    FORMAT FIIDTicket as @WSID{01}, @CKNUM{04}, MID(@Chk_Open_Time, 10, 2) , MID(@Chk_Open_Time, 13, 2)
ENDSUB

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                       SUBRUTINA VerExistCuponAplicado                         //
// VERIFICA SI UN CUPON ESTA REALMENTE APLICADO SI NO ES ASI LO BORRA            //
// MODIFICACION ALSEA                                                            //
// FECHA MODIFICACION: 2017-05-26                                                //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
SUB VerExistCuponAplicado(Var IDFolioCupon : A10,var IDTipoPromocion:N8,var IDTicket: A12,var EstacionTrabajo: A4)
   Var I                   : N4    = 0
   VAR TipoPromocionExist  : N10
   ExistCuponAplica = ""
   
   //OBTIENE EL NUMERO DEL DESCUENTO
   CALL obtTipoCupConf(IDTipoPromocion)
   TipoPromocionExist = CUPONES_ALSEA_RESP
   
   IF IDTipoPromocion = 1 OR IDTipoPromocion = 3 OR IDTipoPromocion = 5 OR IDTipoPromocion = 6
      FOR I = 1 TO @NumDTLT
          IF @DTL_TYPE[I] = "D" AND @DTL_IS_VOID[I] = 0
             IF TipoPromocionExist = @DTL_OBJECT[I]
                ExistCuponAplica = "T"
                BREAK
             ELSE
                ExistCuponAplica = "F"
             ENDIF              
          ENDIF
      ENDFOR
      IF ExistCuponAplica = ""
         ExistCuponAplica = "F"
      ENDIF
   ELSEIF IDTipoPromocion = 2
      FOR I = 1 TO @NumDTLT 
          IF @DTL_TYPE[I] = "M" AND @DTL_IS_VOID[I] = 0 
             IF FCValorCupon = @DTL_OBJECT[I]
                ExistCuponAplica = "T"
                BREAK
             ELSE
                ExistCuponAplica = "F"
             ENDIF
          ENDIF
	  ENDFOR
      IF ExistCuponAplica = ""
		 ExistCuponAplica = "F"
	  ENDIF	  
   ELSEIF IDTipoPromocion = 4
      IF FNFormaPagoMarca > 0
         TipoPromocionExist = FNFormaPagoMarca
	  ELSE
	     //OBTIENE EL NUMERO DE LA FORMA DE PAGO
         CALL obtTipoCupConf(4)
	     TipoPromocionExist = CUPONES_ALSEA_RESP
	  ENDIF
      FOR I = 1 TO @NumDTLT        
          IF @DTL_TYPE[I] = "T" AND @DTL_IS_VOID[I] = 0
             IF TipoPromocionExist = @DTL_OBJECT[I]
                ExistCuponAplica = "T"
                BREAK
             ELSE
                ExistCuponAplica = "F"
             ENDIF
          ENDIF
      ENDFOR
      IF @TMDNUM = TipoPromocionExist
         ExistCuponAplica = "T"
      ENDIF
      IF ExistCuponAplica = ""
         ExistCuponAplica = "F"
      ENDIF
   ENDIF
   
   IF FCValorCupon = 0 AND FITipoCuponID = 3 OR FIIDTipoPromocion <> 6
      ExistCuponAplica = "T"
   ENDIF

   IF ExistCuponAplica = "F"
   	  CALL DeleteCupon(IDFolioCupon,1)
   ENDIF
ENDSUB

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                        Subrutina VerOrdenAplica                               //
// VERIFICA SI EL CUPON APLICA PARA EL TIPO DE ORDEN                             //
// MODIFICACION ALSEA                                                            //
// FECHA MODIFICACION: 2017-05-26                                                //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
Sub VerOrdenAplica(Var IDFolioCupon: A10, Var OrderType:N3,Var IDTicket: A12, Var EstacionTrabajo: A4)
    Var CG_MSG_CUPON_ORDERTYPE : A80 = "EL CUPON NO APLICA PARA ESTE TIPO DE ORDEN"
    Var DFuncSQL               : A30 = "verOrderType"

    // VERIFICA EL TIPO DE ORDEN
    CALL VALIDA_CUPONES_ALSEA(DFuncSQL, IDFolioCupon, OrderType, IDTicket, EstacionTrabajo) 

	IF CUPONES_ALSEA_RESP = "0"
       CALL DeleteCupon(IDFolioCupon,0)
       INFOMESSAGE "Error",CG_MSG_CUPON_ORDERTYPE
       EXITCANCEL
	ENDIF
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                        Subrutina VerificaVntaCupon                            //
// VERIFICA SI EXISTEN RESTRICCIONES AL MOMENTO DE REALIZAR UN PAGO              //
// MODIFICACION ALSEA                                                            //
// FECHA MODIFICACION: 2017-05-26                                                //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
Sub VerificaVntaCupon(Var vTMED : A10)
//VARIABLES PARA TRANSACCIONES
var FIIDTicket                  : A12  = ""
var FCEstacionTrabajo           : A10  = ""
Var FCIDFolioCupon              : A10  = ""
var FIIDTienda                  : N8   = 0    
var FIIDMarca                   : N8   = 0    
var FIIDCodigoEstatus           : N8   = 0    
var FCDescripcionEstatus        : A500 = ""        
var FIIDCampana                 : N8   = 0    
var FIIDTipoPromocion           : N8   = 0    
var FCValorCupon                : A10  = ""    
var FNMontoLigado               : $12  = 0 
var FIContieneCuponLigado       : N8   = 0    
var FITipoCuponID               : N8   = 0    
var FIGeneralParcial            : N8   = 0    
var FIAplicaWOW                 : N8   = 0    
var FIAplicaMSR                 : N8   = 0        
var FIFormaPagLigada            : A8   = ""       
var FISolicitaTarjeta           : N8   = 0    
var FITarjetaUlt4Num            : A16  = ""    
var FISolicitaEmail             : N8   = 0    
var FNMontoMax                  : $12  = 0     
var FCNombreEmpleado            : A50  = ""    
var FCFolioCuponLigadoID        : A10  = ""    
var FCLeyendaCupon              : A150 = ""
var FIIDColaborador             : A150 = ""
var FIMontoTotalVenta           : $12  = 0 
var FCEmailCliente              : A150 = ""
var FITiposOrdenAplican         : N3   = 0
var FNFormaPagoMarca            : N10  = 0

//VARIABLE PARA VALIDACIONES CUPONES
Var CUPONES_ALSEA_RESP          : A200 = ""
var DFuncSQL                    : A30  = ""
Var IDFolioCupon                : A10  = ""
var ParamCons                   : A20  = 0
Var IDTicket                    : A12  = ""
Var EstacionTrabajo             : A4   = ""
Var MuestraMsg                  : A1   = "0"
Var CuponExisteOtroPOS          : A1   = "F"
Var CuponExiste                 : A1   = "F"
Var ExistCuponAplica            : A1   = ""

    //OBTIENE NUMERO DE ESTACION DE TRABAJO
    EstacionTrabajo = @WSID
	
    //OBTIENE NUMERO DE TICKET
	CALL Calcula_Numero_Ticket
	IDTicket = FIIDTicket
	
    //FUNCION PARA REALIZAR LA CAPTURA DEL FOLIO DEL CUPON
    CALL ObtFolioCupon(FIIDTicket)
    IDFolioCupon     = CUPONES_ALSEA_RESP
	
	//VERIFICA SI EL CUPON YA HA SIDO APLICADO EN ESTE POS
    CALL ConsultaCupon(IDFolioCupon, IDTicket, EstacionTrabajo) 
	
    IF CuponExiste = "T"       
       CALL VerExistCuponAplicado(IDFolioCupon, FIIDTipoPromocion, IDTicket, EstacionTrabajo)
    ELSE
	   AplicaMSR = 1
	   AplicaWOW = 1
    ENDIF

	IF ExistCuponAplica = "T" 
	   AplicaMSR = FIAplicaMSR
	   AplicaWOW = FIAplicaWOW
	   
	   IF FIIDCodigoEstatus = 1
	      IF FIFormaPagLigada = "T"
             CALL VerificaFPagoCupon(IDFolioCupon,IDTicket,EstacionTrabajo)
	      ENDIF

          IF (vTMED = "PAGOS" OR vTMED = "WOW" OR vTMED = "MSR" ) AND FIIDTipoPromocion = 6
             IF (@TMDNUM >= 20 AND @TMDNUM <= 22) OR @TMDNUM = 61 OR @TMDNUM = 62
             ELSEIF vTMED = "MSR" AND AplicaMSR = 1
	         ELSE
			    INFOMESSAGE "TDC/TDD EMPLEADO","NO SE PERMITE ESTA FORMA DE PAGO"
                ExitCancel
             ENDIF			 
	      ENDIF

	      IF vTMED = "WOW" AND AplicaWOW = 0
	         INFOMESSAGE "WOW","NO SE PERMITE ESTA FORMA DE PAGO"
             ExitCancel 
	      ENDIF
	      
          IF vTMED = "MSR" AND AplicaMSR = 0
	         INFOMESSAGE "MSR","NO SE PERMITE ESTA FORMA DE PAGO"
             ExitCancel 
	      ENDIF
		  
		  IF vTMED <> "PINPAD"
	     //    CALL ActualizarStatusPOS(3,IDFolioCupon,IDTicket)
	      ENDIF
	   ENDIF
	ELSE
	   AplicaMSR = 1
	   AplicaWOW = 1
	ENDIF
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                 Subrutina VerificaExistePrevCuponAplicado                     //
// VERIFICA SI EXISTE UN CUPON PREVIAMENTE APLICADO Y SU TIPO                    //
// MODIFICACION ALSEA                                                            //
// FECHA MODIFICACION: 2017-05-26                                                //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
sub VerificaExistePrevCuponAplicado
    var I                              : N4 = 0
	var ContCupon                      : N4 = 0
    var ArrayCupones[100]              : A10
	var ArrayIDTipoPromocion[100]      : A4
	
    //OBTIENE NUMERO DE ESTACION DE TRABAJO
    EstacionTrabajo = @WSID
	
    //OBTIENE NUMERO DE TICKET
	CALL Calcula_Numero_Ticket
    IDTicket = FIIDTicket
	
	//VERIFICA SI EL CUPON YA HA SIDO APLICADO EN ESTE POS
    CALL ConsultaCuponesExistentes(IDTicket) 
	
	FOR I = 1 TO ContCupon                                                                                      
	    IF ArrayCupones[I] <> ""
		   IDFolioCupon = ArrayCupones[I]
		   //VERIFICA SI EL CUPON YA HA SIDO APLICADO EN ESTE POS
           CALL ConsultaCupon(IDFolioCupon, IDTicket, EstacionTrabajo) 
		   
		   IF CuponExiste = "T"       
              CALL VerExistCuponAplicado(IDFolioCupon, FIIDTipoPromocion, IDTicket, EstacionTrabajo)
           ENDIF
	        
	       IF ExistCuponAplica = "T" 
	          FIIDTipoPromocionPrevio   = FIIDTipoPromocion			   
              IDFolioCuponPrevio        = IDFolioCupon
              ExistCuponPrevioAplica    = ExistCuponAplica
	       ELSE
		      CALL DeleteCupon(IDFolioCupon,0)
		   ENDIF
		   
		ENDIF   
	ENDFOR	
	
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                      Subrutina VerificaDescAplicado                           //
// Vefirica si ahi resticciones con formas de pago durante una promocion         //
// MODIFICACION ALSEA                                                            //
// FECHA MODIFICACION: 2017-05-26                                                //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
Sub VerificaFPagoCupon(Var IDFolioCupon: A10,Var IDTicket: A12)
Var I                        : N4
Var Fpago                    : N10
    Fpago   = @TMDNUM 
    CALL FPagoCuponInc(IDFolioCupon, Fpago, IDTicket) 
    IF CUPONES_ALSEA_RESP = "1"
	
    ELSE
       ExitWithError "NO SE PERMITE ESTA FORMA DE PAGO"
       ExitCancel 
    ENDIF
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                      Subrutina ConsulTipoCupon                                //
// CONSULTA SI EL DESCUENTO ES UN CUPON                                          //
// MODIFICACION ALSEA                                                            //
// FECHA MODIFICACION: 2017-05-26                                                //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
Sub ConsulTipoCupon(var ObjDTLCupon : A10) 
   Var pResp             : A500 = ""
   VAR pSqlString        : A500 = ""
   Var pSqlResp          : A500 = ""
    
    CUPONES_ALSEA_RESP = ""    
    
    // OBTIENE VERSION DE MICROS
    CALL Version_Micros_Cup_SyBase
        
    // CONECTA A BASE DE DATOS
    CALL AbreConexion
	
    // CONJUNTO DE RUTINAS EN SP PARA CONFIGURACION DE PROMOCIONES
    FORMAT pSqlString as "CALL custom.SPConsulTipoCuponDesc(paObjDTLCupon = ",ObjDTLCupon,")"

    IF Version_Micros_Cup = 31
       DLLCALL hODBCDLLCUP, sqlGetRecordSet(pSqlString)
       pSqlResp = ""
       DLLCALL hODBCDLLCUP, sqlGetLastErrorString(ref pSqlResp)
       IF pSqlResp <> ""
          CALL Muestra_Error(pSqlResp)
		  // CIERRA LA CONEXION
          CALL CierraOdbc
          EXITCANCEL
       ENDIF
       pSqlResp = ""
       DLLCALL hODBCDLLCUP, sqlGetFirst(ref pSqlResp)
    ELSE
       DLLCALL_CDECL hODBCDLLCUP, sqlGetRecordSet(pSqlString)
       pSqlResp = ""
       DLLCALL_CDECL hODBCDLLCUP, sqlGetLastErrorString(ref pSqlResp)
       IF (pSqlResp <> "")
		  CALL DeleteCupon(IDFolioCupon,0)
          CALL Muestra_Error(pSqlResp)
		  // CIERRA LA CONEXION
          CALL CierraOdbc
          EXITCANCEL
       ENDIF
       pSqlResp = ""
       DLLCALL_CDECL hODBCDLLCUP, sqlGetFirst(ref pSqlResp)
    ENDIF
    
    // VERIFICA QUE LA CONSULTA CONTENGA DATOS
    IF Len(Trim(pSqlResp)) > 1 Then
          SPLIT pSqlResp, ";", pResp
          CUPONES_ALSEA_RESP = pResp          
    ENDIF      
    
    // CIERRA LA CONEXION
    CALL CierraOdbc
    
EndSub 

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                      Subrutina ProcesaCupones                                 //
// REDIME EL CUPON Y CONSULTA SI EXISTE UN CUPON LIGADO PARA SU IMPRESION        //
// MODIFICACION ALSEA                                                            //
// FECHA MODIFICACION: 2017-05-26                                                //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
Sub ProcesaCupones(Var IDTicket : A12)
    Var pResp               : A500 = ""
    VAR pSqlString          : A500 = ""
    Var pSqlResp            : A500 = ""
    var I                   : N1   = 0
	var pContadorCupon      : N1   = 0
	var pCupon[100] : A10	    
        
    // OBTIENE VERSION DE MICROS
    CALL Version_Micros_Cup_SyBase
        
    // CONECTA A BASE DE DATOS
    CALL AbreConexion
    
    FORMAT pSqlString as "CALL custom.SPObtFolioCupon(paFIIDTicket = '", IDTicket,"')"
	
	IF Version_Micros_Cup = 31
       DLLCALL hODBCDLLCUP, sqlGetRecordSet(pSqlString)
       pSqlResp = ""
       DLLCALL hODBCDLLCUP, sqlGetLastErrorString(ref pSqlResp)
       IF pSqlResp <> ""
          CALL Muestra_Error(pSqlResp)
		  // CIERRA LA CONEXION
          CALL CierraOdbc
          EXITCANCEL
       ENDIF
       pSqlResp = ""
       DLLCALL hODBCDLLCUP, sqlGetFirst(ref pSqlResp)
    ELSE
       DLLCALL_CDECL hODBCDLLCUP, sqlGetRecordSet(pSqlString)
       pSqlResp = ""
       DLLCALL_CDECL hODBCDLLCUP, sqlGetLastErrorString(ref pSqlResp)
       IF (pSqlResp <> "")
          CALL Muestra_Error(pSqlResp)
		  // CIERRA LA CONEXION
          CALL CierraOdbc
          EXITCANCEL
       ENDIF
       pSqlResp = ""
       DLLCALL_CDECL hODBCDLLCUP, sqlGetFirst(ref pSqlResp)
    ENDIF

	// VERIFICA QUE LA CONSULTA CONTENGA DATOS
    WHILE pSqlResp <> ""
	      pContadorCupon = pContadorCupon + 1
		  
          SPLIT pSqlResp, ";", pCupon[pContadorCupon]		  
		  
		  pSqlResp = ""
		  
          IF Version_Micros_Cup = 31
             DLLCALL hODBCDLLCUP,sqlGetNext(ref pSqlResp)
		  ELSE
		     DLLCALL_CDECL hODBCDLLCUP,sqlGetNext(ref pSqlResp)
		  ENDIF
    ENDWHILE
	
	// CIERRA LA CONEXION
    CALL CierraOdbc
	
	FOR I=1 TO pContadorCupon
	    IF pCupon[I] <> ""
           CALL RedimeCupon(pCupon[I],IDTicket)		   
	       CALL ImprimeCuponLigado(pCupon[I],IDTicket)
		ENDIF   
	ENDFOR	
EndSub

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                      Subrutina ActualizarDatosCupon                           //
// RUTINA QUE ACTUALIZA EL ESTADO DEL CUPON EN LA BASE DE DATOS LOCAL            //
// MODIFICACION ALSEA                                                            //
// FECHA MODIFICACION: 2017-05-26                                                //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
Sub ActualizarDatosCupon(Var IDTicket : A12, var NombreEmpleado: A50,var EstacionTrabajo :N4, var IDColaborador :N10)
    Var pSqlString        : A1000 = ""
    Var pSqlResp          : A1000 = ""
    Var MontoTotalVenta   : $12   = 0
	
	MontoTotalVenta = (@Prevpay - @SVC)
	FORMAT pSqlString as "CALL custom.SPupdCuponRedime(paIDTicket = '",IDTicket,"', paNombreEmpleado = '",MID(NombreEmpleado,1,50),"', paEstacionTrabajo = ",EstacionTrabajo,", paMontoTotalVenta = ",MontoTotalVenta,", paIDColaborador = ",IDColaborador,")" 

	// OBTIENE VERSION DE MICROS
    CALL Version_Micros_Cup_SyBase

    // CONECTA A BASE DE DATOS
    CALL AbreConexion

	 IF Version_Micros_Cup = 31
       DLLCALL hODBCDLLCUP, sqlExecuteQuery(pSqlString)
       pSqlResp = ""
       DLLCALL hODBCDLLCUP, sqlGetLastErrorString(ref pSqlResp)
       IF pSqlResp <> ""
          CALL Muestra_Error(pSqlResp)
          CALL CierraOdbc          
       ENDIF
    ELSE
       DLLCALL_CDECL hODBCDLLCUP, sqlExecuteQuery(pSqlString)
       pSqlResp = ""
       DLLCALL_CDECL hODBCDLLCUP, sqlGetLastErrorString(ref pSqlResp)
       IF (pSqlResp <> "")
          CALL Muestra_Error(pSqlResp)
          CALL CierraOdbc
       ENDIF
    ENDIF

    // CIERRA LA CONEXION
    CALL CierraOdbc
EndSub 

//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
//*******************************************************************************//
//                      Subrutina IngresaCorreo                                  //
// SOLICITA EMAIL AL CLIENTE E INSERTA EN CUPON                                  //
// MODIFICACION ALSEA                                                            //
// FECHA MODIFICACION: 2017-05-26                                                //
//*******************************************************************************//
///////////////////////////////////////////////////////////////////////////////////
Sub IngresaCorreo(Var IDTicket : A12,Var IDFolioCupon : A10)
    Var pSqlString        : A500 = ""
    Var pSqlResp          : A500 = ""
	var lCorreo           : A150 = ""
    Var Key_Pressed		  : key
    Var PromptLine        : A40 = ""

    FOREVER
       TouchScreen 3056
       FORMAT PromptLine as "Digite Mail y [ENTER]:"
       Input lCorreo, PromptLine
       IF Len(lCorreo) <> 0
          BREAK
       ELSE
          INFOMESSAGE "Cupones", "eMail INCORRECTO"
       ENDIF
    ENDFOR

    // OBTIENE VERSION DE MICROS
    CALL Version_Micros_Cup_SyBase

    // CONECTA A BASE DE DATOS
    CALL AbreConexion

	FORMAT pSqlString as "Update custom.TADatosCupon Set FCEmailCliente='",TRIM(lCorreo),"' WHERE FIIDTicket=",IDTicket," AND FCIDFolioCupon='",IDFolioCupon,"'"

	IF Version_Micros_Cup = 31
       DLLCALL hODBCDLLCUP, sqlExecuteQuery(pSqlString)
       pSqlResp = ""
       DLLCALL hODBCDLLCUP, sqlGetLastErrorString(ref pSqlResp) 
    ELSE
       DLLCALL_CDECL hODBCDLLCUP, sqlExecuteQuery(pSqlString)
       pSqlResp = ""
       DLLCALL_CDECL hODBCDLLCUP, sqlGetLastErrorString(ref pSqlResp) 
    ENDIF

    IF (pSqlResp <> "")
	   CALL DeleteCupon(IDFolioCupon,0)
       CALL Muestra_Error(pSqlResp)
	   // CIERRA LA CONEXION
       CALL CierraOdbc
       EXITCANCEL
    ENDIF

    // CIERRA LA CONEXION
    CALL CierraOdbc
EndSub