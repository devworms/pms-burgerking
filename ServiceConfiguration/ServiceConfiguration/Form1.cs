﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace ServiceConfiguration
{
    public partial class Form1 : Form
    {
        String filename = "C:\\Program Files (x86)\\Devworms\\SetupCRMWOW\\config.txt";
        String ruta = "https://soaprod.grupoalsea.com.mx/Services/Loyalty/ALSEAMemberService";
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnApli_Click(object sender, EventArgs e)
        {
            if (rbTest.Checked == true)
            {

                ruta = "https://soatest.grupoalsea.com.mx/Services/Loyalty/ALSEAMemberService";

            }
            else if (rbDev.Checked == true)
            {
                ruta = "https://alsea-soadev.oracleoutsourcing.com:443/Services/Loyalty/ALSEAMemberService";
            }
            else
            {

                ruta = "https://soaprod.grupoalsea.com.mx/Services/Loyalty/ALSEAMemberService";
            }
            if (File.Exists(filename))
            {
                File.Delete(filename);
            }


            System.IO.StreamWriter file = new System.IO.StreamWriter(filename, true);
            file.WriteLine(ruta, true);

            file.Close();
            MessageBox.Show("Cambios aplicados correctamente");
            this.Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (File.Exists(filename))
            {
                File.Delete(filename);
            }


            System.IO.StreamWriter file = new System.IO.StreamWriter(filename, true);
            file.WriteLine(ruta, true);

            file.Close();
          //  MessageBox.Show("Modo produccion default");
           // this.Close();
        }

       
    }
}
