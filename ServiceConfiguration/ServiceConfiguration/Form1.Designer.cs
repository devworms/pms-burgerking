﻿namespace ServiceConfiguration
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnApli = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.rbProd = new System.Windows.Forms.RadioButton();
            this.rbDev = new System.Windows.Forms.RadioButton();
            this.rbTest = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.btnApli);
            this.groupBox1.Controls.Add(this.btnCancel);
            this.groupBox1.Controls.Add(this.rbProd);
            this.groupBox1.Controls.Add(this.rbDev);
            this.groupBox1.Controls.Add(this.rbTest);
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(260, 163);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Modo";
            // 
            // btnApli
            // 
            this.btnApli.BackColor = System.Drawing.Color.Blue;
            this.btnApli.ForeColor = System.Drawing.Color.White;
            this.btnApli.Location = new System.Drawing.Point(149, 76);
            this.btnApli.Name = "btnApli";
            this.btnApli.Size = new System.Drawing.Size(105, 34);
            this.btnApli.TabIndex = 4;
            this.btnApli.Text = "Aplicar";
            this.btnApli.UseVisualStyleBackColor = false;
            this.btnApli.Click += new System.EventHandler(this.btnApli_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.Red;
            this.btnCancel.Location = new System.Drawing.Point(149, 122);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(105, 34);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancelar";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // rbProd
            // 
            this.rbProd.AutoSize = true;
            this.rbProd.Checked = true;
            this.rbProd.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbProd.ForeColor = System.Drawing.Color.Black;
            this.rbProd.Location = new System.Drawing.Point(6, 131);
            this.rbProd.Name = "rbProd";
            this.rbProd.Size = new System.Drawing.Size(137, 20);
            this.rbProd.TabIndex = 2;
            this.rbProd.TabStop = true;
            this.rbProd.Text = "Modo Producción";
            this.rbProd.UseVisualStyleBackColor = true;
            // 
            // rbDev
            // 
            this.rbDev.AutoSize = true;
            this.rbDev.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbDev.ForeColor = System.Drawing.Color.Black;
            this.rbDev.Location = new System.Drawing.Point(6, 85);
            this.rbDev.Name = "rbDev";
            this.rbDev.Size = new System.Drawing.Size(130, 20);
            this.rbDev.TabIndex = 1;
            this.rbDev.Text = "Modo Desarrollo";
            this.rbDev.UseVisualStyleBackColor = true;
            // 
            // rbTest
            // 
            this.rbTest.AutoSize = true;
            this.rbTest.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbTest.ForeColor = System.Drawing.Color.Black;
            this.rbTest.Location = new System.Drawing.Point(6, 43);
            this.rbTest.Name = "rbTest";
            this.rbTest.Size = new System.Drawing.Size(90, 20);
            this.rbTest.TabIndex = 0;
            this.rbTest.Text = "Modo Test";
            this.rbTest.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(284, 187);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.MaximumSize = new System.Drawing.Size(300, 300);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ServiceConfiguration";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbProd;
        private System.Windows.Forms.RadioButton rbDev;
        private System.Windows.Forms.RadioButton rbTest;
        private System.Windows.Forms.Button btnApli;
        private System.Windows.Forms.Button btnCancel;
    }
}

